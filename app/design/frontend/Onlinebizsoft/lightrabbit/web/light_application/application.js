(function(){
    require(['jquery','prototype'], function(jQuery){
    var ImageLoader = {};

ImageLoader.count = 0;
ImageLoader.loaded = 0;
ImageLoader.isImagesLoaded = function(){
    return this.count==this.loaded;
};
ImageLoader.imageLoaded = function(){
    this.loaded++;
   
    if( this.isImagesLoaded() )
        this.callback(this.loaded_images);
};
ImageLoader.addImage = function(src){
    var that = this;

    this.count++;
    var _image = new Image();
    _image.src = src;
    _image.onload = function(){
        that.imageLoaded();
    }
    return _image;
};
ImageLoader.Load = function(images, callback){
    this.loaded_images = jQuery.extend(true, {}, images);
    this.callback = callback;
    var that = this;
    jQuery.each(this.loaded_images, function(key, value){
        if(!jQuery.isArray(this)){
            that.loaded_images[key] = that.addImage(this);
        }
        else{
            var arr = this,
                _key = key;
            jQuery.each(arr, function(key, value){
                arr[key] = that.addImage(this);
            });
        }
    });

};














function slider(config){
    var that = this;
    this.config = jQuery.extend({}, config);

    this.slide = new Kinetic.Image({
        image: Application.images.slider_horizontal,
        x: that.config.x,
        y: that.config.y
    });
    this.config.layer.add(this.slide);

    this.length = this.slide.getWidth() - 20;

    this.valueChanged = false;

    this.handler = new Kinetic.Image({
        image: Application.images.slider_handler,
        x: that.config.x,
        y: that.config.y,
        offset: [0, 5],
        draggable: true,
        dragBoundFunc: function(pos) {
            var minX = that.slide.getX(),
                maxX = minX + that.length,
                posX;


                var res = that.findClosestPoint(pos.x)
                posX = res.coordinate;
                var value = Number( res.value.toFixed(2) );

                if( value !== that.value ){
                    if( that.value >= value)
                        that.vector = 1;
                    else
                        that.vector = -1;

                    that.value = value;
                    that.valueChanged = true;
                }
                else{
                    that.valueChanged = false;
                }

            return {
                x: posX,
                y: this.getAbsolutePosition().y
            }
        }
    });
    this.handler.on("dragmove", function(){
        var handler = this;

        if(typeof that.config.onvaluechange === "function" && that.valueChanged == true){
            that.config.onvaluechange({
                value: that.value,
                position: that.handler.getAbsolutePosition()
            });
        }
    });
    this.handler.on("mouseenter", function(){
        document.body.style.cursor = "pointer";
    });
    this.handler.on("mouseleave", function(){
        document.body.style.cursor = "default";
    });

    this.config.layer.add(this.handler);

    this.rangeValues = [];
    this.setRange(this.config.range, this.config.h);

    if(this.config.value !== undefined){
        this.setValue( this.config.value );
    }
}
slider.prototype.setValue = function(val){
    var that = this;

    this.value = val;
    this.vector = 1;
    var pos = this.findClosestPoint(undefined, val).coordinate;

    this.handler.setX( pos );

    if(typeof that.config.onvaluechange === "function")
            that.config.onvaluechange({
                value: that.value,
                position: that.handler.getAbsolutePosition()
            });
}

slider.prototype.setRange = function(range, h){
    var start = this.slide.getX(),
        deltaX = this.length / ( (range[1]-range[0])/h ),
        sum = sum;

    for(var i=range[0]; Number(i.toFixed(2)) <= Number(range[1].toFixed(2)); i+=h){
        this.rangeValues.push({
            value: Number(i.toFixed(2)),
            coordinate: start
        });
        start += deltaX;
    }
}
slider.prototype.findClosestPoint = function(pos, value){
    var result = [];

    for(var i=0; i<this.rangeValues.length; i++){
        if(pos !== undefined)
            result.push( Math.abs(pos-this.rangeValues[i].coordinate) )
        else
            result.push( Math.abs(value-this.rangeValues[i].value) )
    }
    var min = Math.min.apply({},result);
    var index = result.indexOf(min);

    return this.rangeValues[index];
}


var Application = {
    container: "#rabbit-application",
    lights: [],
    meters: [],

    init: function(){
        var that = this;

        this.stage = new Kinetic.Stage({
            container: "rabbit-application",
            width: 650,
            height: 400
        });

        this.fieldPadding = 30;
        this.one_meter_eq = 80;


        this.lightHeightMultiplyer = -55;
        this.lightHeight = {
            min: 2,
            max: 5
        };
        this.lightHeightL = Math.abs( (this.lightHeight.max - this.lightHeight.min)*this.lightHeightMultiplyer);

        this.angleRange = {
            min: 7,
            max: 180
        }
        this.angleRangeL = Math.abs( (this.angleRange.max - this.angleRange.min) );



        this.layerControls = new Kinetic.Layer();
        this.stage.add(this.layerControls);

        this.layerLight = new Kinetic.Layer();
        this.stage.add(this.layerLight);
        this.layerLight.setY(400);

        this.center = {
            x: this.stage.getWidth()/2,
            y: this.stage.getHeight()/2
        }

        /*
         *      App params
         */
        this.appData = {};
        this.setData();

        this.angle = parseInt( jQuery(".beam").parent().next("td").text());
        if(this.angle > 180)
                this.angle = this.angle / 2;

        /*
         *      Additional images
         *
         */
        this.logo = new Kinetic.Image({
            image: Application.images.logo,
            x: that.center.x - 427/2,
            y: -10,
            opacity: 0.1
        });
        this.layerControls.add(this.logo);

        this.man = new Kinetic.Image({
            image: Application.images.man,
            x: that.center.x-13,
            y: -95
        });
        this.layerLight.add(this.man);



        /*
         *      Indicators
         */
        this.angle_text = new Kinetic.Text({
            text: "",
            fontSize: 14,
            fontFamily: 'Arial',
            textFill: '#d3d3d3',
            offset: [50, 30]
        });
        this.layerControls.add(this.angle_text);

        this.slider_angle = new slider({
            layer: this.layerControls,
            x: that.center.x - 190,
            y: 30,
            range:[that.angleRange.min, that.angleRange.max],
            h: 1,
            value: 60,
            onvaluechange: function(state){
				var text_angle = jQuery(".beam-angel").data("title");
				var text_set = text_angle ? text_angle : 'Beam angle';
                that.angle_text.setText(text_set + ": "+ state.value);
                that.angle_text.setAbsolutePosition(state.position);

                for(var i = 0; i< that.lights.length; i++){
                    var light = that.lights[i];
                    light.angle = state.value;

                }

                that.layerLight.draw();

                that.calculateData();
            }
        });



        this.height_text = new Kinetic.Text({
            text: "",
            fontSize: 14,
            fontFamily: 'Arial',
            textFill: '#d3d3d3',
            offset: [25, 30]
        });
        this.layerControls.add(this.height_text);

        this.slider_height = new slider({
            layer: this.layerControls,
            x: that.center.x - 190,
            y: 95,
            range:[that.lightHeight.min, that.lightHeight.max],
            h: 0.01,
            value: 2,
            onvaluechange: function(state){
				var text_height = jQuery(".beam-angel").data("height_text");
				var text_set = text_height ? text_height : 'Height';
                that.height_text.setText(text_set + ": "+ state.value);
                that.height_text.setAbsolutePosition(state.position);


                for(var i = 0; i< that.lights.length; i++){
                    var light = that.lights[i];
                    light.setY( state.value * that.lightHeightMultiplyer );
                }
                that.layerLight.draw();

                that.calculateData();
            }
        });

        this.layerControls.draw();
        this.layerLight.draw();


    },
    setData: function(manually){
        manually = manually || false;

        this.appData.square = Number( jQuery("#rabbit-application-container .square").val() ) || 1;

        this.appData.number_of_fixtures =  Number( jQuery("#rabbit-application-container .number_of_fixtures").val() ) || 1;

        if(manually)
            this.appData.fixture_lux = Number( jQuery("#rabbit-application-container .lumens_per_fixture").val() );
        else{
            this.appData.fixture_lux = parseInt( jQuery(".wattage").parent().next("td").text());
            jQuery("#rabbit-application-container .lumens_per_fixture").val( this.appData.fixture_lux || 250 );
        }
        this.appData.fixture_lux = this.appData.fixture_lux || 1;


        this.layerLight.draw();

        if(manually)
            this.calculateData(manually);

    },
    calculateData: function(){


        this.lux = (this.appData.fixture_lux * this.appData.number_of_fixtures) / this.appData.square;
        this.powerful_light = this.lux;

        /*
         *      Dependecies to angle/height
         */
        if(this.slider_height !== undefined && this.slider_angle !== undefined){

            this.lux =  this.powerful_light / (this.slider_height.value - 1);
            this.lux =  (this.lux/5) / (this.slider_angle.value / 180);

        }

        jQuery("#rabbit-application-container .lux_level span").text( this.lux.toFixed(1) );
        this.layerLight.draw();
    },
    addLight: function(config){
        var height = this.lightHeight.max * this.lightHeightMultiplyer,
            width = this.stage.getWidth(),
            that = this;

        if(config.x == "center")
            config.x = width/2;

        var light = new Kinetic.Shape({
            drawFunc: function(context) {
                    var h = 800,
                        b_side = h * Math.tan(light.angle/2*Math.PI/180);

                    var brightness = that.lux/that.powerful_light;

                    context.save();
                    if(this.angle < 180){
                        context.beginPath();
                        context.moveTo(0, 0);
                        context.lineTo(-b_side, h);
                        context.lineTo(b_side, h);
                        context.closePath();
                    }
                    else{
                        context.beginPath();
                        context.rect(-width/2, 0, width, h);
                    }

                    context.fillStyle = "rgba(255,255,255, "+brightness+")";

                    context.shadowColor = 'white';
                    context.shadowBlur = 20;
                    context.shadowOffsetX = 0;
                    context.shadowOffsetY = 0;
                    context.fill();
                    context.restore();
            },

            x: that.center.x,
            y: this.slider_height.value * that.lightHeightMultiplyer
        });
        light.angle = 60;
        light.brightness = 1;


        this.lights.push(light)
        this.layerLight.add(light);

        light.moveToBottom();

        this.layerLight.draw();
    }


};


jQuery(window).on('load', function() {
    var baseImages = "/static/frontend/Onlinebizsoft/lightrabbit/en_US/images/light_application/";
    var images = {
        slider_horizontal: baseImages + "slider_hor.png",
        slider_handler: baseImages + "slider_handler.png",
        logo: baseImages + "logo.png",
        man: baseImages + "man.png"
    };
    ImageLoader.Load(images, function(loaded_images){
        Application.images = loaded_images;
        Application.init();
        Application.addLight({x:"center"});

        Application.calculateData();

        jQuery("#rabbit-application-container .square, #rabbit-application-container .number_of_fixtures, #rabbit-application-container .lumens_per_fixture").keyup(function(){
            Application.setData(true);
        });
        
    });
});

});
})();