var config = {
    paths: {            
            'owlcarousel': "js/owl.carousel",
            d3: "js/d3",
            'bootstrap': "js/bootstrap.min",
            'bootstrap/select': "js/bootstrap-select.min",
            fancybox:'fancybox/source/jquery.fancybox',
            mousewheel:'fancybox/lib/jquery.mousewheel-3.0.6.pack',
        },   
    shim: {
        'owlcarousel': {
            deps: ['jquery']
        },
        'mousewheel': {
            deps: ['jquery']
        },
        'bootstrap': {
            deps: ['jquery']
        },
        'fancybox': {
            deps: ['mousewheel']
        },
        'bootstrap/select': {
            deps: ['bootstrap']
        }
    }
};