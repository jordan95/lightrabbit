var config = {
    shim: {
        'owl.carousel/owl.carousel': {
            deps: ['jquery']
        },
        'Smartwave_Megamenu/js/sw_megamenu': {
            deps: ['jquery']
        },
        'lightslider/lightslider': {
            deps: ['jquery']
        }
    }
};