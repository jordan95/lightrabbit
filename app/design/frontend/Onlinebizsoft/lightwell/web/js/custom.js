 (function  () {
    require(["jquery","owl.carousel/owl.carousel"],function($) {
            $(document).ready(function() {
                $(".stockist-logo").owlCarousel({
                      autoplay: true,
                      loop:true,
                      margin:10,
                      dots:false,
                      autoplayTimeout:5000,
                      nav:false,
                      responsiveClass:true,
                      responsive:{
                          0:{
                              items:3
                          },
                          479:{
                              items:4
                          },
                          767:{
                              items:5
                          },
                          1100:{
                              items:6
                          }
                      }
                    });
                  $("#banner-slider-demo-4").owlCarousel({
                      items: 1,
                      autoplay: true,
                      autoplayTimeout: 5000,
                      autoplayHoverPause: true,
                      navRewind: true,
                      dots: false,
                      animateIn: 'fadeIn',
                      animateOut: 'fadeOut',
                      loop: true,
                      nav: false,
                      dots:true
                    });
                    $(".nav-toggle").click(function(){
                         $("#header-sidenav").toggleClass("open");
                    });
                  
                    $(".scroll-down").click(function(event){   
                      event.preventDefault();
                      $('html,body').animate({scrollTop:$(this.hash).offset().top-156}, 800);
                      
                    });


                    $('#thumbnail-slider ul li').click(function(event){
                        event.preventDefault();
                        $src_thumbs = $(this).children().children('img').attr('src');
                        $('#ninja-slider img').attr('src',$src_thumbs);
                    });
            });
        });
  })();
// Start Sticky Header
(function(){
    require(["jquery"],function($) {
        $(document).ready(function() {
              $(window).scroll(function(){
                  "use strict";
                  if($(window).scrollTop()>10){
                    $(".page-header").addClass("sticky");
                  }else{
                    $(".page-header").removeClass("sticky");                     
                  }
                });
      });
    });
  })();
// End Sticky Header
// Start Toogle Form Search Header And Toogle Menu Top
(function(){
    require(["jquery"],function($) {
        $(document).ready(function() {
             $(".icon-search-mobie").on("click",function(){
                  $(".search-header").stop(true,true).slideToggle();
             }); 
             $(".menu-header li").on("click",function(){
                  $(this).children("ul").slideToggle();
                  $(this).children("i").stop(true,true).toggleClass("porto-icon-up-open");
                  $(this).children("i").stop(true,true).toggleClass("porto-icon-down-open");
             }); 
      });
    });
  })();
// End Toogle Form Search Header  And Toogle Menu Top
(function  () {
    require(["jquery","lightslider/lightslider"],function($) {
      $(document).ready(function() {
        if($(window).width()>1024){
           var option = {
              gallery:true,
              item:1,
              vertical:true,
              verticalHeight:380,
              vThumbWidth:135,
              thumbItem:4,
              thumbMargin:4,
              slideMargin:0
            };
          }else{
             var option = {
                 adaptiveHeight:true,
                item:1,
                slideMargin:0,
                loop:true
              };
          }
        $('#ninja-slider').lightSlider(option);  
      });
  });
})();