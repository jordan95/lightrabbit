(function  () {
    require(["jquery"],function($) {
            $(document).ready(function() {
             
                $(".scroll-down").click(function(event){   
                    event.preventDefault();
                    $('html,body').animate({scrollTop:$(this.hash).offset().top-87}, 800);          
                  });
                //Contact Popup
                $('.contact-popup').on('click',function(){
                    $("#hdmx__contact-popup").toggle();
                    $("#helpdesk-contact-form-overlay").toggle();
                });
                $(window).on('click',function(event){
                      if (event.target == $("#hdmx__contact-popup")) {
                        $("#hdmx__contact-popup").hide();
                      }
                });
                $(document).on('click','.wrap-header-option .click-question',function(event){   
                  event.preventDefault();
                  $(".wrap-header-option .wrap-info-help").show();
                });
                $(document).on('click','.wrap-header-option .close-popup',function(event){   
                  event.preventDefault();
                  $(".wrap-header-option .wrap-info-help").hide();
                });
                
                //Show hide menutop 
                 $('.menu-toggle').on('click',function(){
                    if(!$('.menu-header').hasClass('open')){
                      $('.menu-header').addClass('open');
                      $("body").addClass('open-menu');
                      $("#header").addClass('open-menu');
                    }else{
                      $('.menu-header').removeClass('open');
                      $("body").removeClass('open-menu');
                      $("#header").removeClass('open-menu');
                    }
                    
                });
                $(window).resize(function(){
                    $('.menu-header').removeClass('open');
                    $("body").removeClass('open-menu');
                    $("#header").removeClass('open-menu');
                });
                    


              // start Custom Select style
                   $('select#listdown').each(function(){
                      var $this = $(this), numberOfOptions = $(this).children('option').length;
                    
                      $this.addClass('select-hidden'); 
                      $this.wrap('<div class="select"></div>');
                      $this.after('<div class="select-styled"><span></span></div>');

                      var $styledSelect = $this.next('div.select-styled');
                      $styledSelect = $styledSelect.children('span');
                      $styledSelect.text($this.children('option').eq(0).text());
                      var $list = $('<ul />', {
                          'class': 'select-options'
                      }).insertAfter($styledSelect);
                    
                      for (var i = 0; i < numberOfOptions; i++) {
                          $('<li />', {
                              text: $this.children('option').eq(i).text(),
                              rel: $this.children('option').eq(i).val(),
                              class: $this.children('option').eq(i).val()
                          }).appendTo($list);
                      }
                    
                      var $listItems = $list.children('li');
                    
                      $styledSelect.click(function(e) {
                          e.stopPropagation();
                          $('div.select-styled.active').not(this).each(function(){
                              $(this).removeClass('active').next('ul.select-options').hide();
                          });
                          $(this).toggleClass('active').next('ul.select-options').toggle();
                      });
                    
                      $listItems.click(function(e) {
                          e.stopPropagation();
                          $styledSelect.text($(this).text()).removeClass('active');
                          $this.val($(this).attr('rel'));
                          $list.hide();
                          //console.log($this.val());
                      });
                    
                      $(document).click(function() {
                          $styledSelect.removeClass('active');
                          $list.hide();
                      });

                  });
              // end Custom Select style   
            });
        });
  })();

