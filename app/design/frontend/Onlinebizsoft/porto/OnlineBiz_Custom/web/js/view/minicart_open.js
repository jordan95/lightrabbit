define(["jquery/ui","jquery"], function(Component, $){
    return function(config, element){
        var minicart = $(element);
        minicart.on('contentLoading', function () {
            minicart.on('contentUpdated', function (e) {
            	if($(window).scrollTop() >=200){
            		$('html,body').animate({scrollTop:$(".page-wrapper").offset().top}, 800,function(){minicart.find('[data-role="dropdownDialog"]').dropdownDialog("open");});            		
	                $('html,body').clearQueue();
	                
            		if($(window).width()<=992){
            			setTimeout(function() {
            			$('html,body').animate({scrollTop:$(".page-wrapper").offset().top}, 800,function(){minicart.find('[data-role="dropdownDialog"]').dropdownDialog('close');});
			                
			                 $('html,body').clearQueue();
	                	}, 1000);
            		}
	            }
            		
            });
        });
    }
});