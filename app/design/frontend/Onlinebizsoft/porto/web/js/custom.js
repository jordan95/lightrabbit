 (function  () {
    require(["jquery"],function($) {
            $(document).ready(function() {
                  //Add(+/-) Button Number Incrementers
                  $(".incr-btn").on("click", function(e) {
                    var $button = $(this);
                    var oldValue = $button.parent().find("input").val();
                    if ($button.hasClass('bundle-plus')) {
                      var newVal = parseFloat(oldValue) + 1;
                    }
                    if($button.hasClass('bundle-minus')){
                        // Don't allow decrementing below 1
                        if (oldValue > 1) {
                          var newVal = parseFloat(oldValue) - 1;
                        } else {
                          newVal = 0;
                        }
                    } 
                    $button.parent().find("input").val(newVal);
                    $button.parent().find("input").trigger('change');
                    e.preventDefault();
                  });
            });
        });
    require([
          'jquery',
          'owl.carousel/owl.carousel'
      ], function ($) {
          $(document).ready(function () {
              $(".slider-blog-recent").owlCarousel({
                  autoplay: true,
                  autoplayTimeout: 2000,
                  autoplayHoverPause: true,
                  margin: 30,
                  nav: false,
                  dots:true,
                  loop:true,
                  items:1
              });
          });
      });
  })();
