<?php
if (version_compare(phpversion(), '5.2.0', '<')===true) {
    echo  '<div style="font:12px/1.35em arial, helvetica, sans-serif;"><div style="margin:0 0 25px 0; border-bottom:1px solid #ccc;"><h3 style="margin:0; font-size:1.7em; font-weight:normal; text-transform:none; text-align:left; color:#2f2f2f;">Whoops, it looks like you have an invalid PHP version.</h3></div><p>Magento supports PHP 5.2.0 or newer. <a href="http://www.magentocommerce.com/install" target="">Find out</a> how to install</a> Magento using PHP-CGI as a work-around.</p></div>';
    exit;
}

if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
    $ip = isset($ips[0]) ? $ips[0]: '';
    if ($ip) {
        $_SERVER['REMOTE_ADDR'] = $ip;
        $_SERVER['HTTP_X_FORWARDED_FOR'] = $ip;
    }
}

if (isset($_SERVER['HTTP_X_CURRENCY']) && $_SERVER['HTTP_X_CURRENCY']) {
	$_COOKIE['currency'] = $_SERVER['HTTP_X_CURRENCY'];
}
require_once 'vendor/autoload.php';

//print_r($_SERVER);
/**
 * Error reporting
 */
error_reporting(E_ALL | E_STRICT);

/**
 * Compilation includes configuration file
 */
$compilerConfig = 'includes/config.php';
if (file_exists($compilerConfig)) {
    include $compilerConfig;
}

$mageFilename = 'app/Mage.php';
$maintenanceFile = 'maintenance.flag';

if (!file_exists($mageFilename)) {
    if (is_dir('downloader')) {
        header("Location: downloader");
    } else {
        echo $mageFilename." was not found";
    }
    exit;
}

if (file_exists($maintenanceFile)) {
    include_once dirname(__FILE__) . '/errors/503.php';
    exit;
}

if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && (isset($_SERVER['HTTP_X_FORWARDED_PORT']) && $_SERVER['HTTP_X_FORWARDED_PORT'] == 443)) {
  $_SERVER['HTTPS'] = 'on';
  $_SERVER['SERVER_PORT'] = 443;
} else {
  $_SERVER['SERVER_PORT'] = 80;
}


require_once $mageFilename;

#Varien_Profiler::enable();

if (isset($_SERVER['MAGE_IS_DEVELOPER_MODE'])) {
    //Mage::setIsDeveloperMode(true);
}

if (isset($_SERVER['HTTP_HOST']) && (strpos($_SERVER['HTTP_HOST'], 'dev') !== FALSE || strpos($_SERVER['HTTP_HOST'], 'test') !== FALSE)) {
	ini_set('display_errors', 1);
	Mage::setIsDeveloperMode(true);
}

ini_set('max_input_vars', 10000);

umask(0);

/* Store or website code */
$mageRunCode = isset($_SERVER['MAGE_RUN_CODE']) ? $_SERVER['MAGE_RUN_CODE'] : '';

/* Run store or run website */
$mageRunType = isset($_SERVER['MAGE_RUN_TYPE']) ? $_SERVER['MAGE_RUN_TYPE'] : 'store';
if(isset($_SERVER['HTTP_HOST']))
{
	switch ($_SERVER['HTTP_HOST']) {
		case 'ring-paare.dev':
		case 'ring-paare.de':
		case 'www.ring-paare.de':
		case 'www1.ring-paare.de':
		case 'test.ring-paare.de':
		case 'dev.ring-paare.de':
			$mageRunCode = 'de';
			$mageRunType = 'website';
		break;
		case 'm.ring-paare.de':
			define('MOBILE_FOLDER', 'm');
			$mageRunCode = 'de';
			$mageRunType = 'website';
		break;
		case 'glamira.de':
		case 'www.glamira.de':
		case 'www1.glamira.de':
		case 'test.glamira.de':
		case 'dev.glamira.de':
		case 'glamira.dev':
			$mageRunCode = 'glde';
		break;
		case 'm.glamira.de':
			//define('MOBILE_FOLDER', 'm');
			$mageRunCode = 'glde';
		break;
		case 'glamira.es':
		case 'www.glamira.es':
		case 'www1.glamira.es':
		case 'dev.glamira.es':
		case 'glamiraes.dev':
			$mageRunCode = 'gles';
		break;
		case 'm.glamira.es':
			//define('MOBILE_FOLDER', 'm');
			$mageRunCode = 'gles';
		break;
		case 'alianzas.glamira.es':
			$mageRunCode = 'es';
		break;
		case 'glamira.co.uk':
		case 'www.glamira.co.uk':
		case 'www1.glamira.co.uk':
		case 'test.glamira.co.uk':
		case 'dev.glamira.co.uk':
			$mageRunCode = 'glgb';
		break;
		case 'm.glamira.co.uk':
			//define('MOBILE_FOLDER', 'm');
			$mageRunCode = 'glgb';
		break;
		case 'wedding.glamira.co.uk':
			$mageRunCode = 'gb';
		break;
		case 'glamira.at':
		case 'www.glamira.at':
			$mageRunCode = 'glat';
		break;
		case 'm.glamira.at':
			//define('MOBILE_FOLDER', 'm');
			$mageRunCode = 'glat';
		break;
		case 'www.b2b.glamira.at':
		case 'b2b.glamira.at':
			define('WHOLESALE_URL', 'b2b.glamira.at');
			$mageRunCode = 'glat';
		break;
		case 'glamira.ch':
		case 'www.glamira.ch':
			$mageRunCode = 'glch';
		break;
		case 'm.glamira.ch':
			//define('MOBILE_FOLDER', 'm');
			$mageRunCode = 'glch';
		break;
		case 'www.b2b.glamira.ch':
		case 'b2b.glamira.ch':
			define('WHOLESALE_URL', 'b2b.glamira.ch');
			$mageRunCode = 'glch';
		break;
		case 'glamira.nl':
		case 'www.glamira.nl':
			$mageRunCode = 'glnl';
		break;
		case 'm.glamira.nl':
			//define('MOBILE_FOLDER', 'm');
			$mageRunCode = 'glnl';
		break;
		case 'www.b2b.glamira.nl':
		case 'b2b.glamira.nl':
			define('WHOLESALE_URL', 'b2b.glamira.nl');
			$mageRunCode = 'glnl';
		break;
		case 'glamira.dk':
		case 'www.glamira.dk':
			$mageRunCode = 'gldk';
		break;
		case 'm.glamira.dk':
			//define('MOBILE_FOLDER', 'm');
			$mageRunCode = 'gldk';
		break;
		case 'www.b2b.glamira.dk':
		case 'b2b.glamira.dk':
			define('WHOLESALE_URL', 'b2b.glamira.dk');
			$mageRunCode = 'gldk';
		break;
		case 'glamira.fr':
		case 'www.glamira.fr':
			$mageRunCode = 'glfr';
		break;
		case 'm.glamira.fr':
			//define('MOBILE_FOLDER', 'm');
			$mageRunCode = 'glfr';
		break;
		case 'www.b2b.glamira.fr':
		case 'b2b.glamira.fr':
			define('WHOLESALE_URL', 'b2b.glamira.fr');
			$mageRunCode = 'glfr';
		break;
		case 'glamira.it':
		case 'www.glamira.it':
			$mageRunCode = 'glit';
		break;
		case 'm.glamira.it':
			//define('MOBILE_FOLDER', 'm');
			$mageRunCode = 'glit';
		break;
		case 'www.b2b.glamira.it':
		case 'b2b.glamira.it':
			define('WHOLESALE_URL', 'b2b.glamira.it');
			$mageRunCode = 'glit';
		break;
		case 'glamira.fi':
		case 'www.glamira.fi':
			$mageRunCode = 'glfi';
		break;
		case 'm.glamira.fi':
			//define('MOBILE_FOLDER', 'm');
			$mageRunCode = 'glfi';
		break;
		case 'www.b2b.glamira.fi':
		case 'b2b.glamira.fi':
			define('WHOLESALE_URL', 'b2b.glamira.fi');
			$mageRunCode = 'glfi';
		break;
		case 'glamira.com.tr':
		case 'www.glamira.com.tr':
			$mageRunCode = 'cotr';
		break;
		case 'm.glamira.com.tr':
			//define('MOBILE_FOLDER', 'm');
			$mageRunCode = 'cotr';
		break;
		case 'www.b2b.glamira.com.tr':
		case 'b2b.glamira.com.tr':
			define('WHOLESALE_URL', 'b2b.glamira.com.tr');
			$mageRunCode = 'cotr';
		break;
		case 'www.glamira.net':
		case 'glamira.net':
			$mageRunCode = 'sale';
			$mageRunType = 'website';
		break;
		case 'm.glamira.net':
			//define('MOBILE_FOLDER', 'm');
			$mageRunCode = 'sale';
			$mageRunType = 'website';
		break;
		case 'www.glamira.se':
		case 'glamira.se':
			$mageRunCode = 'glse';
		break;
		case 'm.glamira.se':
			//define('MOBILE_FOLDER', 'm');
			$mageRunCode = 'glse';
		break;
		case 'www.b2b.glamira.se':
		case 'b2b.glamira.se':
			define('WHOLESALE_URL', 'b2b.glamira.se');
			$mageRunCode = 'glse';
		break;
		case 'www.glamira.ru':
		case 'glamira.ru':
			$mageRunCode = 'glru';
		break;
		case 'm.glamira.ru':
			//define('MOBILE_FOLDER', 'm');
			$mageRunCode = 'glru';
		break;
		case 'www.b2b.glamira.ru':
		case 'b2b.glamira.ru':
			define('WHOLESALE_URL', 'b2b.glamira.ru');
			$mageRunCode = 'glru';
		break;
		case 'www.glamira.cn':
		case 'glamira.cn':
			$mageRunCode = 'glcn';
		break;
		case 'm.glamira.cn':
			//define('MOBILE_FOLDER', 'm');
			$mageRunCode = 'glcn';
		break;
		case 'www.b2b.glamira.cn':
		case 'b2b.glamira.cn':
			define('WHOLESALE_URL', 'b2b.glamira.cn');
			$mageRunCode = 'glcn';
		break;
		case 'www.glamira.jp':
		case 'glamira.jp':
			$mageRunCode = 'gljp';
		break;
		case 'm.glamira.jp':
			//define('MOBILE_FOLDER', 'm');
			$mageRunCode = 'gljp';
		break;	
		case 'www.glamira.no':
		case 'glamira.no':
			$mageRunCode = 'glno';
		break;
		case 'm.glamira.no':
			//define('MOBILE_FOLDER', 'm');
			$mageRunCode = 'glno';
		break;
		case 'www.b2b.glamira.no':
		case 'b2b.glamira.no':
			define('WHOLESALE_URL', 'b2b.glamira.no');
			$mageRunCode = 'glno';
		break;
		case 'www.glamira.com.br':
		case 'glamira.com.br':
			$mageRunCode = 'glbr';
		break;
		case 'www.b2b.glamira.com.br':
		case 'b2b.glamira.com.br':
			define('WHOLESALE_URL', 'b2b.glamira.com.br');
			$mageRunCode = 'glbr';
		break;
		case 'www.glamira.com.au':
		case 'glamira.com.au':
			$mageRunCode = 'glau';
		break;
		case 'm.glamira.com.br':
			//define('MOBILE_FOLDER', 'm');
			$mageRunCode = 'glbr';
		break;
		case 'www.b2b.glamira.com.au':
		case 'b2b.glamira.com.au':
			define('WHOLESALE_URL', 'b2b.glamira.com.au');
			$mageRunCode = 'glau';
		break;
		case 'www.b2b.glamira.co.uk':
		case 'b2b.glamira.co.uk':
			define('WHOLESALE_URL', 'b2b.glamira.co.uk');
			$mageRunCode = 'glgb';
		break;
		case 'www.b2b.glamira.de':
		case 'b2b.glamira.de':
			define('WHOLESALE_URL', 'b2b.glamira.de');
			$mageRunCode = 'glde';
		break;
		case 'www.b2b.glamira.es':
		case 'b2b.glamira.es':
			define('WHOLESALE_URL', 'b2b.glamira.es');
			$mageRunCode = 'gles';
		break;
		case 'www.b2b.glamira.jp':
		case 'b2b.glamira.jp':
			define('WHOLESALE_URL', 'b2b.glamira.jp');
			$mageRunCode = 'gljp';
		break;
		//pt store
		case 'www.glamira.pt':
		case 'glamira.pt':
			$mageRunCode = 'glpt';
		break;
		case 'b2b.glamira.pt':
			define('WHOLESALE_URL', 'b2b.glamira.pt');
			$mageRunCode = 'glpt';
		break;
		
		//sg store
		case 'www.glamira.sg':
		case 'glamira.sg':
			$mageRunCode = 'glsg';
		break;
		case 'b2b.glamira.sg':
			define('WHOLESALE_URL', 'b2b.glamira.sg');
			$mageRunCode = 'glsg';
		break;

		//ee store
		case 'www.glamira.ee':
		case 'glamira.ee':
			$mageRunCode = 'glee';
		break;
		case 'b2b.glamira.sg':
			define('WHOLESALE_URL', 'b2b.glamira.ee');
			$mageRunCode = 'glee';
		break;
		
		//hr store
		case 'www.glamira.hr':
		case 'glamira.hr':
			$mageRunCode = 'glhr';
		break;
		case 'b2b.glamira.hr':
			define('WHOLESALE_URL', 'b2b.glamira.hr');
			$mageRunCode = 'glhr';
		break;
		
		//lt store
		case 'www.glamira.lt':
		case 'glamira.lt':
			$mageRunCode = 'gllt';
		break;
		case 'b2b.glamira.lt':
			define('WHOLESALE_URL', 'b2b.glamira.lt');
			$mageRunCode = 'gllt';
		break;
		
		//lv store
		case 'www.glamira.lv':
		case 'glamira.lv':
			$mageRunCode = 'gllv';
		break;
		case 'b2b.glamira.lv':
			define('WHOLESALE_URL', 'b2b.glamira.lv');
			$mageRunCode = 'gllv';
		break;
		
		//ca store
		case 'www.glamira.ca':
		case 'glamira.ca':
			$mageRunCode = 'glca';
		break;
		case 'b2b.glamira.ca':
			define('WHOLESALE_URL', 'b2b.glamira.ca');
			$mageRunCode = 'glca';
		break;

		//cz store
		case 'www.glamira.cz':
		case 'glamira.cz':
			$mageRunCode = 'glcz';
		break;
		case 'b2b.glamira.cz':
			define('WHOLESALE_URL', 'b2b.glamira.cz');
			$mageRunCode = 'glcz';
		break;
		
		//sk store
		case 'www.glamira.sk':
		case 'glamira.sk':
			$mageRunCode = 'glsk';
		break;
		case 'b2b.glamira.sk':
			define('WHOLESALE_URL', 'b2b.glamira.sk');
			$mageRunCode = 'glsk';
		break;	

		//hu store
		case 'www.glamira.hu':
		case 'glamira.hu':
			$mageRunCode = 'glhu';
		break;
		case 'b2b.glamira.hu':
			define('WHOLESALE_URL', 'b2b.glamira.hu');
			$mageRunCode = 'glhu';
		break;	

		//pl store
		case 'www.glamira.pl':
		case 'glamira.pl':
			$mageRunCode = 'glpl';
		break;
		case 'b2b.glamira.pl':
			define('WHOLESALE_URL', 'b2b.glamira.pl');
			$mageRunCode = 'glpl';
		break;	

		//ro store
		case 'www.glamira.ro':
		case 'glamira.ro':
			$mageRunCode = 'glro';
		break;
		case 'b2b.glamira.ro':
			define('WHOLESALE_URL', 'b2b.glamira.ro');
			$mageRunCode = 'glro';
		break;		
		
		//rs store
		case 'www.glamira.rs':
		case 'glamira.rs':
			$mageRunCode = 'glrs';
		break;
		case 'b2b.glamira.rs':
			define('WHOLESALE_URL', 'b2b.glamira.rs');
			$mageRunCode = 'glrs';
		break;			
		
		//ie store
		case 'www.glamira.ie':
		case 'glamira.ie':
			$mageRunCode = 'glie';
		break;
		case 'b2b.glamira.ie':
			define('WHOLESALE_URL', 'b2b.glamira.ie');
			$mageRunCode = 'glie';
		break;
		
		//kr store
		case 'www.glamira.co.kr':
		case 'www.glamira.kr':
		case 'glamira.co.kr':
		case 'glamira.kr':
			$mageRunCode = 'glkr';
		break;
		case 'b2b.glamira.co.kr':
		case 'b2b.glamira.kr':
			define('WHOLESALE_URL', 'b2b.glamira.kr');
			$mageRunCode = 'glkr';
		break;		

		//us store
		case 'www.glamira.com':
		case 'glamira.com':
			$mageRunCode = 'glus';
		break;
		case 'b2b.glamira.com':
			define('WHOLESALE_URL', 'b2b.glamira.com');
			$mageRunCode = 'glus';
		break;		
		
		//vn store
		case 'www.glamira.vn':
		case 'www.glamira.com.vn':
		case 'glamira.com.vn':
		case 'glamira.vn':
			$mageRunCode = 'glvn';
		break;
		case 'b2b.glamira.vn':
		case 'b2b.glamira.com.vn':
			define('WHOLESALE_URL', 'b2b.glamira.com.vn');
			$mageRunCode = 'glvn';
		break;	
		
		//hk store
		case 'www.glamira.com.hk':
		case 'glamira.com.hk':
		case 'www.glamira.hk':
		case 'glamira.hk':
			$mageRunCode = 'glhk';
		break;
		case 'b2b.glamira.com.hk':
		case 'b2b.glamira.hk':
			define('WHOLESALE_URL', 'b2b.glamira.hk');
			$mageRunCode = 'glhk';
		break;		

		//ar store
		case 'www.glamira.ar':
		case 'glamira.ar':
			$mageRunCode = 'glar';
		break;
		case 'b2b.glamira.ar':
			define('WHOLESALE_URL', 'b2b.glamira.ar');
			$mageRunCode = 'glar';
		break;

		//mt store
		case 'www.glamira.com.mt':
		case 'glamira.com.mt':
			$mageRunCode = 'glmt';
		break;
		case 'b2b.glamira.com.mt':
			define('WHOLESALE_URL', 'b2b.glamira.com.mt');
			$mageRunCode = 'glmt';
		break;

		//be store
		case 'www.glamira.be':
		case 'glamira.be':
			$mageRunCode = 'glbe_nl';
		break;
		case 'b2b.glamira.be':
			define('WHOLESALE_URL', 'b2b.glamira.be');
			$mageRunCode = 'glbe_nl';
		break;
		
		//moncoeur diamonds website
		case 'moncoeur-diamonds.com':
		case 'www.moncoeur-diamonds.com':
		case 'moncoeurdiamonds.com':
		case 'www.moncoeurdiamonds.com':
			$mageRunCode = 'moncoeur_de';
		break;
		
		//usa store
		case 'www.glamira.com':
		case 'glamira.com':
			$mageRunCode = 'glus';
		break;
		case 'b2b.glamira.com':
			define('WHOLESALE_URL', 'b2b.glamira.com');
			$mageRunCode = 'glus';
		break;	

		//mx store
		case 'www.glamira.com.mx':
		case 'glamira.com.mx':
			$mageRunCode = 'glmx';
		break;
		case 'b2b.glamira.com.mx':
			define('WHOLESALE_URL', 'b2b.glamira.com.mx');
			$mageRunCode = 'glmx';
		break;	

		//nz store
		case 'www.glamira.co.nz':
		case 'glamira.co.nz':
			$mageRunCode = 'glnz';
		break;
		case 'b2b.glamira.co.nz':
			define('WHOLESALE_URL', 'b2b.glamira.co.nz');
			$mageRunCode = 'glnz';
		break;	
		
		//ae store
		case 'www.glamira.ae':
		case 'glamira.ae':
			$mageRunCode = 'glae';
		break;
		case 'b2b.glamira.ae':
			define('WHOLESALE_URL', 'b2b.glamira.ae');
			$mageRunCode = 'glae';
		break;	

		//in store
		case 'www.glamira.in':
		case 'glamira.in':
			$mageRunCode = 'glin';
		break;
		case 'b2b.glamira.in':
			define('WHOLESALE_URL', 'b2b.glamira.in');
			$mageRunCode = 'glin';
		break;	

		//ar store
		case 'www.glamira.com.ar':
		case 'glamira.com.ar':
			$mageRunCode = 'glar';
		break;
		case 'b2b.glamira.com.ar':
			define('WHOLESALE_URL', 'b2b.glamira.com.ar');
			$mageRunCode = 'glar';
		break;	

		//bg store
		case 'www.glamira.bg':
		case 'glamira.bg':
			$mageRunCode = 'glbg';
		break;
		case 'b2b.glamira.bg':
			define('WHOLESALE_URL', 'b2b.glamira.bg');
			$mageRunCode = 'glbg';
		break;	

		//ua store
		case 'www.glamira.com.ua':
		case 'glamira.com.ua':
			$mageRunCode = 'glua';
		break;
		case 'b2b.glamira.com.ua':
			define('WHOLESALE_URL', 'b2b.glamira.com.ua');
			$mageRunCode = 'glua';
		break;	

		//co store
		case 'www.glamira.com.co':
		case 'glamira.com.co':
			$mageRunCode = 'glco';
		break;
		case 'b2b.glamira.com.co':
			define('WHOLESALE_URL', 'b2b.glamira.com.co');
			$mageRunCode = 'glco';
		break;
		
		//do store
		case 'www.glamira.com.do':
		case 'glamira.com.do':
			$mageRunCode = 'gldo';
		break;
		case 'b2b.glamira.com.do':
			define('WHOLESALE_URL', 'b2b.glamira.com.do');
			$mageRunCode = 'gldo';
		break;		

		//gt store
		case 'www.glamira.com.gt':
		case 'glamira.com.gt':
			$mageRunCode = 'glgt';
		break;
		case 'b2b.glamira.com.gt':
			define('WHOLESALE_URL', 'b2b.glamira.com.gt');
			$mageRunCode = 'glgt';
		break;	

		//cr store
		case 'www.glamira.co.cr':
		case 'glamira.co.cr':
			$mageRunCode = 'glcr';
		break;
		case 'b2b.glamira.co.cr':
			define('WHOLESALE_URL', 'b2b.glamira.co.cr');
			$mageRunCode = 'glcr';
		break;	
		
		//pe store
		case 'www.glamira.com.pe':
		case 'glamira.com.pe':
			$mageRunCode = 'glpe';
		break;
		case 'b2b.glamira.com.pe':
			define('WHOLESALE_URL', 'b2b.glamira.com.pe');
			$mageRunCode = 'glpe';
		break;				
	
		//ec store
		case 'www.glamira.com.ec':
		case 'glamira.com.ec':
			$mageRunCode = 'glec';
		break;
		case 'b2b.glamira.com.ec':
			define('WHOLESALE_URL', 'b2b.glamira.com.ec');
			$mageRunCode = 'glec';
		break;	
		
		//hn store
		case 'www.glamira.hn':
		case 'glamira.hn':
			$mageRunCode = 'glhn';
		break;
		case 'b2b.glamira.hn':
			define('WHOLESALE_URL', 'b2b.glamira.hn');
			$mageRunCode = 'glhn';
		break;			
		
		//bo store
		case 'www.glamira.com.bo':
		case 'glamira.com.bo':
			$mageRunCode = 'glbo';
		break;
		case 'b2b.glamira.com.bo':
			define('WHOLESALE_URL', 'b2b.glamira.com.bo');
			$mageRunCode = 'glbo';
		break;	
		
		//za store
		case 'www.glamira.co.za':
		case 'glamira.co.za':
			$mageRunCode = 'glza';
		break;
		case 'b2b.glamira.co.za':
			define('WHOLESALE_URL', 'b2b.glamira.co.za');
			$mageRunCode = 'glza';
		break;			
		
		//si store
		case 'www.glamira.si':
		case 'glamira.si':
			$mageRunCode = 'glsi';
		break;
		case 'b2b.glamira.si':
			define('WHOLESALE_URL', 'b2b.glamira.si');
			$mageRunCode = 'glsi';
		break;		


	}
}

function shutdownCapture() {
    $error = error_get_last();
    if ($error !== null && (strpos($error['message'], 'Allowed memory size') !== false 
						|| strpos($error['message'], 'Maximum execution time of') !== false 
						|| strpos($error['message'], 'Uncaught Error: ') !== false 
						|| strpos($error['message'], 'Uncaught PDOException: ') !== false 
						|| strpos($error['message'], 'Call to undefined method') !== false 						
						|| strpos($error['message'], 'Call to a member function') !== false)) {
		file_put_contents('/home/cloudpanel/htdocs/www.glamira.de/shared/var/log/fatal_error.log', date('Y-m-d H:i:s'). "\r\n", FILE_APPEND);
		file_put_contents('/home/cloudpanel/htdocs/www.glamira.de/shared/var/log/fatal_error.log', $error['message']. "\r\n", FILE_APPEND);
		file_put_contents('/home/cloudpanel/htdocs/www.glamira.de/shared/var/log/fatal_error.log', $_SERVER['REQUEST_URI']. '?' . $_SERVER['QUERY_STRING'] . "\r\n", FILE_APPEND);
		file_put_contents('/home/cloudpanel/htdocs/www.glamira.de/shared/var/log/fatal_error.log', print_r($_REQUEST, true). "\r\n", FILE_APPEND);
		file_put_contents('/home/cloudpanel/htdocs/www.glamira.de/shared/var/log/fatal_error.log', print_r($_SERVER, true). "\r\n", FILE_APPEND);
	}
}
register_shutdown_function('shutdownCapture');

$start = microtime(true);
Mage::run($mageRunCode, $mageRunType);
$end = microtime(true);
$route = Mage::app()->getRequest()->getRouteName();

if (($route=='checkout' || $route=='customcheckout' || $route=='customer') && $mageRunCode=='de') {
	if (!isset($_SERVER['HTTP_REFERER'])) {
		$_SERVER['HTTP_REFERER'] = '';
	}
	$controller = Mage::app()->getRequest()->getControllerName();
	$action = Mage::app()->getRequest()->getActionName();
	Mage::log($_SERVER['REMOTE_ADDR'] . ' ' . ($end-$start). ' seconds ' . $route . '/' . $controller . '/' . $action . ' ('.$_SERVER['HTTP_REFERER'].' - '.Mage::helper('core/http')->getHttpUserAgent().')' , null ,'checkout_'.$mageRunCode.'.log');
}

if (($end-$start) >= 6) {
	if (!isset($_SERVER['HTTP_REFERER'])) {
		$_SERVER['HTTP_REFERER'] = '';
	}
	Mage::log($_SERVER['REMOTE_ADDR'] . ' ' . ($end-$start). ' seconds ' . Mage::helper('core/url')->getCurrentUrl() . ' ('.$_SERVER['HTTP_REFERER'].' - '.Mage::helper('core/http')->getHttpUserAgent().')' , null ,'slow_'.$mageRunCode.'.log');
}

if ($route=='cms' && Mage::app()->getRequest()->getControllerName()=='index' && Mage::app()->getRequest()->getActionName()=='noRoute' && $mageRunCode=='de') {
	if (!isset($_SERVER['HTTP_REFERER'])) {
		$_SERVER['HTTP_REFERER'] = '';
	}
	Mage::log($_SERVER['REMOTE_ADDR'] . ' ' . ($end-$start). ' seconds ' . Mage::helper('core/url')->getCurrentUrl() . ' ('.$_SERVER['HTTP_REFERER'].' - '.Mage::helper('core/http')->getHttpUserAgent().')' , null ,'404_'.$mageRunCode.'.log');
}
   
if (isset($_GET['profile'])) {
	$caches = Mage::app()->getLogCache();
	foreach($caches as $key => $cache)
	{
		echo '<div><b>'.$key . '</b></div><br /><div style="text-align:left; padding-left:10px;">';
		
		foreach($cache as $values)
		{
			echo '<i>Id: '.$values['id']. '</i>. Time: '. $values['time']. '<br/>';
			if($values['tags']) 
				echo 'Tags: '. implode(',',$values['tags']). '<br/>';
		}
		echo '</div><br/>';
	}
	$totalCacheTime = Mage::app()->getCacheTotalTime();
	echo '<b>Total cache time: '.$totalCacheTime . ' </b>';
}