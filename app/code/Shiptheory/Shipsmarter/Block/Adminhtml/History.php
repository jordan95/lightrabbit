<?php

/**
 * @author Mad Capsule Media
 * @package Shiptheory_Shipsmarter
 */

namespace Shiptheory\Shipsmarter\Block\Adminhtml;

class History extends \Magento\Backend\Block\Widget\Grid\Container {

    /**
     * constructor
     *
     * @return void
     */
    protected function _construct() {
        $this->_controller = 'adminhtml_history';
        $this->_blockGroup = 'Shiptheory_Shipsmarter';
        $this->_headerText = __('Shiptheory History');

        parent::_construct();
        $this->removeButton('add');
    }

}
