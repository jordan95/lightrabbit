<?php

/**
 * @author Mad Capsule Media
 * @package Shiptheory_Shipsmarter
 */

namespace Shiptheory\Shipsmarter\Controller\Adminhtml\History;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use \Shiptheory\Shipsmarter\Helper\Data;

class Ship extends \Magento\Sales\Controller\Adminhtml\Order\AbstractMassAction {

    /**
     * constructor
     */
    public function __construct(
    \Shiptheory\Shipsmarter\Model\HistoryFactory $historyFactory, \Magento\Ui\Component\MassAction\Filter $filter, \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $collectionFactory, \Magento\Backend\App\Action\Context $context, Data $helper
    ) {
        parent::__construct($context, $filter);
        $this->collectionFactory = $collectionFactory;
        $this->historyFactory = $historyFactory;
        $this->helper = $helper;
    }

    protected function massAction(AbstractCollection $collection) {

        $ignored = 0;
        $shipped = 0;
        foreach ($collection->getItems() as $order) {

            if (!$order->canShip()) {
                $ignored++;
                continue;
            }

            // Initialize order shipment
            $convertOrder = $this->_objectManager->create('Magento\Sales\Model\Convert\Order');
            $shipment = $convertOrder->toShipment($order);
            foreach ($order->getAllItems() AS $orderItem) {
                // Check if order item has qty to ship or is virtual
                if (!$orderItem->getQtyToShip() || $orderItem->getIsVirtual()) {
                    continue;
                }

                $qtyShipped = $orderItem->getQtyToShip();

                // Create shipment item with qty
                $shipmentItem = $convertOrder->itemToShipmentItem($orderItem)->setQty($qtyShipped);

                // Add shipment item to shipment
                $shipment->addItem($shipmentItem);
            }

            $shipment->register();
            $shipment->getOrder()->setIsInProcess(true);

            try {
                // Save created shipment and order
                $shipment->save();
                $shipment->getOrder()->save();

                // Send email
                $this->_objectManager->create('Magento\Shipping\Model\ShipmentNotifier')
                        ->notify($shipment);

                $shipment->save();
            } catch (\Exception $e) {
                throw new \Magento\Framework\Exception\LocalizedException(
                __($e->getMessage())
                );
            }
            $shipped++;
        }

        if (!empty($ignored)) {
            $this->messageManager->addError(__('%1 order(s) can not be shipped. (Likely reason; already fully shipped)', $ignored));
        }

        if (!empty($shipped)) {
            $this->messageManager->addSuccess(__('%1 order(s) have been shipped.', $shipped));
        }

        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('sales/order/index'/* $this->getComponentRefererUrl() */);
        return $resultRedirect;
    }

}
