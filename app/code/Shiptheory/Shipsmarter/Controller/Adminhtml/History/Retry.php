<?php

/**
 * @author Mad Capsule Media
 * @package Shiptheory_Shipsmarter
 */

namespace Shiptheory\Shipsmarter\Controller\Adminhtml\History;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use \Shiptheory\Shipsmarter\Helper\Data;

class Retry extends \Magento\Sales\Controller\Adminhtml\Order\AbstractMassAction {

    /**
     * constructor
     */
    public function __construct(
    \Shiptheory\Shipsmarter\Model\HistoryFactory $historyFactory, \Magento\Ui\Component\MassAction\Filter $filter, \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $collectionFactory, \Magento\Backend\App\Action\Context $context, Data $helper
    ) {
        parent::__construct($context, $filter);
        $this->collectionFactory = $collectionFactory;
        $this->historyFactory = $historyFactory;
        $this->helper = $helper;
    }

    /*
     * NOT IMPLEMENTED
     */

    protected function massAction(AbstractCollection $collection) {

        /* NOT IMPLEMENTED */
        /*
        
        foreach ($collection->getItems() as $order) {
            //$order->getId();
            //$order->getOrderId();
            //$order->getIncrementId();
        }

        $result = $this->helper->getShiptheory([
            'shipment_id' => '000000011'
        ]);


        $history = $this->historyFactory->create();
        $history->load(1); //658
        $history->setAttempts(6);
        $history->save();
        */
        
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('sales/order/index'/* $this->getComponentRefererUrl() */);
        return $resultRedirect;
    }

}
