<?php

/**
 * @author Mad Capsule Media
 * @package Shiptheory_Shipsmarter
 */

namespace Shiptheory\Shipsmarter\Controller\Adminhtml\History;

class Index extends \Magento\Backend\App\Action {

    protected $resultPageFactory = false;
    protected $resultPage;

    public function __construct(
    \Magento\Backend\App\Action\Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    public function execute() {
        $this->_setPageData();
        return $this->getResultPage();
    }

    /*
     * Check permission via ACL resource
     */

    protected function _isAllowed() {
        return $this->_authorization->isAllowed('Shiptheory_Shipsmarter::history');
    }

    public function getResultPage() {
        if (is_null($this->resultPage)) {
            $this->resultPage = $this->resultPageFactory->create();
        }
        return $this->resultPage;
    }

    protected function _setPageData() {
        $resultPage = $this->getResultPage();
        $resultPage->setActiveMenu('Shiptheory_Shipsmarter::history');
        $resultPage->getConfig()->getTitle()->prepend((__('Shiptheory History')));
        $resultPage->addBreadcrumb(__('Shiptheory'), __('Shiptheory'));
        return $this;
    }

}
