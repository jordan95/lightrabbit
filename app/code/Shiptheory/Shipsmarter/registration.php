<?php

/**
 * @author Mad Capsule Media
 * @package Shiptheory_Shipsmarter
 */
\Magento\Framework\Component\ComponentRegistrar::register(
        \Magento\Framework\Component\ComponentRegistrar::MODULE, 'Shiptheory_Shipsmarter', __DIR__
);
