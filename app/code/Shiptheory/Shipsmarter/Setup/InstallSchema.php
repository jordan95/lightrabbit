<?php

/**
 * @author Mad Capsule Media
 * @package Shiptheory_Shipsmarter
 */

namespace Shiptheory\Shipsmarter\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface {

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        $installer = $setup;

        $installer->startSetup();
        $table = $installer->getConnection()
            ->newTable($installer->getTable('shiptheory_messages'))
            ->addColumn(
                    'id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'ID'
            )->addColumn(
            'shipment_id', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, null, ['default' => null, 'nullable' => false], 'Shipment ID'
            )->addColumn(
                    'order_id', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, null, ['default' => null, 'nullable' => false], 'Order ID'
            )->addColumn(
                    'attempts', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['default' => 1, 'nullable' => true], 'Attempts'
            )->addColumn(
                    'created_at', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null, ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT], 'Created At'
            )->addColumn(
                    'updated_at', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null, ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE], 'Updated At'
            )->addColumn(
                    'message', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, null, ['default' => '', 'nullable' => true], 'Message'
            )->addColumn(
            'status', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['default' => 1, 'nullable' => false], 'Status'
        );
        $installer->getConnection()->createTable($table);
        $installer->endSetup();
    }

}
