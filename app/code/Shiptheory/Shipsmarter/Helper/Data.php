<?php

/**
 * @author Mad Capsule Media
 * @package Shiptheory_Shipsmarter
 */

namespace Shiptheory\Shipsmarter\Helper;

use Magento\Framework\HTTP\ZendClientFactory;
use Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper {

    public function __construct(\Magento\Framework\App\Helper\Context $context, ZendClientFactory $httpClientFactory, \Magento\Store\Model\StoreManagerInterface $storeManager) {
        $this->storeManager = $storeManager;
        $this->httpClientFactory = $httpClientFactory;
        parent::__construct($context);
    }

    public function getConfig($config_path) {
        return $this->scopeConfig->getValue(
                        $config_path, \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function endpoint() {
        return 'http://magento-quark.madcapsule.com/shiptheory.py';
    }

    private function getStoreUrl() {
        return parse_url($this->storeManager->getStore()->getBaseUrl(), PHP_URL_HOST);
    }

    /**
     * GET Shiptheory
     *
     * @param array
     * */
    public function getShiptheory($data) {

        //$extension_version = (string) Mage::helper('shippinglabels')->getExtensionVersion();
        // $endpoint = Mage::getStoreConfig('shippinglabels/endpoint/order');

        $ident = $this->getConfig('shiptheory_shipsmarter/general/api_key');
        $secret = $this->getConfig('shiptheory_shipsmarter/general/api_secret');

        if (empty($ident) || empty($secret)) {
            return array('error' => 'Shiptheory API Key and/Secret is missing. See Config page');
        }

        $client = $this->httpClientFactory->create();
        $client->setUri($this->endpoint());
        $client->setConfig(['maxredirects' => 2, 'timeout' => 30]);
        $client->setParameterGet(array(
            'channel' => substr($ident, 0, -4) . "/Magento2/" . $this->getStoreUrl() . "/" . $data['shipment_id'] . "/shipment/created",
            'sec' => $secret
        ));

        $client->setMethod(\Zend_Http_Client::GET);
        $client->setHeaders('Accept', 'application/json');
        $client->setHeaders('User-Agent', 'Shiptheory-Magento2');
        $client->setHeaders('Shiptheory-Ident', $ident);
        $client->setHeaders('Shiptheory-Version', '0.0.2');

        try {
            $response = $client->request();
            //$responseBody = $response->getBody();
        } catch (\Exception $e) {
            return array('error' => $e->getMessage());
        }
    }

}
