<?php

/**
 * @author Mad Capsule Media
 * @package Shiptheory_Shipsmarter
 */

namespace Shiptheory\Shipsmarter\Observer;

use Magento\Framework\Event\ObserverInterface;
use \Shiptheory\Shipsmarter\Helper\Data;

class Shipment implements ObserverInterface {

    public function __construct(
    \Shiptheory\Shipsmarter\Model\HistoryFactory $historyFactory,
    #\Shiptheory\Shipsmarter\Logger\Logger $logger,
    \Magento\Sales\Api\Data\OrderInterface $order, \Magento\Framework\Registry $registry, Data $helper) {
        $this->historyFactory = $historyFactory;
        //$this->logger = $logger;
        $this->order = $order;
        $this->helper = $helper;
        $this->registry = $registry;
    }

    public function execute(\Magento\Framework\Event\Observer $observer) {

        if (empty($this->helper->getConfig('shiptheory_shipsmarter/general/enabled'))) {
            return;
        }

        //catch event firing twice
        if ($this->registry->registry('shiptheory_event_shipment_id') == $observer->getEvent()->getShipment()->getIncrementId()) {
            return;
        } else {
            $this->registry->unregister('shiptheory_event_shipment_id');
            $this->registry->register('shiptheory_event_shipment_id', $observer->getEvent()->getShipment()->getIncrementId());
        }

        $order = $this->order->load($observer->getEvent()->getShipment()->getOrderId());
        $model = $this->historyFactory->create();
        $model->setShipmentId($observer->getEvent()->getShipment()->getIncrementId());
        $model->setOrderId($order->getIncrementId());
        $model->save();

        //$this->logger->debug('Shiptheory shipment found');

        $result = $this->helper->getShiptheory([
            'shipment_id' => $observer->getEvent()->getShipment()->getIncrementId()
        ]);

        if (empty($model->getId())) {
            return;
        }

        if (!empty($result['error'])) {
            //failed
            $model->setStatus(4);
            $model->setMessage($result['error']);
        } else {
            //success
            $model->setStatus(3);
        }

        $model->save();
    }

}
