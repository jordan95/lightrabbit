<?php

/**
 * @author Mad Capsule Media
 * @package Shiptheory_Shipsmarter
 */

namespace Shiptheory\Shipsmarter\Model;

class History extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface, \Shiptheory\Shipsmarter\Model\Api\Data\HistoryInterface {

    /**
     * Cache tag
     * 
     * @var string
     */
    const CACHE_TAG = 'shiptheory_shipsmarter_history';

    /**
     * Cache tag
     * 
     * @var string
     */
    protected $_cacheTag = 'shiptheory_shipsmarter_history';

    /**
     * Event prefix
     * 
     * @var string
     */
    protected $_eventPrefix = 'shiptheory_shipsmarter_history';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct() {
        $this->_init('Shiptheory\Shipsmarter\Model\ResourceModel\History');
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities() {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * get entity default values
     *
     * @return array
     */
    public function getDefaultValues() {
        $values = [];
        return $values;
    }

}
