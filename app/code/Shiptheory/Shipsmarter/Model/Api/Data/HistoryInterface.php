<?php

/**
 * @author Mad Capsule Media
 * @package Shiptheory_Shipsmarter
 */

namespace Shiptheory\Shipsmarter\Model\Api\Data;

interface HistoryInterface {

    public function getId();

    public function setId($value);

}
