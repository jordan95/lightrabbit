<?php
/**
 *  CART2QUOTE CONFIDENTIAL
 *  __________________
 *
 *    [2009] - [2017] Cart2Quote B.V.
 *    All Rights Reserved.
 *
 *   NOTICE OF LICENSE
 *
 *   All information contained herein is, and remains
 *   the property of Cart2Quote B.V. and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to Cart2Quote B.V.
 *   and its suppliers and may be covered by European and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from Cart2Quote B.V.
 *
 * @category      Cart2Quote
 * @package       Not2Order
 * @copyright     Copyright (c) 2017 Cart2Quote B.V. (https://www.cart2quote.com)
 * @license       https://www.cart2quote.com/ordering-licenses(https://www.cart2quote.com)
 */

namespace Cart2Quote\Not2Order\Block\Magento\Framework\Pricing;

use Magento\Framework\Pricing\Amount\AmountInterface;
use Magento\Framework\Pricing\Price\PriceInterface;
use Magento\Framework\Pricing\SaleableInterface;
use Magento\Framework\Pricing\Render\Layout;
use Magento\Framework\View\Element\Template;
use Cart2Quote\Not2Order\Helper\Data;

/**
 * Class Render
 * @package Cart2Quote\Not2Order\Block\Magento\Framework\Pricing
 */
class Render extends \Magento\Framework\Pricing\Render
{
    /**
     * @var \Cart2Quote\Not2Order\Helper\Data
     */
    private $dataHelper;

    /**
     * Render constructor.
     * @param \Cart2Quote\Not2Order\Helper\Data $dataHelper
     * @param Template\Context $context
     * @param Layout $priceLayout
     * @param array $data
     */
    public function __construct(Data $dataHelper, Template\Context $context, Layout $priceLayout, array $data)
    {
        $this->dataHelper = $dataHelper;
        parent::__construct($context, $priceLayout, $data);
    }

    /**
     * @param string $priceCode
     * @param SaleableInterface $saleableItem
     * @param array $arguments
     * @return string
     */
    public function render($priceCode, SaleableInterface $saleableItem, array $arguments = [])
    {
        $customerGroupId = $this->dataHelper->getCustomerGroup();

        if ($this->dataHelper->showPrice($saleableItem, $customerGroupId)) {
            return parent::render($priceCode, $saleableItem, $arguments);
        } else {
            return '';
        }
    }

    /**
     * @param AmountInterface $amount
     * @param PriceInterface $price
     * @param SaleableInterface|null $saleableItem
     * @param array $arguments
     * @return string
     */
    public function renderAmount(
        AmountInterface $amount,
        PriceInterface $price,
        SaleableInterface $saleableItem = null,
        array $arguments = []
    ) {
        $customerGroupId = $this->dataHelper->getCustomerGroup();

        if ($this->dataHelper->showPrice($saleableItem, $customerGroupId)) {
            return parent::renderAmount($amount, $price, $saleableItem, $arguments);
        } else {
            return '<div class="price-box"></div>';
        }
    }
}
