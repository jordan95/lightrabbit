<?php
/**
 *  CART2QUOTE CONFIDENTIAL
 *  __________________
 *
 *    [2009] - [2017] Cart2Quote B.V.
 *    All Rights Reserved.
 *
 *   NOTICE OF LICENSE
 *
 *   All information contained herein is, and remains
 *   the property of Cart2Quote B.V. and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to Cart2Quote B.V.
 *   and its suppliers and may be covered by European and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from Cart2Quote B.V.
 *
 * @category      Cart2Quote
 * @package       Not2Order
 * @copyright     Copyright (c) 2017 Cart2Quote B.V. (https://www.cart2quote.com)
 * @license       https://www.cart2quote.com/ordering-licenses(https://www.cart2quote.com)
 */

namespace Cart2Quote\Not2Order\Html;

/**
 * Class Parser
 * @package Cart2Quote\Not2Order\Html
 */
class Parser extends \Magento\Framework\Xml\Parser
{
    /**
     * @param string $string
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function loadHtml($string)
    {
        if ($this->errorHandlerIsActive) {
            set_error_handler([$this, 'errorHandler']);
        }

        try {
            @$this->getDom()->loadHtml(mb_convert_encoding($string, 'HTML-ENTITIES', 'UTF-8'), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            restore_error_handler();
            throw new \Magento\Framework\Exception\LocalizedException(
                __($e->getMessage()),
                $e
            );
        }

        if ($this->errorHandlerIsActive) {
            restore_error_handler();
        }

        return $this;
    }

    /**
     * @param $query
     * @return \DOMNodeList
     */
    public function xpath($query)
    {
        $domXpath = new \DOMXPath($this->getDom());
        return $domXpath->query($query);
    }
}
