<?php
/**
 *  CART2QUOTE CONFIDENTIAL
 *  __________________
 *
 *    [2009] - [2017] Cart2Quote B.V.
 *    All Rights Reserved.
 *
 *   NOTICE OF LICENSE
 *
 *   All information contained herein is, and remains
 *   the property of Cart2Quote B.V. and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to Cart2Quote B.V.
 *   and its suppliers and may be covered by European and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from Cart2Quote B.V.
 *
 * @category      Cart2Quote
 * @package       Not2Order
 * @copyright     Copyright (c) 2017 Cart2Quote B.V. (https://www.cart2quote.com)
 * @license       https://www.cart2quote.com/ordering-licenses(https://www.cart2quote.com)
 */

namespace Cart2Quote\Not2Order\CustomerData;

use Magento\Wishlist\Block\Customer\Sidebar;
use Magento\Catalog\Helper\ImageFactory;
use Magento\Framework\App\ViewInterface;
use Magento\Wishlist\Helper\Data as WishData;
use Magento\Wishlist\Model\Item;
use Cart2Quote\Not2Order\Helper\Data;

/**
 * Class Wishlist
 * @package Cart2Quote\Not2Order\CustomerData
 */
class Wishlist extends \Magento\Wishlist\CustomerData\Wishlist
{
    /**
     * @var Data
     */
    private $dataHelper;

    /**
     * Wishlist constructor.
     * @param Data $dataHelper
     * @param WishData $wishlistHelper
     * @param Sidebar $block
     * @param ImageFactory $imageHelperFactory
     * @param ViewInterface $view
     */
    public function __construct(
        Data $dataHelper,
        WishData $wishlistHelper,
        Sidebar $block,
        ImageFactory $imageHelperFactory,
        ViewInterface $view
    ) {
        $this->dataHelper = $dataHelper;
        parent::__construct($wishlistHelper, $block, $imageHelperFactory, $view);
    }

    /**
     * Retrieve wishlist item data
     * Hides add to cart button wishlist sidebar.
     *
     * @param Item $wishlistItem
     * @return array
     */
    protected function getItemData(Item $wishlistItem)
    {
        $product = $wishlistItem->getProduct();
        $customerGroupId = $this->dataHelper->getCustomerGroup();
        return [
            'image' => $this->getImageData($product),
            'product_url' => $this->wishlistHelper->getProductUrl($wishlistItem),
            'product_name' => $product->getName(),
            'product_price' => $this->block->getProductPriceHtml(
                $product,
                'wishlist_configured_price',
                \Magento\Framework\Pricing\Render::ZONE_ITEM_LIST,
                ['item' => $wishlistItem]
            ),
            'product_is_saleable_and_visible' => $product->isSaleable() && $product->isVisibleInSiteVisibility() &&
                $this->dataHelper->hideOrderButton($product, $customerGroupId),
            'product_has_required_options' => $product->getTypeInstance()->hasRequiredOptions($product),
            'add_to_cart_params' => $this->wishlistHelper->getAddToCartParams($wishlistItem, true),
            'delete_item_params' => $this->wishlistHelper->getRemoveParams($wishlistItem, true),
        ];
    }
}
