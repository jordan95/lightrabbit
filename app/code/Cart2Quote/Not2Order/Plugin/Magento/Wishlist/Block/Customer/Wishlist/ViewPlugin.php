<?php
/**
 *  CART2QUOTE CONFIDENTIAL
 *  __________________
 *
 *    [2009] - [2017] Cart2Quote B.V.
 *    All Rights Reserved.
 *
 *   NOTICE OF LICENSE
 *
 *   All information contained herein is, and remains
 *   the property of Cart2Quote B.V. and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to Cart2Quote B.V.
 *   and its suppliers and may be covered by European and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from Cart2Quote B.V.
 *
 * @category      Cart2Quote
 * @package       Not2Order
 * @copyright     Copyright (c) 2017 Cart2Quote B.V. (https://www.cart2quote.com)
 * @license       https://www.cart2quote.com/ordering-licenses(https://www.cart2quote.com)
 */

namespace Cart2Quote\Not2Order\Plugin\Magento\Wishlist\Block\Customer\Wishlist;

use Magento\Wishlist\Block\Customer\Wishlist;
use Cart2Quote\Not2Order\Plugin\RemoveCartPlugin;

/**
 * Class ViewPlugin
 * @package Cart2Quote\Not2Order\Plugin\Magento\Wishlist\Block\Customer\Wishlist
 */
class ViewPlugin extends RemoveCartPlugin
{
    /**
     * Remove add to cart from wishlist.
     *
     * @param Wishlist $subject
     * @param $result
     * @return string
     */
    public function afterToHtml(Wishlist $subject, $result)
    {
        if ($this->dataHelper->isModuleOutputDisabled()) {
            return $result;
        }

        $this->parser->loadHtml($result);

        $xpathToCart = '//div[@class="actions-primary"]/button[@data-role="tocart"]';
        $xpathAllToCart = '//div[@class="primary"]/button[@data-role="all-tocart"]';

        $domNodeListToCart = $this->parser->xpath($xpathToCart);
        $domNodeListAllToCart = $this->parser->xpath($xpathAllToCart);

        $customerGroupId = $this->dataHelper->getCustomerGroup();
        $replace = $this->dataHelper->replaceButtonCheck();
        $replaceButton = $this->dataHelper->getReplaceButton();
        $hideAllButton = false;

        foreach ($domNodeListToCart as $domNodeToCart) {
            $form = $domNodeToCart->parentNode;
            foreach ($form->childNodes as $childNode) {
                if ($childNode->nodeName == 'button') {
                    $value = $childNode->getAttribute('data-item-id');
                    $item = $subject->getWishlistItems()->getItemById($value);
                    $actualProduct = $item->getProduct();
                    $show = $this->dataHelper->hideOrderButton($actualProduct, $customerGroupId);

                    if (!$show && !$replace) {
                        $form->removeChild($domNodeToCart);
                        $hideAllButton = true;
                    } elseif (!$show && $replace) {
                        $fragment = $this->parser->getDom()->createDocumentFragment();
                        $fragment->appendXML($replaceButton);
                        $form->replaceChild($fragment, $domNodeToCart);
                        $hideAllButton = true;
                    }
                }
            }
        }
        foreach ($domNodeListAllToCart as $domNodeAllToCart) {
            if ($domNodeListToCart->length > 0) {
                if ($hideAllButton) {
                    $domNodeAllToCart->parentNode->removeChild($domNodeAllToCart);
                }
            }
        }
        $result = $this->parser->getDom()->saveHTML();

        return $result;
    }
}
