<?php
/**
 *  CART2QUOTE CONFIDENTIAL
 *  __________________
 *
 *    [2009] - [2017] Cart2Quote B.V.
 *    All Rights Reserved.
 *
 *   NOTICE OF LICENSE
 *
 *   All information contained herein is, and remains
 *   the property of Cart2Quote B.V. and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to Cart2Quote B.V.
 *   and its suppliers and may be covered by European and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from Cart2Quote B.V.
 *
 * @category      Cart2Quote
 * @package       Not2Order
 * @copyright     Copyright (c) 2017 Cart2Quote B.V. (https://www.cart2quote.com)
 * @license       https://www.cart2quote.com/ordering-licenses(https://www.cart2quote.com)
 */

namespace Cart2Quote\Not2Order\Plugin\Magento\Checkout\Block\Cart;

use Magento\Checkout\Block\Cart\Sidebar;
use Cart2Quote\Not2Order\Plugin\RemoveCartPlugin;

/**
 * Class SidebarPlugin
 * @package Cart2Quote\Not2Order\Plugin\Magento\Checkout\Block\Cart
 */
class SidebarPlugin extends RemoveCartPlugin
{
    /**
     * Remove Minicart
     *
     * @param Sidebar $subject
     * @param $result
     * @return string
     */
    public function afterToHtml(Sidebar $subject, $result)
    {
        if ($this->dataHelper->isModuleOutputDisabled()) {
            return $result;
        }

        if ($this->dataHelper->hideOrderReferences()) {
            $this->parser->loadHtml('<div>' . $result . '</div>');
            $divClassName = $this->dataHelper->getMiniCartDivClass();
            $aClassName = $this->dataHelper->getMiniCartAClass();
            $xpath = sprintf('//div[@class="%s"]/a[@class="%s"]', $divClassName, $aClassName);

            $domNodeList = $this->parser->xpath($xpath);
            if ($domNodeList->length > 0) {
                foreach ($domNodeList as $domNode) {
                    $domNode->parentNode->removeChild($domNode);
                }
            }
            $result = $this->parser->getDom()->saveHTML();
        }

        return $result;
    }
}
