<?php
/**
 *  CART2QUOTE CONFIDENTIAL
 *  __________________
 *
 *    [2009] - [2017] Cart2Quote B.V.
 *    All Rights Reserved.
 *
 *   NOTICE OF LICENSE
 *
 *   All information contained herein is, and remains
 *   the property of Cart2Quote B.V. and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to Cart2Quote B.V.
 *   and its suppliers and may be covered by European and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from Cart2Quote B.V.
 *
 * @category      Cart2Quote
 * @package       Not2Order
 * @copyright     Copyright (c) 2017 Cart2Quote B.V. (https://www.cart2quote.com)
 * @license       https://www.cart2quote.com/ordering-licenses(https://www.cart2quote.com)
 */

namespace Cart2Quote\Not2Order\Plugin\Magento\CatalogWidget\Block\Product;

use Magento\Catalog\Model\ProductRepository;
use Magento\CatalogWidget\Block\Product\ProductsList;
use Cart2Quote\Not2Order\Plugin\RemoveCartPlugin;
use Cart2Quote\Not2Order\Helper\Data;
use Cart2Quote\Not2Order\Html\Parser;

/**
 * Class ProductListPlugin
 * @package Cart2Quote\Not2Order\Plugin\Magento\CatalogWidget\Block\Product
 */
class ProductListPlugin extends RemoveCartPlugin
{
    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    private $productRepository;

    /**
     * ProductListPlugin constructor.
     * @param Parser $parser
     * @param Data $dataHelper
     * @param ProductRepository $productRepository
     */
    public function __construct(Parser $parser, Data $dataHelper, ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
        parent::__construct($parser, $dataHelper);
    }

    /**
     * Remove add to cart from homepage.
     *
     * @param ProductsList $result
     * @return ProductsList|string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function afterToHtml(ProductsList $subject, $result)
    {
        if ($this->dataHelper->isModuleOutputDisabled()) {
            return $result;
        }

        $this->parser->loadHtml($result);
        $buttonClass = $this->dataHelper->getAddToCartClass();
        $xpath = sprintf('//button[@class="%s"]', $buttonClass);
        $domNodeList = $this->parser->xpath($xpath);

        $customerGroupId = $this->dataHelper->getCustomerGroup();
        $replace = $this->dataHelper->replaceButtonCheck();
        $replaceButton = $this->dataHelper->getReplaceButton();

        foreach ($domNodeList as $domNode) {
            $productString = $domNode->getAttribute('data-post');
            if ($productString) {
                $productData = json_decode($productString, true);
                $productId = $productData["data"]["product"];
                $product = $this->productRepository->getById($productId);
                $showCartButton = $this->dataHelper->hideOrderButton($product, $customerGroupId);

                if (!$showCartButton && !$replace) {
                    $domNode->parentNode->removeChild($domNode);
                } elseif (!$showCartButton && $replace) {
                    $fragment = $this->parser->getDom()->createDocumentFragment();
                    $fragment->appendXML($replaceButton);
                    $domNode->parentNode->replaceChild($fragment, $domNode);
                }
            }
        }
        $result = $this->parser->getDom()->saveHTML();

        return $result;
    }
}
