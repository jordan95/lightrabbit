<?php
/**
 *  CART2QUOTE CONFIDENTIAL
 *  __________________
 *
 *    [2009] - [2017] Cart2Quote B.V.
 *    All Rights Reserved.
 *
 *   NOTICE OF LICENSE
 *
 *   All information contained herein is, and remains
 *   the property of Cart2Quote B.V. and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to Cart2Quote B.V.
 *   and its suppliers and may be covered by European and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from Cart2Quote B.V.
 *
 * @category      Cart2Quote
 * @package       Not2Order
 * @copyright     Copyright (c) 2017 Cart2Quote B.V. (https://www.cart2quote.com)
 * @license       https://www.cart2quote.com/ordering-licenses(https://www.cart2quote.com)
 */

namespace Cart2Quote\Not2Order\Plugin\Magento\Catalog\Block\Product\Compare;

use Magento\Catalog\Helper\Product\Compare;
use Magento\Catalog\Block\Product\Compare\ListCompare;
use Cart2Quote\Not2Order\Plugin\RemoveCartPlugin;
use Cart2Quote\Not2Order\Helper\Data;
use Cart2Quote\Not2Order\Html\Parser;

/**
 * Class ListComparePlugin
 * @package Cart2Quote\Not2Order\Plugin\Magento\Catalog\Block\Product\Compare
 */
class ListComparePlugin extends RemoveCartPlugin
{
    /**
     * @var \Magento\Catalog\Helper\Product\Compare
     */
    private $compareHelper;

    /**
     * ListComparePlugin constructor.
     * @param Parser $parser
     * @param Data $dataHelper
     * @param Compare $compareHelper
     */
    public function __construct(Parser $parser, Data $dataHelper, Compare $compareHelper)
    {
        $this->compareHelper = $compareHelper;
        parent::__construct($parser, $dataHelper);
    }

    /**
     * Remove add to cart button from compare list.
     *
     * @param ListCompare $subject
     * @param $result
     * @return string
     */
    public function afterToHtml(ListCompare $subject, $result)
    {
        if ($this->dataHelper->isModuleOutputDisabled()) {
            return $result;
        }

        $this->parser->loadHtml('<div>' . $result . '</div>');
        $customerGroupId = $this->dataHelper->getCustomerGroup();
        $buttonClass = $this->dataHelper->getAddToCartClass();
        $replace = $this->dataHelper->replaceButtonCheck();
        $replaceButton = $this->dataHelper->getReplaceButton();

        foreach ($subject->getItems() as $item) {
            $addCartUrl = $this->compareHelper->getAddToCartUrl($item);
            $show = $this->dataHelper->hideOrderButton($item, $customerGroupId);

            $xpath = sprintf(
                '//form[@data-role="tocart-form"][@action="%s"]/button[@class="%s"]',
                $addCartUrl,
                $buttonClass
            );

            $domNodeList = $this->parser->xpath($xpath);

            foreach ($domNodeList as $domNode) {
                if (!$show && !$replace) {
                    $domNode->parentNode->removeChild($domNode);
                } elseif (!$show && $replace) {
                    $fragment = $this->parser->getDom()->createDocumentFragment();
                    $fragment->appendXML($replaceButton);
                    $domNode->parentNode->replaceChild($fragment, $domNode);
                }
            }
        }
        $result = $this->parser->getDom()->saveHTML();

        return $result;
    }
}
