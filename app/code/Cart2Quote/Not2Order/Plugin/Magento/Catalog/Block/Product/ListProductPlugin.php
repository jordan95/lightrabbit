<?php
/**
 *  CART2QUOTE CONFIDENTIAL
 *  __________________
 *
 *    [2009] - [2017] Cart2Quote B.V.
 *    All Rights Reserved.
 *
 *   NOTICE OF LICENSE
 *
 *   All information contained herein is, and remains
 *   the property of Cart2Quote B.V. and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to Cart2Quote B.V.
 *   and its suppliers and may be covered by European and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from Cart2Quote B.V.
 *
 * @category      Cart2Quote
 * @package       Not2Order
 * @copyright     Copyright (c) 2017 Cart2Quote B.V. (https://www.cart2quote.com)
 * @license       https://www.cart2quote.com/ordering-licenses(https://www.cart2quote.com)
 */

namespace Cart2Quote\Not2Order\Plugin\Magento\Catalog\Block\Product;

use Magento\Catalog\Block\Product\ListProduct;
use Cart2Quote\Not2Order\Plugin\RemoveCartPlugin;

/**
 * Class ListProductPlugin
 * @package Cart2Quote\Not2Order\Plugin\Magento\Catalog\Block\Product
 */
class ListProductPlugin extends RemoveCartPlugin
{
    /**
     * Remove add to cart from list/grid view.
     *
     * @param \Magento\Catalog\Block\Product\ListProduct $subject
     * @param $result
     * @return string
     */
    public function afterToHtml(ListProduct $subject, $result)
    {
        if ($this->dataHelper->isModuleOutputDisabled()) {
            return $result;
        }

        $this->parser->loadHtml('<div>' . $result . '</div>');
        $buttonClass = $this->dataHelper->getAddToCartClass();
        $xpath = sprintf(
            '//form[@data-role="tocart-form"]/input[@type="hidden"][@name="product"]/parent::form/button[@class="%s"]',
            $buttonClass
        );
        $domNodeList = $this->parser->xpath($xpath);

        $customerGroupId = $this->dataHelper->getCustomerGroup();
        $replace = $this->dataHelper->replaceButtonCheck();
        $replaceButton = $this->dataHelper->getReplaceButton();
        foreach ($domNodeList as $domNode) {
            $form = $domNode->parentNode;

            foreach ($form->childNodes as $childNode) {
                if ($childNode->nodeName == 'input' && $childNode->getAttribute('type') == 'hidden' &&
                    $childNode->getAttribute('name') == 'product') {
                    $value = $childNode->getAttribute('value');
                    $actualProduct = $subject->getLoadedProductCollection()->getItemById($value);
                    $show = $this->dataHelper->hideOrderButton($actualProduct, $customerGroupId);

                    if (!$show && !$replace) {
                        $domNode->parentNode->removeChild($domNode);
                    } elseif (!$show && $replace) {
                        $fragment = $this->parser->getDom()->createDocumentFragment();
                        $fragment->appendXML($replaceButton);
                        $domNode->parentNode->replaceChild($fragment, $domNode);
                    }
                }
            }
        }
        $result = $this->parser->getDom()->saveHTML();

        return $result;
    }
}
