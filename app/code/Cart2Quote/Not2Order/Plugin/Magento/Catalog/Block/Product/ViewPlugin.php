<?php
/**
 *  CART2QUOTE CONFIDENTIAL
 *  __________________
 *
 *    [2009] - [2017] Cart2Quote B.V.
 *    All Rights Reserved.
 *
 *   NOTICE OF LICENSE
 *
 *   All information contained herein is, and remains
 *   the property of Cart2Quote B.V. and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to Cart2Quote B.V.
 *   and its suppliers and may be covered by European and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictlycategory and  forbidden unless prior written permission is obtained
 *   from Cart2Quote B.V.
 *
 * @category      Cart2Quote
 * @package       Not2Order
 * @copyright     Copyright (c) 2017 Cart2Quote B.V. (https://www.cart2quote.com)
 * @license       https://www.cart2quote.com/ordering-licenses(https://www.cart2quote.com)
 */

namespace Cart2Quote\Not2Order\Plugin\Magento\Catalog\Block\Product;

use Magento\Catalog\Block\Product\View;
use Magento\Framework\App\ObjectManager;
use Cart2Quote\Not2Order\Plugin\RemoveCartPlugin;

/**
 * Class ViewPlugin
 * @package Cart2Quote\Not2Order\Plugin\Magento\Catalog\Block\Product
 */
class ViewPlugin extends RemoveCartPlugin
{
    /**
     * Remove add to cart from product view.
     *
     * @param View $subject
     * @param $result
     * @return string
     */
    public function afterToHtml(View $subject, $result)
    {
        if ($this->dataHelper->isModuleOutputDisabled()) {
            return $result;
        }

        $this->parser->loadHtml($result);
        $buttonId = $this->dataHelper->getAddToCartId();
        $xpath = sprintf('//button[@id="%s"]', $buttonId);

        $domNodeList = $this->parser->xpath($xpath);
        if ($domNodeList->length > 0) {
            $customerGroupId = $this->dataHelper->getCustomerGroup();
            $showCartButton = $this->dataHelper->hideOrderButton($subject->getProduct(), $customerGroupId);
            $replace = $this->dataHelper->replaceButtonCheck();
            $replaceButton = $this->dataHelper->getReplaceButton();

            foreach ($domNodeList as $domNode) {
                if (!$showCartButton && !$replace) {
                    $domNode->parentNode->removeChild($domNode);
                } elseif (!$showCartButton && $replace) {
                    $fragment = $this->parser->getDom()->createDocumentFragment();
                    $fragment->appendXML($replaceButton);
                    $domNode->parentNode->replaceChild($fragment, $domNode);
                }
            }
            $result = $this->parser->getDom()->saveHTML();
        }

        return $result;
    }
}
