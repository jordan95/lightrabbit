<?php
/**
 *  CART2QUOTE CONFIDENTIAL
 *  __________________
 *
 *    [2009] - [2017] Cart2Quote B.V.
 *    All Rights Reserved.
 *
 *   NOTICE OF LICENSE
 *
 *   All information contained herein is, and remains
 *   the property of Cart2Quote B.V. and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to Cart2Quote B.V.
 *   and its suppliers and may be covered by European and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from Cart2Quote B.V.
 *
 * @category      Cart2Quote
 * @package       Not2Order
 * @copyright     Copyright (c) 2017 Cart2Quote B.V. (https://www.cart2quote.com)
 * @license       https://www.cart2quote.com/ordering-licenses(https://www.cart2quote.com)
 */

namespace Cart2Quote\Not2Order\Plugin\Magento\AdvancedCheckout\Model;

use Cart2Quote\Not2Order\Helper\Data;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\AdvancedCheckout\Model\Cart as AdvancedCheckoutCart;
use Magento\AdvancedCheckout\Helper\Data as AdvancedCheckoutData;

/**
 * Class Cart
 * @package Cart2Quote\Not2Order\Plugin\Magento\AdvancedCheckout\Model
 */
class Cart
{
    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var Data
     */
    protected $dataHelper;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Cart constructor.
     * @param ProductRepositoryInterface $productRepository
     * @param Data $dataHelper
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ProductRepositoryInterface $productRepository,
        Data $dataHelper,
        StoreManagerInterface $storeManager
    ) {
        $this->dataHelper = $dataHelper;
        $this->productRepository = $productRepository;
        $this->storeManager = $storeManager;
    }

    /**
     * Check if product can be added to the cart from Order by Sku
     *
     * @param AdvancedCheckoutCart $subject
     * @param array $result
     * @return array
     */
    public function afterCheckItem(AdvancedCheckoutCart $subject, $result)
    {
        if (isset($result['id'])) {
            $customerGroupId = $this->dataHelper->getCustomerGroup();
            $storeId = $this->storeManager->getStore()->getId();
            $productId = $result['id'];
            $product = $this->productRepository->getById($productId, false, $storeId);
            if ($product->getId()) {
                $allowed = $this->dataHelper->hideOrderButton($product, $customerGroupId);
                if (!$allowed) {
                    $result['code'] = AdvancedCheckoutData::ADD_ITEM_STATUS_FAILED_PERMISSIONS;
                }
            }
        }

        return $result;
    }
}