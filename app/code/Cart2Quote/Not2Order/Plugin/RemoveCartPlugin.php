<?php
/**
 *  CART2QUOTE CONFIDENTIAL
 *  __________________
 *
 *    [2009] - [2017] Cart2Quote B.V.
 *    All Rights Reserved.
 *
 *   NOTICE OF LICENSE
 *
 *   All information contained herein is, and remains
 *   the property of Cart2Quote B.V. and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to Cart2Quote B.V.
 *   and its suppliers and may be covered by European and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from Cart2Quote B.V.
 *
 * @category      Cart2Quote
 * @package       Not2Order
 * @copyright     Copyright (c) 2017 Cart2Quote B.V. (https://www.cart2quote.com)
 * @license       https://www.cart2quote.com/ordering-licenses(https://www.cart2quote.com)
 */

namespace Cart2Quote\Not2Order\Plugin;

use Cart2Quote\Not2Order\Helper\Data;
use Cart2Quote\Not2Order\Html\Parser;

/**
 * Class RemoveCartPlugin
 * @package Cart2Quote\Not2Order\Plugin
 */
class RemoveCartPlugin
{
    /**
     * @var Data
     */
    protected $dataHelper;
    /**
     * @var Parser
     */
    protected $parser;

    /**
     * RemoveCartPlugin constructor.
     * @param Parser $parser
     * @param Data $dataHelper
     */
    public function __construct(Parser $parser, Data $dataHelper)
    {
        $this->dataHelper = $dataHelper;
        $this->parser = $parser;
    }
}
