<?php
/**
 *  CART2QUOTE CONFIDENTIAL
 *  __________________
 *
 *    [2009] - [2017] Cart2Quote B.V.
 *    All Rights Reserved.
 *
 *   NOTICE OF LICENSE
 *
 *   All information contained herein is, and remains
 *   the property of Cart2Quote B.V. and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to Cart2Quote B.V.
 *   and its suppliers and may be covered by European and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from Cart2Quote B.V.
 *
 * @category      Cart2Quote
 * @package       Not2Order
 * @copyright     Copyright (c) 2017 Cart2Quote B.V. (https://www.cart2quote.com)
 * @license       https://www.cart2quote.com/ordering-licenses(https://www.cart2quote.com)
 */

namespace Cart2Quote\Not2Order\Plugin\Cart2Quote\Quotation\CustomerData;

use Cart2Quote\Not2Order\Plugin\RemoveCartPlugin;

/**
 * Class Quote
 * @package Cart2Quote\Not2Order\Plugin\Cart2Quote\Quotation\CustomerData
 */
class Quote extends RemoveCartPlugin
{
    /**
     * Remove the price and price column from the quote and order page
     *
     * @param $subject
     * @param $result
     * @return string
     */
    public function afterGetSectionData($subject, $result)
    {
        if ($this->dataHelper->isModuleOutputDisabled()) {
            return $result;
        }

        $customerGroupId = $this->dataHelper->getCustomerGroup();
        $items = $result['items'];
        if (is_array($items)) {
            foreach ($items as $id => $item) {
                foreach ($subject->getQuote()->getItems() as $quoteItem) {
                    if ($quoteItem->getItemId() == $item['item_id']) {
                        $product = $quoteItem->getProduct();
                        if (!$this->dataHelper->showPrice($product, $customerGroupId)) {
                            $result['items'][$id]['product_price'] = '';
                        }
                    }
                }
            }
        }

        return $result;
    }
}
