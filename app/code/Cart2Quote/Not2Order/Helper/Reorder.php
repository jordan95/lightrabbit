<?php
/**
 *  CART2QUOTE CONFIDENTIAL
 *  __________________
 *
 *    [2009] - [2017] Cart2Quote B.V.
 *    All Rights Reserved.
 *
 *   NOTICE OF LICENSE
 *
 *   All information contained herein is, and remains
 *   the property of Cart2Quote B.V. and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to Cart2Quote B.V.
 *   and its suppliers and may be covered by European and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from Cart2Quote B.V.
 *
 * @category      Cart2Quote
 * @package       Not2Order
 * @copyright     Copyright (c) 2017 Cart2Quote B.V. (https://www.cart2quote.com)
 * @license       https://www.cart2quote.com/ordering-licenses(https://www.cart2quote.com)
 */

namespace Cart2Quote\Not2Order\Helper;

use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\App\Helper\Context;
use Magento\Customer\Model\Session;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Helper\Reorder as SalesReorder;
use Cart2Quote\Not2Order\Helper\Data;

/**
 * Class Reorder
 * @package Cart2Quote\Not2Order\Helper
 */
class Reorder extends SalesReorder
{
    /**
     * @var Data
     */
    private $dataHelper;

    /**
     * Reorder constructor.
     * @param Data $dataHelper
     * @param Context $context
     * @param Session $customerSession
     * @param OrderRepositoryInterface $orderRepository
     */
    public function __construct(
        Data $dataHelper,
        Context $context,
        Session $customerSession,
        OrderRepositoryInterface $orderRepository
    ) {
        $this->dataHelper = $dataHelper;
        parent::__construct($context, $customerSession, $orderRepository);
    }

    /**
     * Check is it possible to reorder
     *
     * @param int $orderId
     * @return bool
     */
    public function canReorder($orderId)
    {
        $order = $this->orderRepository->get($orderId);
        $customerGroupId = $this->dataHelper->getCustomerGroup();
        $items = $order->getItems();
        $hideReorder = false;

        if (isset($items)) {
            foreach ($items as $item) {
                $product = $item->getProduct();
                if (isset($product)) {
                    $allowedReorder[] = $this->dataHelper->hideOrderButton($product, $customerGroupId);
                }
            }
            $hideReorder = in_array(false, $allowedReorder);
        }
        if (!$this->isAllowed($order->getStore()) || $hideReorder) {
            return false;
        }
        if ($this->customerSession->isLoggedIn()) {
            return $order->canReorder();
        } else {
            return true;
        }
    }
}
