<?php
/**
 *  CART2QUOTE CONFIDENTIAL
 *  __________________
 *
 *    [2009] - [2017] Cart2Quote B.V.
 *    All Rights Reserved.
 *
 *   NOTICE OF LICENSE
 *
 *   All information contained herein is, and remains
 *   the property of Cart2Quote B.V. and its suppliers,
 *   if any.  The intellectual and technical concepts contained
 *   herein are proprietary to Cart2Quote B.V.
 *   and its suppliers and may be covered by European and Foreign Patents,
 *   patents in process, and are protected by trade secret or copyright law.
 *   Dissemination of this information or reproduction of this material
 *   is strictly forbidden unless prior written permission is obtained
 *   from Cart2Quote B.V.
 *
 * @category      Cart2Quote
 * @package       Not2Order
 * @copyright     Copyright (c) 2017 Cart2Quote B.V. (https://www.cart2quote.com)
 * @license       https://www.cart2quote.com/ordering-licenses(https://www.cart2quote.com)
 */

namespace Cart2Quote\Not2Order\Model\Config;

use Magento\Eav\Model\Entity\Attribute\Source\Boolean;

/**
 * Class YesNoUseConfig
 * @package Cart2Quote\Not2Order\Model\Config
 */
class YesNoUseConfig extends Boolean
{
    /**
     * Use Store setting
     *
     */
    const VALUE_USECONFIG = 2;

    /**
     * Retrieve all options array ( rewritten from parent )
     *
     * @return array
     */
    public function getAllOptions()
    {
        if ($this->_options === null) {
            $this->_options = [
                ['label' => __('Yes'), 'value' => self::VALUE_YES],
                ['label' => __('No'), 'value' => self::VALUE_NO],
                ['label' => __('Use config'), 'value' => self::VALUE_USECONFIG],
            ];
        }

        return $this->_options;
    }

    /**
     * Get a text for index option value ( rewritten from parent )
     *
     * @param  string|int $value
     * @return string|bool
     */
    public function getIndexOptionText($value)
    {
        switch ($value) {
            case self::VALUE_YES:
                return __('Yes');
            case self::VALUE_NO:
                return __('No');
            case self::VALUE_USECONFIG:
                return __('Use config');
        }

        return parent::getIndexOptionText($value);
    }
}
