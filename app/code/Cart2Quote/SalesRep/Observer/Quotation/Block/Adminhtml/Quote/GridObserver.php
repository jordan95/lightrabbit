<?php
/**
 *
 * CART2QUOTE CONFIDENTIAL
 * __________________
 *
 *  [2009] - [2017] Cart2Quote B.V.
 *  All Rights Reserved.
 *
 * NOTICE OF LICENSE
 *
 * All information contained herein is, and remains
 * the property of Cart2Quote B.V. and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Cart2Quote B.V.
 * and its suppliers and may be covered by European and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Cart2Quote B.V.
 *
 * @category    Cart2Quote
 * @package     SalesRep
 * @copyright   Copyright (c) 2017 Cart2Quote B.V. (https://www.cart2quote.com)
 * @license     https://www.cart2quote.com/ordering-licenses(https://www.cart2quote.com)
 */
namespace Cart2Quote\SalesRep\Observer\Quotation\Block\Adminhtml\Quote;

use Cart2Quote\SalesRep\Observer\CollectionAbstract;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class GridObserver
 * @package Cart2Quote\SalesRep\Observer\Quotation\Block\Adminhtml\Quote
 */
class GridObserver extends CollectionAbstract
{
    /**
     * The function that gets executed when the event is observed
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var \Cart2Quote\Quotation\Model\ResourceModel\Quote\Grid\Collection $collection */
        if ($observer->getCollection() instanceof \Cart2Quote\Quotation\Model\ResourceModel\Quote\Grid\Collection) {
            $this->innerJoin($observer->getCollection());
        }
    }

    /**
     * Get the corresponding sales rep type
     *
     * @return string
     */
    public function getTypeId()
    {
        return \Cart2Quote\SalesRep\Model\Type::SALES_REP_TYPE_QUOTATION;
    }
}
