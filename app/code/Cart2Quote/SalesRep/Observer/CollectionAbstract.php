<?php
/**
 *
 * CART2QUOTE CONFIDENTIAL
 * __________________
 *
 *  [2009] - [2017] Cart2Quote B.V.
 *  All Rights Reserved.
 *
 * NOTICE OF LICENSE
 *
 * All information contained herein is, and remains
 * the property of Cart2Quote B.V. and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Cart2Quote B.V.
 * and its suppliers and may be covered by European and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Cart2Quote B.V.
 *
 * @category    Cart2Quote
 * @package     SalesRep
 * @copyright   Copyright (c) 2017 Cart2Quote B.V. (https://www.cart2quote.com)
 * @license     https://www.cart2quote.com/ordering-licenses(https://www.cart2quote.com)
 */
namespace Cart2Quote\SalesRep\Observer;

use Cart2Quote\SalesRep\Observer\CollectionAbstractInterface;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class CollectionAbstract
 * @package Cart2Quote\SalesRep\Observer
 */
abstract class CollectionAbstract implements ObserverInterface, CollectionAbstractInterface
{
    /**
     * Inner join
     *
     * @param \Cart2Quote\Quotation\Model\ResourceModel\Quote\Grid\Collection $collection
     * @return $this
     */
    public function innerJoin(\Cart2Quote\Quotation\Model\ResourceModel\Quote\Grid\Collection $collection)
    {
        $assembledSelect = $collection->getSelect()->assemble();

        if (strpos($assembledSelect, $collection->getTable('salesrep_user')) === false) {
            $collection->getSelect()
                ->joinLeft(
                    $collection->getTable('salesrep_user'),
                    $collection->getTable('salesrep_user') . '.object_id = entity_id AND ' .
                    $collection->getTable('salesrep_user') . '.type_id = "' . $this->getTypeId() . '"',
                    $cols = '*',
                    $schema = null
                );
        }

        return $this;
    }
}
