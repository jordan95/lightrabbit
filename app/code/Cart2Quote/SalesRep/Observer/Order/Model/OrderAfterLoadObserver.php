<?php
/**
 *
 * CART2QUOTE CONFIDENTIAL
 * __________________
 *
 *  [2009] - [2017] Cart2Quote B.V.
 *  All Rights Reserved.
 *
 * NOTICE OF LICENSE
 *
 * All information contained herein is, and remains
 * the property of Cart2Quote B.V. and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Cart2Quote B.V.
 * and its suppliers and may be covered by European and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Cart2Quote B.V.
 *
 * @category    Cart2Quote
 * @package     SalesRep
 * @copyright   Copyright (c) 2017 Cart2Quote B.V. (https://www.cart2quote.com)
 * @license     https://www.cart2quote.com/ordering-licenses(https://www.cart2quote.com)
 */
namespace Cart2Quote\SalesRep\Observer\Order\Model;

use Cart2Quote\SalesRep\Observer\AbstractObserver;
use Cart2Quote\SalesRep\Api\Data\UserInterface;

/**
 * Class OrderAfterLoadObserver
 * @package Cart2Quote\SalesRep\Observer\Order\Model
 */
class OrderAfterLoadObserver extends AbstractObserver
{
    /**
     * Copy the quotation user if exists
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $quoteId = $observer->getOrder()->getQuoteId();
        if (!$quoteId) {
            return;
        }

        $user = $this->userRepository->getMainUserByAssociatedId(
            $quoteId,
            \Cart2Quote\SalesRep\Model\Type::SALES_REP_TYPE_QUOTATION
        );

        $this->copyUser($user, $quoteId);

        parent::execute($observer);
    }

    /**
     * Copy the salesrep to order
     *
     * @param UserInterface $user
     * @param int $quoteId
     * @return void
     */
    private function copyUser(UserInterface $user, $quoteId)
    {
        if ($user->getUserId()) {
            $orderUser = $this->userFactory->create();
            $orderUser->setObjectId($quoteId);
            $orderUser->setTypeId($this->getTypeId());
            $orderUser->setUserId($user->getUserId());
            $orderUser->setIsMain(true);

            $this->userRepository->save($orderUser);
        }
    }

    /**
     * Get the object type
     *
     * @return string
     */
    public function getTypeId()
    {
        return \Cart2Quote\SalesRep\Model\Type::SALES_REP_TYPE_ORDER;
    }

    /**
     * Get the user id
     *
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return int
     */
    public function getUserId(\Magento\Framework\Model\AbstractModel $object)
    {
        return $object->getUserId();
    }

    /**
     * Get the quote id because order id is not always available and it makes it easier to make the link to quotes.
     *
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return int
     */
    public function getObjectId(\Magento\Framework\Model\AbstractModel $object)
    {
        return $object->getQuoteId();
    }
}
