<?php
/**
 *
 * CART2QUOTE CONFIDENTIAL
 * __________________
 *
 *  [2009] - [2017] Cart2Quote B.V.
 *  All Rights Reserved.
 *
 * NOTICE OF LICENSE
 *
 * All information contained herein is, and remains
 * the property of Cart2Quote B.V. and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Cart2Quote B.V.
 * and its suppliers and may be covered by European and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Cart2Quote B.V.
 *
 * @category    Cart2Quote
 * @package     SalesRep
 * @copyright   Copyright (c) 2017 Cart2Quote B.V. (https://www.cart2quote.com)
 * @license     https://www.cart2quote.com/ordering-licenses(https://www.cart2quote.com)
 */
namespace Cart2Quote\SalesRep\Observer;

use Magento\Framework\Event\ObserverInterface;

/**
 * Class AbstractObserver
 * @package Cart2Quote\SalesRep\Observer
 */
abstract class AbstractObserver implements ObserverInterface, ModelAbstractInterface
{
    /**
     * User repository
     *
     * @var \Cart2Quote\SalesRep\Api\UserRepositoryInterface
     */
    protected $userRepository;

    /**
     * User Factory
     *
     * @var \Cart2Quote\SalesRep\Api\Data\UserInterfaceFactory
     */
    protected $userFactory;

    /**
     * TicketObserver constructor.
     * @param \Cart2Quote\SalesRep\Api\UserRepositoryInterface $userRepository
     * @param \Cart2Quote\SalesRep\Api\Data\UserInterfaceFactory $userFactory
     */
    public function __construct(
        \Cart2Quote\SalesRep\Api\UserRepositoryInterface $userRepository,
        \Cart2Quote\SalesRep\Api\Data\UserInterfaceFactory $userFactory
    ) {
        $this->userRepository = $userRepository;
        $this->userFactory = $userFactory;
    }

    /**
     * The function that gets executed when the event is observed
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var \Magento\Framework\Model\AbstractModel $object */
        $object = $observer->getDataObject();
        if (!$object->getUserId()) {
            $user = $this->userRepository->getMainUserByAssociatedId(
                $this->getObjectId($object),
                $this->getTypeId()
            );

            if ($user->getId()) {
                $object->setUserId($user->getUserId());
                $object->setSalesRepId($user->getId());
            } else {
                $object->setUserId(0);
            }

            $this->saveByCustomerSalesRep($object);
        }
    }

    /**
     * Get the object Id
     *
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return int
     */
    public function getObjectId(\Magento\Framework\Model\AbstractModel $object)
    {
        return $object->getId();
    }

    /**
     * Get customer type
     *
     * @return string
     */
    public function getCustomerType()
    {
        return \Cart2Quote\SalesRep\Model\Type::SALES_REP_TYPE_CUSTOMER;
    }

    /**
     * Create new user
     *
     * @param \Magento\Framework\Model\AbstractModel  $object
     * @param int $userId
     * @return \Cart2Quote\SalesRep\Api\Data\UserInterface
     */
    protected function createUser(\Magento\Framework\Model\AbstractModel $object, $userId)
    {
        $user = $this->userFactory->create();
        $user->setIsMain(true)
            ->setObjectId($this->getObjectId($object))
            ->setTypeId($this->getTypeId())
            ->setUserId($userId);

        return $this->userRepository->save($user);
    }

    /**
     * Save the object by the associated customer's sales rep
     *
     * @param $object
     * @return void
     */
    protected function saveByCustomerSalesRep($object)
    {
        if (!$object instanceof \Magento\Customer\Model\Customer && $object->getCustomerId() && !$object->getUserId()) {
            $customerId = $object->getCustomerId();
            $user = $this->userRepository->getMainUserByAssociatedId(
                $customerId,
                $this->getCustomerType()
            );

            if ($user->getUserId()) {
                $user = $this->createUser($object, $user->getUserId());
                $object->setUserId($user->getUserId());
                $object->setSalesRepId($user->getId());
            }
        }
    }

    /**
     * Delete user
     *
     * @param \Magento\Framework\Model\AbstractModel  $object
     * @return $this
     */
    protected function deleteUser(\Magento\Framework\Model\AbstractModel $object)
    {
        $this->userRepository->delete(
            $this->userRepository->getMainUserByAssociatedId(
                $this->getObjectId($object),
                $this->getTypeId()
            )
        );

        return $this;
    }
}
