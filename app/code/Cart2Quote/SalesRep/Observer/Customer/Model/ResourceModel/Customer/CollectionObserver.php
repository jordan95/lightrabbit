<?php
/**
 *
 * CART2QUOTE CONFIDENTIAL
 * __________________
 *
 *  [2009] - [2017] Cart2Quote B.V.
 *  All Rights Reserved.
 *
 * NOTICE OF LICENSE
 *
 * All information contained herein is, and remains
 * the property of Cart2Quote B.V. and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Cart2Quote B.V.
 * and its suppliers and may be covered by European and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Cart2Quote B.V.
 *
 * @category    Cart2Quote
 * @package     SalesRep
 * @copyright   Copyright (c) 2017 Cart2Quote B.V. (https://www.cart2quote.com)
 * @license     https://www.cart2quote.com/ordering-licenses(https://www.cart2quote.com)
 */
namespace Cart2Quote\SalesRep\Observer\Customer\Model\ResourceModel\Customer;

use Magento\Framework\Event\ObserverInterface;

/**
 * Class CollectionObserver
 * @package Cart2Quote\SalesRep\Observer\Customer\Model\ResourceModel\Customer
 */
class CollectionObserver implements ObserverInterface
{
    /**
     * The function that gets executed when the event is observed
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if ($observer->getCollection() instanceof \Magento\Customer\Model\ResourceModel\Customer\Collection) {
            $this->innerJoin($observer->getCollection());
        }
    }

    /**
     * Inner join
     *
     * @param \Magento\Customer\Model\ResourceModel\Customer\Collection $collection
     * @return $this
     */
    public function innerJoin(\Magento\Customer\Model\ResourceModel\Customer\Collection $collection)
    {
        $assembledSelect = $collection->getSelect()->assemble();

        if (strpos($assembledSelect, $collection->getTable('salesrep_user')) === false) {
            $collection->getSelect()
                ->joinLeft(
                    $collection->getTable('salesrep_user'),
                    $collection->getTable('salesrep_user') . '.object_id = `e`.' . $collection->getIdFieldName() .
                    ' AND ' . $collection->getTable('salesrep_user') .
                    '.type_id = "' . \Cart2Quote\SalesRep\Model\Type::SALES_REP_TYPE_CUSTOMER . '"',
                    $cols = '*',
                    $schema = null
                );
        }
        return $this;
    }
}
