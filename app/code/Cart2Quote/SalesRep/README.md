-- LICENSE --

CART2QUOTE CONFIDENTIAL

[2009] - [2017] Cart2Quote B.V.
All Rights Reserved.
https://www.cart2quote.com/ordering-licenses(https://www.cart2quote.com)

NOTICE OF LICENSE

All information contained herein is, and remains
the property of Cart2Quote B.V. and its suppliers,
if any.  The intellectual and technical concepts contained
herein are proprietary to Cart2Quote B.V.
and its suppliers and may be covered by European and Foreign Patents,
patents in process, and are protected by trade secret or copyright law.
Dissemination of this information or reproduction of this material
is strictly forbidden unless prior written permission is obtained
from Cart2Quote B.V.

-- REQUIREMENTS --

PHP: 		5.6 / 5.6 / 7.0.2 / 7.0.6 / 7.1.0

MAGENTO:        2.1.*

-- INSTALLATION --

1. 	Copy the SalesRep folder to your Magento root (Result: app/code/Cart2Quote/SalesRep)

* Enter the following commands in your (SSH) command line:

2. php -f bin/magento module:enable Cart2Quote_SalesRep

3. php -f bin/magento setup:upgrade

4. php -f bin/magento cache:clean

5. php -f bin/magento setup:di:compile

6. php -f bin/magento setup:static-content:deploy

7. php -f bin/magento indexer:reindex


-- CONTACT --

User manual:
https://www.cart2quote.com/documentation/magento-2-cart2quote-user-manual/

Request a support ticket:
https://cart2quote.zendesk.com/hc/en-us/requests/new

Request a customization:
https://www.cart2quote.com/magento-customizations

Request an update or upgrade:
https://www.cart2quote.com/cart2quote-update-upgrade.html
