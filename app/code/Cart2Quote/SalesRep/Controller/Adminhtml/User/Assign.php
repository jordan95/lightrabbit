<?php
/**
 *
 * CART2QUOTE CONFIDENTIAL
 * __________________
 *
 *  [2009] - [2017] Cart2Quote B.V.
 *  All Rights Reserved.
 *
 * NOTICE OF LICENSE
 *
 * All information contained herein is, and remains
 * the property of Cart2Quote B.V. and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Cart2Quote B.V.
 * and its suppliers and may be covered by European and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Cart2Quote B.V.
 *
 * @category    Cart2Quote
 * @package     SalesRep
 * @copyright   Copyright (c) 2017 Cart2Quote B.V. (https://www.cart2quote.com)
 * @license     https://www.cart2quote.com/ordering-licenses(https://www.cart2quote.com)
 */
namespace Cart2Quote\SalesRep\Controller\Adminhtml\User;

use Cart2Quote\SalesRep\Api\Data\UserInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\TestFramework\Inspection\Exception;

/**
 * Class Assign
 * @package Cart2Quote\SalesRep\Controller\Adminhtml\User
 */
class Assign extends \Magento\Backend\App\Action
{
    const SALESREP_ADMIN_USER = 'salesrep_user';

    /**
     * JSON factory
     *
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * User Repository
     *
     * @var \Cart2Quote\SalesRep\Api\UserRepositoryInterface
     */
    protected $userRepository;

    /**
     * User Factory
     *
     * @var \Cart2Quote\SalesRep\Api\Data\UserInterfaceFactory
     */
    protected $userFactory;

    /**
     * Assign constructor.
     * @param \Cart2Quote\SalesRep\Api\UserRepositoryInterface $userRepository
     * @param \Cart2Quote\SalesRep\Api\Data\UserInterfaceFactory $userFactory
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Magento\Backend\App\Action\Context $context
     */
    public function __construct(
        \Cart2Quote\SalesRep\Api\UserRepositoryInterface $userRepository,
        \Cart2Quote\SalesRep\Api\Data\UserInterfaceFactory $userFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Backend\App\Action\Context $context
    ) {
        $this->userFactory = $userFactory;
        $this->userRepository = $userRepository;
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    /**
     * Get admin users drop down
     *
     * @return \Magento\Framework\View\Result\Page
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        /** @var \Magento\Framework\View\Result\Page $resultPage */
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $layout = $resultPage->getLayout();

        try{
            $user = $this->userFactory->create();
            $user->setIsMain(true);
            $user->setObjectId($this->getRequest()->getParam('id'));
            $user->setTypeId($this->getRequest()->getParam('type_id'));
            $user->setUserId($this->getRequest()->getParam('user_id'));

            $salesRep = $this->userRepository->save($user);

            /** @var \Cart2Quote\SalesRep\Block\Adminhtml\SalesRep\User $block */
            $block = $layout->getBlock(self::SALESREP_ADMIN_USER);
            if (!$block instanceof \Cart2Quote\SalesRep\Block\Adminhtml\SalesRep\User) {
                throw new \Exception(
                    __('Block need to be instance of \Cart2Quote\SalesRep\Block\Adminhtml\SalesRep\User')
                );
            }

            $html = $block->setSalesRep($salesRep)->toHtml();
            if ($html) {
                $response = [
                    'html' => $html,
                    'success' => true
                ];
            } else {
                $response = $this->getFailedReturn();
            }
        } catch(\Exception $e) {
            $response = $this->getFailedReturn();
        }

        $resultJson = $this->resultJsonFactory->create();
        return $resultJson->setHttpResponseCode(200)->setData($response);
    }

    /**
     * Get failed return message
     *
     * @return array
     */
    private function getFailedReturn()
    {
        return $response = [
                'html' => __('Error while saving the Sales Representative'),
                'success' => false
            ];
    }
}
