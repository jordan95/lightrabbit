<?php
/**
 *
 * CART2QUOTE CONFIDENTIAL
 * __________________
 *
 *  [2009] - [2017] Cart2Quote B.V.
 *  All Rights Reserved.
 *
 * NOTICE OF LICENSE
 *
 * All information contained herein is, and remains
 * the property of Cart2Quote B.V. and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Cart2Quote B.V.
 * and its suppliers and may be covered by European and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Cart2Quote B.V.
 *
 * @category    Cart2Quote
 * @package     SalesRep
 * @copyright   Copyright (c) 2017 Cart2Quote B.V. (https://www.cart2quote.com)
 * @license     https://www.cart2quote.com/ordering-licenses(https://www.cart2quote.com)
 */
namespace Cart2Quote\SalesRep\Model\Data;

/**
 * Class Type
 * @package Cart2Quote\SalesRep\Model\Data
 */
class Type extends \Magento\Framework\Api\AbstractExtensibleObject implements
    \Cart2Quote\SalesRep\Api\Data\TypeInterface
{

    /**
     * Get type id
     *
     * @api
     * @return int
     */
    public function getTypeId()
    {
        return $this->_get(self::TYPE_ID);
    }

    /**
     * Set type id
     *
     * @param int $typeId
     * @api
     *
     * @return $this
     */
    public function setTypeId($typeId)
    {
        return $this->setData(self::TYPE_ID, $typeId);
    }

    /**
     * Get deleted
     *
     * @api
     * @return bool
     */
    public function getDeleted()
    {
        return $this->_get(self::DELETED);
    }

    /**
     * Set deleted
     *
     * @param bool $deleted
     * @api
     *
     * @return $this
     */
    public function setDeleted($deleted)
    {
        return $this->setData(self::DELETED, $deleted);
    }
    
}
