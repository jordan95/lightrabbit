<?php
/**
 *
 * CART2QUOTE CONFIDENTIAL
 * __________________
 *
 *  [2009] - [2017] Cart2Quote B.V.
 *  All Rights Reserved.
 *
 * NOTICE OF LICENSE
 *
 * All information contained herein is, and remains
 * the property of Cart2Quote B.V. and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Cart2Quote B.V.
 * and its suppliers and may be covered by European and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Cart2Quote B.V.
 *
 * @category    Cart2Quote
 * @package     SalesRep
 * @copyright   Copyright (c) 2017 Cart2Quote B.V. (https://www.cart2quote.com)
 * @license     https://www.cart2quote.com/ordering-licenses(https://www.cart2quote.com)
 */
namespace Cart2Quote\SalesRep\Model\ResourceModel;

use Cart2Quote\SalesRep\Api\Data\TicketSearchResultsInterfaceFactory;
use Cart2Quote\SalesRep\Model\ResourceModel\Ticket;
use Cart2Quote\SalesRep\Model\TicketFactory;
use Cart2Quote\SalesRep\Api\Data\UserInterface;
use Cart2Quote\SalesRep\Api\Data\UserSearchResultsInterfaceFactory;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\InputException;

/**
 * Class UserRepository
 * @package Cart2Quote\SalesRep\Model\ResourceModel
 */
class UserRepository implements \Cart2Quote\SalesRep\Api\UserRepositoryInterface
{
    /**
     * User Factory
     *
     * @var \Cart2Quote\SalesRep\Model\TypeFactory
     */
    protected $userFactory;

    /**
     * User Resource Model
     *
     * @var \Cart2Quote\SalesRep\Model\ResourceModel\User
     */
    protected $userResourceModel;

    /**
     * User Search Result Interface Factory
     *
     * @var UserSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * User Collection
     *
     * @var User\Collection
     */
    protected $userCollectionFactory;

    /**
     * UserRepository constructor.
     *
     * @param \Cart2Quote\SalesRep\Model\TypeFactory $userFactory
     * @param \Cart2Quote\SalesRep\Model\ResourceModel\User $userResourceModel
     * @param UserSearchResultsInterfaceFactory $userSearchResultsInterfaceFactory
     * @param User\Collection $userCollection
     */
    public function __construct(
        \Cart2Quote\SalesRep\Model\TypeFactory $userFactory,
        \Cart2Quote\SalesRep\Model\ResourceModel\User $userResourceModel,
        UserSearchResultsInterfaceFactory $userSearchResultsInterfaceFactory,
        \Cart2Quote\SalesRep\Model\ResourceModel\User\CollectionFactory $userCollectionFactory
    ) {
        $this->userFactory = $userFactory;
        $this->userResourceModel = $userResourceModel;
        $this->searchResultsFactory = $userSearchResultsInterfaceFactory;
        $this->userCollectionFactory = $userCollectionFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function save(UserInterface $user)
    {
        $this->validate($user);

        if ($existingUser = $this->getMainUserByAssociatedId($user->getObjectId(), $user->getTypeId())) {
            $user->setId($existingUser->getId());
        }

        $userModel = $this->userFactory->create();
        $userModel->updateData($user);

        $this->userResourceModel->save($userModel);
        $userModel->afterLoad();
        $user = $userModel->getDataModel();

        return $user;
    }

    /**
     * {@inheritdoc}
     */
    public function getById($userId)
    {
        $userModel = $this->userFactory->create();
        $this->userResourceModel->load($userModel, $userId);
        $userModel->afterLoad();

        return $userModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $collection = $this->userCollectionFactory->create();

        //Add filters from root filter group to the collection
        foreach ($searchCriteria->getFilterGroups() as $group) {
            $this->addFilterGroupToCollection($group, $collection);
        }

        $searchResults->setTotalCount($collection->getSize());
        $sortOrders = $searchCriteria->getSortOrders();
        if ($sortOrders) {
            /** @var SortOrder $sortOrder */
            foreach ($searchCriteria->getSortOrders() as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }

        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());
        $users = [];
        /** @var \Cart2Quote\SalesRep\Model\Ticket $userModel */
        foreach ($collection as $userModel) {
            $users[] = $userModel->getDataModel();
        }

        $searchResults->setItems($users);
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(UserInterface $user)
    {
        return $this->deleteById($user->getId());
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($userId)
    {
        $userModel = $this->userFactory->create();
        $userModel->setId($userId);
        $this->userResourceModel->delete($userModel);
        return true;
    }

    /**
     * Validate quotation user attribute values.
     *
     * @param UserInterface $user
     * @throws InputException
     * @throws \Exception
     * @throws \Zend_Validate_Exception
     *
     * @return void
     */
    protected function validate(UserInterface $user)
    {
        $exception = new InputException();

        if (!\Zend_Validate::is(trim($user->getUserId()), 'NotEmpty')) {
            $exception->addError(__(InputException::requiredField('user_id')));
        }

        if (!\Zend_Validate::is(trim($user->getObjectId()), 'NotEmpty')) {
            $exception->addError(__(InputException::requiredField('quote_id')));
        }

        if ($exception->wasErrorAdded()) {
            throw $exception;
        }
    }

    /**
     * Helper function that adds a FilterGroup to the collection.
     *
     * @param \Magento\Framework\Api\Search\FilterGroup $filterGroup
     * @param \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection $collection
     *
     * @return void
     */
    protected function addFilterGroupToCollection(
        \Magento\Framework\Api\Search\FilterGroup $filterGroup,
        \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection $collection
    ) {
        $fields = [];
        $conditions = [];
        foreach ($filterGroup->getFilters() as $filter) {
            $field = $filter->getField();
            $value = $filter->getValue();
            if (isset($field) && isset($value)) {
                $conditionType = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
                $fields[] = $field;
                $conditions[] = [$conditionType => $value];
            }

        }

        if ($fields) {
            $collection->addFieldToFilter($fields, $conditions);
        }
    }

    /**
     * @param int $id
     * @param string $type
     * @return mixed
     */
    public function getMainUserByAssociatedId($id, $type)
    {
        $userModel = $this->userCollectionFactory->create()
            ->addFieldToFilter('object_id', $id)
            ->addFieldToFilter('type_id', $type)
            ->addFieldToFilter('is_main', 1)
            ->getFirstItem();

        $userModel->afterLoad();

        return $userModel->getDataModel();
    }
}
