<?php
/**
 *
 * CART2QUOTE CONFIDENTIAL
 * __________________
 *
 *  [2009] - [2017] Cart2Quote B.V.
 *  All Rights Reserved.
 *
 * NOTICE OF LICENSE
 *
 * All information contained herein is, and remains
 * the property of Cart2Quote B.V. and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Cart2Quote B.V.
 * and its suppliers and may be covered by European and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Cart2Quote B.V.
 *
 * @category    Cart2Quote
 * @package     SalesRep
 * @copyright   Copyright (c) 2017 Cart2Quote B.V. (https://www.cart2quote.com)
 * @license     https://www.cart2quote.com/ordering-licenses(https://www.cart2quote.com)
 */
namespace Cart2Quote\SalesRep\Model;

/**
 * Class Type
 * @package Cart2Quote\SalesRep\Model
 */
class Type extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Quotation Type
     */
    const SALES_REP_TYPE_QUOTATION = 'quotation';

    /**
     * Ticket Type
     */
    const SALES_REP_TYPE_TICKET = 'ticket';

    /**
     * Order Type
     */
    const SALES_REP_TYPE_ORDER = 'order';

    /**
     * Customer Type
     */
    const SALES_REP_TYPE_CUSTOMER = 'customer';

    /**
     * User Factory
     *
     * @var \Cart2Quote\SalesRep\Model\TypeFactory
     */
    protected $userFactory;

    /**
     * Data Object Helper
     *
     * @var \Magento\Framework\Api\DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * Data Object Processor
     *
     * @var \Magento\Framework\Reflection\DataObjectProcessor
     */
    protected $dataObjectProcessor;

    /**
     * User constructor.
     * @param \Cart2Quote\SalesRep\Api\Data\UserInterfaceFactory $userFactory
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Cart2Quote\SalesRep\Model\ResourceModel\User $resource
     * @param \Cart2Quote\SalesRep\Model\ResourceModel\User\Collection $resourceCollection
     * @param \Magento\Framework\Reflection\DataObjectProcessor $dataObjectProcessor
     * @param \Magento\Framework\Api\DataObjectHelper $dataObjectHelper
     * @param array $data
     */
    public function __construct(
        \Cart2Quote\SalesRep\Api\Data\UserInterfaceFactory $userFactory,
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Cart2Quote\SalesRep\Model\ResourceModel\User $resource,
        \Cart2Quote\SalesRep\Model\ResourceModel\User\Collection $resourceCollection,
        \Magento\Framework\Reflection\DataObjectProcessor $dataObjectProcessor,
        \Magento\Framework\Api\DataObjectHelper $dataObjectHelper,
        array $data = []
    ) {
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->userFactory = $userFactory;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Cart2Quote\SalesRep\Model\ResourceModel\Type');
    }

    /**
     * Retrieve User model with User data
     *
     * @return \Cart2Quote\SalesRep\Api\Data\TicketInterface
     */
    public function getDataModel()
    {
        $data = $this->getData();
        $userDataObject = $this->userFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $userDataObject,
            $data,
            '\Cart2Quote\SalesRep\Api\Data\UserInterface'
        );
        $userDataObject->setId($this->getId());
        return $userDataObject;
    }

    /**
     * Update User data
     *
     * @param \Cart2Quote\SalesRep\Api\Data\UserInterface $user
     * @return $this
     */
    public function updateData(\Cart2Quote\SalesRep\Api\Data\UserInterface $user)
    {
        $userDataAttributes = $this->dataObjectProcessor->buildOutputDataArray(
            $user,
            '\Cart2Quote\SalesRep\Api\Data\UserInterface'
        );

        foreach ($userDataAttributes as $attributeCode => $attributeData) {
            $this->setDataUsingMethod($attributeCode, $attributeData);
        }

        $customAttributes = $user->getCustomAttributes();
        if ($customAttributes !== null) {
            foreach ($customAttributes as $attribute) {
                $this->setDataUsingMethod($attribute->getAttributeCode(), $attribute->getValue());
            }
        }

        return $this;
    }
}
