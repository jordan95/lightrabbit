<?php
/**
 *
 * CART2QUOTE CONFIDENTIAL
 * __________________
 *
 *  [2009] - [2017] Cart2Quote B.V.
 *  All Rights Reserved.
 *
 * NOTICE OF LICENSE
 *
 * All information contained herein is, and remains
 * the property of Cart2Quote B.V. and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Cart2Quote B.V.
 * and its suppliers and may be covered by European and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Cart2Quote B.V.
 *
 * @category    Cart2Quote
 * @package     SalesRep
 * @copyright   Copyright (c) 2017 Cart2Quote B.V. (https://www.cart2quote.com)
 * @license     https://www.cart2quote.com/ordering-licenses(https://www.cart2quote.com)
 */
namespace Cart2Quote\SalesRep\Model\AdminUser;

/**
 * Class Collection
 * @package Cart2Quote\SalesRep\Model\AdminUser
 */
class Collection extends \Magento\User\Model\ResourceModel\User\Collection
{

    /**
     * Flag for add unassigned to option array.
     *
     * @var bool
     */
    protected $addUnassigned = true;

    /**
     * Format collection to option array.
     *
     * @return array
     */
    public function toOptionArray()
    {
        $optionArray = [];

        if ($this->getAddUnassigned()) {
            $optionArray[] = ['value' => 0, 'label' => __('Unassigned')];
        }

        foreach ($this as $user) {
            $optionArray[] = ['value' => $user->getId(), 'label' => $this->formatUser($user)];
        }

        return $optionArray;
    }

    /**
     * Set to true to add the unassigned to the option array.
     *
     * @param bool $value
     * @return $this
     */
    public function setAddUnassigned($value)
    {
        $this->addUnassigned = $value;
        return $this;
    }

    /**
     * Get if unassigned is added to the option array.
     *
     * @return bool
     */
    public function getAddUnassigned()
    {
        return $this->addUnassigned;
    }

    /**
     * To String method for the admin user: Admin name - Admin email
     *
     * @param \Magento\User\Model\User $user
     * @return string
     */
    private function formatUser(\Magento\User\Model\User $user)
    {
        return "{$user->getName()}";
    }

}
