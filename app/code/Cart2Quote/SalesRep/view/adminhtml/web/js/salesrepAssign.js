/**
 *
 * CART2QUOTE CONFIDENTIAL
 * __________________
 *
 *  [2009] - [2017] Cart2Quote B.V.
 *  All Rights Reserved.
 *
 * NOTICE OF LICENSE
 *
 * All information contained herein is, and remains
 * the property of Cart2Quote B.V. and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Cart2Quote B.V.
 * and its suppliers and may be covered by European and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Cart2Quote B.V.
 *
 * @category    Cart2Quote
 * @package     SalesRep
 * @copyright   Copyright (c) 2017 Cart2Quote B.V. (https://www.cart2quote.com)
 * @license     https://www.cart2quote.com/ordering-licenses(https://www.cart2quote.com)
 */

define([
    'underscore',
    "jquery",
    "jquery/ui"
], function (_, $) {
    'use strict';

    $.widget('salesrep.salesrepAssign', {
        options: {
            selectors: {
                details : '.salesrep-details',
                container : '.salesrep-user-container',
                icon : '.salesrep-icon',
                assign : '.salesrep-assign a',
                userList : '.salesrep-user-list',
                assignCancel : '.salesrep-assign-cancel a',
                dropdown: '#salesrep-dropdown'
            },
            url: undefined,
            id: 0,
            typeId: undefined
        },

        _create: function() {
            var self = this;

            self.addAssignEvent();
            self.addCancelEvent();

            $(self.options.selectors.dropdown).change(function(event){
                self.updateElement(
                    $(self.options.selectors.container),
                    self.options.url,
                    event.target.value,
                    self.options.id,
                    self.options.typeId
                );
            });
        },

        showUsersDropDown: function() {
            $(this.options.selectors.details).hide();
            $(this.options.selectors.icon).hide();
            $(this.options.selectors.assign).hide();
            $(this.options.selectors.userList).show();
            $(this.options.selectors.assignCancel).show();
        },

        hideUsersDropDown: function() {
            $(this.options.selectors.details).show();
            $(this.options.selectors.icon).show();
            $(this.options.selectors.assign).show();
            $(this.options.selectors.userList).hide();
            $(this.options.selectors.assignCancel).hide();
        },

        addAssignEvent: function() {
            var self = this;
            $(this.options.selectors.assign).click(function(event){
                event.preventDefault();
                self.showUsersDropDown();
            });
        },

        addCancelEvent: function() {
            var self = this;
            $(this.options.selectors.assignCancel).click(function(event){
                event.preventDefault();
                self.hideUsersDropDown();
            });
        },

        /**
         * Makes ajax request to update an element on the page.
         *
         * @returns {*}
         */
        updateElement: function(e, url, userId, id, typeId) {
            if (url != undefined && userId != undefined && id != 0 && typeId != undefined) {
                var self = this;
                $.ajax({
                    url: url,
                    timeout: 15000,
                    data: {form_key: FORM_KEY, user_id: userId, id: id, type_id: typeId},
                    success: function (resp) {
                        if (resp.html !== "") {
                            $(e).html(resp.html);
                            self.hideUsersDropDown();
                            self.addAssignEvent();
                        }
                    }
                });
            }
        }
    });

    return $.salesrep.salesrepAssign;
});