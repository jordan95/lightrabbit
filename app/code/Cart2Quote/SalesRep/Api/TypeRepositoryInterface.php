<?php
/**
 *
 * CART2QUOTE CONFIDENTIAL
 * __________________
 *
 *  [2009] - [2017] Cart2Quote B.V.
 *  All Rights Reserved.
 *
 * NOTICE OF LICENSE
 *
 * All information contained herein is, and remains
 * the property of Cart2Quote B.V. and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Cart2Quote B.V.
 * and its suppliers and may be covered by European and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Cart2Quote B.V.
 *
 * @category    Cart2Quote
 * @package     SalesRep
 * @copyright   Copyright (c) 2017 Cart2Quote B.V. (https://www.cart2quote.com)
 * @license     https://www.cart2quote.com/ordering-licenses(https://www.cart2quote.com)
 */
namespace Cart2Quote\SalesRep\Api;

/**
 * Interface TypeRepositoryInterface
 * @package Cart2Quote\SalesRep\Api
 */
interface TypeRepositoryInterface
{
    /**
     * Create type.
     *
     * @param \Cart2Quote\SalesRep\Api\Data\TypeInterface $quotationType
     * @api
     *
     * @return \Cart2Quote\SalesRep\Api\Data\TypeInterface
     */
    public function save(\Cart2Quote\SalesRep\Api\Data\TypeInterface $quotationType);

    /**
     * Retrieve type.
     *
     * @param int $quotationTypeId
     * @api
     *
     * @return \Cart2Quote\SalesRep\Api\Data\TypeInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException If customer with the specified ID does not exist.
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($quotationTypeId);

    /**
     * Retrieve types which match a specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @api
     *
     * @return []
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Delete Type.
     *
     * @param \Cart2Quote\SalesRep\Api\Data\TypeInterface $type
     * @api
     *
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Cart2Quote\SalesRep\Api\Data\TypeInterface $type);

    /**
     * Delete an type by ID.
     *
     * @param int $quotationTypeId
     * @api
     *
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($quotationTypeId);
}
