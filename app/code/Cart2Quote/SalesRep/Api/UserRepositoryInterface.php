<?php
/**
 *
 * CART2QUOTE CONFIDENTIAL
 * __________________
 *
 *  [2009] - [2017] Cart2Quote B.V.
 *  All Rights Reserved.
 *
 * NOTICE OF LICENSE
 *
 * All information contained herein is, and remains
 * the property of Cart2Quote B.V. and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Cart2Quote B.V.
 * and its suppliers and may be covered by European and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Cart2Quote B.V.
 *
 * @category    Cart2Quote
 * @package     SalesRep
 * @copyright   Copyright (c) 2017 Cart2Quote B.V. (https://www.cart2quote.com)
 * @license     https://www.cart2quote.com/ordering-licenses(https://www.cart2quote.com)
 */
namespace Cart2Quote\SalesRep\Api;

/**
 * Interface UserRepositoryInterface
 * @package Cart2Quote\SalesRep\Api
 */
interface UserRepositoryInterface
{
    /**
     * Create user.
     *
     * @param \Cart2Quote\SalesRep\Api\Data\UserInterface $quotationUser
     * @api
     *
     * @return \Cart2Quote\SalesRep\Api\Data\UserInterface
     */
    public function save(\Cart2Quote\SalesRep\Api\Data\UserInterface $quotationUser);

    /**
     * Retrieve user.
     *
     *
     * @param int $quotationUserId
     * @api
     *
     * @return \Cart2Quote\SalesRep\Api\Data\UserInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException If customer with the specified ID does not exist.
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($quotationUserId);

    /**
     * Retrieve Main User by Associated ID.
     *
     *
     * @param int $id
     * @param string $type
     * @return Data\UserInterface
     * @api
     */
    public function getMainUserByAssociatedId($id, $type);

    /**
     * Retrieve users which match a specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @api
     *
     * @return []
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Delete User.
     *
     * @param \Cart2Quote\SalesRep\Api\Data\UserInterface $user
     * @api
     *
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Cart2Quote\SalesRep\Api\Data\UserInterface $user);

    /**
     * Delete an user by ID.
     *
     * @param int $quotationUserId
     * @api
     *
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($quotationUserId);
}
