<?php
/**
 *
 * CART2QUOTE CONFIDENTIAL
 * __________________
 *
 *  [2009] - [2017] Cart2Quote B.V.
 *  All Rights Reserved.
 *
 * NOTICE OF LICENSE
 *
 * All information contained herein is, and remains
 * the property of Cart2Quote B.V. and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Cart2Quote B.V.
 * and its suppliers and may be covered by European and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Cart2Quote B.V.
 *
 * @category    Cart2Quote
 * @package     SalesRep
 * @copyright   Copyright (c) 2017 Cart2Quote B.V. (https://www.cart2object.com)
 * @license     https://www.cart2object.com/ordering-licenses(https://www.cart2object.com)
 */
namespace Cart2Quote\SalesRep\Api\Data;

/**
 * Interface TypeInterface
 * @package Cart2Quote\SalesRep\Api\Data
 */
interface TypeInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{
    /**
     * Constants defined for keys of the data array. Identical to the name of the getter in snake case
     */
    const TYPE_ID = 'type_id';

    /**
     * Deleted
     */
    const DELETED = 'deleted';

    /**
     * Get type id
     *
     * @api
     * @return int
     */
    public function getTypeId();

    /**
     * Set type id
     *
     * @param int $typeId
     * @api
     *
     * @return $this
     */
    public function setTypeId($typeId);

    /**
     * Get deleted
     *
     * @api
     * @return boolean
     */
    public function getDeleted();

    /**
     * Set deleted
     *
     * @param boolean $deleted
     * @api
     *
     * @return $this
     */
    public function setDeleted($deleted);
}
