<?php
//register this module as required by Magento
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Cart2Quote_SalesRep',
    __DIR__
);