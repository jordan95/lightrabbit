<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Gallery\Block\StoreCategory;

use Lof\Gallery\Model\Config;

class View extends \Magento\Framework\View\Element\Template
{


    /**
     * @var \Lof\Gallery\Model\Album
     */
    protected $_albumFactory;
    /**
     * @var \Magento\Framework\Registry
     */
    protected $_registry;

    /**
     * @var \Lof\Gallery\Model\Config
     */
    protected $galleryConfig;


    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry                      $registry
     * @param \Lof\Gallery\Model\Album                         $albumFactory
     * @param \Lof\Gallery\Model\Config                        $galleryConfig
     * @param array                                            $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Lof\Gallery\Model\Album $albumFactory,
        \Lof\Gallery\Model\Config $galleryConfig,
        array $data = []
    ) {
        parent::__construct($context);
        $this->_registry     = $registry;
        $this->_albumFactory = $albumFactory;
        $this->galleryConfig = $galleryConfig;

    }//end __construct()


    public function _toHtml()
    {
        $collection = $this->getCollection();
        if($this->getConfig('general_settings/enable') && $this->getConfig('category_page/enable') && ($collection && 0< $collection->getSize())) {
            return parent::_toHtml();
        }

        return;

    }//end _toHtml()


    /**
     * @return string
     */
    public function getAlbumListHtml($options)
    {
        $collection = $this->getCollection();
        if($options['show_image_of_albums']){
            $block      = $this->getLayout()->createBlock('Lof\Gallery\Block\Album\AlbumRelated')->setCollection($collection)->setData($options);
        }else{
            $block      = $this->getLayout()->createBlock('Lof\Gallery\Block\Album\AlbumList')->setCollection($collection)->setData($options);
        }

        return $block->toHtml();

    }//end getAlbumListHtml()


    /**
     * Get product object
     *
     * @return \Magento\Catalog\Model\Product
     */
    public function getCategory()
    {
        return $this->_registry->registry('current_category');

    }//end getCategory()


    protected function _beforeToHtml()
    {
        $itemPerPage = $this->getConfig('category_page/number_item');
        $categoryId  = $this->getCategory()->getId();

        $store      = $this->_storeManager->getStore();
        $collection = $this->_albumFactory
            ->getCollection()
            ->addStoreFilter($store)
            ->addFieldToFilter("is_active", 1)
            ->addFieldToFilter("page_type", ['like' => '%category%'])
            ->addFieldToFilter("store_categories", ['like' => '%"'.$categoryId.'"%']);

        if ($itemPerPage) {
            $collection->setPagesize($itemPerPage);
        }

        $this->setCollection($collection);
        return parent::_beforeToHtml();

    }//end _beforeToHtml()


    /**
     * @param AbstractCollection $collection
     * @return $this
     */
    public function setCollection($collection)
    {
        $this->_albumCollection = $collection;
        return $this;

    }//end setCollection()


    public function getCollection()
    {
        return $this->_albumCollection;

    }//end getCollection()


    public function getConfig($key, $default = '')
    {
        $c = explode("/", $key);
        if (count($c) == 2 && $this->hasData($c[1])) {
            return $this->getData($c[1]);
        }

        if ($this->hasData($key)) {
            return $this->getData($key);
        }

        if ($val = $this->galleryConfig->getConfig($key)) {
            return $val;
        }

        return $default;

    }//end getConfig()


}//end class
