<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Gallery\Block\Widget;

use Lof\Gallery\Model\Config;

class Album extends \Magento\Framework\View\Element\Template implements \Magento\Widget\Block\BlockInterface
{
    protected $_collection;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $_resource;

    /**
     * @var \Magento\Catalog\Block\Product\ReviewRendererInterface
     */
    protected $reviewRenderer;

    /**
     * @var \Lof\Gallery\Model\ResourceModel\Album\CollectionFactory
     */
    protected $albumCollectionFactory;



    /**
     * @var \Lof\Gallery\Model\ResourceModel\Category\CollectionFactory
     */
    protected $categoryCollectionFactory;

    /**
     * @var \Lof\Gallery\Model\Config
     */
    protected $galleryConfig;


    /**
     * @param \Magento\Framework\View\Element\Template\Context            $context
     * @param \Magento\Framework\App\ResourceConnection                   $resource
     * @param \Magento\Catalog\Block\Product\ReviewRendererInterface      $reviewRenderer
     * @param \Lof\Gallery\Model\ResourceModel\Album\CollectionFactory    $albumCollectionFactory
     * @param \Lof\Gallery\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory
     * @param \Lof\Gallery\Model\Config                                   $galleryConfig
     * @param array                                                       $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Catalog\Block\Product\ReviewRendererInterface $reviewRenderer,
        \Lof\Gallery\Model\ResourceModel\Album\CollectionFactory $albumCollectionFactory,
        \Lof\Gallery\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        \Lof\Gallery\Model\Config $galleryConfig,
        \Lof\Gallery\Helper\Data $helper,
        array $data = []
        ) {
        parent::__construct($context);
        $this->_resource                 = $resource;
        $this->reviewRenderer            = $reviewRenderer;
        $this->albumCollectionFactory    = $albumCollectionFactory;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->galleryConfig             = $galleryConfig;
        $this->helper                    = $helper;
    }//end __construct()


    public function setCollection($collection)
    {
        $this->_collection = $collection;
        return $this->_collection;

    }//end setCollection()


    public function getCollection()
    {
        return $this->_collection;

    }//end getCollection()


    public function getSocialnetworks()
    {
        return explode(",", $this->galleryConfig->getConfig('general_settings/socialnetworks'));

    }//end getSocialnetworks()


    /**
     * @return string
     */
    public function getAlbumListHtml($options)
    {
        $collection = $this->getCollection();
        $block      = $this->_layout->createBlock('Lof\Gallery\Block\Album\AlbumList')->setCollection($collection)->setData($options);
        return $block->toHtml();

    }//end getAlbumListHtml()


    /**
     * Get product reviews summary
     *
     * @param  \Magento\Catalog\Model\Product $product
     * @param  bool                           $templateType
     * @param  bool                           $displayIfNoReviews
     * @return string
     */
    public function getReviewsSummaryHtml(
        \Magento\Catalog\Model\Product $product,
        $templateType = false,
        $displayIfNoReviews = false
        ) {
        return $this->reviewRenderer->getReviewsSummaryHtml($product, $templateType, $displayIfNoReviews);

    }//end getReviewsSummaryHtml()


    /**
     * {@inheritdoc}
     *
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function getLofProductPriceHtml(
        \Magento\Catalog\Model\Product $product,
        $priceType = null,
        $renderZone = \Magento\Framework\Pricing\Render::ZONE_ITEM_LIST,
        array $arguments = []
        ) {
        if (!isset($arguments['zone'])) {
            $arguments['zone'] = $renderZone;
        }

        $arguments['price_id']          = isset($arguments['price_id']) ? $arguments['price_id'] : 'old-price-'.$product->getId().'-'.$priceType;
        $arguments['include_container'] = isset($arguments['include_container']) ? $arguments['include_container'] : true;
        $arguments['display_minimal_price'] = isset($arguments['display_minimal_price']) ? $arguments['display_minimal_price'] : true;

            /*
                * @var \Magento\Framework\Pricing\Render $priceRender
        */
            $priceRender = $this->_layout->getBlock('product.price.render.default');

            $price = '';
            if ($priceRender) {
                $price = $priceRender->render(
                    \Magento\Catalog\Pricing\Price\FinalPrice::PRICE_CODE,
                    $product,
                    $arguments
                    );
            }

            return $price;

    }//end getLofProductPriceHtml()


    public function getTabs()
    {
        $tabs = $this->getData('tabs');
        if($tabs) {
            if(base64_decode($tabs, true) == true) {
                $tabs = str_replace(" ", "+", $tabs);
                $tabs = base64_decode($tabs);
                if(base64_decode($tabs, true) == true) {
                    $tabs = base64_decode($tabs);
                }
            }

            $tabs = unserialize($tabs);
            if(isset($tabs['__empty'])) { unset($tabs['__empty']);
        }

        $loops = array();
        $i     = 0;
        foreach ($tabs as $key => $tab) {
            $loops[$i] = $tab;
            $i++;
        }

        return $loops;
        }//end if

        return false;

    }//end getTabs()


    /**
     * Need use as _prepareLayout - but problem in declaring collection from
     * another block (was problem with search result)
     *
     * @return $this
     */
    public function _toHtml()
    {
        $enable = $this->galleryConfig->getConfig('general_settings/enable');
        if (!$enable) {
            return;
        }
        $type = $this->getData('layout_mode');
        switch ($type) {
            case 'owl':
            case 'list':
            case 'grid':
            $this->setTemplate('widget/album_image.phtml');
                break;
            case 'masonry':
                $this->setTemplate('widget/album_masonry.phtml');
                break;
        }

        $collection = $this->albumCollectionFactory->create()
        ->addFieldToFilter("is_active", 1)
        ->setCurPage(1);

        $limit = (int)$this->getData('limit');
        if ($limit) {
            $collection->setPageSize($limit);
        }
        if ($albums = $this->getData('albums')) {
            $collection->addFieldToFilter('album_id', ['in' => explode(",", $albums)]);
        }

        $images = [];
        foreach ($collection as $album) {
            if (is_array($album->getImages())) {
                foreach ($album->getImages() as $image) {
                    $imageLink = $album->getAlbumLink() . '/' . $this->helper->getImageIdentifier($image);
                    if (isset($image['customurl']) && $image['customurl']) {
                        $imageLink = $image['customurl'];
                    }
                    $image['link'] = $imageLink;
                    $images[] = $image;
                }
            }
        }
        usort($images, function($a, $b) {
            return strcmp($a['title'], $b['title']);
        });
        $this->setCollection($collection);
        $this->setImages($images);

        return parent::_toHtml();

    }//end _toHtml()


    public function getCustomColClass($i)
    {
        $type = $this->getData('layout_mode');
        $tabs = $this->getTabs();
        if ($type == 'grid' || $type == 'lookbook-list' || $type == 'lookbook-slider') {
            $lg_column_item = (int) $this->getData('lg_column');
            $md_column_item = (int) $this->getData('md_column');
            $sm_column_item = (int) $this->getData('sm_column');
            $xs_column_item = (int) $this->getData('xs_column');
            $lg_column      = (12 / $lg_column_item);
            $md_column      = (12 / $md_column_item);
            $sm_column      = (12 / $sm_column_item);
            $xs_column      = (12 / $xs_column_item);
        }

        if ($type == 'list') {
            $xs_column = $sm_column = $md_column = $lg_column = 1;
        }

        if(isset($tabs[$i]) && $type == 'grid') {
            $tabs[$i]["large_desktop"] ? $lg_column = (12 / $tabs[$i]["large_desktop"]) : 1;
            $tabs[$i]["desktop"] ? $md_column       = (12 / $tabs[$i]["desktop"]) : 1;
            $tabs[$i]["tablet"] ? $sm_column        = (12 / $tabs[$i]["tablet"]) : 1;
            $tabs[$i]["mobile"] ? $xs_column        = (12 / $tabs[$i]["mobile"]) : 1;
            $class = 'col-lg-'.$lg_column.' col-md-'.$md_column.' col-sm-'.$sm_column.' col-xs-'.$xs_column;
        } else {
            $class = 'col-lg-'.$lg_column.' col-md-'.$md_column.' col-sm-'.$sm_column.' col-xs-'.$xs_column;
        }

        return $class;

    }//end getCustomColClass()

    public function getAlbumId(){


        return $this->getData('albums');
    }

}//end class
