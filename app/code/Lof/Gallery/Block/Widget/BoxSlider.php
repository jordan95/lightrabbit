<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Gallery\Block\Widget;

use Lof\Gallery\Model\Config;

class BoxSlider extends \Magento\Framework\View\Element\Template implements \Magento\Widget\Block\BlockInterface
{
    /**
     * @var \Lof\Gallery\Model\ResourceModel\Album\Collection
     */
    protected $_collection;

    /**
     * @var string
     */
    protected $_template = 'widget/boxslider.phtml';


    /**
     * @param \Magento\Framework\View\Element\Template\Context         $context
     * @param \Magento\Framework\App\ResourceConnection                $resource
     * @param \Lof\Gallery\Model\ResourceModel\Album\CollectionFactory $albumCollectionFactory
     * @param \Lof\Gallery\Model\Config                                $galleryConfig
     * @param array                                                    $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\App\ResourceConnection $resource,
        \Lof\Gallery\Model\ResourceModel\Album\CollectionFactory $albumCollectionFactory,
        \Lof\Gallery\Model\Config $galleryConfig,
        array $data = []
    ) {
        parent::__construct($context);
        $this->_resource = $resource;
        $this->albumCollectionFactory = $albumCollectionFactory;
        $this->galleryConfig          = $galleryConfig;

    }//end __construct()


    public function setCollection($collection)
    {
        $this->_collection = $collection;
        return $this->_collection;

    }//end setCollection()


    public function getCollection()
    {
        return $this->_collection;

    }//end getCollection()


    /**
     * Need use as _prepareLayout - but problem in declaring collection from
     * another block (was problem with search result)
     *
     * @return $this
     */
    public function _toHtml()
    {
        $enable = $this->galleryConfig->getConfig('general_settings/enable');
        if (!$enable) {
            return;
        }

        $categories = $this->getData('categories');
        $categories = explode(",", $categories);

        $collection = $this->albumCollectionFactory->create()
            ->addFieldToFilter("is_active", 1)
            ->setCurPage(1);

        $collection->getSelect()
            ->joinLeft(
                [
                 'lg' => $this->_resource->getTableName(Config::TABLE_GALLERY_ALBUM_CATEGORY),
                ],
                'lg.album_id = main_table.album_id',
                [
                 'album_id' => 'album_id',
                 'position' => 'position',
                ]
            )
            ->group('main_table.album_id');

        $orderby = $this->getData('orderby');
        if ($orderby == 1) {
            $collection->getSelect()->order("main_table.album_id DESC");
        } else if ($orderby == 2) {
            $collection->getSelect()->order("main_table.album_id ASC");
        } else if ($orderby == 3) {
            $collection->getSelect()->order('RAND()');
        }

        if ($this->getData('categories')) {
            $collection->getSelect()->where('lg.entity_id IN (?)', $categories);
        }

        $limit = (int) $this->getData('limit');
        if ($limit) {
            $collection->setPageSize($limit)->setCurPage(1);
        }

        $this->setCollection($collection);

        return parent::_toHtml();

    }//end _toHtml()


}//end class
