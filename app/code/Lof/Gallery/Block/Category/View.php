<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Gallery\Block\Category;

use Lof\Gallery\Model\Config;

class View extends \Magento\Framework\View\Element\Template
{
    protected $_collection;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry;

    /**
     * @var \Lof\Gallery\Model\ResourceModel\Album\CollectionFactory
     */
    protected $albumCollectionFactory;

    /**
     * @var \Lof\Gallery\Model\Config
     */
    protected $galleryConfig;


    /**
     * @param \Magento\Framework\View\Element\Template\Context         $context
     * @param \Magento\Framework\Registry                              $registry
     * @param \Lof\Gallery\Model\ResourceModel\Album\CollectionFactory $albumCollectionFactory
     * @param \Lof\Gallery\Model\Config                                $galleryConfig
     * @param array                                                    $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Lof\Gallery\Model\ResourceModel\Album\CollectionFactory $albumCollectionFactory,
        \Lof\Gallery\Model\Config $galleryConfig,
        array $data = []
    ) {
        parent::__construct($context);
        $this->coreRegistry           = $registry;
        $this->albumCollectionFactory = $albumCollectionFactory;
        $this->galleryConfig          = $galleryConfig;

    }//end __construct()


    public function setCollection($collection)
    {
        $this->_collection = $collection;
        return $this->_collection;

    }//end setCollection()


    public function getCollection()
    {
        return $this->_collection;

    }//end getCollection()


    /**
     * Prepare breadcrumbs
     *
     * @param  \Magento\Cms\Model\Page $brand
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return void
     */
    protected function _addBreadcrumbs()
    {
        $breadcrumbsBlock = $this->getLayout()->getBlock('breadcrumbs');
        $baseUrl          = $this->_storeManager->getStore()->getBaseUrl();
        $route            = $this->getConfig('general_settings/route');
        $page_title       = $this->getConfig('gallery_page/page_title');
        $showBreadcrumbBlock = $this->getConfig('gallery_page/show_breadcrumbblock');

        if ($showBreadcrumbBlock && $breadcrumbsBlock) {
            $breadcrumbsBlock->addCrumb(
                'home',
                [
                 'label' => __('Home'),
                 'title' => __('Go to Home Page'),
                 'link'  => $baseUrl,
                ]
            );

            if ($route) {
                $route = ucfirst(str_replace("-", " ", $route));
                $breadcrumbsBlock->addCrumb(
                    'lofgallery',
                    [
                     'label' => $route,
                     'title' => $route,
                     'link'  => $this->galleryConfig->getGalleryPageUrl(),
                    ]
                );
            }

            $category = $this->getCategory();
            $breadcrumbsBlock->addCrumb(
                'category',
                [
                 'label' => $category->getName(),
                 'title' => $category->getName(),
                 'link'  => '',
                ]
            );
        }//end if

    }//end _addBreadcrumbs()


    public function getConfig($key)
    {
        return $this->galleryConfig->getConfig($key);

    }//end getConfig()


    /**
     * @return string
     */
    public function getAlbumListHtml($layout_type = '', $lg_column = '', $md_column = '', $sm_column = '', $xs_column = '', $lightbox = 'fancybox')
    {
        $collection = $this->getCollection();
        $block      = $this->getLayout()->getBlock('gallery.albums.list');
        if ($layout_type) {
            $block->setData('layout_type', $layout_type);
        }

        if ($lg_column) {
            $block->setData('lg_column', $lg_column);
        }

        if ($md_column) {
            $block->setData('md_column', $md_column);
        }

        if ($sm_column) {
            $block->setData('sm_column', $sm_column);
        }

        if ($xs_column) {
            $block->setData('xs_column', $xs_column);
        }

        if ($lightbox) {
            $block->setData('lightbox', $lightbox);
        }

        $block->setCollection($collection);


        return $block->toHtml();

    }//end getAlbumListHtml()


    /**
     * Need use as _prepareLayout - but problem in declaring collection from
     * another block (was problem with search result)
     *
     * @return $this
     */
    protected function _beforeToHtml()
    {
        $category    = $this->getCategory();
        $itemPerPage = $category->getItemsPerPage();
        $category    = $this->getCategory();
        $store      = $this->_storeManager->getStore();
        $collection  = $category->getAlbums($store);
        if ($itemPerPage) {
            $collection->setPagesize($itemPerPage);
        }

        $this->setCollection($collection);
        $toolbar = $this->getToolbarBlock();
        if ($toolbar && $itemPerPage) {
            $toolbar->setData('_current_limit', $itemPerPage)->setCollection($collection);
            $this->setChild('toolbar', $toolbar);
        }

        return parent::_beforeToHtml();

    }//end _beforeToHtml()


    /**
     * Prepare global layout
     *
     * @return $this
     */
    protected function _prepareLayout()
    {
        $this->_addBreadcrumbs();
        $this->pageConfig->addBodyClass('gallery-category');

        $category  = $this->getCategory();
        $pageTitle = $category->getPageTitle();
        if ($pageTitle == '') {
            $pageTitle = $category->getName();
        }

        if ($pageTitle) {
            $this->pageConfig->getTitle()->set($pageTitle);
        }

        if($metaKeywords = $category->getMetaKeywords()) {
            $this->pageConfig->setKeywords($metaKeywords);
        }

        if($metaDescription = $category->getMetaDescription()) {
            $this->pageConfig->setDescription($metaDescription);
        }

        $pageMainTitle = $this->getLayout()->getBlock('page.main.title');
        if ($pageMainTitle) {
            $pageMainTitle->setPageTitle($category->getName());
        }

        return parent::_prepareLayout();

    }//end _prepareLayout()


    /**
     * Retrieve Toolbar block
     *
     * @return \Magento\Catalog\Block\Product\ProductList\Toolbar
     */
    public function getToolbarBlock()
    {
        $block = $this->getLayout()->getBlock('gallery_toolbar');
        return $block;

    }//end getToolbarBlock()


    public function getCategory()
    {
        $album = $this->coreRegistry->registry('gallery_category');
        return $album;

    }//end getCategory()


}//end class
