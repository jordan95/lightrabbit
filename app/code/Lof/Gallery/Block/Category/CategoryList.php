<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Gallery\Block\Category;

class CategoryList extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Lof\Gallery\Model\Config
     */
    protected $galleryConfig;

    /**
     * @var \Lof\Gallery\Model\ResourceModel\Category\CollectionFactory
     */
    protected $categoryCollectionFactory;

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;


    /**
     * @param \Magento\Framework\View\Element\Template\Context            $context
     * @param \Magento\Framework\Registry                                 $registry
     * @param \Lof\Gallery\Model\Config                                   $galleryConfig
     * @param \Lof\Gallery\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory
     * @param array                                                       $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Lof\Gallery\Model\Config $galleryConfig,
        \Lof\Gallery\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->galleryConfig = $galleryConfig;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->_coreRegistry = $registry;

    }//end __construct()


    public function getConfig($key)
    {
        return $this->galleryConfig->getConfig($key);

    }//end getConfig()


    public function drawItems($collection, $cat, $level = 0)
    {
        foreach ($collection as $_cat) {
            if($_cat->getParentId() == $cat->getId()) {
                $children[] = $this->drawItems($collection, $_cat, ($level + 1));
                $cat->setChildren($children);
            }
        }

        $cat->setSelevel($level);
        return $cat;

    }//end drawItems()


    public function _toHtml()
    {
        if(!$this->getConfig('general_settings/enable')) { return;
        }

        $store      = $this->_storeManager->getStore();
        $collection = $this->categoryCollectionFactory->create()
            ->addFieldToFilter('is_active', 1)
            ->addStoreFilter($store)
            ->setOrder("cat_position", "ASC");
        $cats       = [];
        foreach ($collection as $_cat) {
            if(!$_cat->getParentId()) {
                $cats[] = $this->drawItems($collection, $_cat);
            }
        }

        $this->setCollection($cats);
        return parent::_toHtml();

    }//end _toHtml()


    public function drawItem($collection, $html = '')
    {
        foreach ($collection as $_cat) {
            $children = $_cat->getChildren();
            $class    = '';
            if($children) { $class = "class='level".$_cat->getLevel()."' parent";
            }

            $linkClass = '';
            $category  = $this->_coreRegistry->registry('gallery_category');
            if ($category) {
                if ($category->getCategoryId() == $_cat->getCategoryId()) {
                    $linkClass = 'class="active"';
                }
            }

            $html .= '<li '.$class.' >';
            $html .= '<a href="'.$_cat->getUrl().'" '.$linkClass.'>';
            $html .= $_cat->getName();
            $html .= '</a>';
            if($children) {
                $html .= '<span class="opener"><i class="fa fa-plus-square-o"></i></span>';
                $html .= '<ul class="level'.$_cat->getLevel().' children">';
                $html .= $this->drawItem($children);
                $html .= '</ul>';
            }

            $html .= '</li>';
        }//end foreach

        return $html;

    }//end drawItem()


    public function getCategoryHtml()
    {
        $collection = $this->getCollection();
        $html       = $this->drawItem($collection, '');
        return $html;

    }//end getCategoryHtml()


    public function setCollection($collection)
    {
        $this->_collection = $collection;
        return $this;

    }//end setCollection()


    public function getCollection()
    {
        return $this->_collection;

    }//end getCollection()


}//end class
