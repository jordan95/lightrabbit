<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Gallery\Block\Search;

use Lof\Gallery\Model\Config;

class Form extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Lof\Gallery\Model\Config
     */
    protected $galleryConfig;


    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Lof\Gallery\Model\Config                        $galleryConfig
     * @param array                                            $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Lof\Gallery\Model\Config $galleryConfig,
        array $data = []
    ) {
        parent::__construct($context);
        $this->galleryConfig = $galleryConfig;

    }//end __construct()


    public function getSearchFormUrl()
    {
        $route   = $this->galleryConfig->getConfig('general_settings/route');
        $baseUrl = $this->_storeManager->getStore()->getBaseUrl();
        return $baseUrl.$route.'/search';

    }//end getSearchFormUrl()


}//end class
