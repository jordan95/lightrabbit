<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Gallery\Block\Album;

class AlbumList extends \Magento\Framework\View\Element\Template
{


    protected $_template = 'album/list.phtml';

    /**
     * @var AbstractCollection
     */
    protected $_collection;

    /**
     * @var \Lof\Gallery\Model\Config
     */
    protected $galleryConfig;


    /**
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param \Lof\Gallery\Model\Config              $galleryConfig
     * @param array                                  $data
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Lof\Gallery\Model\Config $galleryConfig,
        array $data = []
    ) {
        parent::__construct($context);
        $this->galleryConfig = $galleryConfig;

    }//end __construct()


    /**
     * @param AbstractCollection $collection
     * @return $this
     */
    public function setCollection($collection)
    {
        $this->_collection = $collection;
        return $this;

    }//end setCollection()


    public function getCollection()
    {
        return $this->_collection;


    }//end getCollection()


    public function getConfig($key, $default = '')
    {
        $c = explode("/", $key);
        if (count($c) == 2 && $this->hasData($c[1])) {
            return $this->getData($c[1]);
        }

        if ($this->hasData($key)) {
            return $this->getData($key);
        }

        if ($val = $this->galleryConfig->getConfig($key)) {
            return $val;
        }

        return $default;

    }//end getConfig()


}//end class
