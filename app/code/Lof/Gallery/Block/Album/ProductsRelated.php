<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Gallery\Block\Album;

class ProductsRelated extends \Magento\Catalog\Block\Product\AbstractProduct implements \Magento\Widget\Block\BlockInterface
{
    protected $_collection;

    /**
     * @var \Lof\Gallery\Model\Config
     */
    protected $galleryConfig;


    /**
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param \Lof\Gallery\Model\Config              $galleryConfig
     * @param array                                  $data
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Lof\Gallery\Model\Config $galleryConfig,
        array $data = []
    ) {
        parent::__construct($context);
        $this->galleryConfig = $galleryConfig;

    }//end __construct()


    public function _toHtml()
    {
        $album = $this->getAlbum();
        $this->setCollection($album->getProductCollection());
        return parent::_toHtml();

    }//end _toHtml()


    public function getConfig($key)
    {
        return $this->galleryConfig->getConfig($key);

    }//end getConfig()


    /**
     * Set brand collection
     *
     * @param \Ves\Blog\Model\Post
     */
    public function setCollection($collection)
    {
        $this->_collection = $collection;
        return $this->_collection;

    }//end setCollection()


    public function getCollection()
    {
        return $this->_collection;

    }//end getCollection()


    public function getAlbum()
    {
        $album = $this->_coreRegistry->registry('gallery_album');
        return $album;

    }//end getAlbum()


}//end class
