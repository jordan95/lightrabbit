<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */

 namespace Lof\Gallery\Block\Album;

class View extends \Magento\Catalog\Block\Product\AbstractProduct implements \Magento\Widget\Block\BlockInterface
{

    /**
     * @var \Magento\Framework\Data\CollectionFactory
     */
    protected $_collectionFactory;

    /**
     * @var array
     */
    protected $_images;

    /**
     * @var \Lof\Gallery\Model\Config
     */
    protected $galleryConfig;


    /**
     * View constructor.
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param \Magento\Framework\Data\CollectionFactory $collectionFactory
     * @param \Lof\Gallery\Model\Config $galleryConfig
     * @param array $data
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Framework\Data\CollectionFactory $collectionFactory,
        \Lof\Gallery\Model\Config $galleryConfig,
        array $data = []
    ) {
        parent::__construct($context);
        $this->_collectionFactory = $collectionFactory;
        $this->galleryConfig      = $galleryConfig;

    }//end __construct()


    public function setImages($images)
    {
        $this->_images = $images;
        return $this;

    }//end setImages()


    public function getImages()
    {
        return $this->_images;

    }//end getImages()


    public function getConfig($key)
    {
        return $this->galleryConfig->getConfig($key);

    }//end getConfig()


    protected function _addBreadcrumbs()
    {
        $breadcrumbBlock = $this->getLayout()->getBlock('breadcrumbs');
        $showBreadcrumb  = $this->galleryConfig->getConfig('album_page/show_breadcrumb');

        if($showBreadcrumb && $breadcrumbBlock) {
            $baseUrl = $this->_storeManager->getStore()->getBaseUrl();
            $breadcrumbBlock->addCrumb(
                'home',
                [
                 'label' => __('Home'),
                 'title' => __('Go to Home Page'),
                 'link'  => $baseUrl,
                ]
            );

            $breadcrumbBlock->addCrumb(
                'gallerypage',
                [
                 'label' => __('Gallery'),
                 'title' => __('Gallery'),
                 'link'  => $this->galleryConfig->getGalleryPageUrl(),
                ]
            );

            $album = $this->getAlbum();
            if($album) {
                $breadcrumbBlock->addCrumb(
                    'album',
                    [
                     'label' => $album->getName(),
                     'title' => $album->getName(),
                     'link'  => '',
                    ]
                );
            }
        }//end if

    }//end _addBreadcrumbs()


    /**
     * Need use as _prepareLayout - but problem in declaring collection from
     * another block (was problem with search result)
     *
     * @return $this
     */
    protected function _beforeToHtml()
    {
        $album       = $this->getAlbum();
        $itemPerPage = $album->getItemsPerPage();
        $toolbar     = $this->getToolbarBlock();
        $collection  = $this->_collectionFactory->create();
        $images      = $album->getImages();
        $curPage     = (int) $this->getRequest()->getParam('p');

        $sort_type   = "random";//random, position asc

        if (!$curPage || $curPage < 0) {
            $curPage = 1;
        }

        if ($itemPerPage && (($curPage * $itemPerPage) > count($images))) {
            $curPage = ceil(count($images) / $itemPerPage);
        }

        $start = (($curPage - 1) * $itemPerPage);

        $i           = 1;
        $albumImages = [];
        foreach ($images as $k => $image) {
            $item = new \Magento\Framework\DataObject($image);
            $collection->addItem($item);
            if (($itemPerPage && $i >= $start && $i < ($start + $itemPerPage)) || !$itemPerPage) {
                $image['id']   = $k;
                $albumImages[] = $image;
            }

            $i++;
        }

        if($albumImages){
            if($sort_type == "random"){
                $random_keys = array_rand($albumImages, count($albumImages));
                $tmp = [];
                for($i = 0; $i < count($random_keys); $i++) {
                    if(isset($albumImages[$i])){
                        $tmp[] = $albumImages[$i];
                    }
                }
                if($tmp) {
                    $albumImages = $tmp;
                }
            }
            
        }

        $this->setImages($albumImages);

        $collection->setCurPage(($curPage - 1));
        if ($toolbar && $itemPerPage) {
            $toolbar->setData('_current_limit', $itemPerPage)->setCollection($collection);
            $this->setChild('toolbar', $toolbar);
        }

        return parent::_beforeToHtml();

    }//end _beforeToHtml()


    /**
     * Retrieve Toolbar block
     *
     * @return \Magento\Catalog\Block\Product\ProductList\Toolbar
     */
    public function getToolbarBlock()
    {
        $block = $this->getLayout()->getBlock('gallery_toolbar');
        return $block;

    }//end getToolbarBlock()


    protected function _prepareLayout()
    {
        $this->_addBreadcrumbs();
        $album = $this->getAlbum();
        if($album) {
            $this->pageConfig->addBodyClass('gallery-album-'.$album->getIdentifier());
            if ($metaKeywords = $album->getMetaKeywords()) {
                $this->pageConfig->setKeywords($metaKeywords);
            }

            if ($metaDescription = $album->getMetaDescription()) {
                $this->pageConfig->setDescription($metaDescription);
            }

            $pageTitle = $album->getPageTitle();
            if ($pageTitle == '') {
                $pageTitle = $album->getName();
            }

            if($pageTitle) {
                $this->pageConfig->getTitle()->set($pageTitle);
            }
        }

        return parent::_prepareLayout();

    }//end _prepareLayout()


    public function getAlbum()
    {
        $album = $this->_coreRegistry->registry('gallery_album');
        return $album;

    }//end getAlbum()


}//end class
