<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Gallery\Block\Adminhtml;

use Lof\All\Model\Config;

class Menu extends \Magento\Backend\Block\Template
{
    /**
     * @var null|array
     */
    protected $items = null;

    /**
     * Block template filename
     *
     * @var string
     */
    protected $_template = 'Lof_All::menu.phtml';


    public function __construct(\Magento\Backend\Block\Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context);

    }//end __construct()


    public function getMenuItems()
    {
        if ($this->items === null) {
            $items = [
                      'album'    => [
                                     'title'    => __('Manage Albums'),
                                     'url'      => $this->getUrl('*/album/index'),
                                     'resource' => 'Lof_Gallery::album',
                                     'child'    => [
                                                    'newAction' => [
                                                                    'title'    => __('New Album'),
                                                                    'url'      => $this->getUrl('*/album/new'),
                                                                    'resource' => 'Lof_Gallery::album_edit',
                                                                   ],
                                                   ],
                                    ],
                      'category' => [
                                     'title'    => __('Manage Categories'),
                                     'url'      => $this->getUrl('*/category/index'),
                                     'resource' => 'Lof_Gallery::category',
                                     'child'    => [
                                                    'newAction' => [
                                                                    'title'    => __('New Category'),
                                                                    'url'      => $this->getUrl('*/category/newAction'),
                                                                    'resource' => 'Lof_Gallery::category_edit',
                                                                   ],
                                                   ],
                                    ],
                      'tag'      => [
                                     'title'    => __('Manage Tags'),
                                     'url'      => $this->getUrl('*/tag/index'),
                                     'resource' => 'Lof_Gallery::tag',
                                    ],
                      'settings' => [
                                     'title'    => __('Settings'),
                                     'url'      => $this->getUrl('adminhtml/system_config/edit/section/lofgallery'),
                                     'resource' => 'Lof_Gallery::config_gallery',
                                    ],
                      'readme'   => [
                                     'title'     => __('Guide'),
                                     'url'       => Config::GALLERY_GUIDE,
                                     'attr'      => ['target' => '_blank'],
                                     'separator' => true,
                                    ],
                      'support'  => [
                                     'title' => __('Get Support'),
                                     'url'   => Config::LANDOFCODER_TICKET,
                                     'attr'  => ['target' => '_blank'],
                                    ],
                     ];
            foreach ($items as $index => $item) {
                if (array_key_exists('resource', $item)) {
                    if (!$this->_authorization->isAllowed($item['resource'])) {
                        unset($items[$index]);
                    }
                }
            }

            $this->items = $items;
        }//end if

        return $this->items;

    }//end getMenuItems()


    /**
     * @return array
     */
    public function getCurrentItem()
    {
        $items          = $this->getMenuItems();
        $controllerName = $this->getRequest()->getControllerName();
        if (array_key_exists($controllerName, $items)) {
            return $items[$controllerName];
        }

        return $items['page'];

    }//end getCurrentItem()


    /**
     * @param array $item
     * @return string
     */
    public function renderAttributes(array $item)
    {
        $result = '';
        if (isset($item['attr'])) {
            foreach ($item['attr'] as $attrName => $attrValue) {
                $result .= sprintf(' %s=\'%s\'', $attrName, $attrValue);
            }
        }

        return $result;

    }//end renderAttributes()


    /**
     * @param $itemIndex
     * @return bool
     */
    public function isCurrent($itemIndex)
    {
        return $itemIndex == $this->getRequest()->getControllerName();

    }//end isCurrent()


}//end class
