<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Gallery\Block\Adminhtml\Tag\Edit\Tab;

class Main extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{


    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('lofgallery_tag');

        if ($this->_isAllowedAction('Lof_Gallery::tag_edit')) {
            $isElementDisabled = false;
        } else {
            $isElementDisabled = true;
        }
        $this->_eventManager->dispatch(
        'lof_check_license',
        ['obj' => $this,'ex'=>'Lof_Gallery']
        );
        if (!$this->getData('is_valid') && !$this->getData('local_valid')) {
            $isElementDisabled = true;
            $wysiwygConfig['enabled'] = $wysiwygConfig['add_variables'] = $wysiwygConfig['add_widgets'] = $wysiwygConfig['add_images'] = 0;
            $wysiwygConfig['plugins'] = [];

        }
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('tag_');
        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Tag Information')]);
        if ($model->getId()) {
            $fieldset->addField('tag_id', 'hidden', ['name' => 'tag_id']);
        }

        $fieldset->addField(
            'name',
            'text',
            [
             'name'     => 'name',
             'label'    => __('Name'),
             'title'    => __('Name'),
             'required' => true,
             'disabled' => $isElementDisabled,
            ]
        );

        $fieldset->addField(
            'alias',
            'text',
            [
             'name'     => 'alias',
             'label'    => __('URL Key'),
             'title'    => __('URL Key'),
             'class'    => 'validate-identifier',
             'required' => true,
             'note'     => __('Relative to Web Site Base URL'),
             'disabled' => $isElementDisabled,
            ]
        );

        $fieldset->addField(
            'is_active',
            'select',
            [
             'label'    => __('Status'),
             'title'    => __('Status'),
             'name'     => 'is_active',
             'options'  => $model->getAvailableStatuses(),
             'disabled' => $isElementDisabled,
            ]
        );

        $fieldset->addField(
            'tag_position',
            'text',
            [
             'label'    => __('Position'),
             'title'    => __('Position'),
             'name'     => 'cat_position',
             'disabled' => $isElementDisabled,
            ]
        );

        $data = $model->getData();
        if (!$form->getId()) {
            $data['is_active'] = 1;
        }

        $form->setValues($data);
        $this->setForm($form);

        return parent::_prepareForm();

    }//end _prepareForm()


    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Category Information');

    }//end getTabLabel()


    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Category Information');

    }//end getTabTitle()


    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;

    }//end canShowTab()


    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;

    }//end isHidden()


    /**
     * Check permission for passed action
     *
     * @param  string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);

    }//end _isAllowedAction()


}//end class
