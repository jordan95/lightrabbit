<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Gallery\Block\Adminhtml\Tag;

class Edit extends \Magento\Backend\Block\Widget\Form\Container
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;


    /**
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Magento\Framework\Registry           $registry
     * @param array                                 $data
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);

    }//end __construct()


    protected function _construct()
    {
        $this->_objectId   = 'tag_id';
        $this->_blockGroup = 'Lof_Gallery';
        $this->_controller = 'adminhtml_tag';

        parent::_construct();

        if ($this->_isAllowedAction('Lof_Gallery::tag_save')) {
            $this->buttonList->update('save', 'label', __('Save Tag'));
            $this->buttonList->add(
                'saveandcontinue',
                [
                 'label'          => __('Save and Continue Edit'),
                 'class'          => 'save',
                 'data_attribute' => [
                                      'mage-init' => [
                                                      'button' => [
                                                                   'event'  => 'saveAndContinueEdit',
                                                                   'target' => '#edit_form',
                                                                  ],
                                                     ],
                                     ],
                ],
                -100
            );
        }else {
            $this->buttonList->remove('save');
        }//end if

        if ($this->_isAllowedAction('Lof_Gallery::tag_delete')) {
            $this->buttonList->update('delete', 'label', __('Delete Tag'));
        } else {
            $this->buttonList->remove('delete');
        }

    }//end _construct()

    
    /**
     * Retrieve text for header element depending on loaded page
     *
     * @return \Magento\Framework\Phrase
     */
    public function getHeaderText()
    {
        if ($this->_coreRegistry->registry('lofgallery_tag')->getId()) {
            return __("Edit Form '%1'", $this->escapeHtml($this->_coreRegistry->registry('lofgallery_tag')->getTitle()));
        } else {
            return __('New Form');
        }

    }//end getHeaderText()


    /**
     * Check permission for passed action
     *
     * @param  string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);

    }//end _isAllowedAction()


    /**
     * Prepare layout
     *
     * @return \Magento\Framework\View\Element\AbstractBlock
     */
    protected function _prepareLayout()
    {
        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('page_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'page_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'page_content');
                }
            };
        ";
        return parent::_prepareLayout();

    }//end _prepareLayout()


}//end class
