<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Gallery\Block\Adminhtml\Album\Renderer;
use Magento\Framework\UrlInterface;

class Image extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\Text
{


    public function _getValue(\Magento\Framework\DataObject $row)
    {

        return sprintf("<img src='%s' style='width:70px;' />", $row['link']);

    }//end _getValue()


}//end class
