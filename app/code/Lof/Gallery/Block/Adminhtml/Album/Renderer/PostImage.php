<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Gallery\Block\Adminhtml\Album\Renderer;
use Magento\Framework\UrlInterface;

class PostImage extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\Text
{


    /**
     * @var \Magento\Store\Model\StoreManager
     */
    protected $_storeManager;


    /**
     * @param \Magento\Backend\Block\Context    $context
     * @param \Magento\Store\Model\StoreManager $storeManager
     */
    public function __construct(
        \Magento\Backend\Block\Context $context,
        \Magento\Store\Model\StoreManager $storeManager
    ) {
        $this->_storeManager = $storeManager;
        parent::__construct($context);

    }//end __construct()


    public function _getValue(\Magento\Framework\DataObject $row)
    {
        $mediaUrl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        if ($row['thumbnail']) {
            $src = $mediaUrl.$row['thumbnail'];
            return sprintf("<img src='%s' class='gallery-image' style='width: 300px;' />", $src);
        } else {
            return '';
        }

    }//end _getValue()


}//end class
