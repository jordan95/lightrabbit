<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Gallery\Block\Adminhtml\Album\Renderer;

class AlbumAction extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\Text
{
    /**
     * @var \Magento\Framework\Url
     */
    protected $_urlBuilder;


    /**
     * @param \Magento\Backend\Block\Context
     * @param \Magento\Framework\Url
     * @param \Magento\Store\Model\StoreManager
     */
    public function __construct(
        \Magento\Backend\Block\Context $context,
        \Magento\Framework\Url $url
    ) {
        $this->_urlBuilder = $url;
        parent::__construct($context);

    }//end __construct()


    public function _getValue(\Magento\Framework\DataObject $row)
    {
        $editUrl = $this->_urlBuilder->getUrl(
            'lofgallery/album/edit',
            [
             'album_id' => $row['album_id'],
            ]
        );
        return sprintf("<a target='_blank' href='%s'>Edit</a>", $editUrl);

    }//end _getValue()


}//end class
