<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Gallery\Block\Adminhtml\Album\Helper\Form\Gallery;

use Magento\Backend\Block\Media\Uploader;
use Magento\Framework\View\Element\AbstractBlock;
use Magento\Framework\App\Filesystem\DirectoryList;
use Lof\Gallery\Model\Config;

class Content extends \Magento\Backend\Block\Widget
{
    /**
     * @var string
     */
    protected $_template = 'Lof_Gallery::helper/gallery.phtml';

    /**
     * @var \Magento\Catalog\Model\Product\Media\Config
     */
    protected $_mediaConfig;

    /**
     * @var \Magento\Framework\Json\EncoderInterface
     */
    protected $_jsonEncoder;

    /**
     * @var \Lof\Gallery\Helper\Data
     */
    protected $_helperFunction;

    /**
     * Content constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Json\EncoderInterface $jsonEncoder
     * @param \Magento\Catalog\Model\Product\Media\Config $mediaConfig
     * @param Config $galleryConfig
     * @param \Lof\Gallery\Helper\Data $helperFunction
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Catalog\Model\Product\Media\Config $mediaConfig,
        \Lof\Gallery\Model\Config $galleryConfig,
        \Lof\Gallery\Helper\Data $helperFunction,
        array $data = []
    ) {
        $this->_helperFunction = $helperFunction;
        $this->_jsonEncoder = $jsonEncoder;
        $this->_mediaConfig = $mediaConfig;
        parent::__construct($context, $data);
        $this->galleryConfig = $galleryConfig;

    }//end __construct()


    /**
     * @return AbstractBlock
     */
    protected function _prepareLayout()
    {
        $this->addChild('uploader', 'Lof\Gallery\Block\Media\Uploader');

        $this->getUploader()->getConfig()->setUrl(
            $this->_urlBuilder->addSessionParam()->getUrl('*/*/upload')
        )->setFileField(
            'image'
        )->setFilters(
            [
             'images' => [
                          'label' => __('Images (.gif, .jpg, .png)'),
                          'files' => [
                                      '*.gif',
                                      '*.jpg',
                                      '*.jpeg',
                                      '*.png',
                                     ],
                         ],
            ]
        );

        return parent::_prepareLayout();

    }//end _prepareLayout()


    /**
     * Retrieve uploader block
     *
     * @return Uploader
     */
    public function getUploader()
    {
        return $this->getChildBlock('uploader');

    }//end getUploader()


    /**
     * Retrieve uploader block html
     *
     * @return string
     */
    public function getUploaderHtml()
    {
        return $this->getChildHtml('uploader');

    }//end getUploaderHtml()


    /**
     * @return string
     */
    public function getJsObjectName()
    {
        return $this->getHtmlId().'JsObject';

    }//end getJsObjectName()


    /**
     * @return string
     */
    public function getAddImagesButton()
    {
        return $this->getButtonHtml(
            __('Add New Images'),
            $this->getJsObjectName().'.showUploader()',
            'add',
            $this->getHtmlId().'_add_images_button'
        );

    }//end getAddImagesButton()


    /**
     * @return string
     */
    public function getImagesJson()
    {
        $value = $this->getElement()->getImages();
        if (is_array($value)
            && array_key_exists('images', $value)
            && is_array($value['images'])
            && count($value['images'])
        ) {
            $directory = $this->_filesystem->getDirectoryRead(DirectoryList::MEDIA);
            foreach ($value['images'] as $k => &$image) {
                $image['field_id'] = $k;
            }
            $images = $this->sortImagesByPosition($value['images']);

            foreach ($images as $k => &$image) {
                $imageNormal = file_exists($this->appendAbsoluteFileSystemPath(Config::MEDIA_PATH . $image['file']));
                $imageLibary =  file_exists($this->appendAbsoluteFileSystemPath($image['file']));

                if (isset($image['file']) && $imageNormal) {
                    $fileHandler   = $directory->stat(Config::MEDIA_PATH.$image['file']);
                    $image['size'] = $fileHandler['size'];
                    $image['url']  = $this->galleryConfig->getFileUrl($image['file']);
                } elseif(isset($image['file']) && !$imageNormal && $imageLibary){
                    $fileHandler   = $directory->stat($image['file']);
                    $image['size'] = $fileHandler['size'];
                    $image['url']  = $this->_helperFunction->getMediaPath().$image['file'];
                } elseif(isset($image['file']) && !$imageNormal && !$imageLibary){
                    $imageInstagram = strpos($image['file'], 'instagram.com/explore/tags') ;
                    if($imageInstagram == true){
                        $image['url'] = $image['file'];
                        $image['size'] = 'Image From #hashTag';
                        $image['sizeLabel'] = $image['size'];
                    }else{
                        $image['url'] = $image['file'];
                        $image['size'] = 'Image Take Other Website';
                        $image['sizeLabel'] = $image['size'];
                    }

                }

            }

            return $this->_jsonEncoder->encode($images);
        }

        return '[]';

    }//end getImagesJson()


    /**
     * @param string $localTmpFile
     * @return string
     */
    protected function appendAbsoluteFileSystemPath($localTmpFile)
    {
        /*
            * @var \Magento\Framework\Filesystem\Directory\Read $mediaDirectory
        */
        $mediaDirectory = $this->_filesystem->getDirectoryRead(DirectoryList::MEDIA);
        $pathToSave     = $mediaDirectory->getAbsolutePath();
        return $pathToSave.$localTmpFile;

    }//end appendAbsoluteFileSystemPath()


    /**
     * Sort images array by position key
     *
     * @param  array $images
     * @return array
     */
    private function sortImagesByPosition($images)
    {
        if (is_array($images) && count($images > 1)) {
            usort(
                $images,
                function ($imageA, $imageB) {
                    return ($imageA['position'] < $imageB['position']) ? (-1) : 1;
                }
            );
        }

        return $images;

    }//end sortImagesByPosition()


    /**
     * @return string
     */
    public function getImagesValuesJson()
    {
        $values = [];
        foreach ($this->getMediaAttributes() as $attribute) {
            // @var $attribute \Magento\Eav\Model\Entity\Attribute
            $values[$attribute->getAttributeCode()] = $this->getElement()->getDataObject()->getData(
                $attribute->getAttributeCode()
            );
        }

        return $this->_jsonEncoder->encode($values);

    }//end getImagesValuesJson()


    /**
     * Get image types data
     *
     * @return array
     */
    public function getImageTypes()
    {
        $images = $this->getElement()->getImages();

        $imageTypes = [];
        foreach ($this->getMediaAttributes() as $attribute) {
            // @var $attribute \Magento\Eav\Model\Entity\Attribute
            $imageTypes[$attribute['code']] = [
                                               'code'  => $attribute['code'],
                                               'value' => '',
                                               'label' => $attribute['label'],
                                               'scope' => __('STORE VIEW'),
                                               'name'  => $this->getElement()->getAttributeFieldName($attribute['code']),
                                              ];

            if (isset($images['images'])) {
                foreach ($images['images'] as $k => $image) {
                    if (isset($image[$attribute['code']]) && $image[$attribute['code']]) {
                        $imageTypes[$attribute['code']]['value'] = $image['file'];
                    }
                }
            }
        }

        return $imageTypes;

    }//end getImageTypes()


    /**
     * Retrieve default state allowance
     *
     * @return bool
     */
    public function hasUseDefault()
    {
        return false;

    }//end hasUseDefault()


    /**
     * Retrieve media attributes
     *
     * @return array
     */
    public function getMediaAttributes()
    {
        $imageTypes[] = [
                         'code'  => 'thumbnail',
                         'label' => __('Thumbnail'),
                        ];
        $imageTypes[] = [
                         'code'  => 'image',
                         'label' => __('Base'),
                        ];
        return $imageTypes;
        return $this->getElement()->getDataObject()->getMediaAttributes();

    }//end getMediaAttributes()


    /**
     * Retrieve JSON data
     *
     * @return string
     */
    public function getImageTypesJson()
    {
        return $this->_jsonEncoder->encode($this->getImageTypes());

    }//end getImageTypesJson()


    public function getAlbumId(){
        return $this->getRequest()->getParam('album_id');
    }

}//end class
