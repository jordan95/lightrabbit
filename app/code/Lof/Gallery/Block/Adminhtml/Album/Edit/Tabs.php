<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Gallery\Block\Adminhtml\Album\Edit;

class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $_moduleManager;


    /**
     * @param \Magento\Backend\Block\Template\Context  $context
     * @param \Magento\Framework\Json\EncoderInterface $jsonEncoder
     * @param \Magento\Backend\Model\Auth\Session      $authSession
     * @param \Magento\Framework\Module\Manager        $moduleManager
     * @param array                                    $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Backend\Model\Auth\Session $authSession,
        \Magento\Framework\Module\Manager $moduleManager,
        array $data = []
    ) {
        $this->_moduleManager = $moduleManager;
        parent::__construct($context, $jsonEncoder, $authSession);

    }//end __construct()


    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('album_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Album Information'));

        $this->addTab(
            'main_section',
            [
             'label'   => __('General'),
             'content' => $this->getLayout()->createBlock('Lof\Gallery\Block\Adminhtml\Album\Edit\Tab\Main')->toHtml(),
            ]
        );

        $this->addTabAfter(
            'related_products',
            [
             'label' => __('Related Products'),
             'url'   => $this->getUrl('lofgallery/album/relatedproducts', ['_current' => true]),
             'class' => 'ajax relatedproducts',
            ],
            'display'
        );

        if ($this->_moduleManager->isEnabled('Ves_Blog')) {
            $this->addTabAfter(
                'related_posts',
                [
                 'label' => __('Related Posts'),
                 'url'   => $this->getUrl('lofgallery/album/relatedposts', ['_current' => true]),
                 'class' => 'ajax relatedposts',
                ],
                'display'
            );
        }

        if ($this->_moduleManager->isEnabled('Lof_StoreLocator')) {
            $this->addTabAfter(
                'related_stores',
                [
                 'label' => __('Related Stores'),
                 'url'   => $this->getUrl('lofgallery/album/relatedstores', ['_current' => true]),
                 'class' => 'ajax relatedstores',
                ],
                'display'
            );
        }

        $this->setActiveTab("main_section");

    }//end _construct()


}//end class
