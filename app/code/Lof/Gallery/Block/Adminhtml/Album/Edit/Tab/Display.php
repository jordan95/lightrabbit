<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Gallery\Block\Adminhtml\Album\Edit\Tab;

use Lof\Gallery\Model\Album;

class Display extends \Magento\Backend\Block\Widget\Form\Generic implements
\Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * @var \Lof\Gallery\Model\Config\Source\PageTypes
     */
    protected $pageTypes;

    /**
     * @var \Lof\Gallery\Model\Config\Source\StoreCategories
     */
    protected $storeCategories;

    /**
     * @var \Lof\Gallery\Model\Config\Source\AlbumLayout
     */
    protected $albumLayout;

    /**
     * @var \Lof\Gallery\Model\Config\Source\Gridcolumn
     */
    protected $gridColumn;


    /**
     * @param \Magento\Backend\Block\Template\Context          $context
     * @param \Magento\Framework\Registry                      $registry
     * @param \Magento\Framework\Data\FormFactory              $formFactory
     * @param \Lof\Gallery\Model\Config\Source\PageTypes       $pageTypes
     * @param \Lof\Gallery\Model\Config\Source\StoreCategories $storeCategories
     * @param \Lof\Gallery\Model\Config\Source\AlbumLayout     $albumLayout
     * @param \Lof\Gallery\Model\Config\Source\Gridcolumn      $gridColumn
     * @param array                                            $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Lof\Gallery\Model\Config\Source\PageTypes $pageTypes,
        \Lof\Gallery\Model\Config\Source\StoreCategories $storeCategories,
        \Lof\Gallery\Model\Config\Source\AlbumLayout $albumLayout,
        \Lof\Gallery\Model\Config\Source\Gridcolumn $gridColumn,
        \Lof\Gallery\Model\Config\Source\Lightbox $lightbox,
        array $data = []
    ) {
        parent::__construct($context, $registry, $formFactory, $data);
        $this->pageTypes       = $pageTypes;
        $this->storeCategories = $storeCategories;
        $this->albumLayout     = $albumLayout;
        $this->gridColumn      = $gridColumn;
        $this->lightbox        = $lightbox;

    }//end __construct()


    /**
     * Prepare form tab configuration
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setShowGlobalIcon(true);

    }//end _construct()


    /**
     * Initialise form fields
     *
     * @return                                        $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm()
    {
        /*
            * Checking if user have permissions to save information
         */
        if ($this->_isAllowedAction('Lof_Gallery::album_edit')) {
            $isElementDisabled = false;
        } else {
            $isElementDisabled = true;
        }
        $this->_eventManager->dispatch(
        'lof_check_license',
        ['obj' => $this,'ex'=>'Lof_Gallery']
        );
        if (!$this->getData('is_valid') && !$this->getData('local_valid')) {
            $isElementDisabled = true;
            $wysiwygConfig['enabled'] = $wysiwygConfig['add_variables'] = $wysiwygConfig['add_widgets'] = $wysiwygConfig['add_images'] = 0;
            $wysiwygConfig['plugins'] = [];

        }
        /*
            * @var \Magento\Framework\Data\Form $form
        */

        $form  = $this->_formFactory->create();
        $model = $this->_coreRegistry->registry('lofgallery_album');
        $fieldset = $form->addFieldset(
            'layout_fieldset',
            [
             'legend'   => __('Position'),
             'class'    => 'fieldset-wide',
             'disabled' => $isElementDisabled,
            ]
        );

        $fieldset->addField(
            'page_type',
            'multiselect',
            [
             'name'               => 'page_type',
             'label'              => __('Where to Display'),
             'style'              => 'height: 200px;',
             'values'             => $this->pageTypes->toOptionArray(),
             'disabled'           => $isElementDisabled,
             'after_element_html' => '
                    <script>
                        require(["jquery"], function($){
                            $( document ).ready(function() {
                                $("#page_type").on("change", function(){
                                    var val = $(this).val();
                                    if (val) {
                                        if (val.indexOf( "'.Album::DISPLAY_CATEGORYPAGE.'") != -1) {
                                            $(".field-store_categories").show();
                                        } else {
                                            $(".field-store_categories").hide();
                                        }
                                    } else {
                                        $("#conditions_fieldset").hide();
                                        $(".field-store_categories").hide();
                                    }
                                }).change();
                            });
                        });
                    </script>
                ',
            ]
        );

        $fieldset->addField(
            'store_categories',
            'Lof\Gallery\Block\Adminhtml\Album\Form\UiSelect',
            [
             'name'     => 'store_categories',
             'label'    => __('Categories'),
             'title'    => __('Categories'),
             'disabled' => $isElementDisabled,
             'options'  => $this->storeCategories->getCategoriesTree(),
            ]
        );

        $fieldset = $form->addFieldset(
            'display_fieldset',
            [
             'legend'   => __('Display'),
             'class'    => 'fieldset-wide',
             'disabled' => $isElementDisabled,
            ]
        );

        $fieldset->addField(
            'lightbox',
            'select',
            [
             'name'     => 'lightbox',
             'label'    => __('Lightbox'),
             'values'   => $this->lightbox->toOptionArray(),
             'disabled' => $isElementDisabled,
            ]
        );

        $fieldset->addField(
            'layout_type',
            'select',
            [
             'name'               => 'layout_type',
             'label'              => __('Type'),
             'values'             => $this->albumLayout->toOptionArray(),
             'disabled'           => $isElementDisabled,
             'after_element_html' => '
                    <script>
                        require(["jquery"], function($){
                            $( document ).ready(function() {
                                $("#layout_type").on("change", function(){
                                    var val = $(this).val();
                                    if (val == "list") {
                                        $(".field-lg_column").hide();
                                        $(".field-md_column").hide();
                                        $(".field-sm_column").hide();
                                        $(".field-xs_column").hide();
                                    } else {
                                        $(".field-lg_column").show();
                                        $(".field-md_column").show();
                                        $(".field-sm_column").show();
                                        $(".field-xs_column").show();
                                    }
                                }).change();

                                $("#items_per_page").on("change", function(){
                                    var val = $(this).val();
                                    if (parseInt(val)>0) {
                                        $(".field-show_toptoolbar").show();
                                        $(".field-show_bottomtoolbar").show();
                                    } else {
                                        $(".field-show_toptoolbar").hide();
                                        $(".field-show_bottomtoolbar").hide();
                                    }
                                }).change();
                            });
                        });
                    </script>
                ',
            ]
        );

        $fieldset->addField(
            'items_per_page',
            'text',
            [
             'name'     => 'items_per_page',
             'label'    => __('Number Items Per Page'),
             'note'     => __('Empty to show all items'),
             'disabled' => $isElementDisabled,
            ]
        );

        $fieldset->addField(
            'show_toptoolbar',
            'select',
            [
             'label'    => __('Top Toolbar'),
             'title'    => __('Top Toolbar'),
             'name'     => 'show_toptoolbar',
             'style'    => 'width: 107px',
             'options'  => $model->getAvailableStatuses(),
             'disabled' => $isElementDisabled,
            ]
        );

        $fieldset->addField(
            'show_bottomtoolbar',
            'select',
            [
             'label'    => __('Bottom Toolbar'),
             'title'    => __('Bottom Toolbar'),
             'name'     => 'show_bottomtoolbar',
             'style'    => 'width: 107px',
             'options'  => $model->getAvailableStatuses(),
             'disabled' => $isElementDisabled,
            ]
        );

        $fieldset->addField(
            'lg_column',
            'select',
            [
             'name'     => 'lg_column',
             'label'    => __('Number Column on Large Desktop'),
             'values'   => $this->gridColumn->toOptionArray(),
             'disabled' => $isElementDisabled,
             'style'    => 'width: 107px;',
             'note'     => __('Large devices Desktops (≥1200px)'),
            ]
        );

        $fieldset->addField(
            'md_column',
            'select',
            [
             'name'     => 'md_column',
             'label'    => __('Number Column on Large Desktop'),
             'values'   => $this->gridColumn->toOptionArray(),
             'disabled' => $isElementDisabled,
             'style'    => 'width: 107px;',
             'note'     => __('Medium devices Desktops (≥992px)'),
            ]
        );

        $fieldset->addField(
            'sm_column',
            'select',
            [
             'name'     => 'sm_column',
             'label'    => __('Number Column on Tablets'),
             'values'   => $this->gridColumn->toOptionArray(),
             'disabled' => $isElementDisabled,
             'style'    => 'width: 107px;',
             'note'     => __('Small devices Tablets (≥768px)'),
            ]
        );

        $fieldset->addField(
            'xs_column',
            'select',
            [
             'name'     => 'xs_column',
             'label'    => __('Number Column on Large Desktop'),
             'values'   => $this->gridColumn->toOptionArray(),
             'disabled' => $isElementDisabled,
             'style'    => 'width: 107px;',
             'note'     => __('Extra small devices Phones (<768px)'),
            ]
        );

        if (!$model->getId()) {
            $model->setData('layout_type', 'masonry');
            $model->setData('lg_column', 3);
            $model->setData('md_column', 3);
            $model->setData('sm_column', 2);
            $model->setData('xs_column', 1);
        }

        $form->setValues($model->getData());
        $this->setForm($form);
        return parent::_prepareForm();

    }//end _prepareForm()


    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Display');

    }//end getTabLabel()


    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Display');

    }//end getTabTitle()


    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;

    }//end canShowTab()


    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;

    }//end isHidden()


    /**
     * Check permission for passed action
     *
     * @param  string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);

    }//end _isAllowedAction()


}//end class
