<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Gallery\Block\Adminhtml\Album\Edit\Tab;

class Main extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * @var \Magento\Cms\Model\Wysiwyg\Config
     */
    protected $wysiwygConfig;

    /**
     * @var Lof\Gallery\Helper\Category
     */
    protected $galleryCategoryHelper;

    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $systemStore;


    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry             $registry
     * @param \Magento\Framework\Data\FormFactory     $formFactory
     * @param \Magento\Cms\Model\Wysiwyg\Config       $wysiwygConfig
     * @param \Lof\Gallery\Helper\Category            $galleryCategoryHelper
     * @param \Magento\Store\Model\System\Store       $systemStore
     * @param array                                   $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig,
        \Lof\Gallery\Helper\Category $galleryCategoryHelper,
        \Magento\Store\Model\System\Store $systemStore,
        array $data = []
    ) {
        parent::__construct($context, $registry, $formFactory, $data);
        $this->wysiwygConfig         = $wysiwygConfig;
        $this->systemStore        = $systemStore;
        $this->galleryCategoryHelper = $galleryCategoryHelper;

    }//end __construct()


    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('lofgallery_album');

        if ($this->_isAllowedAction('Lof_Gallery::album_edit')) {
            $isElementDisabled = false;
        } else {
            $isElementDisabled = true;
        }
        $this->_eventManager->dispatch(
        'lof_check_license',
        ['obj' => $this,'ex'=>'Lof_Gallery']
        );


        $wysiwygConfig = $this->wysiwygConfig->getConfig(['tab_id' => $this->getTabId().time()]);
        if (!$this->getData('is_valid') && !$this->getData('local_valid')) {
            $isElementDisabled = true;
            $wysiwygConfig['enabled'] = $wysiwygConfig['add_variables'] = $wysiwygConfig['add_widgets'] = $wysiwygConfig['add_images'] = 0;
            $wysiwygConfig['plugins'] = [];

        }
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('album_');
        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Album Information')]);
        if ($model->getId()) {
            $fieldset->addField('album_id', 'hidden', ['name' => 'album_id']);
        }

        $fieldset->addField(
            'name',
            'text',
            [
             'name'     => 'name',
             'label'    => __('Album Title'),
             'title'    => __('Album Title'),
             'required' => true,
             'disabled' => $isElementDisabled,
            ]
        );

        $fieldset->addField(
            'identifier',
            'text',
            [
             'name'     => 'identifier',
             'label'    => __('URL Key'),
             'title'    => __('URL Key'),
             'class'    => 'validate-identifier',
             'required' => true,
             'note'     => __('Relative to Web Site Base URL'),
             'disabled' => $isElementDisabled,
            ]
        );

        $fieldset->addField(
            'categories',
            'Lof\Gallery\Block\Adminhtml\Album\Form\UiSelect',
            [
             'name'     => 'categories',
             'label'    => __('Categories'),
             'title'    => __('Categories'),
             'options'  => $this->galleryCategoryHelper->getTree(),
             'disabled' => $isElementDisabled,
            ]
        );

        $fieldset->addField(
            'tags',
            'Lof\Gallery\Block\Adminhtml\Album\Form\Tag',
            [
             'name'     => 'tags',
             'label'    => __('Tags'),
             'title'    => __('Tags'),
             'disabled' => $isElementDisabled,
            ]
        );

        
        $fieldset->addField(
            'description',
            'editor',
            [
             'name'     => 'description',
             'label'    => __('Description'),
             'style'    => 'height:20em;',
             'disabled' => $isElementDisabled,
             'config'   => $wysiwygConfig,
            ]
        );

        if (!$this->_storeManager->isSingleStoreMode()) {
            $field    = $fieldset->addField(
                'store_id',
                'multiselect',
                [
                 'name'     => 'stores[]',
                 'label'    => __('Store View'),
                 'title'    => __('Store View'),
                 'required' => true,
                 'values'   => $this->systemStore->getStoreValuesForForm(false, true),
                 'disabled' => $isElementDisabled,
                ]
            );
            $renderer = $this->getLayout()->createBlock(
                'Magento\Backend\Block\Store\Switcher\Form\Renderer\Fieldset\Element'
            );
            $field->setRenderer($renderer);
        } else {
            $fieldset->addField(
                'store_id',
                'hidden',
                [
                 'name'  => 'stores[]',
                 'value' => $this->_storeManager->getStore(true)->getId(),
                ]
            );
            $model->setStoreId($this->_storeManager->getStore(true)->getId());
        }//end if

        $dateFormat = $this->_localeDate->getDateFormat(\IntlDateFormatter::SHORT);
        $fieldset->addField(
            'created_at',
            'date',
            [
             'name'        => 'created_at',
             'label'       => __('Created time'),
             'required'    => true,
             'disabled' => $isElementDisabled,
             'date_format' => $dateFormat,
            ]
        );

        $fieldset->addField(
            'is_active',
            'select',
            [
             'label'    => __('Status'),
             'title'    => __('Status'),
             'name'     => 'is_active',
             'style'    => 'width: 160px',
             'options'  => $model->getAvailableStatuses(),
             'disabled' => $isElementDisabled,
            ]
        );

        $fieldset->addField(
            'enable_comment',
            'select',
            [
             'label'    => __('Comment'),
             'title'    => __('Comment'),
             'name'     => 'enable_comment',
             'style'    => 'width: 160px',
             'options'  => $model->getAvailableStatuses(),
             'disabled' => $isElementDisabled,
            ]
        );

        $fieldset->addField(
            'hits',
            'text',
            [
            'disabled' => $isElementDisabled,
             'name'  => 'hits',
             'label' => __('Hits'),
            ]
        );

        $fieldset->addField(
            'position',
            'text',
            [
                'disabled' => $isElementDisabled,
                'name'  => 'position',
                'label' => __('Position'),
            ]
        );

        if (!$model->getId()) {
            $model->setData('submit_button_text', __('Click here'));
            $model->setData('is_active', $isElementDisabled ? '0' : '1');
            $model->setData('enable_comment', $isElementDisabled ? '0' : '1');
        }

        if ($categories = $model->getData('categories')) {
            $newArr = [];
            foreach ($categories as $k => $v) {
                $newArr[] = $v['entity_id'];
            }
            $model->setData('categories', $newArr);
        }

        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();

    }//end _prepareForm()


    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Album Information');

    }//end getTabLabel()


    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Album Information');

    }//end getTabTitle()


    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;

    }//end canShowTab()


    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;

    }//end isHidden()


    /**
     * Check permission for passed action
     *
     * @param  string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);

    }//end _isAllowedAction()


}//end class
