<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Gallery\Block\Adminhtml\Album\Edit;

use Magento\Framework\Data\Form\Element\Fieldset;

class NewVideo extends \Magento\Backend\Block\Widget\Form\Generic
{
    /**
     * Anchor is product video
     */
    const PATH_ANCHOR_PRODUCT_VIDEO = 'catalog_product_video-link';

    /**
     * @var \Magento\ProductVideo\Helper\Media
     */
    protected $mediaHelper;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var \Magento\Framework\Json\EncoderInterface
     */
    protected $jsonEncoder;

    /**
     * @var string
     */
    protected $videoSelector = '#images_content';


    /**
     * @param \Magento\Backend\Block\Template\Context  $context
     * @param \Magento\Framework\Registry              $registry
     * @param \Magento\Framework\Data\FormFactory      $formFactory
     * @param \Magento\ProductVideo\Helper\Media       $mediaHelper
     * @param \Magento\Framework\Json\EncoderInterface $jsonEncoder
     * @param array                                    $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\ProductVideo\Helper\Media $mediaHelper,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Lof\Gallery\Block\Adminhtml\Album\Helper\Form\Gallery\Content $galleryContent,
        array $data = []
    ) {
        parent::__construct($context, $registry, $formFactory, $data);
        $this->mediaHelper    = $mediaHelper;
        $this->urlBuilder     = $context->getUrlBuilder();
        $this->jsonEncoder    = $jsonEncoder;
        $this->galleryContent = $galleryContent;
        $this->setUseContainer(true);

    }//end __construct()


    /**
     * Form preparation
     *
     * @return                                        void
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm()
    {
        /*
            * @var \Magento\Framework\Data\Form $form
        */
        $form = $this->_formFactory->create(
            [
             'data' => [
                        'id'      => 'new_video_form',
                        'class'   => 'admin__scope-old',
                        'enctype' => 'multipart/form-data',
                       ],
            ]
        );
        $form->setUseContainer($this->getUseContainer());
        $form->addField('new_video_messages', 'note', []);
        $fieldset = $form->addFieldset('new_video_form_fieldset', []);
        $fieldset->addField(
            '',
            'hidden',
            [
             'name'  => 'form_key',
             'value' => $this->getFormKey(),
            ]
        );
        $fieldset->addField(
            'item_id',
            'hidden',
            []
        );
        $fieldset->addField(
            'file_name',
            'hidden',
            []
        );
        $fieldset->addField(
            'video_provider',
            'hidden',
            ['name' => 'video_provider']
        );
        $fieldset->addField(
            'video_url',
            'text',
            [
             'class'    => 'edited-data validate-url',
             'label'    => __('Url'),
             'title'    => __('Url'),
             'required' => true,
             'name'     => 'video_url',
             'note'     => $this->getNoteVideoUrl(),
            ]
        );
        $fieldset->addField(
            'video_title',
            'text',
            [
             'class'    => 'edited-data',
             'label'    => __('Title'),
             'title'    => __('Title'),
             'required' => true,
             'name'     => 'video_title',
            ]
        );
        $fieldset->addField(
            'video_description',
            'textarea',
            [
             'class' => 'edited-data',
             'label' => __('Description'),
             'title' => __('Description'),
             'name'  => 'video_description',
            ]
        );
        $fieldset->addField(
            'new_video_screenshot',
            'file',
            [
             'label' => __('Preview Image'),
             'title' => __('Preview Image'),
             'name'  => 'image',
            ]
        );
        $fieldset->addField(
            'new_video_screenshot_preview',
            'button',
            [
             'class' => 'preview-image-hidden-input',
             'label' => '',
             'name'  => '_preview',
            ]
        );
        $fieldset->addField(
            'new_video_get',
            'button',
            [
             'label' => '',
             'title' => 'Get Video Information',
             'name'  => 'new_video_get',
             'value' => __('Get Video Information'),
             'class' => 'action-default',
            ]
        );
        $this->addMediaRoleAttributes($fieldset);
        $fieldset->addField(
            'new_video_disabled',
            'checkbox',
            [
             'class' => 'edited-data',
             'label' => __('Hide from Album Page'),
             'title' => __('Hide from Album Page'),
             'name'  => 'disabled',
            ]
        );

        $fieldset->addField(
            'categorydisabled',
            'checkbox',
            [
             'class' => 'edited-data',
             'label' => __('Hide from Category Page'),
             'title' => __('Hide from Category Page'),
             'name'  => 'categorydisabled',
            ]
        );

        $fieldset->addField(
            'productdisabled',
            'checkbox',
            [
             'class' => 'edited-data',
             'label' => __('Hide from Product Page'),
             'title' => __('Hide from Product Page'),
             'name'  => 'productdisabled',
            ]
        );

        $fieldset->addField(
            'homedisabled',
            'checkbox',
            [
             'class' => 'edited-data',
             'label' => __('Hide from Home Page'),
             'title' => __('Hide from Home Page'),
             'name'  => 'homedisabled',
            ]
        );

        $fieldset->addField(
            'shoppingcartdisabled',
            'checkbox',
            [
             'class' => 'edited-data',
             'label' => __('Hide from Shopping Cart'),
             'title' => __('Hide from Shopping Cart'),
             'name'  => 'shoppingcartdisabled',
            ]
        );

        $fieldset->addField(
            'checkoutdisabled',
            'checkbox',
            [
             'class' => 'edited-data',
             'label' => __('Hide from Checkout Page'),
             'title' => __('Hide from Checkout Page'),
             'name'  => 'checkoutdisabled',
            ]
        );
        $this->setForm($form);

    }//end _prepareForm()


    /**
     * Get html id
     *
     * @return mixed
     */
    public function getHtmlId()
    {
        if (null === $this->getData('id')) {
            $this->setData('id', $this->mathRandom->getUniqueHash('id_'));
        }

        return $this->getData('id');

    }//end getHtmlId()


    /**
     * Get widget options
     *
     * @return string
     */
    public function getWidgetOptions()
    {
        return $this->jsonEncoder->encode(
            [
             'saveVideoUrl'       => $this->getUrl('*/*/upload'),
             'saveRemoteVideoUrl' => $this->getUrl('lofgallery/album/retrieveImage'),
             'htmlId'             => $this->getHtmlId(),
             'youTubeApiKey'      => $this->mediaHelper->getYouTubeApiKey(),
             'videoSelector'      => $this->videoSelector,
            ]
        );

    }//end getWidgetOptions()


    /**
     * Retrieve currently viewed product object
     *
     * @return \Magento\Catalog\Model\Product
     */
    protected function getProduct()
    {
        if (!$this->hasData('product')) {
            $this->setData('product', $this->_coreRegistry->registry('product'));
        }

        return $this->getData('product');

    }//end getProduct()


    /**
     * Add media role attributes to fieldset
     *
     * @param  Fieldset $fieldset
     * @return $this
     */
    protected function addMediaRoleAttributes(Fieldset $fieldset)
    {
        $fieldset->addField('role-label', 'note', ['text' => __('Role')]);

        $mediaAttributes = $this->galleryContent->getMediaAttributes();

        $mediaRoles = [];
        foreach ($mediaAttributes as $attribute) {
            $mediaRoles[$attribute['code']] = $attribute['label'];
        }

        foreach ($mediaRoles as $k => $mediaRole) {
            $fieldset->addField(
                'video_'.$k,
                'checkbox',
                [
                 'class'     => 'video_image_role',
                 'label'     => $mediaRole,
                 'title'     => $mediaRole,
                 'data-role' => 'role-type-selector',
                 'value'     => $k,
                ]
            );
        }

        return $this;

    }//end addMediaRoleAttributes()


    /**
     * Get note for video url
     *
     * @return \Magento\Framework\Phrase
     */
    protected function getNoteVideoUrl()
    {
        $result = __('YouTube and Vimeo supported.');
        if ($this->mediaHelper->getYouTubeApiKey() === null) {
            $result = __(
                'Vimeo supported.<br />'.'To add YouTube video, please <a href="%1">enter YouTube API Key</a> first.',
                $this->getConfigApiKeyUrl()
            );
        }

        return $result;

    }//end getNoteVideoUrl()


    /**
     * Get url for config params
     *
     * @return string
     */
    protected function getConfigApiKeyUrl()
    {
        return $this->urlBuilder->getUrl(
            'adminhtml/system_config/edit',
            [
             'section'   => 'catalog',
             '_fragment' => self::PATH_ANCHOR_PRODUCT_VIDEO,
            ]
        );

    }//end getConfigApiKeyUrl()


}//end class
