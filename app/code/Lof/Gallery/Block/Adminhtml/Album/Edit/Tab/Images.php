<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Gallery\Block\Adminhtml\Album\Edit\Tab;

class Images extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * Gallery field name suffix
     *
     * @var string
     */
    protected $fieldNameSuffix = 'album';

    /**
     * Gallery html id
     *
     * @var string
     */
    protected $htmlId = 'media_gallery';

    /**
     * Gallery name
     *
     * @var string
     */
    protected $name = 'album[media_gallery]';

    /**
     * Html id for data scope
     *
     * @var string
     */
    protected $image = 'image';

    /**
     * @var string
     */
    protected $formName = 'album_form';

    /**
     * @var \Magento\Framework\Data\Form
     */
    protected $form;

    /**
     * @var Registry
     */
    protected $registry;


    /**
     * @param \Magento\Backend\Block\Template\Context    $context
     * @param \Magento\Framework\Registry                $registry
     * @param \Magento\Framework\Data\FormFactory        $formFactory
     * @param array                                      $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        array $data = []
    ) {
        parent::__construct($context, $registry, $formFactory, $data);
        $this->registry     = $registry;
        $this->form         = $formFactory->create();

    }//end __construct()


    /**
     * @return string
     */
    public function getElementHtml()
    {
        $html = $this->getContentHtml();
        return $html;

    }//end getElementHtml()


    /**
     * Get product images
     *
     * @return array|null
     */
    public function getImages()
    {
        $mediaGallery = $this->registry->registry('lofgallery_album')->getData('media_gallery');
        if (!empty($mediaGallery)) {
            foreach ($mediaGallery['images'] as $key => $image) {
                if (isset($image['video_description']) && $image['video_description'] != '') {
                    $mediaGallery['images'][$key]['video_description'] = base64_decode($image['video_description']);
                }

                if (isset($image['role']) && $image['role'] != '') {
                    $mediaGallery['images'][$key]['role'] = base64_decode($image['role']);
                }

                if (isset($mediaGallery['images']) && isset($mediaGallery['images'][$key]['description'])) {
                    $mediaGallery['images'][$key]['description'] = base64_decode($image['description']);
                }
            }

            return $mediaGallery;
        }

    }//end getImages()


    /**
     * Prepares content block
     *
     * @return string
     */
    public function getContentHtml()
    {
        // @var $content \Magento\Catalog\Block\Adminhtml\Product\Helper\Form\Gallery\Content
        $content = $this->getChildBlock('content');
        $content->setId($this->getHtmlId().'_content')->setElement($this);
        $content->setFormName($this->formName);
        $galleryJs = $content->getJsObjectName();
        $content->getUploader()->getConfig()->setMegiaGallery($galleryJs);
        return $content->toHtml();

    }//end getContentHtml()


    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;

    }//end getName()


    /**
     * @return string
     */
    public function getFieldNameSuffix()
    {
        return $this->fieldNameSuffix;

    }//end getFieldNameSuffix()


    /**
     * @return string
     */
    public function getDataScopeHtmlId()
    {
        return $this->image;

    }//end getDataScopeHtmlId()


    /**
     * Check "Use default" checkbox display availability
     *
     * @param  Attribute $attribute
     * @return bool
     */
    public function canDisplayUseDefault($attribute)
    {
        if (!$attribute->isScopeGlobal() && $this->getDataObject()->getStoreId()) {
            return true;
        }

        return false;

    }//end canDisplayUseDefault()


    /**
     * Check default value usage fact
     *
     * @param  Attribute $attribute
     * @return bool
     */
    public function usedDefault($attribute)
    {
        $attributeCode = $attribute->getAttributeCode();
        $defaultValue  = $this->getDataObject()->getAttributeDefaultValue($attributeCode);

        if (!$this->getDataObject()->getExistsStoreValueFlag($attributeCode)) {
            return true;
        } else if ($this->getValue() == $defaultValue
            && $this->getDataObject()->getStoreId() != $this->_getDefaultStoreId()
        ) {
            return false;
        }

        if ($defaultValue === false && !$attribute->getIsRequired() && $this->getValue()) {
            return false;
        }

        return $defaultValue === false;

    }//end usedDefault()


    /**
     * Retrieve label of attribute scope
     *
     * GLOBAL | WEBSITE | STORE
     *
     * @param  Attribute $attribute
     * @return string
     */
    public function getScopeLabel($attribute)
    {
        $html = '';
        if ($this->_storeManager->isSingleStoreMode()) {
            return $html;
        }

        if ($attribute->isScopeGlobal()) {
            $html .= __('[GLOBAL]');
        } else if ($attribute->isScopeWebsite()) {
            $html .= __('[WEBSITE]');
        } else if ($attribute->isScopeStore()) {
            $html .= __('[STORE VIEW]');
        }

        return $html;

    }//end getScopeLabel()


    /**
     * Retrieve data object related with form
     *
     * @return ProductInterface|Product
     */
    public function getDataObject()
    {
        return $this->registry->registry('current_product');

    }//end getDataObject()


    /**
     * Retrieve attribute field name
     *
     * @param  Attribute $attribute
     * @return string
     */
    public function getAttributeFieldName($attributeCode)
    {
        $name = $attributeCode;
        if ($suffix = $this->getFieldNameSuffix()) {
            $name = $this->form->addSuffixToName($name, $suffix);
        }

        return $name;

    }//end getAttributeFieldName()


    /**
     * @return string
     */
    public function toHtml()
    {
        return $this->getElementHtml();

    }//end toHtml()


    /**
     * Default sore ID getter
     *
     * @return integer
     */
    protected function _getDefaultStoreId()
    {
        return \Magento\Store\Model\Store::DEFAULT_STORE_ID;

    }//end _getDefaultStoreId()


    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Images');

    }//end getTabLabel()


    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Images');

    }//end getTabTitle()


    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;

    }//end canShowTab()


    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;

    }//end isHidden()


    /**
     * Check permission for passed action
     *
     * @param  string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);

    }//end _isAllowedAction()


}//end class
