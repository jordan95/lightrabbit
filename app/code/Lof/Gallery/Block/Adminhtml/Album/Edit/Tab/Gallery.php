<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Gallery\Block\Adminhtml\Album\Edit\Tab;

class Gallery extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var \Lof\Gallery\Model\Album
     */
    protected $albumFactory;


    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data            $backendHelper
     * @param \Lof\Gallery\Model\Album                $albumFactory
     * @param array                                   $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Lof\Gallery\Model\Album $albumFactory,
        array $data = []
    ) {
        parent::__construct($context, $backendHelper, $data);
        $this->albumFactory = $albumFactory;

    }//end __construct()


    /**
     * Set grid params
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('album_produtcs_grid');
        $this->setDefaultSort('album_id');
        $this->setUseAjax(true);

        if ($this->getRequest()->getParam('id')) {
            $this->setDefaultFilter(['in_products' => 1]);
        }

        if ($this->isReadonly()) {
            $this->setFilterVisibility(false);
        }

    }//end _construct()


    protected function _prepareCollection()
    {
        $collection = $this->albumFactory->getCollection();

        if ($this->isReadonly()) {
            $productIds = $this->_getSelectedProducts();
            if (empty($productIds)) {
                $productIds = [0];
            }

            $collection->addFieldToFilter('main_table.album_id', ['in' => $productIds]);
        }

        $this->setCollection($collection);
        return parent::_prepareCollection();

    }//end _prepareCollection()


    /**
     * Add columns to grid
     *
     * @return                                        $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'in_products',
            [
             'type'             => 'checkbox',
             'name'             => 'in_products',
             'values'           => $this->_getSelectedProducts(),
             'align'            => 'center',
             'index'            => 'album_id',
             'header_css_class' => 'col-select',
             'column_css_class' => 'col-select',
            ]
        );

        $this->addColumn(
            'galbum_id',
            [
             'header'           => __('ID'),
             'sortable'         => true,
             'index'            => 'album_id',
             'header_css_class' => 'col-id',
             'column_css_class' => 'col-id',
            ]
        );
        $this->addColumn(
            'gtitle',
            [
             'header'           => __('Title'),
             'index'            => 'title',
             'header_css_class' => 'col-name',
             'column_css_class' => 'col-name',
            ]
        );

        $this->addColumn(
            'gimage',
            [
             'header'   => __('Images'),
             'type'     => 'action',
             'renderer' => 'Lof\Gallery\Block\Adminhtml\Album\Renderer\AlbumImage',
            ]
        );

        $this->addColumn(
            'gimage_url',
            [
             'header'   => __('Image Url'),
             'type'     => 'action',
             'renderer' => 'Lof\Gallery\Block\Adminhtml\Album\Renderer\Image',
            ]
        );

        $this->addColumn(
            'gis_active',
            [
             'header'           => __('Active'),
             'index'            => 'is_active',
             'type'             => 'options',
             'options'          => $this->_status->getOptionArray(),
             'header_css_class' => 'col-status',
             'column_css_class' => 'col-status',
            ]
        );

        $this->addColumn(
            'position',
            [
             'header'                    => __('Position'),
             'name'                      => 'position',
             'type'                      => 'number',
             'validate_class'            => 'validate-number',
             'index'                     => 'position',
             'header_css_class'          => 'col-position',
             'column_css_class'          => 'col-position',
             'editable'                  => true,
             'edit_only'                 => true,
             'sortable'                  => false,
             'filter_condition_callback' => [
                                             $this,
                                             'filterProductPosition',
                                            ],
            ]
        );

        $this->addColumn(
            'gaction',
            [
             'header'   => __('Action'),
             'type'     => 'action',
             'renderer' => 'Lof\Gallery\Block\Adminhtml\Album\Renderer\AlbumAction',
            ]
        );

        return parent::_prepareColumns();

    }//end _prepareColumns()


    /**
     * Add filter
     *
     * @param  object $column
     * @return $this
     */
    protected function _addColumnFilterToCollection($column)
    {
        // Set custom filter for in product flag
        if ($column->getId() == 'in_products') {
            $albumIds = $this->_getSelectedProducts();
            if (empty($albumIds)) {
                $albumIds = 0;
            }

            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('main_table.album_id', ['in' => $albumIds]);
            } else {
                if ($productIds) {
                    $this->getCollection()->addFieldToFilter('main_table.album_id', ['nin' => $albumIds]);
                }
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }

        return $this;

    }//end _addColumnFilterToCollection()


    /**
     * Checks when this block is readonly
     *
     * @return boolean
     */
    public function isReadonly()
    {
        return false;

    }//end isReadonly()


    /**
     * Prepare collection
     *
     * @return \Magento\Backend\Block\Widget\Grid\Extended
     */


    protected function _getSelectedProducts()
    {

        $productsalbum = $this->getProductsAlbum();
        if (!is_array($productsalbum)) {
            $productsalbum = array_keys($this->getSelectedAlbum());
        }

        return $productsalbum;

    }//end _getSelectedProducts()


    public function getSelectedAlbum()
    {
        $productsalbum = [];
        $collection    = $this->albumFactory->getCollection();
        $productId     = $this->getRequest()->getParam('id');

        if($productId) {
            $album = $collection->addAlbumToFilter($productId);
            if(!empty($album)) {
                foreach ($album as $_album) {
                    $productsalbum[$_album['album_id']] = ['position' => $_album['position']];
                }
            }
        }

        return $productsalbum;

    }//end getSelectedAlbum()


        /**
         * Rerieve grid URL
         *
         * @return string
         */
    public function getGridUrl()
    {
        return $this->_getData(
            'grid_url'
        ) ? $this->_getData(
            'grid_url'
        ) : $this->getUrl(
            '*/*/galleryGrid/id/'.$this->getRequest()->getParam('id'),
            ['_current' => true]
        );

    }//end getGridUrl()


    /**
     * Apply `position` filter to cross-sell grid.
     *
     * @param  \Magento\Catalog\Model\ResourceModel\Product\Link\Product\Collection $collection
     * @param  \Magento\Backend\Block\Widget\Grid\Column\Extended                   $column
     * @return $this
     */
    public function filterProductPosition($collection, $column)
    {
        $condition = $column->getFilter()->getCondition();
        $condition['entity_id'] = $this->getRequest()->getParam('id');
        $collection->addLinkAttributeToFilterProducts($column->getIndex(), $condition);
        return $this;

    }//end filterProductPosition()


}//end class
