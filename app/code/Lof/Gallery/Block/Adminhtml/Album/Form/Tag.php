<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Gallery\Block\Adminhtml\Album\Form;

class Tag extends \Magento\Framework\Data\Form\Element\AbstractElement
{
    /**
     * @var \Magento\Framework\View\LayoutInterface
     */
    protected $_layout;

    /**
     * @var \Lof\Gallery\Model\ResourceModel\Tag\CollectionFactory
     */
    protected $tagCollectionFactory;


    /**
     * @param \Magento\Framework\Data\Form\Element\Factory           $factoryElement
     * @param \Magento\Framework\Data\Form\Element\CollectionFactory $factoryCollection
     * @param \Magento\Framework\Escaper                             $escaper
     * @param \Magento\Framework\View\LayoutInterface                $layout
     * @param \Lof\Gallery\Model\ResourceModel\Tag\CollectionFactory $tagCollectionFactory
     * @param array                                                  $data
     */
    public function __construct(
        \Magento\Framework\Data\Form\Element\Factory $factoryElement,
        \Magento\Framework\Data\Form\Element\CollectionFactory $factoryCollection,
        \Magento\Framework\Escaper $escaper,
        \Magento\Framework\View\LayoutInterface $layout,
        \Lof\Gallery\Model\ResourceModel\Tag\CollectionFactory $tagCollectionFactory,
        $data = []
    ) {
        parent::__construct($factoryElement, $factoryCollection, $escaper, $data);
        $this->_layout = $layout;
        $this->tagCollectionFactory = $tagCollectionFactory;

    }//end __construct()


    public function getElementHtml()
    {
        $tags = $this->tagCollectionFactory->create();
        $html = $this->_layout->createBlock('\Magento\Backend\Block\Template')->setTags($tags)->setTemplate('Lof_Gallery::album/form/tag.phtml')->setElement($this)->setData('options', $this->getCategoriesTree())->toHtml();
        return $html;

    }//end getElementHtml()


}//end class
