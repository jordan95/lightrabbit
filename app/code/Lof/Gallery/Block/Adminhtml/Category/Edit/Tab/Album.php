<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Gallery\Block\Adminhtml\Category\Edit\Tab;

class Album extends \Magento\Backend\Block\Widget\Grid\Extended
{

    /**
     * @var \Magento\Catalog\Model\Product\Attribute\Source\Status
     */
    protected $status;

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var \Lof\Gallery\Model\Album
     */
    protected $album;


    /**
     * @param \Magento\Backend\Block\Template\Context                $context
     * @param \Magento\Backend\Helper\Data                           $backendHelper
     * @param \Magento\Catalog\Model\Product\Attribute\Source\Status $status
     * @param \Magento\Framework\Registry                            $coreRegistry
     * @param \Lof\Gallery\Model\Album                               $album
     * @param array                                                  $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Catalog\Model\Product\Attribute\Source\Status $status,
        \Magento\Framework\Registry $coreRegistry,
        \Lof\Gallery\Model\Album $album,
        array $data = []
    ) {
        $this->_coreRegistry = $coreRegistry;
        $this->status        = $status;
        $this->album         = $album;
        parent::__construct($context, $backendHelper, $data);

    }//end __construct()


    /**
     * Set grid params
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('album_content_grid');
        $this->setDefaultSort('album_id');
        $this->setUseAjax(true);
        if ($this->getCategory() && $this->getCategory()->getCategoryId()) {
            $this->setDefaultFilter(['in_products' => 1]);
        }

        if ($this->isReadonly()) {
            $this->setFilterVisibility(false);
        }

    }//end _construct()


    protected function _prepareCollection()
    {
        $collection = $this->album->getCollection();
        if ($this->isReadonly()) {
            $productIds = $this->_getSelectedProducts();
            if (empty($productIds)) {
                $productIds = [0];
            }

            $collection->addFieldToFilter('main_table.album_id', ['in' => $productIds]);
            $collection->joinCategoryRelationTable();
            $collection->getSelect()->order("album_category_table.position ASC");
        }

        $category = $this->getCategory();
        $this->setCollection($collection);
        return parent::_prepareCollection();

    }//end _prepareCollection()


    /**
     * Add columns to grid
     *
     * @return                                        $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareColumns()
    {

        $this->addColumn(
            'in_products',
            [
             'type'             => 'checkbox',
             'name'             => 'in_products',
             'values'           => $this->_getSelectedProducts(),
             'align'            => 'center',
             'index'            => 'album_id',
             'header_css_class' => 'col-select',
             'column_css_class' => 'col-select',
            ]
        );

        $this->addColumn(
            'galbum_id',
            [
             'header'           => __('ID'),
             'sortable'         => true,
             'index'            => 'album_id',
             'header_css_class' => 'col-id',
             'column_css_class' => 'col-id',
             'style'            => 'width: 100px;',
            ]
        );

        $this->addColumn(
            'gtitle',
            [
             'header'           => __('Name'),
             'index'            => 'name',
             'header_css_class' => 'col-name',
             'column_css_class' => 'col-name',
            ]
        );

        $this->addColumn(
            'gimage',
            [
             'header'   => __('Thumbnail'),
             'type'     => 'action',
             'renderer' => 'Lof\Gallery\Block\Adminhtml\Album\Renderer\AlbumImage',
            ]
        );

        $this->addColumn(
            'gis_active',
            [
             'header'           => __('Active'),
             'index'            => 'is_active',
             'type'             => 'options',
             'options'          => $this->status->getOptionArray(),
             'header_css_class' => 'col-status',
             'column_css_class' => 'col-status',
            ]
        );

        $this->addColumn(
            'position',
            [
             'header'                    => __('Position'),
             'name'                      => 'position',
             'type'                      => 'number',
             'validate_class'            => 'validate-number',
             'index'                     => 'position',
             'header_css_class'          => 'col-position',
             'column_css_class'          => 'col-position',
             'style'                     => 'width: 100px;',
             'editable'                  => true,
             'edit_only'                 => true,
             'sortable'                  => false,
             'filter_condition_callback' => [
                                             $this,
                                             'filterProductPosition',
                                            ],
            ]
        );

        $this->addColumn(
            'gaction',
            [
             'header'   => __('Action'),
             'type'     => 'action',
             'renderer' => 'Lof\Gallery\Block\Adminhtml\Album\Renderer\AlbumAction',
            ]
        );

        return parent::_prepareColumns();

    }//end _prepareColumns()


    /**
     * Retirve currently edited product model
     *
     * @return \Magento\Catalog\Model\Product
     */
    public function getCategory()
    {
        return $this->_coreRegistry->registry('current_category');

    }//end getCategory()


    /**
     * Add filter
     *
     * @param  object $column
     * @return $this
     */
    protected function _addColumnFilterToCollection($column)
    {

        // Set custom filter for in product flag
        if ($column->getId() == 'in_products') {
            $productIds = $this->_getSelectedProducts();
            if (empty($productIds)) {
                $productIds = 0;
            }

            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('main_table.album_id', ['in' => $productIds]);
            } else {
                if ($productIds) {
                    $this->getCollection()->addFieldToFilter('main_table.album_id', ['nin' => $productIds]);
                }
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }

        return $this;

    }//end _addColumnFilterToCollection()


    /**
     * Checks when this block is readonly
     *
     * @return boolean
     */
    public function isReadonly()
    {
        return false;

    }//end isReadonly()


    /**
     * Rerieve grid URL
     *
     * @return string
     */
    public function getGridUrl()
    {
        $category = $this->getCategory();
        return $this->_getData(
            'grid_url'
        ) ? $this->_getData(
            'grid_url'
        ) : $this->getUrl(
            'lofgallery/*/albumGrid/category_id/'.$category->getCategoryId(),
            ['_current' => true]
        );

    }//end getGridUrl()


    protected function _getSelectedProducts()
    {
        $products = $this->getProductsUpsell();

        if (!is_array($products)) {
            $products = array_keys($this->getSelectedAlbum());
        }

        return $products;

    }//end _getSelectedProducts()


    public function getSelectedAlbum()
    {
        $products = [];
        $category = $this->getCategory();
        if($category) {
            $albums = $category->getAlbums();
            if($albums->count()) {
                foreach ($albums as $album) {
                    $products[$album->getId()] = ['position' => $album->getPosition()];
                }
            }
        }

        return $products;

    }//end getSelectedAlbum()


    /**
     * Apply `position` filter to cross-sell grid.
     *
     * @param  \Magento\Catalog\Model\ResourceModel\Product\Link\Product\Collection $collection
     * @param  \Magento\Backend\Block\Widget\Grid\Column\Extended                   $column
     * @return $this
     */
    public function filterProductPosition($collection, $column)
    {
        $condition = $column->getFilter()->getCondition();
        $category  = $this->getCategory();
        $condition['category_id'] = $category->getCategoryId();
        $collection->addLinkAttributeToFilter($column->getIndex(), $condition);
        return $this;

    }//end filterProductPosition()


}//end class
