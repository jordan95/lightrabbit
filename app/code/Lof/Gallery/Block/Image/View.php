<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Gallery\Block\Image;

class View extends \Magento\Framework\View\Element\Template
{

    /**
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry;

    /**
     * @var \Lof\Gallery\Model\Config
     */
    protected $galleryConfig;


    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry                      $registry
     * @param \Lof\Gallery\Model\Config                        $galleryConfig
     * @param array                                            $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Lof\Gallery\Model\Config $galleryConfig,
        array $data = []
    ) {
        parent::__construct($context);
        $this->coreRegistry  = $registry;
        $this->galleryConfig = $galleryConfig;

    }//end __construct()


    public function getImage()
    {
        $album = $this->coreRegistry->registry('album_image');
        return $album;

    }//end getImage()


    public function getAlbum()
    {
        $album = $this->coreRegistry->registry('gallery_album');
        return $album;

    }//end getAlbum()


    public function getConfig($key)
    {
        return $this->galleryConfig->getConfig($key);

    }//end getConfig()


    protected function _addBreadcrumbs()
    {
        $breadcrumbBlock = $this->getLayout()->getBlock('breadcrumbs');
        $showBreadcrumb  = $this->getConfig('album_page/show_breadcrumb');

        if($breadcrumbBlock) {
            $baseUrl = $this->_storeManager->getStore()->getBaseUrl();
            $breadcrumbBlock->addCrumb(
                'home',
                [
                 'label' => __('Home'),
                 'title' => __('Go to Home Page'),
                 'link'  => $baseUrl,
                ]
            );

            $breadcrumbBlock->addCrumb(
                'gallerypage',
                [
                 'label' => __('Gallery'),
                 'title' => __('Gallery'),
                 'link'  => $this->galleryConfig->getGalleryPageUrl(),
                ]
            );

            $album = $this->getAlbum();
            if($album) {
                $breadcrumbBlock->addCrumb(
                    'album',
                    [
                     'label' => $album->getName(),
                     'title' => $album->getName(),
                     'link'  => $album->getAlbumLink(),
                    ]
                );
            }

            if($image = $this->getImage()){
                $breadcrumbBlock->addCrumb(
                    'image',
                    [
                     'label' => $image['video_title'] ? $image['video_title'] : $image['title'],
                     'title' => $image['video_title'] ? $image['video_title'] : $image['title'],
                     'link'  => '',
                    ]
                );
            }
        }//end if

    }//end _addBreadcrumbs()


    protected function _prepareLayout()
    {
        $this->_addBreadcrumbs();
        $image = $this->getImage();
        $this->pageConfig->addBodyClass('gallery-album-image');
        $pageTitle = $image['video_title'] ? $image['video_title'] : $image['title'];
        if($pageTitle) {
            $this->pageConfig->getTitle()->set($pageTitle);
        }

        return parent::_prepareLayout();

    }//end _prepareLayout()


}//end class
