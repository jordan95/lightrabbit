<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Gallery\Block\Post;

use Lof\Gallery\Model\Config;

class View extends \Magento\Framework\View\Element\Template
{


    /**
     * \Lof\Gallery\Model\Album
     *
     * @var [type]
     */
    protected $_albumFactory;
    /**
     * @var \Magento\Framework\Registry
     */
    protected $_registry;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    protected $_resource;

    /**
     * @var \Lof\Gallery\Model\Config
     */
    protected $galleryConfig;


    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry                      $registry
     * @param \Magento\Framework\App\ResourceConnection        $resource
     * @param \Lof\Gallery\Model\Album                         $albumFactory
     * @param \Lof\Gallery\Model\Config                        $galleryConfig
     * @param array                                            $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\ResourceConnection $resource,
        \Lof\Gallery\Model\Album $albumFactory,
        \Lof\Gallery\Model\Config $galleryConfig,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_registry     = $registry;
        $this->_resource     = $resource;
        $this->_albumFactory = $albumFactory;
        $this->galleryConfig = $galleryConfig;

    }//end __construct()


    public function _toHtml()
    {
        if ($this->getConfig('general_settings/enable') && $this->getConfig('product_page/enable')) {
            return parent::_toHtml();
        }

        return;

    }//end _toHtml()


    /**
     * Get product object
     *
     * @return \Magento\Catalog\Model\Product
     */
    public function getProduct()
    {
        return $this->_registry->registry('current_post');

    }//end getProduct()


    protected function _beforeToHtml()
    {
        $enable = $this->galleryConfig->getConfig('general_settings/enable');
        if (!$enable) {
            return;
        }

        $itemPerPage = $this->getConfig('product_page/number_item');
        $productId   = $this->getProduct()->getId();

        $store      = $this->_storeManager->getStore();
        $collection = $this->_albumFactory
            ->getCollection()
            ->addStoreFilter($store)
            ->addFieldToFilter("is_active", 1);

        if ($itemPerPage) {
            $collection->setPagesize($itemPerPage);
        }

        $collection->getSelect()
            ->joinLeft(
                [
                 'lg' => $this->_resource->getTableName(Config::TABLE_GALLERY_ALBUM_POST),
                ],
                'lg.album_id = main_table.album_id',
                [
                 'album_id' => 'album_id',
                 'position' => 'position',
                ]
            )
            ->where('lg.post_id = (?)', $productId)
            ->order('lg.position ASC')
            ->group('main_table.album_id');

        $this->setCollection($collection);
        return parent::_beforeToHtml();

    }//end _beforeToHtml()


    /**
     * @param AbstractCollection $collection
     * @return $this
     */
    public function setCollection($collection)
    {
        $this->_albumCollection = $collection;
        return $this;

    }//end setCollection()


    public function getCollection()
    {
        return $this->_albumCollection;

    }//end getCollection()

    public function getConfig($key, $default = '')
    {
        $c = explode("/", $key);
        if (count($c) == 2 && $this->hasData($c[1])) {
            return $this->getData($c[1]);
        }

        if ($this->hasData($key)) {
            return $this->getData($key);
        }

        if ($val = $this->galleryConfig->getConfig($key)) {
            return $val;
        }

        return $default;

    }//end getConfig()


}//end class
