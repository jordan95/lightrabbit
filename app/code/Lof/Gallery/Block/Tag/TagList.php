<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Gallery\Block\Tag;

class TagList extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry;

    /**
     * @var \Lof\Gallery\Model\ResourceModel\Tag\CollectionFactory
     */
    protected $tagCollectionFactory;

    /**
     * @var \Lof\Gallery\Model\Config
     */
    protected $galleryConfig;


    /**
     * @param \Magento\Framework\View\Element\Template\Context       $context
     * @param \Magento\Framework\Registry                            $registry
     * @param \Lof\Gallery\Model\ResourceModel\Tag\CollectionFactory $tagCollectionFactory
     * @param \Lof\Gallery\Model\Config                              $galleryConfig
     * @param array                                                  $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Lof\Gallery\Model\ResourceModel\Tag\CollectionFactory $tagCollectionFactory,
        \Lof\Gallery\Model\Config $galleryConfig,
        array $data = []
    ) {
        parent::__construct($context);
        $this->coreRegistry         = $registry;
        $this->tagCollectionFactory = $tagCollectionFactory;
        $this->galleryConfig        = $galleryConfig;

    }//end __construct()


    public function getCurrentTag()
    {
        return $this->coreRegistry->registry('gallery_tag');

    }//end getCurrentTag()


    public function getTags()
    {
        $collection = $this->tagCollectionFactory->create()->addFieldToFilter('is_active', \Lof\Gallery\Model\Tag::STATUS_ENABLED);
        return $collection;

    }//end getTags()


    public function getConfig($key)
    {
        return $this->galleryConfig->getConfig($key);

    }//end getConfig()


}//end class
