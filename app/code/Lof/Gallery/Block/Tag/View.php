<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Gallery\Block\Tag;

use Lof\Gallery\Model\Config;

class View extends \Magento\Framework\View\Element\Template
{
    protected $_collection;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry;

    /**
     * @var \Lof\Gallery\Model\Config
     */
    protected $galleryConfig;


    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry                      $registry
     * @param \Lof\Gallery\Model\Config                        $galleryConfig
     * @param array                                            $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Lof\Gallery\Model\Config $galleryConfig,
        array $data = []
    ) {
        parent::__construct($context);
        $this->coreRegistry  = $registry;
        $this->galleryConfig = $galleryConfig;

    }//end __construct()


    public function setCollection($collection)
    {
        $this->_collection = $collection;
        return $this->_collection;

    }//end setCollection()


    public function getCollection()
    {
        return $this->_collection;

    }//end getCollection()


    /**
     * Prepare breadcrumbs
     *
     * @param  \Magento\Cms\Model\Page $brand
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return void
     */
    protected function _addBreadcrumbs()
    {
        $breadcrumbsBlock = $this->getLayout()->getBlock('breadcrumbs');
        $baseUrl          = $this->_storeManager->getStore()->getBaseUrl();
        $route            = $this->getConfig('general_settings/route');
        $showBreadcrumbBlock = $this->getConfig('tag_page/show_breadcrumbblock');

        if ($showBreadcrumbBlock && $breadcrumbsBlock) {
            $breadcrumbsBlock->addCrumb(
                'home',
                [
                 'label' => __('Home'),
                 'title' => __('Go to Home Page'),
                 'link'  => $baseUrl,
                ]
            );

            if ($route) {
                $route = ucfirst(str_replace("-", " ", $route));
                $breadcrumbsBlock->addCrumb(
                    'lofgallery',
                    [
                     'label' => $route,
                     'title' => $route,
                     'link'  => $this->galleryConfig->getGalleryPageUrl(),
                    ]
                );
            }

            $breadcrumbsBlock->addCrumb(
                'tag',
                [
                 'label' => __('Tag'),
                 'title' => __('Tag'),
                 'link'  => '',
                ]
            );

            $tag = $this->getTag();
            $breadcrumbsBlock->addCrumb(
                'item',
                [
                 'label' => $tag->getName(),
                 'title' => $tag->getName(),
                 'link'  => '',
                ]
            );
        }//end if

    }//end _addBreadcrumbs()


    public function getConfig($key)
    {
        return $this->galleryConfig->getConfig($key);

    }//end getConfig()


    /**
     * @return string
     */
    public function getAlbumListHtml($layout_type = '', $lg_column = '', $md_column = '', $sm_column = '', $xs_column = '', $lightbox = '')
    {
        $collection = $this->getCollection();
        $block      = $this->getLayout()->getBlock('gallery.albums.list');
        if ($layout_type) {
            $block->setData('layout_type', $layout_type);
        }

        if ($lg_column) {
            $block->setData('lg_column', $lg_column);
        }

        if ($md_column) {
            $block->setData('md_column', $md_column);
        }

        if ($sm_column) {
            $block->setData('sm_column', $sm_column);
        }

        if ($xs_column) {
            $block->setData('xs_column', $xs_column);
        }

        if ($lightbox) {
            $block->setData('lightbox', $lightbox);
        }

        $block->setCollection($collection);
        return $block->toHtml();

    }//end getAlbumListHtml()


    /**
     * Need use as _prepareLayout - but problem in declaring collection from
     * another block (was problem with search result)
     *
     * @return $this
     */
    protected function _beforeToHtml()
    {
        $itemPerPage = $this->getConfig('tag_page/items_per_page');
        $tag         = $this->getTag();
        $store      = $this->_storeManager->getStore();
        $collection  = $tag->getAlbums($store);

        $this->setCollection($collection);
        $toolbar = $this->getToolbarBlock();
        if ($toolbar && $itemPerPage) {
            $toolbar->setData('_current_limit', $itemPerPage)->setCollection($collection);
            $this->setChild('toolbar', $toolbar);
        }

        return parent::_beforeToHtml();

    }//end _beforeToHtml()


    /**
     * Prepare global layout
     *
     * @return $this
     */
    protected function _prepareLayout()
    {
        $this->_addBreadcrumbs();
        $this->pageConfig->addBodyClass('gallery-tag');

        $tag       = $this->getTag();
        $pageTitle = $tag->getPageTitle();
        if ($pageTitle == '') {
            $pageTitle = ucfirst(str_replace("-", " ", $tag->getName()));
        }

        if($pageTitle) {
            $this->pageConfig->getTitle()->set('#'.$pageTitle);
        }

        if($metaKeywords = $tag->getMetaKeywords()) {
            $this->pageConfig->setKeywords($metaKeywords);
        }

        if($metaDescription = $tag->getMetaDescription()) {
            $this->pageConfig->setDescription($metaDescription);
        }

        $pageMainTitle = $this->getLayout()->getBlock('page.main.title');
        if ($pageMainTitle) {
            $pageMainTitle->setPageTitle(ucfirst(str_replace("-", " ", '#'.$tag->getName())));
        }

        return parent::_prepareLayout();

    }//end _prepareLayout()


    /**
     * Retrieve Toolbar block
     *
     * @return \Magento\Catalog\Block\Product\ProductList\Toolbar
     */
    public function getToolbarBlock()
    {
        $block = $this->getLayout()->getBlock('gallery_toolbar');
        return $block;

    }//end getToolbarBlock()


    public function getTag()
    {
        $album = $this->coreRegistry->registry('gallery_tag');
        return $album;

    }//end getTag()


}//end class
