<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Gallery\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;
use Lof\Gallery\Model\Config;

class InstallSchema implements InstallSchemaInterface
{


    /**
     * Install table
     *
     * @param  \Magento\Framework\Setup\SchemaSetupInterface   $setup
     * @param  \Magento\Framework\Setup\ModuleContextInterface $context
     * @throws \Zend_Db_Exception
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        /*
            * Create table TABLE_GALLERY_CATEGORY
         */
        $setup->getConnection()->dropTable($setup->getTable(Config::TABLE_GALLERY_CATEGORY));
        $table = $installer->getConnection()->newTable(
            $installer->getTable(Config::TABLE_GALLERY_CATEGORY)
        )->addColumn(
            'category_id',
            Table::TYPE_SMALLINT,
            null,
            [
             'identity' => true,
             'nullable' => false,
             'primary'  => true,
            ],
            'Category Id'
        )->addColumn(
            'name',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Category Name'
        )->addColumn(
            'identifier',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Identifier'
        )->addColumn(
            'page_layout',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Page Layout'
        )->addColumn(
            'layout_type',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Layout Type'
        )->addColumn(
            'is_active',
            Table::TYPE_SMALLINT,
            null,
            ['nullable' => false],
            'Active'
        )->addColumn(
            'items_per_page',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false],
            'Items Per Page'
        )->addColumn(
            'lg_column',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false],
            'Large Desktop (≥1200px)'
        )->addColumn(
            'md_column',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false],
            'Medium devices Desktops (≥992px)'
        )->addColumn(
            'sm_column',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false],
            'Small devices Tablets (≥768px)'
        )->addColumn(
            'xs_column',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false],
            'Phone < 768px'
        )->addColumn(
            'cat_position',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false],
            'Cat Position'
        )->addColumn(
            'parent_id',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false],
            'Parent Id'
        )->addColumn(
            'page_title',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Meta Title'
        )->addColumn(
            'lightbox',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Lightbox'
        )->addColumn(
            'layout_update_xml',
            Table::TYPE_TEXT,
            '2M',
            [],
            'Layout Update Xml'
        )->addColumn(
            'meta_keywords',
            Table::TYPE_TEXT,
            '2M',
            [],
            'Meta Keywords'
        )->addColumn(
            'meta_description',
            Table::TYPE_TEXT,
            '2M',
            [],
            'Meta Description'
        )->addColumn(
            'description',
            Table::TYPE_TEXT,
            '2M',
            [],
            'Description'
        )->addColumn(
            'show_toptoolbar',
            Table::TYPE_SMALLINT,
            null,
            ['nullable' => false],
            'Show Top Toolbar'
        )->addColumn(
            'show_bottomtoolbar',
            Table::TYPE_SMALLINT,
            null,
            ['nullable' => false],
            'Show Bottom Toolbar'
        )->setComment(
            'Album Category Table'
        );
        $installer->getConnection()->createTable($table);

        /*
            * Create table TABLE_GALLERY_CATEGORY_STORE
         */
        $setup->getConnection()->dropTable($setup->getTable(Config::TABLE_GALLERY_CATEGORY_STORE));
        $table = $installer->getConnection()->newTable(
            $installer->getTable(Config::TABLE_GALLERY_CATEGORY_STORE)
        )->addColumn(
            'entity_id',
            Table::TYPE_SMALLINT,
            null,
            [
             'nullable' => false,
             'primary'  => true,
            ],
            'Category Id'
        )->addColumn(
            'store_id',
            Table::TYPE_SMALLINT,
            null,
            [
             'unsigned' => true,
             'nullable' => false,
             'primary'  => true,
            ],
            'Store ID'
        )->addIndex(
            $installer->getIdxName(Config::TABLE_GALLERY_CATEGORY_STORE, ['entity_id']),
            ['entity_id']
        )->addForeignKey(
            $installer->getFkName(Config::TABLE_GALLERY_CATEGORY_STORE, 'entity_id', Config::TABLE_GALLERY_CATEGORY, 'category_id'),
            'entity_id',
            $installer->getTable(Config::TABLE_GALLERY_CATEGORY),
            'category_id',
            Table::ACTION_CASCADE
        )->addForeignKey(
            $installer->getFkName(Config::TABLE_GALLERY_CATEGORY_STORE, 'store_id', 'store', 'store_id'),
            'store_id',
            $installer->getTable('store'),
            'store_id',
            Table::ACTION_CASCADE
        )->setComment(
            'Category Store'
        );
        $installer->getConnection()->createTable($table);

        /*
            * Create table TABLE_GALLERY_CATEGORY_ALBUM
         */
        $setup->getConnection()->dropTable($setup->getTable(Config::TABLE_GALLERY_ALBUM));
        $table = $installer->getConnection()->newTable(
            $installer->getTable(Config::TABLE_GALLERY_ALBUM)
        )->addColumn(
            'album_id',
            Table::TYPE_SMALLINT,
            null,
            [
             'identity' => true,
             'nullable' => false,
             'primary'  => true,
            ],
            'Album ID'
        )->addColumn(
            'name',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Album Name'
        )->addColumn(
            'description',
            Table::TYPE_TEXT,
            '2M',
            ['nullable' => false],
            'Description'
        )->addColumn(
            'created_at',
            Table::TYPE_DATE,
            null,
            ['nullable' => true],
            'Created At'
        )->addColumn(
            'is_active',
            Table::TYPE_SMALLINT,
            null,
            ['nullable' => false],
            'Active'
        )->addColumn(
            'page_title',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Page Title'
        )->addColumn(
            'meta_keywords',
            Table::TYPE_TEXT,
            '2M',
            [],
            'Meta Keywords'
        )->addColumn(
            'meta_description',
            Table::TYPE_TEXT,
            '2M',
            [],
            'Meta Description'
        )->addColumn(
            'description',
            Table::TYPE_TEXT,
            '2M',
            [],
            'Description'
        )->addColumn(
            'identifier',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Identifier'
        )->addColumn(
            'media_gallery',
            Table::TYPE_TEXT,
            '2M',
            [],
            'Media Gallery'
        )->addColumn(
            'page_layout',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Page Layout'
        )->addColumn(
            'layout_update_xml',
            Table::TYPE_TEXT,
            '2M',
            [],
            'Layout Update Xml'
        )->addColumn(
            'store_categories',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Store Categories'
        )->addColumn(
            'categories',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Categories'
        )->addColumn(
            'page_type',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Page Type'
        )->addColumn(
            'hits',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false],
            'Hits'
        )->addColumn(
            'enable_comment',
            Table::TYPE_SMALLINT,
            null,
            ['nullable' => false],
            'Enable Comment'
        )->addColumn(
            'layout_type',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Layout Type'
        )->addColumn(
            'items_per_page',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false],
            'Items Per Page'
        )->addColumn(
            'lightbox',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Lightbox'
        )->addColumn(
            'lg_column',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false],
            'Large Desktop (≥1200px)'
        )->addColumn(
            'md_column',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false],
            'Medium devices Desktops (≥992px)'
        )->addColumn(
            'sm_column',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false],
            'Small devices Tablets (≥768px)'
        )->addColumn(
            'xs_column',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false],
            'Phone < 768px'
        )->addColumn(
            'show_toptoolbar',
            Table::TYPE_SMALLINT,
            null,
            ['nullable' => false],
            'Show Top Toolbar'
        )->addColumn(
            'show_bottomtoolbar',
            Table::TYPE_SMALLINT,
            null,
            ['nullable' => false],
            'Show Bottom Toolbar'
        )->setComment(
            'Lof Gallery Album'
        );
        $installer->getConnection()->createTable($table);

        /*
            * Create table Config::TABLE_GALLERY_ALBUM_CATEGORY
         */
        $setup->getConnection()->dropTable($setup->getTable(Config::TABLE_GALLERY_ALBUM_CATEGORY));
        $table = $installer->getConnection()->newTable(
            $installer->getTable(Config::TABLE_GALLERY_ALBUM_CATEGORY)
        )->addColumn(
            'album_id',
            Table::TYPE_SMALLINT,
            null,
            [
             'nullable' => false,
             'primary'  => true,
            ],
            'Album Id'
        )->addColumn(
            'entity_id',
            Table::TYPE_SMALLINT,
            null,
            [
             'nullable' => false,
             'primary'  => true,
            ],
            'Category id'
        )->addColumn(
            'position',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Position'
        )->addIndex(
            $installer->getIdxName(Config::TABLE_GALLERY_ALBUM_CATEGORY, ['entity_id']),
            ['entity_id']
        )->addForeignKey(
            $installer->getFkName(Config::TABLE_GALLERY_ALBUM_CATEGORY, 'album_id', Config::TABLE_GALLERY_ALBUM, 'album_id'),
            'album_id',
            $installer->getTable(Config::TABLE_GALLERY_ALBUM),
            'album_id',
            Table::ACTION_CASCADE
        )->addForeignKey(
            $installer->getFkName(Config::TABLE_GALLERY_ALBUM_CATEGORY, 'entity_id', Config::TABLE_GALLERY_CATEGORY, 'category_id'),
            'entity_id',
            $installer->getTable(Config::TABLE_GALLERY_CATEGORY),
            'category_id',
            Table::ACTION_CASCADE
        )->setComment(
            'Album Category Table'
        );
        $installer->getConnection()->createTable($table);

        /*
            * Create table TABLE_GALLERY_ALBUM_PRODUCT
         */
        $setup->getConnection()->dropTable($setup->getTable(Config::TABLE_GALLERY_ALBUM_PRODUCT));
        $table = $installer->getConnection()->newTable(
            $installer->getTable(Config::TABLE_GALLERY_ALBUM_PRODUCT)
        )->addColumn(
            'album_id',
            Table::TYPE_SMALLINT,
            null,
            [
             'nullable' => false,
             'primary'  => true,
            ],
            'Album Id'
        )->addColumn(
            'entity_id',
            Table::TYPE_INTEGER,
            null,
            [
             'unsigned' => true,
             'nullable' => false,
             'primary'  => true,
            ],
            'Product id'
        )->addColumn(
            'position',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Position'
        )->addIndex(
            $installer->getIdxName(Config::TABLE_GALLERY_ALBUM_PRODUCT, ['album_id']),
            ['album_id']
        )->addForeignKey(
            $installer->getFkName(Config::TABLE_GALLERY_ALBUM_PRODUCT, 'album_id', Config::TABLE_GALLERY_ALBUM, 'album_id'),
            'album_id',
            $installer->getTable(Config::TABLE_GALLERY_ALBUM),
            'album_id',
            Table::ACTION_CASCADE
        )->setComment(
            'Album Product Table'
        )->addForeignKey(
            $installer->getFkName(Config::TABLE_GALLERY_ALBUM_PRODUCT, 'entity_id', 'catalog_product_entity', 'entity_id'),
            'entity_id',
            $installer->getTable('catalog_product_entity'),
            'entity_id',
            Table::ACTION_CASCADE
        );
        $installer->getConnection()->createTable($table);

        /*
            * Create table TABLE_GALLERY_TAG
         */
        $setup->getConnection()->dropTable($setup->getTable(Config::TABLE_GALLERY_TAG));
        $table = $installer->getConnection()->newTable(
            $installer->getTable(Config::TABLE_GALLERY_TAG)
        )->addColumn(
            'tag_id',
            Table::TYPE_SMALLINT,
            null,
            [
             'identity' => true,
             'nullable' => false,
             'primary'  => true,
            ],
            'Tag ID'
        )->addColumn(
            'name',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Tag Name'
        )->addColumn(
            'alias',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Alias'
        )->addColumn(
            'tag_position',
            Table::TYPE_INTEGER,
            255,
            ['nullable' => false],
            'Position'
        )->addColumn(
            'is_active',
            Table::TYPE_SMALLINT,
            null,
            ['nullable' => false],
            'Active'
        )->addColumn(
            'page_title',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Meta Title'
        )->addColumn(
            'meta_keywords',
            Table::TYPE_TEXT,
            '2M',
            [],
            'Meta Keywords'
        )->addColumn(
            'meta_description',
            Table::TYPE_TEXT,
            '2M',
            [],
            'Meta Description'
        )->setComment(
            'Album Tag'
        );
        $installer->getConnection()->createTable($table);

        /*
            * Create table TABLE_GALLERY_ALBUM_TAG
         */
        $setup->getConnection()->dropTable($setup->getTable(Config::TABLE_GALLERY_ALBUM_TAG));
        $table = $installer->getConnection()->newTable(
            $installer->getTable(Config::TABLE_GALLERY_ALBUM_TAG)
        )->addColumn(
            'tag_id',
            Table::TYPE_SMALLINT,
            null,
            [
             'identity' => true,
             'nullable' => false,
             'primary'  => true,
            ],
            'Tag Id'
        )->addColumn(
            'album_id',
            Table::TYPE_SMALLINT,
            null,
            [
             'nullable' => false,
             'primary'  => true,
            ],
            'Album Id'
        )->addColumn(
            'position',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => true],
            'Position'
        )->addIndex(
            $installer->getIdxName(Config::TABLE_GALLERY_TAG, ['tag_id']),
            ['tag_id']
        )->addForeignKey(
            $installer->getFkName(Config::TABLE_GALLERY_ALBUM_TAG, 'album_id', Config::TABLE_GALLERY_ALBUM, 'album_id'),
            'album_id',
            $installer->getTable(Config::TABLE_GALLERY_ALBUM),
            'album_id',
            Table::ACTION_CASCADE
        )->setComment(
            'Album Tag'
        );
        $installer->getConnection()->createTable($table);

        /*
            * Create table TABLE_GALLERY_ALBUM_IMAGE
         */
        $setup->getConnection()->dropTable($setup->getTable(Config::TABLE_GALLERY_ALBUM_IMAGE));
        $table = $installer->getConnection()->newTable(
            $installer->getTable(Config::TABLE_GALLERY_ALBUM_IMAGE)
        )->addColumn(
            'image_id',
            Table::TYPE_INTEGER,
            null,
            [
             'identity' => true,
             'nullable' => false,
             'unsigned' => true,
             'primary'  => true,
            ],
            'Image ID'
        )->addColumn(
            'key',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Key'
        )->addColumn(
            'album_id',
            Table::TYPE_SMALLINT,
            null,
            ['nullable' => false],
            'Album Id'
        )->addColumn(
            'params',
            Table::TYPE_TEXT,
            '2M',
            [],
            'Params'
        )->addForeignKey(
            $installer->getFkName(Config::TABLE_GALLERY_ALBUM_IMAGE, 'album_id', Config::TABLE_GALLERY_ALBUM, 'album_id'),
            'album_id',
            $installer->getTable(Config::TABLE_GALLERY_ALBUM),
            'album_id',
            Table::ACTION_CASCADE
        )->setComment(
            'Album Image'
        );
        $installer->getConnection()->createTable($table);

        $installer->endSetup();

    }//end install()


}//end class
