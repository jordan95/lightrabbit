<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Gallery\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\DB\Ddl\Table;
use Lof\Gallery\Model\Config;

class UpgradeSchema implements UpgradeSchemaInterface
{


    /**
     * {@inheritdoc}
     *
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        //Update for version 1.0.3
        if (version_compare($context->getVersion(), '1.0.3', '<')) {
            /*
                * Create table TABLE_GALLERY_ALBUM_STORE
             */
            $table = $installer->getConnection()->newTable(
                $installer->getTable(Config::TABLE_GALLERY_ALBUM_STORE)
            )->addColumn(
                'album_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                null,
                [
                 'nullable' => false,
                 'primary'  => true,
                ],
                'Album ID'
            )->addColumn(
                'storelocator_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                null,
                [
                 'nullable' => false,
                 'primary'  => true,
                ],
                'Store Id'
            )->addColumn(
                'position',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                255,
                ['nullable' => false],
                'Position'
            )->addForeignKey(
                $installer->getFkName(Config::TABLE_GALLERY_ALBUM_STORE, 'album_id', Config::TABLE_GALLERY_ALBUM, 'album_id'),
                'album_id',
                $installer->getTable(Config::TABLE_GALLERY_ALBUM),
                'album_id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            )->addForeignKey(
                $installer->getFkName(Config::TABLE_GALLERY_ALBUM_STORE, 'storelocator_id', 'lof_storelocator_storelocator', 'storelocator_id'),
                'storelocator_id',
                $installer->getTable('lof_storelocator_storelocator'),
                'storelocator_id',
                Table::ACTION_CASCADE
            )->setComment(
                'Album Store Table'
            );
            $installer->getConnection()->createTable($table);

            /*
                * Create table TABLE_GALLERY_ALBUM_POST
             */
            $table = $installer->getConnection()->newTable(
                $installer->getTable(Config::TABLE_GALLERY_ALBUM_POST)
            )->addColumn(
                'album_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                null,
                [
                 'nullable' => false,
                 'primary'  => true,
                ],
                'Album ID'
            )->addColumn(
                'post_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                null,
                [
                 'nullable' => false,
                 'primary'  => true,
                ],
                'Post Id'
            )->addColumn(
                'position',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                255,
                ['nullable' => false],
                'Position'
            )->addForeignKey(
                $installer->getFkName(Config::TABLE_GALLERY_ALBUM_POST, 'album_id', Config::TABLE_GALLERY_ALBUM, 'album_id'),
                'album_id',
                $installer->getTable(Config::TABLE_GALLERY_ALBUM),
                'album_id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            )->addForeignKey(
                $installer->getFkName(Config::TABLE_GALLERY_ALBUM_STORE, 'post_id', 'ves_blog_post', 'post_id'),
                'post_id',
                $installer->getTable('ves_blog_post'),
                'post_id',
                Table::ACTION_CASCADE
            )->setComment(
                'Album Post Table'
            );
            $installer->getConnection()->createTable($table);
        }

        //Update for version 1.0.4
        if (version_compare($context->getVersion(), '1.0.4', '<')) {
            /*
                * Create table TABLE_GALLERY_ALBUM_CORESTORE
             */
            $table = $installer->getConnection()->newTable(
                $installer->getTable(Config::TABLE_GALLERY_ALBUM_CORESTORE)
            )->addColumn(
                'album_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                null,
                [
                 'nullable' => false,
                 'primary'  => true,
                ],
                'Album ID'
            )->addColumn(
                'store_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                null,
                [
                 'unsigned' => true,
                 'nullable' => false,
                 'primary'  => true,
                ],
                'Store Id'
            )->addIndex(
                $installer->getIdxName(Config::TABLE_GALLERY_ALBUM_CORESTORE, ['album_id']),
                ['album_id']
            )->addForeignKey(
                $installer->getFkName(Config::TABLE_GALLERY_ALBUM_CORESTORE, 'album_id', Config::TABLE_GALLERY_ALBUM, 'album_id'),
                'album_id',
                $installer->getTable(Config::TABLE_GALLERY_ALBUM),
                'album_id',
                Table::ACTION_CASCADE
            )->addForeignKey(
                $installer->getFkName(Config::TABLE_GALLERY_ALBUM_CORESTORE, 'store_id', 'store', 'store_id'),
                'store_id',
                $installer->getTable('store'),
                'store_id',
                Table::ACTION_CASCADE
            )->setComment(
                'Core Store Table'
            );
            $installer->getConnection()->createTable($table);
        }

        if (version_compare($context->getVersion(), '1.0.5', '<')) {
            $tableName = $setup->getTable('lof_gallery_album');
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $connection = $setup->getConnection();
                $column = [
                    'type' => Table::TYPE_NUMERIC,
                    'nullable' => false,
                    'comment' => 'Position',
                    'default' => '0'
                ];
                $connection->addColumn($tableName, 'position', $column);
            }
        }

        $installer->endSetup();


    }//end upgrade()


}//end class
