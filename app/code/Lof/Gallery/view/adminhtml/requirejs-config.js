/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
/*eslint no-unused-vars: 0*/
var config = {
    deps: [
        "mage/adminhtml/browser",
        "Lof_Gallery/js/browser"
    ],
    map: {
        '*': {
            lofOpenVideoModal: 'Lof_Gallery/js/video-modal',
            lofProductGallery: 'Lof_Gallery/js/product-gallery',
            lofNewVideoDialog: 'Lof_Gallery/js/new-video-dialog',
            lofFabric: 'Lof_Gallery/js/fabric.min',
            lofFileSaver: 'Lof_Gallery/js/FileSaver.min',
            lofAddImage: 'Lof_Gallery/js/addimage',
            'mage/adminhtml/browser':'Lof_Gallery/js/browser'
        }
    },
    "shim": {
        'Lof_Gallery/js/browser':["jquery"]
    }

};

