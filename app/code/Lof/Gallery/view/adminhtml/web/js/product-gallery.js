/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

 define([
   'jquery',
   'Lof_Gallery/js/image-edit',
   'Magento_Catalog/js/product-gallery'
   ], function ($) {
    'use strict';

    $.widget('lof.gallery', $.mage.productGallery, {
    	options: {
            imageSelector: '[data-role=lof-image]',
            imageElementSelector: '[data-role=lof-image-element]',
            template: '[data-template=lof-image]',
            imageResolutionLabel: '[data-role=lof-resolution]',
            imgTitleSelector: '[data-role=lof-img-title]',
            imageSizeLabel: '[data-role=lof-size]',
            types: null,
            initialized: false,
            editImageSelector: '[data-role=edit-image]'
        },

        /**
         * Initializes dialog element.
         */
         _initDialog: function () {
            var $dialog = $(this.dialogContainerTmpl());

            $dialog.modal({
                'type': 'slide',
                title: $.mage.__('Image Detail'),
                buttons: [],
                opened: function () {
                    $dialog.trigger('open');
                },
                closed: function () {
                    $dialog.trigger('close');
                }
            });

            $dialog.on('open', this.onDialogOpen.bind(this));
            $dialog.on('close', function () {
                var $imageContainer = $dialog.data('imageContainer');

                $imageContainer.removeClass('active');
                $dialog.find('#hide-from-product-page').remove();
            });

            $dialog.on('change', '[data-role=type-selector]', function () {
                var parent = $(this).closest('.item'),
                selectedClass = 'selected';

                parent.toggleClass(selectedClass, $(this).prop('checked'));
            });

            $dialog.on('change', '[data-role=type-selector]', $.proxy(this._notifyType, this));

            $dialog.on('change', '[data-role=visibility-trigger]', $.proxy(function (e) {
                var imageData = $dialog.data('imageData');

                this.element.trigger('updateVisibility', {
                    disabled: $(e.currentTarget).is(':checked'),
                    imageData: imageData
                })
            }, this));

            $dialog.on('change', '[data-role=productdisabled]', $.proxy(function (e) {
                var imageData = $dialog.data('imageData');
                imageData.productdisabled = $(e.currentTarget).is(':checked');
                var $imageContainer = this.findElement(imageData);
                $imageContainer.find('[name*="productdisabled"]').val(+imageData.productdisabled);
            }, this));

            $dialog.on('change', '[data-role=categorydisabled]', $.proxy(function (e) {
                var imageData = $dialog.data('imageData');
                imageData.categorydisabled = $(e.currentTarget).is(':checked');
                var $imageContainer = this.findElement(imageData);
                $imageContainer.find('[name*="categorydisabled"]').val(+imageData.categorydisabled);
            }, this));

            $dialog.on('change', '[data-role=linktargetblank]', $.proxy(function (e) {
                var imageData = $dialog.data('imageData');
                imageData.linktargetblank = $(e.currentTarget).is(':checked');
                var $imageContainer = this.findElement(imageData);
                $imageContainer.find('[name*="linktargetblank"]').val(+imageData.linktargetblank);
            }, this));

            /** Image Edit */
            $dialog.on('click', this.options.editImageSelector, $.proxy(function (e) {
                var imageData = $dialog.data('imageData');
                var fileId = imageData.file_id;
                if($(this).data('lof-imageeditor')){
                    $(this).imageeditor('destroy').trigger('change');
                    $('.tui-image-editor').html('');
                }

                console.log(imageData);

                $(this).imageeditor({
                    imageUrl: imageData.url,
                    imageName: imageData.file,
                    ajaxUploadUrl: $('#fileupload').data('url'),
                    imageData: imageData
                });
                $('.image-panel-preview').hide();
                $('.lofgallery-imageedit').show();
                $(this.options.editImageSelector).hide();
                
            }, this));

            $dialog.on('click', '.lofgallery-imageedit #btn-cancel', $.proxy(function (e) {
                $('.lofgallery-imageedit').hide();
                $(this.options.editImageSelector).show();
                $('.image-panel-preview').show();
                $('.tui-image-editor').html('');
            }, this));

            $dialog.on('change', '[data-role="image-label"]', function (e) {
                var target = $(e.target),
                targetName = target.attr('name'),
                desc = target.val(),
                imageData = $dialog.data('imageData');

                this.element.find('input[type="hidden"][name="' + targetName + '"]').val(desc);

                imageData.label = desc;
                imageData.label_default = desc;

                this.element.trigger('updateImageTitle', {
                    imageData: imageData,
                });
            }.bind(this));

            $dialog.on('change', '[data-role="image-title"]', function (e) {
                var target = $(e.target),
                targetName = target.attr('name'),
                title = target.val(),
                imageData = $dialog.data('imageData');

                this.element.find('input[type="hidden"][name="' + targetName + '"]').val(title);

                imageData.title = title;
            }.bind(this));

            $dialog.on('change', '[data-role="image-description"]', function (e) {
                var target = $(e.target),
                targetName = target.attr('name'),
                description = target.val(),
                imageData = $dialog.data('imageData');
                this.element.find('textarea[name="' + targetName + '"]').val(description);

                imageData.description = description;
            }.bind(this));

            $dialog.on('change', '[data-role="image-customurl"]', function (e) {
                var target = $(e.target),
                targetName = target.attr('name'),
                customurl = target.val(),
                imageData = $dialog.data('imageData');
                this.element.find('input[type="hidden"][name="' + targetName + '"]').val(customurl);
                imageData.customurl = customurl;
            }.bind(this));

            this.$dialog = $dialog;
        },

        onDialogOpen: function (event) {
            var imageData = this.$dialog.data('imageData'),
                imageSizeKb = imageData.sizeLabel,
                image = document.createElement('img'),
                sizeSpan = this.$dialog.find(this.options.imageSizeLabel)
                    .find('[data-message]'),
                resolutionSpan = this.$dialog.find(this.options.imageResolutionLabel)
                    .find('[data-message]'),
                sizeText = sizeSpan.attr('data-message').replace('{size}', imageSizeKb),
                resolutionText;

            image.src = imageData.url;

            resolutionText = resolutionSpan
                .attr('data-message')
                .replace('{width}^{height}', image.width + ' x ' + image.height);

            sizeSpan.text(sizeText);
            resolutionSpan.text(resolutionText);

            $(event.target)
                .find('[data-role=type-selector]')
                .each($.proxy(function (index, checkbox) {
                    var $checkbox = $(checkbox),
                        parent = $checkbox.closest('.item'),
                        selectedClass = 'selected',
                        isChecked = this.options.types[$checkbox.val()].value == imageData.file;

                    $checkbox.prop(
                        'checked',
                        isChecked
                    );
                    parent.toggleClass(selectedClass, isChecked);
                }, this));
        }
    });
return $.lof.gallery;
});