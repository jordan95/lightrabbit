define([
    'jquery',
    'Lof_Gallery/js/tui-image-editor'
    ], function ($) {
        'use strict';

        $.widget('lof.imageeditor', {
            options: {
                btns: '.menu-item',
                inputImage: '#input-image-file',
                btnSave: '#btn-save',
                btnCancel: '#btn-cancel',
                btnUndo: '#btn-undo',
                btnRedo: '#btn-redo',
                btnClearObjects: '#btn-clear-objects',
                btnRemoveActiveObject: '#btn-remove-active-object',
                btnCrop: '#btn-crop',
                btnFlip: '#btn-flip',
                btnRotation: '#btn-rotation',
                btnScale: '#btn-scale',
                btnApplyCrop: '#btn-apply-crop',
                btnApplyScale: '#btn-apply-scale',
                btnCancelCrop: '#btn-cancel-crop',
                btnFlipX: '#btn-flip-x',
                btnFlipY: '#btn-flip-y',
                btnResetFlip: '#btn-reset-flip',
                btnRotateClockwise: '#btn-rotate-clockwise',
                btnRotateCounterClockWise: '#btn-rotate-counter-clockwise',
                btnClose: '.close',
                inputRotationRange: '#input-rotation-range',
                inputFontSizeRange: '#input-font-size-range',
                inputStrokeWidthRange: '#input-stroke-width-range',
                inputCheckTransparent: '#input-check-transparent',
                cropSubMenu: '#crop-sub-menu',
                flipSubMenu: '#flip-sub-menu',
                rotationSubMenu: '#rotation-sub-menu',
                scaleSubMenu: '#scale-sub-menu',
                filterSubMenu: '#filter-sub-menu',
                imageData: '',
                imageUrl: '',
                imageName: '',
                ajaxUploadUrl: ''
            },

            _create: function () {

                var supportingFileAPI = !!(window.File && window.FileList && window.FileReader);
                var rImageType = /data:(image\/.+);base64,/;

                var btnsActivatable = $(this.options.btns).filter('.activatable');
                var displayingSubMenu = $();
                tui = window.tui;
                var imageEditor = new tui.component.ImageEditor('.tui-image-editor', {
                    cssMaxWidth: 800,
                    cssMaxHeight: 600,
                    selectionStyle: {
                        cornerSize: 20,
                        rotatingPointOffset: 70
                    }
                });

                imageEditor.clearUndoStack();

                // Attach image editor custom events
                imageEditor.once('loadImage', function() {
                    imageEditor.clearUndoStack();
                });

                var self = this;
                imageEditor.on({
                    emptyUndoStack: function() {
                        $(self.options.btnUndo).addClass('disabled');
                        self.resizeEditor();
                    },
                    emptyRedoStack: function() {
                        $(self.options.btnRedo).addClass('disabled');
                        self.resizeEditor();
                    },
                    pushUndoStack: function() {
                        $(self.options.btnUndo).removeClass('disabled');
                        self.resizeEditor();
                    },
                    pushRedoStack: function() {
                        $(self.options.btnRedo).removeClass('disabled');
                        self.resizeEditor();
                    },
                    endCropping: function() {
                        $(self.options.cropSubMenu).hide();
                        self.resizeEditor();
                    },
                    endFreeDrawing: function() {
                        $(self.options.freeDrawingSubMenu).hide();
                    }
                });


                // Attach button click event listeners
                $(this.options.btns).on('click', function() {
                    btnsActivatable.removeClass('active');
                });

                btnsActivatable.on('click', function() {
                    $(this).addClass('active');
                });

                $(this.options.btnUndo).on('click', function() {
                    displayingSubMenu.hide();
                    imageEditor.undo();
                });

                $(this.options.btnRedo).on('click', function() {
                    displayingSubMenu.hide();
                    imageEditor.redo();
                });

                $(this.options.btnCrop).on('click', function() {
                    imageEditor.startCropping();
                    displayingSubMenu.hide();
                    displayingSubMenu = $(self.options.cropSubMenu).show();
                });

                $(this.options.btnFlip).on('click', function() {
                    imageEditor.endAll();
                    displayingSubMenu.hide();
                    displayingSubMenu = $(self.options.flipSubMenu).show();
                });

                $(this.options.btnRotation).on('click', function() {
                    imageEditor.endAll();
                    displayingSubMenu.hide();
                    displayingSubMenu = $(self.options.rotationSubMenu).show();
                });

                $(this.options.btnScale).on('click', function() {
                    displayingSubMenu.hide();
                    displayingSubMenu = $(self.options.scaleSubMenu).show();
                });

                $(this.options.btnClose).on('click', function() {
                    imageEditor.endAll();
                    displayingSubMenu.hide();
                });

                $(this.options.btnApplyCrop).on('click', function() {
                    imageEditor.endCropping(true);
                });

                $(this.options.btnApplyScale).on('click', function() {
                    var width = $('input[name=imagewidth]').val();
                    var height = $('input[name=imageheight]').val();
                    imageEditor.changeImageSize(width, height);
                });

                $(this.options.btnCancelCrop).on('click', function() {
                    imageEditor.endCropping();
                });

                $(this.options.btnFlipX).on('click', function() {
                    imageEditor.flipX();
                });

                $(this.options.btnFlipY).on('click', function() {
                    imageEditor.flipY();
                });

                $(this.options.btnResetFlip).on('click', function() {
                    imageEditor.resetFlip();
                });

                $(this.options.btnRotateClockwise).on('click', function() {
                    imageEditor.rotate(30);
                });

                $(this.options.btnRotateCounterClockWise).on('click', function() {
                    imageEditor.rotate(-30);
                });

                $(this.options.inputRotationRange).on('mousedown', function() {
                    var changeAngle = function() {
                        imageEditor.setAngle(parseInt($(self.options.inputRotationRange).val(), 10));
                    };
                    $(document).on('mousemove', changeAngle);
                    $(document).on('mouseup', function stopChangingAngle() {
                        $(document).off('mousemove', changeAngle);
                        $(document).off('mouseup', stopChangingAngle);
                    });
                });

                $(this.options.inputImage).on('change', function(event) {
                    var file;

                    if (!supportingFileAPI) {
                        alert('This browser does not support file-api');
                    }

                    file = event.target.files[0];
                    imageEditor.loadImageFromFile(file);
                });

                $(this.options.btnSave).on('click', function(e) {   
                    e.preventDefault();
                    $(this).unbind( "click" );
                    var imageName = imageEditor.getImageName();
                    var dataURL   = imageEditor.toDataURL();
                    var w;
                    if (supportingFileAPI) {
                        var url = self.options.ajaxUploadUrl;
                        $('.gallery-loading').show();
                        $('.dialog-left').append('<div class="gallery-overlay"></div>');
                        $.ajax({
                            url: url,
                            data: {image: dataURL, imageName: imageName},
                            type: 'post',
                            success: $.proxy(function (result) {
                            	$('.tui-image-editor').html('');
                                $('.dialog-left').find('.gallery-overlay').remove();
                                $('.gallery-loading').hide();
                                $('[data-role=edit-image]').show();
                                $('.lofgallery-imageedit').hide();
                                $('.image-panel-preview').show();
                                $('.image-panel-preview img').attr('src', self.options.imageUrl + "?t=" + new Date().getTime());
                                $('.image-' + self.options.imageData.file_id).find('.product-image').attr('src', self.options.imageUrl + "?t=" + new Date().getTime());

                                imageEditor.reloadImageFromURL(self.options.imageUrl, self.options.imageName);
                            }, self)
                        });
                    } else {
                        alert('This browser needs a file-server');
                        w = window.open();
                        w.document.body.innerHTML = '<img src=' + dataURL + '>';
                    }
                });

                // Etc..
                // Load sample image
                imageEditor.loadImageFromURL(self.options.imageUrl + '?t='  + new Date().getTime(), self.options.imageName || 'Image');
            },

            base64ToBlob: function(data) {
                var mimeString = '';
                var raw, uInt8Array, i, rawLength;

                raw = data.replace(rImageType, function(header, imageType) {
                    mimeString = imageType;

                    return '';
                });

                raw = atob(raw);
                rawLength = raw.length;
                uInt8Array = new Uint8Array(rawLength); // eslint-disable-line

                for (i = 0; i < rawLength; i += 1) {
                    uInt8Array[i] = raw.charCodeAt(i);
                }

                return new Blob([uInt8Array], {type: mimeString});
            },

            resizeEditor: function() {
                var $editor = $('.tui-image-editor');
                var $container = $('.tui-image-editor-canvas-container');
                var height = parseFloat($container.css('max-height'));

                $editor.height(height);
            }

        });
        return $.lof.imageeditor;
});