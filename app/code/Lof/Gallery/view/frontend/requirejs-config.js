/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
/*eslint no-unused-vars: 0*/
var config = {
    paths: {
        'jquery.Isotope': 'Lof_Gallery/js/isotope.min',
        'jquery.Imagesloaded': 'Lof_Gallery/js/imagesloaded.min',
        'jquery.Bridget': 'Lof_Gallery/js/jquery-bridget',
        'jquery.Ias': 'Lof_Gallery/js/jquery-ias',
        'jquery.Fakescroll': 'Lof_Gallery/js/fakescroll.min',
        'jquery.MagnificPopup': 'Lof_Gallery/js/jquery.magnific-popup.min',
        'jquery.PrettyPhoto': 'Lof_Gallery/js/jquery.prettyPhoto',
        'jquery.Colorbox': 'Lof_Gallery/js/jquery.colorbox',
        'lgl.boxSlider': 'Lof_Gallery/js/box-slider.min',
    },
    shim: {
    	'jquery.Isotope': {
            deps: ['jquery']
        },
        'jquery.Imagesloaded': {
            deps: ['jquery']
        },
        'jquery.Bridget': {
            deps: ['jquery']
        },
        'jquery.Ias': {
            deps: ['jquery']
        },
        'jquery.Fakescroll': {
            deps: ['jquery']
        },
        'jquery.MagnificPopup': {
            deps: ['jquery']
        },
        'jquery.prettyPhoto': {
            deps: ['jquery']
        },
        'jquery.Colorbox': {
            deps: ['jquery']
        },
        'lgl.boxSlider': {
            deps: ['jquery']
        },
    }
};