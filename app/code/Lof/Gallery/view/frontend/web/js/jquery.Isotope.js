//jquery.Isotope file content
define(['jquery', 'Isotope', 'jquery-bridget/jquery-bridget'], function ($, Isotope) {
    'use strict';
    $.bridget('isotope', Isotope);
    return $;
});