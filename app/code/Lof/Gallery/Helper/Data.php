<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Gallery\Helper;

use Lof\Gallery\Model\Config;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \Magento\Cms\Model\Template\FilterProvider
     */
    protected $filterProvider;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $localeDate;

    /**
     * @var \Lof\Gallery\Model\ResourceModel\Category\CollectionFactory
     */
    protected $categoryCollectionFactory;

    /**
     * @var \Lof\Gallery\Helper\Image
     */
    protected $galleryHelperImage;

    /**
     * @var \Lof\Gallery\Model\Config
     */
    protected $galleryConfig;
    protected $_filesystem;

    /**
     * Data constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Magento\Cms\Model\Template\FilterProvider $filterProvider
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate
     * @param \Lof\Gallery\Model\ResourceModel\Tag\CollectionFactory $tagCollectionFactory
     * @param \Lof\Gallery\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory
     * @param Image $galleryHelperImage
     * @param Config $galleryConfig
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Cms\Model\Template\FilterProvider $filterProvider,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Lof\Gallery\Model\ResourceModel\Tag\CollectionFactory $tagCollectionFactory,
        \Lof\Gallery\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        \Lof\Gallery\Helper\Image $galleryHelperImage,
        \Lof\Gallery\Model\Config $galleryConfig
    ) {
        parent::__construct($context);
        $this->_filesystem   = $filesystem;
        $this->filterProvider = $filterProvider;
        $this->storeManager   = $storeManager;
        $this->localeDate     = $localeDate;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->galleryHelperImage        = $galleryHelperImage;
        $this->galleryConfig = $galleryConfig;

    }//end __construct()


    public function filter($str)
    {
        $html = $this->filterProvider->getPageFilter()->filter($str);
        return $html;

    }//end filter()


    public function getCategoryCollection()
    {
        $collection = $this->categoryCollectionFactory->create()
            ->addFieldToFilter('is_active', ['eq' => 1]);
        $collection->getSelect()->order('cat_position ASC');
        return $collection;

    }//end getCategoryCollection()


    public function getSearchFormUrl()
    {
        $url        = $this->storeManager->getStore()->getBaseUrl();
        $url_prefix = $this->galleryConfig->getConfig('general_settings/url_prefix');
        $urlPrefix  = '';
        if ($url_prefix) {
            $urlPrefix = $url_prefix.'/';
        }

        return $url.$urlPrefix.'search';

    }//end getSearchFormUrl()


    public function getSearchKey()
    {
        return $this->_request->getParam('s');

    }//end getSearchKey()


    public function getLatestPageUrl()
    {
        $url        = $this->storeManager->getStore()->getBaseUrl();
        $url_prefix = $this->galleryConfig->getConfig('general_settings/url_prefix');
        $route      = $this->galleryConfig->getConfig('latest_page/route');
        $urlPrefix  = '';
        if ($url_prefix) {
            $urlPrefix = $url_prefix.'/';
        }

        return $url.$urlPrefix.$route ;

    }//end getLatestPageUrl()


    public function subString( $text, $length = 100, $replacer ='...', $is_striped=true )
    {
        if($length == 0) { return $text;
        }

        $text = ($is_striped == true) ? strip_tags($text) : $text;
        if(strlen($text) <= $length) {
            return $text;
        }

        $text      = substr($text, 0, $length);
        $pos_space = strrpos($text, ' ');
        return substr($text, 0, $pos_space).$replacer;

    }//end subString()


    public function formatDate(
        $date = null,
        $format = \IntlDateFormatter::SHORT,
        $showTime = false,
        $timezone = null
    ) {
        $date = $date instanceof \DateTimeInterface ? $date : new \DateTime($date);
        return $this->localeDate->formatDateTime(
            $date,
            $format,
            $showTime ? $format : \IntlDateFormatter::NONE,
            null,
            $timezone
        );

    }//end formatDate()


    public function getFormatDate($date, $type = 'full')
    {
        $result = '';
        switch ($type) {
        case 'full':
            $result = $this->formatDate($date, \IntlDateFormatter::FULL);
            break;
        case 'long':
            $result = $this->formatDate($date, \IntlDateFormatter::LONG);
            break;
        case 'medium':
            $result = $this->formatDate($date, \IntlDateFormatter::MEDIUM);
            break;
        case 'short':
            $result = $this->formatDate($date, \IntlDateFormatter::SHORT);
            break;
        }

        return $result;

    }//end getFormatDate()


    public function getAlbumImageHtml($image, $imageWidth = '', $imageHeight = '')
    {
        $url = $image['video_url'];
        if ($url = $image['video_url']) {
            if (preg_match('/youtu\.be/i', $url) || preg_match('/youtube\.com\/watch/i', $url)) {
                $pattern = '/^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/';
                preg_match($pattern, $url, $matches);
                if (count($matches) && strlen($matches[7]) == 11) {
                    $embedLink = 'https://www.youtube.com/embed/'.$matches[7];
                }

                return '<iframe width="100%" height="600" src="'.$embedLink.'" frameborder="0" allowfullscreen></iframe>';
            }

            if (preg_match('/vimeo\.com/i', $url)) {
                $pattern = '/\/\/(www\.)?vimeo.com\/(\d+)($|\/)/';
                preg_match($pattern, $url, $matches);
                if (count($matches)) {
                    $embedLink = 'https://player.vimeo.com/video/'.$matches[2];
                    return '<iframe src="'.$embedLink.'" width="100%" height="600" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
                }
            }
        }

        $src = $image['filelink'];
        if ((int) $imageWidth) {
            $src = $this->galleryHelperImage->resizeImage($src, (int) $imageWidth, (int) $imageHeight);
        }

        return '<img src="'.$src.'"/>';

    }//end getAlbumImageHtml()


    public function getImageIdentifier($image)
    {
        $image['name'] = isset($image['name'])?$image['name']:"";
        if(!$image['name'])
            return "";
        $identifier = explode(".", $image['name']);
        $identifier = str_replace('&', '', strtolower($identifier[0]));
        $identifier = str_replace(':', '', strtolower($identifier));
        $identifier = str_replace('’', '', strtolower($identifier));

        if ($image['media_type'] == 'external-video' && $this->startWith($identifier, '/')) {
            $identifier = substr($identifier, 1, -1);
        }else{
            if(strpos( $identifier, '/' ) !== false ){
                $arrayFile =  explode('/', $identifier);
                $numberFile = count($arrayFile);
                $i = 0;
                foreach ($arrayFile as $key => $value){
                    if(++$i === $numberFile) {
                        $identifier = $value;
                    }
                }

            }
        }

        return $identifier;

    }//end getImageIdentifier()


    public function startWith($haystack,$needle)
    {
        if(substr($haystack, 0, strlen($needle)) === $needle) {
            return true;
        }

    }//end startWith()


    public function getPopupUrl($thumbnail, $type = '')
    {

        if ($type == 'magnific' || $type == 'colorbox') {
            $imageNormal = file_exists($this->appendAbsoluteFileSystemPath(Config::MEDIA_PATH.$thumbnail['file']));
            $imageLibary =  file_exists($this->appendAbsoluteFileSystemPath($thumbnail['file']));
            if ($imageNormal) {
                $thumbnail['filelink'] = $this->galleryConfig->getFileUrl($thumbnail['file']);
            }elseif(!$imageNormal && $imageLibary){
                $thumbnail['filelink'] = $this->galleryConfig->getMediaUrl().$thumbnail['file'];
            } elseif(isset($image['file']) && !$imageNormal && !$imageLibary){
                $thumbnail['filelink'] = $thumbnail['file'];
            }
            return $thumbnail['filelink'];
        }

        if ($type == 'prettyphoto') {
            if ($thumbnail['video_url']) {
                return $thumbnail['video_url'];
            }

            return $thumbnail['filelink'];
        }

        $result = '';
        if ($url = $thumbnail['video_url']) {
            $embedLink = '';
            if (preg_match('/youtu\.be/i', $url) || preg_match('/youtube\.com\/watch/i', $url)) {
                $pattern = '/^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/';
                preg_match($pattern, $url, $matches);
                if (count($matches) && strlen($matches[7]) == 11) {
                    $embedLink = 'https://www.youtube.com/embed/'.$matches[7];
                }
            }

            if (preg_match('/vimeo\.com/i', $url)) {
                $pattern = '/\/\/(www\.)?vimeo.com\/(\d+)($|\/)/';
                preg_match($pattern, $url, $matches);
                if (count($matches)) {
                    $embedLink = 'https://player.vimeo.com/video/'.$matches[2];
                }
            }

            $result = $embedLink;
        } else {
            $result = $thumbnail['filelink'];
        }//end if

        return $result;

    }//end getPopupUrl()


    /**
     * Escape quotes in java script
     *
     * @param  mixed  $data
     * @param  string $quote
     * @return mixed
     */
    public function jsQuoteEscape($data, $quote='\'')
    {
        if (is_array($data)) {
            $result = array();
            foreach ($data as $item) {
                $result[] = str_replace($quote, '\\'.$quote, $item);
            }

            return $result;
        }

        return str_replace($quote, '\\'.$quote, $data);

    }//end jsQuoteEscape()

    public function getMediaPath(){
         return   $this->storeManager
            ->getStore()
            ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
    }

    public function getBaseUrl(){
        return $this->storeManager->getStore()->getBaseUrl();
    }


    /**
     * @param string $localTmpFile
     * @return string
     */
    public function appendAbsoluteFileSystemPath($localTmpFile)
    {
        /*
            * @var \Magento\Framework\Filesystem\Directory\Read $mediaDirectory
        */
        $mediaDirectory = $this->_filesystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
        $pathToSave     = $mediaDirectory->getAbsolutePath();
        return $pathToSave.$localTmpFile;

    }//end appendAbsoluteFileSystemPath()


    public function getAlbumImageSrc($image, $imageWidth = '', $imageHeight = '')
    {
        $url = $image['video_url'];
        if ($url = $image['video_url']) {
            if (preg_match('/youtu\.be/i', $url) || preg_match('/youtube\.com\/watch/i', $url)) {
                $pattern = '/^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/';
                preg_match($pattern, $url, $matches);
                if (count($matches) && strlen($matches[7]) == 11) {
                    $embedLink = 'https://www.youtube.com/embed/'.$matches[7];
                }

                return '<iframe width="100%" height="600" src="'.$embedLink.'" frameborder="0" allowfullscreen></iframe>';
            }

            if (preg_match('/vimeo\.com/i', $url)) {
                $pattern = '/\/\/(www\.)?vimeo.com\/(\d+)($|\/)/';
                preg_match($pattern, $url, $matches);
                if (count($matches)) {
                    $embedLink = 'https://player.vimeo.com/video/'.$matches[2];
                    return '<iframe src="'.$embedLink.'" width="100%" height="600" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
                }
            }
        }

        $src = $image['filelink'];
        if ((int) $imageWidth) {
            $src = $this->galleryHelperImage->resizeImage($src, (int) $imageWidth, (int) $imageHeight);
        }

        return $src;

    }//end getAlbumImageHtml()
}//end class
