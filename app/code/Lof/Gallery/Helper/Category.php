<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Gallery\Helper;

class Category extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \Magefan\Blog\Model\ResourceModel\Category\Collection
     */
    protected $_categoryCollection;

    /**
     * @var array
     */
    protected $_drawLevel = [];


    /**
     * Category constructor.
     * @param \Lof\Gallery\Model\ResourceModel\Category\Collection $categoryCollection
     */
    public function __construct(
        \Lof\Gallery\Model\ResourceModel\Category\Collection $categoryCollection
    ) {
        $this->_categoryCollection = $categoryCollection;

    }//end __construct()


    public function getTree()
    {
        $collection = $this->getCatCollection();
        $cats       = [];
        foreach ($collection as $_cat) {
            if(!$_cat->getParentId()) {
                $cat    = [
                           'label'     => $_cat->getName(),
                           'value'     => $_cat->getId(),
                           'id'        => $_cat->getId(),
                           'parent_id' => $_cat->getParentId(),
                           'level'     => 0,
                           'postion'   => $_cat->getCatPosition(),
                          ];
                $cats[] = $this->drawItems($collection, $cat);
            }
        }

        $this->drawSpaces($cats);

        return $this->_drawLevel;

    }//end getTree()


    public function getCatCollection()
    {
        $collection = $this->_categoryCollection
            ->setOrder('cat_position', 'ASC');
        return $collection;

    }//end getCatCollection()


    protected function _getSpaces($n)
    {
        $s = '';
        for($i = 0; $i < $n; $i++) {
            $s .= '--- ';
        }

        return $s;

    }//end _getSpaces()


    public function drawItems($collection, $cat, $level = 0)
    {
        foreach ($collection as $_cat) {
            if($_cat->getParentId() == $cat['id']) {
                $cat1            = [
                                    'label'     => $_cat->getName(),
                                    'value'     => $_cat->getId(),
                                    'id'        => $_cat->getId(),
                                    'parent_id' => $_cat->getParentId(),
                                    'level'     => 0,
                                    'postion'   => $_cat->getCatPosition(),
                                   ];
                $children[]      = $this->drawItems($collection, $cat1, ($level + 1));
                $cat['optgroup'] = $children;
            }
        }

        $cat['level'] = $level;
        return $cat;

    }//end drawItems()


    public function drawSpaces($cats)
    {
        if(is_array($cats)) {
            foreach ($cats as $k => $v) {
                $v['label'] = $this->_getSpaces($v['level']).$v['label'];
                if ($v['level'] == 0) {
                    $this->_drawLevel[] = $v;
                    if(isset($v['optgroup']) && $children = $v['optgroup']) {
                               $this->drawSpaces($children);
                    }
                }
            }
        }

    }//end drawSpaces()


}//end class
