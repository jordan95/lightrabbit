<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Gallery\Controller\Category;

class View extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var \Magento\Framework\Controller\Result\ForwardFactory
     */
    protected $resultForwardFactory;

    /**
     * @var \Lof\Gallery\Model\Config
     */
    protected $galleryConfig;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry;


    /**
     * @param \Magento\Framework\App\Action\Context               $context
     * @param \Magento\Framework\View\Result\PageFactory          $resultPageFactory
     * @param \Magento\Framework\Controller\Result\ForwardFactory $resultForwardFactory
     * @param \Magento\Framework\Registry                         $registry
     * @param \Lof\Gallery\Model\Config                           $galleryConfig
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Controller\Result\ForwardFactory $resultForwardFactory,
        \Magento\Framework\Registry $registry,
        \Lof\Gallery\Model\Config $galleryConfig
    ) {
        parent::__construct($context);
        $this->resultPageFactory    = $resultPageFactory;
        $this->resultForwardFactory = $resultForwardFactory;
        $this->galleryConfig        = $galleryConfig;
        $this->coreRegistry         = $registry;

    }//end __construct()


    /**
     * Default customer account page
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        if(!$this->galleryConfig->getConfig('general_settings/enable')) {
            $resultForward = $this->resultForwardFactory->create();
            return $resultForward->forward('defaultnoroute');
        }

        $category = $this->coreRegistry->registry('gallery_category');
        $resultPage->addHandle(['type' => 'LOFGALLERY_CATEGORY_VIEW_'.$category->getCategoryId()]);
        if (($layoutUpdate = $category->getLayoutUpdateXml()) && trim($layoutUpdate) != '') {
            $resultPage->addUpdate($layoutUpdate);
        }

        $pageLayout = $category->getPageLayout();

        if($pageLayout) {
            $resultPage->getConfig()->setPageLayout($pageLayout);
        }

        return $resultPage;

    }//end execute()


}//end class
