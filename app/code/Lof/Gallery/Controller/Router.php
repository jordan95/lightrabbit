<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Gallery\Controller;

use Lof\Gallery\Model\Tag;
use Lof\Gallery\Model\Category;

class Router implements \Magento\Framework\App\RouterInterface
{
    /**
     * @var \Magento\Framework\App\ActionFactory
     */
    protected $actionFactory;

    /**
     * Event manager
     *
     * @var \Magento\Framework\Event\ManagerInterface
     */
    protected $_eventManager;

    /**
     * Store manager
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * Page factory
     *
     * @var \Magento\Cms\Model\PageFactory
     */
    protected $_albumFactory;

    /**
     * @var bool
     */
    protected $dispatched;

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry;

    /**
     * @var \Lof\Gallery\Model\Category
     */
    protected $_categoryFactory;

    /**
     * @var \Lof\Gallery\Model\TagFactory
     */
    protected $tagFactory;

    /**
     * @var \Lof\Gallery\Model\Config
     */
    protected $galleryConfig;

    /**
     * @var \Lof\Gallery\Helper\Data
     */
    protected $galleryHelper;


    /**
     * @param \Magento\Framework\App\ActionFactory       $actionFactory   [description]
     * @param \Magento\Framework\Event\ManagerInterface  $eventManager    [description]
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager    [description]
     * @param \Magento\Framework\Registry                $registry        [description]
     * @param \Lof\Gallery\Model\Album                   $albumFactory    [description]
     * @param \Lof\Gallery\Model\Category                $categoryFactory [description]
     * @param \Lof\Gallery\Model\TagFactory              $tagFactory      [description]
     * @param \Lof\Gallery\Model\Config                  $galleryConfig   [description]
     * @param \Lof\Gallery\Helper\Data                   $galleryHelper   [description]
     */
    public function __construct(
        \Magento\Framework\App\ActionFactory $actionFactory,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Registry $registry,
        \Lof\Gallery\Model\Album $albumFactory,
        \Lof\Gallery\Model\Category $categoryFactory,
        \Lof\Gallery\Model\TagFactory $tagFactory,
        \Lof\Gallery\Model\Config $galleryConfig,
        \Lof\Gallery\Helper\Data $galleryHelper
    ) {
        $this->actionFactory    = $actionFactory;
        $this->_eventManager    = $eventManager;
        $this->_storeManager    = $storeManager;
        $this->coreRegistry     = $registry;
        $this->_albumFactory    = $albumFactory;
        $this->_categoryFactory = $categoryFactory;
        $this->tagFactory       = $tagFactory;
        $this->galleryConfig    = $galleryConfig;
        $this->galleryHelper    = $galleryHelper;

    }//end __construct()


    /**
     * Validate and Match Cms Page and modify request
     *
     * @param  \Magento\Framework\App\RequestInterface $request
     * @return bool
     */
    public function match(\Magento\Framework\App\RequestInterface $request)
    {
        $store = $this->_storeManager->getStore();
        if (!$this->dispatched) {
            $identifier = trim($request->getPathInfo(), '/');
            $origUrlKey = $identifier;

            $condition = new \Magento\Framework\DataObject(['identifier' => $identifier, 'continue' => true]);
            $this->_eventManager->dispatch(
                'lofgallery_controller_router_match_before',
                [
                 'router'    => $this,
                 'condition' => $condition,
                ]
            );
            $identifier = $condition->getIdentifier();

            if ($condition->getRedirectUrl()) {
                $this->response->setRedirect($condition->getRedirectUrl());
                $request->setDispatched(true);
                return $this->actionFactory->create(
                    'Magento\Framework\App\Action\Redirect',
                    ['request' => $request]
                );
            }

            $route  = $this->galleryConfig->getConfig('general_settings/route');
            $enable = $this->galleryConfig->getConfig('general_settings/enable');

            if ($enable) {
                // Gallery page
                $identifiers = explode('/', $identifier);
                if(count($identifiers) == 1 && $identifiers[0] == $route) {
                    $request->setModuleName('lofgallery')
                        ->setControllerName('index')
                        ->setActionName('index');
                    $request->setAlias(\Magento\Framework\Url::REWRITE_REQUEST_PATH_ALIAS, $origUrlKey);
                    $request->setDispatched(true);
                    $this->dispatched = true;
                    return $this->actionFactory->create(
                        'Magento\Framework\App\Action\Forward',
                        ['request' => $request]
                    );
                }

                if(count($identifiers) == 2 && ($route == $identifiers[0]) && $identifiers[1] != '') {
                    $alias    = $identifiers[1];
                    $category = $this->_categoryFactory->load($alias);
                    if(isset($identifiers[1]) && $category && $category->getCategoryId() && $category->getIsActive() == Category::STATUS_ENABLED) {
                        $this->coreRegistry->register("gallery_category", $category);
                        $request->setModuleName('lofgallery')
                            ->setControllerName('category')
                            ->setActionName('view');
                        $request->setAlias(\Magento\Framework\Url::REWRITE_REQUEST_PATH_ALIAS, $origUrlKey);
                        $request->setDispatched(true);
                        $this->dispatched = true;
                        return $this->actionFactory->create(
                            'Magento\Framework\App\Action\Forward',
                            ['request' => $request]
                        );
                    }
                }

                // Tag Page
                if(count($identifiers) == 3 && ($route == $identifiers[0]) && $identifiers[1] == 'tag' && $identifiers[2] != '') {
                    $alias = $identifiers[2];

                    $tag = $this->tagFactory->create()->load($alias);
                    if(isset($identifiers[1]) && $tag && $tag->getTagId() && $tag->getIsActive() == Tag::STATUS_ENABLED) {
                        $this->coreRegistry->register("gallery_tag", $tag);
                        $request->setModuleName('lofgallery')
                            ->setControllerName('tag')
                            ->setActionName('view');
                        $request->setAlias(\Magento\Framework\Url::REWRITE_REQUEST_PATH_ALIAS, $origUrlKey);
                        $request->setDispatched(true);
                        $this->dispatched = true;
                        return $this->actionFactory->create(
                            'Magento\Framework\App\Action\Forward',
                            ['request' => $request]
                        );
                    }
                }

                // Search Page
                if (count($identifiers) == 2 && $route == $identifiers[0] && $identifiers[1] == 'search') {
                    $request->setModuleName('lofgallery')
                        ->setControllerName('search')
                        ->setActionName('result');
                    $request->setAlias(\Magento\Framework\Url::REWRITE_REQUEST_PATH_ALIAS, $origUrlKey);
                    $request->setDispatched(true);
                    $this->dispatched = true;
                    return $this->actionFactory->create(
                        'Magento\Framework\App\Action\Forward',
                        ['request' => $request]
                    );
                }

                // Album Page
                if (count($identifiers) == 2 && $route == $identifiers[0]) {
                    $alias = $identifiers[1];
                    $album = $this->_albumFactory->getCollection()
                        ->addFieldToFilter('identifier', $alias)
                        ->addFieldToFilter('is_active', 1)
                        ->addStoreFilter($store)
                        ->getFirstItem();

                    if($album && $album->getId()) {
                        $this->coreRegistry->register("gallery_album", $album);
                        $request->setModuleName('lofgallery')
                            ->setControllerName('album')
                            ->setActionName('view');
                        $request->setAlias(\Magento\Framework\Url::REWRITE_REQUEST_PATH_ALIAS, $origUrlKey);
                        $request->setDispatched(true);
                        $this->dispatched = true;
                        return $this->actionFactory->create(
                            'Magento\Framework\App\Action\Forward',
                            ['request' => $request]
                        );
                    }
                }//end if

                // Image Page
                if (count($identifiers) == 3 && $route == $identifiers[0]) {
                    $alias = $identifiers[1];
                    $album = $this->_albumFactory->getCollection()
                        ->addFieldToFilter('identifier', $alias)
                        ->addStoreFilter($store)
                        ->addFieldToFilter('is_active', 1)
                        ->getFirstItem();

                    $images = $album->getImages();
                    if ($album && $album->getId()) {
                        $result = '';
                        foreach ($images as $k => $image) {
                            $imageIdentifier = $this->galleryHelper->getImageIdentifier($image);
                            if ($imageIdentifier == $identifiers[2]) {
                                $result = $image;
                                break;
                            }
                        }

                        if ($result) {
                            $this->coreRegistry->register("gallery_album", $album);
                            $this->coreRegistry->register("album_image", $result);
                            $request->setModuleName('lofgallery')
                                ->setControllerName('image')
                                ->setActionName('view');
                            $request->setAlias(\Magento\Framework\Url::REWRITE_REQUEST_PATH_ALIAS, $origUrlKey);
                            $request->setDispatched(true);
                            $this->dispatched = true;
                            return $this->actionFactory->create(
                                'Magento\Framework\App\Action\Forward',
                                ['request' => $request]
                            );
                        }
                    }//end if
                }//end if
            }//end if
        }//end if

    }//end match()


}//end class
