<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Gallery\Controller\Album;

use Magento\Customer\Controller\AccountInterface;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class View extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $_request;

    /**
     * @var \Magento\Framework\App\ResponseInterface
     */
    protected $_response;

    /**
     * @var \Magento\Framework\Controller\Result\RedirectFactory
     */
    protected $resultRedirectFactory;

    /**
     * @var \Magento\Framework\Controller\ResultFactory
     */
    protected $resultFactory;

    /**
     * @var \Lof\Gallery\Helper\Data
     */
    protected $_galleryHelper;

    /**
     * @var \Magento\Framework\Controller\Result\ForwardFactory
     */
    protected $resultForwardFactory;


    /**
     * @param Context                                             $context
     * @param \Magento\Framework\View\Result\PageFactory          $resultPageFactory
     * @param \Lof\Gallery\Helper\Data                            $galleryHelper
     * @param \Magento\Framework\Controller\Result\ForwardFactory $resultForwardFactory
     * @param \Magento\Framework\Registry                         $registry
     */
    public function __construct(
        Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Controller\Result\ForwardFactory $resultForwardFactory,
        \Magento\Framework\Registry $registry,
        \Lof\Gallery\Model\Config $galleryConfig
    ) {
        parent::__construct($context);
        $this->resultPageFactory    = $resultPageFactory;
        $this->resultForwardFactory = $resultForwardFactory;
        $this->coreRegistry         = $registry;
        $this->galleryConfig        = $galleryConfig;

    }//end __construct()


    public function execute()
    {
        $album = $this->coreRegistry->registry('gallery_album');
        $album->setHits(((int) $album->getHits() + 1));
        $album->setData("isfrontend", true);
        $album->save();

        $resultPage = $this->resultPageFactory->create();
        if(!$this->galleryConfig->getConfig('general_settings/enable') || !$album->getId()) {
            $resultForward = $this->resultForwardFactory->create();
            return $resultForward->forward('defaultnoroute');
        }

        $pageLayout = $album->getPageLayout();
        if($pageLayout) {
            $resultPage->getConfig()->setPageLayout($pageLayout);
        }

        return $resultPage;

    }//end execute()


}//end class
