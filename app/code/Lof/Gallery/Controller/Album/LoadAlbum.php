<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Gallery\Controller\Album;

use Magento\Customer\Controller\AccountInterface;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Lof\Gallery\Model\Config;

class LoadAlbum extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $_resource;

    /**
     * @var \Magento\Framework\View\LayoutInterface
     */
    protected $_layout;

    /**
     * @var \Lof\Gallery\Model\ResourceModel\Album\CollectionFactory
     */
    protected $albumCollectionFactory;


    /**
     * @param \Magento\Framework\App\Action\Context                    $context
     * @param \Magento\Framework\View\LayoutInterface                  $layout
     * @param \Magento\Framework\App\ResourceConnection                $resource
     * @param \Lof\Gallery\Model\ResourceModel\Album\CollectionFactory $albumCollectionFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\LayoutInterface $layout,
        \Magento\Framework\App\ResourceConnection $resource,
        \Lof\Gallery\Model\ResourceModel\Album\CollectionFactory $albumCollectionFactory
    ) {
        parent::__construct($context);
        $this->_resource = $resource;
        $this->_layout   = $layout;
        $this->albumCollectionFactory = $albumCollectionFactory;

    }//end __construct()


    /**
     * Default customer account page
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $responseData['status'] = true;
        $params = $this->_request->getPostValue();
        try {
            $curPage = $params['currentPage'];
            if (!empty($params) && isset($params['categories']) && isset($params['blockId'])) {
                if ($params['limit'] && ($params['pageSize'] * ($curPage + 1)) > $params['limit']) {
                    $params['pageSize'] = ($params['limit'] - ($params['pageSize'] * $curPage));
                }

                if ($params['pageSize']) {
                    $collection = $this->albumCollectionFactory->create()
                        ->addFieldToFilter("is_active", 1);
                    $categories = $params['categories'];
                    $collection->getSelect()
                        ->joinLeft(
                            [
                             'lg' => $this->_resource->getTableName(Config::TABLE_GALLERY_ALBUM_CATEGORY),
                            ],
                            'lg.album_id = main_table.album_id',
                            [
                             'album_id' => 'album_id',
                             'position' => 'position',
                            ]
                        )
                        ->order('lg.position ASC')
                        ->order('lg.album_id DESC')
                        ->limit($params['limit'])
                        ->group('main_table.album_id');
                    if ($categories) {
                        $collection->getSelect()->where('lg.entity_id IN (?)', $categories);
                    }

                    $collection->setPageSize($params['pageSize']);
                    $collection->setCurPage($curPage + 1);
                    $albumBlock            = $this->_layout->createBlock('Lof\Gallery\Block\Album\AlbumList')
                        ->setData('layout_type', 'masonry')
                        ->setData($params['options'])
                        ->setCollection($collection);
                    $responseData['items'] = $albumBlock->toHtml();
                    $responseData['count'] = $collection->count();
                } else {
                    $responseData['status'] = true;
                    $responseData['items']  = 0;
                }//end if
            }//end if
        } catch (\Exception $e) {
            $responseData['status'] = false;
            $responseData['items']  = '';
        }//end try

        $this->getResponse()->representJson(
            $this->_objectManager->get('Magento\Framework\Json\Helper\Data')->jsonEncode($responseData)
        );
        return;

    }//end execute()


}//end class
