<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Gallery\Controller\Adminhtml\Album;

use Magento\Framework\App\Filesystem\DirectoryList;
use Lof\Gallery\Model\Config;

class Save extends \Lof\Gallery\Controller\Adminhtml\Album
{
    /**
     * @var \Magento\Catalog\Model\Product\Media\Config
     */
    protected $mediaConfig;

    /**
     * @var \Magento\Framework\Filesystem\Directory\WriteInterface
     */
    protected $mediaDirectory;

    /**
     * @var \Magento\Backend\Helper\Js
     */
    protected $jsHelper;
    /**
     * @var \Magento\Framework\Filesystem
     */
    protected $_fileSystem;


    /**
     * @param \Magento\Backend\App\Action\Context                            $context
     * @param \Magento\Framework\Registry                                    $coreRegistry
     * @param \Magento\Backend\Helper\Js                                     $jsHelper
     * @param \Magento\Catalog\Model\Product\Media\Config                    $mediaConfig
     * @param \Magento\Framework\Filesystem                                  $filesystem
     * @param \Lof\Gallery\Block\Adminhtml\Album\Helper\Form\Gallery\Content $galleryContent
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Backend\Helper\Js $jsHelper,
        \Magento\Catalog\Model\Product\Media\Config $mediaConfig,
        \Magento\Framework\Filesystem $filesystem,
        \Lof\Gallery\Block\Adminhtml\Album\Helper\Form\Gallery\Content $galleryContent
    ) {
        parent::__construct($context, $coreRegistry);
        $this->_fileSystem    = $filesystem;
        $this->jsHelper       = $jsHelper;
        $this->mediaConfig    = $mediaConfig;
        $this->mediaDirectory = $filesystem->getDirectoryWrite(DirectoryList::MEDIA);
        $this->galleryContent = $galleryContent;

    }//end __construct()
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Lof_Gallery::album_save');

    }//end _isAllowed()

    public function execute()
    {
        /*
            *
            *
            * @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect
        */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data           = $this->getRequest()->getPostValue();
        $links          = $this->getRequest()->getPost('links');
        $links          = is_array($links) ? $links : [];

        if (!empty($links) && isset($links['related_products'])) {
            $relatedProducts          = $this->jsHelper->decodeGridSerializedInput($links['related_products']);
            $data['related_products'] = $relatedProducts;
        }

        if (!empty($links) && isset($links['related_stores'])) {
            $relatedStores          = $this->jsHelper->decodeGridSerializedInput($links['related_stores']);
            $data['related_stores'] = $relatedStores;
        }

        if (!empty($links) && isset($links['related_posts'])) {
            $relatedPosts          = $this->jsHelper->decodeGridSerializedInput($links['related_posts']);
            $data['related_posts'] = $relatedPosts;
        }

        if ($data) {
            if (isset($data['album']['media_gallery'])) {
                $mediaGallery = $data['album']['media_gallery'];
                if ($mediaGallery) {
                    foreach ($mediaGallery['images'] as $key => $image) {

                        if (!isset($image['file']) || $image['file']=='') {
                            unset($mediaGallery['images'][$key]);
                            continue;
                        }

                        if (isset($image['removed']) && $image['removed'] == 1) {
                            $this->removeDeletedImages($image['file']);
                            unset($mediaGallery['images'][$key]);
                            continue;
                        }

                        if (isset($image['video_description']) && $image['video_description'] != '') {
                            $mediaGallery['images'][$key]['video_description'] = base64_encode($image['video_description']);
                        }

                        if (isset($image['role']) && $image['role'] != '') {
                            $mediaGallery['images'][$key]['role'] = base64_encode($image['role']);
                        }

                        if (isset($image['description']) && $image['description'] != '') {
                            $mediaGallery['images'][$key]['description'] = base64_encode($image['description']);
                        }

                        $imageAttributes = $this->galleryContent->getMediaAttributes();
                        foreach ($imageAttributes as $k => $attribute) {
                            if (isset($data['album'][$attribute['code']]) && ($data['album'][$attribute['code']] == $image['file'])) {
                                $mediaGallery['images'][$key][$attribute['code']] = 1;
                            } else {
                                $mediaGallery['images'][$key][$attribute['code']] = 0;
                            }
                        }
                    }//end foreach

                    $data['images']        = $mediaGallery['images'];
                    $data['media_gallery'] = $mediaGallery;
                    unset($data['album']);
                }//end if
            }//end if

            if ($data['identifier'] == '') {
                $data['identifier'] = $data['name'];
            }

            $data['identifier'] = str_replace(' ', '-', strtolower($data['identifier']));
            $data['identifier'] = str_replace('&', '', strtolower($data['identifier']));
            $data['identifier'] = str_replace(':', '', strtolower($data['identifier']));
            $data['identifier'] = str_replace('’', '', strtolower($data['identifier']));

            $id    = $this->getRequest()->getParam('album_id');
            $model = $this->_objectManager->create('Lof\Gallery\Model\Album')->load($id);
            if (!$model->getId() && $id) {
                $this->messageManager->addError(__('This album no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }

            $mediaDirectory = $this->_objectManager->get('Magento\Framework\Filesystem')
                ->getDirectoryRead(DirectoryList::MEDIA);
            $mediaFolder    = 'lof/gallery/';
            $path           = $mediaDirectory->getAbsolutePath($mediaFolder);


            $model->setData($data);

            // try to save it
            try {
                // save the data
                $model->save();
                // display success message
                $this->messageManager->addSuccess(__('You saved the album.'));
                // clear previously saved data from session
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);

                if($this->getRequest()->getParam("duplicate")) {
                    unset($data['album_id']);
                    $data['identifier'] = $data['identifier'].time();

                    $album = $this->_objectManager->create('Lof\Gallery\Model\Album');
                    $album->setData($data);
                    try{
                        $album->save();
                        $this->messageManager->addSuccess(__('You duplicated this album.'));
                        return $resultRedirect->setPath('*/*/edit', ['album_id' => $model->getId()]);
                    } catch (\Magento\Framework\Exception\LocalizedException $e) {
                        $this->messageManager->addError($e->getMessage());
                    } catch (\RuntimeException $e) {
                        $this->messageManager->addError($e->getMessage());
                    } catch (\Exception $e) {
                        $this->messageManager->addException($e, __('Something went wrong while duplicating the album.'));
                    }
                }

                // check if 'Save and Continue'
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['album_id' => $model->getId()]);
                }

                if ($this->getRequest()->getParam("new")) {
                    return $resultRedirect->setPath('*/*/new');
                }

                // go to grid
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                // display error message
                $this->messageManager->addError($e->getMessage());
                // save data in session
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData($data);
                // redirect to edit form
                return $resultRedirect->setPath('*/*/edit', ['album_id' => $this->getRequest()->getParam('album_id')]);
            }//end try
        }//end if

        return $resultRedirect->setPath('*/*/');

    }//end execute()


    /**
     * @param array $files
     * @return null
     */
    protected function removeDeletedImages($filePath)
    {
        $catalogPath = $this->mediaConfig->getBaseMediaPath();
        $this->mediaDirectory->delete(Config::MEDIA_PATH.$filePath);

    }//end removeDeletedImages()


}//end class
