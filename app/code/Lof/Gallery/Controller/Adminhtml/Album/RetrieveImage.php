<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Gallery\Controller\Adminhtml\Album;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\File\Uploader;
use Magento\Framework\Validator\AllowedProtocols;
use Magento\Framework\Exception\LocalizedException;

use Lof\Gallery\Model\Config;

class RetrieveImage extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\Controller\Result\RawFactory
     */
    protected $resultRawFactory;

    /**
     * @var \Magento\Catalog\Model\Product\Media\Config
     */
    protected $mediaConfig;

    /**
     * @var \Magento\Framework\Filesystem
     */
    protected $fileSystem;

    /**
     * @var \Magento\Framework\Image\Adapter\AbstractAdapter
     */
    protected $imageAdapter;

    /**
     * @var \Magento\Framework\HTTP\Adapter\Curl
     */
    protected $curl;

    /**
     * @var \Magento\MediaStorage\Model\ResourceModel\File\Storage\File
     */
    protected $fileUtility;

    /**
     * URI validator
     *
     * @var AllowedProtocols
     */
    private $validator;


    /**
     * @param \Magento\Backend\App\Action\Context                         $context
     * @param \Magento\Framework\Controller\Result\RawFactory             $resultRawFactory
     * @param \Magento\Catalog\Model\Product\Media\Config                 $mediaConfig
     * @param \Magento\Framework\Filesystem                               $fileSystem
     * @param \Magento\Framework\Image\AdapterFactory                     $imageAdapterFactory
     * @param \Magento\Framework\HTTP\Adapter\Curl                        $curl
     * @param \Magento\MediaStorage\Model\ResourceModel\File\Storage\File $fileUtility
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
        \Magento\Catalog\Model\Product\Media\Config $mediaConfig,
        \Magento\Framework\Filesystem $fileSystem,
        \Magento\Framework\Image\AdapterFactory $imageAdapterFactory,
        \Magento\Framework\HTTP\Adapter\Curl $curl,
        \Magento\MediaStorage\Model\ResourceModel\File\Storage\File $fileUtility,
        \Lof\Gallery\Model\Config $galleryConfig
    ) {
        parent::__construct($context);
        $this->resultRawFactory = $resultRawFactory;
        $this->mediaConfig      = $mediaConfig;
        $this->fileSystem       = $fileSystem;
        $this->imageAdapter     = $imageAdapterFactory->create();
        $this->curl          = $curl;
        $this->fileUtility   = $fileUtility;
        $this->galleryConfig = $galleryConfig;

    }//end __construct()


    /**
     * @return \Magento\Framework\Controller\Result\Raw
     */
    public function execute()
    {
        $baseTmpMediaPath = $this->mediaConfig->getBaseTmpMediaPath();
        try {
            $remoteFileUrl = $this->getRequest()->getParam('remote_image');
            $this->validateRemoteFile($remoteFileUrl);
            $originalFileName   = basename($remoteFileUrl);
            $localFileName      = Uploader::getCorrectFileName($originalFileName);
            $localFileMediaPath = Config::MEDIA_PATH.DIRECTORY_SEPARATOR.$localFileName;

            $localUniqueFileMediaPath = $this->appendNewFileName($localFileMediaPath);
            $this->retrieveRemoteImage($remoteFileUrl, $localUniqueFileMediaPath);
            $localFileFullPath = $this->appendAbsoluteFileSystemPath($localUniqueFileMediaPath);
            $this->imageAdapter->validateUploadFile($localFileFullPath);
            $result = $this->appendResultSaveRemoteImage($localUniqueFileMediaPath);
        } catch (\Exception $e) {
            $result = [
                       'error'     => $e->getMessage(),
                       'errorcode' => $e->getCode(),
                      ];
        }

        /*
            * @var \Magento\Framework\Controller\Result\Raw $response
        */
        $response = $this->resultRawFactory->create();
        $response->setHeader('Content-type', 'text/plain');
        $response->setContents(json_encode($result));
        return $response;

    }//end execute()


    /**
     * Get URI validator
     *
     * @return AllowedProtocols
     */
    private function getValidator()
    {
        if ($this->validator === null) {
            $this->validator = $this->_objectManager->get(AllowedProtocols::class);
        }

        return $this->validator;

    }//end getValidator()


    /**
     * Validate remote file
     *
     * @param  string $remoteFileUrl
     * @throws LocalizedException
     *
     * @return $this
     */
    private function validateRemoteFile($remoteFileUrl)
    {
        /*
            * @var AllowedProtocols $validator
        */
        $validator = $this->getValidator();
        if (!$validator->isValid($remoteFileUrl)) {
            throw new LocalizedException(
                __("Protocol isn't allowed")
            );
        }

        return $this;

    }//end validateRemoteFile()


    /**
     * @param string $fileName
     * @return mixed
     */
    protected function appendResultSaveRemoteImage($fileName)
    {
        $fileInfo        = pathinfo($fileName);
        $tmpFileName     = Uploader::getDispretionPath($fileInfo['basename']).DIRECTORY_SEPARATOR.$fileInfo['basename'];
        $result['name']  = DIRECTORY_SEPARATOR.$fileInfo['basename'];
        $result['type']  = $this->imageAdapter->getMimeType();
        $result['error'] = 0;
        $result['size']  = filesize($this->appendAbsoluteFileSystemPath($fileName));
        $result['url']   = $this->galleryConfig->getFileUrl($result['name']);
        $result['file']  = DIRECTORY_SEPARATOR.$fileInfo['basename'];
        return $result;

    }//end appendResultSaveRemoteImage()


    /**
     * @param string $fileUrl
     * @param string $localFilePath
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function retrieveRemoteImage($fileUrl, $localFilePath)
    {
        $this->curl->setConfig(['header' => false]);
        $this->curl->write('GET', $fileUrl);
        $image = $this->curl->read();
        if (empty($image)) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('Could not get preview image information. Please check your connection and try again.')
            );
        }

        $this->fileUtility->saveFile($localFilePath, $image);

    }//end retrieveRemoteImage()


    /**
     * @param string $localFilePath
     * @return string
     */
    protected function appendNewFileName($localFilePath)
    {
        $destinationFile = $this->appendAbsoluteFileSystemPath($localFilePath);
        $fileName        = Uploader::getNewFileName($destinationFile);
        $fileInfo        = pathinfo($localFilePath);
        return $fileInfo['dirname'].DIRECTORY_SEPARATOR.$fileName;

    }//end appendNewFileName()


    /**
     * @param string $localTmpFile
     * @return string
     */
    protected function appendAbsoluteFileSystemPath($localTmpFile)
    {
        /*
            * @var \Magento\Framework\Filesystem\Directory\Read $mediaDirectory
        */
        $mediaDirectory = $this->fileSystem->getDirectoryRead(DirectoryList::MEDIA);
        $pathToSave     = $mediaDirectory->getAbsolutePath();
        return $pathToSave.$localTmpFile;

    }//end appendAbsoluteFileSystemPath()


}//end class
