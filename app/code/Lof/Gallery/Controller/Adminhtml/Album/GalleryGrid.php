<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Gallery\Controller\Adminhtml\Album;

use Magento\Framework\Controller\ResultFactory;

class GalleryGrid extends \Magento\Catalog\Controller\Adminhtml\Product
{


    public function execute()
    {
        $product      = $this->productBuilder->build($this->getRequest());
        $resultLayout = $this->resultFactory->create(ResultFactory::TYPE_LAYOUT);
        $resultLayout->getLayout()->getBlock('gallery.album.edit.tab.gallery')
            ->setProductsAlbum($this->getRequest()->getPost('album', null))
            ->setProductId($product->getId())
            ->setUseAjax(true);
        return $resultLayout;

    }//end execute()


    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Lof_Gallery::album');

    }//end _isAllowed()


}//end class
