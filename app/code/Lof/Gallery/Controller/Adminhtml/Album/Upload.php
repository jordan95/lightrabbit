<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Gallery\Controller\Adminhtml\Album;

use Braintree\Exception;
use Magento\Framework\App\Filesystem\DirectoryList;
use Lof\Gallery\Model\Config;
use Magento\Framework\File\Uploader;

class Upload extends \Magento\Backend\App\Action
{

    /**
     * @var \Magento\Framework\Controller\Result\RawFactory
     */
    protected $resultRawFactory;

    /**
     * @var \Lof\Gallery\Model\Config
     */
    protected $galleryConfig;

    /**
     * @var MEDIA
     */
    protected $mediaDirectory;

    /**
     * @var \Magento\Framework\Filesystem
     */
    protected $fileSystem;

    /**
     * @var \Magento\Catalog\Model\Product\Media\Config
     */
    protected $mediaConfig;

    /**
     * @var \Magento\Framework\Url\DecoderInterface
     */
    protected $urlDecoder;
    /**
     * @var \Magento\Framework\Filesystem
     */
    protected $_filesystem;
    /**
     * @var \Magento\Framework\Filesystem\Directory\WriteInterface
     */
    protected $_directoryMedia;
    /**
     * @var \Lof\Gallery\Helper\Data
     */
    protected $_helperFunction;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Controller\Result\RawFactory $resultRawFactory
     * @param \Lof\Gallery\Model\Config $galleryConfig
     * @param \Magento\Catalog\Model\Product\Media\Config $mediaConfig
     * @param \Magento\Framework\Filesystem $filesystem
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
        \Lof\Gallery\Model\Config $galleryConfig,
        \Lof\Gallery\Helper\Data $functionHelper,
        \Magento\Catalog\Model\Product\Media\Config $mediaConfig,
        \Magento\Framework\Url\DecoderInterface $urlDecoder,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList

    )
    {
        parent::__construct($context);
        $this->_directoryMedia = $filesystem->getDirectoryWrite(DirectoryList::MEDIA);
        $this->urlDecoder = $urlDecoder;
        $this->resultRawFactory = $resultRawFactory;
        $this->galleryConfig = $galleryConfig;
        $this->mediaDirectory = $filesystem->getDirectoryWrite(DirectoryList::MEDIA);
        $this->fileSystem = $filesystem;
        $this->mediaConfig = $mediaConfig;
        $this->_helperFunction = $functionHelper;
    }//end __construct()


    public function base64_to_jpeg($base64_string, $output_file)
    {
        $ifp = fopen($output_file, "wb");
        fwrite($ifp, base64_decode($base64_string));
        fclose($ifp);
        return ($output_file);

    }//end base64_to_jpeg()


    /**
     * @param array $files
     * @return null
     */
    protected function removeDeletedImages($filePath)
    {
        $this->mediaDirectory->delete(Config::MEDIA_PATH . $filePath);

    }//end removeDeletedImages()

    public function explodeStringToArray($work, $str, $number = null)
    {
        $itemArray = explode($work, $str, $number);
        return $itemArray;
    }

    public function implodeArrayToString($work, $array)
    {
        $itemString = implode($work, $array);
        return $itemString;
    }

    /**
     * @return \Magento\Framework\Controller\Result\Raw
     */
    public function execute()
    {
        $post = $this->getRequest()->getPostValue();

        if (isset($post['imagePost'])) {
            if (strpos($post['imagePost'], $this->_helperFunction->getBaseUrl()) !== false) {
                $pathBackend = $this->explodeStringToArray('/', $this->_backendUrl->getUrl('cms/wysiwyg/directive'), -3);

                $pathBackendFinal = $this->implodeArrayToString('/', $pathBackend);
                $pathImageWithoutUrlBackend = str_replace($pathBackendFinal . '/___directive/', '', $post['imagePost']);
                $pathImage = $this->explodeStringToArray('/', $pathImageWithoutUrlBackend, -3);
                $decodeImage = $this->implodeArrayToString('/', $pathImage);
                $path = $this->urlDecoder->decode($decodeImage);
                $pathversion1 = str_replace('{{media url="', '', $path);
                $pathImageOnly = str_replace('"}}', '', $pathversion1);
                $nameFile = substr($pathImageOnly, strpos($pathImageOnly, "/") + 1);
                $temp_file = tempnam(sys_get_temp_dir(), $pathImageOnly);
                $imageInfo = getimagesize($this->_helperFunction->getMediaPath() . $pathImageOnly);
                $typeImage = $imageInfo['mime'];
                $imageSize = get_headers($this->_helperFunction->getMediaPath() . $pathImageOnly, 1);
                if(strpos( $nameFile, '/' ) !== false ){
                    $arrayFile =  explode('/', $nameFile);
                    $numberFile = count($arrayFile);
                    $i = 0;
                    foreach ($arrayFile as $key => $value){
                        if(++$i === $numberFile) {
                            $nameFile = $value;
                        }
                    }

                }
                $_FILES = [
                    'image' => [
                        'file' => $pathImageOnly,
                        'name' => $nameFile,
                        'type' => $typeImage,
                        'tmp_name' => $temp_file,
                        'error' => 0,
                        'size' => $imageSize['Content-Length'],
                        'url' => $this->_helperFunction->getMediaPath() . $pathImageOnly
                    ]
                ];
                
            }
        }

        if (isset($post['urlOnline'])) {
            try {
                $type = substr($post['urlOnline'], strrpos($post['urlOnline'], '.') + 1);
                if ($type == 'jpg' || $type == 'png' || $type == 'jpeg' || $type == 'gif') {
                    $temp_file = tempnam(sys_get_temp_dir(), $post['urlOnline']);
                    $name = substr($post['urlOnline'], strrpos($post['urlOnline'], '/') + 1);

                    $_FILES = [
                        'image' => [
                            'file' => $post['urlOnline'],
                            'type' => 'image/' . $type,
                            'name' => $name,
                            'tmp_name' => $temp_file,
                            'error' => 0,
                            'size' => 'Form another website',
                            'url' => $post['urlOnline']
                        ]
                    ];
                }
            } catch (Exception $e) {
                $result = [
                    'error' => $e->getMessage(),
                    'errorcode' => $e->getCode(),
                ];
            }


        }
        if (isset($post['urlhashTag'])) {
            try {
                $url = $post['urlhashTag'];
                $json = json_decode(file_get_contents($url));
                if ($json->graphql->hashtag->edge_hashtag_to_media->edges) {
                    $itemInstagram = array();
                    $edges = $json->graphql->hashtag->edge_hashtag_to_media->edges;
                    foreach ($edges as $key => $edge) {
                        $itemInstagram[] = [
                            'image' => [
                                'file' => $edge->node->thumbnail_src,
                                'name' => $edge->node->id,
                                'type' => 'Instagram',
                                'tmp_name' => null,
                                'error' => 0,
                                'size' => 'Image From #hashtag',
                                'sizeLabel' => 'Image From #hashtag',
                                'url' => $edge->node->display_url,
                                'description' => $edge->node->edge_media_to_caption->edges[0]->node->text
                            ]
                        ];
                    }
                }
            } catch (\Exception $e) {
                $result = [
                    'error' => 'Url hashtag don\'t not exits',
                    'errorcode' => '699',
                ];
            }

        }

        if (!empty($post) && isset($post["image"]) && $post["imageName"]) {
            try {
                $encodedData = explode(',', $post["image"]);
                $data = base64_decode($encodedData[1]);
                $img = imagecreatefromstring($data);
                if ($img) {
                    $imageName = $post["imageName"];
                    $this->removeDeletedImages($imageName);
                    $imageName = Config::MEDIA_PATH . DIRECTORY_SEPARATOR . $post["imageName"];
                    $fileFullPath = $this->appendAbsoluteFileSystemPath($imageName);
                    imagepng($img, $fileFullPath, 0);
                    imagedestroy($img);
                }
            } catch (\Exception $e) {
                $result = [
                    'error' => $e->getMessage(),
                    'errorcode' => $e->getCode(),
                ];
            }
        }

        try {

            if (!empty($_FILES)) {
                if (isset($post['imagePost']) || isset($post['urlOnline'])) {
                    $result = $_FILES['image'];
                } else {
                    $uploader = $this->_objectManager->create(
                        'Magento\MediaStorage\Model\File\Uploader',
                        ['fileId' => 'image']
                    );
                    $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
                    /*
                        * @var \Magento\Framework\Image\Adapter\AdapterInterface $imageAdapter
                    */
                    $imageAdapter = $this->_objectManager->get('Magento\Framework\Image\AdapterFactory')->create();
                    $uploader->addValidateCallback('catalog_product_image', $imageAdapter, 'validateUploadFile');
                    $uploader->setAllowRenameFiles(true);
                    $uploader->setFilesDispersion(true);

                    /*
                        * @var \Magento\Framework\Filesystem\Directory\Read $mediaDirectory
                    */
                    $mediaDirectory = $this->_objectManager->get('Magento\Framework\Filesystem')
                        ->getDirectoryRead(DirectoryList::MEDIA);
                    $config = $this->_objectManager->get('Magento\Catalog\Model\Product\Media\Config');
                    $result = $uploader->save($mediaDirectory->getAbsolutePath(Config::MEDIA_PATH));
                    unset($result['tmp_name']);
                    unset($result['path']);
                    $result['url'] = $this->galleryConfig->getFileUrl($result['file']);
                }

            } elseif (isset($post['urlhashTag'])) {
                if (isset($itemInstagram)) {
                    $result = $itemInstagram;
                }
            } else {
                $result = [];
            }//end if
        } catch (\Exception $e) {
            $result = [
                'error' => $e->getMessage(),
                'errorcode' => $e->getCode(),
            ];
        }//end try

        /*
            * @var \Magento\Framework\Controller\Result\Raw $response
        */
        $response = $this->resultRawFactory->create();
        $response->setHeader('Content-type', 'text/plain');
        $response->setContents(json_encode($result));

        return $response;

    }//end execute()


    protected function startsWith($haystack, $needle)
    {
        $length = strlen($needle);
        return (substr($haystack, 0, $length) === $needle);

    }//end startsWith()


    /**
     * @param string $localTmpFile
     * @return string
     */
    protected function appendAbsoluteFileSystemPath($localTmpFile)
    {
        /*
            * @var \Magento\Framework\Filesystem\Directory\Read $mediaDirectory
        */
        $mediaDirectory = $this->fileSystem->getDirectoryRead(DirectoryList::MEDIA);
        $pathToSave = $mediaDirectory->getAbsolutePath();
        return $pathToSave . $localTmpFile;

    }//end appendAbsoluteFileSystemPath()


}//end class
