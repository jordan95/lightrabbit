<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Gallery\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Cms\Block\Adminhtml\Page\Grid\Renderer\Action\UrlBuilder;
use Magento\Framework\UrlInterface;
use Lof\Gallery\Model\Config;


class AlbumThumbnail extends Column
{
    /**
     * @var \Lof\Gallery\Model\Config
     */
    protected $galleryConfig;

    /**
     * @var \Lof\Gallery\Helper\Data
     */
    protected $_helperFunction;

    /**
     * AlbumThumbnail constructor.
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param Config $galleryConfig
     * @param \Lof\Gallery\Helper\Data $helperFunction
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        \Lof\Gallery\Model\Config $galleryConfig,
        \Lof\Gallery\Helper\Data $helperFunction,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->galleryConfig = $galleryConfig;
        $this->_helperFunction = $helperFunction;

    }//end __construct()


    /**
     * Prepare Data Source
     *
     * @param  array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            foreach ($dataSource['data']['items'] as & $item) {
                $thumbnailFileUrl = '';
                $result           = [];
                $imageGallery     = $item['media_gallery'];
                if ($imageGallery != '') {
                    if (isset($imageGallery['images']) && is_array($imageGallery['images'])) {
                        foreach ($imageGallery['images'] as $k => &$image) {
                            if ($image['thumbnail']) {
                                $imageNormal = file_exists($this->_helperFunction->appendAbsoluteFileSystemPath(Config::MEDIA_PATH.$image['file']));
                                $imageLibary =  file_exists($this->_helperFunction->appendAbsoluteFileSystemPath($image['file']));

                                if ($imageNormal) {
                                    $thumbnailFileUrl = $this->galleryConfig->getFileUrl($image['file']);
                                }elseif(!$imageNormal && $imageLibary){
                                    $thumbnailFileUrl = $this->galleryConfig->getMediaUrl().$image['file'];
                                } elseif(!$imageNormal && !$imageLibary) {
                                    $thumbnailFileUrl = $image['file'];
                                }else {
                                    $thumbnailFileUrl = '';
                                }
                                break;
                            }
                        }
                    }
                }

                if ($thumbnailFileUrl) {
                    $item[$fieldName.'_html'] = '<div style="text-align: center;"><img class="admin__control-thumbnail" style="width: 200px;" src="'.$thumbnailFileUrl.'"/></div>';
                }
            }
        }//end if

        return $dataSource;

    }//end prepareDataSource()


}//end class
