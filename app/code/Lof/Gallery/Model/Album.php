<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */
namespace Lof\Gallery\Model;

class Album extends \Magento\Framework\Model\AbstractModel
{


    const STATUS_ENABLED       = 1;
    const STATUS_DISABLED      = 0;
    const DISPLAY_PRODUCTPAGE  = 'product';
    const DISPLAY_CATEGORYPAGE = 'category';

    /**
     * @var \Magento\CatalogRule\Model\Rule\Condition\CombineFactory
     */
    protected $_combineFactory;

    /**
     * @var \Magento\CatalogRule\Model\Rule\Action\CollectionFactory
     */
    protected $_actionCollectionFactory;

    /**
     * Catalog config
     *
     * @var \Magento\Catalog\Model\Config
     */
    protected $_catalogConfig;

    /**
     * Store manager
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Filesystem instance
     *
     * @var Filesystem
     */
    protected $_filesystem;

    /**
     * @var \Lof\Gallery\Model\Config
     */
    protected $_galleryConfig;

    /**
     * Album constructor.
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Filesystem $filesystem
     * @param ResourceModel\Album|null $resource
     * @param ResourceModel\Album\Collection|null $resourceCollection
     * @param ResourceModel\Category\CollectionFactory $categoryCollectionFactory
     * @param ResourceModel\Tag\CollectionFactory $tagCollectionFactory
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
     * @param \Magento\Catalog\Model\Product\Visibility $catalogProductVisibility
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Catalog\Model\Config $catalogConfig
     * @param Config $galleryConfig
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Filesystem $filesystem,
        \Lof\Gallery\Model\ResourceModel\Album $resource,
        \Lof\Gallery\Model\ResourceModel\Album\Collection $resourceCollection,
        \Lof\Gallery\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        \Lof\Gallery\Model\ResourceModel\Tag\CollectionFactory $tagCollectionFactory,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Model\Product\Visibility $catalogProductVisibility,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\Config $catalogConfig,
        Config $galleryConfig,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection);
        $this->resource = $resource;
        $this->_filesystem = $filesystem;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->tagCollectionFactory      = $tagCollectionFactory;
        $this->productCollectionFactory  = $productCollectionFactory;
        $this->catalogProductVisibility  = $catalogProductVisibility;
        $this->storeManager   = $storeManager;
        $this->_catalogConfig = $catalogConfig;
        $this->galleryConfig  = $galleryConfig;

    }//end __construct()


    protected function _construct()
    {
        $this->_init('Lof\Gallery\Model\ResourceModel\Album');

    }//end _construct()


    public function getAvailableStatuses()
    {
        return [
                self::STATUS_ENABLED  => __('Enabled'),
                self::STATUS_DISABLED => __('Disabled'),
               ];

    }//end getAvailableStatuses()


    public function getPageTypes()
    {
        return [
                self::DISPLAY_PRODUCTPAGE  => __('Product Pages'),
                self::DISPLAY_CATEGORYPAGE => __('Category Pages'),
               ];

    }//end getPageTypes()


    public function getAlbumLink()
    {
        $baseUrl = $this->storeManager->getStore()->getBaseUrl();
        $route   = $this->galleryConfig->getConfig('general_settings/route');
        $link    = $baseUrl.$route.'/'.$this->getIdentifier();
        return $link;

    }//end getAlbumLink()

    /**
     * Receive page store ids
     *
     * @return int[]
     */
    public function getStores()
    {
        return $this->hasData('stores') ? $this->getData('stores') : $this->getData('store_id');

    }//end getStores()

    public function getMediaGallery()
    {
        $result       = [];
        $imageGallery = $this->getData('media_gallery');
        if ($imageGallery != '') {
            if (is_array($imageGallery)) {
                $newImages = $imageGallery;
            } else {
                $connection   = $this->resource->getConnection();
                $imageGallery = unserialize($imageGallery);
                $select       = $connection->select()
                    ->from($this->resource->getTable(Config::TABLE_GALLERY_ALBUM_IMAGE))
                    ->where(
                        'album_id = ' . $this->getAlbumId()
                    );

                $images = $connection->fetchAll($select);
                $newImages    = [];
                foreach ($imageGallery as $key) {
                    foreach ($images as $k => $image) {
                        if ($image['key'] == $key && $image['params']) {
                            $newImages['images'][$key] = unserialize($image['params']);
                        }
                    }
                }
            }

            if (isset($newImages['images']) && is_array($newImages['images'])) {
                foreach ($newImages['images'] as $k => &$image) {
                    if (isset($image['video_description']) && $image['video_description'] != '') {
                        $image['video_description'] = base64_decode($image['video_description']);
                    }

                    if (isset($image['role']) && $image['role'] != '') {
                        $image['role'] = base64_decode($image['role']);
                    }

                    if (isset($image['description']) && $image['description'] != '') {
                        $image['description'] = base64_decode($image['description']);
                    }

                    if (isset($image['file'])) {
                        $imageNormal = file_exists($this->appendAbsoluteFileSystemPath(Config::MEDIA_PATH.$image['file']));
                        $imageLibary =  file_exists($this->appendAbsoluteFileSystemPath($image['file']));
                        if ($imageNormal) {
                            $image['filelink'] = $this->galleryConfig->getFileUrl($image['file']);
                        }elseif(!$imageNormal && $imageLibary){
                            $image['filelink'] = $this->galleryConfig->getMediaUrl().$image['file'];
                        } elseif(isset($image['file']) && !$imageNormal && !$imageLibary){
                            $image['filelink'] = $image['file'];
                        }
                    } else {
                        $image['filelink'] = '';
                    }
                }

                $result = $newImages['images'];
            }
        }//end if

        return $result;

    }//end getMediaGallery()

    /**
     * @param string $localTmpFile
     * @return string
     */
    protected function appendAbsoluteFileSystemPath($localTmpFile)
    {
        /*
            * @var \Magento\Framework\Filesystem\Directory\Read $mediaDirectory
        */
        $mediaDirectory = $this->_filesystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
        $pathToSave     = $mediaDirectory->getAbsolutePath();
        return $pathToSave.$localTmpFile;

    }//end appendAbsoluteFileSystemPath()
    public function getImages()
    {
        return $this->getMediaGallery();

    }//end getImages()


    public function getCategoryCollection()
    {
        $categories = $this->getData('categories');
        $categories = str_replace(['[', ']', '"'], ['', '', ''], $categories);
        $categories = explode(",", $categories);
        $collection = $this->categoryCollectionFactory->create()
            ->addFieldToFilter('is_active', ['eq' => 1])
            ->addFieldToFilter('category_id', ['in' => $categories]);
        return $collection;

    }//end getCategoryCollection()


    public function getTagCollection()
    {
        $collection = $this->tagCollectionFactory->create();
        $collection->getSelect()->join(
            ['tag' => $this->resource->getTable(Config::TABLE_GALLERY_ALBUM_TAG)],
            'main_table.tag_id = tag.tag_id',
            []
        )
            ->where('tag.album_id = (?)', $this->getAlbumId())
            ->group(
                'main_table.tag_id'
            );
        return $collection;

    }//end getTagCollection()


    public function getImageCount()
    {
        $images = $this->getMediaGallery();
        $count  = 0;
        foreach ($images as $image) {
            if (isset($image['disabled']) && (int) $image['disabled'] == 0) {
                $count++;
            }
        }

        return $count;

    }//end getImageCount()


    public function getProductCollection()
    {
        /*
            *
            *
            * @var $collection \Magento\Catalog\Model\ResourceModel\Product\Collection
        */
        $collection = $this->productCollectionFactory->create();
        $collection->setVisibility($this->catalogProductVisibility->getVisibleInCatalogIds());

        $collection = $this->_addProductAttributesAndPrices($collection)
            ->addStoreFilter()
            ->setCurPage(1);

        $collection->getSelect()->join(
            ['p' => $this->resource->getTable(Config::TABLE_GALLERY_ALBUM_PRODUCT)],
            'e.entity_id = p.entity_id',
            []
        )
            ->where('p.album_id = (?)', $this->getAlbumId())
            ->group(
                'e.entity_id'
            )
            ->order('p.position ASC');

        return $collection;

    }//end getProductCollection()


    public function getThumbnail()
    {
        $images    = $this->getMediaGallery();
        $thumbnail = [];
        if (!empty($images)) {
            foreach ($images as $image) {
                if (isset($image['thumbnail']) && $image['thumbnail']) {
                    $thumbnail = $image;
                    break;
                }
            }
        }

        return $thumbnail;

    }//end getThumbnail()


    /**
     * Add all attributes and apply pricing logic to products collection
     * to get correct values in different products lists.
     * E.g. crosssells, upsells, new products, recently viewed
     *
     * @param  \Magento\Catalog\Model\ResourceModel\Product\Collection $collection
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    protected function _addProductAttributesAndPrices(
        \Magento\Catalog\Model\ResourceModel\Product\Collection $collection
    ) {
        return $collection
            ->addMinimalPrice()
            ->addFinalPrice()
            ->addTaxPercents()
            ->addAttributeToSelect($this->_catalogConfig->getProductAttributes())
            ->addUrlRewrite();

    }//end _addProductAttributesAndPrices()


    public function getAlbumImage()
    {
        $images    = $this->getMediaGallery();
        $baseImage = [];
        if (!empty($images)) {
            foreach ($images as $image) {
                if (isset($image['image']) && $image['image']) {
                    $baseImage = $image;
                    break;
                }
            }
        }

        return $baseImage;

    }//end getAlbumImage()


}//end class
