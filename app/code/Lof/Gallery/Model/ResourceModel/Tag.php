<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Gallery\Model\ResourceModel;

use Lof\Gallery\Model\Config;

class Tag extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Store manager
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * Tag constructor.
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param null $connectionName
     */
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        $connectionName = null
    ) {
        parent::__construct($context, $connectionName);
        $this->_storeManager = $storeManager;

    }//end __construct()


    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(Config::TABLE_GALLERY_TAG, 'tag_id');

    }//end _construct()


    /**
     * Process block data before deleting
     *
     * @param  \Magento\Framework\Model\AbstractModel $object
     * @return \Magento\Cms\Model\ResourceModel\Page
     */
    protected function _beforeDelete(\Magento\Framework\Model\AbstractModel $object)
    {
        $condition = ['tag_id = ?' => (int) $object->getId()];
        $this->getConnection()->delete($this->getTable(Config::TABLE_GALLERY_ALBUM_TAG), $condition);
        return parent::_beforeDelete($object);

    }//end _beforeDelete()


    /**
     * Load an object using 'identifier' field if there's no field specified and value is not numeric
     *
     * @param  \Magento\Framework\Model\AbstractModel $object
     * @param  mixed                                  $value
     * @param  string                                 $field
     * @return $this
     */
    public function load(\Magento\Framework\Model\AbstractModel $object, $value, $field = null)
    {
        if (!is_numeric($value) && $field === null) {
            $field = 'alias';
        }

        return parent::load($object, $value, $field);

    }//end load()


    public function getIsUniqueBlockToStores(\Magento\Framework\Model\AbstractModel $object)
    {
        $select = $this->getConnection()->select()->from(
            ['lgbc' => $this->getMainTable()]
        )->where(
            'lgbc.alias = ?',
            $object->getData('alias')
        );
        if ($object->getId()) {
            $select->where('lgbc.tag_id <> ?', $object->getId());
        }

        if ($this->getConnection()->fetchRow($select)) {
            return false;
        }

            return true;

    }//end getIsUniqueBlockToStores()


    /**
     * Perform operations after object save
     *
     * @param  \Magento\Framework\Model\AbstractModel $object
     * @return $this
     */
    protected function _afterSave(\Magento\Framework\Model\AbstractModel $object)
    {
        // Category posts
        if($albums = $object->getData('albums')) {
            $table = $this->getTable(Config::TABLE_GALLERY_ALBUM_TAG);
            $where = ['tag_id = ?' => (int) $object->getId()];
            $this->getConnection()->delete($table, $where);
            $data = [];

            foreach ($albums as $k => $_album) {
                $data[] = [
                           'album_id' => $k,
                           'tag_id'   => (int) $object->getId(),
                           'position' => $_album['position'],
                          ];
            }

            $this->getConnection()->insertMultiple($table, $data);
        }

        return parent::_afterSave($object);

    }//end _afterSave()


    /**
     * Perform operations after object load
     *
     * @param  \Magento\Framework\Model\AbstractModel $object
     * @return $this
     */
    protected function _afterLoad(\Magento\Framework\Model\AbstractModel $object)
    {
        if ($id = $object->getId()) {
            $connection = $this->getConnection();
            $select     = $connection->select()
                ->from($this->getTable(Config::TABLE_GALLERY_ALBUM_TAG))
                ->where(
                    'tag_id = '.(int) $id
                );
            $albums     = $connection->fetchAll($select);
            $object->setData('tag_albums', $albums);
        }

        return parent::_afterLoad($object);

    }//end _afterLoad()


    protected function _beforeSave(\Magento\Framework\Model\AbstractModel $object)
    {
        if (!$this->getIsUniqueBlockToStores($object)) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('A tag identifier with the same properties already exists in the selected store.')
            );
        }

        return $this;

    }//end _beforeSave()


}//end class
