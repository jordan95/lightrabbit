<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Gallery\Model\ResourceModel;

use Lof\Gallery\Model\Config;

class Category extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Store manager
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;


    /**
     * Category constructor.
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param null $connectionName
     */
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        $connectionName = null
    ) {
        parent::__construct($context, $connectionName);
        $this->_storeManager = $storeManager;

    }//end __construct()


    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(Config::TABLE_GALLERY_CATEGORY, 'category_id');

    }//end _construct()


    /**
     * Perform operations after object save
     *
     * @param  \Magento\Framework\Model\AbstractModel $object
     * @return $this
     */
    protected function _afterSave(\Magento\Framework\Model\AbstractModel $object)
    {
        // Category posts
        if ($albums = $object->getData('albums')) {
            $table = $this->getTable(Config::TABLE_GALLERY_ALBUM_CATEGORY);
            $where = ['entity_id = ?' => (int) $object->getId()];
            $this->getConnection()->delete($table, $where);
            $data = [];

            foreach ($albums as $k => $_album) {
                $data[] = [
                           'album_id'  => $k,
                           'entity_id' => (int) $object->getId(),
                           'position'  => $_album['position'],
                          ];
            }

            $this->getConnection()->insertMultiple($table, $data);
        }

        if ($stores = $object->getStores()) {
            $table = $this->getTable(Config::TABLE_GALLERY_CATEGORY_STORE);
            $where = ['entity_id = ?' => (int) $object->getId()];
            $this->getConnection()->delete($table, $where);
            if ($stores) {
                $data = [];
                foreach ($stores as $storeId) {
                    $data[] = [
                               'entity_id' => (int) $object->getId(),
                               'store_id'  => (int) $storeId,
                              ];
                }

                $this->getConnection()->insertMultiple($table, $data);
            }
        }

        return parent::_afterSave($object);

    }//end _afterSave()


    /**
     * Load an object using 'identifier' field if there's no field specified and value is not numeric
     *
     * @param  \Magento\Framework\Model\AbstractModel $object
     * @param  mixed                                  $value
     * @param  string                                 $field
     * @return $this
     */
    public function load(\Magento\Framework\Model\AbstractModel $object, $value, $field = null)
    {
        if (!is_numeric($value) && $field === null) {
            $field = 'identifier';
        }

        return parent::load($object, $value, $field);

    }//end load()


    /**
     * Process block data before deleting
     *
     * @param  \Magento\Framework\Model\AbstractModel $object
     * @return \Magento\Cms\Model\ResourceModel\Page
     */
    protected function _beforeDelete(\Magento\Framework\Model\AbstractModel $object)
    {
        $condition = ['entity_id = ?' => (int) $object->getId()];
        $this->getConnection()->delete($this->getTable(Config::TABLE_GALLERY_CATEGORY_STORE), $condition);
        $this->getConnection()->delete($this->getTable(Config::TABLE_GALLERY_ALBUM_CATEGORY), $condition);
        return parent::_beforeDelete($object);

    }//end _beforeDelete()


    /**
     * Perform operations after object load
     *
     * @param  \Magento\Framework\Model\AbstractModel $object
     * @return $this
     */
    protected function _afterLoad(\Magento\Framework\Model\AbstractModel $object)
    {
        if ($id = $object->getId()) {
            $connection = $this->getConnection();
            $select     = $connection->select()
                ->from($this->getTable(Config::TABLE_GALLERY_ALBUM_CATEGORY))
                ->where(
                    'entity_id = '.(int) $id
                );
            $album      = $connection->fetchAll($select);
            $object->setData('album', $album);
        }

        if ($object->getId()) {
            $stores = $this->lookupStoreIds($object->getId());
            $object->setData('store_id', $stores);
            $object->setData('stores', $stores);
        }

        if ($id = $object->getId()) {
            $connection = $this->getConnection();
            $select     = $connection->select()
                ->from($this->getTable(Config::TABLE_GALLERY_CATEGORY_STORE))
                ->where(
                    'entity_id = '.(int) $id
                );
            $categories = $connection->fetchAll($select);
            $object->setData('categories', $categories);
        }

            return parent::_afterLoad($object);

    }//end _afterLoad()


        /**
         * Get store ids to which specified item is assigned
         *
         * @param  int $id
         * @return array
         */
    public function lookupStoreIds($id)
    {
        $connection = $this->getConnection();

        $select    = $connection->select()->from(
            $this->getTable(Config::TABLE_GALLERY_CATEGORY_STORE),
            'store_id'
        )->where(
            'entity_id = :entity_id'
        );
            $binds = [':entity_id' => (int) $id];
            return $connection->fetchCol($select, $binds);

    }//end lookupStoreIds()


    public function getIsUniqueBlockToStores(\Magento\Framework\Model\AbstractModel $object)
    {
        $select = $this->getConnection()->select()->from(
            ['lgbc' => $this->getMainTable()]
        )->where(
            'lgbc.identifier = ?',
            $object->getData('identifier')
        );
        if ($object->getId()) {
            $select->where('lgbc.category_id <> ?', $object->getId());
        }

        if ($this->getConnection()->fetchRow($select)) {
            return false;
        }

            return true;

    }//end getIsUniqueBlockToStores()


    protected function _beforeSave(\Magento\Framework\Model\AbstractModel $object)
    {
        if (!$this->getIsUniqueBlockToStores($object)) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('A category identifier with the same properties already exists in the selected store.')
            );
        }

        return $this;

    }//end _beforeSave()

}//end class
