<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Gallery\Model\ResourceModel\Category;

use \Lof\Gallery\Model\ResourceModel\AbstractCollection;
use Lof\Gallery\Model\Config;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'category_id';


    /**
     * Perform operations after collection load
     *
     * @return $this
     */
    protected function _afterLoad()
    {
        $this->loadStore();
        return parent::_afterLoad();

    }//end _afterLoad()


    /**
     * [performAfterLoad description]
     *
     * @param  [type] $tableName  [description]
     * @param  [type] $columnName [description]
     * @return [type]             [description]
     */
    protected function loadStore()
    {
        $columnName = 'category_id';
        $tableName  = Config::TABLE_GALLERY_CATEGORY_STORE;
        $items      = $this->getColumnValues($columnName);
        if (count($items)) {
            $connection = $this->getConnection();
            $select     = $connection->select()->from(['gallery_entity_store' => $this->getTable($tableName)])
                ->where('gallery_entity_store.entity_id IN (?)', $items);
            $result     = $connection->fetchPairs($select);
            if ($result) {
                foreach ($this as $item) {
                    $entityId = $item->getData($columnName);
                    if (!isset($result[$entityId])) {
                        continue;
                    }

                    if ($result[$entityId] == 0) {
                        $stores    = $this->storeManager->getStores(false, true);
                        $storeId   = current($stores)->getId();
                        $storeCode = key($stores);
                    } else {
                        $storeId   = $result[$item->getData($columnName)];
                    }

                    $item->setData('_first_store_id', $storeId);
                    $item->setData('store_id', [$result[$entityId]]);
                }
            }//end if
        }//end if

    }//end loadStore()


    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Lof\Gallery\Model\Category', 'Lof\Gallery\Model\ResourceModel\Category');
        $this->_map['fields']['store'] = 'store_table.store_id';

    }//end _construct()


    /**
     * Returns pairs category_id - title
     *
     * @return array
     */
    public function toOptionArray()
    {
        return $this->_toOptionArray('category_id', 'name');

    }//end toOptionArray()


    /**
     * Add filter by store
     *
     * @param  int|array|\Magento\Store\Model\Store $store
     * @param  bool                                 $withAdmin
     * @return $this
     */
    public function addStoreFilter($store, $withAdmin = true)
    {
        $this->performAddStoreFilter($store, $withAdmin);

        return $this;

    }//end addStoreFilter()


    /**
     * Join store relation table if there is store filter
     *
     * @return void
     */
    protected function _renderFiltersBefore()
    {
        $this->joinStoreRelationTable(Config::TABLE_GALLERY_CATEGORY_STORE, 'category_id', 'entity_id');

    }//end _renderFiltersBefore()


}//end class
