<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Gallery\Model\ResourceModel;

use Magento\Framework\Model\AbstractModel;
use Lof\Gallery\Model\Config;

class Album extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var \Lof\Gallery\Model\ResourceModel\Tag\CollectionFactory
     */
    private $tagCollectionFactory;

    /**
     * @var \Lof\Gallery\Model\Tag
     */
    private $galleryTag;


    /**
     * Album constructor.
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param Tag\CollectionFactory $tagCollectionFactory
     * @param \Lof\Gallery\Model\Tag $galleryTag
     * @param null $connectionName
     */
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\Module\Manager $moduleManager,
        \Lof\Gallery\Model\ResourceModel\Tag\CollectionFactory $tagCollectionFactory,
        \Lof\Gallery\Model\Tag $galleryTag,
        $connectionName = null
    ) {
        parent::__construct($context, $connectionName);
        $this->objectManager        = $objectManager;
        $this->tagCollectionFactory = $tagCollectionFactory;
        $this->galleryTag           = $galleryTag;
        $this->_moduleManager       = $moduleManager;

    }//end __construct()


    /**
     * Process block data before deleting
     *
     * @param  \Magento\Framework\Model\AbstractModel $object
     * @return \Magento\Cms\Model\ResourceModel\Page
     */
    protected function _beforeDelete(\Magento\Framework\Model\AbstractModel $object)
    {
        $condition = ['album_id = ?' => (int) $object->getId()];
        $this->getConnection()->delete($this->getTable(Config::TABLE_GALLERY_ALBUM_CATEGORY), $condition);
        $this->getConnection()->delete($this->getTable(Config::TABLE_GALLERY_ALBUM_IMAGE), $condition);
        $this->getConnection()->delete($this->getTable(Config::TABLE_GALLERY_ALBUM_PRODUCT), $condition);
        $this->getConnection()->delete($this->getTable(Config::TABLE_GALLERY_ALBUM_TAG), $condition);
        $this->getConnection()->delete($this->getTable(Config::TABLE_GALLERY_ALBUM_STORE), $condition);
        $this->getConnection()->delete($this->getTable(Config::TABLE_GALLERY_ALBUM_POST), $condition);
        $this->getConnection()->delete($this->getTable(Config::TABLE_GALLERY_ALBUM_CORESTORE), $condition);
        return parent::_beforeDelete($object);

    }//end _beforeDelete()


    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(Config::TABLE_GALLERY_ALBUM, 'album_id');

    }//end _construct()


    public function getIsUniqueBlockToStores(AbstractModel $object)
    {
        $select = $this->getConnection()->select()->from(
            ['lgb' => $this->getMainTable()]
        )->where(
            'lgb.identifier = ?',
            $object->getData('identifier')
        );
        if ($object->getId()) {
            $select->where('lgb.album_id <> ?', $object->getId());
        }

        if ($this->getConnection()->fetchRow($select)) {
            return false;
        }

            return true;

    }//end getIsUniqueBlockToStores()


    /**
     * Perform operations after object save
     *
     * @param  \Magento\Framework\Model\AbstractModel $object
     * @return $this
     */
    protected function _afterSave(AbstractModel $object)
    {
        if(!$object->getData("isfrontend")) {
            $postData = $object->getData();

            if (isset($postData['categories'])) {
                $categories = $postData['categories'];
                $categories = explode(",", str_replace(['[', ']', '"'], ['', '', ''], $categories));

                $table = $this->getTable(Config::TABLE_GALLERY_ALBUM_CATEGORY);
                $where = ['album_id = ?' => (int) $object->getId()];
                $this->getConnection()->delete($table, $where);
                $data = [];
                foreach ($categories as $catId) {
                    if ($catId) {
                        $data[] = [
                                   'album_id'  => (int) $object->getId(),
                                   'entity_id' => $catId,
                                   'position'  => 0,
                                  ];
                    }
                }

                if (!empty($data)) {
                    $this->getConnection()->insertMultiple($table, $data);
                }
            }//end if

            // Related Products
            if (isset($postData['related_products'])) {
                $productsRelated = $postData['related_products'];
                $table           = $this->getTable(Config::TABLE_GALLERY_ALBUM_PRODUCT);
                $where           = ['album_id = ?' => (int) $object->getId()];
                $this->getConnection()->delete($table, $where);
                $data = [];
                if(!empty($productsRelated)) {
                    foreach ($productsRelated as $k => $product) {
                        $data[] = [
                                   'album_id'  => $object->getId(),
                                   'entity_id' => $k,
                                   'position'  => $product['productposition'],
                                  ];
                    }

                    if (!empty($data)) {
                        $this->getConnection()->insertMultiple($table, $data);
                    }
                }
            }

            // Related Posts
            if ($this->_moduleManager->isEnabled('Ves_Blog') && isset($postData['related_posts'])) {
                $postsRelated = $postData['related_posts'];
                $table        = $this->getTable(Config::TABLE_GALLERY_ALBUM_POST);
                $where        = ['album_id = ?' => (int) $object->getId()];
                $this->getConnection()->delete($table, $where);
                $data = [];
                if(!empty($postsRelated)) {
                    foreach ($postsRelated as $k => $post) {
                        $data[] = [
                                   'album_id' => $object->getId(),
                                   'post_id'  => $k,
                                   'position' => $post['postposition'],
                                  ];
                    }

                    if (!empty($data)) {
                        $this->getConnection()->insertMultiple($table, $data);
                    }
                }
            }

            // Related Stores
            if ($this->_moduleManager->isEnabled('Lof_StoreLocator') && isset($postData['related_stores'])) {
                $storesRelated = $postData['related_stores'];
                $table         = $this->getTable(Config::TABLE_GALLERY_ALBUM_STORE);
                $where         = ['album_id = ?' => $object->getId()];
                $this->getConnection()->delete($table, $where);
                $data = [];
                if(!empty($storesRelated)) {
                    foreach ($storesRelated as $k => $store) {
                        $data[] = [
                                   'album_id'        => (int) $object->getId(),
                                   'storelocator_id' => $k,
                                   'position'        => $store['storeposition'],
                                  ];
                    }

                    $this->getConnection()->insertMultiple($table, $data);
                }
            }

            if (isset($postData['tags'])) {
                $tags  = $postData['tags'];
                $tags  = explode(",", $tags);
                $table = $this->getTable(Config::TABLE_GALLERY_ALBUM_TAG);
                $where = ['album_id = ?' => (int) $object->getId()];
                $this->getConnection()->delete($table, $where);
                if(!empty($tags)) {
                    $data = [];
                    foreach ($tags as $k => $_tag) {
                        if ($_tag) {
                            $alias = strtolower(str_replace(["_", " "], "-", trim($_tag)));

                            $tag = $this->tagCollectionFactory->create()->addFieldToFilter('alias', $alias)->getFirstItem();
                            if ($alias) {
                                if (!$tag->getTagId()) {
                                    $tagData = [
                                                'alias'     => $alias,
                                                'name'      => $alias,
                                                'is_active' => 1,
                                               ];
                                    $tag     = $this->objectManager->create('Lof\Gallery\Model\Tag');
                                    $tag->setData($tagData)->save();
                                }

                                $data[] = [
                                           'album_id' => $object->getId(),
                                           'tag_id'   => $tag->getTagId(),
                                          ];
                            }
                        }//end if
                    }//end foreach

                    if (!empty($data)) {
                        $this->getConnection()->insertMultiple($table, $data);
                    }
                }//end if
            }//end if

            if (isset($postData['images'])) {
                $images = $postData['images'];
                $table  = $this->getTable(Config::TABLE_GALLERY_ALBUM_IMAGE);
                $where  = ['album_id = ?' => (int) $object->getId()];
                $this->getConnection()->delete($table, $where);
                $data = [];
                foreach ($images as $k => $image) {
                    $data[] = [
                               'album_id' => $object->getId(),
                               'key'      => $k,
                               'params'   => serialize($image),
                              ];
                }

                if (!empty($data)) {
                    $this->getConnection()->insertMultiple($table, $data);
                }
            }

            if ($stores = $object->getStores()) {
                $table = $this->getTable(Config::TABLE_GALLERY_ALBUM_CORESTORE);
                $where = ['album_id = ?' => (int) $object->getId()];
                $this->getConnection()->delete($table, $where);
                if ($stores) {
                    $data = [];
                    foreach ($stores as $storeId) {
                        $data[] = [
                                   'album_id' => (int) $object->getId(),
                                   'store_id'  => (int) $storeId,
                                  ];
                    }

                    $this->getConnection()->insertMultiple($table, $data);
                }
            }
        } else {
        }//end if
        return parent::_afterSave($object);

    }//end _afterSave()


    protected function _beforeSave(AbstractModel $object)
    {
        $postData     = $object->getData();
        if (isset($postData['media_gallery'])) {
            $mediaGallery = $postData['media_gallery'];
            $object->setData('media_gallery', serialize(array_keys($mediaGallery['images'])));
        }
        if (!$this->getIsUniqueBlockToStores($object)) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('A album identifier with the same properties already exists in the selected store.')
            );
        }

        if (($pageTypes = $object->getData('page_type')) && is_array($pageTypes)) {
            $object->setData('page_type', implode(',', $pageTypes));
        }

        return $this;

    }//end _beforeSave()


     /**
      * Perform operations after object load
      *
      * @param  \Magento\Framework\Model\AbstractModel $object
      * @return $this
      */
    protected function _afterLoad(AbstractModel $object)
    {
        if ($id = $object->getId()) {
            /*
            *
            * RELATED CATEGORIES
            */
            $connection = $this->getConnection();
            $select     = $connection->select()
                ->from($this->getTable(Config::TABLE_GALLERY_ALBUM_CATEGORY))
                ->where(
                    'album_id = '.$id
                );
            $products   = $connection->fetchAll($select);
            $object->setData('categories', $products);

            /*
            *
            * RELATED PRODUCTS
            */
            $connection = $this->getConnection();
            $select     = $connection->select()
                ->from($this->getTable(Config::TABLE_GALLERY_ALBUM_PRODUCT))
                ->where(
                    'album_id = '.$id
                );
            $products   = $connection->fetchAll($select);
            $object->setData('related_products', $products);

            /*
                *
                * RELATED POST
            */
            $select = $connection->select()
                ->from($this->getTable(Config::TABLE_GALLERY_ALBUM_POST))
                ->where(
                    'album_id = '.$id
                );
            $posts  = $connection->fetchAll($select);
            $object->setData('related_posts', $posts);

            /*
                *
                * RELATED STORE
            */
            $select = $connection->select()
                ->from($this->getTable(Config::TABLE_GALLERY_ALBUM_STORE))
                ->where(
                    'album_id = '.$id
                );
            $stores = $connection->fetchAll($select);
            $object->setData('related_stores', $stores);

            /*
                *
                * GALLERY
            */
            $select = $connection->select()
                ->from($this->getTable(Config::TABLE_GALLERY_ALBUM_TAG))
                ->where(
                    'album_id = '.$id
                );
            $tags   = $connection->fetchAll($select);
            $object->setData('tags', $tags);

            if ($mediaGallery = $object->getData('media_gallery')) {
                $mediaGallery = unserialize($mediaGallery);

                $select    = $connection->select()
                    ->from($this->getTable(Config::TABLE_GALLERY_ALBUM_IMAGE))
                    ->where(
                        'album_id = '.$id
                    );
                $images    = $connection->fetchAll($select);
                $newImages = [];
                foreach ($mediaGallery as $key) {
                    foreach ($images as $image) {
                        if ($image['key'] == $key && $image['params']) {
                            $newImages['images'][$key] = unserialize($image['params']);
                        }
                    }
                }

                $object->setData('media_gallery', $newImages);
            }

            /*
                *
                * Album by core stores
            */
            if ($object->getId()) {
                $stores = $this->lookupStoreIds($object->getId());
                $object->setData('store_id', $stores);
                $object->setData('stores', $stores);
            }
        }//end if

        return parent::_afterLoad($object);

    }//end _afterLoad()
    /**
     * Get store ids to which specified item is assigned
     *
     * @param  int $id
     * @return array
     */
    public function lookupStoreIds($id)
    {
        $connection = $this->getConnection();

        $select    = $connection->select()->from(
            $this->getTable(Config::TABLE_GALLERY_ALBUM_CORESTORE),
            'store_id'
        )->where(
            'album_id = :entity_id'
        );
        $binds = [':entity_id' => (int) $id];
        return $connection->fetchCol($select, $binds);

    }//end lookupStoreIds()

}//end class
