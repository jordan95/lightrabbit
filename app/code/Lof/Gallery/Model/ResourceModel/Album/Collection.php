<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Gallery\Model\ResourceModel\Album;

use \Lof\Gallery\Model\ResourceModel\AbstractCollection;
use Lof\Gallery\Model\Config;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'album_id';
    protected $joined_filter_added = false;
    /**
     * Define resource model
     *
     * @return void
     */


     /**
      * Perform operations after collection load
      *
      * @return $this
      */
    protected function _afterLoad()
    {
        $this->loadStore();
        $this->loadProducts();
        $this->loadImages();
        return parent::_afterLoad();

    }//end _afterLoad()

    /**
     * [performAfterLoad description]
     *
     * @param  [type] $tableName  [description]
     * @param  [type] $columnName [description]
     * @return [type]             [description]
     */
    public function loadStore()
    {
        $columnName = 'album_id';
        $tableName  = Config::TABLE_GALLERY_ALBUM_CORESTORE;
        $items      = $this->getColumnValues($columnName);
        if (count($items)) {
            $connection = $this->getConnection();
            $select     = $connection->select()->from(['gallery_entity_store' => $this->getTable($tableName)])
                ->where('gallery_entity_store.album_id IN (?)', $items);
            $result     = $connection->fetchPairs($select);
            if ($result) {
                foreach ($this as $item) {
                    $entityId = $item->getData($columnName);
                    if (!isset($result[$entityId])) {
                        $item->setData('_first_store_id', 0);
                        $item->setData('store_id', 0);
                        continue;
                    }

                    if ($result[$entityId] == 0) {
                        $stores    = $this->storeManager->getStores(false, true);
                        $storeId   = current($stores)->getId();
                        $storeCode = key($stores);
                    } else {
                        $storeId   = $result[$item->getData($columnName)];
                    }

                    $item->setData('_first_store_id', $storeId);
                    $item->setData('store_id', [$result[$entityId]]);
                }
                $this->store_filter_added = true;
            }//end if
        }//end if
        return $this;
    }//end loadStore()
    protected function _construct()
    {
        $this->_init('Lof\Gallery\Model\Album', 'Lof\Gallery\Model\ResourceModel\Album');
        $this->_map['fields']['store'] = 'store_table.store_id';

    }//end _construct()


      /**
       * Returns pairs category_id - title
       *
       * @return array
       */
    public function toOptionArray()
    {
        return $this->_toOptionArray('album_id', 'title');

    }//end toOptionArray()


    /**
     * Add link attribute to filter.
     *
     * @param  string $code
     * @param  array  $condition
     * @return $this
     */
    public function addStoreFilter($store, $withAdmin = true)
    {
        if(!$this->joined_filter_added) {
            $this->joinStoreRelationTable(Config::TABLE_GALLERY_ALBUM_CORESTORE, "album_id");
            $this->joined_filter_added = true;
        }
        $this->performAddStoreFilter($store, $withAdmin);

        return $this;

    }//end addStoreFilter()


    public function addLinkAttributeToFilter($code, $condition)
    {

        if($code == 'position') {
            $connection = $this->getConnection();
            $where      = '';
            if(isset($condition['from'])) {
                $where .= 'position >= '.$condition['from'].' AND ';
            }

            if(isset($condition['to'])) {
                $where .= ' position <= '.$condition['to'].' AND ';
            }

            if($where != '') {
                $where .= ' category_id = '.$condition['category_id'];
            }

            $select  = 'SELECT album_id FROM '.$this->getTable(Config::TABLE_GALLERY_CATEGORY).' WHERE '.$where;
            $postIds = $connection->fetchCol($select);
            $this->getSelect()->where('album_id IN (?)', $postIds);
        }

        return $this;

    }//end addLinkAttributeToFilter()


    public function addLinkAttributeToFilterProducts($code, $condition)
    {
        if($code == 'position') {
            $connection = $this->getConnection();
            $where      = '';
            if(isset($condition['from'])) {
                $where .= 'position >= '.$condition['from'].' AND ';
            }

            if(isset($condition['to'])) {
                $where .= ' position <= '.$condition['to'].' AND ';
            }

            if($where != '') {
                $where .= ' entity_id = '.$condition['entity_id'];
            }

            $select  = 'SELECT album_id FROM '.$this->getTable(Config::TABLE_GALLERY_ALBUM_PRODUCT).' WHERE '.$where;
            $postIds = $connection->fetchCol($select);
            $this->getSelect()->where('album_id IN (?)', $postIds);
        }

        return $this;

    }//end addLinkAttributeToFilterProducts()


    public function addAlbumToFilter($productId)
    {
        $album = [];
        if ($productId) {
            $connection = $this->getConnection();
            $select     = $connection->select()
                ->from($this->getTable(Config::TABLE_GALLERY_ALBUM_PRODUCT))
                ->where(
                    'entity_id = '.(int) $productId
                );
            $album = $connection->fetchAll($select);
        }

        return $album;

    }//end addAlbumToFilter()

    /**
     * Add filter by tag id
     *
     * @param  int $tagId
     * @return \Lof\Gallery\Model\ResourceModel\Album\Collection
     */
    public function addFilterByTag($tagId = 0) {
        if($tagId) {
            $this->getSelect()->joinLeft(
                [
                 'p' => $this->getTable(Config::TABLE_GALLERY_ALBUM_TAG),
                ],
                'p.album_id = main_table.album_id',
                ['album_id' => 'album_id']
            )->where('p.tag_id = (?)', $tagId);
        }
        return $this;
    }
    /**
     * Perform operations after collection load
     *
     * @return void
     */
    protected function loadProducts()
    {
        $items = $this->getColumnValues("album_id");
        if (count($items)) {
            $connection = $this->getConnection();
            $select     = 'SELECT * FROM '.$this->getTable(Config::TABLE_GALLERY_ALBUM_PRODUCT);
            $products   = $connection->fetchAll($select);
            foreach ($this as $item) {
                $select   = 'SELECT entity_id FROM '.$this->getTable(Config::TABLE_GALLERY_ALBUM_PRODUCT).' WHERE album_id = '.$item->getData("album_id").' ORDER BY position DESC ';
                $products = $connection->fetchCol($select);
                $item->setData('products', $products);
            }
        }

    }//end loadProducts()


    protected function loadImages()
    {
        $items = $this->getColumnValues("album_id");
        if (count($items)) {
            $connection = $this->getConnection();
            foreach ($this as $item) {
                if ($mediaGallery = $item->getData('media_gallery')) {
                    $mediaGallery = unserialize($mediaGallery);

                    $select    = $connection->select()
                        ->from($this->getTable(Config::TABLE_GALLERY_ALBUM_IMAGE))
                        ->where(
                            'album_id = '.$item->getId()
                        );
                    $images    = $connection->fetchAll($select);
                    $newImages = [];
                    foreach ($mediaGallery as $key) {
                        foreach ($images as $image) {
                            if ($image['key'] == $key && $image['params']) {
                                $newImages['images'][$key] = unserialize($image['params']);
                                $newImages['images'][$key]['image_id'] = $image['image_id'];
                            }
                        }
                    }

                    $item->setData('media_gallery', $newImages);
                }
            }//end foreach
        }//end if

    }//end loadImages()
     /**
     * Join album category relation table if there is store filter
     *
     */
    public function joinCategoryRelationTable()
    {
        $this->getSelect()->join(
            ['album_category_table' => $this->getTable("lof_gallery_album_category")],
            'main_table.album_id = album_category_table.album_id',
            []
        )->group(
            'main_table.album_id'
        );
        parent::_renderFiltersBefore();
        return $this;

    }//end joinCategoryRelationTable()

}//end class
