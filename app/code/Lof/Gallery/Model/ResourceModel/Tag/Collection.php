<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Gallery\Model\ResourceModel\Tag;

use \Lof\Gallery\Model\ResourceModel\AbstractCollection;
use \Lof\Gallery\Model\Config;

class Collection extends AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'category_id';

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * Collection constructor.
     * @param \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param null $connection
     * @param null $resource
     */
    public function __construct(
        \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        $connection = null,
        $resource = null) {
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $storeManager, $connection, $resource);
        $this->_storeManager        = $storeManager;
    }

    protected function _construct()
    {
        $this->_init('Lof\Gallery\Model\Tag', 'Lof\Gallery\Model\ResourceModel\Tag');

    }//end _construct()


    /**
     * Perform operations after collection load
     *
     * @return $this
     */
    protected function _afterLoad()
    {
        $this->getTotalAlbum();
        return parent::_afterLoad();

    }//end _afterLoad()


    public function getStoreId()
    {
        return $this->_storeManager->getStore()->getId();
    }

    /**
     * Perform operations after collection load
     *
     * @param  string $tableName
     * @param  string $columnName
     * @return void
     */
    protected function getTotalAlbum()
    {
        $items = $this->getColumnValues("tag_id");
        if (count($items)) {
            $connection = $this->getConnection();
            foreach ($this as $item) {
                $select = $connection->select()->from(array('main_table' => $this->getTable(Config::TABLE_GALLERY_ALBUM_TAG)))->join(
                ['store_table' => $this->getTable(Config::TABLE_GALLERY_ALBUM_CORESTORE)],
                'main_table.album_id = store_table.album_id',
                [])
                    ->where('tag_id = (?)', $item->getTagId())
                    ->where('store_id', $this->getStoreId())
                    ->order('position ASC')
                    ->order('main_table.album_id ASC')
                    ->group('main_table.album_id');

                $result = $connection->fetchAll($select);
                $albums = [];
                foreach ($result as $row) {
                    $albums[] = $row['album_id'];
                }

                $item->setData('albums', $albums);
            }
        }

    }//end getTotalAlbum()


    /**
     * Add filter by store
     *
     * @param  int|array|\Magento\Store\Model\Store $store
     * @param  bool                                 $withAdmin
     * @return $this
     */
    public function addStoreFilter($store, $withAdmin = true)
    {
        if (!$this->getFlag('store_filter_added')) {
            $this->performAddStoreFilter($store, $withAdmin);
        }

        return $this;

    }//end addStoreFilter()


}//end class
