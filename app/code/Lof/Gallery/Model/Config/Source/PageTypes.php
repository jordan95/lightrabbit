<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Gallery\Model\Config\Source;

class PageTypes implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @var \Lof\Gallery\Model\Album
     */
    protected $galleryAlbum;


    /**
     * @param \Lof\Gallery\Model\Album $galleryAlbum
     */
    public function __construct(
        \Lof\Gallery\Model\Album $galleryAlbum
    ) {
        $this->galleryAlbum = $galleryAlbum;

    }//end __construct()


    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $pageTypes = $this->galleryAlbum->getPageTypes();
        $data      = [];
        foreach ($pageTypes as $k => $v) {
            $data[] = [
                       'value' => $k,
                       'label' => $v,
                      ];
        }

        return $data;

    }//end toOptionArray()


}//end class
