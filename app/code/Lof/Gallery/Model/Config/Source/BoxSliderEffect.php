<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Gallery\Model\Config\Source;

class BoxSliderEffect implements \Magento\Framework\Option\ArrayInterface
{


    public function toOptionArray()
    {
        $options   = [];
        $options[] = [
                      'label' => 'scrollVert3d',
                      'value' => 'scrollVert3d',
                     ];
        $options[] = [
                      'label' => 'scrollHorz3d',
                      'value' => 'scrollHorz3d',
                     ];
        $options[] = [
                      'label' => 'tile3d',
                      'value' => 'tile3d',
                     ];
        $options[] = [
                      'label' => '2D tiles',
                      'value' => 'tile',
                     ];
        $options[] = [
                      'label' => 'Vertical scroll',
                      'value' => 'scrollVert',
                     ];
        $options[] = [
                      'label' => 'Horizontal scroll',
                      'value' => 'scrollHorz',
                     ];
        $options[] = [
                      'label' => 'blindLeft',
                      'value' => 'blindLeft',
                     ];
        $options[] = [
                      'label' => 'blindDown',
                      'value' => 'blindDown',
                     ];
        return $options;

    }//end toOptionArray()


}//end class
