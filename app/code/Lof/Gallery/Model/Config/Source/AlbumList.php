<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Gallery\Model\Config\Source;

class AlbumList implements \Magento\Framework\Option\ArrayInterface
{

    public function __construct(
        \Lof\Gallery\Model\ResourceModel\Album\CollectionFactory $albumCollectionFactory
    ) {
        $this->albumCollectionFactory = $albumCollectionFactory;
    }

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $collection = $collection  = $this->albumCollectionFactory->create()
            ->addFieldToFilter("is_active", 1)
            ->setCurPage(1);

        $albums = [];

        foreach ($collection as $album) {
            $albums[] = [
                'value' => $album->getId(),
                'label' => $album->getName()
            ];
        }

        return $albums;

    }//end toOptionArray()


}//end class
