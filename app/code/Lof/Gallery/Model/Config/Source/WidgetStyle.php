<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Gallery\Model\Config\Source;

class WidgetStyle implements \Magento\Framework\Option\ArrayInterface
{


    public function toOptionArray()
    {
        $options   = [];
        $options[] = [
                      'label' => 'Default',
                      'value' => 'default',
                     ];
        $options[] = [
                      'label' => 'Style 1',
                      'value' => 'style1',
                     ];
        $options[] = [
                      'label' => 'Style 2',
                      'value' => 'style2',
                     ];
        $options[] = [
                      'label' => 'Style 3',
                      'value' => 'style3',
                     ];
        $options[] = [
                      'label' => 'Style 4',
                      'value' => 'style4',
                     ];
        $options[] = [
                      'label' => 'Style 5',
                      'value' => 'style5',
                     ];
        $options[] = [
                      'label' => 'Style 6',
                      'value' => 'style6',
                     ];
        $options[] = [
                      'label' => 'Style 7',
                      'value' => 'style7',
                     ];
        $options[] = [
                      'label' => 'Style 8',
                      'value' => 'style8',
                     ];
        $options[] = [
                      'label' => 'Style 9',
                      'value' => 'style9',
                     ];
        $options[] = [
                      'label' => 'Style 10',
                      'value' => 'style10',
                     ];
        $options[] = [
                      'label' => 'Style 11',
                      'value' => 'style11',
                     ];
        $options[] = [
                      'label' => 'Style 12',
                      'value' => 'style12',
                     ];
        $options[] = [
                      'label' => 'Style 13',
                      'value' => 'style13',
                     ];
        $options[] = [
                      'label' => 'Style 14',
                      'value' => 'style14',
                     ];
        $options[] = [
                      'label' => '3D Hover Effect',
                      'value' => '3d',
                     ];
        // $options[] = [
        // 'label' => 'Group by Category',
        // 'value' => 'category'
        // ];
        return $options;

    }//end toOptionArray()


}//end class
