<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */
namespace Lof\Gallery\Model;

class Category extends \Magento\Framework\Model\AbstractModel
{
    const STATUS_ENABLED  = 1;
    const STATUS_DISABLED = 0;

    /**
     * Store manager
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $resource;

    /**
     * @var \Lof\Gallery\Model\ResourceModel\Album\Collection
     */
    protected $albumCollection;


    /**
     * Category constructor.
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param ResourceModel\Category|null $resource
     * @param ResourceModel\Category\Collection|null $resourceCollection
     * @param ResourceModel\Album\CollectionFactory $albumCollection
     * @param \Magento\Framework\App\ResourceConnection $mageResource
     * @param Config $galleryConfig
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Lof\Gallery\Model\ResourceModel\Category $resource = null,
        \Lof\Gallery\Model\ResourceModel\Category\Collection $resourceCollection = null,
        \Lof\Gallery\Model\ResourceModel\Album\CollectionFactory $albumCollection,
        \Magento\Framework\App\ResourceConnection $mageResource,
        Config $galleryConfig,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection);
        $this->storeManager    = $storeManager;
        $this->mageResource        = $mageResource;
        $this->albumCollection = $albumCollection;
        $this->galleryConfig   = $galleryConfig;

    }//end __construct()


    protected function _construct()
    {
        $this->_init('Lof\Gallery\Model\ResourceModel\Category');

    }//end _construct()


    public function getUrl()
    {
        $baseUrl = $this->storeManager->getStore()->getBaseUrl();
        $route   = $this->galleryConfig->getConfig('general_settings/route');
        $link    = $baseUrl.$route.'/'.$this->getIdentifier();
        return $link;

    }//end getUrl()

    /**
     * Receive page store ids
     *
     * @return int[]
     */
    public function getStores()
    {
        return $this->hasData('stores') ? $this->getData('stores') : $this->getData('store_id');

    }//end getStores()

    /**
     * Receive albums of category
     *
     * @return Lof\Gallery\Model\ResourceModel\Album\Collection
     */
    public function getAlbums($store = null)
    {
        $collection = $this->albumCollection->create();
        $collection->getSelect()->joinLeft(
            [
             'p' => $this->mageResource->getTableName(Config::TABLE_GALLERY_ALBUM_CATEGORY),
            ],
            'p.album_id = main_table.album_id',
            [
             'album_id' => 'album_id',
             'position' => 'position',
            ]
        )
            ->where('p.entity_id = (?)', $this->getId())->order('p.position ASC');
        return $collection;

    }//end getAlbums()


    public function getAvailableStatuses()
    {
        return [
                self::STATUS_ENABLED  => __('Enabled'),
                self::STATUS_DISABLED => __('Disabled'),
               ];

    }//end getAvailableStatuses()


}//end class
