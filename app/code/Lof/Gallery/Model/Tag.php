<?php
/**
 * Venustheme
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Venustheme.com license that is
 * available through the world-wide-web at this URL:
 * http://www.venustheme.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Venustheme
 * @package   Ves_Blog
 * @copyright Copyright (c) 2016 Venustheme (http://www.venustheme.com/)
 * @license   http://www.venustheme.com/LICENSE-1.0.html
 */
namespace Lof\Gallery\Model;

use Magento\Cms\Api\Data\PageInterface;
use Magento\Framework\DataObject\IdentityInterface;

class Tag extends \Magento\Framework\Model\AbstractModel
{
    const STATUS_ENABLED  = 1;
    const STATUS_DISABLED = 0;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Lof\Gallery\Model\ResourceModel\Tag
     */
    protected $resource;

    /**
     * @var \Lof\Gallery\Model\ResourceModel\Album\CollectionFactory
     */
    protected $albumCollection;

    /**
     * @var Config
     */
    protected $galleryConfig;


    /**
     * Tag constructor.
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param ResourceModel\Tag|null $resource
     * @param ResourceModel\Tag\Collection|null $resourceCollection
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param ResourceModel\Album\CollectionFactory $albumCollection
     * @param Config $galleryConfig
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Lof\Gallery\Model\ResourceModel\Tag $resource = null,
        \Lof\Gallery\Model\ResourceModel\Tag\Collection $resourceCollection = null,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Lof\Gallery\Model\ResourceModel\Album\CollectionFactory $albumCollection,
        Config $galleryConfig,
        array $data = []
    ) {
        $this->resource     = $resource;
        $this->storeManager = $storeManager;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->albumCollection = $albumCollection;
        $this->galleryConfig   = $galleryConfig;

    }//end __construct()


    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Lof\Gallery\Model\ResourceModel\Tag');

    }//end _construct()


    public function getAlbums($store = null)
    {
        $collection = $this->albumCollection->create();
        $collection->addFilterByTag($this->getId());
        if($store){
            $collection->addStoreFilter($store);
        }
        $collection->getSelect()
            ->order('position ASC')
            ->order('p.tag_id DESC');
        return $collection;

    }//end getAlbums()


    /**
     * Prepare page's statuses.
     * Available event cms_page_get_available_statuses to customize statuses.
     *
     * @return array
     */
    public function getAvailableStatuses()
    {
        return [
                self::STATUS_ENABLED  => __('Enabled'),
                self::STATUS_DISABLED => __('Disabled'),
               ];

    }//end getAvailableStatuses()


    public function getUrl()
    {
        $baseUrl = $this->storeManager->getStore()->getBaseUrl();
        $route   = $this->galleryConfig->getConfig('general_settings/route');
        $link    = $baseUrl.$route.'/tag/'.$this->getAlias();
        return $link;

    }//end getUrl()


}//end class
