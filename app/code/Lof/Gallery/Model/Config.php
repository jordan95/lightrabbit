<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */
namespace Lof\Gallery\Model;

class Config
{
    const MEDIA_PATH          = 'lof/gallery/album';
    const TABLE_GALLERY_ALBUM = "lof_gallery_album";
    const TABLE_GALLERY_ALBUM_CATEGORY = "lof_gallery_album_category";
    const TABLE_GALLERY_ALBUM_TAG      = "lof_gallery_album_tag";
    const TABLE_GALLERY_ALBUM_PRODUCT  = "lof_gallery_album_product";
    const TABLE_GALLERY_ALBUM_IMAGE    = "lof_gallery_album_image";
    const TABLE_GALLERY_ALBUM_STORE    = "lof_gallery_album_store";
    const TABLE_GALLERY_ALBUM_POST     = "lof_gallery_album_post";
    const TABLE_GALLERY_TAG            = "lof_gallery_tag";
    const TABLE_GALLERY_CATEGORY       = "lof_gallery_category";
    const TABLE_GALLERY_CATEGORY_STORE = "lof_gallery_category_store";
    const TABLE_GALLERY_ALBUM_CORESTORE = "lof_gallery_album_corestore";

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;


    /**
     * Config constructor.
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->storeManager = $storeManager;
        $this->scopeConfig  = $scopeConfig;

    }//end __construct()


    /**
     * @return string
     */
    public function getFileUrl($file)
    {
        $path = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).self::MEDIA_PATH.$file;
        return $path;

    }//end getFileUrl()

    public function getMediaUrl(){
        return $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
    }
    public function getGalleryPageUrl()
    {
        $url   = $this->storeManager->getStore()->getBaseUrl();
        $route = $this->getConfig('general_settings/route');
        return $url.$route;

    }//end getGalleryPageUrl()


    /**
     * Return brand config value by key and store
     *
     * @param  string                                $key
     * @param  \Magento\Store\Model\Store|int|string $store
     * @return string|null
     */
    public function getConfig($key, $store = null)
    {
        $store     = $this->storeManager->getStore($store);
        $websiteId = $store->getWebsiteId();
        $result    = $this->scopeConfig->getValue(
            'lofgallery/'.$key,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        );
        return $result;

    }//end getConfig()


}//end class
