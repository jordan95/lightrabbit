<?php namespace BoostMyShop\Amazon\Test\Unit\Helper\Offer;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;

/**
 * Class StockTest
 *
 * @package   BoostMyShop\Amazon\Test\Unit\Helper\Offer
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class StockTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var \BoostMyShop\Amazon\Helper\Offer\Stock
     */
    protected $instance;

    /**
     * @var \Magento\CatalogInventory\Api\StockRegistryInterface| \PHPUnit_Framework_MockObject_MockObject
     */
    protected $stockRegistry;

    public function setUp()
    {

        $this->stockRegistry = $this->getMockBuilder('\Magento\CatalogInventory\Api\StockRegistryInterface')
            ->disableOriginalConstructor()
            ->getMock();

        $objectManager = new ObjectManager($this);

        $this->instance = $objectManager->getObject(
            '\BoostMyShop\Amazon\Helper\Offer\Stock',
            [
                'stockRegistry' => $this->stockRegistry
            ]
        );

    }

    public function testGetStockToExportForDisabledProduct(){

        $finalStock = 0;

        $productMock = $this->getMockBuilder('\Magento\Catalog\Model\Product')
            ->setMethods(['getAmazonStatus'])
            ->disableOriginalConstructor()
            ->getMock();

        $productMock->expects($this->once())
            ->method('getAmazonStatus')
            ->willReturn(\BoostMyShop\Amazon\Model\Product::STATUS_DISABLED);

        $countryMock = $this->getMockBuilder('\BoostMyShop\Amazon\Model\Account\Country')
            ->disableOriginalConstructor()
            ->getMock();

        $this->assertEquals($finalStock, $this->instance->getStockToExport($productMock, $countryMock));

    }

    public function testGetStockToExport(){

        $finalStock = 5;
        $stockAttribute = '';
        $productId = 8;
        $websiteId = 2;

        $stockItemMock = $this->getMockBuilder('\Magento\CatalogInventory\Api\Data\StockItemInterface')
            ->disableOriginalConstructor()
            ->setMethods(['getQty'])
            ->getMockForAbstractClass();

        $stockItemMock->expects($this->once())->method('getQty')->willReturn($finalStock);

        $storeMock = $this->getMockBuilder('\Magento\Store\Model\Store')
            ->setMethods(['getWebsiteId'])
            ->disableOriginalConstructor()
            ->getMock();

        $storeMock->expects($this->once())
            ->method('getWebsiteId')
            ->willReturn($websiteId);

        $productMock = $this->getMockBuilder('\Magento\Catalog\Model\Product')
            ->setMethods(['getAmazonStatus','getId','getStore'])
            ->disableOriginalConstructor()
            ->getMock();

        $productMock->expects($this->once())
            ->method('getAmazonStatus')
            ->willReturn(\BoostMyShop\Amazon\Model\Product::STATUS_ASSOCIATED);

        $productMock->expects($this->once())->method('getId')->willReturn($productId);
        $productMock->expects($this->once())->method('getStore')->willReturn($storeMock);

        $countryMock = $this->getMockBuilder('\BoostMyShop\Amazon\Model\Account\Country')
            ->setMethods(['getOption'])
            ->disableOriginalConstructor()
            ->getMock();

        $countryMock->expects($this->once())->method('getOption')->willReturn($stockAttribute);

        $this->stockRegistry->expects($this->once())
            ->method('getStockItem')
            ->with($productId, $websiteId);

        $this->assertEquals($finalStock, $this->instance->getStockToExport($productMock, $countryMock));

    }

}