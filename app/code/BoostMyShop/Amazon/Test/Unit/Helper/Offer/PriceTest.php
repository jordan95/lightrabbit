<?php namespace BoostMyShop\Amazon\Test\Unit\Helper\Offer;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;

/**
 * Class PriceTest
 *
 * @package   BoostMyShop\Amazon\Test\Unit\Helper\Offer
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class PriceTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var \Magento\Catalog\Helper\Data| \PHPUnit_Framework_MockObject_MockObject
     */
    protected $catalogHelper;

    /**
     * @var \BoostMyShop\Amazon\Helper\Offer\Price
     */
    protected $instance;

    public function setUp()
    {

        $this->catalogHelper = $this->getMockBuilder('\Magento\Catalog\Helper\Data')
            ->setMethods(['getTaxPrice'])
            ->disableOriginalConstructor()
            ->getMock();

        $objectManager = new ObjectManager($this);

        $this->instance = $objectManager->getObject(
            '\BoostMyShop\Amazon\Helper\Offer\Price',
            [
                'catalogHelper' => $this->catalogHelper
            ]
        );

    }

    public function testGetPriceToExport()
    {

        $productPrice = 59.99;
        $addTax = 0;
        $priceAttribute = '';
        $priceCoef = '';
        $useSpecialPrice = 0;

        $storeMock = $this->getMockBuilder('\Magento\Store\Model\Store')
            ->disableOriginalConstructor()
            ->setMethods(['getId'])
            ->getMock();

        $productMock = $this->getMockBuilder('\Magento\Catalog\Model\Product')
            ->disableOriginalConstructor()
            ->setMethods(['getPrice'])
            ->getMock();

        $productMock->expects($this->once())->method('getPrice')->willReturn($productPrice);

        $countryMock = $this->getMockBuilder('\BoostMyShop\Amazon\Model\Account\Country')
            ->setMethods(['getOption', 'getStore'])
            ->disableOriginalConstructor()
            ->getMock();

        $countryMock->expects($this->at(0))->method('getOption')->with('add_tax')->willReturn($addTax);
        $countryMock->expects($this->at(1))->method('getOption')->with('price_attribute')->willReturn($priceAttribute);
        $countryMock->expects($this->at(2))->method('getOption')->with('price_coef')->willReturn($priceCoef);
        $countryMock->expects($this->at(4))
            ->method('getOption')
            ->with('use_special_price')
            ->willReturn($useSpecialPrice);

        $countryMock->expects($this->at(3))->method('getStore')->willReturn($storeMock);

        $this->catalogHelper->expects($this->once())
            ->method('getTaxPrice')
            ->with($productMock, $productPrice, $addTax, null, null, null, $storeMock)
            ->willReturn($productPrice);

        $result = $this->instance->getPriceToExport($productMock, $countryMock);
        $this->assertEquals(['price' => $productPrice], $result);

    }

    public function testGetPriceToExportWithCustomAttributeWithEmptyValue()
    {

        $productPrice = 59.99;
        $addTax = 0;
        $priceAttribute = 'custom';
        $customPrice = null;
        $priceCoef = '';
        $useSpecialPrice = 0;

        $storeMock = $this->getMockBuilder('\Magento\Store\Model\Store')
            ->disableOriginalConstructor()
            ->setMethods(['getId'])
            ->getMock();

        $productMock = $this->getMockBuilder('\Magento\Catalog\Model\Product')
            ->disableOriginalConstructor()
            ->setMethods(['getPrice', 'getData'])
            ->getMock();

        $productMock->expects($this->once())->method('getPrice')->willReturn($productPrice);
        $productMock->expects($this->at(1))->method('getData')->with($priceAttribute)->willReturn($customPrice);

        $countryMock = $this->getMockBuilder('\BoostMyShop\Amazon\Model\Account\Country')
            ->setMethods(['getOption', 'getStore'])
            ->disableOriginalConstructor()
            ->getMock();

        $countryMock->expects($this->at(0))->method('getOption')->with('add_tax')->willReturn($addTax);
        $countryMock->expects($this->at(1))->method('getOption')->with('price_attribute')->willReturn($priceAttribute);
        $countryMock->expects($this->at(2))->method('getOption')->with('price_coef')->willReturn($priceCoef);
        $countryMock->expects($this->at(4))
            ->method('getOption')
            ->with('use_special_price')
            ->willReturn($useSpecialPrice);

        $countryMock->expects($this->at(3))->method('getStore')->willReturn($storeMock);

        $this->catalogHelper->expects($this->once())
            ->method('getTaxPrice')
            ->with($productMock, $productPrice, $addTax, null, null, null, $storeMock)
            ->willReturn($productPrice);

        $result = $this->instance->getPriceToExport($productMock, $countryMock);
        $this->assertEquals(['price' => $productPrice], $result);

    }

    public function testGetPriceToExportWithValidCustomPrice()
    {

        $productPrice = 59.99;
        $addTax = 0;
        $priceAttribute = 'custom';
        $customPrice = 12.59;
        $priceCoef = '';
        $useSpecialPrice = 0;

        $storeMock = $this->getMockBuilder('\Magento\Store\Model\Store')
            ->disableOriginalConstructor()
            ->setMethods(['getId'])
            ->getMock();

        $productMock = $this->getMockBuilder('\Magento\Catalog\Model\Product')
            ->disableOriginalConstructor()
            ->setMethods(['getPrice', 'getData'])
            ->getMock();

        $productMock->expects($this->once())->method('getPrice')->willReturn($productPrice);
        $productMock->expects($this->at(1))->method('getData')->with($priceAttribute)->willReturn($customPrice);
        $productMock->expects($this->at(2))->method('getData')->with($priceAttribute)->willReturn($customPrice);

        $countryMock = $this->getMockBuilder('\BoostMyShop\Amazon\Model\Account\Country')
            ->setMethods(['getOption', 'getStore'])
            ->disableOriginalConstructor()
            ->getMock();

        $countryMock->expects($this->at(0))->method('getOption')->with('add_tax')->willReturn($addTax);
        $countryMock->expects($this->at(1))->method('getOption')->with('price_attribute')->willReturn($priceAttribute);
        $countryMock->expects($this->at(2))->method('getOption')->with('price_coef')->willReturn($priceCoef);
        $countryMock->expects($this->at(4))
            ->method('getOption')
            ->with('use_special_price')
            ->willReturn($useSpecialPrice);

        $countryMock->expects($this->at(3))->method('getStore')->willReturn($storeMock);

        $this->catalogHelper->expects($this->once())
            ->method('getTaxPrice')
            ->with($productMock, $customPrice, $addTax, null, null, null, $storeMock)
            ->willReturn($customPrice);

        $result = $this->instance->getPriceToExport($productMock, $countryMock);
        $this->assertEquals(['price' => $customPrice], $result);

    }

    public function testGetPriceToExportWithPriceCoef()
    {

        $productPrice = 59.99;
        $addTax = 0;
        $priceAttribute = '';
        $priceCoef = '2';
        $useSpecialPrice = 0;
        $finalPrice = $productPrice * $priceCoef;

        $storeMock = $this->getMockBuilder('\Magento\Store\Model\Store')
            ->disableOriginalConstructor()
            ->setMethods(['getId'])
            ->getMock();

        $productMock = $this->getMockBuilder('\Magento\Catalog\Model\Product')
            ->disableOriginalConstructor()
            ->setMethods(['getPrice'])
            ->getMock();

        $productMock->expects($this->once())->method('getPrice')->willReturn($productPrice);

        $countryMock = $this->getMockBuilder('\BoostMyShop\Amazon\Model\Account\Country')
            ->setMethods(['getOption', 'getStore'])
            ->disableOriginalConstructor()
            ->getMock();

        $countryMock->expects($this->at(0))->method('getOption')->with('add_tax')->willReturn($addTax);
        $countryMock->expects($this->at(1))->method('getOption')->with('price_attribute')->willReturn($priceAttribute);
        $countryMock->expects($this->at(2))->method('getOption')->with('price_coef')->willReturn($priceCoef);
        $countryMock->expects($this->at(4))
            ->method('getOption')
            ->with('use_special_price')
            ->willReturn($useSpecialPrice);

        $countryMock->expects($this->at(3))->method('getStore')->willReturn($storeMock);

        $this->catalogHelper->expects($this->once())
            ->method('getTaxPrice')
            ->with($productMock, $finalPrice, $addTax, null, null, null, $storeMock)
            ->willReturn($finalPrice);

        $result = $this->instance->getPriceToExport($productMock, $countryMock);
        $this->assertEquals(['price' => $finalPrice], $result);

    }

    public function testGetPriceToExportWithEmptySpecialPrice()
    {

        $productPrice = 59.99;
        $addTax = 0;
        $priceAttribute = '';
        $priceCoef = '';
        $useSpecialPrice = 1;
        $specialPrice = 0;

        $storeMock = $this->getMockBuilder('\Magento\Store\Model\Store')
            ->disableOriginalConstructor()
            ->setMethods(['getId'])
            ->getMock();

        $productMock = $this->getMockBuilder('\Magento\Catalog\Model\Product')
            ->disableOriginalConstructor()
            ->setMethods(['getPrice', 'getSpecialPrice', 'getSpecialFromDate', 'getSpecialToDate'])
            ->getMock();

        $productMock->expects($this->once())->method('getPrice')->willReturn($productPrice);
        $productMock->expects($this->once())->method('getSpecialPrice')->willReturn($specialPrice);

        $countryMock = $this->getMockBuilder('\BoostMyShop\Amazon\Model\Account\Country')
            ->setMethods(['getOption', 'getStore'])
            ->disableOriginalConstructor()
            ->getMock();

        $countryMock->expects($this->at(0))->method('getOption')->with('add_tax')->willReturn($addTax);
        $countryMock->expects($this->at(1))->method('getOption')->with('price_attribute')->willReturn($priceAttribute);
        $countryMock->expects($this->at(2))->method('getOption')->with('price_coef')->willReturn($priceCoef);
        $countryMock->expects($this->at(4))
            ->method('getOption')
            ->with('use_special_price')
            ->willReturn($useSpecialPrice);

        $countryMock->expects($this->at(3))->method('getStore')->willReturn($storeMock);

        $this->catalogHelper->expects($this->once())
            ->method('getTaxPrice')
            ->with($productMock, $productPrice, $addTax, null, null, null, $storeMock)
            ->willReturn($productPrice);

        $result = $this->instance->getPriceToExport($productMock, $countryMock);
        $this->assertEquals(['price' => $productPrice], $result);

    }

    public function testGetPriceToExportWithEndedSpecialPrice()
    {

        $productPrice = 59.99;
        $addTax = 0;
        $priceAttribute = '';
        $priceCoef = '';
        $useSpecialPrice = 1;
        $specialPrice = 9.99;
        $specialFromDate = date('Y-m-d', time() - 24 * 3600);
        $specialToDate = date('Y-m-d', time() - 24 * 3600 * 15);

        $storeMock = $this->getMockBuilder('\Magento\Store\Model\Store')
            ->disableOriginalConstructor()
            ->setMethods(['getId'])
            ->getMock();

        $productMock = $this->getMockBuilder('\Magento\Catalog\Model\Product')
            ->disableOriginalConstructor()
            ->setMethods(['getPrice', 'getSpecialPrice', 'getSpecialFromDate', 'getSpecialToDate'])
            ->getMock();

        $productMock->expects($this->once())->method('getPrice')->willReturn($productPrice);
        $productMock->expects($this->once())->method('getSpecialPrice')->willReturn($specialPrice);
        $productMock->expects($this->once())->method('getSpecialFromDate')->willReturn($specialFromDate);
        $productMock->expects($this->once())->method('getSpecialToDate')->willReturn($specialToDate);

        $countryMock = $this->getMockBuilder('\BoostMyShop\Amazon\Model\Account\Country')
            ->setMethods(['getOption', 'getStore'])
            ->disableOriginalConstructor()
            ->getMock();

        $countryMock->expects($this->at(0))->method('getOption')->with('add_tax')->willReturn($addTax);
        $countryMock->expects($this->at(1))->method('getOption')->with('price_attribute')->willReturn($priceAttribute);
        $countryMock->expects($this->at(2))->method('getOption')->with('price_coef')->willReturn($priceCoef);
        $countryMock->expects($this->at(4))
            ->method('getOption')
            ->with('use_special_price')
            ->willReturn($useSpecialPrice);

        $countryMock->expects($this->at(3))->method('getStore')->willReturn($storeMock);

        $this->catalogHelper->expects($this->once())
            ->method('getTaxPrice')
            ->with($productMock, $productPrice, $addTax, null, null, null, $storeMock)
            ->willReturn($productPrice);

        $result = $this->instance->getPriceToExport($productMock, $countryMock);
        $this->assertEquals(['price' => $productPrice], $result);

    }

    public function testGetPriceToExportWithFutureSpecialPrice()
    {

        $productPrice = 59.99;
        $addTax = 0;
        $priceAttribute = '';
        $priceCoef = '';
        $useSpecialPrice = 1;
        $specialPrice = 9.99;
        $specialFromDate = date('Y-m-d', time() + 24 * 3600);

        $storeMock = $this->getMockBuilder('\Magento\Store\Model\Store')
            ->disableOriginalConstructor()
            ->setMethods(['getId'])
            ->getMock();

        $productMock = $this->getMockBuilder('\Magento\Catalog\Model\Product')
            ->disableOriginalConstructor()
            ->setMethods(['getPrice', 'getSpecialPrice', 'getSpecialFromDate', 'getSpecialToDate'])
            ->getMock();

        $productMock->expects($this->once())->method('getPrice')->willReturn($productPrice);
        $productMock->expects($this->once())->method('getSpecialPrice')->willReturn($specialPrice);
        $productMock->expects($this->once())->method('getSpecialFromDate')->willReturn($specialFromDate);

        $countryMock = $this->getMockBuilder('\BoostMyShop\Amazon\Model\Account\Country')
            ->setMethods(['getOption', 'getStore'])
            ->disableOriginalConstructor()
            ->getMock();

        $countryMock->expects($this->at(0))->method('getOption')->with('add_tax')->willReturn($addTax);
        $countryMock->expects($this->at(1))->method('getOption')->with('price_attribute')->willReturn($priceAttribute);
        $countryMock->expects($this->at(2))->method('getOption')->with('price_coef')->willReturn($priceCoef);
        $countryMock->expects($this->at(4))
            ->method('getOption')
            ->with('use_special_price')
            ->willReturn($useSpecialPrice);

        $countryMock->expects($this->at(3))->method('getStore')->willReturn($storeMock);

        $this->catalogHelper->expects($this->once())
            ->method('getTaxPrice')
            ->with($productMock, $productPrice, $addTax, null, null, null, $storeMock)
            ->willReturn($productPrice);

        $result = $this->instance->getPriceToExport($productMock, $countryMock);
        $this->assertEquals(['price' => $productPrice], $result);

    }

    public function testGetPriceToExportWithValidSpecialPrice()
    {

        $productPrice = 59.99;
        $addTax = 0;
        $priceAttribute = '';
        $priceCoef = '';
        $useSpecialPrice = 1;
        $specialPrice = 9.99;
        $specialFromDate = date('Y-m-d\T08:00:00', time() - 24 * 3600);
        $specialToDate = date('Y-m-d\T08:00:00', time() + 24 * 3600 * 15);

        $storeMock = $this->getMockBuilder('\Magento\Store\Model\Store')
            ->disableOriginalConstructor()
            ->setMethods(['getId'])
            ->getMock();

        $productMock = $this->getMockBuilder('\Magento\Catalog\Model\Product')
            ->disableOriginalConstructor()
            ->setMethods(['getPrice', 'getSpecialPrice', 'getSpecialFromDate', 'getSpecialToDate'])
            ->getMock();

        $productMock->expects($this->once())->method('getPrice')->willReturn($productPrice);
        $productMock->expects($this->exactly(2))->method('getSpecialPrice')->willReturn($specialPrice);
        $productMock->expects($this->exactly(2))->method('getSpecialFromDate')->willReturn($specialFromDate);
        $productMock->expects($this->exactly(2))->method('getSpecialToDate')->willReturn($specialToDate);

        $countryMock = $this->getMockBuilder('\BoostMyShop\Amazon\Model\Account\Country')
            ->setMethods(['getOption', 'getStore'])
            ->disableOriginalConstructor()
            ->getMock();

        $countryMock->expects($this->at(0))->method('getOption')->with('add_tax')->willReturn($addTax);
        $countryMock->expects($this->at(1))->method('getOption')->with('price_attribute')->willReturn($priceAttribute);
        $countryMock->expects($this->at(2))->method('getOption')->with('price_coef')->willReturn($priceCoef);
        $countryMock->expects($this->at(4))
            ->method('getOption')
            ->with('use_special_price')
            ->willReturn($useSpecialPrice);

        $countryMock->expects($this->exactly(2))->method('getStore')->willReturn($storeMock);

        $this->catalogHelper->expects($this->at(0))
            ->method('getTaxPrice')
            ->with($productMock, $productPrice, $addTax, null, null, null, $storeMock)
            ->willReturn($productPrice);

        $this->catalogHelper->expects($this->at(1))
            ->method('getTaxPrice')
            ->with($productMock, $specialPrice, $addTax, null, null, null, $storeMock)
            ->willReturn($specialPrice);

        $result = $this->instance->getPriceToExport($productMock, $countryMock);
        $expected = [
            'price' => $productPrice,
            'sale' => [
                'price' => $specialPrice,
                'startDate' => $specialFromDate,
                'endDate' => $specialToDate
            ]
        ];
        $this->assertEquals($expected, $result);

    }

    public function testGetPriceToExportWithTax()
    {

        $productPrice = 59.99;
        $addTax = 1;
        $priceAttribute = '';
        $priceCoef = '';
        $useSpecialPrice = 0;

        $storeMock = $this->getMockBuilder('\Magento\Store\Model\Store')
            ->disableOriginalConstructor()
            ->setMethods(['getId'])
            ->getMock();

        $productMock = $this->getMockBuilder('\Magento\Catalog\Model\Product')
            ->disableOriginalConstructor()
            ->setMethods(['getPrice'])
            ->getMock();

        $productMock->expects($this->once())->method('getPrice')->willReturn($productPrice);

        $countryMock = $this->getMockBuilder('\BoostMyShop\Amazon\Model\Account\Country')
            ->setMethods(['getOption', 'getStore'])
            ->disableOriginalConstructor()
            ->getMock();

        $countryMock->expects($this->at(0))->method('getOption')->with('add_tax')->willReturn($addTax);
        $countryMock->expects($this->at(1))->method('getOption')->with('price_attribute')->willReturn($priceAttribute);
        $countryMock->expects($this->at(2))->method('getOption')->with('price_coef')->willReturn($priceCoef);
        $countryMock->expects($this->at(4))
            ->method('getOption')
            ->with('use_special_price')
            ->willReturn($useSpecialPrice);

        $countryMock->expects($this->at(3))->method('getStore')->willReturn($storeMock);

        $this->catalogHelper->expects($this->once())
            ->method('getTaxPrice')
            ->with($productMock, $productPrice, $addTax, null, null, null, $storeMock)
            ->willReturn($productPrice);

        $result = $this->instance->getPriceToExport($productMock, $countryMock);
        $this->assertEquals(['price' => $productPrice], $result);

    }

}