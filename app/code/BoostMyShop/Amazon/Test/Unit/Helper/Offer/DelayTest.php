<?php namespace BoostMyShop\Amazon\Test\Unit\Helper\Offer;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;

/**
 * Class DelayTest
 *
 * @package   BoostMyShop\Amazon\Test\Unit\Helper\Offer
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class DelayTest extends \PHPUnit_Framework_TestCase {

    /**
     * @var \BoostMyShop\Amazon\Helper\Offer\Delay
     */
    protected $instance;

    public function setUp(){

        $this->instance = (new ObjectManager($this))->getObject(
            '\BoostMyShop\Amazon\Helper\Offer\Delay',
            []
        );

    }

    public function testGetDelayToExport(){

        $defaultDelay = 5;
        $delayAttribute = '';

        $productMock = $this->getMockBuilder('\Magento\Catalog\Model\Product')
            ->setMethods(['getData'])
            ->disableOriginalConstructor()
            ->getMock();

        $countryMock = $this->getMockBuilder('\BoostMyShop\Amazon\Model\Account\Country')
            ->setMethods(['getOption'])
            ->disableOriginalConstructor()
            ->getMock();

        $countryMock->expects($this->at(0))->method('getOption')->with('default_delay')->willReturn($defaultDelay);
        $countryMock->expects($this->at(1))->method('getOption')->with('delay_attribute')->willReturn($delayAttribute);

        $this->assertEquals($defaultDelay, $this->instance->getDelayToExport($productMock, $countryMock));

    }

    public function testGetDelayToExportWithEmptyAttributeValue(){

        $defaultDelay = 5;
        $delayAttribute = 'custom';
        $delayAttributeValue = '';

        $productMock = $this->getMockBuilder('\Magento\Catalog\Model\Product')
            ->setMethods(['getData'])
            ->disableOriginalConstructor()
            ->getMock();

        $productMock->expects($this->once())->method('getData')->with($delayAttribute)->willReturn($delayAttributeValue);

        $countryMock = $this->getMockBuilder('\BoostMyShop\Amazon\Model\Account\Country')
            ->setMethods(['getOption'])
            ->disableOriginalConstructor()
            ->getMock();

        $countryMock->expects($this->at(0))->method('getOption')->with('default_delay')->willReturn($defaultDelay);
        $countryMock->expects($this->at(1))->method('getOption')->with('delay_attribute')->willReturn($delayAttribute);

        $this->assertEquals($defaultDelay, $this->instance->getDelayToExport($productMock, $countryMock));

    }

    public function testGetDelayToExportWithAttributeValue(){

        $defaultDelay = 5;
        $delayAttribute = 'custom';
        $delayAttributeValue = 10;

        $productMock = $this->getMockBuilder('\Magento\Catalog\Model\Product')
            ->setMethods(['getData'])
            ->disableOriginalConstructor()
            ->getMock();

        $productMock->expects($this->exactly(2))->method('getData')->with($delayAttribute)->willReturn($delayAttributeValue);

        $countryMock = $this->getMockBuilder('\BoostMyShop\Amazon\Model\Account\Country')
            ->setMethods(['getOption'])
            ->disableOriginalConstructor()
            ->getMock();

        $countryMock->expects($this->at(0))->method('getOption')->with('default_delay')->willReturn($defaultDelay);
        $countryMock->expects($this->at(1))->method('getOption')->with('delay_attribute')->willReturn($delayAttribute);

        $this->assertEquals($delayAttributeValue, $this->instance->getDelayToExport($productMock, $countryMock));

    }

}