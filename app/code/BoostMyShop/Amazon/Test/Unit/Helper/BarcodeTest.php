<?php namespace BoostMyShop\Amazon\Test\Unit\Helper;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;

/**
 * Class BarcodeTest
 *
 * @package   BoostMyShop\Amazon\Test\Unit\Helper
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class BarcodeTest extends \PHPUnit_Framework_TestCase {

    /**
     * @var \BoostMyShop\Amazon\Helper\Barcode
     */
    protected $instance;

    public function setUp(){

        $this->instance = (new ObjectManager($this))->getObject(
            '\BoostMyShop\Amazon\Helper\Barcode',
            []
        );

    }

    public function testGetTypeForEan(){

        $barcode = '0724393155429';
        $this->assertEquals(\BoostMyShop\Amazon\Helper\Barcode::TYPE_EAN, $this->instance->getType($barcode));

    }

    public function testGetTypeForUpc(){

        $barcode = '724393155429';
        $this->assertEquals(\BoostMyShop\Amazon\Helper\Barcode::TYPE_UPC, $this->instance->getType($barcode));

    }

}