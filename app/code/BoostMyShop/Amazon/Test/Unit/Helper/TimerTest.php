<?php namespace BoostMyShop\Amazon\Test\Unit\Helper;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;

/**
 * Class TimerTest
 *
 * @package   BoostMyShop\Amazon\Test\Unit\Helper
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class TimerTest extends \PHPUnit_Framework_TestCase {

    /**
     * @var \BoostMyShop\Amazon\Helper\Timer
     */
    protected $instance;

    public function setUp(){

        $this->instance = (new ObjectManager($this))->getObject(
            '\BoostMyShop\Amazon\Helper\Timer',
            []
        );

    }

    public function testStart(){

        $this->assertInstanceOf('\BoostMyShop\Amazon\Helper\Timer', $this->instance->start());

    }

    public function testStop(){

        $this->assertInstanceOf('\BoostMyShop\Amazon\Helper\Timer', $this->instance->stop());
        $this->assertNotEmpty($this->instance->getDuration());

    }

}