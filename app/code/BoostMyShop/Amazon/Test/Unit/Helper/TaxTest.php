<?php namespace BoostMyShop\Amazon\Test\Unit\Helper;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;

/**
 * Class TaxTest
 *
 * @package   BoostMyShop\Amazon\Test\Unit\Helper
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class TaxTest extends \PHPUnit_Framework_TestCase {

    /**
     * @var \BoostMyShop\Amazon\Helper\Tax
     */
    protected $instance;

    /**
     * @var \Magento\Tax\Model\CalculationFactory| \PHPUnit_Framework_MockObject_MockObject
     */
    protected $taxCalculationFactory;

    /**
     * @var \Magento\Framework\DataObjectFactory| \PHPUnit_Framework_MockObject_MockObject
     */
    protected $requestFactory;

    /**
     * @var \Magento\Store\Model\StoreFactory| \PHPUnit_Framework_MockObject_MockObject
     */
    protected $storeFactory;

    public function setUp(){

        $this->taxCalculationFactory = $this->getMockBuilder('\Magento\Tax\Model\CalculationFactory')
            ->setMethods(['create'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->requestFactory = $this->getMockBuilder('\Magento\Framework\DataObjectFactory')
            ->setMethods(['create'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->storeFactory = $this->getMockBuilder('\Magento\Store\Model\StoreFactory')
            ->setMethods(['create'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->instance = (new ObjectManager($this))->getObject(
            '\BoostMyShop\Amazon\Helper\Tax',
            [
                'taxCalculationFactory' => $this->taxCalculationFactory,
                'requestFactory' => $this->requestFactory,
                'storeFactory' => $this->storeFactory
            ]
        );

    }

    public function testCalculateProductTaxRate(){

        $storeId = 2;
        $countryCode = 'FR';
        $region = 'region';
        $postCode = '74800';
        $customerTaxClassId = 1;
        $productTaxClassId = 2;

        $this->instance->setStoreId($storeId);
        $this->instance->setCountryCode($countryCode);
        $this->instance->setRegion($region);
        $this->instance->setPostCode($postCode);
        $this->instance->setCustomerTaxClassId($customerTaxClassId);
        $this->instance->setProductTaxClassId($productTaxClassId);

        $storeMock = $this->getMockBuilder('Magento\Store\Model\Store')
            ->setMethods(['load'])
            ->disableOriginalConstructor()
            ->getMock();

        $storeMock->expects($this->once())->method('load')->with($storeId)->willReturn($storeMock);

        $taxRequestMock = $this->getMockBuilder('\Magento\Framework\DataObject')
            ->setMethods(['setCountryId', 'setRegionId', 'setPostCode', 'setStore', 'setCustomerClassId', 'setProductClassId'])
            ->disableOriginalConstructor()
            ->getMock();

        $taxRequestMock->expects($this->once())->method('setCountryId')->with($countryCode);
        $taxRequestMock->expects($this->once())->method('setRegionId')->with($region);
        $taxRequestMock->expects($this->once())->method('setPostcode')->with($postCode);
        $taxRequestMock->expects($this->once())->method('setStore')->with($storeMock);
        $taxRequestMock->expects($this->once())->method('setCustomerClassId')->with($customerTaxClassId);
        $taxRequestMock->expects($this->once())->method('setProductClassId')->with($productTaxClassId);

        $this->storeFactory->expects($this->once())->method('create')->willReturn($storeMock);
        $this->requestFactory->expects($this->once())->method('create')->willReturn($taxRequestMock);

        $taxCalculationMock = $this->getMockBuilder('\Magento\Tax\Model\Calculation')
            ->setMethods(['getRate'])
            ->disableOriginalConstructor()
            ->getMock();

        $taxCalculationMock->expects($this->once())->method('getRate')->with($taxRequestMock);

        $this->taxCalculationFactory->expects($this->once())->method('create')->willReturn($taxCalculationMock);

        $this->instance->calculateProductTaxRate();

    }

}