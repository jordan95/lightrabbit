<?php namespace BoostMyShop\Amazon\Test\Unit\Helper;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;

/**
 * Class ProductTest
 *
 * @package   BoostMyShop\Amazon\Test\Unit\Helper
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class ProductTest extends \PHPUnit_Framework_TestCase {

    /**
     * @var \BoostMyShop\Amazon\Helper\Product
     */
    protected $instance;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory| \PHPUnit_Framework_MockObject_MockObject
     */
    protected $productCollectionFactory;

    /**
     * @var \Magento\Catalog\Model\ProductFactory| \PHPUnit_Framework_MockObject_MockObject
     */
    protected $productFactory;

    public function setUp(){

        $this->productCollectionFactory = $this->getMockBuilder('\Magento\Catalog\Model\ResourceModel\Product\CollectionFactory')
            ->setMethods(['create'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->productFactory = $this->getMockBuilder('\Magento\Catalog\Model\ProductFactory')
            ->setMethods(['create'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->instance = (new ObjectManager($this))->getObject(
            '\BoostMyShop\Amazon\Helper\Product',
            [
                'productCollectionFactory' => $this->productCollectionFactory,
                'productFactory' => $this->productFactory
            ]
        );

    }

    public function testGetProductFromSellerSku(){

        $sellerSku = 'sellerSku';
        $reference = 'sku';

        $productMock = $this->getMockBuilder('\Magento\Catalog\Model\Product')
            ->setMethods(['getId'])
            ->disableOriginalConstructor()
            ->getMock();

        $productCollectionMock = $this->getMockBuilder('\Magento\Catalog\Model\ResourceModel\Product\Collection')
            ->setMethods(['addAttributeToFilter', 'getFirstItem'])
            ->disableOriginalConstructor()
            ->getMock();

        $productCollectionMock->expects($this->once())
            ->method('addAttributeToFilter')
            ->with($reference, $sellerSku)
            ->willReturn($productCollectionMock);

        $productCollectionMock->expects($this->once())
            ->method('getFirstItem')
            ->willReturn($productMock);

        $this->productCollectionFactory->expects($this->once())->method('create')->willReturn($productCollectionMock);

        $this->instance->getProductFromSellerSku($sellerSku, $reference);

    }

    public function testLoadProductFromSellerSku(){

        $sellerSku = 'sellerSku';
        $reference = 'sku';
        $productId = 5;

        $loadedProductMock = $this->getMockBuilder('\Magento\Catalog\Model\Product')
            ->setMethods(['load'])
            ->disableOriginalConstructor()
            ->getMock();

        $loadedProductMock->expects($this->once())->method('load')->with($productId);

        $productMock = $this->getMockBuilder('\Magento\Catalog\Model\Product')
            ->setMethods(['getId'])
            ->disableOriginalConstructor()
            ->getMock();

        $productMock->expects($this->once())->method('getId')->willReturn($productId);

        $productCollectionMock = $this->getMockBuilder('\Magento\Catalog\Model\ResourceModel\Product\Collection')
            ->setMethods(['addAttributeToFilter', 'getFirstItem'])
            ->disableOriginalConstructor()
            ->getMock();

        $productCollectionMock->expects($this->once())
            ->method('addAttributeToFilter')
            ->with($reference, $sellerSku)
            ->willReturn($productCollectionMock);

        $productCollectionMock->expects($this->once())
            ->method('getFirstItem')
            ->willReturn($productMock);

        $this->productCollectionFactory->expects($this->once())->method('create')->willReturn($productCollectionMock);
        $this->productFactory->expects($this->once())->method('create')->willReturn($loadedProductMock);

        $this->instance->loadProductFromSellerSku($sellerSku, $reference);

    }

}