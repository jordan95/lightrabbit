<?php namespace BoostMyShop\Amazon\Test\Unit\Console\Command;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * Class CleanBatchHistoryTest
 *
 * @package   BoostMyShop\Amazon\Test\Unit\Console\Command
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class CleanBatchHistoryTest extends \PHPUnit_Framework_TestCase {

    /**
     * @var \BoostMyShop\Amazon\Console\Command\CleanBatchHistory
     */
    protected $instance;

    /**
     * @var \Magento\Framework\App\State| \PHPUnit_Framework_MockObject_MockObject
     */
    protected $state;

    /**
     * @var \BoostMyShop\Amazon\Model\ResourceModel\Batch\CollectionFactory| \PHPUnit_Framework_MockObject_MockObject
     */
    protected $batchCollectionFactory;

    public function setUp(){

        $this->state = $this->getMockBuilder('\Magento\Framework\App\State')
            ->setMethods(['setAreaCode'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->batchCollectionFactory = $this->getMockBuilder('\BoostMyShop\Amazon\Model\ResourceModel\Batch\CollectionFactory')
            ->setMethods(['create'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->instance = (new ObjectManager($this))->getObject(
            '\BoostMyShop\Amazon\Console\Command\CleanBatchHistory',
            [
                'state' => $this->state,
                'batchCollectionFactory' => $this->batchCollectionFactory
            ]
        );

    }

    public function testExecuteWithoutAll(){

        $this->state->expects($this->once())->method('setAreaCode')->with('adminhtml');
        $batchCollection = $this->getMockBuilder('\BoostMyShop\Amazon\Model\ResourceModel\Batch\Collection')
            ->setMethods(['cleanHistory'])
            ->disableOriginalConstructor()
            ->getMock();
        $batchCollection->expects($this->once())->method('cleanHistory');
        $this->batchCollectionFactory->expects($this->once())->method('create')->willReturn($batchCollection);

        $commandTester = (new CommandTester($this->instance));
        $commandTester->execute([], []);

    }

    public function testExecuteWithAll(){

        $this->state->expects($this->once())->method('setAreaCode')->with('adminhtml');
        $batchCollection = $this->getMockBuilder('\BoostMyShop\Amazon\Model\ResourceModel\Batch\Collection')
            ->setMethods(['delete'])
            ->disableOriginalConstructor()
            ->getMock();
        $batchCollection->expects($this->once())->method('delete');
        $this->batchCollectionFactory->expects($this->once())->method('create')->willReturn($batchCollection);

        $commandTester = (new CommandTester($this->instance));
        $commandTester->execute(['--all' => 1], []);

    }

}