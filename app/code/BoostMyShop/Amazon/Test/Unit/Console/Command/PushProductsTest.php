<?php namespace BoostyMyShop\Amazon\Test\Unit\Console\Command;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * Class PushProductsTest
 *
 * @package   BoostyMyShop\Amazon\Test\Unit\Console\Command
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class PushProductsTest extends \PHPUnit_Framework_TestCase {

    /**
     * @var \BoostMyShop\Amazon\Console\Command\PushProducts
     */
    protected $instance;

    /**
     * @var \BoostMyShop\Amazon\Model\Account\CountryFactory| \PHPUnit_Framework_MockObject_MockObject
     */
    protected $countryFactory;

    /**
     * @var \Magento\Framework\App\State| \PHPUnit_Framework_MockObject_MockObject
     */
    protected $state;

    public function setUp(){

        $this->countryFactory = $this->getMockBuilder('\BoostMyShop\Amazon\Model\Account\CountryFactory')
            ->setMethods(['create'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->state = $this->getMockBuilder('\Magento\Framework\App\State')
            ->setMethods(['setAreaCode'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->instance = (new ObjectManager($this))->getObject(
            '\BoostMyShop\Amazon\Console\Command\PushProducts',
            [
                'countryFactory' => $this->countryFactory,
                'state' => $this->state
            ]
        );

    }

    public function testExecute(){

        $countryId= 1;
        $this->state->expects($this->once())->method('setAreaCode')->with('adminhtml');
        $country = $this->getMockBuilder('\BoostMyShop\Amazon\Model\Account\Country')
            ->setMethods(['load', 'getId', 'pushProducts'])
            ->disableOriginalConstructor()
            ->getMock();

        $country->expects($this->once())->method('load')->willReturnSelf();
        $country->expects($this->once())->method('getId')->willReturn($countryId);
        $country->expects($this->once())->method('pushProducts')->willReturn('');

        $this->countryFactory->expects($this->once())->method('create')->willReturn($country);

        $commandTester = (new CommandTester($this->instance));
        $commandTester->execute(['--countryId'=>$countryId], []);

    }

    public function testExecuteWithBadCountryId(){

        $countryId= 1;
        $this->state->expects($this->once())->method('setAreaCode')->with('adminhtml');
        $country = $this->getMockBuilder('\BoostMyShop\Amazon\Model\Account\Country')
            ->setMethods(['load', 'getId'])
            ->disableOriginalConstructor()
            ->getMock();

        $country->expects($this->once())->method('load')->willReturnSelf();
        $country->expects($this->once())->method('getId')->willReturn(null);

        $this->countryFactory->expects($this->once())->method('create')->willReturn($country);

        $commandTester = (new CommandTester($this->instance));
        $commandTester->execute(['--countryId'=>$countryId], []);

    }

}