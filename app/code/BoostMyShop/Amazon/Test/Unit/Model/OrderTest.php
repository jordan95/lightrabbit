<?php namespace BoostMyShop\Amazon\Test\Unit\Model;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;

/**
 * Class OrderTest
 *
 * @package   BoostMyShop\Amazon\Test\Unit\Model
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class OrderTest extends \PHPUnit_Framework_TestCase {

    /**
     * @var \BoostMyShop\Amazon\Model\Order
     */
    protected $instance;

    /**
     * @var \BoostMyShop\Amazon\Helper\ProductFactory
     */
    protected $productHelperFactory;

    /**
     * @var \BoostMyShop\Amazon\Model\ResourceModel\Order\Item\CollectionFactory
     */
    protected $orderItemCollectionFactory;

    /**
     * @var \BoostMyShop\Amazon\Model\Order\ItemFactory
     */
    protected $orderItemFactory;

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    protected $orderCollectionFactory;

    /**
     * @var \Magento\Framework\Model\Context
     */
    protected $context;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    public function setUp(){

        $this->productHelperFactory = $this->getMockBuilder('\BoostMyShop\Amazon\Helper\ProductFactory')
            ->setMethods(['create'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->orderItemCollectionFactory = $this->getMockBuilder('\BoostMyShop\Amazon\Model\ResourceModel\Order\Item\CollectionFactory')
            ->setMethods(['create'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->orderItemFactory = $this->getMockBuilder('\BoostMyShop\Amazon\Model\Order\ItemFactory')
            ->setMethods(['create'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->orderCollectionFactory = $this->getMockBuilder('\Magento\Sales\Model\ResourceModel\Order\CollectionFactory')
            ->setMethods(['create'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->context = $this->getMockBuilder('\Magento\Framework\Model\Context')
            ->disableOriginalConstructor()
            ->getMock();

        $this->registry = $this->getMockBuilder('\Magento\Framework\Registry')
            ->disableOriginalConstructor()
            ->getMock();

        $this->instance = (new ObjectManager($this))->getObject(
            '\BoostMyShop\Amazon\Model\Order',
            [
                'productHelperFactory' => $this->productHelperFactory,
                'orderItemCollectionFactory' => $this->orderItemCollectionFactory,
                'orderItemFactory' => $this->orderItemFactory,
                'orderCollectionFactory' => $this->orderCollectionFactory,
                'context' => $this->context,
                'registry' => $this->registry
            ]
        );

    }

}