<?php namespace BoostMyShop\Amazon\Test\Unit\Model\Country;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;

/**
 * Class CountryTest
 *
 * @package   BoostMyShop\Amazon\Test\Unit\Model\Country
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class CountryTest extends \PHPUnit_Framework_TestCase {

    protected $taxHelper;

    protected $request;

    protected $quoteManagement;

    protected $customerRepository;

    protected $customerFactory;

    protected $storeFactory;

    protected $quoteFactory;

    protected $invoiceModel;

    protected $countryFactory;

    protected $productHelper;

    protected $orderFactory;

    protected $domDocumentFactory;

    protected $batchFactory;

    protected $mwsHelper;

    protected $accountFactory;

    protected $countryOptionFactory;

    protected $countryOptionCollectionFactory;

    protected $context;

    protected $registry;

    protected $helper;

    protected function mockFactory($namespace){

        return $this->getMockBuilder($namespace)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();

    }

    protected function mockObject($namespace){

        return $this->getMockBuilder($namespace)
            ->disableOriginalConstructor()
            ->getMock();

    }

    protected function setUp(){

        $this->taxHelper = $this->mockObject('\BoostMyShop\Amazon\Helper\Tax');
        $this->request = $this->mockObject('\Magento\Framework\DataObject');
        $this->quoteManagement = $this->mockObject('\Magento\Quote\Model\QuoteManagement');
        $this->customerRepository = $this->mockObject('\Magento\Customer\Api\CustomerRepositoryInterface');
        $this->customerFactory = $this->mockFactory('\Magento\Customer\Model\CustomerFactory');
        $this->storeFactory = $this->mockFactory('\Magento\Store\Model\StoreFactory');
        $this->quoteFactory = $this->mockFactory('\Magento\Quote\Model\QuoteFactory');
        $this->invoiceModel = $this->mockObject('\BoostMyShop\Amazon\Model\Order\Invoice');
        $this->countryFactory = $this->mockFactory('\BoostMyShop\Amazon\Model\CountryFactory');
        $this->productHelper = $this->mockObject('\BoostMyShop\Amazon\Helper\Product');
        $this->orderFactory = $this->mockFactory('\BoostMyShop\Amazon\Model\OrderFactory');
        $this->domDocumentFactory = $this->mockFactory('\BoostMyShop\Amazon\Helper\DOMDocumentFactory');
        $this->batchFactory = $this->mockFactory('\BoostMyShop\Amazon\Model\BatchFactory');
        $this->mwsHelper = $this->mockObject('\BoostMyShop\Amazon\Helper\MWS');
        $this->accountFactory = $this->mockFactory('\BoostMyShop\Amazon\Model\AccountFactory');
        $this->countryOptionFactory = $this->mockFactory('\BoostMyShop\Amazon\Model\Country\OptionFactory');
        $this->countryOptionCollectionFactory = $this->mockFactory('\BoostMyShop\Amazon\Model\ResourceModel\Country\Option\CollectionFactory');
        $this->context = $this->mockObject('\Magento\Framework\Model\Context');
        $this->registry = $this->mockObject('\Magento\Framework\Registry');

        $objectManager = new ObjectManager($this);

        $this->helper = $objectManager->getObject(
            '\BoostMyShop\Amazon\Model\Account\Country',
            [
                'taxHelper' => $this->taxHelper,
                'request' => $this->request,
                'quoteManagement' => $this->quoteManagement,
                'customerRepository' => $this->customerRepository,
                'customerFactory' => $this->customerFactory,
                'storeFactory' => $this->storeFactory,
                'quoteFactory' => $this->quoteFactory,
                'invoiceModel' => $this->invoiceModel,
                'countryFactory' => $this->countryFactory,
                'productHelper' => $this->productHelper,
                'orderFactory' => $this->orderFactory,
                'DOMDocumentFactory' => $this->domDocumentFactory,
                'batchFactory' => $this->batchFactory,
                'mwsHelper' => $this->mwsHelper,
                'accountFactory' => $this->accountFactory,
                'countryOptionFactory' => $this->countryOptionFactory,
                'countryOptionCollectionFactory' => $this->countryOptionCollectionFactory,
                'context' => $this->context,
                'registry' => $this->registry
            ]
        );

    }

    public function testSetOption(){

        $option = 'field';
        $value = 'value';
        $countryId = 1;

        $this->helper->setId($countryId);

        $countryOptionMock = $this->getMockBuilder('\BoostMyShop\Amazon\Model\Country\Option')
            ->disableOriginalConstructor()
            ->getMock();
        $countryOptionMock->expects($this->once())
            ->method('addOrUpdateParam')
            ->with($countryId, $option, $value);

        $this->countryOptionFactory->expects($this->once())
            ->method('create')
            ->willReturn($countryOptionMock);

        $this->helper->setOption($option, $value);

    }

    public function testGetOption(){

        $option = 'option';
        $value = 'value';
        $countryId = 1;

        $this->helper->setId($countryId);

        $countryOptionCollectionMock = $this->getMockBuilder('\BoostMyShop\Amazon\Model\ResourceModel\Country\Option\Collection')
            ->disableOriginalConstructor()
            ->getMock();
        $countryOptionCollectionMock->expects($this->once())
            ->method('getParamValue')
            ->with($countryId, $option)
            ->willReturn($value);

        $this->countryOptionCollectionFactory->expects($this->once())
            ->method('create')
            ->willReturn($countryOptionCollectionMock);

        $this->assertEquals($value, $this->helper->getOption($option));

    }

    public function testEmptyGetOptionWithoutDefaultValue(){

        $option = 'option';
        $countryId = 1;

        $this->helper->setId($countryId);

        $countryOptionCollectionMock = $this->getMockBuilder('\BoostMyShop\Amazon\Model\ResourceModel\Country\Option\Collection')
            ->disableOriginalConstructor()
            ->getMock();
        $countryOptionCollectionMock->expects($this->once())
            ->method('getParamValue')
            ->with($countryId, $option)
            ->willReturn('');

        $this->countryOptionCollectionFactory->expects($this->once())
            ->method('create')
            ->willReturn($countryOptionCollectionMock);

        $this->assertEquals('', $this->helper->getOption($option));

    }

    public function testGetDefaultOptionValueWithoutDefaultValue(){

        $this->assertEquals('', $this->helper->getDefaultOptionValue('option'));

    }

    public function testGetDefaultOptionValueForReference(){

        $this->assertEquals('sku', $this->helper->getDefaultOptionValue('reference'));

    }

    public function testGetDefaultOptionValueForOrderState(){

        $this->assertEquals('pending', $this->helper->getDefaultOptionValue('order_state'));

    }

    public function testGetDefaultOptionValueForCustomerMode(){

        $this->assertEquals('create', $this->helper->getDefaultOptionValue('customer_mode'));

    }

    public function testApplyOptions(){

        $this->fail('todo');

    }

    public function testGetLabel(){

        $this->fail('todo');

    }

    public function testGetAccount(){

        $this->fail('todo');

    }

    public function testImportOrders(){

        $this->fail('todo');

    }

    public function testShipOrders(){

        $this->fail('todo');

    }

}