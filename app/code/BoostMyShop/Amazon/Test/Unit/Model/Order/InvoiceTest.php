<?php namespace BoostMyShop\Amazon\Test\Unit\Model\Order;

/**
 * Class InvoiceTest
 *
 * @package   BoostMyShop\Amazon\Test\Unit\Model\Order
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class InvoiceTest extends \PHPUnit_Framework_TestCase {

    /**
     * @var \BoostMyShop\Amazon\Model\Order\Invoice
     */
    protected $instance;

}