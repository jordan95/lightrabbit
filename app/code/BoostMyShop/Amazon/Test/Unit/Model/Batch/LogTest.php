<?php namespace BoostMyShop\Amazon\Test\Unit\Model\Batch;

/**
 * Class LogTest
 *
 * @package   BoostMyShop\Amazon\Test\Unit\Model\Batch
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class LogTest extends \PHPUnit_Framework_TestCase {

    /**
     * @var \BoostMyShop\Amazon\Model\Batch\Log
     */
    protected $instance;

}