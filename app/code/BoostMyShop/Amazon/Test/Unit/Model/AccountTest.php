<?php namespace BoostMyShop\Amazon\Test\Unit\Model;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;

/**
 * Class AccountTest
 *
 * @package   BoostMyShop\Amazon\Test\Unit\Model
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class AccountTest extends \PHPUnit_Framework_TestCase {

    protected $mwsSellerFactory;

    protected $accountCountryFactory;

    protected $accountType;

    protected $logger;

    protected $country;

    protected $context;

    protected $registry;

    protected $resource;

    protected $resourceCollection;

    protected $instance;

    public function setUp(){

        $this->mwsSellerFactory = $this->getMockBuilder('\BoostMyShop\Amazon\Helper\Mws\SellersFactory')
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();

        $this->accountCountryFactory = $this->getMockBuilder('\BoostMyShop\Amazon\Model\Account\CountryFactory')
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();

        $this->accountType = $this->getMockBuilder('\BoostMyShop\Amazon\Model\Account\Type')
            ->disableOriginalConstructor()
            ->getMock();

        $this->logger = $this->getMockBuilder('\BoostMyShop\Amazon\Helper\Logger')
            ->disableOriginalConstructor()
            ->getMock();

        $this->country = $this->getMockBuilder('\BoostMyShop\Amazon\Model\Country')
            ->disableOriginalConstructor()
            ->getMock();

        $this->context = $this->getMockBuilder('\Magento\Framework\Model\Context')
            ->disableOriginalConstructor()
            ->getMock();

        $this->registry = $this->getMockBuilder('\Magento\Framework\Registry')
            ->disableOriginalConstructor()
            ->getMock();

        $objectManager = new ObjectManager($this);

        $this->instance = $objectManager->getObject(
            '\BoostMyShop\Amazon\Model\Account',
            [
                $this->mwsSellerFactory,
                $this->accountCountryFactory,
                $this->accountType,
                $this->logger,
                $this->country,
                $this->context,
                $this->registry,
                $this->resource,
                $this->resourceCollection,
                []
            ]
        );


    }

    public function testAddMarketplace(){

        $countryCode = 'FR';
        $marketplace = [
            'marketplaceId' => '',
            'name' => '',
            'countryCode' => '',
            'currencyCode' => '',
            'languageCode' => '',
            'domain' => '',
            'isProdMarketplace' => ''
        ];

        $result = $this->instance->addMarketplace($countryCode, $marketplace);
        $this->assertInstanceOf('\BoostMyShop\Amazon\Model\Account', $result);

    }

    public function testBeforeSave(){

        $this->fail('todo');

    }

    public function testValidateBeforeSave(){

        $this->fail('todo');

    }

    public function testAfterSave(){

        $this->fail('todo');

    }

    public function testGetMarketplaces(){

        $this->fail('todo');

    }

    public function testGetMarketplacesWithoutSuspendedListing(){

        $this->fail('todo');

    }

}