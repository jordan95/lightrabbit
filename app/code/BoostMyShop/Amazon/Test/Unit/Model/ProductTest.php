<?php namespace BoostMyShop\Amazon\Test\Unit\Model;

require_once dirname(__FILE__).'/../../../../../../functions.php';

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;

/**
 * Class ProductTest
 *
 * @package   BoostMyShop\Amazon\Test\Unit\Model
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class ProductTest extends \PHPUnit_Framework_TestCase {

    /**
     * @var \BoostMyShop\Amazon\Model\Product
     */
    protected $instance;

    public function setUp(){

        $this->instance = (new ObjectManager($this))->getObject(
               '\BoostMyShop\Amazon\Model\Product',
            []
        );

    }

    public function testGetStatuses(){

        $statuses = $this->instance->getStatuses();
        $this->assertNotEmpty($statuses);

    }

    public function testGetOfferStatuses(){

        $statuses = $this->instance->getOfferStatuses();
        $this->assertNotEmpty($statuses);

    }

}