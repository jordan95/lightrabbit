<?php namespace BoostMyShop\Amazon\Test\Unit\Observer;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;

/**
 * Class CatalogProductAfterSaveTest
 *
 * @package   BoostMyShop\Amazon\Test\Unit\Observer
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class CatalogProductAfterSaveTest extends \PHPUnit_Framework_TestCase {

    /**
     * @var \BoostMyShop\Amazon\Observer\CatalogProductAfterSave
     */
    protected $instance;

    public function setUp(){

        $this->instance = (new ObjectManager($this))->getObject(
            '\BoostMyShop\Amazon\Observer\CatalogProductAfterSave',
            []
        );

    }

    public function testExecute(){

        $productId = 1;

        $connection = $this->getMockBuilder('\Magento\Framework\DB\Adapter\AdapterInterface')
            ->setMethods(['query'])
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $connection->expects($this->once())->method('query');

        $resource = $this->getMockBuilder('\Magento\Framework\Model\ResourceModel\Db\AbstractDb')
            ->setMethods(['getTable', 'getConnection'])
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $resource->expects($this->once())->method('getConnection')->willReturn($connection);
        $resource->expects($this->once())->method('getTable')->with('catalog_product_entity')->willReturn('catalog_product_entity');

        $product = $this->getMockBuilder('\Magento\Catalog\Model\Product')
            ->setMethods(['getResource', 'getId'])
            ->disableOriginalConstructor()
            ->getMock();

        $product->expects($this->once())->method('getId')->willReturn($productId);
        $product->expects($this->any())->method('getResource')->willReturn($resource);

        $event = $this->getMockBuilder('\Magento\Framework\Event')
            ->setMethods(['getProduct'])
            ->disableOriginalConstructor()
            ->getMock();

        $event->expects($this->once())->method('getProduct')->willReturn($product);

        $observer = $this->getMockBuilder('\Magento\Framework\Event\Observer')
            ->setMethods(['getEvent'])
            ->disableOriginalConstructor()
            ->getMock();

        $observer->expects($this->once())->method('getEvent')->willReturn($event);

        $this->instance->execute($observer);

    }

}