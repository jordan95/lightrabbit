<?php namespace BoostMyShop\Amazon\Test\Unit\Block\Dashboard;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;

/**
 * Class SalesTest
 *
 * @package   BoostMyShop\Amazon\Test\Unit\Block\Dashboard
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class SalesTest extends \PHPUnit_Framework_TestCase {

    /**
     * @var \BoostMyShop\Amazon\Block\Dashboard\Sales
     */
    protected $instance;

}