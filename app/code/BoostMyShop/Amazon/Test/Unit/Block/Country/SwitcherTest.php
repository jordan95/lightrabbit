<?php namespace BoostMyShop\Amazon\Test\Unit\Block\Account;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;

/**
 * Class CountryTest
 *
 * @package   BoostMyShop\Amazon\Test\Unit\Block\Account
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class CountryTest extends \PHPUnit_Framework_TestCase {

    /**
     * @var \BoostMyShop\Amazon\Block\Country\Switcher
     */
    protected $instance;

    /**
     * @var \Magento\Framework\Registry| \PHPUnit_Framework_MockObject_MockObject
     */
    protected $registry;

    /**
     * @var \BoostMyShop\Amazon\Model\Account\Country| \PHPUnit_Framework_MockObject_MockObject
     */
    protected $country;

    /**
     * @var \Magento\Backend\Block\Widget\Context| \PHPUnit_Framework_MockObject_MockObject
     */
    protected $context;

    public function setUp(){

        $this->registry = $this->getMockBuilder('\Magento\Framework\Registry')
            ->setMethods(['registry'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->country = $this->getMockBuilder('\BoostMyShop\Amazon\Model\Account\Country')
            ->setMethods(['getCollection'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->context = $this->getMockBuilder('\Magento\Backend\Block\Widget\Context')
            ->disableOriginalConstructor()
            ->getMock();

        $this->instance = (new ObjectManager($this))->getObject(
            '\BoostMyShop\Amazon\Block\Country\Switcher',
            [
                'registry' => $this->registry,
                'country' => $this->country,
                'context' => $this->context
            ]
        );

    }

    public function testGetCountries(){

        $this->fail('todo');

    }

    public function testGetCurrentAccountCountryId(){

        $this->registry->expects($this->once())->method('registry')->with('account_country_id');
        $this->instance->getCurrentAccountCountryId();

    }

}