<?php namespace BoostMyShop\Amazon\Test\Unit\Block\Account\Edit\Tabs;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;

/**
 * Class MarketplaceTest
 *
 * @package   BoostMyShop\Amazon\Test\Unit\Block\Account\Edit\Tabs
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class MarketplaceTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var \BoostMyShop\Amazon\Block\Account\Edit\Tabs\Marketplace
     */
    protected $instance;

    /**
     * @var \Magento\Backend\App\ConfigInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $backendConfig;

    /**
     * @var \Magento\Tax\Model\TaxClass\Source\Customer| \PHPUnit_Framework_MockObject_MockObject
     */
    protected $customerTaxModel;

    /**
     * @var \Magento\Quote\Model\ResourceModel\Quote\Address\Rate\CollectionFactory| \PHPUnit_Framework_MockObject_MockObject
     */
    protected $rateCollectionFactory;

    /**
     * @var \Magento\Payment\Model\Config| \PHPUnit_Framework_MockObject_MockObject
     */
    protected $paymentModelConfig;

    /**
     * @var \Magento\Customer\Model\GroupFactory| \PHPUnit_Framework_MockObject_MockObject
     */
    protected $customerGroupFactory;

    /**
     * @var \Magento\Eav\Model\Entity\TypeFactory| \PHPUnit_Framework_MockObject_MockObject
     */
    protected $entityTypeFactory;

    /**
     * @var \Magento\Eav\Model\Entity\AttributeFactory| \PHPUnit_Framework_MockObject_MockObject
     */
    protected $entityAttributeFactory;

    /**
     * @var \Magento\Sales\Model\Order\StatusFactory| \PHPUnit_Framework_MockObject_MockObject
     */
    protected $orderStatusFactory;

    /**
     * @var \Magento\Backend\Block\Template\Context| \PHPUnit_Framework_MockObject_MockObject
     */
    protected $context;

    /**
     * @var \Magento\Framework\Registry| \PHPUnit_Framework_MockObject_MockObject
     */
    protected $registry;

    /**
     * @var \Magento\Framework\Data\FormFactory| \PHPUnit_Framework_MockObject_MockObject
     */
    protected $formFactory;

    public function setUp(){

        $this->backendConfig = $this->getMockBuilder('\Magento\Backend\App\ConfigInterface')
            ->disableGeneralConstructor()
            ->getMock();

        $this->customerTaxModel = $this->getMockBuilder('\Magento\Tax\Model\TaxClass\Source\Customer')
            ->disableOriginalConstructor()
            ->getMock();

        $this->rateCollectionFactory = $this->getMockBuilder('\Magento\Quote\Model\ResourceModel\Quote\Address\Rate\CollectionFactory')
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();

        $this->paymentModelConfig = $this->getMockBuilder('\Magento\Payment\Model\Config')
            ->disableOriginalConstructor()
            ->getrMock();

        $this->customerGroupFactory = $this->getMockBuilder('\Magento\Customer\Model\GroupFactory')
            ->disableOriginalConstructor()
            ->getMock();

        $this->entityTypeFactory = $this->getMockBuilder('\Magento\Eav\Model\Entity\TypeFactory')
            ->disableOriginalConstructor()
            ->getMock();

        $this->entityAttributeFactory = $this->getMockBuilder('\Magento\Eav\Model\Entity\AttributeFactory')
            ->disableOriginalConstructor()
            ->getMock();

        $this->orderStatusFactory = $this->getMockBuilder('\Magento\Sales\Model\Order\StatusFactory')
            ->disableOriginalConstructor()
            ->getMock();

        $this->context = $this->getMockBuilder('\Magento\Backend\Block\Template\Context')
            ->disableOriginalConstructor()
            ->getMock();

        $this->registry = $this->getMockBuilder('\Magento\Framework\Registry')
            ->disableOriginalConstructor()
            ->getMock();

        $this->formFactory = $this->getMockBuilder('\Magento\Framework\Data\FormFactory')
            ->disableOriginalConstructor()
            ->getMock();

        $objectManager = new ObjectManager($this);
        $this->instance = $objectManager->getObject(
            '\BoostMyShop\Amazon\Block\Account\Edit\Tabs\Marketplace',
            [
                'backendConfig' => $this->backendConfig,
                'customerTaxClassModel' => $this->customerTaxModel,
                'rateCollection>Factory' => $this->rateCollectionFactory,
                'paymentModelConfig' => $this->paymentModelConfig,
                'customerGroupFactory' => $this->customerGroupFactory,
                'entityTypeFactory' => $this->entityTypeFactory,
                'entityAttributeFactory' => $this->entityAttributeFactory,
                'orderStatusFactory' => $this->orderStatusFactory,
                'context' => $this->context,
                'registry' => $this->registry,
                'formFactory' => $this->formFactory
            ]
        );

    }

}