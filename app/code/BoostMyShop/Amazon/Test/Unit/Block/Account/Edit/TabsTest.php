<?php namespace BoostMyShop\Amazon\Test\Unit\Block\Account\Edit;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;

/**
 * Class TabsTest
 *
 * @package   BoostMyShop\Amazon\Test\Unit\Block\Account\Edit
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class TabsTest extends \PHPUnit_Framework_TestCase {

    /**
     * @var \BoostMyShop\Amazon\Block\Account\Edit\Tabs
     */
    protected $instance;

    /**
     * @var \Magento\Framework\Registry| \PHPUnit_Framework_MockObject_MockObject
     */
    protected $registry;

    /**
     * @var \Magento\Backend\Block\Template\Context| \PHPUnit_Framework_MockObject_MockObject
     */
    protected $context;

    /**
     * @var \Magento\Framework\Json\EncoderInterface| \PHPUnit_Framework_MockObject_MockObject
     */
    protected $jsonEncoder;

    /**
     * @var \Magento\Backend\Model\Auth\Session| \PHPUnit_Framework_MockObject_MockObject
     */
    protected $authSession;

    public function setUp(){

        $this->registry = $this->getMockBuilder('\Magento\Framework\Registry')
            ->disableOriginalConstructor()
            ->getMock();

        $this->context = $this->getMockBuilder('\Magento\Backend\Block\Template\Context')
            ->disableOriginalConstructor()
            ->getMock();

        $this->jsonEncoder = $this->getMockBuilder('\Magento\Framework\Json\EncoderInterface')
            ->disableOriginalConstructor()
            ->getMock();

        $this->authSession = $this->getMockBuilder('\Magento\Backend\Model\Auth\Session')
            ->disableOriginalConstructor()
            ->getMock();

        $objectManager = new ObjectManager($this);
        $this->instance = $objectManager->getObject(
            '\BoostMyShop\Amazon\Block\Account\Edit\Tabs',
            [
                'registry' => $this->registry,
                'context' => $this->context,
                'jsonEncoder' => $this->jsonEncoder,
                'authSession' => $this->authSession
            ]
        );

    }

}