<?php namespace BoostMyShop\Amazon\Test\Unit\Block\Account;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;

/**
 * Class GridTest
 *
 * @package   BoostMyShop\Amazon\Test\Unit\Block\Account
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class GridTest extends \PHPUnit_Framework_TestCase {

    /**
     * @var \BoostMyShop\Amazon\Block\Account\Grid
     */
    protected $instance;

    /**
     * @var \BoostMyShop\Amazon\Model\ResourceModel\Account\CollectionFactory| \PHPUnit_Framework_MockObject_MockObject
     */
    protected $accountCollectionFactory;

    /**
     * @var \Magento\Backend\Block\Template\Context| \PHPUnit_Framework_MockObject_MockObject
     */
    protected $context;

    /**
     * @var \Magento\Backend\Helper\Data| \PHPUnit_Framework_MockObject_MockObject
     */
    protected $backendHelper;

    public function setUp(){

        $this->accountCollectionFactory = $this->getMockBuilder('\BoostMyShop\Amazon\Model\ResourceModel\Account\CollectionFactory')
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();

        $this->context = $this->getMockBuilder('\Magento\Backend\Block\Template\Context')
            ->disableOriginalConstructor()
            ->getMock();

        $this->backendHelper = $this->getMockBuilder('\Magento\Backend\Helper\Data')
            ->disableOriginalConstructor()
            ->getMock();

        $this->instance = (new ObjectManager($this))->getObject(
            '\BoostMyShop\Amazon\Block\Account\Grid',
            [
                'accountCollectionFactory' => $this->accountCollectionFactory,
                'context' => $this->context,
                'backendHelper' => $this->backendHelper
            ]
        );

    }

}