<?php namespace BoostMyShop\Amazon\Test\Unit\Block\Account\Edit\Tabs;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;

/**
 * Class GeneralTest
 *
 * @package   BoostMyShop\Amazon\Test\Unit\Block\Account\Edit\Tabs
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class GeneralTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \BoostMyShop\Amazon\Block\Account\Edit\Tabs\General
     */
    protected $instance;

    public function setUp(){

        $objectManager = new ObjectManager($this);

        $this->instance = $objectManager->getObject(
            '\BoostMyShop\Amazon\Block\Account\Edit\Tabs\General',
            []
        );

    }
}