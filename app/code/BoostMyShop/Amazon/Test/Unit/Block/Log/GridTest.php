<?php namespace BoostMyShop\Amazon\Test\Unit\Block\Log;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;

/**
 * Class GridTest
 *
 * @package   BoostMyShop\Amazon\Test\Unit\Block\Log
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class GridTest extends \PHPUnit_Framework_TestCase {

    /**
     * @var \BoostMyShop\Amazon\Block\Log\Grid
     */
    protected $instance;

    /**
     * @var \Magento\Framework\Registry| \PHPUnit_Framework_MockObject_MockObject
     */
    protected $coreRegistry;

    /**
     * @var \BoostMyShop\Amazon\Model\Batch\LogFactory| \PHPUnit_Framework_MockObject_MockObject
     */
    protected $logFactory;

    /**
     * @var \Magento\Backend\Block\Template\Context| \PHPUnit_Framework_MockObject_MockObject
     */
    protected $context;

    /**
     * @var \Magento\Backend\Helper\Data| \PHPUnit_Framework_MockObject_MockObject
     */
    protected $backendHelper;

    public function setUp(){

        $this->coreRegistry = $this->getMockBuilder('\Magento\Framework\Registry')
            ->disableOriginalConstructor()
            ->getMock();

        $this->logFactory = $this->getMockBuilder('\BoostMyShop\Amazon\Model\Batch\LogFactory')
            ->setMethods(['create'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->context = $this->getMockBuilder('\Magento\Backend\Block\Template\Context')
            ->disableOriginalConstructor()
            ->getMock();

        $this->backendHelper = $this->getMockBuilder('\Magento\Backend\Helper\Data')
            ->disableOriginalConstructor()
            ->getMock();

        $this->instance = (new ObjectManager($this))->getObject(
            '\BoostMyShop\Amazon\Block\Log\Grid',
            [
                'coreRegistry' => $this->coreRegistry,
                'logFactory' => $this->logFactory,
                'context' => $this->context,
                'backendHelper' => $this->backendHelper
            ]
        );

    }

}