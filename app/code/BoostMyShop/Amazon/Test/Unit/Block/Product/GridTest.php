<?php namespace BoostMyShop\Amazon\Test\Unit\Block\Product;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;

/**
 * Class GridTest
 *
 * @package   BoostMyShop\Amazon\Test\Unit\Block\Product
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class GridTest extends \PHPUnit_Framework_TestCase {

    /**
     * @var \BoostMyShop\Amazon\Block\Product\Grid
     */
    protected $instance;

    /**
     * @var \Magento\Framework\Registry| \PHPUnit_Framework_MockObject_MockObject
     */
    protected $registry;

    /**
     * @var \BoostMyShop\Amazon\Model\Account\CountryFactory| \PHPUnit_Framework_MockObject_MockObject
     */
    protected $countryFactory;

    /**
     * @var \BoostMyShop\Amazon\Model\ProductFactory| \PHPUnit_Framework_MockObject_MockObject
     */
    protected $productFactory;

    /**
     * @var \BoostMyShop\Amazon\Model\ResourceModel\Catalog\Product\CollectionFactory| \PHPUnit_Framework_MockObject_MockObject
     */
    protected $productCollectionFactory;

    /**
     * @var \Magento\Backend\Block\Template\Context| \PHPUnit_Framework_MockObject_MockObject
     */
    protected $context;

    /**
     * @var \Magento\Backend\Helper\Data| \PHPUnit_Framework_MockObject_MockObject
     */
    protected $backendHelper;

    public function setUp(){

        $this->registry = $this->getMockBuilder('\Magento\Framework\Registry')
            ->disableOriginalConstructor()
            ->getMock();

        $this->countryFactory = $this->getMockBuilder('\BoostMyShop\Amazon\Model\Account\CountryFactory')
            ->setMethods(['create'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->productFactory = $this->getMockBuilder('\BoostMyShop\Amazon\Model\ProductFactory')
            ->setMethods(['create'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->productCollectionFactory = $this->getMockBuilder('\BoostMyShop\Amazon\Model\ResourceModel\Catalog\Product\CollectionFactory')
            ->setMethods(['create'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->context = $this->getMockBuilder('\Magento\Backend\Block\Template\Context')
            ->disableOriginalConstructor()
            ->getMock();

        $this->backendHelper = $this->getMockBuilder('\Magento\Backend\Helper\Data')
            ->disableOriginalConstructor()
            ->getMock();

        $this->instance = (new ObjectManager($this))->getObject(
            '\BoostMyShop\Amazon\Block\Product\Grid',
            [
                'registry' => $this->registry,
                'countryFactory' => $this->countryFactory,
                'productFactory' => $this->productFactory,
                'productCollectionFactory' => $this->productCollectionFactory,
                'context' => $this->context,
                'backendHelper' => $this->backendHelper
            ]
        );

    }

}