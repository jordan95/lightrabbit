<?php namespace BoostMyShop\Amazon\Test\Unit\Block\Batch;

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;

/**
 * Class GridTest
 *
 * @package   BoostMyShop\Amazon\Test\Unit\Block\Batch
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class GridTest extends \PHPUnit_Framework_TestCase {

    /**
     * @var \BoostMyShop\Amazon\Block\Batch\Grid
     */
    protected $instance;

    /**
     * @var \BoostMyShop\Amazon\Model\Account\CountryFactory| \PHPUnit_Framework_MockObject_MockObject
     */
    protected $countryFactory;

    /**
     * @var \BoostMyShop\Amazon\Model\BatchFactory| \PHPUnit_Framework_MockObject_MockObject
     */
    protected $batchFactory;

    /**
     * @var \Magento\Backend\Block\Template\Context| \PHPUnit_Framework_MockObject_MockObject
     */
    protected $context;

    /**
     * @var \Magento\Backend\Helper\Data| \PHPUnit_Framework_MockObject_MockObject
     */
    protected $backendHelper;

    public function setUp(){

        $this->countryFactory = $this->getMockBuilder('\BoostMyShop\Amazon\Model\Account\CountryFactory')
            ->setMethods(['create'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->batchFactory = $this->getMockBuilder('\BoostMyShop\Amazon\Model\BatchFactory')
            ->setMethods(['create'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->context = $this->getMockBuilder('\Magento\Backend\Block\Template\Context')
            ->disableOriginalConstructor()
            ->getMock();

        $this->backendHelper = $this->getMockBuilder('\Magento\Backend\Helper\Data')
            ->disableOriginalConstructor()
            ->getMock();

        $this->instance = (new ObjectManager($this))->getObject(
            '\BoostMyShop\Amazon\Block\Batch\Grid',
            [
                'countryFactory' => $this->countryFactory,
                'batchFactory' => $this->batchFactory,
                'context' => $this->context,
                'backendHelper' => $this->backendHelper
            ]
        );

    }

}