<?php namespace BoostMyShop\Amazon\Test\Unit\Controller\Adminhtml\Account;

/**
 * Class EditTest
 *
 * @package   BoostMyShop\Amazon\Test\Unit\Controller\Adminhtml\Account
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class EditTest extends \BoostMyShop\Amazon\Test\Unit\Controller\Adminhtml\Account {

    public function getClassName()
    {
        return '\BoostMyShop\Amazon\Controller\Adminhtml\Account\Edit';
    }

    /**
     * @todo
     */
    public function testExecute(){

        $this->fail('todo');

    }

}