<?php namespace BoostMyShop\Amazon\Test\Unit\Controller\Adminhtml\Account;

require_once dirname(__FILE__).'/../../../../../../../../functions.php';

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;

/**
 * Class IndexTest
 *
 * @package   BoostMyShop\Amazon\Test\Unit\Controller\Adminhtml\Account
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class IndexTest extends \PHPUnit_Framework_TestCase {

    /**
     * @var \BoostMyShop\Amazon\Controller\Adminhtml\Account\Index
     */
    protected $instance;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry;

    /**
     * @var \BoostMyShop\Amazon\Model\AccountFactory
     */
    protected $countryFactory;

    /**
     * @var \Magento\Backend\App\Action\Context
     */
    protected $context;

    public function setUp()
    {

        $this->coreRegistry = $this->getMockBuilder('\Magento\Framework\Registry')
            ->disableOriginalConstructor()
            ->getMock();

        $this->countryFactory = $this->getMockBuilder('\BoostMyShop\Amazon\Model\AccountFactory')
            ->setMethods(['create'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->context = $this->getMockBuilder('\Magento\Backend\App\Action\Context')
            ->setMethods(['getView'])
            ->disableOriginalConstructor()
            ->getMock();

        $title = $this->getMockBuilder('\Magento\Framework\App\Title')
            ->setMethods(['prepend'])
            ->disableOriginalConstructor()
            ->getMock();

        $title->expects($this->once())->method('prepend')->with('Amazon accounts');

        $config = $this->getMockBuilder('\Magento\Framework\View\Page\Config')
            ->setMethods(['getTitle'])
            ->disableOriginalConstructor()
            ->getMock();

        $config->expects($this->once())->method('getTitle')->willReturn($title);

        $page = $this->getMockBuilder('\Magento\Framework\View\Result\Page')
            ->setMethods(['getConfig'])
            ->disableOriginalConstructor()
            ->getMock();

        $page->expects($this->once())->method('getConfig')->willReturn($config);

        $view = $this->getMockBuilder('\Magento\Framework\App\View')
            ->disableOriginalConstructor()
            ->getMock();

        $view->expects($this->once())->method('loadLayout');
        $view->expects($this->once())->method('renderLayout');
        $view->expects($this->once())->method('getPage')->willReturn($page);

        $this->context->expects($this->once())->method('getView')->willReturn($view);

        $this->instance = (new ObjectManager($this))->getObject(
            '\BoostMyShop\Amazon\Controller\Adminhtml\Account\Index',
            [
                'coreRegistry' => $this->coreRegistry,
                'countryFactory' => $this->countryFactory,
                'context' => $this->context
            ]
        );
    }

    public function testExecute(){

        $this->instance->execute();

    }
}