<?php namespace BoostMyShop\Amazon\Test\Unit\Controller\Adminhtml\Account;

/**
 * Class DeleteTest
 *
 * @package   BoostMyShop\Amazon\Test\Unit\Controller\Adminhtml\Account
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class DeleteTest extends \BoostMyShop\Amazon\Test\Unit\Controller\Adminhtml\Account {

    public function getClassName()
    {
        return '\BoostMyShop\Amazon\Controller\Adminhtml\Account\Delete';
    }

    public function testExecute(){

        $accountId = 1;

        $this->request->expects($this->once())->method('getParam')->with('id')->willReturn($accountId);

        $account = $this->getMockBuilder('\BoostMyShop\Amazon\Model\Account')
            ->setMethods(['load', 'delete'])
            ->disableOriginalConstructor()
            ->getMock();

        $account->expects($this->once())->method('load')->with($accountId)->willReturn($account);
        $account->expects($this->once())->method('delete');

        $this->accountFactory->expects($this->once())->method('create')->willReturn($account);

        $this->messageManager->expects($this->once())->method('addSuccess');

        $resultRedirect = $this->getMockBuilder('\Magento\Framework\Controller\Result\Redirect')
            ->setMethods(['setPath'])
            ->disableOriginalConstructor()
            ->getMock();

        $resultRedirect->expects($this->once())->method('setPath')->with('amazon/account/index');

        $this->resultRedirectFactory->expects($this->once())->method('create')->willReturn($resultRedirect);

        $this->instance->execute();

    }

}