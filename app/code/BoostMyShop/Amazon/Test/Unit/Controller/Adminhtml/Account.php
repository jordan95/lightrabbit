<?php namespace BoostMyShop\Amazon\Test\Unit\Controller\Adminhtml;

require_once dirname(__FILE__).'/../../../../../../../functions.php';

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;

/**
 * Class Account
 *
 * @package   BoostMyShop\Amazon\Test\Unit\Controller\Adminhtml
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
abstract class Account extends \PHPUnit_Framework_TestCase {

    /**
     * @var \BoostMyShop\Amazon\Model\AccountFactory
     */
    protected $accountFactory;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry;

    /**
     * @var \Magento\Backend\App\Action\Context
     */
    protected $context;

    /**
     * @var \Magento\Framework\App\Request\Http
     */
    protected $request;

    /**
     * @var \Magento\Framework\Message\Manager
     */
    protected $messageManager;

    /**
     * @var \Magento\Framework\Controller\Result\RedirectFactory
     */
    protected $resultRedirectFactory;

    public function setUp(){

        $this->coreRegistry = $this->getMockBuilder('\Magento\Framework\Registry')
            ->disableOriginalConstructor()
            ->getMock();

        $this->accountFactory = $this->getMockBuilder('\BoostMyShop\Amazon\Model\AccountFactory')
            ->setMethods(['create'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->request = $this->getMockBuilder('\Magento\Framework\App\Request\Http')
            ->setMethods(['getParam'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->messageManager = $this->getMockBuilder('\Magento\Framework\Message\Manager')
            ->setMethods(['addSuccess', 'addError'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->resultRedirectFactory = $this->getMockBuilder('\Magento\Framework\Controller\Result\RedirectFactory')
            ->setMethods(['create'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->context = $this->getMockBuilder('\Magento\Backend\App\Action\Context')
            ->setMethods(['getRequest', 'getMessageManager', 'getResultRedirectFactory'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->context->expects($this->once())->method('getRequest')->willReturn($this->request);
        $this->context->expects($this->once())->method('getMessageManager')->willReturn($this->messageManager);
        $this->context->expects($this->once())->method('getResultRedirectFactory')->willReturn($this->resultRedirectFactory);

        $this->instance = (new ObjectManager($this))->getObject(
            '\BoostMyShop\Amazon\Controller\Adminhtml\Account\Delete',
            [
                'accountFactory' => $this->accountFactory,
                'coreRegistry' => $this->coreRegistry,
                'context' => $this->context
            ]
        );

    }

    abstract function getClassName();

}