<?php namespace BoostMyShop\Amazon\Test\Unit\Controller\Adminhtml\Dashboard;

require_once dirname(__FILE__).'/../../../../../../../../functions.php';

use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;

/**
 * Class IndexTest
 *
 * @package   BoostMyShop\Amazon\Test\Unit\Controller\Adminhtml\Dashboard
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class IndexTest extends \PHPUnit_Framework_TestCase {

    /**
     * @var \BoostMyShop\Amazon\Controller\Adminhtml\Dashboard\Index
     */
    protected $instance;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var \BoostMyShop\Amazon\Model\Account\Country| \PHPUnit_Framework_MockObject_MockObject
     */
    protected $country;

    /**
     * @var \Magento\Framework\Registry| \PHPUnit_Framework_MockObject_MockObject
     */
    protected $registry;

    /**
     * @var \Magento\Backend\App\Action\Context| \PHPUnit_Framework_MockObject_MockObject
     */
    protected $context;

    /**
     * @var \Magento\Framework\App\Request\Http| \PHPUnit_Framework_MockObject_MockObject
     */
    protected $request;

    /**
     * @var \Magento\Framework\App\Response\Http| \PHPUnit_Framework_MockObject_MockObject
     */
    protected $response;

    public function setUp(){

        $objectManager = new ObjectManager($this);

        $this->resultPageFactory = $this->getMockBuilder('\Magento\Framework\View\Result\PageFactory')
            ->setMethods(['create'])
            ->disableOriginalConstructor()
            ->getMock();

        $this->country = $this->getMockBuilder('\BoostMyShop\Amazon\Model\Account\Country')
            ->disableOriginalConstructor()
            ->getMock();

        $this->registry = $this->getMockBuilder('\Magento\Framework\Registry')
            ->disableOriginalConstructor()
            ->getMock();

        $this->request = $this->getMockBuilder('\Magento\Framework\App\Request\Http')
            ->disableOriginalConstructor()
            ->setMethods(['getParam'])
            ->getMock();

        $this->response = $this->getMockBuilder('\Magento\Framework\App\Response\Http')
            ->disableOriginalConstructor()
            ->getMock();

        $this->context = $objectManager->getObject(
            '\Magento\Backend\App\Action\Context',
            [
                'request' => $this->request,
                'response' => $this->response
            ]
        );

        $this->instance = $objectManager->getObject(
            '\BoostMyShop\Amazon\Controller\Adminhtml\Dashboard\Index',
            [
                'resultPageFactory' => $this->resultPageFactory,
                'country' => $this->country,
                'registry' => $this->registry,
                'context' => $this->context
            ]
        );

    }

    public function testExecute(){

        $accountCountryId = 1;

        $resultPage = $this->getMockBuilder('\Magento\Framework\View\Result\Page')
            ->setMethods(['getConfig'])
            ->disableOriginalConstructor()
            ->getMock();

        $title = $this->getMockBuilder('\Magento\Framework\App\Title')
            ->setMethods(['prepend'])
            ->disableOriginalConstructor()
            ->getMock();

        $title->expects($this->once())->method('prepend')->with('Dashboard');

        $config = $this->getMockBuilder('\Magento\Framework\View\Page\Config')
            ->setMethods(['getTitle'])
            ->disableOriginalConstructor()
            ->getMock();

        $config->expects($this->once())->method('getTitle')->willReturn($title);

        $resultPage->expects($this->once())->method('getConfig')->willReturn($config);

        $this->resultPageFactory->expects($this->once())->method('create')->willReturn($resultPage);

        $this->request->expects($this->once())
            ->method('getParam')
            ->with('account_country_id', false)
            ->willReturn($accountCountryId);

        $this->registry->expects($this->once())->method('register')->with('account_country_id', $accountCountryId);

        $this->instance->execute();

    }

}