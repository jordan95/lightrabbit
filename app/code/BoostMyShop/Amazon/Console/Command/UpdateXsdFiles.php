<?php namespace BoostMyShop\Amazon\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class UpdateXsdFiles
 *
 * @package   BoostMyShop\Amazon\Console\Command
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class UpdateXsdFiles extends Command {

    /**
     * @var \BoostMyShop\Amazon\Helper\XsdFactory
     */
    protected $_xsdHelperFactory;

    /**
     * @var \Magento\Framework\App\State
     */
    protected $_state;

    /**
     * UpdateXsdFiles constructor.
     * @param \BoostMyShop\Amazon\Helper\XsdFactory $xsdHelperFactory
     * @param \Magento\Framework\App\State $state
     * @param null $name
     */
    public function __construct(
        \BoostMyShop\Amazon\Helper\XsdFactory $xsdHelperFactory,
        \Magento\Framework\App\State $state,
        $name = null
    ){
        parent::__construct($name);
        $this->_xsdHelperFactory = $xsdHelperFactory;
        $this->_state = $state;
    }

    protected function configure()
    {
        $options = [];

        $this->setName('bms:amazon:update-xsd-files')->setDescription('Update XSD files')->setDefinition($options);
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function execute(InputInterface $input, OutputInterface $output){

        $outputFormatter = $output->getFormatter();
        $outputFormatter->setStyle('detail', new OutputFormatterStyle('blue'));

        $this->_state->setAreaCode('adminhtml');

        $this->_xsdHelperFactory->create()->update();

    }

}