<?php namespace BoostMyShop\Amazon\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CleanBatchHistory
 *
 * @package   BoostMyShop\Amazon\Console\Command
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class CleanBatchHistory extends Command {

    /**
     * @var \Magento\Framework\App\State
     */
    protected $_state;

    /**
     * @var \BoostMyShop\Amazon\Model\ResourceModel\Batch\CollectionFactory
     */
    protected $_batchCollectionFactory;

    /**
     * CleanBatchHistory constructor.
     * @param \BoostMyShop\Amazon\Model\ResourceModel\Batch\CollectionFactory $batchCollectionFactory
     * @param \Magento\Framework\App\State $state
     * @param null $name
     */
    public function __construct(
        \BoostMyShop\Amazon\Model\ResourceModel\Batch\CollectionFactory $batchCollectionFactory,
        \Magento\Framework\App\State $state,
        $name = null
    ){
        parent::__construct($name);
        $this->_batchCollectionFactory = $batchCollectionFactory;
        $this->_state = $state;
    }

    protected function configure()
    {
        $options = [
            new InputOption(
                'all',
                null,
                InputOption::VALUE_OPTIONAL,
                'All'
            )
        ];

        $this->setName('bms:amazon:clean-batch-history')->setDescription('Clean batch history')->setDefinition($options);
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function execute(InputInterface $input, OutputInterface $output){

        $outputFormatter = $output->getFormatter();
        $outputFormatter->setStyle('detail', new OutputFormatterStyle('blue'));

        $output->writeln('<detail>Start clean batch history</detail>');

        $this->_state->setAreaCode('adminhtml');

        if($input->getOption('all')){

            $cptItemsDeleted = $this->_batchCollectionFactory->create()->delete();

        }else{

            $cptItemsDeleted = $this->_batchCollectionFactory->create()->cleanHistory();

        }

        $output->writeln('<info>'.$cptItemsDeleted.' batch deleted</info>');

        $output->writeln('<detail>End clean batch history</detail>');

    }

}