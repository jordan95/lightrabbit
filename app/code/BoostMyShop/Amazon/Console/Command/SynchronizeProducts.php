<?php namespace BoostMyShop\Amazon\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class SynchronizeProducts
 *
 * @package   BoostMyShop\Amazon\Console\Command
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class SynchronizeProducts extends Command {

    /**
     * @var \BoostMyShop\Amazon\Model\ResourceModel\Account\CountryFactory
     */
    protected $_countryFactory;

    /**
     * @var \Magento\Framework\App\State
     */
    protected $_state;

    /**
     * ImportOrder constructor.
     * @param \BoostMyShop\Amazon\Model\Account\CountryFactory $countryFactory
     * @param \Magento\Framework\App\State $state
     * @param null $name
     */
    public function __construct(
        \BoostMyShop\Amazon\Model\Account\CountryFactory $countryFactory,
        \Magento\Framework\App\State $state,
        $name = null
    ){
        parent::__construct($name);
        $this->_countryFactory = $countryFactory;
        $this->_state = $state;
    }

    protected function configure()
    {
        $options = [
            new InputOption(
                'countryId',
                null,
                InputOption::VALUE_REQUIRED,
                'Country Id'
            )
        ];

        $this->setName('bms:amazon:synchronize-products')->setDescription('Synchronize products')->setDefinition($options);
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function execute(InputInterface $input, OutputInterface $output){

        $outputFormatter = $output->getFormatter();
        $outputFormatter->setStyle('detail', new OutputFormatterStyle('blue'));

        $output->writeln('<detail>Start product synchronization</detail>');

        $this->_state->setAreaCode('adminhtml');

        $countryId = $input->getOption('countryId');

        $country = $this->_countryFactory->create()->load($countryId);

        if($country->getId()){

            foreach(explode(',',$country->importProducts()) as $info)
                $output->writeln('<info>'.trim($info).'</info>');

        }else{

            $output->writeln('<error>Not able to load country</error>');

        }

        $output->writeln('<detail>End product synchronization</detail>');

    }

}