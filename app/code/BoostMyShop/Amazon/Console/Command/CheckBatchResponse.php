<?php namespace BoostMyShop\Amazon\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CheckBatchResponse
 *
 * @package   BoostMyShop\Amazon\Console\Command
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class CheckBatchResponse extends Command {

    /**
     * @var \Magento\Framework\App\State
     */
    protected $_state;

    /**
     * @var \BoostMyShop\Amazon\Model\Account\CountryFactory
     */
    protected $_countryFactory;

    /**
     * CheckBatchResponse constructor.
     * @param \BoostMyShop\Amazon\Model\Account\CountryFactory $countryFactory
     * @param \Magento\Framework\App\State $state
     * @param null $name
     */
    public function __construct(
        \BoostMyShop\Amazon\Model\Account\CountryFactory $countryFactory,
        \Magento\Framework\App\State $state,
        $name = null
    ){
        parent::__construct($name);
        $this->_countryFactory = $countryFactory;
        $this->_state = $state;
    }

    protected function configure()
    {
        $options = [
            new InputOption(
                'countryId',
                null,
                InputOption::VALUE_REQUIRED,
                'Country Id'
            )
        ];

        $this->setName('bms:amazon:check-batch-response')->setDescription('Check Batch Response')->setDefinition($options);
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function execute(InputInterface $input, OutputInterface $output){

        $outputFormatter = $output->getFormatter();
        $outputFormatter->setStyle('detail', new OutputFormatterStyle('blue'));

        $output->writeln('<detail>Start check batch response</detail>');

        $this->_state->setAreaCode('adminhtml');

        $country = $this->_countryFactory->create()->load($input->getOption('countryId'));

        if($country->getId()){

            $updated = $country->checkResponseForProcessingBatches();
            foreach($updated as $item){

                $output->writeln('<info>'.$item.' updated</info>');

            }

        }

        $output->writeln('<detail>End check batch repsonse</detail>');

    }

}