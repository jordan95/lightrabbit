<?php namespace BoostMyShop\Amazon\Helper;

/**
 * Class Order
 *
 * @package   BoostMyShop\Amazon\Helper
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
abstract class Order extends \BoostMyShop\Amazon\Helper\Feed {

    /**
     * @var \BoostMyShop\Amazon\Helper\Mws\FeedsFactory
     */
    protected $_mwsFeedClientFactory;

    /**
     * @var \BoostMyShop\Amazon\Helper\Mws\Orders
     */
    protected $_mwsOrderClientFactory;

    /**
     * Order constructor.
     * @param \BoostMyShop\Amazon\Helper\Mws\OrdersFactory $mwsOrderClientFactory
     * @param \BoostMyShop\Amazon\Helper\Mws\FeedsFactory $mwsFeedClientFactory
     * @param \BoostMyShop\Amazon\Helper\Timer $timer
     * @param \BoostMyShop\Amazon\Model\BatchFactory $batchFactory
     */
    public function __construct(
        \BoostMyShop\Amazon\Helper\Mws\OrdersFactory $mwsOrderClientFactory,
        \BoostMyShop\Amazon\Helper\Mws\FeedsFactory $mwsFeedClientFactory,
        \BoostMyShop\Amazon\Helper\Timer $timer,
        \BoostMyShop\Amazon\Model\BatchFactory $batchFactory
    ){
        parent::__construct($mwsFeedClientFactory, $timer, $batchFactory);
        $this->_mwsOrderClientFactory = $mwsOrderClientFactory;
        $this->_mwsFeedClientFactory = $mwsFeedClientFactory;
    }

    /**
     * @param array $createdDates
     * @param array $updatedDates
     * @param array $statuses
     * @param array $fulfillmentChannels
     * @param null $sellerOrderId
     * @param null $buyerEmail
     * @param array $paymentMethods
     * @param array $tfmShipmentStatuses
     * @param int $maxResultsPerPage
     * @return mixed
     * @throws \BoostMyShop\Amazon\Exception\ErrorResponseException
     * @throws \Exception
     */
    protected function _listOrders(
        $createdDates = array(),
        $updatedDates = array(),
        $statuses = array(),
        $fulfillmentChannels = array(),
        $sellerOrderId = null,
        $buyerEmail = null,
        $paymentMethods = array(),
        $tfmShipmentStatuses = array(),
        $maxResultsPerPage = 50
    ){

        /* @see \BoostMyShop\Amazon\Lib\MWS\Order::listOrders */
        $response = $this->_getMwsOrderClient()->listOrders(
            $createdDates,
            $updatedDates,
            $statuses,
            $fulfillmentChannels,
            $sellerOrderId,
            $buyerEmail,
            $paymentMethods,
            $tfmShipmentStatuses,
            $maxResultsPerPage
        );

        $xml = simplexml_load_string($response->getBody());
        $object = json_decode(json_encode($xml));
        $orders = (isset($object->ListOrdersResult->Orders->Order)) ? $object->ListOrdersResult->Orders->Order : [];
        return (is_object($orders)) ? [$orders] : $orders;
    }

    /**
     * @param int $amazonOrderId
     * @return array $orderItems
     * @throws \BoostMyShop\Amazon\Exception\ErrorResponseException
     * @throws \Exception
     */
    protected function _listOrderItems($amazonOrderId)
    {

        $orderItems = array();
        $nextToken = null;

        do {

            if ($nextToken === null) {
                /* @see \BoostMyShop\Amazon\Lib\MWS\Order::listOrderItems */
                $response = $this->_getMwsOrderClient()->listOrderItems($amazonOrderId);
            } else {
                /* @see \BoostMyShop\Amazon\Lib\MWS\Order::listOrderItemsByNextToken */
                $response = $this->_getMwsOrderClient()->listOrderItemsByNextToken($nextToken);
            }

            if ($response !== false) {

                $xml = simplexml_load_string($response->getBody());
                $items = json_decode(json_encode($xml))->ListOrderItemsResult->OrderItems->OrderItem;
                if(is_array($items))
                    $orderItems = array_merge($orderItems, $items);
                else
                    $orderItems[] = $items;

            }

        } while ($nextToken !== null && $response !== false);

        return $orderItems;

    }

    /**
     * @return \BoostMyShop\Amazon\Lib\MWS\Client
     * @throws \Exception
     */
    protected function _getMwsOrderClient()
    {

        return $this->_mwsOrderClientFactory->create()->setBatch($this->_batch)->reset()
            ->setMerchantId($this->getCountry()->getAccount()->getMerchantId())
            ->setAccessKeyId($this->getCountry()->getAccount()->getAccessKeyId())
            ->setSecretKey($this->getCountry()->getAccount()->getSecretKey())
            ->setBaseUrl($this->getCountry()->getendpoint())
            ->setMarketplaceId($this->getCountry()->getmarketplace_id());

    }

}