<?php namespace BoostMyShop\Amazon\Helper;

/**
 * Class Feed
 *
 * @package   BoostMyShop\Amazon\Helper
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
abstract class Feed extends Base {

    /**
     * @var \BoostMyShop\Amazon\Helper\Mws\FeedsFactory
     */
    protected $_mwsFeedClientFactory;

    /**
     * @var array
     */
    protected $_batches = [];

    /**
     * Feed constructor.
     * @param \BoostMyShop\Amazon\Helper\Mws\FeedsFactory $mwsFeedClientFactory
     * @param \BoostMyShop\Amazon\Helper\Timer $timer
     * @param \BoostMyShop\Amazon\Model\BatchFactory $batchFactory
     */
    public function __construct(
        \BoostMyShop\Amazon\Helper\Mws\FeedsFactory $mwsFeedClientFactory,
        \BoostMyShop\Amazon\Helper\Timer $timer,
        \BoostMyShop\Amazon\Model\BatchFactory $batchFactory
    ){
        parent::__construct($timer, $batchFactory);
        $this->_mwsFeedClientFactory = $mwsFeedClientFactory;
    }

    /**
     * @return array
     */
    public function getBatches(){

        return $this->_batches;

    }

    /**
     * @return \BoostMyShop\Amazon\Lib\MWS\Client
     */
    protected function _getMwsFeedClient()
    {

        return $this->_mwsFeedClientFactory->create()->setBatch($this->_batch)->reset()
            ->setMerchantId($this->getCountry()->getAccount()->getMerchantId())
            ->setAccessKeyId($this->getCountry()->getAccount()->getAccessKeyId())
            ->setSecretKey($this->getCountry()->getAccount()->getSecretKey())
            ->setBaseUrl($this->getCountry()->getendpoint())
            ->setMarketplaceId($this->getCountry()->getmarketplace_id());

    }

    /**
     * @param string $type
     * @param \BoostMyShop\Amazon\Lib\MWS\Xml\Feed|null $feed
     * @param string $summary
     * @param bool $logWithoutSend choose whether or not the feed should actually be sent to Amazon.
     *        Helpful for debug purpose or just log an empty request
     */
    protected function _sendFeed($type, $feed, $summary, $logWithoutSend = false){

        if(!$this->_batch)
            $this->_openBatch($type);

        if(!$logWithoutSend) {
            $mwsFeedClient = $this->_getMwsFeedClient()
                ->setContent($feed->closeStream());
            $mwsFeedClient->submitFeed($feed->getType());
            $batchId = $mwsFeedClient->getFeedSubmissionId();

            $summary .= 'duration ' . $this->_timer->stop()->getDuration() . ' s';

            $this->_batch->updateBatchId($batchId)
                ->updateAsProcessing();
        } else {
            $this->_closeBatch();
        }

        $this->_batch->setSummary($summary);
        $this->_batch->save();

        $this->_batches[] = $this->_batch;

    }

}