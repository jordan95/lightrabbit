<?php namespace BoostMyShop\Amazon\Helper;

/**
 * Class Product
 *
 * @package   BoostMyShop\Amazon\Helper
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Product {

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $_productCollectionFactory;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    /**
     * Product constructor.
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     */
    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory
    ){
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_productFactory = $productFactory;
    }

    /**
     * @param string $sellerSku
     * @param string $reference
     * @return \Magento\Catalog\Model\Product
     */
    public function getProductFromSellerSku($sellerSku, $reference){

        $item = $this->_productCollectionFactory->create()
            ->addAttributeToFilter($reference, $sellerSku)
            ->getFirstItem();

        return $item;

    }

    /**
     * @param string $sellerSku
     * @param string $reference
     * @return \Magento\Catalog\Model\Product
     */
    public function loadProductFromSellerSku($sellerSku, $reference){

        $item = $this->getProductFromSellerSku($sellerSku, $reference);

        /* reload product because tax calculation for $item doesn't work */
        return $this->_productFactory->create()->load($item->getId());

    }

    /**
     * @param string $country
     * @param string $asin
     * @return string
     */
    public function getAmazonProductUri($country, $asin)
    {
        switch (strtolower($country)) {
            case 'cn':
                $country = 'com.cn';
                break;
            case 'jp':
                $country = 'co.jp';
                break;
            case 'br':
                $country = 'com.br';
                break;
            case 'au':
                $country = 'com.au';
                break;
            case 'mx':
                $country = 'com.mx';
                break;
            case 'uk':
            case 'gb':
                $country = 'co.uk';
                break;
            case 'us':
                $country = 'com';
                break;
        }

        return 'http://www.amazon.'.$country.'/gp/product/'.$asin;
    }

}