<?php namespace BoostMyShop\Amazon\Helper;

/**
 * Class Timer
 *
 * @package   BoostMyShop\Amazon\Helper
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Timer {

    /**
     * @var float
     */
    protected $_start;

    /**
     * @var float
     */
    protected $_stop;

    /**
     * @var float
     */
    protected $_duration;

    /**
     * @return $this
     */
    public function start(){
        $this->_start = microtime(true);
        return $this;
    }

    /**
     * @return $this
     */
    public function stop(){
        $this->_stop = microtime(true);
        $this->_duration = round($this->_stop - $this->_start, 2);
        return $this;
    }

    /**
     * @return float
     */
    public function getDuration(){
        return $this->_duration;
    }

}