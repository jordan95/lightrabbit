<?php namespace BoostMyShop\Amazon\Helper;

/**
 * Class Barcode
 *
 * @package   BoostMyShop\Amazon\Helper
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Barcode {

    const TYPE_UPC = 'UPC';
    const TYPE_EAN = 'EAN';
    const TYPE_ISBN = 'ISBN';

    /**
     * @param $code
     * @return null|string
     */
    public function getType($code)
    {
        switch (strlen($code)) {

            case '12':
                $type = self::TYPE_UPC;
                break;
            case '13':
            case '14':
            case '8':
                $type = self::TYPE_EAN;
                break;
            default:
                $type = null;
                break;

        }

        return $type;
    }

}