<?php namespace BoostMyShop\Amazon\Helper;

/**
 * Class Cache
 *
 * @package   BoostMyShop\Amazon\Helper
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Cache
{

    /**
     * @var \Magento\Framework\Filesystem\DirectoryList
     */
    protected $_directoryList;

    /**
     * Cache constructor.
     * @param \Magento\Framework\Filesystem\DirectoryList $directoryList
     */
    public function __construct(
        \Magento\Framework\Filesystem\DirectoryList $directoryList
    ){
        $this->_directoryList = $directoryList;
    }

    /**
     * @return string
     */
    public function getBaseDirname()
    {
        return $this->_directoryList->getPath(\Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR) . '/boostmyshop/amazon/cache/';
    }

    /**
     * @param string $key
     * @param int $ttl
     * @return bool|string
     */
    public function load($key, $ttl = 7200)
    {

        $content = false;

        $filename = $this->getBaseDirname() . str_replace('_', '/', $key) . '.cache';
        if (file_exists($filename)) {
            if ($ttl < 0 || filemtime($filename) > time() - $ttl) {
                $content = file_get_contents($filename);
            }
        }

        return $content;

    }

    /**
     * @param string $key
     * @param string $content
     */
    public function add($key, $content)
    {

        $tmp = explode('_', $key);
        $name = array_pop($tmp);
        $dir = (count($tmp) > 0) ? $this->getBaseDirname() . implode('/', $tmp) : $this->getBaseDirname();

        if (!empty($dir)) {
            if (!file_exists($dir)) {
                mkdir($dir, 0755, true);
            }

            file_put_contents($dir . '/' . $name . '.cache', $content);
        }

    }

    /**
     * @param string $key
     */
    public function flush($key)
    {
        $filename = str_replace('_', '/', $this->getBaseDirname() . $key) . '.cache';
        if (file_exists($filename)) {
            unlink($filename);
        }
    }

    /**
     * @param string $dir
     */
    protected function _flushDir($dir){

        if (file_exists($dir)) {
            $handle = opendir($dir);
            while ($file = readdir($handle)) {
                if (!in_array($file, array('.','..'))) {
                    if (is_dir($dir . '/' . $file)) {
                        $this->_flushDir($dir . '/' . $file);
                    } else {
                        unlink($dir . '/' . $file);
                    }
                }
            }

            if (count(scandir($dir)) == 2) {
                rmdir($dir);
            }
        }

    }

    public function flushAll()
    {
        $this->_flushDir($this->getBaseDirname());
    }

}