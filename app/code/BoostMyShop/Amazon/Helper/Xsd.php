<?php namespace BoostMyShop\Amazon\Helper;

/**
 * Class Xsd
 *
 * @package   BoostMyShop\Amazon\Helper
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Xsd
{

    const url = 'https://images-na.ssl-images-amazon.com/images/G/01/rainier/help/xsd/release_1_9/';

    /**
     * @var \Magento\Framework\Filesystem\DirectoryList
     */
    protected $_directoryList;

    /**
     * Logger constructor.
     * @param \Magento\Framework\Filesystem\DirectoryList $directoryList
     */
    public function __construct(
        \Magento\Framework\Filesystem\DirectoryList $directoryList
    ){
        $this->_directoryList = $directoryList;
    }

    /**
     * @return array
     */
    public function getRootFiles(){

        return ['Product.xsd', 'amzn-envelope.xsd'];

    }

    public function update()
    {

        foreach ($this->getRootFiles() as $filename)
            $this->updateFile($filename);

    }

    /**
     * @param string $filename
     */
    public function updateFile($filename)
    {

        $this->_parse($this->_save($filename));

    }

    /**
     * @param string $content
     */
    protected function _parse($content)
    {

        $xml = new \DomDocument();
        $xml->loadXML($content);

        foreach ($xml->getElementsByTagName('include') as $include) {

            $filename = $include->getAttribute('schemaLocation');
            $this->_save($filename);

        }

    }

    /**
     * @param string $filename
     * @return string $content
     */
    protected function _save($filename)
    {
        $dirname = $this->getXsdDirectory();
        if(!file_exists($dirname)){
            mkdir($dirname, 0777, true);
        }
        $content = file_get_contents(self::url . $filename);
        file_put_contents($dirname . '/' . $filename, $content);
        return $content;
    }

    /**
     * @return string
     */
    public function getXsdDirectory(){

        return $this->_directoryList->getPath(\Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR).'/boostmyshop/amazon/xsd';

    }

}