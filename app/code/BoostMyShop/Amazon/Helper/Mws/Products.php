<?php namespace BoostMyShop\Amazon\Helper\Mws;

/**
 * Class Products
 *
 * @method \Zend\Http\Response listMatchingProducts(string $query, string $queryContextId)
 * @method \Zend\Http\Response getMatchingProduct(array $asinList)
 * @method \Zend\Http\Response getMatchingProductForId(string $idType, array $idList)
 * @method \Zend\Http\Response getCompetitivePricingForSku(array $sellerSkuList)
 * @method \Zend\Http\Response getCompetitivePricingForAsin(array $asinList)
 * @method \Zend\Http\Response getLowestPricedOffersForSku(string $sellerSku, string $itemCondition)
 * @method \Zend\Http\Response getLowestPricedOffersForAsin(string $asin, string $itemCondition)
 * @method \Zend\Http\Response getLowestOfferListingsForSku(array $sellerSkuList)
 * @method \Zend\Http\Response getLowestOfferListingsForAsin(array $asinList)
 * @method \Zend\Http\Response getMyPriceForSku(array $sellerSkuList)
 * @method \Zend\Http\Response getMyPriceForAsin(array $asinList)
 * @method \Zend\Http\Response getProductCategoriesForSku(string $sellerSku)
 * @method \Zend\Http\Response getProductCategoriesForAsin(string $asin)
 *
 *
 * @package   BoostMyShop\Amazon\Helper\Mws
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Products extends Base {

    /**
     * Products constructor.
     * @param \BoostMyShop\Amazon\Lib\MWS\Products $mwsProductClient
     * @param \BoostMyShop\Amazon\Helper\Logger $logger
     */
    public function __construct(
        \BoostMyShop\Amazon\Lib\MWS\Products $mwsProductClient,
        \BoostMyShop\Amazon\Helper\Logger $logger
    ){
        parent::__construct($logger);
        $this->_mwsClient = $mwsProductClient;
    }

}