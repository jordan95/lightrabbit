<?php namespace BoostMyShop\Amazon\Helper\Mws;

/**
 * Class Sellers
 *
 * @method \Zend\Http\Response listMarketplaceParticipations()
 * @method \Zend\Http\Response listMarketplaceParticipationsByNextToken(string $nextToken)
 *
 * @package   BoostMyShop\Amazon\Helper\Mws
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Sellers extends Base {

    /**
     * Sellers constructor.
     * @param \BoostMyShop\Amazon\Lib\MWS\Sellers $mwsSellerClient
     * @param \BoostMyShop\Amazon\Helper\Logger $logger
     */
    public function __construct(
        \BoostMyShop\Amazon\Lib\MWS\Sellers $mwsSellerClient,
        \BoostMyShop\Amazon\Helper\Logger $logger
    ){
        parent::__construct($logger);
        $this->_mwsClient = $mwsSellerClient;
    }

}