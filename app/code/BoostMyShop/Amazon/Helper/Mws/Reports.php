<?php namespace BoostMyShop\Amazon\Helper\Mws;

/**
 * Class Reports
 *
 * @method \Zend\Http\Response getReport(string $reportId)
 * @method \Zend\Http\Response getReportCount(array $reportTypeList, string $acknowledged, string $availableFromDate, string $availableToDate)
 * @method \Zend\Http\Response getReportList(string $maxCount, array $reportTypeList, string $acknowledged, string $availableFromDate, string $availableToDate, array $reportRequestIdList)
 * @method \Zend\Http\Response getReportListByNextToken(string $token)
 * @method \Zend\Http\Response getReportRequestCount(string $requestedFromDate, string $requestedToDate, array $reportTypeList, array $reportProcessingStatusList)
 * @method \Zend\Http\Response getReportRequestList(int $maxCount, string $requestedFromDate, string $requestedToDate, array $reportRequestIdList, array $reportTypeList, array $reportProcessingStatusList)
 * @method \Zend\Http\Response getReportRequestListByNextToken(string $nextToken)
 * @method \Zend\Http\Response cancelReportRequests(string $requestedFromDate, string $requestedToDate, array $reportRequestIdList, array $reportTypeList, array $reportProcessingStatusList)
 * @method \Zend\Http\Response requestReport(string $reportType, array $marketplaceIdList, string $startDate, string $endDate, string $reportOptions)
 * @method \Zend\Http\Response manageReportSchedule(string $reportType, string $schedule, string $scheduleDate)
 * @method \Zend\Http\Response getReportScheduleList(array $reportTypeList)
 * @method \Zend\Http\Response getReportScheduleCount(array $reportTypeList)
 * @method \Zend\Http\Response updateReportAcknowledgements(array $reportTypeList, string $acknowledged)
 * @method \Zend\Http\Response getReportScheduleListByNextToken(string $nextToken)
 *
 * @package   BoostMyShop\Amazon\Helper\Mws
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Reports extends Base {

    /**
     * Reports constructor.
     * @param \BoostMyShop\Amazon\Lib\MWS\Reports $mwsReportClient
     * @param \BoostMyShop\Amazon\Helper\Logger $logger
     */
    public function __construct(
        \BoostMyShop\Amazon\Lib\MWS\Reports $mwsReportClient,
        \BoostMyShop\Amazon\Helper\Logger $logger
    ){
        parent::__construct($logger);
        $this->_mwsClient = $mwsReportClient;
    }

}