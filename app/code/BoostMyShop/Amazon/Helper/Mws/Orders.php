<?php namespace BoostMyShop\Amazon\Helper\Mws;

/**
 * Class Orders
 *
 * @method \Zend\Http\Response listOrders(array $createdDates = [], array $updatedDates = [], array $statuses = [], array $fulfillmentChannels = '', string $sellerOrderId = null, string $buyerEmail = null, array $paymentMethods = [], array $tfmShipmentStatuses = [], int $maxResultsPerPage = 50)
 * @method \Zend\Http\Response listOrdersByNextToken(string $nextToken)
 * @method \Zend\Http\Response getOrder(array $amazonOrderIds)
 * @method \Zend\Http\Response listOrderItems(string $amazonOrderId)
 * @method \Zend\Http\Response listOrderItemsByNextToken(string $nextToken)
 *
 * @package   BoostMyShop\Amazon\Helper\Mws
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Orders extends Base {

    /**
     * Orders constructor.
     * @param \BoostMyShop\Amazon\Lib\MWS\Orders $mwsOrderClient
     * @param \BoostMyShop\Amazon\Helper\Logger $logger
     */
    public function __construct(
        \BoostMyShop\Amazon\Lib\MWS\Orders $mwsOrderClient,
        \BoostMyShop\Amazon\Helper\Logger $logger
    ){
        parent::__construct($logger);
        $this->_mwsClient = $mwsOrderClient;
    }

}