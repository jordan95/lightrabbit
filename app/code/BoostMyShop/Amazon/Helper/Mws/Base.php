<?php namespace BoostMyShop\Amazon\Helper\Mws;

use BoostMyShop\Amazon\Exception\ErrorResponseException;

/**
 * Class Base
 *
 * @package   BoostMyShop\Amazon\Helper\Mws
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
abstract class Base
{

    /**
     * @var \BoostMyShop\Amazon\Helper\Logger
     */
    protected $_logger;

    /**
     * @var \BoostMyShop\Amazon\Model\Batch
     */
    protected $_batch;

    /**
     * @var \BoostMyShop\Amazon\Lib\MWS\Client
     */
    protected $_mwsClient;

    /**
     * @var bool
     */
    protected $_logFiles = true;

    /**
     * @return $this
     */
    public function disableLogFiles()
    {
        $this->_logFiles = false;
        return $this;
    }

    /**
     * @param \BoostMyShop\Amazon\Model\Batch $batch
     * @return $this
     */
    public function setBatch(\BoostMyShop\Amazon\Model\Batch $batch)
    {
        $this->_batch = $batch;
        return $this;
    }

    /**
     * @return \BoostMyShop\Amazon\Model\Batch
     */
    public function getBatch()
    {
        return $this->_batch;
    }

    /**
     * Base constructor.
     * @param \BoostMyShop\Amazon\Helper\Logger $logger
     */
    public function __construct(
        \BoostMyShop\Amazon\Helper\Logger $logger
    ){
        $this->_logger = $logger;
    }

    /**
     * @param $merchantId
     * @return $this
     */
    public function setMerchantId($merchantId)
    {
        $this->_mwsClient->setMerchantId($merchantId);
        return $this;

    }

    /**
     * @param $accessKeyId
     * @return $this
     */
    public function setAccessKeyId($accessKeyId)
    {
        $this->_mwsClient->setAccessKeyId($accessKeyId);
        return $this;
    }

    /**
     * @param $secretKey
     * @return $this
     */
    public function setSecretKey($secretKey)
    {
        $this->_mwsClient->setSecretKey($secretKey);
        return $this;
    }

    /**
     * @param $baseUrl
     * @return $this
     */
    public function setBaseUrl($baseUrl)
    {
        $this->_mwsClient->setBaseUrl($baseUrl);
        return $this;
    }

    /**
     * @param $marketplaceId
     * @return $this
     */
    public function setMarketplaceId($marketplaceId)
    {
        $this->_mwsClient->setMarketplaceId($marketplaceId);
        return $this;
    }

    /**
     * @return $this
     */
    public function reset()
    {
        $this->_mwsClient->reset();
        return $this;
    }

    /**
     * @param $content
     * @return $this
     */
    public function setContent($content)
    {
        $this->_mwsClient->setContent($content);
        return $this;
    }

    /**
     * @param $name
     * @param $arguments
     * @return mixed
     * @throws \BoostMyShop\Amazon\Exception\ErrorResponseException
     * @throws \Exception
     */
    public function __call($name, $arguments)
    {

        if (!is_object($this->_mwsClient) || !($this->_mwsClient instanceof \BoostMyShop\Amazon\Lib\MWS\Client))
            throw new \Exception('Not a valid instance');

        if (!method_exists($this->_mwsClient, $name))
            throw new \Exception('Action ' . $name . ' does not exists for object ' . get_class($this->_mwsClient));

        $response = call_user_func_array(array($this->_mwsClient, $name), $arguments);

        if($response instanceof \Zend\Http\Response){

            if ($this->getBatch() && $this->_logFiles === true) {

                $lastRequest = $this->_mwsClient->getLastRequest();
                $filename = $this->getBatch()->getOperationType() . '/' . $this->getBatch()->getBatchId() . '/' . $name . '/' . md5(json_encode($arguments));

                $fileContent = $lastRequest . "\n\n";
                //$fileContent .= implode($response->getHeaders(),"\n")."\n\n";
                $fileContent .= $response->getBody();

                $this->_logger->appendFile($filename . '/' . $this->_logger->getFilenameFromOperationParams($name, $arguments) . '.xml', $fileContent);

            }

            if ($response->getStatusCode() != 200) {

                $this->_parseResponse($response);

            }

        }

        return $response;

    }

    /**
     * @param $response
     * @throws \BoostMyShop\Amazon\Exception\ErrorResponseException
     */
    protected function _parseResponse($response)
    {

        $errorType = '';
        $errorCode = '';
        $errorMessage = '';
        $requestId = '';

        $domDocument = new \DOMDocument();
        $domDocument->loadXML($response->getBody());

        if ($domDocument->getElementsByTagName('Type') && $domDocument->getElementsByTagName('Type')->item(0))
            $errorType = $domDocument->getElementsByTagName('Type')->item(0)->nodeValue;

        if ($domDocument->getElementsByTagName('Code') && $domDocument->getElementsByTagName('Code')->item(0))
            $errorCode = $domDocument->getElementsByTagName('Code')->item(0)->nodeValue;

        if ($domDocument->getElementsByTagName('Message') && $domDocument->getElementsByTagName('Message')->item(0))
            $errorMessage = $domDocument->getElementsByTagName('Message')->item(0)->nodeValue;

        if ($domDocument->getElementsByTagName('RequestId') && $domDocument->getElementsByTagName('RequestId')->item(0))
            $requestId = $domDocument->getElementsByTagName('RequestId')->item(0)->nodeValue;

        if ($domDocument->getElementsByTagName('RequestID') && $domDocument->getElementsByTagName('RequestID')->item(0))
            $requestId = $domDocument->getElementsByTagName('RequestID')->item(0)->nodeValue;

        throw new ErrorResponseException($errorType, $errorCode, $errorMessage, $requestId);

    }

}