<?php namespace BoostMyShop\Amazon\Helper\Mws;

/**
 * Class Feeds
 *
 * @method \Zend\Http\Response cancelFeedSubmissions(array $feedSubmissionIdList, array $feedTypeList, string $submittedFromDate, string $submittedToDate)
 * @method \Zend\Http\Response getFeedSubmissionList(string $submittedFromDate, string $submittedToDate, array $feedTypeList, array $feedProcessingStatusList, int $maxCount, array $feedSubmissionIdList)
 * @method \Zend\Http\Response getFeedSubmissionListByNextToken(string $nextToken)
 * @method \Zend\Http\Response getFeedSubmissionCount(string $submittedFromDate, string $submittedToDate, array $feedTypeList, array $feedProcessingStatusList)
 * @method \Zend\Http\Response getFeedSubmissionResult(int $feedSubmissionId)
 * @method \Zend\Http\Response submitFeed(string $type)
 * @method \BoostMyShop\Amazon\Lib\MWS\Feeds setContent(string $content)
 *
 * @package   BoostMyShop\Amazon\Helper\Mws
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Feeds extends Base {

    /**
     * Feeds constructor.
     * @param \BoostMyShop\Amazon\Lib\MWS\Feeds $mwsFeedClient
     * @param \BoostMyShop\Amazon\Helper\Logger $logger
     */
    public function __construct(
        \BoostMyShop\Amazon\Lib\MWS\Feeds $mwsFeedClient,
        \BoostMyShop\Amazon\Helper\Logger $logger
    ){
        parent::__construct($logger);
        $this->_mwsClient = $mwsFeedClient;
    }

}