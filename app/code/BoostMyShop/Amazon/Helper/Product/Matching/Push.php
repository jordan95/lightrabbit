<?php namespace BoostMyShop\Amazon\Helper\Product\Matching;

/**
 * Class Push
 *
 * @package   BoostMyShop\Amazon\Helper\Product\Matching
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Push extends \BoostMyShop\Amazon\Helper\Feed {

    /**
     * @var \BoostMyShop\Amazon\Model\ResourceModel\Catalog\Product\CollectionFactory
     */
    protected $_productCollectionFactory;

    /**
     * @var \BoostMyShop\Amazon\Lib\MWS\Xml\MatchingFactory
     */
    protected $_matchingFeedFactory;

    /**
     * @var \BoostMyShop\Amazon\Model\ProductFactory
     */
    protected $_productFactory;

    /**
     * Push constructor.
     * @param \BoostMyShop\Amazon\Model\ProductFactory $productFactory
     * @param \BoostMyShop\Amazon\Lib\MWS\Xml\MatchingFactory $matchingFeedFactory
     * @param \BoostMyShop\Amazon\Model\ResourceModel\Catalog\Product\CollectionFactory $productCollectionFactory
     * @param \BoostMyShop\Amazon\Helper\Mws\FeedsFactory $mwsFeedClientFactory
     * @param \BoostMyShop\Amazon\Helper\Timer $timer
     * @param \BoostMyShop\Amazon\Model\BatchFactory $batchFactory
     */
    public function __construct(
        \BoostMyShop\Amazon\Model\ProductFactory $productFactory,
        \BoostMyShop\Amazon\Lib\MWS\Xml\MatchingFactory $matchingFeedFactory,
        \BoostMyShop\Amazon\Model\ResourceModel\Catalog\Product\CollectionFactory $productCollectionFactory,
        \BoostMyShop\Amazon\Helper\Mws\FeedsFactory $mwsFeedClientFactory,
        \BoostMyShop\Amazon\Helper\Timer $timer,
        \BoostMyShop\Amazon\Model\BatchFactory $batchFactory
    ){
        parent::__construct($mwsFeedClientFactory, $timer, $batchFactory);
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_matchingFeedFactory = $matchingFeedFactory;
        $this->_productFactory = $productFactory;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function execute(){

        $summary = '';
        $submittedProducts = [];
        $productsToUpdate = [];
        $this->_timer->start();

        $collection = $this->_productCollectionFactory->create();
        $collection->joinAmazonProducts($this->getCountry())
            ->addFieldToFilter('amazon_status', \BoostMyShop\Amazon\Model\Product::STATUS_TO_PUSH);

        if($collection->getSize() > 0) {

            $matchingFeed = $this->_matchingFeedFactory->create();
            $matchingFeed->setMerchantIdentifier($this->getCountry()->getAccount()->getMerchantId())
                ->openStream();

            foreach($collection as $item){

                $matchingFeed->happenProduct($item->getData($this->getCountry()->getOption('reference')), $item->getAsin());
                $submittedProducts[] = $item->getAmazonProductId();
                $productsToUpdate[] = $item->getId();

            }

            $summary = count($submittedProducts).' product(s) submitted, ';

            $this->_sendFeed(\BoostMyShop\Amazon\Model\Batch::OPERATION_TYPE_MATCHING, $matchingFeed, $summary);

            foreach($submittedProducts as $amazonProductId){

                $item = $this->_productFactory->create()->load($amazonProductId);
                $item->setStatus(\BoostMyShop\Amazon\Model\Product::STATUS_PENDING)
                    ->save();

            }


        }
        return $summary;

    }

}