<?php namespace BoostMyShop\Amazon\Helper\Product\Matching;

/**
 * Class Match
 *
 * @package   BoostMyShop\Amazon\Helper\Product\Matching
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Match extends \BoostMyShop\Amazon\Helper\Base {

    /**
     * @var \BoostMyShop\Amazon\Model\ResourceModel\Catalog\Product\Matching\CollectionFactory
     */
    protected $_matchingCollectionFactory;

    /**
     * @var \BoostMyShop\Amazon\Helper\Barcode
     */
    protected $_barcodeHelper;

    /**
     * @var \BoostMyShop\Amazon\Model\ProductFactory
     */
    protected $_productFactory;

    /**
     * @var \BoostMyShop\Amazon\Helper\Mws\ProductsFactory
     */
    protected $_mwsProductClientFactory;

    /**
     * Match constructor.
     * @param \BoostMyShop\Amazon\Helper\Mws\ProductsFactory $mwsProductClientFactory
     * @param \BoostMyShop\Amazon\Model\ProductFactory $productFactory
     * @param \BoostMyShop\Amazon\Helper\Barcode $barcodeHelper
     * @param \BoostMyShop\Amazon\Model\ResourceModel\Catalog\Product\Matching\CollectionFactory $matchingCollectionFactory
     * @param \BoostMyShop\Amazon\Helper\Timer $timer
     * @param \BoostMyShop\Amazon\Model\BatchFactory $batchFactory
     */
    public function __construct(
        \BoostMyShop\Amazon\Helper\Mws\ProductsFactory $mwsProductClientFactory,
        \BoostMyShop\Amazon\Model\ProductFactory $productFactory,
        \BoostMyShop\Amazon\Helper\Barcode $barcodeHelper,
        \BoostMyShop\Amazon\Model\ResourceModel\Catalog\Product\Matching\CollectionFactory $matchingCollectionFactory,
        \BoostMyShop\Amazon\Helper\Timer $timer,
        \BoostMyShop\Amazon\Model\BatchFactory $batchFactory
    ){
        parent::__construct($timer, $batchFactory);
        $this->_matchingCollectionFactory = $matchingCollectionFactory;
        $this->_barcodeHelper = $barcodeHelper;
        $this->_productFactory = $productFactory;
        $this->_mwsProductClientFactory = $mwsProductClientFactory;
    }

    /**
     * @param array $productIds
     * @return string
     * @throws \Exception
     */
    public function execute($productIds){

        $result = [];
        $data = [];
        $summary = '';
        $this->_timer->start();
        $this->_openBatch(\BoostMyShop\Amazon\Model\Batch::OPERATION_TYPE_MATCHING);

        try {
            $collection = $this->_matchingCollectionFactory->create()->loadForCountry($productIds, $this->getCountry());

            $matchingQueries = [
                \BoostMyShop\Amazon\Helper\Barcode::TYPE_EAN => [],
                \BoostMyShop\Amazon\Helper\Barcode::TYPE_UPC => []
            ];

            foreach ($collection as $item) {

                $barcode = $item->getData($this->getCountry()->getOption('barcode'));
                $barcodeType = $this->_barcodeHelper->getType($barcode);
                if (isset($matchingQueries[$barcodeType])) {

                    $data[$barcode] = $item->getId();
                    $matchingQueries[$barcodeType][] = $barcode;

                }

            }

            $result = array_merge($result, $this->_processMatching(\BoostMyShop\Amazon\Helper\Barcode::TYPE_EAN, $matchingQueries[\BoostMyShop\Amazon\Helper\Barcode::TYPE_EAN]));
            $result = array_merge($result, $this->_processMatching(\BoostMyShop\Amazon\Helper\Barcode::TYPE_UPC, $matchingQueries[\BoostMyShop\Amazon\Helper\Barcode::TYPE_UPC]));

            $result = $this->_processMatchingResult($result, $data);

            $summary .= count($productIds) . ' product(s) to match, ';
            $summary .= count($data) . ' product(s) selected, ';
            $summary .= $result['success'] . ' product(s) successfully matched, ';
            $summary .= $result['error'] . ' product(s) in error, ';
            $summary .= $result['missing'] . ' product(s) planed, ';
            $summary .= 'duration ' . $this->_timer->stop()->getDuration() . ' s';

            $this->_batch->setSummary($summary)
                ->save();

        }catch(\Exception $e){

            $summary .= $e->getMessage();
            $this->_batch->addErrorMessage($e->getMessage());

        }

        $this->_closeBatch();

        return $summary;

    }

    /**
     * @param string $idType
     * @param array $barcodes
     * @return array
     * @throws \Exception
     */
    protected function _processMatching($idType, $barcodes)
    {

        $result = [];
        $mwsClient = $this->_mwsProductClientFactory->create();

        if(count($barcodes) > 0) {

            $lists = array_chunk($barcodes, 5);

            do {

                $mwsClient
                    ->reset()
                    ->disableLogFiles()
                    ->setBatch($this->_batch)
                    ->setMerchantId($this->getCountry()->getAccount()->getMerchantId())
                    ->setAccessKeyId($this->getCountry()->getAccount()->getAccessKeyId())
                    ->setSecretKey($this->getCountry()->getAccount()->getSecretKey())
                    ->setBaseUrl($this->getCountry()->getendpoint())
                    ->setMarketplaceId($this->getCountry()->getmarketplace_id());

                $idList = array_shift($lists);

                $response = $mwsClient->getMatchingProductForId($idType, $idList);
                $headers = $response->getHeaders()->toArray();
                $result = array_merge($result, $this->_parseMatchingResponse($response->getBody()));

            } while (count($lists) > 0 && $headers['x-mws-quota-remaining'] > 0);

        }

        return $result;

    }

    /**
     * @param string $response
     * @return array $result
     */
    protected function _parseMatchingResponse($response){

        $resultTab = [];

        $domDocument = $this->_getDomDocumentSingleton();
        $domDocument->loadXML($response);

        foreach($domDocument->getElementsByTagName('GetMatchingProductForIdResult') as $result){

            $barcode = '_'.$result->getAttribute('Id');
            $asinTab = [];

            if($result->getAttribute('status') == 'Success'){

                foreach ($result->getElementsByTagName('Product') as $productNode) {
                    $asinTab[] = $productNode->getElementsByTagName('ASIN')->item(0)->nodeValue;
                }

                $resultTab[$barcode] = [
                    'status' => 'success',
                    'asins' => $asinTab
                ];

            }else{

                $resultTab[$barcode] = [
                    'status' => 'error',
                    'message' => $result->getElementsByTagName('Message')->item(0)->nodeValue
                ];

            }

        }

        return $resultTab;

    }

    /**
     * @param array $result
     * @param array $data
     * @return array
     */
    protected function _processMatchingResult($result, $data){

        $success  = 0;
        $error = 0;
        $missing = 0;

        $amazonProductModel = $this->_productFactory->create();

        foreach($data as $barcode => $productId){

            $product = $amazonProductModel->loadByAccountCountryId($productId, $this->getCountry()->getId()); // TODO : test this more

            if(isset($result['_'.$barcode])){

                if($result['_'.$barcode]['status'] == 'success'){

                    $product->setAsin($result['_'.$barcode]['asins'][0])
                        ->setStatus(\BoostMyShop\Amazon\Model\Product::STATUS_TO_PUSH)
                        ->setErrorMessage('')
                        ->setAccountCountryId($this->getCountry()->getId())
                        ->setProductId($productId)
                        ->save();

                    $this->_batch->addSuccessMessage($barcode.' will be associated with '.$result['_'.$barcode]['asins'][0]);
                    $success++;

                }else{

                    $product->setStatus(\BoostMyShop\Amazon\Model\Product::STATUS_NOT_FOUND)
                        ->setErrorMessage($result['_'.$barcode]['message'])
                        ->setAccountCountryId($this->getCountry()->getId())
                        ->setProductId($productId)
                        ->save();

                    $this->_batch->addErrorMessage($barcode.' in error : '.$result['_'.$barcode]['message']);
                    $error++;

                }


            }else{

                $product->setStatus(\BoostMyShop\Amazon\Model\Product::STATUS_TO_MATCH)
                    ->setAccountCountryId($this->getCountry()->getId())
                    ->setProductId($productId)
                    ->setErrorMessage('')
                    ->save();

                $this->_batch->addNoticeMessage($barcode.' will be submitted later');
                $missing++;

            }

        }

        return ['success' => $success, 'error' => $error, 'missing' => $missing];

    }

    /**
     * @return mixed
     */
    protected function _getDomDocumentSingleton(){

        if(is_null($this->getDomDocument())){

            $this->setDomDocument(new \DOMDocument());

        }

        return $this->getDomDocument();

    }

}