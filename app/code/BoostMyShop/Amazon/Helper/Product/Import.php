<?php namespace BoostMyShop\Amazon\Helper\Product;

/**
 * Class Import
 *
 * @package   BoostMyShop\Amazon\Helper\Product
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Import extends \BoostMyShop\Amazon\Helper\Base {

    /**
     * @var \BoostMyShop\Amazon\Model\ProductFactory
     */
    protected $_productFactory;

    /**
     * @var \BoostMyShop\Amazon\Helper\Product
     */
    protected $_productHelper;

    /**
     * @var \BoostMyShop\Amazon\Helper\Mws\ReportsFactory
     */
    protected $_mwsReportClientFactory;

    /**
     * Import constructor.
     * @param \BoostMyShop\Amazon\Helper\Mws\ReportsFactory $mwsReportClientFactory
     * @param \BoostMyShop\Amazon\Helper\Product $productHelper
     * @param \BoostMyShop\Amazon\Model\ProductFactory $productFactory
     * @param \BoostMyShop\Amazon\Helper\Timer $timer
     * @param \BoostMyShop\Amazon\Model\BatchFactory $batchFactory
     */
    public function __construct(
        \BoostMyShop\Amazon\Helper\Mws\ReportsFactory $mwsReportClientFactory,
        \BoostMyShop\Amazon\Helper\Product $productHelper,
        \BoostMyShop\Amazon\Model\ProductFactory $productFactory,
        \BoostMyShop\Amazon\Helper\Timer $timer,
        \BoostMyShop\Amazon\Model\BatchFactory $batchFactory
    ){
        parent::__construct($timer, $batchFactory);
        $this->_productFactory = $productFactory;
        $this->_productHelper = $productHelper;
        $this->_mwsReportClientFactory = $mwsReportClientFactory;
    }

    /**
     * @return \BoostMyShop\Amazon\Lib\MWS\Client
     * @throws \Exception
     */
    protected function _getMwsReportClient()
    {

        return $this->_mwsReportClientFactory->create()
            ->reset()
            ->setBatch($this->_batch)
            ->setMerchantId($this->getCountry()->getAccount()->getMerchantId())
            ->setAccessKeyId($this->getCountry()->getAccount()->getAccessKeyId())
            ->setSecretKey($this->getCountry()->getAccount()->getSecretKey())
            ->setBaseUrl($this->getCountry()->getendpoint())
            ->setMarketplaceId($this->getCountry()->getmarketplace_id());

    }

    /**
     * @return string
     * @throws \Exception
     */
    public function execute()
    {

        $this->_openBatch(\BoostMyShop\Amazon\Model\Batch::OPERATION_TYPE_CATALOG);
        $this->_timer->start();
        $lines = [];
        $mwsClient = $this->_getMwsReportClient();
        $reportAmazonProductIds = [];
        $amazonProductCollection = $this->_productFactory->create()
            ->getCollection()
            ->addFieldToFilter('account_country_id', $this->getCountry()->getId())
            ->addFieldToFilter('status', \BoostMyShop\Amazon\Model\Product::STATUS_ASSOCIATED);

        $list = $mwsClient->getReportList(
            10,
            [\BoostMyShop\Amazon\Lib\MWS\Reports::REPORT_TYPE_INVENTORY]
        )->getBody();
        $mwsClient = $this->_getMwsReportClient();

        $xml = new \DOMDocument();
        $xml->loadXML($list);

        if($xml->getElementsByTagName('ReportId')->item(0)){

            $reportId = $xml->getElementsByTagName('ReportId')->item(0)->nodeValue;
            $report = $mwsClient->getReport($reportId)->getBody();

            $lines = explode("\n", $report);
            $header = array_shift($lines);

            foreach ($lines as $line) {

                $info = explode("\t", $line);
                $product = $this->_productHelper->getProductFromSellerSku($info[0], $this->getCountry()->getOption('reference'));
                if (!$product->getId()) {
                    $this->_batch->addErrorMessage('Missing product ' . $info[0]);
                } else {
                    $item = $this->_productFactory->create()->getCollection()
                        ->addFieldToFilter('account_country_id', $this->getCountry()->getId())
                        ->addFieldToFilter('product_id', $product->getId())
                        ->getFirstItem();

                    if ($item->getId()) {

                        $item->setAsin($info[1])
                            ->setStatus(\BoostMyShop\Amazon\Model\Product::STATUS_ASSOCIATED)
                            ->save();

                        $this->_batch->addSuccessMessage($info[0] . ' updated');

                    } else {

                        $item = $this->_productFactory->create();
                        $item->setAsin($info[1])
                            ->setProductId($product->getId())
                            ->setAccountCountryId($this->getCountry()->getId())
                            ->setStatus(\BoostMyShop\Amazon\Model\Product::STATUS_ASSOCIATED)
                            ->save();

                        $this->_batch->addSuccessMessage($info[0] . ' added');

                    }

                    $reportAmazonProductIds[$item->getId()] = $item->getId();

                }

            }

        }

        $nbrDeleted = 0;
        foreach($amazonProductCollection as $amazonProduct){

            if(!isset($reportAmazonProductIds[$amazonProduct->getId()])){

                $amazonProduct->delete();
                $nbrDeleted++;

            }

        }

        $this->_getMwsReportClient()->requestReport(\BoostMyShop\Amazon\Lib\MWS\Reports::REPORT_TYPE_INVENTORY);

        $summary = count($lines) . ' product(s) listed, ';
        $summary .= $nbrDeleted . ' product(s) deleted, ';
        $summary .= 'duration ' . $this->_timer->stop()->getDuration() . ' s';
        $this->_batch->setSummary($summary)
            ->save();
        $this->_closeBatch();

        return $this->_batch->getId();

    }

}