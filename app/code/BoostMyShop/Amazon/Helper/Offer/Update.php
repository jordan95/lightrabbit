<?php namespace BoostMyShop\Amazon\Helper\Offer;

/**
 * Class Update
 *
 * @package   BoostMyShop\Amazon\Helper\Offer
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Update extends \BoostMyShop\Amazon\Helper\Feed {

    /**
     * @var \BoostMyShop\Amazon\Model\ResourceModel\Catalog\Product\CollectionFactory
     */
    protected $_productCollectionFactory;

    /**
     * @var \BoostMyShop\Amazon\Lib\MWS\Xml\InventoryFactory
     */
    protected $_inventoryFeedFactory;

    /**
     * @var \BoostMyShop\Amazon\Lib\MWS\Xml\PriceFactory
     */
    protected $_priceFeedFactory;

    /**
     * @var \BoostMyShop\Amazon\Model\ProductFactory
     */
    protected $_amazonProductFactory;

    /**
     * @var \BoostMyShop\Amazon\Helper\Offer\Stock
     */
    protected $_stockHelper;

    /**
     * @var \BoostMyShop\Amazon\Helper\Offer\Price
     */
    protected $_priceHelper;

    /**
     * @var \BoostMyShop\Amazon\Helper\Offer\Delay
     */
    protected $_delayHelper;

    /**
     * Update constructor.
     * @param \BoostMyShop\Amazon\Helper\Offer\Delay $delayHelper
     * @param \BoostMyShop\Amazon\Helper\Offer\Stock $stockHelper
     * @param \BoostMyShop\Amazon\Helper\Offer\Price $priceHelper
     * @param \BoostMyShop\Amazon\Model\ProductFactory $amazonProductFactory
     * @param \BoostMyShop\Amazon\Lib\MWS\Xml\InventoryFactory $inventoryFeedFactory
     * @param \BoostMyShop\Amazon\Lib\MWS\Xml\PriceFactory $priceFeedFactory
     * @param \BoostMyShop\Amazon\Model\ResourceModel\Catalog\Product\CollectionFactory $productCollectionFactory
     * @param \BoostMyShop\Amazon\Helper\Mws\FeedsFactory $mwsFeedClientFactory
     * @param \BoostMyShop\Amazon\Helper\Timer $timer
     * @param \BoostMyShop\Amazon\Model\BatchFactory $batchFactory
     */
    public function __construct(
        \BoostMyShop\Amazon\Helper\Offer\Delay $delayHelper,
        \BoostMyShop\Amazon\Helper\Offer\Stock $stockHelper,
        \BoostMyShop\Amazon\Helper\Offer\Price $priceHelper,
        \BoostMyShop\Amazon\Model\ProductFactory $amazonProductFactory,
        \BoostMyShop\Amazon\Lib\MWS\Xml\InventoryFactory $inventoryFeedFactory,
        \BoostMyShop\Amazon\Lib\MWS\Xml\PriceFactory $priceFeedFactory,
        \BoostMyShop\Amazon\Model\ResourceModel\Catalog\Product\CollectionFactory $productCollectionFactory,
        \BoostMyShop\Amazon\Helper\Mws\FeedsFactory $mwsFeedClientFactory,
        \BoostMyShop\Amazon\Helper\Timer $timer,
        \BoostMyShop\Amazon\Model\BatchFactory $batchFactory
    ){
        parent::__construct($mwsFeedClientFactory, $timer, $batchFactory);
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_inventoryFeedFactory = $inventoryFeedFactory;
        $this->_priceFeedFactory = $priceFeedFactory;
        $this->_amazonProductFactory = $amazonProductFactory;
        $this->_stockHelper = $stockHelper;
        $this->_priceHelper = $priceHelper;
        $this->_delayHelper = $delayHelper;
    }

    /**
     * @param array $productIds
     * @return string $summary
     */
    public function processIds($productIds){

        $collection = $this->_productCollectionFactory->create()
            ->joinAmazonProducts($this->getCountry())
            ->addFieldToFilter('entity_id', ['in' => $productIds]);

        return $this->_updateCollection($collection);

    }

    /**
     * @return string
     */
    public function processToUpdate(){

        $collection = $this->_productCollectionFactory->create()
            ->joinAmazonProducts($this->getCountry())
            ->applyExportFilter();

        return $this->_updateCollection($collection);

    }

    /**
     * @param \BoostMyShop\Amazon\Model\ResourceModel\Catalog\Product\Collection $collection
     * @return string
     */
    protected function _updateCollection(\BoostMyShop\Amazon\Model\ResourceModel\Catalog\Product\Collection $collection){

        $summary = 'Nothing to update';

        if($collection->getSize() > 0) {
            $this->_timer->start();
            $priceFeed = $this->_priceFeedFactory->create();
            $priceFeed->setMerchantIdentifier($this->getCountry()->getAccount()->getMerchantId())
                ->openStream();

            $inventoryFeed = $this->_inventoryFeedFactory->create();
            $inventoryFeed->setMerchantIdentifier($this->getCountry()->getAccount()->getMerchantId())
                ->openStream();

            $productsUpdated = [];
            foreach ($collection as $item) {

                $inventoryFeed->happenInventory(
                    [
                        'sku' => $item->getData($this->getCountry()->getOption('reference')),
                        'quantity' => $this->_stockHelper->getStockToExport($item, $this->getCountry()),
                        'fulfillmentLatency' => $this->_delayHelper->getDelayToExport($item, $this->getCountry())
                    ]
                );

                $priceParams = [
                    'sku' => $item->getData($this->getCountry()->getOption('reference')),
                    'currency' => $this->getCountry()->getCurrencyCode(),
                ];
                $priceToExport = $this->_priceHelper->getPriceToExport($item, $this->getCountry());
                $priceFeed->happenPrice(array_merge($priceParams, $priceToExport));

                $productsUpdated[] = $item->getAmazonProductId();

            }

            $summary = count($productsUpdated) . ' products updated, ';

            $this->_sendFeed(\BoostMyShop\Amazon\Model\Batch::OPERATION_TYPE_PRICE, $priceFeed, $summary);
            $this->_sendFeed(\BoostMyShop\Amazon\Model\Batch::OPERATION_TYPE_INVENTORY, $inventoryFeed, $summary);

            $lastUpdate = time();
            foreach ($productsUpdated as $amazonProductId) {

                $item = $this->_amazonProductFactory->create()->load($amazonProductId);
                $item->setLastUpdate($lastUpdate)
                    ->save();

            }
        }

        return $summary;

    }

}