<?php namespace BoostMyShop\Amazon\Helper\Offer;

/**
 * Class Price
 *
 * @package   BoostMyShop\Amazon\Helper\Offer
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Price {

    /**
     * @var \Magento\Catalog\Helper\Data
     */
    protected $_catalogHelper;

    /**
     * Price constructor.
     * @param \Magento\Catalog\Helper\Data $catalogHelper
     */
    public function __construct(
        \Magento\Catalog\Helper\Data $catalogHelper
    ){
        $this->_catalogHelper = $catalogHelper;
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @param \BoostMyShop\Amazon\Model\Account\Country $country
     * @return array
     */
    public function getPriceToExport(\Magento\Catalog\Model\Product $product, \BoostMyShop\Amazon\Model\Account\Country $country){

        $priceToExport = [];
        $addTax = ($country->getOption('add_tax') == 1) ? true : false;
        $price = $product->getPrice();
        $priceAttribute = $country->getOption('price_attribute');
        if(!empty($priceAttribute)){

            $price = ($product->getData($priceAttribute)) ? $product->getData($priceAttribute) : $price;

        }

        $price = $this->_applyCoef($price, $country);

        $priceToExport['price'] = $this->_catalogHelper->getTaxPrice($product, $price, $addTax, null, null, null, $country->getStore());

        if($country->getOption('use_special_price') && $this->_hasSpecialPrice($product)){

            $priceToExport['sale'] = [
                'price' => $this->_catalogHelper->getTaxPrice($product, $product->getSpecialPrice(), $addTax, null, null, null, $country->getStore()),
                'startDate' => date('Y-m-d\TH:i:s', strtotime($product->getSpecialFromDate())),
                'endDate' => date('Y-m-d\TH:i:s', strtotime($product->getSpecialToDate()))
            ];

        }

        return $priceToExport;

    }

    /**
     * Is product has special price
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return boolean
     */
    protected function _hasSpecialPrice(\Magento\Catalog\Model\Product $product)
    {
        $hasSpecialPrice = false;

        $specialPrice = $product->getSpecialPrice();

        if (!empty($specialPrice)) {
            $hasSpecialPrice = true;

            $fromDate = $product->getSpecialFromDate();

            if ($fromDate != '') {
                if (strtotime($fromDate) > time()) {
                    $hasSpecialPrice = false;
                }
            }

            $toDate = $product->getSpecialToDate();

            if ($toDate != '') {
                if (strtotime($toDate) < time()) {
                    $hasSpecialPrice = false;
                }
            }
        }

        return $hasSpecialPrice;
    }

    /**
     * @param float $price
     * @param \BoostMyShop\Amazon\Model\Account\Country $country
     * @return mixed
     */
    protected function _applyCoef($price, $country){

        $coef = str_replace(',', '.', $country->getOption('price_coef'));

        if(!empty($coef)){
            $price *= $coef;
        }

        return $price;

    }

}