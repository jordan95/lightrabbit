<?php namespace BoostMyShop\Amazon\Helper\Offer;

/**
 * Class Stock
 *
 * @package   BoostMyShop\Amazon\Helper\Offer
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Stock {

    /**
     * @var \Magento\CatalogInventory\Api\StockRegistryInterface
     */
    protected $_stockRegistry;

    /**
     * Stock constructor.
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
     */
    public function __construct(
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
    ){
        $this->_stockRegistry = $stockRegistry;
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @param \BoostMyShop\Amazon\Model\Account\Country $country
     * @return int
     */
    public function getStockToExport(\Magento\Catalog\Model\Product $product, \BoostMyShop\Amazon\Model\Account\Country $country){

        if($product->getAmazonStatus() == \BoostMyShop\Amazon\Model\Product::STATUS_DISABLED){

            $stock = 0;

        }else{

            $stockItem = $this->_stockRegistry->getStockItem($product->getId(), $product->getStore()->getWebsiteId());
            $stock = $stockItem->getQty();

            $stockAttribute = $country->getOption('stock_attribute');
            if(!empty($stockAttribute)){

                $stock = ($product->getData($stockAttribute)) ? $product->getData($stockAttribute) : $stock;

            }

        }

        return $stock;

    }

}