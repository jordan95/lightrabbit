<?php namespace BoostMyShop\Amazon\Helper\Offer;

/**
 * Class Delay
 *
 * @package   BoostMyShop\Amazon\Helper\Offer
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Delay {

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @param \BoostMyShop\Amazon\Model\Account\Country $country
     * @return int
     */
    public function getDelayToExport(\Magento\Catalog\Model\Product $product, \BoostMyShop\Amazon\Model\Account\Country $country){

        $delay = $country->getOption('default_delay');

        $delayAttribute = $country->getOption('delay_attribute');
        if(!empty($delayAttribute)){

            $delay = ($product->getData($delayAttribute)) ? $product->getData($delayAttribute) : $delay;

        }

        return $delay;

    }

}