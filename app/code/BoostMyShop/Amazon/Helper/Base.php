<?php namespace BoostMyShop\Amazon\Helper;

/**
 * Class Base
 *
 * @package   BoostMyShop\Amazon\Helper
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
abstract class Base extends \Magento\Framework\DataObject
{

    /**
     * @var \BoostMyShop\Amazon\Model\BatchFactory
     */
    protected $_batchFactory;

    /**
     * @var \BoostMyShop\Amazon\Model\Batch
     */
    protected $_batch;

    /**
     * @var \BoostMyShop\Amazon\Helper\Timer
     */
    protected $_timer;

    /**
     * Base constructor.
     * @param \BoostMyShop\Amazon\Helper\Timer $timer
     * @param \BoostMyShop\Amazon\Model\BatchFactory $batchFactory
     */
    public function __construct(
        \BoostMyShop\Amazon\Helper\Timer $timer,
        \BoostMyShop\Amazon\Model\BatchFactory $batchFactory
    ){
        $this->_batchFactory = $batchFactory;
        $this->_timer = $timer;
    }

    /**
     * @param string $type
     */
    protected function _openBatch($type)
    {

        $this->_batch = $this->_batchFactory->create()
            ->setOperationType($type)
            ->setStatus(\BoostMyShop\Amazon\Model\Batch::STATUS_NEW)
            ->setAccountCountryId($this->getCountry()->getId())
            ->save();

    }

    protected function _closeBatch()
    {

        if ($this->_batch->getStatus() != \BoostMyShop\Amazon\Model\Batch::STATUS_ERROR) {

            $this->_batch->setStatus(\BoostMyShop\Amazon\Model\Batch::STATUS_SUCCESS)
                ->save();

        }

    }

}