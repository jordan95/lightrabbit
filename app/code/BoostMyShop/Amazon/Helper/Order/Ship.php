<?php namespace BoostMyShop\Amazon\Helper\Order;

/**
 * Class Ship
 *
 * @package   BoostMyShop\Amazon\Helper\Order
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Ship extends \BoostMyShop\Amazon\Helper\Order {

    /**
     * @var \BoostMyShop\Amazon\Model\OrderFactory
     */
    protected $_orderFactory;

    /**
     * @var \BoostMyShop\Amazon\Lib\MWS\Xml\OrderFulfillmentFactory
     */
    protected $_orderFulfillmentFactory;

    /**
     * @var \Magento\Quote\Model\ResourceModel\Quote\Address\Rate\CollectionFactory
     */
    protected $_rateCollectionFactory;

    /**
     * Ship constructor.
     * @param \Magento\Quote\Model\ResourceModel\Quote\Address\Rate\CollectionFactory $rateCollectionFactory
     * @param \BoostMyShop\Amazon\Lib\MWS\Xml\OrderFulfillmentFactory $orderFulfillmentFactory
     * @param \BoostMyShop\Amazon\Model\OrderFactory $orderFactory
     * @param \BoostMyShop\Amazon\Helper\Mws\OrdersFactory $mwsOrderClientFactory
     * @param \BoostMyShop\Amazon\Helper\Mws\FeedsFactory $mwsFeedClientFactory
     * @param \BoostMyShop\Amazon\Helper\Timer $timer
     * @param \BoostMyShop\Amazon\Model\BatchFactory $batchFactory
     */
    public function __construct(
        \Magento\Quote\Model\ResourceModel\Quote\Address\Rate\CollectionFactory $rateCollectionFactory,
        \BoostMyShop\Amazon\Lib\MWS\Xml\OrderFulfillmentFactory $orderFulfillmentFactory,
        \BoostMyShop\Amazon\Model\OrderFactory $orderFactory,
        \BoostMyShop\Amazon\Helper\Mws\OrdersFactory $mwsOrderClientFactory,
        \BoostMyShop\Amazon\Helper\Mws\FeedsFactory $mwsFeedClientFactory,
        \BoostMyShop\Amazon\Helper\Timer $timer,
        \BoostMyShop\Amazon\Model\BatchFactory $batchFactory
    ){
        parent::__construct($mwsOrderClientFactory, $mwsFeedClientFactory, $timer, $batchFactory);
        $this->_orderFactory = $orderFactory;
        $this->_orderFulfillmentFactory = $orderFulfillmentFactory;
        $this->_rateCollectionFactory = $rateCollectionFactory;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function execute()
    {
        $this->_openBatch(\BoostMyShop\Amazon\Model\Batch::OPERATION_TYPE_SHIPMENT);
        $this->_timer->start();

        $orders = $this->_listOrders(
            ['CreatedAfter' => date('Y-m-d\TH:i:s', time() - 3600 * 24 * 14)],
            [],
            ['Unshipped', 'PartiallyShipped'],
            ['MFN']
        );

        $amazonOrderIds = $this->_extractAmazonOrderIds($orders);
        $amazonOrderIdsCount = count($amazonOrderIds);
        $orderFulfillment = $this->generateOrderFulfillmentFeed($amazonOrderIds);
        $logWithoutSend = false;
        $summary = $amazonOrderIdsCount . ' order(s) listed, ';

        if($orderFulfillment === null) {
            $summary = 'No orders to ship';
            $logWithoutSend = true;
        }

        $this->_sendFeed(\BoostMyShop\Amazon\Model\Batch::OPERATION_TYPE_SHIPMENT, $orderFulfillment, $summary, $logWithoutSend);

        return $summary;

    }

    /**
     * @param array $orders
     * @return array $amazonOrderIds
     */
    protected function _extractAmazonOrderIds($orders)
    {

        $amazonOrderIds = [];

        foreach ($orders as $order) {

            $amazonOrderIds[] = $order->AmazonOrderId;

        }

        return $amazonOrderIds;

    }

    /**
     * @param array $amazonOrderIds
     * @return \BoostMyShop\Amazon\Lib\MWS\Xml\OrderFulfillment $orderFulfillment
     */
    public function generateOrderFulfillmentFeed($amazonOrderIds)
    {

        $ordersToShipCollection = $this->_orderFactory->create()->getOrdersToShipCollection($amazonOrderIds);
        $orderFulfillment = $this->_orderFulfillmentFactory->create();
        $orderFulfillment
            ->setMerchantIdentifier($this->getCountry()->getAccount()->getMerchantId())
            ->openStream();

        $cptOrdersCompleteWithTracking = 0;
        foreach ($ordersToShipCollection as $order) {

            $shipments = [];
            foreach ($order->getShipmentsCollection() as $shipment) {

                $tracks = $shipment->getTracksCollection();

                if ($tracks->getFirstItem()) {

                    $shipmentInfo = [
                        'MerchantFulfillmentId' => $shipment->getId(),
                        'FulfillmentDate' => date("Y-m-d\TH:i:s", strtotime($shipment->getCreatedAt())),
                        'CarrierCode' => $tracks->getFirstItem()->getCarrierCode(),
                        'CarrierName' => $tracks->getFirstItem()->getTitle(),
                        'ShippingMethod' => $this->getShippingMethodName(),
                        'ShipperTrackingNumber' => $tracks->getFirstItem()->getTrackNumber(),
                        'products' => []
                    ];

                    foreach ($shipment->getAllItems() as $item) {

                        $shipmentInfo['products'][] = [
                            'Quantity' => $item->getQty(),
                            'AmazonOrderItemCode' => $this->_orderFactory->create()->load($order->getAmazonOrderId(), 'amazon_order_id')->retrieveAmazonOrderItemId($item->getProductId(), $this->getCountry()->getOption('reference')),
                            'MerchantOrderItemID' => $item->getOrderItemId(),
                            'MerchantFulfillmentItemID' => $item->getId()
                        ];

                    }

                    $shipments[] = $shipmentInfo;

                }

            }
            if (count($shipments) > 0) {
                $cptOrdersCompleteWithTracking++;
                $orderFulfillment->happenOrderFulfillment($order->getAmazonOrderId(), $shipments);
            }

        }

        if($cptOrdersCompleteWithTracking === 0) {
            return null;
        }

        return $orderFulfillment;

    }

    /**
     * @return string
     */
    public function getShippingMethodName()
    {

        foreach ($this->_rateCollectionFactory->create() as $rate) {

            if ($rate->getCode() == $this->getOption('shipment_method'))
                return $rate->getCarrierTitle() . ' - ' . $rate->getMethodTitle();

        }

        return '';

    }

}