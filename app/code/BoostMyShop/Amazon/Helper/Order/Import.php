<?php namespace BoostMyShop\Amazon\Helper\Order;

/**
 * Class Import
 *
 * @package   BoostMyShop\Amazon\Helper\Order
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Import extends \BoostMyShop\Amazon\Helper\Order {

    /**
     * @var \BoostMyShop\Amazon\Model\OrderFactory
     */
    protected $_orderFactory;

    /**
     * @var \Magento\Store\Model\StoreFactory
     */
    protected $_storeFactory;

    /**
     * @var \Magento\Quote\Model\QuoteFactory
     */
    protected $_quoteFactory;

    /**
     * @var \BoostMyShop\Amazon\Model\Order\Invoice
     */
    protected $_invoiceModel;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $_customerRepository;

    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    protected $_customerFactory;

    /**
     * @var \Magento\Customer\Model\AddressFactory
     */
    protected $_addressFactory;

    /**
     * @var \BoostMyShop\Amazon\Helper\Product
     */
    protected $_productHelper;

    /**
     * @var \BoostMyShop\Amazon\Helper\Tax
     */
    protected $_taxHelper;

    /**
     * @var \Magento\Framework\DataObject
     */
    protected $_request;

    /**
     * @var \Magento\Quote\Model\QuoteManagement
     */
    protected $_quoteManagement;

    /**
     * @var \Magento\Directory\Model\RegionFactory
     */
    protected $_regionFactory;

    /**
     * Import constructor.
     * @param \Magento\Quote\Model\QuoteManagement $quoteManagement
     * @param \Magento\Framework\DataObject $request
     * @param \BoostMyShop\Amazon\Helper\Tax $taxHelper
     * @param \BoostMyShop\Amazon\Helper\Product $productHelper
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     * @param \Magento\Customer\Model\AddressFactory $addressFactory
     * @param \Magento\Store\Model\StoreFactory $storeFactory
     * @param \Magento\Quote\Model\QuoteFactory $quoteFactory
     * @param \BoostMyShop\Amazon\Model\Order\Invoice $invoiceModel
     * @param \BoostMyShop\Amazon\Model\OrderFactory $orderFactory
     * @param \BoostMyShop\Amazon\Helper\Mws\OrdersFactory $mwsOrderClientFactory
     * @param \BoostMyShop\Amazon\Helper\Mws\FeedsFactory $mwsFeedClientFactory
     * @param \BoostMyShop\Amazon\Helper\Timer $timer
     * @param \BoostMyShop\Amazon\Model\BatchFactory $batchFactory
     * @param \Magento\Directory\Model\RegionFactory $regionFactory
     */
    public function __construct(
        \Magento\Quote\Model\QuoteManagement $quoteManagement,
        \Magento\Framework\DataObject $request,
        \BoostMyShop\Amazon\Helper\Tax $taxHelper,
        \BoostMyShop\Amazon\Helper\Product $productHelper,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Model\AddressFactory $addressFactory,
        \Magento\Store\Model\StoreFactory $storeFactory,
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
        \BoostMyShop\Amazon\Model\Order\Invoice $invoiceModel,
        \BoostMyShop\Amazon\Model\OrderFactory $orderFactory,
        \BoostMyShop\Amazon\Helper\Mws\OrdersFactory $mwsOrderClientFactory,
        \BoostMyShop\Amazon\Helper\Mws\FeedsFactory $mwsFeedClientFactory,
        \BoostMyShop\Amazon\Helper\Timer $timer,
        \BoostMyShop\Amazon\Model\BatchFactory $batchFactory,
        \Magento\Directory\Model\RegionFactory $regionFactory
    ){
        parent::__construct( $mwsOrderClientFactory, $mwsFeedClientFactory, $timer, $batchFactory);
        $this->_orderFactory = $orderFactory;
        $this->_storeFactory = $storeFactory;
        $this->_quoteFactory = $quoteFactory;
        $this->_invoiceModel = $invoiceModel;
        $this->_customerRepository = $customerRepository;
        $this->_customerFactory = $customerFactory;
        $this->_addressFactory = $addressFactory;
        $this->_productHelper = $productHelper;
        $this->_taxHelper = $taxHelper;
        $this->_request = $request;
        $this->_quoteManagement = $quoteManagement;
        $this->_regionFactory = $regionFactory;
    }

    /**
     * @return string
     */
    public function execute()
    {

        $this->_openBatch(\BoostMyShop\Amazon\Model\Batch::OPERATION_TYPE_ORDER);
        $this->_timer->start();

        $orders = $this->_listOrders(
            ['CreatedAfter' => date('Y-m-d\TH:i:s', time() - $this->getCountry()->getOption('order_importation_range') * 3600 * 24)],
            [],
            ['Unshipped', 'PartiallyShipped'],
            ['MFN']
        );

        $cptOrdersListed = count($orders);
        $cptOrdersNotAlreadyImported = 0;
        $cptOrdersInError = 0;
        $cptOrdersAlreadyImported = 0;
        $cptOrderImported = 0;

        foreach ($orders as $order) {

            try {

                if ($this->_orderFactory->create()->orderAlreadyImported($order->AmazonOrderId)) {
                    $this->_batch->addNoticeMessage('Order #' . $order->AmazonOrderId . ' is already imported');
                    $cptOrdersAlreadyImported++;
                    continue;
                }

                $cptOrdersNotAlreadyImported++;
                $address = $this->_buildAddressFromOrder($order);

                $store = $this->_storeFactory->create()->load($this->getCountry()->getOption('store'));
                $quote = $this->_quoteFactory->create();
                $quote->setStore($store);

                if ($this->getCountry()->getOption('customer_mode') == 'guest') {

                    $quote->setCustomerIsGuest(true);
                    $quote->setCustomerEmail($order->BuyerEmail);
                    $quote->setCheckoutMethod('guest');

                } else {

                    $customer = $this->_createCustomer(
                        $address,
                        //$order->BuyerEmail,
                        uniqid() . '@gmail.com',
                        $this->getCountry()->getOption('store'),
                        $this->getCountry()->getOption('customer_group')
                    );

                    $customer = $this->_customerRepository->getById($customer->getEntityId());
                    $quote->assignCustomer($customer);

                }

                $orderItems = $this->_listOrderItems($order->AmazonOrderId);
                foreach ($orderItems as $item) {

                    $product = $this->_productHelper->loadProductFromSellerSku($item->SellerSKU, $this->getCountry()->getOption('reference'));
                    if (!$product->getId()) {
                        throw new \Exception('reference ' . $item->SellerSKU . ' does not exist');
                    }

                    try{

                        $taxRate = $this->_taxHelper
                            ->setStoreId($store->getId())
                            ->setCountryCode($address['country_id'])
                            ->setRegionId($address['region_id'])
                            ->setPostCode($address['postcode'])
                            ->setCustomerTaxClassId($this->getCountry()->getOption('customer_tax_class_id'))
                            ->setProductTaxClassId($product->getTaxClassId())
                            ->calculateProductTaxRate();

                        $itemPrice = ($item->ItemPrice->Amount / $item->QuantityOrdered) / (1 + $taxRate / 100);

                        $this->_request->setQty(intval($item->QuantityOrdered))
                            ->setCustomPrice($itemPrice);
                        $quote->addProduct($product, $this->_request);

                    }catch(\Exception $e){

                        throw new \Exception($e->getMessage().' (product '.$product->getSku().')');

                    }

                }

                $quote->setCustomerTaxClassId($this->getCountry()->getOption('customer_tax_class_id'));
                $quote->getBillingAddress()->addData($address);
                $quote->getShippingAddress()->addData($address);
                $quote->getShippingAddress()->setCollectShippingRates(true);
                $quote->getShippingAddress()->setShippingMethod($this->getCountry()->getOption('shipment_method'));
                $quote->getShippingAddress()->setFreeShipping(false);
                $quote->getShippingAddress()->collectShippingRates();
                foreach($quote->getShippingAddress()->getShippingRatesCollection() as $rate){
                    $rate->setPrice($item->ShippingPrice->Amount - $item->ShippingTax->Amount);
                }
                $quote->setPaymentMethod($this->getCountry()->getOption('payment_method'));
                $quoteItems = $quote->getItems();
                $quote->save();
                $quote->getPayment()->importData(['method' => $this->getCountry()->getOption('payment_method')]);
                $quote->collectTotals()->save();
                $quote->setItems($quoteItems);

                $magentoOrder = $this->_quoteManagement->submit($quote);
                $history = $magentoOrder->addStatusHistoryComment('Amazon order #' . $order->AmazonOrderId, $magentoOrder->getStatus());
                $history->setIsVisibleOnFront(false);
                $history->setIsCustomerNotified(false);
                $history->save();

                $magentoOrder->setState(\Magento\Sales\Model\Order::STATE_NEW, $this->getOrderState())->save(); // TODO : option order state ??

                $amazonOrder = $this->_orderFactory->create();
                $amazonOrder->setAmazonOrderId($order->AmazonOrderId)
                    ->setOrderId($magentoOrder->getId())
                    ->setAccountCountryId($this->getCountry()->getId())
                    ->save();
                $amazonOrder->addAmazonOrderItems($orderItems);

                $cptOrderImported++;

                $this->_batch->addSuccessMessage('Order #' . $order->AmazonOrderId . ' has been successfully imported in Magento #' . $magentoOrder->getIncrementId());

                $magentoOrder->save();
                if ($this->getCountry()->getOption('generate_invoice')) {

                    $this->_invoiceModel->createInvoice($magentoOrder);
                    $magentoOrder->save();

                }

            } catch (\Exception $e) {

                $cptOrdersInError++;
                $this->_batch->setStatus(\BoostMyShop\Amazon\Model\Batch::STATUS_ERROR)
                    ->addErrorMessage('Order #' . $order->AmazonOrderId . ' can not be imported : ' . $e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine())
                    ->save();

            }

        }

        $summary = $cptOrdersListed . ' order(s) listed, ';
        $summary .= $cptOrderImported . ' order(s) imported, ';
        $summary .= $cptOrdersAlreadyImported . ' order(s) already imported, ';
        $summary .= $cptOrdersNotAlreadyImported . ' order(s) to import, ';
        $summary .= $cptOrdersInError . ' order(s) in error, ';
        $summary .= 'duration ' . $this->_timer->stop()->getDuration() . ' s';
        $this->_batch->setSummary($summary)
            ->save();

        $this->_closeBatch();

        return $this->_batch->getId();

    }

    /**
     * @param \DOMElement $order
     * @return array
     */
    protected function _buildAddressFromOrder($order)
    {

        $nameTab = explode(' ', $order->ShippingAddress->Name);
        $lastname = array_pop($nameTab);
        $firstname = implode(' ', $nameTab);
        $firstname = (empty($firstname)) ? $lastname : $firstname;

        $street = '';
        for ($i = 1; $i < 4; $i++) {
            $attribute = 'AddressLine' . $i;
            if (isset($order->ShippingAddress->$attribute))
                $street .= $order->ShippingAddress->$attribute . ' ';
        }

        $countryCode = $order->ShippingAddress->CountryCode;
        $stateOrRegion = isset($order->ShippingAddress->StateOrRegion) ? $order->ShippingAddress->StateOrRegion : '';

        return [
            'firstname' => $firstname,
            'lastname' => $lastname,
            'street' => $street,
            'city' => $order->ShippingAddress->City,
            'country_id' => $order->ShippingAddress->CountryCode,
            'region_id' => $this->retrieveRegionId($stateOrRegion, $countryCode),
            'postcode' => $order->ShippingAddress->PostalCode,
            'telephone' => (isset($order->ShippingAddress->Phone)) ? $order->ShippingAddress->Phone : '-',
            'save_in_address_book' => 0
        ];

    }

    /**
     * @param array $address
     * @param string $email
     * @param int $storeId
     * @param int $customerGroupId
     * @return \Magento\Customer\Model\Customer $customer
     */
    protected function _createCustomer($address, $email, $storeId, $customerGroupId)
    {

        $firstname = $address['firstname'];
        $lastname = $address['lastname'];

        $webSiteId = $this->_storeFactory->create()->load($storeId)->getWebsiteId();

        /** @var \Magento\Customer\Model\Customer $customer */
        $customer = $this->_customerFactory->create()
            ->setWebsiteId($webSiteId)
            ->loadByEmail($email);
        if ($customer->getId()) {
            return $customer;
        }

        $customer = $this->_customerFactory->create()
            ->setWebsiteId($webSiteId)
            ->setStoreId($storeId)
            ->setFirstname($firstname)
            ->setLastname($lastname)
            ->setEmail($email)
            ->setGroupId($customerGroupId)
            ->setPassword($email)
            ->save();

        $this->_addAddressToCustomer($customer, $address);

        return $customer;

    }

    /**
     * @param string $regionCode
     * @param string $countryCode
     * @return string|int regionId
     */
    protected function retrieveRegionId($regionCode, $countryCode)
    {
        if(empty($regionCode) || empty($countryCode))
            return '';

        $regionModel = $this->_regionFactory->create()->loadByCode(
            $regionCode,
            $countryCode
        );

        if($regionModel && $regionModel->getId() > 0)
            return $regionModel->getId();
    }

    /**
     * @param \Magento\Customer\Model\Customer $customer
     * @param $address
     */
    protected function _addAddressToCustomer($customer, $address)
    {
        /** @var \Magento\Customer\Model\Address $addressModel */
        $addressModel = $this->_addressFactory->create();
        $addressModel->addData($address);
        $addressModel->setCustomerId($customer->getId());
        $addressModel->setIsDefaultBilling('1');
        $addressModel->setIsDefaultShipping('1');
        $addressModel->setSaveInAddressBook('1');
        $addressModel->save();
        $addressModel->reindex();
    }

}