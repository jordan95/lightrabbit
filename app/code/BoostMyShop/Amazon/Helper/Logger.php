<?php namespace BoostMyShop\Amazon\Helper;

/**
 * Class Logger
 *
 * @package   BoostMyShop\Amazon\Helper
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Logger {

    const kLogGeneral = 'general';
    const kLogError = 'error';
    const kLogRequest = 'request';
    const kLogResponse = 'response';

    /**
     * @var \Magento\Framework\Filesystem\DirectoryList
     */
    protected $_directoryList;

    /**
     * Logger constructor.
     * @param \Zend\Log\Logger $logger
     * @param \Magento\Framework\Filesystem\DirectoryList $directoryList
     */
    public function __construct(
        \Magento\Framework\Filesystem\DirectoryList $directoryList
    ){
        $this->_directoryList = $directoryList;
    }

    /**
     * @param string $msg
     * @param string $type
     */
    public function log($msg, $type = self::kLogGeneral)
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/bms_amazon_'.$type.'.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info($msg);
    }

    /**
     * @param string $filename
     * @param string $content
     * @throws \Exception
     */
    public function appendFile($filename, $content){

        $filename = $this->getDirname().$filename;

        if (!file_exists(dirname($filename))) {
            if (!@mkdir(dirname($filename), 0777, true)) {
                throw new \Exception('Can\'t create dir ' . dirname($filename));
            }
        }

        file_put_contents($filename, $content);
        chmod($filename, 0777);

    }

    /**
     * @param \BoostMyShop\Amazon\Model\Batch $batch
     * @return bool|int
     */
    public function deleteBatchFiles(\BoostMyShop\Amazon\Model\Batch $batch)
    {
        $dir = $this->getDirname().$batch->getOperationType().'/'.$batch->getBatchId();
        if(file_exists($dir))
            return $this->_delTree($dir);
        return 0;
    }

    /**
     * @param string $dir
     * @return bool
     */
    protected function _delTree($dir)
    {
        $files = array_diff(scandir($dir), array('.', '..'));
        foreach ($files as $file) {
            (is_dir("$dir/$file")) ? $this->_delTree("$dir/$file") : unlink("$dir/$file");
        }
        return rmdir($dir);
    }

    /**
     * @return string
     */
    public function getDirname(){

        return $this->_directoryList->getPath(\Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR).'/boostmyshop/amazon/';

    }

    /**
     * @param string $operation
     * @param array $params
     * @return string
     */
    public function getFilenameFromOperationParams($operation, $params){

        if($operation == 'listOrderItems'){

            return $params[0];

        }

        return self::kLogRequest;

    }

}