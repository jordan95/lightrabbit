<?php namespace BoostMyShop\Amazon\Helper;

/**
 * Class Tax
 *
 * @package   BoostMyShop\Amazon\Helper
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Tax extends \Magento\Framework\DataObject {

    /**
     * @var \Magento\Framework\DataObjectFactory
     */
    protected $_requestFactory;

    /**
     * @var \Magento\Store\Model\StoreFactory
     */
    protected $_storeFactory;

    /**
     * @var \Magento\Tax\Model\CalculationFactory
     */
    protected $_taxCalculationFactory;

    /**
     * Tax constructor.
     * @param \Magento\Tax\Model\CalculationFactory $taxCalculationFactory
     * @param \Magento\Framework\DataObjectFactory $requestFactory
     * @param \Magento\Store\Model\StoreFactory $storeFactory
     */
    public function __construct(
        \Magento\Tax\Model\CalculationFactory $taxCalculationFactory,
        \Magento\Framework\DataObjectFactory $requestFactory,
        \Magento\Store\Model\StoreFactory $storeFactory
    ){
        $this->_requestFactory = $requestFactory;
        $this->_storeFactory = $storeFactory;
        $this->_taxCalculationFactory = $taxCalculationFactory;
    }

    /**
     * @return float
     */
    public function calculateProductTaxRate(){

        $store = $this->_storeFactory->create()->load($this->getStoreId());
        $taxRequest = $this->_requestFactory->create();
        $taxRequest->setCountryId($this->getCountryCode());
        $taxRequest->setRegionId($this->getRegion());
        $taxRequest->setPostcode($this->getPostCode());
        $taxRequest->setStore($store);
        $taxRequest->setCustomerClassId($this->getCustomerTaxClassId());
        $taxRequest->setProductClassId($this->getProductTaxClassId());

        return $this->_taxCalculationFactory->create()->getRate($taxRequest);

    }

}