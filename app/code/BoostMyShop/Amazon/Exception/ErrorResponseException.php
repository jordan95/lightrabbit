<?php namespace BoostMyShop\Amazon\Exception;

/**
 * Class ErrorResponseException
 *
 * @package   BoostMyShop\Amazon\Exception
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class ErrorResponseException extends \Exception {

    /**
     * @var string
     */
    protected $_errorType;

    /**
     * @var string
     */
    protected $_requestId;

    /**
     * @var string
     */
    protected $_errorCode;

    /**
     * ErrorResponseException constructor.
     * @param string $errorType
     * @param string $errorCode
     * @param string $errorMessage
     * @param string $requestId
     * @param int $code
     * @param \Exception|null $previous
     */
    public function __construct(
        $errorType = '',
        $errorCode = '',
        $errorMessage = '',
        $requestId = '',
        $code = 0,
        \Exception $previous = null
    ){
        parent::__construct($errorMessage, $code, $previous);
        $this->_errorType = $errorType;
        $this->_requestId = $requestId;
        $this->_errorCode = $errorCode;
    }

    /**
     * @return string
     */
    public function getRequestId(){

        return $this->_requestId;

    }

    /**
     * @return string
     */
    public function getErrorType(){

        return $this->_errorType;

    }

    /**
     * @return string
     */
    public function getErrorCode(){

        return $this->_errorCode;

    }

}