<?php namespace BoostMyShop\Amazon\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface {

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context){

        $setup->startSetup();
        $this->insertAccountTypes($setup);
        $this->insertCountries($setup);
        $setup->endSetup();

    }

    protected function insertAccountTypes($setup){

        $setup->getConnection()->insertMultiple($setup->getTable('bms_amazon_account_type'),
            [
                [
                    'code' => 'EU',
                    'active' => 1
                ],
                [
                    'code' => 'NA',
                    'active' => 1
                ],
                [
                    'code' => 'JP',
                    'active' => 0
                ],
                [
                    'code' => 'IN',
                    'active' => 0
                ],
                [
                    'code' => 'CN',
                    'active' => 0
                ]
            ]
        );

    }

    protected function insertCountries($setup){

        $sql = 'SELECT id FROM '.$setup->getTable('bms_amazon_account_type').' WHERE code = "EU"';
        $res = $setup->getConnection()->query($sql)->fetchAll();
        $euAccountTypeId = $res[0]['id'];

        $sql = 'SELECT id FROM '.$setup->getTable('bms_amazon_account_type').' WHERE code = "NA"';
        $res = $setup->getConnection()->query($sql)->fetchAll();
        $usAccountTypeId = $res[0]['id'];

        $sql = 'SELECT id FROM '.$setup->getTable('bms_amazon_account_type').' WHERE code = "JP"';
        $res = $setup->getConnection()->query($sql)->fetchAll();
        $jpAccountTypeId = $res[0]['id'];

        $sql = 'SELECT id FROM '.$setup->getTable('bms_amazon_account_type').' WHERE code = "IN"';
        $res = $setup->getConnection()->query($sql)->fetchAll();
        $inAccountTypeId = $res[0]['id'];

        $sql = 'SELECT id FROM '.$setup->getTable('bms_amazon_account_type').' WHERE code = "CN"';
        $res = $setup->getConnection()->query($sql)->fetchAll();
        $cnAccountTypeId = $res[0]['id'];

        /* @see https://docs.developer.amazonservices.com/en_US/dev_guide/DG_Endpoints.html */
        $setup->getConnection()->insertMultiple($setup->getTable('bms_amazon_country'),
            [
                [
                    'code' => 'FR',
                    'endpoint' => 'mws-eu.amazonservices.com',
                    'active' => 1,
                    'account_type_id' => $euAccountTypeId,
                    'marketplace_id' => 'A13V1IB3VIYZZH'
                ],
                [
                    'code' => 'IT',
                    'endpoint' => 'mws-eu.amazonservices.com',
                    'active' => 1,
                    'account_type_id' => $euAccountTypeId,
                    'marketplace_id' => 'APJ6JRA9NG5V4'
                ],
                [
                    'code' => 'DE',
                    'endpoint' => 'mws-eu.amazonservices.com',
                    'active' => 1,
                    'account_type_id' => $euAccountTypeId,
                    'marketplace_id' => 'A1PA6795UKMFR9'
                ],
                [
                    'code' => 'ES',
                    'endpoint' => 'mws-eu.amazonservices.com',
                    'active' => 1,
                    'account_type_id' => $euAccountTypeId,
                    'marketplace_id' => 'A1RKKUPIHCS9HS'
                ],
                [
                    'code' => 'GB',
                    'endpoint' => 'mws-eu.amazonservices.com',
                    'active' => 1,
                    'account_type_id' => $euAccountTypeId,
                    'marketplace_id' => 'A1F83G8C2ARO7P'
                ],
                [
                    'code' => 'US',
                    'endpoint' => 'mws.amazonservices.com',
                    'active' => 1,
                    'account_type_id' => $usAccountTypeId,
                    'marketplace_id' => 'ATVPDKIKX0DER'
                ],
                [
                    'code' => 'MX',
                    'endpoint' => 'mws.amazonservices.com',
                    'active' => 1,
                    'account_type_id' => $usAccountTypeId,
                    'marketplace_id' => 'A1AM78C64UM0Y8'
                ],
                [
                    'code' => 'CA',
                    'endpoint' => 'mws.amazonservices.com',
                    'active' => 1,
                    'account_type_id' => $usAccountTypeId,
                    'marketplace_id' => 'A2EUQ1WTGCTBG2'
                ],
                [
                    'code' => 'IN',
                    'endpoint' => 'mws.amazonservices.in',
                    'active' => 0,
                    'account_type_id' => $inAccountTypeId,
                    'marketplace_id' => 'A21TJRUUN4KGV'
                ],
                [
                    'code' => 'JP',
                    'endpoint' => 'mws.amazonservices.jp',
                    'active' => 0,
                    'account_type_id' => $jpAccountTypeId,
                    'marketplace_id' => 'A1VC38T7YXB528'
                ],
                [
                    'code' => 'CN',
                    'endpoint' => 'mws.amazonservices.com.cn',
                    'active' => 0,
                    'account_type_id' => $cnAccountTypeId,
                    'marketplace_id' => 'AAHKV2X7AFYLW'
                ]
            ]
        );

    }

}