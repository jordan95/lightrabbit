<?php namespace BoostMyShop\Amazon\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class UpgradeSchema
 *
 * @package   BoostMyShop\Amazon\Setup
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class UpgradeSchema implements UpgradeSchemaInterface {

    /**
     * @param \Magento\Framework\Setup\SchemaSetupInterface $setup
     * @param \Magento\Framework\Setup\ModuleContextInterface $context
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context){

        $setup->startSetup();

        if(version_compare($context->getVersion(), '0.0.2', '<')){

            $setup->getConnection()
                ->addIndex(
                    $setup->getTable('bms_amazon_account_type'),
                    $setup->getIdxName($setup->getTable('bms_amazon_account_type'), ['code']),
                    ['code'],
                    \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
                );

            $setup->getConnection()
                ->addIndex(
                    $setup->getTable('bms_amazon_country'),
                    $setup->getIdxName($setup->getTable('bms_amazon_country'), ['code']),
                    ['code'],
                    \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
                );

            $accountCountryTable = $setup->getConnection()
                ->newTable($setup->getTable('bms_amazon_account_country'))
                ->addColumn('id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Id')
                ->addColumn('account_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['unsigned' => true, 'nullable' => false], 'Account Id')
                ->addColumn('country_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['unsigned' => true, 'nullable' => false], 'Country Id')
                ->addColumn('suspended_listings', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 1, ['unsigned' => true, 'nullable' => false], 'Suspended Listing')
                ->setComment('Account country');
            $setup->getConnection()->createTable($accountCountryTable);

            $setup->getConnection()
                ->addIndex(
                    $setup->getTable('bms_amazon_account_country'),
                    $setup->getIdxName($setup->getTable('bms_amazon_account_country'), ['account_id', 'country_id']),
                    ['account_id', 'country_id'],
                    \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
                );

            $setup->getConnection()->addForeignKey(
                'FK_bms_amazon_account_country_REF_bms_amazon_account',
                $setup->getTable('bms_amazon_account_country'),
                'account_id',
                $setup->getTable('bms_amazon_account'),
                'id',
                \Magento\Framework\DB\Adapter\AdapterInterface::FK_ACTION_CASCADE
            );

            $setup->getConnection()->addForeignKey(
                'FK_bms_amazon_account_country_REF_bms_amazon_country',
                $setup->getTable('bms_amazon_account_country'),
                'country_id',
                $setup->getTable('bms_amazon_country'),
                'id',
                \Magento\Framework\DB\Adapter\AdapterInterface::FK_ACTION_NO_ACTION
            );

        }

        if(version_compare($context->getVersion(), '0.0.3', '<')){

            $accountCountryOption = $setup->getConnection()
                ->newTable($setup->getTable('bms_amazon_country_option'))
                ->addColumn('id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Id')
                ->addColumn('option', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 50, ['nullable' => false], 'Option Name')
                ->addColumn('value', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 50, ['nullable' => false], 'Option Value')
                ->addColumn('created_at', \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME, null, ['nullable' => false], 'Created At')
                ->addColumn('updated_at', \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME, null, ['nullable' => false], 'Updated At')
                ->addColumn('account_country_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['unsigned' => true, 'nullable' => false], 'Account Country Id')
                ->setComment('Country options');

            $setup->getConnection()->createTable($accountCountryOption);

            $setup->getConnection()->addForeignKey(
                'FK_bms_amazon_country_option_REF_bms_amazon_account_country',
                $setup->getTable('bms_amazon_country_option'),
                'account_country_id',
                $setup->getTable('bms_amazon_account_country'),
                'id',
                \Magento\Framework\DB\Adapter\AdapterInterface::FK_ACTION_CASCADE
            );

            $setup->getConnection()
                ->addIndex(
                    $setup->getTable('bms_amazon_country_option'),
                    $setup->getIdxName($setup->getTable('bms_amazon_country_option'), ['account_country_id', 'option']),
                    ['account_country_id', 'option'],
                    \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
                );

        }

        if(version_compare($context->getVersion(), '0.0.4', '<')){

            $orderTable = $setup->getConnection()
                ->newTable($setup->getTable('bms_amazon_order'))
                ->addColumn('id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Id')
                ->addColumn('amazon_order_id', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 50, ['nullale' => false], 'Amazon Order Id')
                ->addColumn('order_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['unsigned' => true, 'nullable' => false], 'Magento Order Id')
                ->addColumn('account_country_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['unsigned' => true, 'nullable' => false], 'Account Country Id')
                ->setComment('Imported Orders');

            $setup->getConnection()->createTable($orderTable);

            $setup->getConnection()->addForeignKey(
                'FK_bms_amazon_order_REF_sales_order',
                $setup->getTable('bms_amazon_order'),
                'order_id',
                $setup->getTable('sales_order'),
                'entity_id',
                \Magento\Framework\DB\Adapter\AdapterInterface::FK_ACTION_NO_ACTION
            );

            $setup->getConnection()->addForeignKey(
                'FK_bms_amazon_order_REF_bms_amazon_account_country',
                $setup->getTable('bms_amazon_order'),
                'account_country_id',
                $setup->getTable('bms_amazon_account_country'),
                'id',
                \Magento\Framework\DB\Adapter\AdapterInterface::FK_ACTION_CASCADE
            );

            $setup->getConnection()
                ->addIndex(
                    $setup->getTable('bms_amazon_order'),
                    $setup->getIdxName($setup->getTable('bms_amazon_order'), ['amazon_order_id', 'order_id']),
                    ['amazon_order_id', 'order_id'],
                    \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
                );

            $batchTable = $setup->getConnection()
                ->newTable($setup->getTable('bms_amazon_batch'))
                ->addColumn('id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Id')
                ->addColumn('batch_id', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 50, ['nullable' => false], 'Batch Id')
                ->addColumn('operation_type', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 50, ['nullable' => false], 'Operation Type')
                ->addColumn('status', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 50, ['nullable' => false], 'Status')
                ->addColumn('created_at', \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME, null, ['nullable' => false], 'Created At')
                ->addColumn('account_country_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['unsigned' => true, 'nullable' => false], 'Account Country Id')
                ->setComment('Amazon Batch');

            $setup->getConnection()->createTable($batchTable);

            $batchLogTable = $setup->getConnection()
                ->newTable($setup->getTable('bms_amazon_batch_log'))
                ->addColumn('id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Id')
                ->addColumn('message', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 250, ['nullable' => false], 'Message')
                ->addColumn('level', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 50, ['nullable' => false], 'Level')
                ->addColumn('created_at', \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME, null, ['nullable' => false], 'Created At')
                ->addColumn('batch_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['unsigned' => true, 'nullable' => false], 'Batch Id')
                ->setComment('Amazon batch logs');

            $setup->getConnection()->createTable($batchLogTable);

            $setup->getConnection()->addForeignKey(
                'FK_bms_amazon_batch_REF_bms_amazon_account_country',
                $setup->getTable('bms_amazon_batch'),
                'account_country_id',
                $setup->getTable('bms_amazon_account_country'),
                'id',
                \Magento\Framework\DB\Adapter\AdapterInterface::FK_ACTION_CASCADE
            );

            $setup->getConnection()
                ->addIndex(
                    $setup->getTable('bms_amazon_batch'),
                    $setup->getIdxName($setup->getTable('batch_id'), ['batch_id']),
                    ['batch_id'],
                    \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
                );

            $setup->getConnection()->addForeignKey(
                'FK_bms_amazon_batch_log_REF_bms_amazon_batch',
                $setup->getTable('bms_amazon_batch_log'),
                'batch_id',
                $setup->getTable('bms_amazon_batch'),
                'id',
                \Magento\Framework\DB\Adapter\AdapterInterface::FK_ACTION_CASCADE
            );

        }

        if(version_compare($context->getVersion(), '0.0.5', '<')){

            $setup->getConnection()->addColumn(
                $setup->getTable('bms_amazon_batch'),
                'summary',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 500,
                    'comment' => 'Batch summary'
                ]

            );

        }

        if(version_compare($context->getVersion(), '0.0.7', '<')){

            $orderItemTable = $setup->getConnection()
                ->newTable($setup->getTable('bms_amazon_order_item'))
                ->addColumn('id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Id')
                ->addColumn('bms_amazon_order_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['nullable' => false, 'unsigned' => true], 'Bms Amazon Order Id')
                ->addColumn('amazon_order_item_id', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 50, ['nullable' => false], 'Amazon Order Item Id')
                ->addColumn('asin', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 50, ['nullable' => false], 'ASIN')
                ->addColumn('seller_sku', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 50, ['nullable' => false], 'Seller Sku')
                ->addColumn('title', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, ['nullable' => false], 'Title')
                ->setComment('Imported Order Items');

            $setup->getConnection()->createTable($orderItemTable);

            $setup->getConnection()->addForeignKey(
                'FK_bms_amazon_order_item_REF_bms_amazon_order',
                $setup->getTable('bms_amazon_order_item'),
                'bms_amazon_order_id',
                $setup->getTable('bms_amazon_order'),
                'id',
                \Magento\Framework\DB\Adapter\AdapterInterface::FK_ACTION_CASCADE
            );

        }

        if(version_compare($context->getVersion(), '0.0.8', '<')){

            $productTable =$setup->getConnection()
                ->newTable($setup->getTable('bms_amazon_product'))
                ->addColumn('id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Id')
                ->addColumn('product_id',  \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['unsigned' => true, 'nullable' => false], 'Product Id')
                ->addColumn('asin', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 50, [], 'ASIN')
                ->addColumn('disabled', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 1, ['nullable' => false, 'default' => 0], 'Disabled')
                ->addColumn('status', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 50,['nullable' => false, 'default' => 'not_associated'], 'Status')
                ->addColumn('error_message', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 500, ['nullable' => true], 'Error message')
                ->addColumn('created_at', \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME, null, ['nullable' => false], 'Created At')
                ->addColumn('updated_at',  \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME, null, ['nullable' => false], 'Updated At')
                ->addColumn('last_update', \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME, null, [], 'Last update')
                ->addColumn('account_country_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['unsigned' => true, 'nullable' => false], 'Account country Id')
                ->setComment('Amazon products');

            $setup->getConnection()->createTable($productTable);

            $setup->getConnection()->addForeignKey(
                'FK_bms_amazon_product_REF_catalog_product_entity',
                $setup->getTable('bms_amazon_product'),
                'product_id',
                $setup->getTable('catalog_product_entity'),
                'entity_id',
                \Magento\Framework\DB\Adapter\AdapterInterface::FK_ACTION_NO_ACTION
            );

            $setup->getConnection()->addForeignKey(
                'FK_bms_amazon_product_REF_bms_amazon_account_country',
                $setup->getTable('bms_amazon_product'),
                'account_country_id',
                $setup->getTable('bms_amazon_account_country'),
                'id',
                \Magento\Framework\DB\Adapter\AdapterInterface::FK_ACTION_CASCADE
            );

            $setup->getConnection()
                ->addIndex(
                    $setup->getTable('bms_amazon_product'),
                    $setup->getIdxName($setup->getTable('bms_amazon_product'), ['product_id', 'account_country_id']),
                    ['product_id', 'account_country_id'],
                    \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
                );


        }

        if(version_compare($context->getVersion(), '0.0.9', '<')){

            $setup->getConnection()->addColumn(
                $setup->getTable('bms_amazon_product'),
                'stock_status',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 50,
                    'comment' => 'Stock status'
                ]

            );

            $setup->getConnection()->addColumn(
                $setup->getTable('bms_amazon_product'),
                'price_status',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 50,
                    'comment' => 'Price status'
                ]

            );

        }

        if(version_compare($context->getVersion(), '1.0.5', '<')){

            $setup->getConnection()->dropForeignKey($setup->getTable('bms_amazon_product'), 'FK_bms_amazon_product_REF_catalog_product_entity');

            $setup->getConnection()->addForeignKey(
                'FK_bms_amazon_product_REF_catalog_product_entity',
                $setup->getTable('bms_amazon_product'),
                'product_id',
                $setup->getTable('catalog_product_entity'),
                'entity_id',
                \Magento\Framework\DB\Adapter\AdapterInterface::FK_ACTION_CASCADE
            );

        }

        $setup->endSetup();

    }

}