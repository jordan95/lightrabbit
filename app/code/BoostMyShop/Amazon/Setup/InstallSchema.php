<?php namespace BoostMyShop\Amazon\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class InstallSchema
 *
 * @package   BoostMyShop\Amazon\Setup
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class InstallSchema implements InstallSchemaInterface {

    /**
     * @var \BoostMyShop\Amazon\Helper\Xsd
     */
    protected $_xsdHelper;

    /**
     * UpgradeSchema constructor.
     * @param \BoostMyShop\Amazon\Helper\Xsd $xsdHelper
     */
    public function __construct(
        \BoostMyShop\Amazon\Helper\Xsd $xsdHelper
    ){
        $this->_xsdHelper = $xsdHelper;
    }

    /**
     * @param \Magento\Framework\Setup\SchemaSetupInterface $setup
     * @param \Magento\Framework\Setup\ModuleContextInterface $context
     * @throws \Zend_Db_Exception
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context){

        // download xsd files
        $this->_xsdHelper->update();

        $setup->startSetup();

        $accountTypeTable = $setup->getConnection()
            ->newTable($setup->getTable('bms_amazon_account_type'))
            ->addColumn('id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Id')
            ->addColumn('code', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 50, ['nullable' => false], 'Code')
            ->addColumn('active', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 1, ['nullable' => false], 'Is active ?')
            ->setComment('Amazon MWS account type');
        $setup->getConnection()->createTable($accountTypeTable);

        $accountTable = $setup->getConnection()
            ->newTable($setup->getTable('bms_amazon_account'))
            ->addColumn('id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Id')
            ->addColumn('name', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 50, [ 'unsigned' => true, 'nullable' => false, ], 'Account Name')
            ->addColumn('active', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 1, [ 'unsigned' => true, 'nullable' => true, 'default' => 1], 'Is Account Active')
            ->addColumn('merchant_id', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 80, ['nullable' => false], 'Merchant ID')
            ->addColumn('access_key_id', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 80, ['nullable' => false], 'Access Key ID')
            ->addColumn('secret_key', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 80, ['nullable' => false], 'Secret Key')
            ->addColumn('aws_auth_token', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 80, ['nullable' => true], 'AWS Auth Token')
            ->addColumn('created_at', \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME, null, ['nullable' => false], 'Created At')
            ->addColumn('updated_at', \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME, null, ['nullable' => false], 'Updated At')
            ->addColumn('account_type_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['unsigned' => true, 'nullable' => false], 'Account type id')
            ->setComment('Amazon account');
        $setup->getConnection()->createTable($accountTable);

        $countryTable = $setup->getConnection()
            ->newTable($setup->getTable('bms_amazon_country'))
            ->addColumn('id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'Id')
            ->addColumn('code', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 50, ['nullable' => false], 'Code')
            ->addColumn('endpoint', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 150, ['nullable' => false], 'Endpoint')
            ->addColumn('active', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 1, ['nullable' => false], 'Is active ?')
            ->addColumn('account_type_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['unsigned' => true, 'nullable' => false], 'Account type id')
            ->addColumn('marketplace_id', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 50, ['nullable' => false], 'Marketplace Id')
            ->setComment('Amazon country');
        $setup->getConnection()->createTable($countryTable);

        $setup->getConnection()->addForeignKey(
            'FK_bms_amazon_account_REF_bms_amazon_account_type',
            $setup->getTable('bms_amazon_account'),
            'account_type_id',
            $setup->getTable('bms_amazon_account_type'),
            'id',
            \Magento\Framework\DB\Adapter\AdapterInterface::FK_ACTION_NO_ACTION
        );

        $setup->getConnection()->addForeignKey(
            'FK_bms_amazon_country_REF_bms_amazon_account_type',
            $setup->getTable('bms_amazon_country'),
            'account_type_id',
            $setup->getTable('bms_amazon_account_type'),
            'id',
            \Magento\Framework\DB\Adapter\AdapterInterface::FK_ACTION_NO_ACTION
        );

        $setup->getConnection()
            ->addIndex(
                $setup->getTable('bms_amazon_country'),
                $setup->getIdxName($setup->getTable('marketplace_id'), ['marketplace_id']),
                ['marketplace_id'],
                \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
            );

        $setup->endSetup();

    }

}