<?php namespace BoostMyShop\Amazon\Block\Dashboard;

/**
 * Class Sales
 *
 * @package   BoostMyShop\Amazon\Block\Dashboard
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Sales extends Base
{

    /**
     * @var string
     */
    protected $_template = 'Dashboard/Sales.phtml';

    /**
     * @var \BoostMyShop\Amazon\Model\OrderFactory
     */
    protected $_orderFactory;

    /**
     * @var \Magento\Store\Model\StoreFactory
     */
    protected $_storeFactory;

    /**
     * Sales constructor.
     * @param \Magento\Store\Model\StoreFactory $storeFactory
     * @param \BoostMyShop\Amazon\Model\OrderFactory $orderFactory
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Store\Model\StoreFactory $storeFactory,
        \BoostMyShop\Amazon\Model\OrderFactory $orderFactory,
        \Magento\Framework\Registry $registry,
        \Magento\Backend\Block\Widget\Context $context,
        array $data = []
    ){
        parent::__construct($registry, $context, $data);
        $this->_orderFactory = $orderFactory;
        $this->_storeFactory = $storeFactory;
    }

    /**
     * @param string $startDate
     * @param string $endDate
     * @return int
     */
    protected function _countOrdersBetween($startDate, $endDate)
    {
        $currency = $this->_storeFactory->create()->getBaseCurrency()->getCurrencySymbol();
        $collection = $this->_orderFactory->create()->getAmazonOrdersCollection()
            ->addFieldToFilter('main_table.created_at', ['date' => true, 'from' => $startDate])
            ->addFieldToFilter('main_table.created_at', ['date' => true, 'to' => $endDate]);

        $accountCountryId = $this->getCurrentAccountCountryId();
        if (!empty($accountCountryId)) {
            $collection->addFieldToFilter('account_country_id', $accountCountryId);
        }

        $size = $collection->getSize();

        $collection->getSelect()->reset(\Magento\Framework\DB\Select::COLUMNS);
        $collection->getSelect()->columns(new \Zend_Db_Expr('IF(SUM(base_grand_total) != "", SUM(base_grand_total), 0) AS turnover'));

        return $size.' / '.$currency.round($collection->getFirstItem()->getData('turnover'), 2);

    }

    /**
     * @return int
     */
    public function countImportedOrdersToday()
    {

        $today = date('Y-m-d');
        $tomorrow = date('Y-m-d', time() + 3600 * 24);

        return $this->_countOrdersBetween($today, $tomorrow);

    }

    /**
     * @return int
     */
    public function countImportedOrdersYesterday()
    {

        $startDate = date('Y-m-d', time() - 3600 * 24);
        $endDate = date('Y-m-d');

        return $this->_countOrdersBetween($startDate, $endDate);

    }

    /**
     * @return int
     */
    public function countImportedOrdersTwoDaysAgo()
    {

        $startDate = date('Y-m-d', time() - 3600 * 48);
        $endDate = date('Y-m-d', time() - 3600 * 24);

        return $this->_countOrdersBetween($startDate, $endDate);

    }

    /**
     * @return int
     */
    public function countImportedOrdersThisWeek()
    {

        $startDate = date('Y-m-d', time() - 3600 * 24 * 7);
        $endDate = date('Y-m-d');

        return $this->_countOrdersBetween($startDate, $endDate);

    }

    /**
     * @return int
     */
    public function countImportedOrdersPreviousWeek()
    {

        $startDate = date('Y-m-d', time() - 3600 * 24 * 14);
        $endDate = date('Y-m-d', time() - 3600 * 24 * 7);

        return $this->_countOrdersBetween($startDate, $endDate);

    }

    /**
     * @return int
     */
    public function countImportedOrdersTwoWeeksAgo()
    {

        $startDate = date('Y-m-d', time() - 3600 * 24 * 21);
        $endDate = date('Y-m-d', time() - 3600 * 24 * 14);

        return $this->_countOrdersBetween($startDate, $endDate);

    }

    /**
     * @return int
     */
    public function countImportedOrdersThisMonth()
    {

        $startDate = date('Y-m-d', time() - 3600 * 24 * 30);
        $endDate = date('Y-m-d');

        return $this->_countOrdersBetween($startDate, $endDate);

    }

    /**
     * @return int
     */
    public function countImportedOrdersPreviousMonth()
    {

        $startDate = date('Y-m-d', time() - 3600 * 24 * 60);
        $endDate = date('Y-m-d', time() - 3600 * 24 * 30);

        return $this->_countOrdersBetween($startDate, $endDate);

    }

    /**
     * @return int
     */
    public function countImportedOrdersTwoMonthsAgo()
    {

        $startDate = date('Y-m-d', time() - 3600 * 24 * 90);
        $endDate = date('Y-m-d', time() - 3600 * 24 * 60);

        return $this->_countOrdersBetween($startDate, $endDate);

    }

}