<?php namespace BoostMyShop\Amazon\Block\Dashboard;

/**
 * Class Index
 *
 * @package   BoostMyShop\Amazon\Block\Dashboard
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Index extends Base {

    /**
     * @var string
     */
    protected $_template = 'Dashboard/Index.phtml';

}