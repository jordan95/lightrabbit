<?php namespace BoostMyShop\Amazon\Block\Dashboard;

/**
 * Class Orders
 *
 * @package   BoostMyShop\Amazon\Block\Dashboard
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Orders extends Base {

    /**
     * @var string
     */
    protected $_template = 'Dashboard/Orders.phtml';

    /**
     * @var \BoostMyShop\Amazon\Model\OrderFactory
     */
    protected $_orderFactory;

    /**
     * @var \BoostMyShop\Amazon\Model\ResourceModel\Batch\CollectionFactory
     */
    protected $_batchCollectionFactory;

    /**
     * @var \BoostMyShop\Amazon\Helper\Mws\Orders
     */
    protected $_mwsOrdersHelper;

    /**
     * @var \BoostMyShop\Amazon\Model\ResourceModel\Account\Country\CollectionFactory
     */
    protected $_countryCollectionFactory;

    /**
     * @var \BoostMyShop\Amazon\Helper\Cache
     */
    protected $_cache;

    /**
     * Orders constructor.
     * @param \BoostMyShop\Amazon\Helper\Cache $cache
     * @param \BoostMyShop\Amazon\Model\ResourceModel\Account\Country\CollectionFactory $countryCollectionFactory
     * @param \BoostMyShop\Amazon\Helper\Mws\Orders $mwsOrdersHelper
     * @param \BoostMyShop\Amazon\Model\ResourceModel\Batch\CollectionFactory $batchCollectionFactory
     * @param \BoostMyShop\Amazon\Model\OrderFactory $orderFactory
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param array $data
     */
    public function __construct(
        \BoostMyShop\Amazon\Helper\Cache $cache,
        \BoostMyShop\Amazon\Model\ResourceModel\Account\Country\CollectionFactory $countryCollectionFactory,
        \BoostMyShop\Amazon\Helper\Mws\Orders $mwsOrdersHelper,
        \BoostMyShop\Amazon\Model\ResourceModel\Batch\CollectionFactory $batchCollectionFactory,
        \BoostMyShop\Amazon\Model\OrderFactory $orderFactory,
        \Magento\Framework\Registry $registry,
        \Magento\Backend\Block\Widget\Context $context,
        array $data = []
    ){
        parent::__construct($registry, $context, $data);
        $this->_orderFactory = $orderFactory;
        $this->_batchCollectionFactory = $batchCollectionFactory;
        $this->_mwsOrdersHelper = $mwsOrdersHelper;
        $this->_countryCollectionFactory = $countryCollectionFactory;
        $this->_cache = $cache;
    }

    /**
     * @return string
     */
    public function getLastOrderImportedDate(){

        $collection = $this->_orderFactory->create()->getAmazonOrdersCollection();

        $accountCountryId = $this->getCurrentAccountCountryId();
        if(!empty($accountCountryId))
            $collection->addFieldToFilter('account_country_id', $accountCountryId);

        $collection->getSelect()->order('main_table.entity_id '.\Magento\Framework\DB\Select::SQL_DESC);
        $createdAt = $collection->getFirstItem()->getCreatedAt();
        return (!empty($createdAt)) ? $createdAt : '-';

    }

    /**
     * @return string
     */
    public function getLastOrderImportProcessRun(){

        $collection = $this->_batchCollectionFactory->create()
            ->addFieldToFilter('operation_type', \BoostMyShop\Amazon\Model\Batch::OPERATION_TYPE_ORDER);

        $accountCountryId = $this->getCurrentAccountCountryId();
        if(!empty($accountCountryId))
            $collection->addFieldToFilter('account_country_id', $accountCountryId);

        $collection->getSelect()->order('created_at '.\Magento\Framework\DB\Select::SQL_DESC);
        $createdAt = $collection->getFirstItem()->getCreatedAt();
        return (!empty($createdAt)) ? $createdAt : '-';

    }

    /**
     * @return int
     */
    public function getShipmentToConfirmCount(){

        $collection = $this->_countryCollectionFactory->create();

        $accountCountryId = $this->getCurrentAccountCountryId();
        if(!empty($accountCountryId))
            $collection->addFieldToFilter('main_table.id', $accountCountryId);

        $nbrOrdersToShip = 0;
        $statuses = [
            \BoostMyShop\Amazon\Lib\MWS\Orders::ORDER_STATUS_PARTIALLY_SHIPPED,
            \BoostMyShop\Amazon\Lib\MWS\Orders::ORDER_STATUS_UN_SHIPPED
        ];

        $createdAt = [
            'CreatedAfter' => date('Y-m-d\TH:i:s', time() - 3600 * 24 * 30)
        ];

        $xml = new \DOMDocument();

        foreach($collection as $country){

            $key = 'dashboard_'.$country->getId().'_'.$country->getmarketplace_id().'_ushipped';
            $nbrUnshipped = $this->_cache->load($key);
            if($nbrUnshipped === false) {
                $response = $this->_mwsOrdersHelper->reset()
                    ->setMerchantId($country->getAccount()->getMerchantId())
                    ->setSecretKey($country->getAccount()->getSecretKey())
                    ->setAccessKeyId($country->getAccount()->getAccessKeyId())
                    ->setBaseUrl($country->getendpoint())
                    ->setMarketplaceId($country->getmarketplace_id())
                    ->listOrders($createdAt, [], $statuses);

                $xml->loadXML($response->getBody());
                $nbrUnshipped = $xml->getElementsByTagName('Order')->length;
                $this->_cache->add($key, $nbrUnshipped);
            }

            $nbrOrdersToShip += $nbrUnshipped;

        }

        return $nbrOrdersToShip;

    }

}