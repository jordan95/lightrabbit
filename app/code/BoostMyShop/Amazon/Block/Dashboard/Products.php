<?php namespace BoostMyShop\Amazon\Block\Dashboard;

/**
 * Class Products
 *
 * @package   BoostMyShop\Amazon\Block\Dashboard
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Products extends Base {

    /**
     * @var string
     */
    protected $_template = 'Dashboard/Products.phtml';

    /**
     * @var \BoostMyShop\Amazon\Model\ResourceModel\Product\CollectionFactory
     */
    protected $_productCollectionFactory;

    /**
     * Products constructor.
     * @param \BoostMyShop\Amazon\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param array $data
     */
    public function __construct(
        \BoostMyShop\Amazon\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Framework\Registry $registry,
        \Magento\Backend\Block\Widget\Context $context,
        array $data = []
    ){
        parent::__construct($registry, $context, $data);
        $this->_productCollectionFactory = $productCollectionFactory;
    }

    /**
     * @return int
     */
    public function getListedProductsCount(){

        $collection = $this->_productCollectionFactory->create()
            ->addFieldToFilter(
                'status',
                ['in' =>
                    [
                        \BoostMyShop\Amazon\Model\Product::STATUS_DISABLED,
                        \BoostMyShop\Amazon\Model\Product::STATUS_ASSOCIATED
                    ]
                ]);

        $accountCountryId = $this->getCurrentAccountCountryId();
        if(!empty($accountCountryId))
            $collection->addFieldToFilter('account_country_id', $accountCountryId);

        return $collection->getSize();

    }

    /**
     * @return int
     */
    public function getErrorProductsCount(){

        $collection = $this->_productCollectionFactory->create()
            ->addFieldToFilter(
                'status',
                ['in' =>
                    [
                        \BoostMyShop\Amazon\Model\Product::STATUS_ERROR
                    ]
                ]);

        $accountCountryId = $this->getCurrentAccountCountryId();
        if(!empty($accountCountryId))
            $collection->addFieldToFilter('account_country_id', $accountCountryId);

        return $collection->getSize();

    }

}