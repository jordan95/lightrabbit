<?php namespace BoostMyShop\Amazon\Block\Dashboard;

/**
 * Class Errors
 *
 * @package   BoostMyShop\Amazon\Block\Dashboard
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Errors extends Base {

    /**
     * @var string
     */
    protected $_template = 'Dashboard/Errors.phtml';

    /**
     * @var \BoostMyShop\Amazon\Model\ResourceModel\Batch\Log\CollectionFactory
     */
    protected $_logCollectionFactory;

    /**
     * Errors constructor.
     * @param \BoostMyShop\Amazon\Model\ResourceModel\Batch\Log\CollectionFactory $logCollectionFactory
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param array $data
     */
    public function __construct(
        \BoostMyShop\Amazon\Model\ResourceModel\Batch\Log\CollectionFactory $logCollectionFactory,
        \Magento\Framework\Registry $registry,
        \Magento\Backend\Block\Widget\Context $context,
        array $data = []
    ){
        parent::__construct($registry, $context, $data);
        $this->_logCollectionFactory = $logCollectionFactory;
    }

    /**
     * @return \BoostMyShop\Amazon\Model\ResourceModel\Batch\Log\Collection
     */
    public function getErrors(){

        $collection = $this->_logCollectionFactory->create()
            ->addFieldToFilter('level', \BoostMyShop\Amazon\Model\Batch\Log::LEVEL_ERROR);

        $collection->join(
            ['b' => $collection->getResource()->getTable('bms_amazon_batch')],
            'b.id=main_table.batch_id',
            [
                'account_country_id',
                'operation_type'
            ]
        );

        $accountCountryId = $this->getCurrentAccountCountryId();
        if(!empty($accountCountryId))
            $collection->addFieldToFilter('account_country_id', $accountCountryId);

        $collection->getSelect()->limit(20);

        return $collection;
    }

    /**
     * @return \BoostMyShop\Amazon\Model\ResourceModel\Batch\Log\Collection
     */
    protected function _getErrorCollection(){

        $collection = $this->_logCollectionFactory->create()
            ->addFieldToFilter('level', \BoostMyShop\Amazon\Model\Batch\Log::LEVEL_ERROR);

        $collection->join(
            ['b' => $collection->getResource()->getTable('bms_amazon_batch')],
            'b.id=main_table.batch_id',
            [
                'account_country_id',
                'operation_type'
            ]
        );

        $accountCountryId = $this->getCurrentAccountCountryId();
        if(!empty($accountCountryId))
            $collection->addFieldToFilter('account_country_id', $accountCountryId);

        return $collection;

    }

    /**
     * @return int
     */
    public function getOrderImportationErrorCount(){

        return $this->_getErrorCollection()
            ->addFieldToFilter('operation_type', \BoostMyShop\Amazon\Model\Batch::OPERATION_TYPE_ORDER)
            ->getSize();

    }

    /**
     * @return int
     */
    public function getShipmentConfirmationErrorCount(){

        return $this->_getErrorCollection()
            ->addFieldToFilter('operation_type', \BoostMyShop\Amazon\Model\Batch::OPERATION_TYPE_SHIPMENT)
            ->getSize();

    }

    /**
     * @return int
     */
    public function getPriceErrorCount(){

        return $this->_getErrorCollection()
            ->addFieldToFilter(
                'operation_type',
                [
                    'in' => [
                        \BoostMyShop\Amazon\Model\Batch::OPERATION_TYPE_PRICE
                    ]
            ])
            ->getSize();

    }

    /**
     * @return int
     */
    public function getInventoryErrorCount(){

        return $this->_getErrorCollection()
            ->addFieldToFilter(
                'operation_type',
                [
                    'in' => [
                        \BoostMyShop\Amazon\Model\Batch::OPERATION_TYPE_INVENTORY
                    ]
                ])
            ->getSize();

    }

    /**
     * @return int
     */
    public function getProductsErrorCount(){

        return $this->_getErrorCollection()
            ->addFieldToFilter(
                'operation_type',
                [
                    'in' => [
                        \BoostMyShop\Amazon\Model\Batch::OPERATION_TYPE_MATCHING
                    ]
            ])
            ->getSize();

    }

    /**
     * @return string
     */
    public function  getOrderImportationErrorUrl(){

        $params = [
            'level' => \BoostMyShop\Amazon\Model\Batch\Log::LEVEL_ERROR,
            'operation_type' => \BoostMyShop\Amazon\Model\Batch::OPERATION_TYPE_ORDER,
            'account_country_id' => $this->getCurrentAccountCountryId()
        ];
        return $this->getUrl('amazon/log/index', $this->_getGridFilterParams($params));

    }

    /**
     * @return string
     */
    public function  getShipmentConfirmationErrorUrl(){

        $params = [
            'level' => \BoostMyShop\Amazon\Model\Batch\Log::LEVEL_ERROR,
            'operation_type' => \BoostMyShop\Amazon\Model\Batch::OPERATION_TYPE_SHIPMENT,
            'account_country_id' => $this->getCurrentAccountCountryId()
        ];
        return $this->getUrl('amazon/log/index', $this->_getGridFilterParams($params));

    }

    /**
     * @return string
     */
    public function  getPriceErrorUrl(){

        $params = [
            'level' => \BoostMyShop\Amazon\Model\Batch\Log::LEVEL_ERROR,
            'operation_type' => \BoostMyShop\Amazon\Model\Batch::OPERATION_TYPE_PRICE
            ,
            'account_country_id' => $this->getCurrentAccountCountryId()
        ];
        return $this->getUrl('amazon/log/index', $this->_getGridFilterParams($params));

    }

    /**
     * @return string
     */
    public function  getInventoryErrorUrl(){

        $params = [
            'level' => \BoostMyShop\Amazon\Model\Batch\Log::LEVEL_ERROR,
            'operation_type' => \BoostMyShop\Amazon\Model\Batch::OPERATION_TYPE_INVENTORY,
            'account_country_id' => $this->getCurrentAccountCountryId()
        ];
        return $this->getUrl('amazon/log/index', $this->_getGridFilterParams($params));

    }

    /**
     * @return string
     */
    public function  getProductsErrorUrl(){

        $params = [
            'level' => \BoostMyShop\Amazon\Model\Batch\Log::LEVEL_ERROR,
            'operation_type' => \BoostMyShop\Amazon\Model\Batch::OPERATION_TYPE_MATCHING,
            'account_country_id' => $this->getCurrentAccountCountryId()
        ];
        return $this->getUrl('amazon/log/index', $this->_getGridFilterParams($params));

    }

    /**
     * @param array $params
     * @return array
     */
    protected function _getGridFilterParams($params){

        $queryStringTab = [];
        foreach($params as $key => $value){

            $queryStringTab[] = urlencode($key).'='.urlencode($value);

        }

        return ['filter' => base64_encode(implode('&', $queryStringTab))];

    }

}