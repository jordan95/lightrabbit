<?php namespace BoostMyShop\Amazon\Block\Dashboard;

/**
 * Class Base
 *
 * @package   BoostMyShop\Amazon\Block\Dashboard
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
abstract class Base extends \Magento\Backend\Block\Widget\Container {

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_registry;

    /**
     * Base constructor.
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Registry $registry,
        \Magento\Backend\Block\Widget\Context $context,
        array $data = []
    ){
        parent::__construct($context, $data);
        $this->_registry = $registry;
    }

    /**
     * @return int
     */
    public function getCurrentAccountCountryId(){

        return $this->_registry->registry('account_country_id');

    }

}