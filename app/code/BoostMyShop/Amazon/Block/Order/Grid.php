<?php namespace BoostMyShop\Amazon\Block\Order;

/**
 * Class Grid
 *
 * @package   BoostMyShop\Amazon\Block\Order
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Grid extends \Magento\Backend\Block\Widget\Grid\Extended {

    /**
     * @var \BoostMyShop\Amazon\Model\OrderFactory
     */
    protected $_orderFactory;

    /**
     * @var \BoostMyShop\Amazon\Model\Account\CountryFactory
     */
    protected $_countryFactory;

    /**
     * @var \Magento\Sales\Model\Order\StatusFactory
     */
    protected $_orderStatusFactory;

    /**
     * Grid constructor.
     * @param \Magento\Sales\Model\Order\StatusFactory $orderStatusFactory
     * @param \BoostMyShop\Amazon\Model\Account\CountryFactory $countryFactory
     * @param \BoostMyShop\Amazon\Model\OrderFactory $orderFactory
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Sales\Model\Order\StatusFactory $orderStatusFactory,
        \BoostMyShop\Amazon\Model\Account\CountryFactory $countryFactory,
        \BoostMyShop\Amazon\Model\OrderFactory $orderFactory,
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        array $data = []
    ){
        $this->_orderFactory = $orderFactory;
        parent::__construct($context, $backendHelper, $data);
        $this->_countryFactory = $countryFactory;
        $this->_orderStatusFactory = $orderStatusFactory;
    }

    /**
     * Class constructor
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('bms_amazon_order_grid');
        $this->setDefaultSort('increment_id');
        $this->setDefaultDir('DESC');
        $this->setTitle(__('Orders'));
        $this->setUseAjax(true);
        $this->setSaveParametersInSession(true);
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = $this->_orderFactory->create()->getAmazonOrdersCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function _prepareColumns()
    {
        $this->addColumn('increment_id', ['header' => __('Increment id'), 'index' => 'increment_id']);
        $this->addColumn('store_id', ['header' => __('Purchase Point'), 'index' => 'store_id', 'type' => 'store']);
        $this->addColumn('created_at', ['header' => __('Created at'), 'index' => 'created_at', 'type' => 'datetime']);
        $this->addColumn('amazon_order_id', ['header' => __('Amazon order id'), 'index' => 'amazon_order_id']);
        $this->addColumn('ship_to_name', ['header' => __('Ship to name'), 'index' => 'order_id', 'renderer' => '\BoostMyShop\Amazon\Block\Order\Widget\Grid\Column\Renderer\ShipToName', 'filter' => false, 'sortable' => false]);
        $this->addColumn('grand_total', ['header' => __('Grand Total'), 'index' => 'grand_total', 'type' => 'range']);
        $this->addColumn('products', ['header' => __('Products'), 'index' => 'order_id', 'filter' => false, 'sortable' => false, 'renderer' => '\BoostMyShop\Amazon\Block\Order\Widget\Grid\Column\Renderer\Products']);
        $this->addColumn('status', ['header' => __('Status'), 'index' => 'status', 'type' => 'options', 'options' => $this->_getOrderStatuses()]);
        $this->addColumn('account_country_id', ['header' => __('Amazon country'), 'index' => 'account_country_id', 'type' => 'options', 'options' =>  $this->_getCountryOptions()]);
        $this->addColumn('action', ['header' => __('Action'), 'index' => 'order_id', 'filter' => false, 'sortable' => false, 'renderer' => '\BoostMyShop\Amazon\Block\Order\Widget\Grid\Column\Renderer\Option']);
        return parent::_prepareColumns();
    }

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('amazon/order/grid');
    }

    /**
     * @return array $options
     */
    public function _getCountryOptions(){

        $options = [];

        foreach($this->_countryFactory->create()->getCollection() as $item){

            $item = $this->_countryFactory->create()->load($item->getId());
            $options[$item->getId()] = $item->getLabel();

        }

        return $options;

    }

    /**
     * @return array $statuses
     */
    protected function _getOrderStatuses(){

        $statuses = [];

        foreach($this->_orderStatusFactory->create()->getCollection() as $status){

            $statuses[$status->getId()] = $status->getLabel();

        }

        return $statuses;

    }

}