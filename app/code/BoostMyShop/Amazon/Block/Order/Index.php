<?php namespace BoostMyShop\Amazon\Block\Order;

/**
 * Class Index
 *
 * @package   BoostMyShop\Amazon\Block\Order
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Index extends \Magento\Backend\Block\Widget\Container {

    /**
     * @var string
     */
    protected $_template = 'Order/Index.phtml';

    /**
     * @return \Magento\Backend\Block\Widget\Container
     */
    protected function _prepareLayout()
    {
        $addButtonProps = [
            'id' => 'import_orders',
            'label' => __('Check for new orders'),
            'class' => 'primary',
            'button_class' => '',
            'class_name' => 'Magento\Backend\Block\Widget\Button',
            'onclick' => 'setLocation("'.$this->getUrl('amazon/order/import').'")',
        ];
        $this->buttonList->add('add_new', $addButtonProps);

        return parent::_prepareLayout();
    }

}