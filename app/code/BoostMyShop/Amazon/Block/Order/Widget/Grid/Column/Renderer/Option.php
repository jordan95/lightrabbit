<?php namespace BoostMyShop\Amazon\Block\Order\Widget\Grid\Column\Renderer;

/**
 * Class Option
 *
 * @package   BoostMyShop\Amazon\Block\Order\Widget\Grid\Column\Renderer
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Option extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer {

    /**
     * @param \Magento\Framework\DataObject $row
     * @return string
     */
    public function render(\Magento\Framework\DataObject $row){

        return '<a href="'.$this->getUrl('sales/order/view', ['order_id' => $row->getOrderId()]).'">'.__('View').'</a>';

    }

}