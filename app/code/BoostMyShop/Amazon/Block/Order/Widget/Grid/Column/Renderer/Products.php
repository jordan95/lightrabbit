<?php namespace BoostMyShop\Amazon\Block\Order\Widget\Grid\Column\Renderer;

/**
 * Class Products
 *
 * @package   BoostMyShop\Amazon\Block\Order\Widget\Grid\Column\Renderer
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Products extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer {

    /**
     * @var \Magento\Sales\Model\Order
     */
    protected $_orderModel;

    /**
     * Products constructor.
     * @param \Magento\Sales\Model\Order $orderModel
     * @param \Magento\Backend\Block\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Sales\Model\Order $orderModel,
        \Magento\Backend\Block\Context $context,
        array $data = []
    ){
        parent::__construct($context, $data);
        $this->_orderModel = $orderModel;
    }

    /**
     * @param \Magento\Framework\DataObject $row
     * @return string
     */
    public function render(\Magento\Framework\DataObject $row){

        $items = [];

        foreach($this->_orderModel->load($row->getOrderId())->getAllItems() as $item){

            $items[] = $item->getName();

        }

        return implode('</br>', $items);

    }

}