<?php namespace BoostMyShop\Amazon\Block\Log;

/**
 * Class Index
 *
 * @package   BoostMyShop\Amazon\Block\Log
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Index extends \Magento\Backend\Block\Widget\Container {

    /**
     * @var string
     */
    protected $_template = 'Log/Index.phtml';

}