<?php namespace BoostMyShop\Amazon\Block\Log;

/**
 * Class Grid
 *
 * @package   BoostMyShop\Amazon\Block\Log
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Grid extends \Magento\Backend\Block\Widget\Grid\Extended {

    /**
     * @var \BoostMyShop\Amazon\Model\Batch\LogFactory
     */
    protected $_logFactory;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * @var \BoostMyShop\Amazon\Model\BatchFactory
     */
    protected $_batchFactory;

    /**
     * @var \BoostMyShop\Amazon\Model\Account\CountryFactory
     */
    protected $_countryFactory;

    /**
     * Grid constructor.
     * @param \BoostMyShop\Amazon\Model\Account\CountryFactory $countryFactory
     * @param \BoostMyShop\Amazon\Model\BatchFactory $batchFactory
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \BoostMyShop\Amazon\Model\Batch\LogFactory $logFactory
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param array $data
     */
    public function __construct(
        \BoostMyShop\Amazon\Model\Account\CountryFactory $countryFactory,
        \BoostMyShop\Amazon\Model\BatchFactory $batchFactory,
        \Magento\Framework\Registry $coreRegistry,
        \BoostMyShop\Amazon\Model\Batch\LogFactory $logFactory,
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        array $data = []
    ){
        $this->_logFactory = $logFactory;
        $this->_coreRegistry = $coreRegistry;
        $this->_batchFactory = $batchFactory;
        $this->_countryFactory = $countryFactory;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * Class constructor
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('bms_amazon_log_grid');
        $this->setDefaultSort('created_at');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setTitle(__('Log'));
        $this->setUseAjax(true);
    }

    /**
     * @return null|\BoostMyShop\Amazon\Model\Batch
     */
    public function getBatch(){

        return $this->_coreRegistry->registry('bms_amazon_current_batch');

    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = $this->_logFactory->create()->getCollection();
        $collection->join(
            ['b' => $collection->getResource()->getTable('bms_amazon_batch')],
            'b.id=main_table.batch_id',
            [
                'operation_type',
                'account_country_id'
            ]
        );
        if(!is_null($this->getBatch()))
            $collection->addFieldToFilter('main_table.batch_id', $this->getBatch()->getId());

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * @return $this
     * @throws \Exception
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareColumns()
    {

        $this->addColumn('message', ['header' => __('Message'), 'index' => 'message']);
        $this->addColumn('level', ['header' => __('Level'), 'index' => 'level', 'type' => 'options', 'options' => $this->_logFactory->create()->getLevels()]);
        $this->addColumn('created_at', ['header' => __('Created At'), 'index' => 'created_at', 'type' => 'datetime']);
        $this->addColumn('account_country_id', ['header' => __('Country'), 'index' => 'account_country_id', 'type' => 'options', 'options' => $this->_getCountryOptions()]);
        $this->addColumn('operation_type', ['header' => __('Operation Type'), 'index' => 'operation_type', 'type' => 'options', 'options' => $this->_batchFactory->create()->getOperationTypes()]);
        if(is_null($this->getBatch()))
            $this->addColumn('batch_id', ['header' => __('Batch Id'), 'index' => 'batch_id', 'type' => 'range']);

        $this->_eventManager->dispatch('bms_amazon_log_grid', ['grid' => $this, 'layout' => $this->getLayout()]);

        return parent::_prepareColumns();
    }

    /**
     * @return string
     */
    public function getGridUrl()
    {
        if(!is_null($this->getBatch())){

            return $this->getUrl('amazon/log/grid', ['batchId' => $this->getBatch()->getId()]);

        }

        return $this->getUrl('amazon/log/grid');

    }

    /**
     * @return array $options
     */
    public function _getCountryOptions(){

        $options = [];

        foreach($this->_countryFactory->create()->getCollection() as $item){

            $item = $this->_countryFactory->create()->load($item->getId());
            $options[$item->getId()] = $item->getLabel();

        }

        return $options;

    }

}