<?php namespace BoostMyShop\Amazon\Block\Product;

/**
 * Class Grid
 *
 * @package   BoostMyShop\Amazon\Block\Product
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Grid extends \Magento\Backend\Block\Widget\Grid\Extended {

    /**
     * @var \BoostMyShop\Amazon\Model\ResourceModel\Product\CollectionFactory
     */
    protected $_productCollectionFactory;

    /**
     * @var \BoostMyShop\Amazon\Model\ProductFactory
     */
    protected $_productFactory;

    /**
     * @var \BoostMyShop\Amazon\Model\Account\CountryFactory
     */
    protected $_countryFactory;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_registry;

    /**
     * @var \BoostMyShop\Amazon\Model\Account\Country
     */
    protected $_country;

    /**
     * Grid constructor.
     * @param \Magento\Framework\Registry $registry
     * @param \BoostMyShop\Amazon\Model\Account\CountryFactory $countryFactory
     * @param \BoostMyShop\Amazon\Model\ProductFactory $productFactory
     * @param \BoostMyShop\Amazon\Model\ResourceModel\Catalog\Product\CollectionFactory $productCollectionFactory
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Registry $registry,
        \BoostMyShop\Amazon\Model\Account\CountryFactory $countryFactory,
        \BoostMyShop\Amazon\Model\ProductFactory $productFactory,
        \BoostMyShop\Amazon\Model\ResourceModel\Catalog\Product\CollectionFactory $productCollectionFactory,
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        array $data = []
    ){
        parent::__construct($context, $backendHelper, $data);
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_productFactory = $productFactory;
        $this->_countryFactory = $countryFactory;
        $this->_registry = $registry;
    }

    /**
     * @return \BoostMyShop\Amazon\Model\Account\Country
     */
    public function getCountry(){

        if(is_null($this->_country)){
            $this->_country = $this->_countryFactory->create()->load($this->_registry->registry('account_country_id'));
        }

        return $this->_country;

    }

    /**
     * Class constructor
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('bms_amazon_product_grid');
        $this->setTitle(__('Products'));
        $this->setUseAjax(true);
        $this->setSaveParametersInSession(true);
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = $this->_productCollectionFactory->create();
        $collection->addAttributeToFilter('type_id', ['nin' => ['configurable', 'grouped', 'bundle']]);
        $collection->joinAmazonProducts($this->getCountry());
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function _prepareColumns()
    {
        $this->addColumn('entity_id', ['header' => __('Id'), 'index' => 'entity_id', 'type' => 'range']);
        $this->addColumn('last_update', ['header' => __('Last Update'), 'index' => 'last_update', 'type' =>'datetime']);
        $this->addColumn('image',
            [
                'header' => __('Image'),
                'index' => 'entity_id',
                'renderer' => '\BoostMyShop\Amazon\Block\Product\Widget\Grid\Column\Renderer\Image',
                'filter' => false,
                'sortable' => false
            ]
        );
        $this->addColumn('sku',
            [
                'header' => __('Sku'),
                'index' => 'sku',
                'renderer' => '\BoostMyShop\Amazon\Block\Product\Widget\Grid\Column\Renderer\Sku'
            ]
        );

        if($this->getCountry()->getOption('reference') != 'sku') {
            $this->addColumn('reference', ['header' => __('Reference'), 'index' => $this->getCountry()->getOption('reference')]);
        }
        $this->addColumn('barcode', ['header' => __('Barcode'), 'index' => $this->getCountry()->getOption('barcode')]);
        $this->addColumn('name', ['header' => __('Name'), 'index' => 'name']);
        $this->addColumn('asin',
            [
                'header' => __('ASIN'),
                'index' => 'asin',
                'renderer' => '\BoostMyShop\Amazon\Block\Product\Widget\Grid\Column\Renderer\Asin'
            ]
        );
        $this->addColumn('amazon_status',
            [
                'header' => __('Listing status'),
                'index' => 'amazon_status',
                'type' => 'options',
                'options' => array_merge(['' => ' '], $this->_productFactory->create()->getStatuses()),
                'renderer' => '\BoostMyShop\Amazon\Block\Product\Widget\Grid\Column\Renderer\Status'
            ]
        );
        $this->addColumn('stock_status',
            [
                'header' => __('Stock status'),
                'index' => 'stock_status',
                'type' => 'options',
                'options' => array_merge(['' => ' '], $this->_productFactory->create()->getOfferStatuses()),
                'status_type' => 'stock',
                'renderer' => '\BoostMyShop\Amazon\Block\Product\Widget\Grid\Column\Renderer\Offer\Status'
            ]
        );
        $this->addColumn('price_status',
            [
                'header' => __('Price status'),
                'index' => 'price_status',
                'type' => 'options',
                'options' => array_merge(['' => ' '], $this->_productFactory->create()->getOfferStatuses()),
                'status_type' => 'price',
                'renderer' => '\BoostMyShop\Amazon\Block\Product\Widget\Grid\Column\Renderer\Offer\Status'
            ]
        );
        $this->addColumn('offer',
            [
                'header' => __('Offer'),
                'index' => 'entity_id',
                'filter' => false,
                'sortable' => false,
                'renderer' => '\BoostMyShop\Amazon\Block\Product\Widget\Grid\Column\Renderer\Offer'
            ]
        );
        $this->addColumn('message', ['header' => __('Message'), 'index' => 'error_message']);

        return parent::_prepareColumns();
    }

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('amazon/product/grid', ['account_country_id' => $this->getCountry()->getId()]);
    }

    /**
     * @return array $options
     */
    public function _getCountryOptions(){

        $options = [];

        foreach($this->_countryFactory->create()->getCollection() as $item){

            $item = $this->_countryFactory->create()->load($item->getId());
            $options[$item->getId()] = $item->getLabel();

        }

        return $options;

    }

    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('e.entity_id');
        $this->getMassactionBlock()->setFormFieldName('amazon_products_mass_actions');

        $this->getMassactionBlock()->addItem(
            'disable',
            [
                'label' => __('Disable'),
                'url' => $this->getUrl('amazon/product/massDisable'),
                'confirm' => __('Are you sure ?')
            ]

        );

        $this->getMassactionBlock()->addItem(
            'enable',
            [
                'label' => __('Enable'),
                'url' => $this->getUrl('amazon/product/massEnable'),
                'confirm' => __('Are you sure ?')
            ]

        );

        $this->getMassactionBlock()->addItem(
            'list',
            [
                'label' => __('List'),
                'url' => $this->getUrl('amazon/product/massList'),
                'confirm' => __('Are you sure ?')
            ]

        );

        $this->getMassactionBlock()->addItem(
            'update',
            [
                'label' => __('Update'),
                'url' => $this->getUrl('amazon/product/massUpdate'),
                'confirm' => __('Are you sure ?')
            ]

        );

        return parent::_prepareMassaction();
    }

    /**
     * @param string $route
     * @param array $params
     * @return string
     */
    public function getUrl($route = '', $params = [])
    {
        $params = array_merge($params, ['account_country_id' => $this->getCountry()->getId()]);
        return parent::getUrl($route, $params);
    }

}