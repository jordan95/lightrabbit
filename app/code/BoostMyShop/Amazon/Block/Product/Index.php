<?php namespace BoostMyShop\Amazon\Block\Product;

/**
 * Class Index
 *
 * @package   BoostMyShop\Amazon\Block\Product
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Index extends \Magento\Backend\Block\Widget\Container {

    protected $_template = 'Product/Index.phtml';

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_registry;

    /***
     * Index constructor.
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Registry $registry,
        \Magento\Backend\Block\Widget\Context $context,
        array $data = []
    ){
        parent::__construct($context, $data);
        $this->_registry = $registry;
    }

    /**
     * @return \Magento\Backend\Block\Widget\Container
     */
    protected function _prepareLayout()
    {
        $addButtonProps = [
            'id' => 'sync_listing',
            'label' => __('Sync Listing'),
            'class' => 'primary',
            'button_class' => '',
            'class_name' => 'Magento\Backend\Block\Widget\Button',
            'onclick' => 'setLocation("'.$this->getUrl('amazon/product/import', ['account_country_id' => $this->_registry->registry('account_country_id')]).'")',
        ];
        $this->buttonList->add('sync_listing', $addButtonProps);

        return parent::_prepareLayout();
    }

}