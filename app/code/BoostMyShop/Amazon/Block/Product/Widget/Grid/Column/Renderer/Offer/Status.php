<?php namespace BoostMyShop\Amazon\Block\Product\Widget\Grid\Column\Renderer\Offer;

/**
 * Class Status
 *
 * @package   BoostMyShop\Amazon\Block\Product\Widget\Grid\Column\Renderer\Offer
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Status extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer {

    /**
     * @var \BoostMyShop\Amazon\Model\Product
     */
    protected $_amazonProductModel;

    /**
     * Status constructor.
     * @param \BoostMyShop\Amazon\Model\Product $amazonProductModel
     * @param \Magento\Backend\Block\Context $context
     * @param array $data
     */
    public function __construct(
        \BoostMyShop\Amazon\Model\Product $amazonProductModel,
        \Magento\Backend\Block\Context $context,
        array $data = []
    ){
        parent::__construct($context, $data);
        $this->_amazonProductModel = $amazonProductModel;
    }

    /**
     * @param \Magento\Framework\DataObject $row
     * @return string
     */
    public function render(\Magento\Framework\DataObject $row){

        $statuses = $this->_amazonProductModel->getOfferStatuses();
        $status_type = $this->getColumn()->getData('status_type');
        $field = $status_type.'_status';
        $status = $row->getData($field);

        if(
            $row->getAmazonStatus() == \BoostMyShop\Amazon\Model\Product::STATUS_ASSOCIATED
            && $status != \BoostMyShop\AMazon\Model\Product::STATUS_OFFER_ERROR
        ){

            $status = ($row->getUpdatedAt() < $row->getLastUpdate()) ? \BoostMyShop\Amazon\Model\Product::STATUS_OFFER_UP_TO_DATE : \BoostMyShop\Amazon\Model\Product::STATUS_OFFER_TO_UPDATE;

        }

        return (!empty($status)) ? $statuses[$status] : '-';

    }

}