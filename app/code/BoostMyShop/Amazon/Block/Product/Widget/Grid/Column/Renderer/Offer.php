<?php namespace BoostMyShop\Amazon\Block\Product\Widget\Grid\Column\Renderer;

/**
 * Class Offer
 *
 * @package   BoostMyShop\Amazon\Block\Product\Widget\Grid\Column\Renderer
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Offer extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer {

    /**
     * @var \BoostMyShop\Amazon\Helper\Offer\Stock
     */
    protected $_stockHelper;

    /**
     * @var \BoostMyShop\Amazon\Helper\Offer\Delay
     */
    protected $_delayHelper;

    /**
     * @var \BoostMyShop\Amazon\Helper\Offer\Price
     */
    protected $_priceHelper;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_registry;

    /**
     * @var \BoostMyShop\Amazon\Model\Account\CountryFactory
     */
    protected $_countryFactory;

    /**
     * Offer constructor.
     * @param \BoostMyShop\Amazon\Model\Account\CountryFactory $countryFactory
     * @param \Magento\Framework\Registry $registry
     * @param \BoostMyShop\Amazon\Helper\Offer\Stock $stockHelper
     * @param \BoostMyShop\Amazon\Helper\Offer\Delay $delayHelper
     * @param \BoostMyShop\Amazon\Helper\Offer\Price $priceHelper
     * @param \Magento\Backend\Block\Context $context
     * @param array $data
     */
    public function __construct(
        \BoostMyShop\Amazon\Model\Account\CountryFactory $countryFactory,
        \Magento\Framework\Registry $registry,
        \BoostMyShop\Amazon\Helper\Offer\Stock $stockHelper,
        \BoostMyShop\Amazon\Helper\Offer\Delay $delayHelper,
        \BoostMyShop\Amazon\Helper\Offer\Price $priceHelper,
        \Magento\Backend\Block\Context $context,
        array $data = []
    ){
        parent::__construct($context, $data);
        $this->_stockHelper = $stockHelper;
        $this->_delayHelper = $delayHelper;
        $this->_priceHelper = $priceHelper;
        $this->_registry = $registry;
        $this->_countryFactory = $countryFactory;
    }

    /**
     * @param \Magento\Framework\DataObject $row
     * @return string $html
     */
    public function render(\Magento\Framework\DataObject $row){

        $html = '';
        $country = $this->_countryFactory->create()->load($this->_registry->registry('account_country_id'));
        $stock = $this->_stockHelper->getStockToExport($row, $country);
        $delay = $this->_delayHelper->getDelayToExport($row, $country);
        $price = $this->_priceHelper->getPriceToExport($row, $country);

        $html .= __('Stock').' : '.$stock.'</br>';

        if(!isset($price['sale'])){

            $html .= __('Price').' : '.$this->_formatPrice($price['price'], $country).'</br>';

        }else{

            $html .= __('Price').' : '.$this->_formatPrice($price['sale']['price'], $country).' (<s>'.$this->_formatPrice($price['price'], $country).'</s>)</br>';

        }

        $html .= __('Delay').' : '.$delay.'</br>';

        return $html;

    }

    /**
     * @param float $price
     * @param \BoostMyShop\Amazon\Model\Account\Country $country
     * @return string
     */
    protected function _formatPrice($price, $country){

        return round($price, 2). ' '.$country->getCurrencyCode();

    }

}