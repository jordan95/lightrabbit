<?php namespace BoostMyShop\Amazon\Block\Product\Widget\Grid\Column\Renderer;

/**
 * Class Sku
 *
 * @package   BoostMyShop\Amazon\Block\Product\Widget\Grid\Column\Renderer
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Sku extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer {

    /**
     * @param \Magento\Framework\DataObject $row
     * @return string
     */
    public function render(\Magento\Framework\DataObject $row){

        return '<a href="'.$this->getUrl('catalog/product/edit', ['id' => $row->getentity_id()]).'">'.$row->getSku().'</a>';

    }

}