<?php namespace BoostMyShop\Amazon\Block\Product\Widget\Grid\Column\Renderer;

/**
 * Class Sku
 *
 * @package   BoostMyShop\Amazon\Block\Product\Widget\Grid\Column\Renderer
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Asin extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer {


    /**
     * @var \BoostMyShop\Amazon\Helper\Product
     */
    protected $_productHelper;

    /**
     * @var \BoostMyShop\Amazon\Model\Account\CountryFactory
     */
    protected $_countryFactory;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_registry;

    public function __construct(
        \BoostMyShop\Amazon\Helper\Product $productHelper,
        \BoostMyShop\Amazon\Model\Account\CountryFactory $countryFactory,
        \Magento\Framework\Registry $registry,
        \Magento\Backend\Block\Context $context,
        array $data = []
    ){
        parent::__construct($context, $data);
        $this->_productHelper = $productHelper;
        $this->_countryFactory = $countryFactory;
        $this->_registry = $registry;
    }

    /**
     * @param \Magento\Framework\DataObject $row
     * @return string
     */
    public function render(\Magento\Framework\DataObject $row){

        /** @var \BoostMyShop\Amazon\Model\Account\Country $country */
        $country = $this->_countryFactory->create()->load($this->_registry->registry('account_country_id'));
        $countryCode = $country->getCode();
        $asin = $row->getData('asin');
        $url = $this->_productHelper->getAmazonProductUri($countryCode, $asin);

        return '<a href="'.$url.'">'.$asin.'</a>';
    }

}