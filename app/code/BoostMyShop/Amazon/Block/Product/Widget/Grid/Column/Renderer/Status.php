<?php namespace BoostMyShop\Amazon\Block\Product\Widget\Grid\Column\Renderer;

/**
 * Class Status
 *
 * @package   BoostMyShop\Amazon\Block\Product\Widget\Grid\Column\Renderer
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Status extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer {

    /**
     * @var \BoostMyShop\Amazon\Model\Product
     */
    protected $_amazonProductModel;

    /**
     * Status constructor.
     * @param \BoostMyShop\Amazon\Model\Product $amazonProductModel
     * @param \Magento\Backend\Block\Context $context
     * @param array $data
     */
    public function __construct(
        \BoostMyShop\Amazon\Model\Product $amazonProductModel,
        \Magento\Backend\Block\Context $context,
        array $data = []
    ){
        parent::__construct($context, $data);
        $this->_amazonProductModel = $amazonProductModel;
    }

    /**
     * @param \Magento\Framework\DataObject $row
     * @return \Magento\Framework\Phrase|mixed
     */
    public function render(\Magento\Framework\DataObject $row){

        $statuses = $this->_amazonProductModel->getStatuses();

        $status = ($row->getData('amazon_status')) ? $row->getData('amazon_status') : \BoostMyShop\Amazon\Model\Product::STATUS_NOT_ASSOCIATED;

        switch($status){

            case \BoostMyShop\Amazon\Model\Product::STATUS_ASSOCIATED:
                $class = 'grid-severity-notice';
                break;
            case \BoostMyShop\Amazon\Model\Product::STATUS_DISABLED:
            case \BoostMyShop\Amazon\Model\Product::STATUS_PENDING:
            case \BoostMyShop\Amazon\Model\Product::STATUS_TO_PUSH:
                $class = 'grid-severity-warning';
                break;
            case \BoostMyShop\Amazon\Model\Product::STATUS_ERROR:
                $class = 'grid-severity-critical';
                break;
            case \BoostMyShop\Amazon\Model\Product::STATUS_NOT_FOUND:
            case \BoostMyShop\Amazon\Model\Product::STATUS_NOT_ASSOCIATED:
            default:
                $class = '';
                break;

        }

        return '<span class="'.$class.'"><span>'.__($statuses[$status]).'</span></span>';

    }

}