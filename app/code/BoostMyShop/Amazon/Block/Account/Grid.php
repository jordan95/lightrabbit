<?php namespace BoostMyShop\Amazon\Block\Account;

/**
 * Class Grid
 *
 * @package   BoostMyShop\Amazon\Block\Account
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Grid extends \Magento\Backend\Block\Widget\Grid\Extended {

    /**
     * @var \BoostMyShop\Amazon\Model\ResourceModel\Account\CollectionFactory
     */
    protected $_accountCollectionFactory;

    /**
     * Grid constructor.
     * @param \BoostMyShop\Amazon\Model\ResourceModel\Account\CollectionFactory $accountCollectionFactory
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param array $data
     */
    public function __construct(
        \BoostMyShop\Amazon\Model\ResourceModel\Account\CollectionFactory $accountCollectionFactory,
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        array $data = []
    ){
        parent::__construct($context, $backendHelper, $data);
        $this->_accountCollectionFactory = $accountCollectionFactory;
    }

    /**
     * Class constructor
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('bms_amazon_account_grid');
        $this->setDefaultSort('created_at');
        $this->setDefaultDir('DESC');
        $this->setTitle(__('Amazon Accounts'));
        $this->setUseAjax(true);
        $this->setSaveParametersInSession(true);
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = $this->_accountCollectionFactory->create();
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * @return $this
     * @throws \Exception
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareColumns()
    {

        $this->addColumn('name', ['header' => __('Name'), 'index' => 'name']);
        $this->addColumn('created_at', ['header' => __('Created At'), 'index' => 'created_at', 'type' => 'datetime']);
        $this->addColumn('updated_at', ['header' => __('Updated At'), 'index' => 'updated_at', 'type' => 'datetime']);
        $this->addColumn('active', ['header' => __('Active'), 'index' => 'active', 'type' => 'options', 'options' => [0 => __('No'), 1 => __('Yes')]]);

        $this->_eventManager->dispatch('bms_amazon_account_grid', ['grid' => $this, 'layout' => $this->getLayout()]);

        return parent::_prepareColumns();
    }

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('amazon/account/grid');
    }

    /**
     * @param \Magento\Catalog\Model\Product|\Magento\Framework\DataObject $item
     * @return string
     */
    public function getRowUrl($item){

        return $this->getUrl('amazon/account/edit', ['id' => $item->getid()]);

    }

}