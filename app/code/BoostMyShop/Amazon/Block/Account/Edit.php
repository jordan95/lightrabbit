<?php namespace BoostMyShop\Amazon\Block\Account;

/**
 * Class Edit
 *
 * @package   BoostMyShop\Amazon\Block\Account
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Edit extends \Magento\Backend\Block\Widget\Form\Container {

    public function _construct(){

        $this->_objectId = 'id';
        $this->_controller = 'Account';
        $this->_blockGroup = 'BoostMyShop_Amazon';

        parent::_construct();

    }

}