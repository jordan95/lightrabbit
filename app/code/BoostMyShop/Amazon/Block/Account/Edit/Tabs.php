<?php namespace BoostMyShop\Amazon\Block\Account\Edit;

/**
 * Class Tabs
 *
 * @package   BoostMyShop\Amazon\Block\Account\Edit
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs {

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * Tabs constructor.
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Json\EncoderInterface $jsonEncoder
     * @param \Magento\Backend\Model\Auth\Session $authSession
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Registry $registry,
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Backend\Model\Auth\Session $authSession,
        array $data = []
    )
    {
        parent::__construct($context, $jsonEncoder, $authSession, $data);
        $this->_coreRegistry = $registry;
        $this->setDestElementId('edit_form');
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    public function getTitle(){

        return __('Account');

    }

    /**
     * @return mixed
     */
    public function getAccount(){

        return $this->_coreRegistry->registry('bms_amazon_current_account');

    }

    /**
     * @return $this
     * @throws \Exception
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareLayout()
    {

        $this->addTab(
            'general',
            [
                'label' => __('General'),
                'content' => $this->getLayout()->createBlock('\BoostMyShop\Amazon\Block\Account\Edit\Tabs\General')->toHtml(),
                'active' => true
            ]
        );

        foreach($this->getAccount()->getMarketplaces() as $marketplace){

            $this->addTab(
                $marketplace->getId(),
                [
                    'label' => 'Amazon '.$marketplace->getcode(),
                    'content' => $this->getLayout()->createBlock('\BoostMyShop\Amazon\Block\Account\Edit\Tabs\Marketplace')->setMarketplace($marketplace)->toHtml(),
                    'active' => false
                ]
            );

        }

        $this->_eventManager->dispatch('bms_amazon_account_edit_tabs', ['account' => $this->getAccount(), 'tabs' => $this, 'layout' => $this->getLayout()]);

        return parent::_prepareLayout();
    }

}