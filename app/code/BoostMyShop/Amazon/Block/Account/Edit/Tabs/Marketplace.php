<?php namespace BoostMyShop\Amazon\Block\Account\Edit\Tabs;

/**
 * Class Marketplace
 *
 * @package   BoostMyShop\Amazon\Block\Account\Edit\Tabs
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Marketplace extends \Magento\Backend\Block\Widget\Form\Generic {

    /**
     * @var \BoostMyShop\Amazon\Model\Account\Country
     */
    protected $_marketplace;

    /**
     * @var \Magento\Sales\Model\Order\StatusFactory
     */
    protected $_orderStatusFactory;

    /**
     * @var \Magento\Eav\Model\Entity\TypeFactory
     */
    protected $_entityTypeFactory;

    /**
     * @var \Magento\Eav\Model\Entity\AttributeFactory
     */
    protected $_entityAttributeFactory;

    /**
     * @var \Magento\Customer\Model\GroupFactory
     */
    protected $_customerGroupFactory;

    /**
     * @var \Magento\Payment\Model\Config
     */
    protected $_paymentModelConfig;

    /**
     * @var \Magento\Shipping\Model\Config\Source\Allmethods
     */
    protected $_allShippingMethods;

    /**
     * @var \Magento\Tax\Model\TaxClass\Source\Customer
     */
    protected $_customerTaxClassModel;

    /**
     * @var \Magento\Backend\App\ConfigInterface
     */
    protected $_backendConfig;

    /**
     * Marketplace constructor.
     * @param \Magento\Backend\App\ConfigInterface $backendConfig
     * @param \Magento\Tax\Model\TaxClass\Source\Customer $customerTaxClassModel
     * @param \Magento\Shipping\Model\Config\Source\Allmethods $allShippingMethods
     * @param \Magento\Payment\Model\Config $paymentModelConfig
     * @param \Magento\Customer\Model\GroupFactory $customerGroupFactory
     * @param \Magento\Eav\Model\Entity\TypeFactory $entityTypeFactory
     * @param \Magento\Eav\Model\Entity\AttributeFactory $entityAttributeFactory
     * @param \Magento\Sales\Model\Order\StatusFactory $orderStatusFactory
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\App\ConfigInterface $backendConfig,
        \Magento\Tax\Model\TaxClass\Source\Customer $customerTaxClassModel,
        \Magento\Shipping\Model\Config\Source\Allmethods $allShippingMethods,
        \Magento\Payment\Model\Config $paymentModelConfig,
        \Magento\Customer\Model\GroupFactory $customerGroupFactory,
        \Magento\Eav\Model\Entity\TypeFactory $entityTypeFactory,
        \Magento\Eav\Model\Entity\AttributeFactory $entityAttributeFactory,
        \Magento\Sales\Model\Order\StatusFactory $orderStatusFactory,
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        array $data = []
    ){
        parent::__construct($context, $registry, $formFactory, $data);
        $this->_orderStatusFactory = $orderStatusFactory;
        $this->_entityTypeFactory = $entityTypeFactory;
        $this->_entityAttributeFactory = $entityAttributeFactory;
        $this->_customerGroupFactory = $customerGroupFactory;
        $this->_paymentModelConfig = $paymentModelConfig;
        $this->_allShippingMethods = $allShippingMethods;
        $this->_customerTaxClassModel = $customerTaxClassModel;
        $this->_backendConfig = $backendConfig;
    }

    /**
     * @param \BoostMyShop\Amazon\Model\Account\Country $marketplace
     * @return $this
     */
    public function setMarketplace(\BoostMyShop\Amazon\Model\Account\Country $marketplace){

        $this->_marketplace = $marketplace;
        return $this;

    }

    /**
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareForm()
    {

        $form = $this->_formFactory->create();

        $this->_prepareFieldSetMain($form);
        $this->_prepareFieldSetOrder($form);
        $this->_prepareFieldSetShipment($form);
        $this->_prepareFieldSetProduct($form);
        $this->_prepareFieldSetOffer($form);

        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * @param $form
     */
    protected function _prepareFieldSetMain($form)
    {

        $fieldSet = $form->addFieldset(
            'group_amazon_account_country_main',
            [
                'legend' => __('Main')
            ]
        );

        $fieldSet->addField(
            'reference',
            'select',
            [
                'name' => 'account[country]['.$this->_marketplace->getId().'][reference]',
                'value' => $this->_marketplace->getOption('reference'),
                'label' => __('Reference'),
                'required' => true,
                'options' => $this->_getProductAttributes(),
                'note' => __('Product reference to use (usually your sku)')
            ]

        );

        $fieldSet->addField(
            'store',
            'select',
            [
                'name' => 'account[country]['.$this->_marketplace->getId().'][store]',
                'value' => $this->_marketplace->getOption('store'),
                'label' => __('Store'),
                'required' => true,
                'options' => $this->_getStores()
            ]

        );

    }

    /**
     * @param $form
     */
    protected function _prepareFieldSetOrder($form)
    {

        $fieldSet = $form->addFieldset(
            'group-amazon-account-country-order',
            [
                'legend' => __('Order Import')
            ]
        );

        $fieldSet->addField(
            'order_importation_enabled',
            'select',
            [
                'name' => 'account[country]['.$this->_marketplace->getId().'][order_importation_enabled]',
                'value' => $this->_marketplace->getOption('order_importation_enabled'),
                'label' => __('Enabled'),
                'required' => true,
                'options' => [0 => __('No'), 1 => __('Yes')]
            ]

        );

        $fieldSet->addField(
            'order_importation_range',
            'select',
            [
                'name' => 'account[country]['.$this->_marketplace->getId().'][order_importation_range]',
                'value' => $this->_marketplace->getOption('order_importation_range'),
                'label' => __('Limit to order placed X days ago'),
                'required' => true,
                'options' => [3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9]
            ]

        );

        $fieldSet->addField(
            'customer_mode',
            'select',
            [
                'name' => 'account[country]['.$this->_marketplace->getId().'][customer_mode]',
                'value' => $this->_marketplace->getOption('customer_mode'),
                'label' => __('Customer Account'),
                'required' => true,
                'options' => ['create' => __('Create new customer'), 'guest' => __('Guest account')]
            ]

        );

        $fieldSet->addField(
            'customer_group',
            'select',
            [
                'name' => 'account[country]['.$this->_marketplace->getId().'][customer_group]',
                'value' => $this->_marketplace->getOption('customer_group'),
                'label' => __('Customer Group'),
                'required' => true,
                'options' => $this->_getCustomerGroups()
            ]

        );

        $fieldSet->addField(
            'customer_tax_class_id',
            'select',
            [
                'name' => 'account[country]['.$this->_marketplace->getId().'][customer_tax_class_id]',
                'value' => $this->_marketplace->getOption('customer_tax_class_id'),
                'label' => __('Customer Tax Class'),
                'required' => true,
                'options' => $this->_getCustomerTaxClasses()
            ]

        );

        $fieldSet->addField(
            'order_state',
            'select',
            [
                'name' => 'account[country]['.$this->_marketplace->getId().'][order_state]',
                'value' => $this->_marketplace->getOption('order_state'),
                'label' => __('Order Status'),
                'required' => true,
                'options' => $this->_getOrderStatuses()
            ]

        );

        $fieldSet->addField(
            'generate_invoice',
            'select',
            [
                'name' => 'account[country]['.$this->_marketplace->getId().'][generate_invoice]',
                'value' => $this->_marketplace->getOption('generate_invoice'),
                'label' => __('Create Invoice'),
                'required' => true,
                'options' => [0 => __('No'), 1 => __('Yes')]
            ]

        );

        $fieldSet->addField(
            'shipment_method',
            'select',
            [
                'name' => 'account[country]['.$this->_marketplace->getId().'][shipment_method]',
                'value' => $this->_marketplace->getOption('shipment_method'),
                'label' => __('Shipping Method'),
                'required' => true,
                'options' => $this->_getShipmentMethods()
            ]

        );

        $fieldSet->addField(
            'payment_method',
            'select',
            [
                'name' => 'account[country]['.$this->_marketplace->getId().'][payment_method]',
                'value' => $this->_marketplace->getOption('payment_method'),
                'label' => __('Payment Method'),
                'required' => true,
                'options' => $this->_getPaymentMethods()
            ]

        );

    }

    /**
     * @return array $statuses
     */
    protected function _getOrderStatuses(){

        $statuses = [];

        foreach($this->_orderStatusFactory->create()->getCollection() as $status){

            $statuses[$status->getId()] = $status->getLabel();

        }

        return $statuses;

    }

    /**
     * @return array $options
     */
    protected function _getProductAttributes($addEmpty = false){

        $options = [];

        if($addEmpty === true){
            $options[] = '';
        }

        $entityTypeId = $this->_entityTypeFactory->create()->loadByCode('catalog_product')->getId();
        $attributes = $this->_entityAttributeFactory->create()->getCollection()->setEntityTypeFilter($entityTypeId);

        foreach($attributes as $attribute){

            $options[$attribute->getAttributeCode()] = $attribute->getName();

        }

        return $options;

    }

    /**
     * @return array $stores
     */
    protected function _getStores(){

        $stores = [];

        foreach($this->_storeManager->getStores() as $store){

            $stores[$store->getId()] = $store->getName();

        }

        return $stores;

    }

    /**
     * @return array $groups
     */
    protected function _getCustomerGroups(){

        $groups = [];

        foreach($this->_customerGroupFactory->create()->getCollection() as $group){

            if(!empty($group->getCustomerGroupId()))
                $groups[$group->getCustomerGroupId()] = $group->getCustomerGroupCode();

        }

        return $groups;

    }

    /**
     * @return array $methods
     */
    protected function _getShipmentMethods(){

        $rates = [];

        foreach($this->_allShippingMethods->toOptionArray(true) as $method => $info){

            if(!empty($method)){

                foreach($info['value'] as $data){

                    $rates[$data['value']] = $data['label'];

                }

            }

        }

        return $rates;

    }

    /**
     * @return array $methods
     */
    protected function _getPaymentMethods(){

        $methods = [];

        foreach($this->_paymentModelConfig->getActiveMethods() as $paymentCode => $paymentModel){

            $paymentTitle = $this->_backendConfig->getValue('payment/'.$paymentCode.'/title');
            $methods[$paymentCode] = $paymentTitle;

        }

        return $methods;

    }

    /**
     * @return array $classes
     */
    protected function _getCustomerTaxClasses(){

        $classes = [];

        foreach($this->_customerTaxClassModel->toOptionArray() as $class){

            $classes[$class['value']] = $class['label'];

        }

        return $classes;

    }

    /**
     * @param $form
     */
    protected function _prepareFieldSetShipment($form){

        $fieldSet = $form->addFieldset(
            'group-amazon-account-country-shipment',
            [
                'legend' => __('Shipment Confirmation')
            ]
        );

        $fieldSet->addField(
            'shipment_confirmation_enabled',
            'select',
            [
                'name' => 'account[country]['.$this->_marketplace->getId().'][shipment_confirmation_enabled]',
                'value' => $this->_marketplace->getOption('shipment_confirmation_enabled'),
                'label' => __('Enabled'),
                'required' => true,
                'options' => [0 => __('No'), 1 => __('Yes')]
            ]

        );

    }

    /**
     * @param $form
     */
    protected function _prepareFieldSetProduct($form){

        $fieldSet = $form->addFieldset(
            'group-amazon-account-country-product',
            [
                'legend' => __('Product')
            ]
        );

        $fieldSet->addField(
            'barcode',
            'select',
            [
                'name' => 'account[country]['.$this->_marketplace->getId().'][barcode]',
                'value' => $this->_marketplace->getOption('barcode'),
                'label' => __('UPC / EAN Attribute'),
                'required' => true,
                'options' => $this->_getProductAttributes()
            ]

        );

    }

    /**
     * @param $form
     */
    protected function _prepareFieldSetOffer($form){

        $fieldSet = $form->addFieldset(
            'group-amazon-account-country-offer',
            [
                'legend' => __('Offer')
            ]
        );

        $fieldSet->addField(
            'offer_update_enabled',
            'select',
            [
                'name' => 'account[country]['.$this->_marketplace->getId().'][offer_update_enabled]',
                'value' => $this->_marketplace->getOption('offer_update_enabled'),
                'label' => __('Enabled'),
                'required' => true,
                'options' => [0 => __('No'), 1 => __('Yes')]
            ]

        );

        $fieldSet->addField(
            'stock_attribute',
            'select',
            [
                'name' => 'account[country]['.$this->_marketplace->getId().'][stock_attribute]',
                'value' => $this->_marketplace->getOption('stock_attribute'),
                'label' => __('Stock Attribute'),
                'required' => false,
                'options' => $this->_getProductAttributes(true)
            ]

        );

        $fieldSet->addField(
            'price_attribute',
            'select',
            [
                'name' => 'account[country]['.$this->_marketplace->getId().'][price_attribute]',
                'value' => $this->_marketplace->getOption('price_attribute'),
                'label' => __('Price Attribute'),
                'required' => false,
                'options' => $this->_getProductAttributes(true)
            ]

        );

        $fieldSet->addField(
            'use_special_price',
            'select',
            [
                'name' => 'account[country]['.$this->_marketplace->getId().'][use_special_price]',
                'value' => $this->_marketplace->getOption('use_special_price'),
                'label' => __('Use special price if exists'),
                'required' => true,
                'options' => [0 => __('No'), 1 => __('Yes')]
            ]

        );

        $fieldSet->addField(
            'add_tax',
            'select',
            [
                'name' => 'account[country]['.$this->_marketplace->getId().'][add_tax]',
                'value' => $this->_marketplace->getOption('add_tax'),
                'label' => __('Add tax'),
                'required' => true,
                'options' => [0 => __('No'), 1 => __('Yes')]
            ]

        );

        $fieldSet->addField(
            'price_coef',
            'text',
            [
                'name' => 'account[country]['.$this->_marketplace->getId().'][price_coef]',
                'value' => $this->_marketplace->getOption('price_coef'),
                'label' => __('Price coef'),
                'required' => false,
            ]

        );

        $fieldSet->addField(
            'delay_attribute',
            'select',
            [
                'name' => 'account[country]['.$this->_marketplace->getId().'][delay_attribute]',
                'value' => $this->_marketplace->getOption('delay_attribute'),
                'label' => __('Delay Attribute'),
                'required' => false,
                'options' => $this->_getProductAttributes(true)
            ]

        );

        $fieldSet->addField(
            'default_delay',
            'text',
            [
                'name' => 'account[country]['.$this->_marketplace->getId().'][default_delay]',
                'value' => ($this->_marketplace->getOption('default_delay')) ? $this->_marketplace->getOption('default_delay') : 2,
                'label' => __('Default Delay'),
                'required' => true,
            ]

        );

    }

}