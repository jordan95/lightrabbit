<?php namespace BoostMyShop\Amazon\Block\Account\Edit\Tabs;

/**
 * Class General
 *
 * @package   BoostMyShop\Amazon\Block\Account\Edit\Tabs
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class General extends \Magento\Backend\Block\Widget\Form\Generic {

    /**
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareForm()
    {

        $form = $this->_formFactory->create();

        $this->_prepareFieldSetMain($form);
        $this->_prepareFieldSetMws($form);

        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * @param $form
     */
    protected function _prepareFieldSetMain($form){

        $fieldSet = $form->addFieldset(
            'group-amazon-account-main',
            [
                'class' => 'user-defined',
                'legend' => __('Main'),
                'collapsable' => false
            ]
        );

        $fieldSet->addField(
            'id',
            'hidden',
            [
                'name' => 'account[id]',
                'value' => $this->_coreRegistry->registry('bms_amazon_current_account')->getId()
            ]
        );

        $fieldSet->addField(
            'name',
            'text',
            [
                'name' => 'account[name]',
                'value' => $this->_coreRegistry->registry('bms_amazon_current_account')->getName(),
                'label' => __('Name'),
                'required' => true
            ]

        );

        $fieldSet->addField(
            'active',
            'select',
            [
                'name' => 'account[active]',
                'value' => $this->_coreRegistry->registry('bms_amazon_current_account')->getActive(),
                'label' => __('Active'),
                'required' => true,
                'options' => [0 => __('No'), 1 => __('Yes')]
            ]
        );

    }

    /**
     * @param $form
     */
    protected function _prepareFieldSetMws($form){

        $fieldSet = $form->addFieldset(
            'group-amazon-account-mws',
            [
                'legend' => __('MWS Credentials'),
                'class' => 'user-defined',
                'collapsable' => false
            ]
        );

        $fieldSet->addField(
            'merchant_id',
            'text',
            array(
                'name' => 'account[merchant_id]',
                'value' => $this->_coreRegistry->registry('bms_amazon_current_account')->getMerchantId(),
                'label' => __('Merchant ID'),
                'required' => true
            )
        );

        $fieldSet->addField(
            'access_key_id',
            'text',
            array(
                'name' => 'account[access_key_id]',
                'value' => $this->_coreRegistry->registry('bms_amazon_current_account')->getAccessKeyId(),
                'label' => __('Access Key ID'),
                'required' => true
            )
        );

        $fieldSet->addField(
            'secret_key',
            'text',
            array(
                'name' => 'account[secret_key]',
                'value' => $this->_coreRegistry->registry('bms_amazon_current_account')->getSecretKey(),
                'label' => __('Secret Key'),
                'required' => true,
                'note' => __('Visit <a href="https://developer.amazonservices.com/" target="_blank">https://developer.amazonservices.com/</a> in order to get your credentials')
            )
        );

    }

}