<?php namespace BoostMyShop\Amazon\Block\Country;

/**
 * Class Switcher
 *
 * @package   BoostMyShop\Amazon\Block\Country
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Switcher extends \Magento\Backend\Block\Widget\Container {

    /**
     * @var string
     */
    protected $_template = 'Country/Switcher.phtml';

    /**
     * @var \BoostMyShop\Amazon\Model\ResourceModel\Account\Country\CollectionFactory
     */
    protected $_countryCollectionFactory;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_registry;

    /**
     * @var string
     */
    protected $_redirectUrl;

    /**
     * Switcher constructor.
     * @param \BoostMyShop\Amazon\Model\ResourceModel\Account\Country\CollectionFactory $countryCollectionFactory
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param array $data
     */
    public function __construct(
        \BoostMyShop\Amazon\Model\ResourceModel\Account\Country\CollectionFactory $countryCollectionFactory,
        \Magento\Framework\Registry $registry,
        \Magento\Backend\Block\Widget\Context $context,
        array $data = []
    ){
        parent::__construct($context, $data);
        $this->_countryCollectionFactory = $countryCollectionFactory;
        $this->_registry = $registry;
    }

    /**
     * @param string $url
     * @return $this
     */
    public function setRedirectUrl($url){

        $this->_redirectUrl = $this->getUrl($url);
        return $this;

    }

    /**
     * @return string
     */
    public function getRedirectUrl(){

        return $this->_redirectUrl;

    }

    /**
     * @return array
     */
    public function getCountries(){

        $countries = [];
        $collection = $this->_countryCollectionFactory->create();

        foreach($collection as $country){

            $account = $country->getAccount();
            if(!isset($countries[$account->getId()])) {
                $countries[$account->getId()] = [
                    'label' => $account->getName(),
                    'options' => []
                ];
            }

            $countries[$account->getId()]['options'][] = $country;

        }

        return $countries;

    }

    /**
     * @return int
     */
    public function getCurrentAccountCountryId(){

        return $this->_registry->registry('account_country_id');

    }

}