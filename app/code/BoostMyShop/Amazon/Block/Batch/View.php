<?php namespace BoostMyShop\Amazon\Block\Batch;

/**
 * Class View
 *
 * @package   BoostMyShop\Amazon\Block\Batch
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class View extends \Magento\Backend\Block\Widget\Container {

    /**
     * @var string
     */
    protected $_template = 'Batch/View.phtml';

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * View constructor.
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Backend\Block\Widget\Context $context,
        array $data = []
    ){
        parent::__construct($context, $data);
        $this->_coreRegistry = $coreRegistry;
    }

    /**
     * @return $this
     */
    protected function _prepareLayout()
    {

        $batch = $this->_coreRegistry->registry('bms_amazon_current_batch');

        $this->getToolbar()->addChild(
            'back_button',
            'Magento\Backend\Block\Widget\Button',
            [
                'label' => __('Back'),
                'title' => __('Back'),
                'onclick' => 'setLocation(\''.$this->getUrl('amazon/batch/index').'\')',
                'class' => 'action-back'
            ]
        );

        $this->getToolbar()->addChild(
            'delete_button',
            'Magento\Backend\Block\Widget\Button',
            [
                'label' => __('Delete'),
                'title' => __('Delete'),
                'class' => 'delete primary',
                'onclick' => 'setLocation(\''.$this->getUrl('amazon/batch/delete', ['id' => $batch->getId()]).'\')',
            ]
        );

        if($batch->getStatus() == \BoostMyShop\Amazon\Model\Batch::STATUS_PROCESSING){

            $this->getToolbar()->addChild(
                'update_button',
                'Magento\Backend\Block\Widget\Button',
                [
                    'label' => __('Update'),
                    'title' => __('Update'),
                    'class' => 'primary',
                    'onclick' => 'setLocation(\''.$this->getUrl('amazon/batch/update', ['id' => $batch->getId()]).'\')',
                ]
            );

        }

        return parent::_prepareLayout();
    }

}