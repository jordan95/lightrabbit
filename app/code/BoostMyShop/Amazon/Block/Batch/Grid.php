<?php namespace BoostMyShop\Amazon\Block\Batch;

/**
 * Class Grid
 *
 * @package   BoostMyShop\Amazon\Block\Batch
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Grid extends \Magento\Backend\Block\Widget\Grid\Extended {

    /**
     * @var \BoostMyShop\Amazon\Model\BatchFactory
     */
    protected $_batchFactory;

    /**
     * @var \BoostMyShop\Amazon\Model\CountryFactory
     */
    protected $_countryFactory;

    /**
     * Grid constructor.
     * @param \BoostMyShop\Amazon\Model\Account\CountryFactory $countryFactory
     * @param \BoostMyShop\Amazon\Model\BatchFactory $batchFactory
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param array $data
     */
    public function __construct(
        \BoostMyShop\Amazon\Model\Account\CountryFactory $countryFactory,
        \BoostMyShop\Amazon\Model\BatchFactory $batchFactory,
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        array $data = []
    ){
        parent::__construct($context, $backendHelper, $data);
        $this->_batchFactory = $batchFactory;
        $this->_countryFactory = $countryFactory;
    }

    /**
     * Class constructor
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('bms_amazon_batch_grid');
        $this->setDefaultSort('created_at');
        $this->setDefaultDir('DESC');
        $this->setTitle(__('Batch'));
        $this->setUseAjax(true);
        $this->setSaveParametersInSession(true);
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = $this->_batchFactory->create()->getCollection();
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * @return $this
     * @throws \Exception
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareColumns()
    {

        $this->addColumn('batch_id', ['header' => __('Batch Id'), 'index' => 'batch_id']);
        $this->addColumn('operation_type', ['header' => __('Operation Type'), 'index' => 'operation_type', 'type' => 'options', 'options' => $this->_batchFactory->create()->getOperationTypes()]);
        $this->addColumn('account_country_id', ['header' => __('Account Country Id'), 'index' => 'account_country_id', 'renderer' => '\BoostMyShop\Amazon\Block\Batch\Widget\Grid\Column\Renderer\Country', 'type' => 'options', 'options' => $this->_getCountryOptions()]);
        $this->addColumn('created_at', ['header' => __('Created At'), 'index' => 'created_at', 'type' => 'datetime']);
        $this->addColumn('status', ['header' => __('Status'), 'index' => 'status', 'type' => 'options', 'options' => $this->_batchFactory->create()->getStatuses(), 'renderer' => '\BoostMyShop\Amazon\Block\Batch\Widget\Grid\Column\Renderer\Status']);

        $this->_eventManager->dispatch('bms_amazon_batch_grid', ['grid' => $this, 'layout' => $this->getLayout()]);

        return parent::_prepareColumns();
    }

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('amazon/batch/grid');
    }

    /**
     * @param \Magento\Catalog\Model\Product|\Magento\Framework\DataObject $item
     * @return string
     */
    public function getRowUrl($item){

        return $this->getUrl('amazon/batch/view', ['id' => $item->getid()]);

    }

    /**
     * @return array $options
     */
    public function _getCountryOptions(){

        $options = [];

        foreach($this->_countryFactory->create()->getCollection() as $item){

            $item = $this->_countryFactory->create()->load($item->getId());
            $options[$item->getId()] = $item->getLabel();

        }

        return $options;

    }

}