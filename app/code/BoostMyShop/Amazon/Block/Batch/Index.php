<?php namespace BoostMyShop\Amazon\Block\Batch;

/**
 * Class Index
 *
 * @package   BoostMyShop\Amazon\Block\Batch
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Index extends \Magento\Backend\Block\Widget\Container {

    /**
     * @var string
     */
    protected $_template = 'Batch/Index.phtml';

}