<?php namespace BoostMyShop\Amazon\Block\Batch\Widget\Grid\Column\Renderer;

/**
 * Class Status
 *
 * @package   BoostMyShop\Amazon\Block\Batch\Widget\Grid\Column\Renderer
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Status extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer {

    /**
     * @param \Magento\Framework\DataObject $row
     * @return mixed
     */
    public function render(\Magento\Framework\DataObject $row){

        $status = $row->getStatus();

        switch($status){

            case \BoostMyShop\Amazon\Model\Batch::STATUS_SUCCESS:
                $class = 'grid-severity-notice';
                break;
            case \BoostMyShop\Amazon\Model\Batch::STATUS_ERROR:
                $class = 'grid-severity-critical';
                break;
            case \BoostMyShop\Amazon\Model\Batch::STATUS_NEW:
            case \BoostMyShop\Amazon\Model\Batch::STATUS_PROCESSING:
            default:
                $class = '';
                break;

        }

        return '<span class="'.$class.'"><span>'.__($status).'</span></span>';

    }

}