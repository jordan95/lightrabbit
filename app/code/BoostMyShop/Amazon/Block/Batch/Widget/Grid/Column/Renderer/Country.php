<?php namespace BoostMyShop\Amazon\Block\Batch\Widget\Grid\Column\Renderer;

/**
 * Class Country
 *
 * @package   BoostMyShop\Amazon\Block\Batch\Widget\Grid\Column\Renderer
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Country extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer {

    /**
     * @var \BoostMyShop\Amazon\Model\Account\CountryFactory
     */
    protected $_accountCountryFactory;

    /**
     * Country constructor.
     * @param \BoostMyShop\Amazon\Model\Account\CountryFactory $accountCountryFactory
     * @param \Magento\Backend\Block\Context $context
     * @param array $data
     */
    public function __construct(
        \BoostMyShop\Amazon\Model\Account\CountryFactory $accountCountryFactory,
        \Magento\Backend\Block\Context $context,
        array $data = []
    ){
        parent::__construct($context, $data);
        $this->_accountCountryFactory = $accountCountryFactory;
    }

    /**
     * @param \Magento\Framework\DataObject $row
     * @return string
     */
    public function render(\Magento\Framework\DataObject $row){

        return $this->_accountCountryFactory->create()->load($row->getAccountCountryId())->getLabel();

    }

}