<?php namespace BoostMyShop\Amazon\Block\Batch\View;

/**
 * Class Tabs
 *
 * @package   BoostMyShop\Amazon\Block\Batch\View
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs {

    /**
     * @var \BoostMyShop\Amazon\Model\Batch
     */
    protected $_batch;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * Tabs constructor.
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Json\EncoderInterface $jsonEncoder
     * @param \Magento\Backend\Model\Auth\Session $authSession
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Backend\Model\Auth\Session $authSession,
        array $data = []
    ){
        parent::__construct($context, $jsonEncoder, $authSession, $data);
        $this->_coreRegistry = $coreRegistry;
        $this->setDestElementId('bms_amazon_batch_tabs_content');
    }

    /**
     * @param \BoostMyShop\Amazon\Model\Batch $batch
     * @return $this
     */
    public function setBatch(\BoostMyShop\Amazon\Model\Batch $batch){

        $this->_batch = $batch;
        return $this;

    }

    /**
     * @return \BoostMyShop\Amazon\Model\Batch
     */
    public function getBatch(){

        return $this->_coreRegistry->registry('bms_amazon_current_batch');

    }

    /**
     * @return \Magento\Framework\Phrase
     */
    public function getTitle(){

        return __('Batch');

    }

    /**
     * @return $this
     * @throws \Exception
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareLayout()
    {

        $this->addTab(
            'info',
            [
                'label' => __('Information'),
                'content' => $this->getLayout()->createBlock('\BoostMyShop\Amazon\Block\Batch\View\Tabs\Info')->toHtml(),
                'active' => true
            ]
        );

        $this->addTab(
            'logs',
            [
                'label' => __('Logs'),
                'content' => $this->getLayout()->createBlock('\BoostMyShop\Amazon\Block\Log\Grid')->toHtml(),
            ]
        );

        $this->addTab(
            'files',
            [
                'label' => __('Files'),
                'content' => $this->getLayout()->createBlock('\BoostMyShop\Amazon\Block\Batch\View\Tabs\Files')->toHtml(),
            ]
        );

        $this->_eventManager->dispatch('bms_amazon_batch_view_tabs', ['batch' => $this->getBatch(), 'tabs' => $this, 'layout' => $this->getLayout()]);

        return parent::_prepareLayout();
    }

}