<?php namespace BoostMyShop\Amazon\Block\Batch\View\Tabs;

/**
 * Class Files
 *
 * @package   BoostMyShop\Amazon\Block\Batch\View\Tabs
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Files extends \Magento\Backend\Block\Template {

    /**
     * @var string
     */
    protected $_template = 'Batch/View/Tabs/Files.phtml';

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * Info constructor.
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Backend\Block\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Backend\Block\Template\Context $context,
        array $data = []
    ){
        parent::__construct($context, $data);
        $this->_coreRegistry = $coreRegistry;
    }

    /**
     * @return null|\BoostMyShop\Amazon\Model\Batch
     */
    public function getBatch(){

        return $this->_coreRegistry->registry('bms_amazon_current_batch');

    }

    /**
     * @return array $files
     */
    public function getFiles(){

        $files = [];

        foreach($this->getBatch()->getFiles() as $file){

            list($empty, $action, $params, $filename) = explode('/', $file);
            if(!isset($files[$action])){
                $files[$action] = [];
            }

            $files[$action][$file] = $filename;

        }

        return $files;

    }

}