<?php namespace BoostMyShop\Amazon\Block\Batch\View\Tabs;

/**
 * Class Info
 *
 * @package   BoostMyShop\Amazon\Block\Batch\View\Tabs
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Info extends \Magento\Backend\Block\Template {

    /**
     * @var string
     */
    protected $_template = 'Batch/View/Tabs/Info.phtml';

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * Info constructor.
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Backend\Block\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Backend\Block\Template\Context $context,
        array $data = []
    ){
        parent::__construct($context, $data);
        $this->_coreRegistry = $coreRegistry;
    }

    /**
     * @return null|\BoostMyShop\Amazon\Model\Batch
     */
    public function getBatch(){

        return $this->_coreRegistry->registry('bms_amazon_current_batch');

    }

}