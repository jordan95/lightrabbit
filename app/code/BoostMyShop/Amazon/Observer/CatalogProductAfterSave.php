<?php namespace BoostMyShop\Amazon\Observer;

/**
 * Class CatalogProductAfterSave
 *
 * @package   BoostMyShop\Amazon\Observer
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class CatalogProductAfterSave implements \Magento\Framework\Event\ObserverInterface {

    /**
     * Update updated_at field with sql query because updated_at is not updated on product save
     * @see Magento issue MAGETWO-60169
     *
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer){

        $product = $observer->getEvent()->getProduct();

        $tableName = $product->getResource()->getTable('catalog_product_entity');
        $updatedAt = date('Y-m-d H:i:s');

        $sql = 'UPDATE '.$tableName.' SET updated_at = "'.$updatedAt.'" WHERE entity_id = '.$product->getId();

        $product->getResource()->getConnection()->query($sql);

    }

}