<?php namespace BoostMyShop\Amazon\Model\Order;

/**
 * Class Item
 *
 * @package   BoostMyShop\Amazon\Model\Order
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Item extends \Magento\Framework\Model\AbstractModel {

    protected function _construct()
    {
        $this->_init('BoostMyShop\Amazon\Model\ResourceModel\Order\Item');
    }

}