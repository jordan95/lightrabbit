<?php namespace BoostMyShop\Amazon\Model\Order;

/**
 * Class Invoice
 *
 * @package   BoostMyShop\Amazon\Model\Order
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Invoice {

    /**
     * @var \Magento\Sales\Model\Service\InvoiceService
     */
    protected $_invoiceService;

    /**
     * @var \Magento\Framework\DB\Transaction
     */
    protected $_transaction;

    /**
     * Invoice constructor.
     * @param \Magento\Sales\Model\Service\InvoiceService $invoiceService
     * @param \Magento\Framework\DB\Transaction $transaction
     */
    public function __construct(
        \Magento\Sales\Model\Service\InvoiceService $invoiceService,
        \Magento\Framework\DB\Transaction $transaction
    ){
        $this->_invoiceService = $invoiceService;
        $this->_transaction = $transaction;
    }

    /**
     * @param $order
     * @return \Magento\Sales\Model\Order\Invoice
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws bool
     */
    public function createInvoice($order)
    {
        $invoiceItems = [];

        foreach($order->getAllItems() as $item)
            $invoiceItems[$item->getitem_id()] = $item->getQtyOrdered();

        $invoice = $this->_invoiceService->prepareInvoice($order, $invoiceItems);
        $invoice->register();
        $invoice->getOrder()->setIsInProcess(true);
        $transactionSave = $this->_transaction->addObject($invoice)->addObject($invoice->getOrder());
        $transactionSave->save();

        return $invoice;
    }

}