<?php namespace BoostMyShop\Amazon\Model\Country;

/**
 * Class Option
 *
 * @package   BoostMyShop\Amazon\Model\Country
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Option extends \Magento\Framework\Model\AbstractModel {

    /**
     * @return $this
     */
    public function beforeSave()
    {
        $date = time();

        if(!$this->getId()){

            $this->setCreatedAt($date)
                ->setUpdatedAt($date);

        }else{

            $this->setUpdatedAt($date);

        }

        return parent::beforeSave();
    }

    protected function _construct()
    {
        $this->_init('BoostMyShop\Amazon\Model\ResourceModel\Country\Option');
    }

    /**
     * @param int $accountCountryId
     * @param string $option
     * @param mixed $value
     * @return $this
     */
    public function addOrUpdateParam($accountCountryId, $option, $value){

        $item = $this->getCollection()->getOptionByAccountCountryId($accountCountryId, $option);
        if($item->getId()){

            $item->setValue($value)
                ->save();

        }else{

            $item->setAccountCountryId($accountCountryId)
                ->setOption($option)
                ->setValue($value)
                ->save();

        }

        return $this;

    }

}