<?php namespace BoostMyShop\Amazon\Model;

/**
 * Class Account
 *
 * @package   BoostMyShop\Amazon\Model
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Account extends \Magento\Framework\Model\AbstractModel {

    /**
     * @var \BoostMyShop\Amazon\Model\Country
     */
    protected $_country;

    /**
     * @var \BoostMyShop\Amazon\Helper\Logger
     */
    protected $_logger;

    /**
     * @var \BoostMyShop\Amazon\Model\Account\Type
     */
    protected $_accountType;

    /**
     * @var \BoostMyShop\Amazon\Model\Account\CountryFactory
     */
    protected $_accountCountryFactory;

    /**
     * @var array
     */
    protected $_marketplaces = [];

    /**
     * @var \BoostMyShop\Amazon\Helper\Mws\SellersFactory
     */
    protected $_mwsSellerFactory;

    /**
     * Account constructor.
     * @param \BoostMyShop\Amazon\Helper\Mws\SellersFactory $mwsSellerFactory
     * @param \BoostMyShop\Amazon\Model\Account\CountryFactory $accountCountryFactory
     * @param \BoostMyShop\Amazon\Model\Account\Type $accountType
     * @param \BoostMyShop\Amazon\Helper\Logger $logger
     * @param \BoostMyShop\Amazon\Model\Country $country
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        \BoostMyShop\Amazon\Helper\Mws\SellersFactory $mwsSellerFactory,
        \BoostMyShop\Amazon\Model\Account\CountryFactory $accountCountryFactory,
        \BoostMyShop\Amazon\Model\Account\Type $accountType,
        \BoostMyShop\Amazon\Helper\Logger $logger,
        \BoostMyShop\Amazon\Model\Country $country,
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ){
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->_country = $country;
        $this->_logger = $logger;
        $this->_accountType = $accountType;
        $this->_accountCountryFactory = $accountCountryFactory;
        $this->_mwsSellerFactory = $mwsSellerFactory;
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('BoostMyShop\Amazon\Model\ResourceModel\Account');
    }

    /**
     * @param string $countryCode
     * @param array $marketplace
     * @return $this
     */
    public function addMarketplace($countryCode, array $marketplace){

        $this->_marketplaces[$countryCode] = $marketplace;
        return $this;

    }

    /**
     * @return $this
     */
    public function beforeSave()
    {
        $date = time();

        if(!$this->getId()){

            $this->setCreatedAt($date)
                ->setUpdatedAt($date);

        }else{

            $this->setUpdatedAt($date);

        }

        return parent::beforeSave();
    }

    /**
     * @return $this
     * @throws \Exception
     * @throws \Magento\Framework\Validator\Exception
     */
    public function validateBeforeSave()
    {
        $this->_listMarketplaceParticipations();
        return parent::validateBeforeSave();
    }

    /**
     * @return int
     * @throws \Exception
     */
    protected function _listMarketplaceParticipations(){

        $response = null;

        foreach($this->_accountType->getActiveCollection() as $accountType){

            try {

                $country = $this->_country->load($accountType->getFirstActiveCountry()->getCode(), 'code');

                $seller = $this->_mwsSellerFactory->create()->reset();
                $response = $seller->setMerchantId($this->getMerchantId())
                    ->setAccessKeyId($this->getAccessKeyId())
                    ->setSecretKey($this->getSecretKey())
                    ->setBaseUrl($country->getendpoint())
                    ->listMarketplaceParticipations();

                if ($response->getStatusCode() == 200) {
                    $this->setAccountTypeId($accountType->getId());
                    break;
                }

            }catch(\Exception $e){

                $this->_logger->log($e->getMessage(), \BoostMyShop\Amazon\Helper\Logger::kLogError);

            }

        }

        if(!is_null($response) && $response->getStatusCode() == 200){

            $domDocument = new \DOMDocument();
            $domDocument->loadXML($response->getBody());
            if ($domDocument->getElementsByTagName('ListMarketplaces')->item(0)) {

                foreach ($domDocument->getElementsByTagName('ListMarketplaces')->item(0)->getElementsByTagName('Marketplace') as $marketplace) {

                    $isProdMarketplace = (preg_match('#^www#', trim($marketplace->getElementsByTagName('DomainName')->item(0)->nodeValue))) ? true : false;

                    if($isProdMarketplace === true) {

                        $countryCode = $marketplace->getElementsByTagName('DefaultCountryCode')->item(0)->nodeValue;

                        $marketplace = array(
                            'marketplaceId' => $marketplace->getElementsByTagName('MarketplaceId')->item(0)->nodeValue,
                            'name' => $marketplace->getElementsByTagName('Name')->item(0)->nodeValue,
                            'countryCode' => $countryCode,
                            'currencyCode' => $marketplace->getElementsByTagName('DefaultCurrencyCode')->item(0)->nodeValue,
                            'languageCode' => $marketplace->getElementsByTagName('DefaultLanguageCode')->item(0)->nodeValue,
                            'domain' => $marketplace->getElementsByTagName('DomainName')->item(0)->nodeValue,
                            'isProdMarketplace' => $isProdMarketplace
                        );

                        $this->addMarketplace($countryCode, $marketplace);

                    }

                }
            }

        }else{
            throw new \Exception('Access denied');
        }

        return 0;

    }

    /**
     * @return $this
     */
    public function afterSave()
    {

        if(count($this->_marketplaces) > 0){

            foreach($this->_marketplaces as $countryCode => $data){

                $country = $this->_country->load($countryCode, 'code');
                if($country->getId()){

                    $record = $this->getMarketplaces()
                        ->addFieldToFilter('country_id', $country->getId())
                        ->getFirstItem();

                    if(!$record->getId()) {

                        $record->setAccountId($this->getId())
                            ->setCountryId($country->getId())
                            ->save();

                    }

                }

            }

        }

        return parent::afterSave();
    }

    /**
     * @return \BoostMyShop\Amazon\Model\ResourceModel\Account\Country\Collection
     */
    public function getMarketplaces(){

        return $this->_accountCountryFactory->create()->getCollection()
            ->join(
                ['bac' => $this->getResource()->getTable('bms_amazon_country')],
                'bac.id=country_id',
                [
                    'code' => 'code',
                    'endpoint' => 'endpoint',
                    'marketplace_id' => 'marketplace_id'
                ]
            )
            ->addFieldToFilter('account_id', $this->getId());

    }

    /**
     * @return \BoostMyShop\Amazon\Model\ResourceModel\Account\Country\Collection
     */
    public function getMarketplacesWithoutSuspendedListing(){

        return $this->getMarketplaces()
            ->addFieldToFilter('suspended_listing', 0);

    }

    /**
     * @param array $countriesOptions
     */
    public function updateCountriesOptions($countriesOptions){

        foreach($countriesOptions as $countryId => $options){

            $country = $this->_accountCountryFactory->create()->load($countryId);
            if($country->getId()){

                $country->applyOptions($options);

            }

        }

    }

}