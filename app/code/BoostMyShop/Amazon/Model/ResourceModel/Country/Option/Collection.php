<?php namespace BoostMyShop\Amazon\Model\ResourceModel\Country\Option;

/**
 * Class Collection
 *
 * @package   BoostMyShop\Amazon\Model\ResourceModel\Country\Option
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection {

    protected function _construct()
    {
        $this->_init('BoostMyShop\Amazon\Model\Country\Option', 'BoostMyShop\Amazon\Model\ResourceModel\Country\Option');
    }

    /**
     * @param int $id
     * @return $this
     */
    public function filterByAccountCountry($id){

        return $this->addFieldToFilter('account_country_id', $id);

    }

    /**
     * @param int $accountCountryId
     * @param string $option
     * @return mixed
     */
    public function getParamValue($accountCountryId, $option){

        return $this->getOptionByAccountCountryId($accountCountryId, $option)
            ->getValue();

    }

    /**
     * @param int $accountCountryId
     * @param string $option
     * @return \Magento\Framework\DataObject
     */
    public function getOptionByAccountCountryId($accountCountryId, $option){

        return $this->addFieldToFilter('account_country_id', $accountCountryId)
            ->addFieldToFilter('option', $option)
            ->getFirstItem();

    }

}