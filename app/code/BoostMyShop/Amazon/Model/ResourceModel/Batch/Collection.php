<?php namespace BoostMyShop\Amazon\Model\ResourceModel\Batch;

/**
 * Class Collection
 *
 * @package   BoostMyShop\Amazon\Model\ResourceModel\Batch
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection {

    protected function _construct()
    {
        $this->_init('BoostMyShop\Amazon\Model\Batch', 'BoostMyShop\Amazon\Model\ResourceModel\Batch');
    }

    /**
     * @return $this
     */
    public function getItemsToClean(){

        $days = 7;
        $time = time() - 3600 * 24 * $days;
        return $this->addFieldToFilter('created_at', ['lt' => date('Y-m-d', $time)]);

    }

    /**
     * @return int $cptItemsDeleted
     */
    public function cleanHistory(){

        $cptItemsDeleted = 0;
        foreach($this->getItemsToClean() as $item){

            $item->delete();
            $cptItemsDeleted++;

        }

        return $cptItemsDeleted;

    }

    /**
     * @return int $cptItemsDeleted
     */
    public function delete(){

        $cptItemsDeleted = 0;
        foreach($this as $item){

            $item->delete();
            $cptItemsDeleted++;

        }
        return $cptItemsDeleted;

    }

    public function getProcessingBatchesForCountry($countryId, $limit = 10){

        $collection = $this->addFieldToFilter('account_country_id', $countryId)
            ->addFieldToFilter('status', 'processing');

        $collection->getSelect()->limit(0, $limit);

        return $collection;

    }

}