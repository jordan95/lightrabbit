<?php namespace BoostMyShop\Amazon\Model\ResourceModel\Account;

/**
 * Class Country
 *
 * @package   BoostMyShop\Amazon\Model\ResourceModel\Account
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Country extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb {

    protected function _construct()
    {
        $this->_init('bms_amazon_account_country', 'id');
    }

}