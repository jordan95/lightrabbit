<?php namespace BoostMyShop\Amazon\Model\ResourceModel\Account\Country;

/**
 * Class Collection
 *
 * @package   BoostMyShop\Amazon\Model\ResourceModel\Account\Country
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection {

    protected function _construct()
    {
        $this->_init('BoostMyShop\Amazon\Model\Account\Country', 'BoostMyShop\Amazon\Model\ResourceModel\Account\Country');
    }

    /**
     * @return $this
     */
    protected function _beforeLoad()
    {
        $this->join(
            ['bac' => $this->getTable('bms_amazon_country')],
            'bac.id=country_id',
            [
                'code',
                'endpoint',
                'active',
                'marketplace_id'
            ]
        )
        ->join(
            ['baa' => $this->getTable('bms_amazon_account')],
            'baa.id=account_id',
            [
                'account_name' => 'name'
            ]
        );

        return parent::_beforeLoad();
    }

}