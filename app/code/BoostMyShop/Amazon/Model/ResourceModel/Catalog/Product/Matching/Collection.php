<?php namespace BoostMyShop\Amazon\Model\ResourceModel\Catalog\Product\Matching;

/**
 * Class Collection
 *
 * @package   BoostMyShop\Amazon\Model\ResourceModel\Catalog\Product\Matching
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Collection extends \Magento\Catalog\Model\ResourceModel\Product\Collection {

    /**
     * @param $productsIds
     * @param \BoostMyShop\Amazon\Model\Account\Country $country
     * @return $this
     */
    public function loadForCountry($productsIds, \BoostMyShop\Amazon\Model\Account\Country $country){

        $this->addFieldToFilter('entity_id', ['in' => $productsIds]);
        $this->addAttributeToSelect($country->getOption('reference'));
        $this->addAttributeToSelect($country->getOption('barcode'));

        return $this;

    }

}