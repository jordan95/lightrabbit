<?php namespace BoostMyShop\Amazon\Model\ResourceModel\Catalog\Product;

/**
 * Class Collection
 *
 * @package   BoostMyShop\Amazon\Model\ResourceModel\Catalog\Product
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Collection extends \Magento\Catalog\Model\ResourceModel\Product\Collection {

    /**
     * @param \BoostMyShop\Amazon\Model\Account\Country $accountCountry
     * @return $this
     * @throws \Exception
     */
    public function joinAmazonProducts(\BoostMyShop\Amazon\Model\Account\Country $accountCountry){

        if(empty($accountCountry->getId()))
            throw new \Exception('Country is missing');

        $store = $accountCountry->getStore();
        $this->addAttributeToSelect($accountCountry->getOption('barcode'));
        $this->addAttributeToSelect($accountCountry->getOption('reference'));
        $this->addAttributeToSelect('special_price');
        $this->addAttributeToSelect('special_from_date');
        $this->addAttributeToSelect('special_to_date');
        $this->addAttributeToSelect('price');
        $customPriceAttribute = $accountCountry->getOption('price_attribute');
        if(!empty($customPriceAttribute))
            $this->addAttributeToSelect($customPriceAttribute);
        $customStockAttribute = $accountCountry->getOption('stock_attribute');
        if(!empty($customStockAttribute))
            $this->addAttributeToSelect($customStockAttribute);
        $customDelayAttribute = $accountCountry->getOption('delay_attribute');
        if(!empty($customDelayAttribute))
            $this->addAttributeToSelect($customDelayAttribute);
        $this->setStore($store);
        $this->addStoreFilter($store);

        $this->getSelect()
            ->joinLeft(
                ['bap' => $this->getTable('bms_amazon_product')],
                'entity_id=bap.product_id and bap.account_country_id = '.$accountCountry->getId(),
                [
                    'amazon_product_id' => 'id',
                    'product_id' => 'product_id',
                    'account_country_id' => 'account_country_id',
                    'asin' => 'asin',
                    'disabled' => 'disabled',
                    'amazon_status' => 'status',
                    'error_message' => 'error_message',
                    'amazon_created_at' => 'created_at',
                    'amazon_updated_at' => 'updated_at',
                    'last_update' => 'last_update',
                    'stock_status' => 'stock_status',
                    'price_status' => 'price_status'
                ]
            );
        return $this;

    }

    /**
     * @return $this
     */
    public function applyExportFilter()
    {
        $conditionSql = '(';
        $conditionSql .= ' status IN ("'.\BoostMyShop\Amazon\Model\Product::STATUS_DISABLED.'","'.\BoostMyShop\Amazon\Model\Product::STATUS_ASSOCIATED.'","'.\BoostMyShop\Amazon\Model\Product::STATUS_PENDING.'") AND (e.updated_at > last_update OR last_update is NULL)';
        $conditionSql .= ' OR stock_status = "'.\BoostMyShop\Amazon\Model\Product::STATUS_OFFER_ERROR.'"';
        $conditionSql .= ' OR price_status = "'.\BoostMyShop\Amazon\Model\Product::STATUS_OFFER_ERROR.'"';
        $conditionSql .= ' )';
        $this->getSelect()->where($conditionSql);
        return $this;
    }

    protected function _construct()
    {
        if ($this->isEnabledFlat()) {
            $this->_init('Magento\Catalog\Model\Product', 'Magento\Catalog\Model\ResourceModel\Product\Flat');
        } else {
            $this->_init('Magento\Catalog\Model\Product', 'Magento\Catalog\Model\ResourceModel\Product');
        }
        $this->setRowIdFieldName('e.entity_id');
        $this->_initTables();
    }

    /**
     * @return $this
     */
    protected function _initSelect()
    {
        parent::_initSelect();
        $this->addAttributeToSelect('name');
        $this->addAttributeToSelect('thumbnail');
        $this->addAttributeToSelect('image');
        $this->addAttributeToSelect('small_image');
        return $this;
    }

    /**
     * @param null $limit
     * @param null $offset
     * @return array
     */
    public function getAllIds($limit = null, $offset = null)
    {
        $idsSelect = $this->_getClearSelect();
        $idsSelect->columns('e.entity_id');
        $idsSelect->limit($limit, $offset);

        return $this->getConnection()->fetchCol($idsSelect, $this->_bindParams);
    }

    /**
     * @return \Magento\Framework\DB\Select
     */
    public function getSelectCountSql()
    {

        $this->_renderFilters();

        $countSelect = clone $this->getSelect();
        $countSelect->reset(\Magento\Framework\DB\Select::ORDER);
        $countSelect->reset(\Magento\Framework\DB\Select::LIMIT_COUNT);
        $countSelect->reset(\Magento\Framework\DB\Select::LIMIT_OFFSET);
        $countSelect->reset(\Magento\Framework\DB\Select::COLUMNS);

        $countSelect->columns('COUNT(*)');

        return $countSelect;
    }

    /**
     * @param \Magento\Eav\Model\Entity\Attribute\AbstractAttribute|string $attribute
     * @param null $condition
     * @param string $joinType
     * @return $this
     */
    public function addAttributeToFilter($attribute, $condition = null, $joinType = 'inner')
    {

        switch($attribute) {
            case 'stock_status':
            case 'price_status':
                $conditionSql = $this->_getConditionSql($attribute, $condition);
                if(isset($condition['eq']) && $condition['eq'] == 'up_to_date'){

                    $conditionSql = 'status in ("disabled","associated") and e.updated_at <= last_update';

                }
                if(isset($condition['eq']) && $condition['eq'] == 'to_update'){

                    $conditionSql = 'status in ("disabled","associated") and e.updated_at > last_update';

                }

                $this->getSelect()->where($conditionSql);

                break;
            case 'account_country_id':
            case 'error_message':
            case 'asin':
                $conditionSql = $this->_getConditionSql($attribute, $condition);
                $this->getSelect()->where($conditionSql);
                break;
            case 'amazon_status':
                $conditionSql = $this->_getConditionSql('status', $condition);
                if(isset($condition['eq']) && $condition['eq'] == 'not_associated'){

                    $conditionSql .= ' or status is null';

                }
                $this->getSelect()->where($conditionSql);
                break;
            default:
                parent::addAttributeToFilter($attribute, $condition, $joinType);
                break;
        }

        return $this;
    }

}
