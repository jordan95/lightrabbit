<?php namespace BoostMyShop\Amazon\Model\Batch;

/**
 * Class Log
 *
 * @package   BoostMyShop\Amazon\Model\Batch
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Log extends \Magento\Framework\Model\AbstractModel {

    const LEVEL_NOTICE = 'notice';
    const LEVEL_ERROR = 'error';
    const LEVEL_WARNING = 'warning';
    const LEVEL_INFO = 'info';

    protected function _construct()
    {
        $this->_init('BoostMyShop\Amazon\Model\ResourceModel\Batch\Log');
    }

    /**
     * @return array
     */
    public function getLevels(){

        return [
            self::LEVEL_INFO => ucfirst(self::LEVEL_INFO),
            self::LEVEL_NOTICE => ucfirst(self::LEVEL_NOTICE),
            self::LEVEL_WARNING => ucfirst(self::LEVEL_WARNING),
            self::LEVEL_ERROR => ucfirst(self::LEVEL_ERROR),
        ];

    }

    public function beforeSave()
    {

        if(!$this->getId()){
            $this->setCreatedAt(time());
        }

        return parent::beforeSave();
    }

}