<?php namespace BoostMyShop\Amazon\Model;

/**
 * Class Batch
 *
 * @package   BoostMyShop\Amazon\Model
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Batch extends \Magento\Framework\Model\AbstractModel {

    const OPERATION_TYPE_ORDER = 'order';
    const OPERATION_TYPE_SELLER = 'seller';
    const OPERATION_TYPE_SHIPMENT = 'shipment';
    const OPERATION_TYPE_CATALOG = 'catalog';
    const OPERATION_TYPE_MATCHING = 'matching';
    const OPERATION_TYPE_PRICE = 'price';
    const OPERATION_TYPE_INVENTORY = 'inventory';
    const OPERATION_TYPE_PURGE_CATALOG = 'purge';

    const STATUS_NEW = 'new';
    const STATUS_PROCESSING = 'processing';
    const STATUS_ERROR = 'error';
    const STATUS_SUCCESS = 'success';

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime|null
     */
    protected $_dateTime = null;

    /**
     * @var \BoostMyShop\Amazon\Model\Account\CountryFactory
     */
    protected $_countryFactory;

    /**
     * @var \BoostMyShop\Amazon\Helper\Logger
     */
    protected $_batchLogger;

    /**
     * @var array
     */
    protected $_messages = [];

    /**
     * Batch constructor.
     * @param \BoostMyShop\Amazon\Helper\Logger $logger
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $dateTime
     * @param \BoostMyShop\Amazon\Model\Account\CountryFactory $countryFactory
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        \BoostMyShop\Amazon\Helper\Logger $logger,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        \BoostMyShop\Amazon\Model\Account\CountryFactory $countryFactory,
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ){
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->_countryFactory = $countryFactory;
        $this->_batchLogger = $logger;
        $this->_logger = $logger;
        $this->_dateTime = $dateTime;
    }

    protected function _construct()
    {
        $this->_init('BoostMyShop\Amazon\Model\ResourceModel\Batch');
    }

    /**
     * @return array
     */
    public function getOperationTypes(){

        return [
            self::OPERATION_TYPE_ORDER => ucfirst(self::OPERATION_TYPE_ORDER),
            self::OPERATION_TYPE_SELLER => ucfirst(self::OPERATION_TYPE_SELLER),
            self::OPERATION_TYPE_SHIPMENT => ucfirst(self::OPERATION_TYPE_SHIPMENT),
            self::OPERATION_TYPE_CATALOG => ucfirst(self::OPERATION_TYPE_CATALOG),
            self::OPERATION_TYPE_MATCHING => ucfirst(self::OPERATION_TYPE_MATCHING),
            self::OPERATION_TYPE_PRICE => ucfirst(self::OPERATION_TYPE_PRICE),
            self::OPERATION_TYPE_INVENTORY => ucfirst(self::OPERATION_TYPE_INVENTORY),
            self::OPERATION_TYPE_PURGE_CATALOG => ucfirst(self::OPERATION_TYPE_PURGE_CATALOG)
        ];

    }

    /**
     * @return array
     */
    public function getStatuses(){

        return [
            self::STATUS_NEW => ucfirst(self::STATUS_NEW),
            self::STATUS_PROCESSING => ucfirst(self::STATUS_PROCESSING),
            self::STATUS_SUCCESS => ucfirst(self::STATUS_SUCCESS),
            self::STATUS_ERROR => ucfirst(self::STATUS_ERROR)
        ];

    }

    /**
     * @param string $errorMessage
     * @return $this|\BoostMyShop\Amazon\Model\Batch
     */
    public function addErrorMessage($errorMessage){

        $this->setStatus(self::STATUS_ERROR);
        return $this->_addMessage($errorMessage, \BoostMyShop\Amazon\Model\Batch\Log::LEVEL_ERROR);

    }

    /**
     * @param string $successMessage
     * @return $this|\BoostMyShop\Amazon\Model\Batch
     */
    public function addSuccessMessage($successMessage){

        return $this->_addMessage($successMessage, \BoostMyShop\Amazon\Model\Batch\Log::LEVEL_INFO);

    }

    /**
     * @param string $warningMessage
     * @return $this|\BoostMyShop\Amazon\Model\Batch
     */
    public function addWarningMessage($warningMessage){

        return $this->_addMessage($warningMessage, \BoostMyShop\Amazon\Model\Batch\Log::LEVEL_WARNING);

    }

    /**
     * @param string $noticeMessage
     * @return $this|\BoostMyShop\Amazon\Model\Batch
     */
    public function addNoticeMessage($noticeMessage){

        return $this->_addMessage($noticeMessage, \BoostMyShop\Amazon\Model\Batch\Log::LEVEL_NOTICE);

    }

    /**
     * @param string $message
     * @param string $level
     * @return $this
     */
    protected function _addMessage($message, $level){

        $this->_messages[] = ['batch_id' => $this->getId(), 'created_at' => $this->_dateTime->gmtDate(), 'message' => $message, 'level' => $level];
        return $this;

    }

    /**
     * @return $this
     */
    public function beforeSave()
    {

        if(!$this->getId()){
            $this->setCreatedAt(time())
                ->setBatchId(uniqid());

        }else{

            $this->flushMessages();

        }

        return parent::beforeSave();
    }

    public function flushMessages(){

        if(count($this->_messages) > 0)
            $this->getResource()->getConnection()
                ->insertMultiple($this->getResource()->getTable('bms_amazon_batch_log'), $this->_messages);

    }

    /**
     * @return \BoostMyShop\Amazon\Model\Account\Country
     */
    public function getCountry(){

        return $this->_countryFactory->create()->load($this->getAccountCountryId());

    }

    /**
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function beforeDelete()
    {
        $this->_batchLogger->deleteBatchFiles($this);
        return parent::beforeDelete();
    }

    protected function _getFilesDir(){

        return $this->_batchLogger->getDirname().$this->getOperationType().'/'.$this->getBatchId();

    }

    /**
     * @return array $files
     */
    public function getFiles(){

        $files = [];
        $dirname = $this->_getFilesDir();

        if(file_exists($dirname)){

            $this->_scanFiles($dirname, $files);

        }

        return $files;

    }

    /**
     * @param string $dirname
     * @param array $filesTab
     */
    protected function _scanFiles($dirname, &$filesTab){

        $files = array_diff(scandir($dirname), array('.', '..'));
        foreach ($files as $file) {
            if(is_dir("$dirname/$file")){
                $this->_scanFiles("$dirname/$file", $filesTab);
            }else{
                list($path, $filename) = explode($this->getBatchId(), $dirname.'/'.$file);
                $filesTab[$filename] = $filename;
            }
        }

    }

    /**
     * @param string $batchId
     * @return $this
     */
    public function updateBatchId($batchId){

        if(!empty($batchId)) {
            $oldFilesDir = $this->_getFilesDir();
            $this->setBatchId($batchId);
            if(file_exists($oldFilesDir)){
                rename($oldFilesDir, $this->_getFilesDir());
            }
        }

        return $this;

    }

    /**
     * @return $this
     */
    public function updateAsProcessing(){

        $this->setStatus(self::STATUS_PROCESSING);
        return $this;

    }

    /**
     * @param int $batchId
     * @return $this
     */
    public function loadByBatchId($batchId){

        return $this->load($batchId, 'batch_id');

    }

}