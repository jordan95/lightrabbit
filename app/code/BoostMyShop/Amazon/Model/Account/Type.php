<?php namespace BoostMyShop\Amazon\Model\Account;

/**
 * Class Type
 *
 * @package   BoostMyShop\Amazon\Model\Account
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Type extends \Magento\Framework\Model\AbstractModel {

    /**
     * @var \BoostMyShop\Amazon\Model\CountryFactory
     */
    protected $_countryFactory;

    /**
     * AccountType constructor.
     * @param \BoostMyShop\Amazon\Model\CountryFactory $countryFactory
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        \BoostMyShop\Amazon\Model\CountryFactory $countryFactory,
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ){
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->_countryFactory = $countryFactory;
    }

    protected function _construct()
    {
        $this->_init('BoostMyShop\Amazon\Model\ResourceModel\Account\Type');
    }

    /**
     * @return \BoostMyShop\Amazon\Model\ResourceModel\Account\Type\Collection
     */
    public function getActiveCollection(){

        return $this->getCollection()
            ->addFieldToFilter('active', 1);

    }

    /**
     * @return \BoostMyShop\Amazon\Model\ResourceModel\Country\Collection
     */
    public function getCountries(){

        return $this->_countryFactory->create()
            ->getCollection()
            ->addFieldToFilter('account_type_id', $this->getId());

    }

    /**
     * @return \BoostMyShop\Amazon\Model\ResourceModel\Country\Collection
     */
    public function getActiveCountries(){

        return $this->getCountries()
            ->addFieldToFilter('active', 1);

    }

    /**
     * @return \BoostMyShop\Amazon\Model\Country
     */
    public function getFirstActiveCountry(){

        return $this->getActiveCountries()
            ->getFirstItem();

    }

}