<?php namespace BoostMyShop\Amazon\Model\Account;

/**
 * Class Country
 *
 * @package   BoostMyShop\Amazon\Model\Account
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Country extends \Magento\Framework\Model\AbstractModel
{

    /**
     * @var \BoostMyShop\Amazon\Model\ResourceModel\Country\Option\CollectionFactory
     */
    protected $_countryOptionCollectionFactory;

    /**
     * @var \BoostMyShop\Amazon\Model\Country\OptionFactory
     */
    protected $_countryOptionFactory;

    /**
     * @var \BoostMyShop\Amazon\Model\AccountFactory
     */
    protected $_accountFactory;

    /**
     * @var \BoostMyShop\Amazon\Model\Account
     */
    protected $_account;

    /**
     * @var \BoostMyShop\Amazon\Model\BatchFactory
     */
    protected $_batchFactory;

    /**
     * @var \BoostMyShop\Amazon\Model\Batch;
     */
    protected $_batch;

    /**
     * @var \BoostMyShop\Amazon\Helper\Product
     */
    protected $_productHelper;

    /**
     * @var \BoostMyShop\Amazon\Model\CountryFactory
     */
    protected $_countryFactory;

    /**
     * @var \BoostMyShop\Amazon\Model\ResourceModel\Batch\CollectionFactory
     */
    protected $_batchCollectionFactory;

    /**
     * @var \BoostMyShop\Amazon\Model\ProductFactory
     */
    protected $_productFactory;

    /**
     * @var \BoostMyShop\Amazon\Helper\Product\Matching\Match
     */
    protected $_productMatchingMatchHelper;

    /**
     * @var \BoostMyShop\Amazon\Helper\Product\Matching\Push
     */
    protected $_productMatchingPushHelper;

    /**
     * @var \BoostMyShop\Amazon\Helper\Product\Import
     */
    protected $_productImportHelper;

    /**
     * @var \BoostMyShop\Amazon\Helper\Order\Import
     */
    protected $_orderImportHelper;

    /**
     * @var \BoostMyShop\Amazon\Helper\Order\Ship
     */
    protected $_orderShipHelper;

    /**
     * @var \BoostMyShop\Amazon\Helper\Mws\FeedsFactory
     */
    protected $_mwsFeedClientFactory;

    /**
     * @var \BoostMyShop\Amazon\Helper\Offer\Update
     */
    protected $_offerUpdateHelper;

    /**
     * @var \Magento\Store\Model\StoreFactory
     */
    protected $_storeFactory;

    /**
     * Country constructor.
     * @param \Magento\Store\Model\StoreFactory $storeFactory
     * @param \BoostMyShop\Amazon\Helper\Offer\Update $offerUpdateHelper
     * @param \BoostMyShop\Amazon\Helper\Mws\FeedsFactory $mwsFeedClientFactory
     * @param \BoostMyShop\Amazon\Helper\Order\Ship $orderShipHelper
     * @param \BoostMyShop\Amazon\Helper\Order\Import $orderImportHelper
     * @param \BoostMyShop\Amazon\Helper\Product\Import $productImportHelper
     * @param \BoostMyShop\Amazon\Helper\Product\Matching\Match $productMatchingMatchHelper
     * @param \BoostMyShop\Amazon\Helper\Product\Matching\Push $productMatchingPushHelper
     * @param \BoostMyShop\Amazon\Model\ProductFactory $productFactory
     * @param \BoostMyShop\Amazon\Model\ResourceModel\Batch\CollectionFactory $batchCollectionFactory
     * @param \BoostMyShop\Amazon\Model\CountryFactory $countryFactory
     * @param \BoostMyShop\Amazon\Helper\Product $productHelper
     * @param \BoostMyShop\Amazon\Model\BatchFactory $batchFactory
     * @param \BoostMyShop\Amazon\Model\AccountFactory $accountFactory
     * @param \BoostMyShop\Amazon\Model\Country\OptionFactory $countryOptionFactory
     * @param \BoostMyShop\Amazon\Model\ResourceModel\Country\Option\CollectionFactory $countryOptionCollectionFactory
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Store\Model\StoreFactory $storeFactory,
        \BoostMyShop\Amazon\Helper\Offer\Update $offerUpdateHelper,
        \BoostMyShop\Amazon\Helper\Mws\FeedsFactory $mwsFeedClientFactory,
        \BoostMyShop\Amazon\Helper\Order\Ship $orderShipHelper,
        \BoostMyShop\Amazon\Helper\Order\Import $orderImportHelper,
        \BoostMyShop\Amazon\Helper\Product\Import $productImportHelper,
        \BoostMyShop\Amazon\Helper\Product\Matching\Match $productMatchingMatchHelper,
        \BoostMyShop\Amazon\Helper\Product\Matching\Push $productMatchingPushHelper,
        \BoostMyShop\Amazon\Model\ProductFactory $productFactory,
        \BoostMyShop\Amazon\Model\ResourceModel\Batch\CollectionFactory $batchCollectionFactory,
        \BoostMyShop\Amazon\Model\CountryFactory $countryFactory,
        \BoostMyShop\Amazon\Helper\Product $productHelper,
        \BoostMyShop\Amazon\Model\BatchFactory $batchFactory,
        \BoostMyShop\Amazon\Model\AccountFactory $accountFactory,
        \BoostMyShop\Amazon\Model\Country\OptionFactory $countryOptionFactory,
        \BoostMyShop\Amazon\Model\ResourceModel\Country\Option\CollectionFactory $countryOptionCollectionFactory,
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    )
    {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->_countryOptionCollectionFactory = $countryOptionCollectionFactory;
        $this->_countryOptionFactory = $countryOptionFactory;
        $this->_accountFactory = $accountFactory;
        $this->_batchFactory = $batchFactory;
        $this->_productHelper = $productHelper;
        $this->_countryFactory = $countryFactory;
        $this->_batchCollectionFactory = $batchCollectionFactory;
        $this->_productFactory = $productFactory;
        $this->_productMatchingMatchHelper = $productMatchingMatchHelper;
        $this->_productMatchingPushHelper = $productMatchingPushHelper;
        $this->_productImportHelper = $productImportHelper;
        $this->_orderImportHelper = $orderImportHelper;
        $this->_orderShipHelper = $orderShipHelper;
        $this->_mwsFeedClientFactory = $mwsFeedClientFactory;
        $this->_offerUpdateHelper = $offerUpdateHelper;
        $this->_storeFactory = $storeFactory;
    }

    protected function _construct()
    {
        $this->_init('BoostMyShop\Amazon\Model\ResourceModel\Account\Country');
    }

    /**
     * @return \BoostMyShop\Amazon\Helper\Offer\Update
     */
    public function getOfferUpdateHelper(){

        return $this->_offerUpdateHelper;

    }

    /**
     * @param string $option
     * @param mixed $value
     * @return $this
     */
    public function setOption($option, $value)
    {

        $this->_countryOptionFactory->create()
            ->addOrUpdateParam($this->getId(), $option, $value);

        return $this;
    }

    /**
     * @param string $option
     * @return mixed
     */
    public function getOption($option)
    {

        $value = $this->_countryOptionCollectionFactory->create()
            ->getParamValue($this->getId(), $option);

        return ($value === '0' || !empty($value)) ? $value : $this->getDefaultOptionValue($option);

    }

    /**
     * @param string $option
     * @return string
     */
    public function getDefaultOptionValue($option)
    {

        $defaultValue = '';

        switch ($option) {

            case 'reference':
                $defaultValue = 'sku';
                break;
            case 'order_importation_range':
                $defaultValue = 5;
                break;
            case 'order_state':
                $defaultValue = 'pending';
                break;
            case 'customer_mode':
                $defaultValue = 'create';
                break;
            case 'price_attribute':
                $defaultValue = 'price';
                break;
            case 'generate_invoice':
                $defaultValue = 1;
                break;

        }

        return $defaultValue;

    }

    /**
     * @param array $options
     */
    public function applyOptions($options)
    {

        foreach ($options as $name => $value) {

            $this->setOption($name, $value);

        }

    }

    /**
     * @return \Magento\Store\Model\Store
     */
    public function getStore(){

        return $this->_storeFactory->create()->load($this->getOption('store'));

    }

    /**
     * @return $this
     * @todo : add this for collection
     */
    protected function _afterLoad()
    {

        $country = $this->_countryFactory->create()->load($this->getCountryId());
        $this->setData('account_type_id', $country->getAccountTypeId());
        $this->setData('code', $country->getCode());
        $this->setData('endpoint', $country->getEndpoint());
        $this->setData('active', $country->getActive());
        $this->setData('marketplace_id', $country->getMarketplaceId());
        $this->setData('account_name', $this->_accountFactory->create()->load($this->getAccountId())->getName());

        return parent::_afterLoad();
    }

    /**
     * @return string
     */
    public function getLabel()
    {

        return $this->getData('account_name') . ' (' . $this->getData('code') . ')';

    }

    /**
     * @return string
     */
    public function getCurrencyCode(){

        return $this->getStore()->getDefaultCurrencyCode();

    }

    /**
     * @return \BoostMyShop\Amazon\Model\Account
     */
    public function getAccount()
    {

        if (is_null($this->_account)) {

            $this->_account = $this->_accountFactory->create()->load($this->getAccountId());

        }

        return $this->_account;

    }

    /**
     * @return bool
     */
    public function isEnabled()
    {

        return ($this->getAccount()->getActive()) ? true : false;

    }

    /**
     * @return bool
     */
    public function isAllowedToImportOrders()
    {

        return $this->getAccount()->getActive() && $this->getOption('order_importation_enabled');

    }

    /**
     * @return string $summary
     */
    public function importOrders()
    {
        return $this->_orderImportHelper
            ->setCountry($this)
            ->execute();
    }

    /**
     * @return bool
     */
    public function isAllowedToShipOrders()
    {

        return $this->getAccount()->getActive() && $this->getOption('shipment_confirmation_enabled');

    }

    public function shipOrders()
    {
        return $this->_orderShipHelper
            ->setCountry($this)
            ->execute();
    }

    public function generateOrderFulfillmentFeed($amazonOrderIds){

        return $this->_orderShipHelper
            ->setCountry($this)
            ->generateOrderFulfillmentFeed($amazonOrderIds);

    }

    /**
     * @param \BoostMyShop\Amazon\Model\Batch $batch
     */
    public function checkResponseForBatch(\BoostMyShop\Amazon\Model\Batch $batch)
    {

        try {

            $result = $this->_mwsFeedClientFactory->create()->reset()
                ->setBatch($batch)
                ->setMerchantId($this->getAccount()->getMerchantId())
                ->setAccessKeyId($this->getAccount()->getAccessKeyId())
                ->setSecretKey($this->getAccount()->getSecretKey())
                ->setBaseUrl($this->getendpoint())
                ->setMarketplaceId($this->getmarketplace_id())
                ->getFeedSubmissionResult($batch->getBatchId());

            $result = json_decode(json_encode(simplexml_load_string($result->getBody())));
            $messagesWithErrors = $result->Message->ProcessingReport->ProcessingSummary->MessagesWithError;

            if ($messagesWithErrors > 0) {

                if(is_array($result->Message->ProcessingReport->Result)){

                    foreach($result->Message->ProcessingReport->Result as $resultItem){

                        $errorMessage = $resultItem->ResultDescription;
                        if(isset($resultItem->AdditionalInfo)){

                            if(isset($resultItem->AdditionalInfo->SKU)){

                                $product = $this->_productHelper->getProductFromSellerSku($resultItem->AdditionalInfo->SKU, $this->getOption('reference'));
                                $amazonProduct = $this->_productFactory->create()->loadByAccountCountryId($product->getId(), $this->getId());

                                switch($batch->getOperationType()){

                                    case \BoostMyShop\Amazon\Model\Batch::OPERATION_TYPE_PRICE:
                                        $amazonProduct->setPriceStatus(\BoostMyShop\Amazon\Model\Product::STATUS_OFFER_ERROR);
                                        break;
                                    case \BoostMyShop\Amazon\Model\Batch::OPERATION_TYPE_INVENTORY:
                                        $amazonProduct->setStockStatus(\BoostMyShop\Amazon\Model\Product::STATUS_OFFER_ERROR);
                                        break;
                                    default:
                                        $amazonProduct->setStatus(\BoostMyShop\Amazon\Model\Product::STATUS_ERROR);
                                        break;

                                }

                                $amazonProduct->setErrorMessage($errorMessage)
                                    ->save();
                                $errorMessage .= $resultItem->AdditionalInfo->SKU.' : '.$errorMessage;

                            }

                        }
                        $batch->addErrorMessage($errorMessage);

                    }

                }else{

                    $resultItem = $result->Message->ProcessingReport->Result;
                    $errorMessage = $resultItem->ResultDescription;
                    if(isset($resultItem->AdditionalInfo)){

                        if(isset($resultItem->AdditionalInfo->SKU)){

                            $product = $this->_productHelper->getProductFromSellerSku($resultItem->AdditionalInfo->SKU, $this->getOption('reference'));
                            $amazonProduct = $this->_productFactory->create()->loadByAccountCountryId($product->getId(), $this->getId());
                            switch($batch->getOperationType()){

                                case \BoostMyShop\Amazon\Model\Batch::OPERATION_TYPE_PRICE:
                                    $amazonProduct->setPriceStatus(\BoostMyShop\Amazon\Model\Product::STATUS_OFFER_ERROR);
                                    break;
                                case \BoostMyShop\Amazon\Model\Batch::OPERATION_TYPE_INVENTORY:
                                    $amazonProduct->setStockStatus(\BoostMyShop\Amazon\Model\Product::STATUS_OFFER_ERROR);
                                    break;
                                default:
                                    $amazonProduct->setStatus(\BoostMyShop\Amazon\Model\Product::STATUS_ERROR);
                                    break;

                            }

                            $amazonProduct->setErrorMessage($errorMessage)
                                ->save();
                            $errorMessage .= $resultItem->AdditionalInfo->SKU.' : '.$errorMessage;

                        }

                    }
                    $batch->addErrorMessage($errorMessage);

                }

            }else{

                $batch->setStatus(\BoostMyShop\Amazon\Model\Batch::STATUS_SUCCESS);

            }

        } catch (\BoostMyShop\Amazon\Exception\ErrorResponseException $e) {

            $batch->addWarningMessage($e->getMessage());

        }

        $batch->save();

    }

    /**
     * @param string $batchId
     */
    public function checkResponseForBatchId($batchId)
    {

        $batch = $this->_batchFactory->create()->loadByBatchId($batchId);
        $this->checkResponseForBatch($batch);

    }

    /**
     * @param int $limit
     * @return array $updated
     */
    public function checkResponseForProcessingBatches($limit = 10)
    {

        $updated = [];
        $batchesCollection = $this->_batchCollectionFactory->create()->getProcessingBatchesForCountry($this->getId(), $limit);

        foreach ($batchesCollection as $batch) {

            $this->checkResponseForBatch($batch);
            $updated[] = $batch->getBatchId();

        }
        return $updated;

    }

    /**
     * @return string
     * @throws \Exception
     */
    public function importProducts()
    {

        return $this->_productImportHelper
            ->setCountry($this)
            ->execute();

    }

    /**
     * @param array $productIds
     * @return string
     */
    public function match($productIds)
    {

        return $this->_productMatchingMatchHelper
            ->setCountry($this)
            ->execute($productIds);

    }

    /**
     * @return string $summary
     * @throws \Exception
     */
    public function pushProducts(){

        return $this->_productMatchingPushHelper
            ->setCountry($this)
            ->execute();

    }

    /**
     * @param array $productIds
     * @return string $summary
     */
    public function updateOffers($productIds){

        return $this->_offerUpdateHelper
            ->setCountry($this)
            ->processIds($productIds);

    }

    /**
     * @return bool
     */
    public function isAllowedToExportOffers()
    {

        return $this->getAccount()->getActive() && $this->getOption('offer_update_enabled');

    }

    /**
     * @return string
     */
    public function exportOffers(){

        return $this->_offerUpdateHelper
            ->setCountry($this)
            ->processToUpdate();

    }

}