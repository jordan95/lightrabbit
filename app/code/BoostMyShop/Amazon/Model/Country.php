<?php namespace BoostMyShop\Amazon\Model;

/**
 * Class Country
 *
 * @package   BoostMyShop\Amazon\Model
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Country extends \Magento\Framework\Model\AbstractModel {

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('BoostMyShop\Amazon\Model\ResourceModel\Country');
    }

}