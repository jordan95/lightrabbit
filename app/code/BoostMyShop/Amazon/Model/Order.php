<?php namespace BoostMyShop\Amazon\Model;

/**
 * Class Order
 *
 * @package   BoostMyShop\Amazon\Model
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Order extends \Magento\Framework\Model\AbstractModel {

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    protected $_orderCollectionFactory;

    /**
     * @var \BoostMyShop\Amazon\Model\Order\ItemFactory
     */
    protected $_orderItemFactory;

    /**
     * @var \BoostMyShop\Amazon\Model\ResourceModel\Order\Item\CollectionFactory
     */
    protected $_orderItemCollectionFactory;

    /**
     * @var \BoostMyShop\Amazon\Helper\ProductFactory
     */
    protected $_productHelperFactory;

    /**
     * Order constructor.
     * @param \BoostMyShop\Amazon\Helper\ProductFactory $productHelperFactory
     * @param \BoostMyShop\Amazon\Model\ResourceModel\Order\Item\CollectionFactory $orderItemCollectionFactory
     * @param \BoostMyShop\Amazon\Model\Order\ItemFactory $orderItemFactory
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        \BoostMyShop\Amazon\Helper\ProductFactory $productHelperFactory,
        \BoostMyShop\Amazon\Model\ResourceModel\Order\Item\CollectionFactory $orderItemCollectionFactory,
        \BoostMyShop\Amazon\Model\Order\ItemFactory $orderItemFactory,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ){
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->_orderCollectionFactory = $orderCollectionFactory;
        $this->_orderItemFactory = $orderItemFactory;
        $this->_orderItemCollectionFactory = $orderItemCollectionFactory;
        $this->_productHelperFactory = $productHelperFactory;
    }

    protected function _construct()
    {
        $this->_init('BoostMyShop\Amazon\Model\ResourceModel\Order');
    }

    /**
     * @param string $amazonOrderId
     * @return bool
     */
    public function orderAlreadyImported($amazonOrderId){

        $entry = $this->load($amazonOrderId, 'amazon_order_id');

        return ($entry->getId()) ? true : false;

    }

    /**
     * @return \Magento\Sales\Model\ResourceModel\Order\Collection
     */
        public function getAmazonOrdersCollection(){

        return $this->_orderCollectionFactory->create()
            ->join(
                ['bao' => $this->getResource()->getTable('bms_amazon_order')],
                'bao.order_id=entity_id',
                ['*']
            )
            ->join(
                ['soa' => $this->getResource()->getTable('sales_order_address')],
                'soa.entity_id=shipping_address_id',
                ['*']
            );

    }

    /**
     * @param array $amazonOrderIds
     * @return \Magento\Sales\Model\ResourceModel\Order\Collection
     */
    public function getOrdersToShipCollection($amazonOrderIds){

        return $this->_orderCollectionFactory->create()
            ->addFieldToFilter('status', 'complete')
            ->join(
                $this->getResource()->getTable('bms_amazon_order'),
                'order_id=entity_id',
                ['*']
            )
            ->addFieldToFilter('amazon_order_id', ['in' => $amazonOrderIds]);

    }

    /**
     * @param $orderItems
     */
    public function addAmazonOrderItems($orderItems){

        foreach($orderItems as $item){

            $amazonOrderItem = $this->_orderItemFactory->create();
            $amazonOrderItem->setBmsAmazonOrderId($this->getId())
                ->setAsin($item->ASIN)
                ->setTitle($item->Title)
                ->setSellerSku($item->SellerSKU)
                ->setAmazonOrderItemId($item->OrderItemId)
                ->save();

        }

    }

    /**
     * @param int $productId
     * @param string $reference
     * @return string
     */
    public function retrieveAmazonOrderItemId($productId, $reference){

        $items = $this->_orderItemCollectionFactory->create()->addBmsAmazonOrderIdFilter($this->getId());

        foreach($items as $item){
            $productHelper = $this->_productHelperFactory->create();
            $product = $productHelper->getProductFromSellerSku($item->getSellerSku(), $reference);
            if($product->getId() && $product->getId() == $productId){
                return $item->getAmazonOrderItemId();
            }

        }
        return '';

    }

}