<?php namespace BoostMyShop\Amazon\Model;

/**
 * Class Product
 *
 * @package   BoostMyShop\Amazon\Model
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Product extends \Magento\Framework\Model\AbstractModel {

    const STATUS_NOT_ASSOCIATED = 'not_associated';
    const STATUS_PENDING = 'pending';
    const STATUS_ASSOCIATED = 'associated';
    const STATUS_TO_PUSH = 'to_push';
    const STATUS_TO_MATCH = 'to_match';
    const STATUS_NOT_FOUND = 'not_found';
    const STATUS_DISABLED = 'disabled';
    const STATUS_ERROR = 'error';

    const STATUS_OFFER_ERROR = 'error';
    const STATUS_OFFER_TO_UPDATE = 'to_update';
    const STATUS_OFFER_UP_TO_DATE = 'up_to_date';

    protected function _construct()
    {
        $this->_init('BoostMyShop\Amazon\Model\ResourceModel\Product');
    }

    /**
     * @return array
     */
    public function getStatuses(){

        return [
            self::STATUS_ASSOCIATED => __('Associated'),
            self::STATUS_NOT_ASSOCIATED => __('Not Associated'),
            self::STATUS_TO_PUSH => __('To Push'),
            self::STATUS_NOT_FOUND => __('Not Found'),
            self::STATUS_PENDING => __('Submitted'),
            self::STATUS_DISABLED => __('Disabled'),
            self::STATUS_ERROR => __('Error')
        ];

    }

    /**
     * @return array
     */
    public function getOfferStatuses(){

        return [
            self::STATUS_OFFER_UP_TO_DATE => __('Up to date'),
            self::STATUS_OFFER_TO_UPDATE => __('To update'),
            self::STATUS_OFFER_ERROR => __('Error')
        ];

    }

    /**
     * @return $this
     */
    public function beforeSave()
    {

        $time = time();

        if(!$this->getId()){

            $this->setCreatedAt($time)
                ->setUpdatedAt($time);

        }else{

            $this->setUpdatedAt($time);

        }

        return parent::beforeSave();
    }

    public function loadByAccountCountryId($productId, $accountCountryId){

        return $this->getCollection()
            ->addFieldToFilter('product_id', $productId)
            ->addFieldToFilter('account_country_id', $accountCountryId)
            ->getFirstItem();

    }

}