<?php namespace BoostMyShop\Amazon\Lib\MWS;

/**
 * Class Report
 *
 * @package   BoostMyShop\Amazon\Lib\MWS
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Reports extends \BoostMyShop\Amazon\Lib\MWS\Client
{

    const SCHEDULE_15_MINUTES = '_15_MINUTES_';
    const SCHEDULE_30_MINUTES = '_30_MINUTES_';
    const SCHEDULE_1_HOUR = '_1_HOUR_';
    const SCHEDULE_2_HOURS = '_2_HOURS_';
    const SCHEDULE_4_HOURS = '_4_HOURS_';
    const SCHEDULE_8_HOURS = '_8_HOURS_';
    const SCHEDULE_12_HOURS = '_12_HOURS_';
    const SCHEDULE_72_HOURS = '_72_HOURS_';
    const SCHEDULE_1_DAY = '_1_DAY_';
    const SCHEDULE_2_DAYS = '_2_DAYS_';
    const SCHEDULE_14_DAYS = '_14_DAYS_';
    const SCHEDULE_15_DAYS = '_15_DAYS_';
    const SCHEDULE_30_DAYS = '_30_DAYS_';
    const SCHEDULE_1_WEEK = '_1_WEEK_';
    const SCHEDULE_NEVER = '_NEVER_';

    //Listings Reports
    const REPORT_TYPE_INVENTORY = '_GET_FLAT_FILE_OPEN_LISTINGS_DATA_';
    const REPORT_TYPE_ALL_LISTINGS = '_GET_MERCHANT_LISTINGS_ALL_DATA_';
    const REPORT_TYPE_ACTIVE_LISTINGS = '_GET_MERCHANT_LISTINGS_DATA_';
    const REPORT_TYPE_INACTIVE_LISTINGS = '_GET_MERCHANT_LISTINGS_INACTIVE_DATA_';
    const REPORT_TYPE_OPEN_LISTINGS = '_GET_MERCHANT_LISTINGS_DATA_BACK_COMPAT_';
    const REPORT_TYPE_OPEN_LISTINGS_LITE = '_GET_MERCHANT_LISTINGS_DATA_LITE_';
    const REPORT_TYPE_OPEN_LISTINGS_LITER = '_GET_MERCHANT_LISTINGS_DATA_LITER_';
    const REPORT_TYPE_CANCELED_LISTINGS = '_GET_MERCHANT_CANCELLED_LISTINGS_DATA_';
    const REPORT_TYPE_SOLD_LISTINGS = '_GET_CONVERGED_FLAT_FILE_SOLD_LISTINGS_DATA_';
    const REPORT_TYPE_LISTING_QUALITY = '_GET_MERCHANT_LISTINGS_DEFECT_DATA_';
    //Order Reports
    const REPORT_TYPE_UNSHIPPED_ORDERS = '_GET_FLAT_FILE_ACTIONABLE_ORDER_DATA_';
    const REPORT_TYPE_SCHEDULED_XML_ORDER = '_GET_ORDERS_DATA_';
    const REPORT_TYPE_REQUESTED_OR_SCHEDULED_FLAT_FILE_ORDER = '_GET_FLAT_FILE_ORDERS_DATA_';
    const REPORT_TYPE_FLAT_FILE_ORDER = '_GET_CONVERGED_FLAT_FILE_ORDER_REPORT_DATA_';
    //Order Tracking Reports
    const REPORT_TYPE_FLAT_FILE_ORDERS_BY_LAST_UPDATE = '_GET_FLAT_FILE_ALL_ORDERS_DATA_BY_LAST_UPDATE_';
    const REPORT_TYPE_FLAT_FILE_ORDERS_BY_ORDER_DATE = '_GET_FLAT_FILE_ALL_ORDERS_DATA_BY_ORDER_DATE_';
    const REPORT_TYPE_XML_ORDERS_BY_LAST_UPDATE = '_GET_XML_ALL_ORDERS_DATA_BY_LAST_UPDATE_';
    const REPORT_TYPE_XML_ORDERS_BY_ORDER_DATE = '_GET_XML_ALL_ORDERS_DATA_BY_ORDER_DATE_';
    //Pending Order Reports
    const REPORT_TYPE_FLAT_FILE_PENDING_ORDERS = '_GET_FLAT_FILE_PENDING_ORDERS_DATA_';
    const REPORT_TYPE_XML_PENDING_ORDERS = '_GET_PENDING_ORDERS_DATA_';
    const REPORT_TYPE_CONVERGED_FLAT_FILE_PENDING_ORDERS = '_GET_CONVERGED_FLAT_FILE_PENDING_ORDERS_DATA_';
    //Performance Reports
    const REPORT_TYPE_FLAT_FILE_FEEDBACK = '_GET_SELLER_FEEDBACK_DATA_';
    const REPORT_TYPE_XML_CUSTOMER_METRICS = '_GET_V1_SELLER_PERFORMANCE_REPORT_';
    //Settlement Reports
    const REPORT_TYPE_FLAT_FILE_SETTLEMENT = '_GET_V2_SETTLEMENT_REPORT_DATA_FLAT_FILE_';
    const REPORT_TYPE_XML_SETTLEMENT = '_GET_V2_SETTLEMENT_REPORT_DATA_XML_';
    const REPORT_TYPE_FLAT_FILE_V2_SETTLEMENT = '_GET_V2_SETTLEMENT_REPORT_DATA_FLAT_FILE_V2_';
    //Sales Tax Reports
    const REPORT_TYPE_SALES_TAX = '_GET_FLAT_FILE_SALES_TAX_DATA_';
    //Browse Tree Reports
    const REPORT_TYPE_BROWSE_TREE = '_GET_XML_BROWSE_TREE_DATA_';

    const kVersion = '2009-01-01';

    const kMarketPlaceIdTypeId = 'id';
    const kMarketPlaceIdTypeList = 'list';

    /* @var string */
    protected $_marketPlaceIdType = '';

    /**
     * Get before query
     *
     * @return string
     */
    protected function getBeforeQuery()
    {
        return '/';
    }

    /**
     * Get uri
     *
     * @return string
     */
    public function getUri()
    {
        return 'https://'.$this->getBaseUrl().$this->getBeforeQuery();
    }

    /**
     * Get version
     *
     * @return string
     */
    public function getVersion()
    {
        return self::kVersion;
    }

    /**
     * Add marketplace id data in request parameters
     */
    protected function addMarketPlaceId()
    {
        switch ($this->_marketPlaceIdType) {
            case self::kMarketPlaceIdTypeList:
                $k = 'MarketplaceIdList.Id.1';
                break;
            case self::kMarketPlaceIdTypeId:
            default:
                $k = 'MarketplaceId';
                break;
        }

        $this->_params[$k] = $this->getMarketplaceId();
    }

    /**
     * Get report by id
     *
     * @param int $reportId
     * @return string
     * @see http://docs.developer.amazonservices.com/en_FR/reports/Reports_GetReport.html
     */
    public function getReport($reportId)
    {
        $params = array(
            'Action' => 'GetReport',
            'ReportId' => $reportId
        );

        return $this->query($params, false);
    }

    /**
     * @param array $reportTypeList
     * @param string $acknowledged
     * @param string $availableFromDate
     * @param string $availableToDate
     * @return \Zend_Http_Response
     * @see http://docs.developer.amazonservices.com/en_FR/reports/Reports_GetReportCount.html
     */
    public function getReportCount($reportTypeList = [], $acknowledged = '', $availableFromDate = '', $availableToDate = '')
    {
        $params = array(
            'Action' => 'GetReportCount'
        );

        $this->addReportTypeListFilter($reportTypeList, $params);
        $this->addAcknowledgedFilter($acknowledged, $params);
        $this->addAvailableFromDateFilter($availableFromDate, $params);
        $this->addAvailableToDateFilter($availableToDate, $params);

        return $this->query($params, false);
    }

    /**
     * @param string $maxCount
     * @param array $reportTypeList
     * @param string $acknowledged
     * @param string $availableFromDate
     * @param string $availableToDate
     * @param array $reportRequestIdList
     * @return \Zend_Http_Response
     * @see http://docs.developer.amazonservices.com/en_FR/reports/Reports_GetReportList.html
     */
    public function getReportList($maxCount = '', $reportTypeList = [], $acknowledged = '', $availableFromDate = '', $availableToDate = '', $reportRequestIdList = [])
    {
        $params = [
            'Action' => 'GetReportList'
        ];

        $this->addMaxCountFilter($maxCount, $params);
        $this->addReportTypeListFilter($reportTypeList, $params);
        $this->addAcknowledgedFilter($acknowledged, $params);
        $this->addAvailableFromDateFilter($availableFromDate, $params);
        $this->addAvailableToDateFilter($availableToDate, $params);
        $this->addReportRequestIdListFilter($reportRequestIdList, $params);

        return $this->query($params, false);
    }

    /**
     * @param $token
     * @return \Zend_Http_Response
     * @see http://docs.developer.amazonservices.com/en_FR/reports/Reports_GetReportListByNextToken.html
     */
    public function getReportListByNextToken($token)
    {
        $params = array(
            'Action' => 'GetReportRequestListByNextToken',
            'NextToken' => $token
        );

        return $this->query($params, false);
    }

    /**
     * @param string $requestedFromDate
     * @param string $requestedToDate
     * @param array $reportTypeList
     * @param array $reportProcessingStatusList
     * @return \Zend_Http_Response
     * @see http://docs.developer.amazonservices.com/en_FR/reports/Reports_GetReportRequestCount.html
     */
    public function getReportRequestCount($requestedFromDate = '', $requestedToDate = '', $reportTypeList = [], $reportProcessingStatusList = []){

        $params = [
            'Action' => 'GetReportRequestCount'
        ];

        $this->addRequestedFromDateFilter($requestedFromDate, $params);
        $this->addRequestedToDateFilter($requestedToDate, $params);
        $this->addReportTypeListFilter($reportTypeList, $params);
        $this->addReportProcessingStatusListFilter($reportProcessingStatusList, $params);

        return $this->query($params, false);

    }

    /**
     * @param $maxCount
     * @param string $requestedFromDate
     * @param string $requestedToDate
     * @param array $reportRequestIdList
     * @param array $reportTypeList
     * @param array $reportProcessingStatusList
     * @return \Zend_Http_Response
     * @see http://docs.developer.amazonservices.com/en_FR/reports/Reports_GetReportRequestList.html
     */
    public function getReportRequestList($maxCount, $requestedFromDate = '', $requestedToDate = '', $reportRequestIdList = [], $reportTypeList = [], $reportProcessingStatusList = [])
    {
        $params = array(
            'Action' => 'GetReportRequestList'
        );

        $this->addMaxCountFilter($maxCount, $params);
        $this->addRequestedFromDateFilter($requestedFromDate, $params);
        $this->addRequestedToDateFilter($requestedToDate, $params);
        $this->addReportRequestIdListFilter($reportRequestIdList, $params);
        $this->addReportTypeListFilter($reportTypeList, $params);
        $this->addReportProcessingStatusListFilter($reportProcessingStatusList, $params);

        return $this->query($params, false);
    }

    /**
     * @param string $nextToken
     * @return \Zend_Http_Response
     * @see http://docs.developer.amazonservices.com/en_FR/reports/Reports_GetReportRequestListByNextToken.html
     */
    public function getReportRequestListByNextToken($nextToken){

        $params = [
            'Action' => 'GetReportRequestListByNextToken',
            'NextToken' => $nextToken
        ];

        return $this->query($params, false);

    }

    /**
     * @param string $requestedFromDate
     * @param string $requestedToDate
     * @param array $reportRequestIdList
     * @param array $reportTypeList
     * @param array $reportProcessingStatusList
     * @return \Zend_Http_Response
     * @see http://docs.developer.amazonservices.com/en_FR/reports/Reports_CancelReportRequests.html
     */
    public function cancelReportRequests($requestedFromDate = '', $requestedToDate = '', $reportRequestIdList = [], $reportTypeList = [], $reportProcessingStatusList = []){

        $params = [
            'Action' => 'CancelReportRequests'
        ];

        $this->addRequestedFromDateFilter($requestedFromDate, $params);
        $this->addRequestedToDateFilter($requestedToDate, $params);
        $this->addReportRequestIdListFilter($reportRequestIdList, $params);
        $this->addReportTypeListFilter($reportTypeList, $params);
        $this->addReportProcessingStatusListFilter($reportProcessingStatusList, $params);

        return $this->query($params, false);

    }

    /**
     * @param $reportType
     * @param array $marketplaceIdList
     * @param string $startDate
     * @param string $endDate
     * @param string $reportOptions
     * @return \Zend_Http_Response
     * @see http://docs.developer.amazonservices.com/en_FR/reports/Reports_RequestReport.html
     */
    public function requestReport($reportType, $marketplaceIdList = [], $startDate = '', $endDate = '', $reportOptions = '')
    {
        $params = array(
            'Action' => 'RequestReport'
        );

        $this->addReportTypeFilter($reportType, $params);
        $this->addMarketplaceIdListFilter($marketplaceIdList, $params);
        $this->addStartDateFilter($startDate, $params);
        $this->addEndDateFilter($endDate, $params);
        $this->addReportOptionsFilter($reportOptions, $params);

        return $this->query($params, true);
    }

    /**
     * @param string $reportType
     * @param string $schedule
     * @param string $scheduleDate
     * @return \Zend_Http_Response
     * @see http://docs.developer.amazonservices.com/en_FR/reports/Reports_ManageReportSchedule.html
     */
    public function manageReportSchedule($reportType, $schedule, $scheduleDate = ''){

        $params = [
            'Action' => 'ManageReportSchedule'
        ];

        $this->addReportTypeFilter($reportType, $params);
        $this->addScheduleFilter($schedule, $params);
        $this->addScheduleDateFilter($scheduleDate, $params);

        return $this->query($params, false);

    }

    /**
     * @param array $reportTypeList
     * @return \Zend_Http_Response
     * @see http://docs.developer.amazonservices.com/en_FR/reports/Reports_GetReportScheduleList.html
     */
    public function getReportScheduleList($reportTypeList = []){

        $params = [
            'Action' => 'GetReportScheduleList'
        ];

        $this->addReportTypeListFilter($reportTypeList, $params);

        return $this->query($params, false);

    }

    /**
     * @param array $reportTypeList
     * @return \Zend_Http_Response
     * @see http://docs.developer.amazonservices.com/en_FR/reports/Reports_GetReportScheduleCount.html
     */
    public function getReportScheduleCount($reportTypeList = []){

        $params = [
            'Action' => 'GetReportScheduleCount'
        ];

        $this->addReportTypeListFilter($reportTypeList, $params);

        return $this->query($params, false);

    }

    /**
     * @param array $reportTypeList
     * @param string $acknowledged
     * @return \Zend_Http_Response
     */
    public function updateReportAcknowledgements($reportTypeList, $acknowledged = ''){

        $params = [
            'Action' => 'UpdateReportAcknowledgements'
        ];

        $this->addReportTypeListFilter($reportTypeList, $params);
        $this->addAcknowledgedFilter($acknowledged, $params);

        return $this->query($params, false);

    }

    /**
     * @param $nextToken
     * @return \Zend_Http_Response
     * @see http://docs.developer.amazonservices.com/en_FR/reports/Reports_UpdateReportAcknowledgements.html
     */
    public function getReportScheduleListByNextToken($nextToken){

        $params = [
            'Action' => 'GetReportScheduleListByNextToken'
        ];

        $this->addNextTokenFilter($nextToken, $params);

        return $this->query($params, false);

    }

    /**
     * @param $nextToken
     * @param $params
     */
    protected function addNextTokenFilter($nextToken, &$params){

        $params['NextToken'] = $nextToken;

    }

    /**
     * @param string $schedule
     * @param array $params
     */
    protected function addScheduleFilter($schedule, &$params){

        $params['Schedule'] = $schedule;

    }

    /**
     * @param string $scheduleDate
     * @param array $params
     */
    protected function addScheduleDateFilter($scheduleDate, &$params){

        if(!empty($scheduleDate)){
            $params['ScheduleDate'] = $$scheduleDate;
        }

    }

    /**
     * @param string $reportType
     * @param $params
     */
    protected function addReportTypeFilter($reportType, &$params){

        $params['ReportType'] = $reportType;

    }

    /**
     * @param string $startDate
     * @param $params
     */
    protected function addStartDateFilter($startDate, &$params){

        if (!empty($startDate)) {
            $params['StartDate'] = $startDate;
        }

    }

    /**
     * @param string $endDate
     * @param $params
     */
    protected function addEndDateFilter($endDate, &$params){

        if (!empty($endDate)) {
            $params['EndDate'] = $endDate;
        }

    }

    /**
     * @param string $reportOptions
     * @param $params
     */
    protected function addReportOptionsFilter($reportOptions, &$params){

        if(!empty($reportOptions)){
            $params['ReportOptions'] = $reportOptions;
        }

    }

    /**
     * @param array $marketplaceIdList
     * @param $params
     */
    protected function addMarketplaceIdListFilter($marketplaceIdList, &$params){

        for($i=0; $i<count($marketplaceIdList); $i++){
            $cpt=$i+1;
            $params['MarketplaceIdList.Id.'.$cpt] = $marketplaceIdList[$i];
        }

    }

    /**
     * @param string $requestedFromDate
     * @param $params
     */
    protected function addRequestedFromDateFilter($requestedFromDate, &$params){

        if(!empty($requestedFromDate)){
            $params['RequestedFromDate'] = $requestedFromDate;
        }

    }

    /**
     * @param string $requestedToDate
     * @param $params
     */
    protected function addRequestedToDateFilter($requestedToDate, &$params){

        if(!empty($requestedToDate)){
            $params['RequestedToDate'] = $requestedToDate;
        }

    }

    /**
     * @param array $reportProcessingStatusList
     * @param $params
     */
    protected function addReportProcessingStatusListFilter($reportProcessingStatusList, &$params){

        for($i=0; $i<count($reportProcessingStatusList); $i++){
            $cpt=$i+1;
            $params['ReportProcessingStatusList.Status.'.$cpt] = $reportProcessingStatusList[$i];
        }

    }

    /**
     * @param array $reportTypeList
     * @param $params
     */
    protected function addReportTypeListFilter($reportTypeList, &$params){

        for($i=0;$i<count($reportTypeList);$i++){
            $cpt=$i+1;
            $params['ReportTypeList.Type.'.$cpt] = $reportTypeList[$i];
        }

    }

    /**
     * @param boolean $acknowledged
     * @param $params
     */
    protected function addAcknowledgedFilter($acknowledged, &$params){

        if(is_bool($acknowledged)){
            $params['Acknowledged'] = ($acknowledged === true) ? 'true' : 'false';
        }

    }

    /**
     * @param string $availableFromDate
     * @param $params
     */
    protected function addAvailableFromDateFilter($availableFromDate, &$params){

        if(!empty($availableFromDate)){
            $params['AvailableFromDate'] = $availableFromDate;
        }

    }

    /**
     * @param string $availableToDate
     * @param $params
     */
    protected function addAvailableToDateFilter($availableToDate, &$params){

        if(!empty($availableToDate)){
            $params['AvailableToDate'] = $availableToDate;
        }

    }

    /**
     * @param int $maxCount
     * @param $params
     */
    protected function addMaxCountFilter($maxCount, &$params){

        if(!empty($maxCount)){
            $params['MaxCount'] = $maxCount;
        }

    }

    /**
     * @param array $reportRequestIdList
     * @param $params
     */
    protected function addReportRequestIdListFilter($reportRequestIdList, &$params){

        for($i=0; $i<count($reportRequestIdList); $i++){
            $cpt=$i+1;
            $params['ReportRequestIdList.Id.'.$cpt] = $reportRequestIdList[$i];
        }

    }
}
