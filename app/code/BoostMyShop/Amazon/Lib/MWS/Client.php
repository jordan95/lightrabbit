<?php namespace BoostMyShop\Amazon\Lib\MWS;

/**
 * Class Client
 *
 * @package   BoostMyShop\Amazon\Lib\MWS
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
abstract class Client {

    const kSignatureMethod = 'HmacSHA256';
    const kSignatureVersion = '2';

    protected $_fp;

    /* @var array */
    protected $_params = array();

    /**
     * @var string
     */
    protected $_marketplaceId;

    /**
     * @var string
     */
    protected $_baseUrl;

    /**
     * @var \Magento\Framework\HTTP\ZendClient
     */
    protected $_client;

    /**
     * @var string
     */
    protected $_secretKey;

    /**
     * @var boolean
     */
    protected $_debug;


    public function __construct(){
        $this->_client = new \Zend\Http\Client();
    }

    /**
     * getVersion
     */
    abstract public function getVersion();

    /**
     * getUri
     */
    abstract public function getUri();

    /**
     * getBeforeQuery
     */
    abstract protected function getBeforeQuery();

    /**
     * @param boolean $debug
     * @return $this
     */
    public function setDebug($debug){

        if(is_bool($debug))
            $this->_debug = $debug;

        return $this;

    }

    /**
     * @return boolean
     */
    public function getDebug(){

        return $this->_debug;

    }

    /**
     * @param string $merchantId
     * @return $this
     */
    public function setMerchantId($merchantId){

        $this->_params['SellerId'] = $merchantId;
        return $this;

    }

    /**
     * @return null|string
     */
    public function getMerchantId(){

        return (isset($this->_params['SellerId']) && !empty($this->_params['SellerId'])) ? $this->_params['SellerId'] : null;

    }

    /**
     * @param string $accessKeyId
     * @return $this
     */
    public function setAccessKeyId($accessKeyId){

        $this->_params['AWSAccessKeyId'] = $accessKeyId;
        return $this;

    }

    /**
     * @return null|string
     */
    public function getAccessKeyId(){

        return (isset($this->_params['AWSAccessKeyId']) && !empty($this->_params['AWSAccessKeyId'])) ? $this->_params['AWSAccessKeyId'] : null;

    }

    /**
     * @param string $secretKey
     * @return $this
     */
    public function setSecretKey($secretKey){

        $this->_secretKey = $secretKey;
        return $this;

    }

    /**
     * @return null|string
     */
    public function getSecretKey(){

        return $this->_secretKey;

    }

    /**
     * @param string $marketplaceId
     * @return $this
     */
    public function setMarketplaceId($marketplaceId){

        $this->_marketplaceId = $marketplaceId;
        return $this;

    }

    /**
     * @return null|string
     */
    public function getMarketplaceId(){

        return $this->_marketplaceId;

    }

    /**
     * @param string $baseUrl
     * @return $this
     */
    public function setBaseUrl($baseUrl){

        $this->_baseUrl = $baseUrl;
        return $this;

    }

    /**
     * @return string
     */
    public function getBaseUrl(){

        return $this->_baseUrl;

    }

    /**
     * @return \Zend_Http_Client
     */
    public function getClient()
    {
        return $this->_client;
    }

    public function reset(){

        $this->_params = [];
        $this->_client->resetParameters();
        return $this;

    }

    /**
     * Send a MWS query (without feed)
     *
     * @param array $params
     * @param bool $flag
     * @return \Zend_Http_Response
     */
    public function query($params, $flag = true)
    {
        $this->init($params, $flag);
        return $this->sendRequest();
    }

    /**
     * Init query
     *
     * @param array $params
     * @param boolean $flag
     */
    protected function init($params, $flag = true)
    {

        if ($flag === true) {
            $this->addMarketPlaceId();
        }

        $this->_params['Version'] = $this->getVersion();
        $this->_params['SignatureMethod'] = self::kSignatureMethod;
        $this->_params['SignatureVersion'] = self::kSignatureVersion;

        $this->_params['Timestamp'] = $this->getAmazonTimestamp();

        // add parameters
        foreach ($params as $k => $v) {
            $this->_params[$k] = $v;
        }

        // sort array parameters
        uksort($this->_params, 'strcmp');

        // add signature
        $this->_params['Signature'] = $this->calculSignature();
    }

    /**
     * Add marketplace id : key depends on operation type...
     */
    protected function addMarketPlaceId()
    {
        $this->_params['MarketplaceId'] = $this->getMarketplaceId();
    }

    /**
     * Calcul signature
     *
     * @return string $signature
     */
    protected function calculSignature()
    {
        $method = 'POST';
        $queryParams = [];

        // url encode parameters names and values
        foreach ($this->_params as $param => $value) {
            $value = str_replace('%7E', '~', rawurlencode($value));
            $queryParams[] = $param . "=" . $value;
        }

        // construct query string
        $query = implode('&', $queryParams);

        // construct signToString
        $stringToSign = $method . "\n";
        $stringToSign .= $this->getBaseUrl() . "\n";
        $stringToSign .= $this->getBeforeQuery() . "\n";
        $stringToSign .= $query;

        // HMAC $signToString with $secretAccessKey and convert result to base64
        return base64_encode(hash_hmac("sha256", $stringToSign, $this->getSecretKey(), true));
    }

    /**
     * Send request
     *
     * @throws Exception
     * @return \Zend_Http_Response $response
     */
    protected function sendRequest()
    {

        $headers = array(
            'User-Agent' => 'Zend_Http_Client Tool/1.0 (Language=PHP/' . phpversion() . '; Platform=Linux/Gentoo)'
        );

        if(!is_null($this->_fp)){

            $content = stream_get_contents($this->_fp);
            fclose($this->_fp);

            $contentMD5 = base64_encode(md5($content, true));
            $headers['Content-Type'] = 'text/xml; charset=iso-8859-1';
            $headers['Content-MD5'] = $contentMD5;
            $headers['Transfert-Encoding'] = 'chunked';

            $this->_client->setRawBody($content);

        }

        $this->_client->setHeaders($headers);
        $this->_client->setUri($this->getUri());
        $this->_client->setOptions(
            [
                'maxredirects' => 0,
                'timeout' => 30,
                'storeresponse' => true
            ]
        );
        $this->_client->setParameterGet($this->_params);
        $this->_client->setMethod('POST');
        $response = $this->_client->send();

        return $response;
    }

    /**
     * @return bool|int
     */
    public function getAmazonTimestamp(){

        $amazonTimestamp = false;

        $domDocument = new \DOMDocument();
        $domDocument->loadXML($this->getAmazonTimestampFeed());
        if ($domDocument->getElementsByTagName('Timestamp')->item(0)) {

            $amazonTimestampNode = $domDocument->getElementsByTagName('Timestamp')->item(0);
            $amazonTimestamp = $amazonTimestampNode->getAttribute('timestamp');

        }

        return ($amazonTimestamp === false) ? time() : $amazonTimestamp;

    }

    /**
     * Get Amazon timestamp
     *
     * @return mixed|string
     */
    protected function getAmazonTimestampFeed()
    {
        $url = 'https://mws.amazonservices.com/';
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $content = curl_exec($curl);
        if (!$content)
            $content = file_get_contents($url);

        return $content;

    }

    /**
     * @return string $lastRequest
     */
    public function getLastRequest(){

        $request = $this->getClient()->getRequest();

        $lastRequest = $request->getHeaders()->toString()."\r\n";
        $lastRequest .= $request->renderRequestLine()."\r\n";
        $lastRequest .= str_replace('&', "\n&", $request->getQuery()->toString())."\r\n";
        $lastRequest .= $request->getContent()."\r\n";

        return $lastRequest;

    }

}