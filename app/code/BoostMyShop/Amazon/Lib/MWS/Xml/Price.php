<?php namespace BoostMyShop\Amazon\Lib\MWS\Xml;

/**
 * Class Price
 *
 * @package   BoostMyShop\Amazon\Lib\MWS\Xml
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Price extends \BoostMyShop\Amazon\Lib\MWS\Xml\Feed {

    /**
     * @return string
     */
    public function getType(){

        return \BoostMyShop\Amazon\Lib\MWS\Feeds::TYPE_PRICING_FEED;

    }

    public function openStream(){

        $this->_domDocument = new \DOMDocument('1.0', 'utf-8');

        $amazonEnvelope =  $this->_domDocument->createElement('AmazonEnvelope', '');

        $xmlxsi =  $this->_domDocument->createAttribute('xmlns:xsi');
        $xmlxsiValue =  $this->_domDocument->createTextNode('http://www.w3.org/2001/XMLSchema-instance');
        $xmlxsi->appendChild($xmlxsiValue);

        $xsi =  $this->_domDocument->createAttribute('xsi:noNamespaceSchemaLocation');
        $xsiValue =  $this->_domDocument->createTextNode('amzn-envelope.xsd');
        $xsi->appendChild($xsiValue);

        $amazonEnvelope->appendChild($xmlxsi);
        $amazonEnvelope->appendChild($xsi);
        $this->_domDocument->appendChild($amazonEnvelope);

        $header =  $this->_domDocument->createElement('Header', '');
        $amazonEnvelope->appendChild($header);

        $documentVersion =  $this->_domDocument->createElement('DocumentVersion', '');
        $txtDocumentVersion =  $this->_domDocument->createTextNode('1.01');
        $documentVersion->appendchild($txtDocumentVersion);

        $header->appendChild($documentVersion);

        $merchantIdentifier =  $this->_domDocument->createElement('MerchantIdentifier');
        $txtMerchantIdentifier =  $this->_domDocument->createTextNode($this->getMerchantIdentifier());
        $merchantIdentifier->appendChild($txtMerchantIdentifier);

        $header->appendChild($merchantIdentifier);

        $messageType =  $this->_domDocument->createElement('MessageType');
        $txtMessageType =  $this->_domDocument->createTextNode('Price');
        $messageType->appendChild($txtMessageType);

        $amazonEnvelope->appendChild($messageType);

    }

    /**
     * @param array $priceItem
     */
    public function happenPrice($priceItem){

        $message = $this->_domDocument->createElement('Message', '');

        $messageId = $this->_domDocument->createElement('MessageID');
        $txtMessageId = $this->_domDocument->createTextNode($this->_domDocument->getElementsByTagName('Message')->length + 1);
        $messageId->appendChild($txtMessageId);
        $message->appendChild($messageId);

        $price = $this->_domDocument->createElement('Price', '');

        $sku = $this->_domDocument->createElement('SKU');
        $skuValue = trim($priceItem['sku']);
        $sku->appendChild($this->_domDocument->createTextNode($skuValue));
        $price->appendChild($sku);

        $standardPrice = $this->_domDocument->createElement('StandardPrice', '');
        $currency = $this->_domDocument->createAttribute('currency');
        $currencyValue = $this->_domDocument->createTextNode($priceItem['currency']);
        $currency->appendChild($currencyValue);
        $standardPrice->appendChild($currency);

        $standardPrice->appendChild($this->_domDocument->createTextNode($priceItem['price']));
        $price->appendChild($standardPrice);

        if(isset($priceItem['sale'])){

            $sale = $this->_domDocument->createElement('Sale');
            $startDate = $this->_domDocument->createElement('StartDate', $priceItem['sale']['startDate']);
            $sale->appendChild($startDate);

            $endDate = $this->_domDocument->createElement('EndDate', $priceItem['sale']['endDate']);
            $sale->appendChild($endDate);

            $salePrice = $this->_domDocument->createElement('SalePrice', $priceItem['sale']['price']);
            $currency = $this->_domDocument->createAttribute('currency');
            $currencyValue = $this->_domDocument->createTextNode($priceItem['currency']);
            $currency->appendChild($currencyValue);
            $salePrice->appendChild($currency);
            $sale->appendChild($salePrice);

            $price->appendChild($sale);

        }

        $message->appendChild($price);
        $this->_domDocument->getElementsByTagName('AmazonEnvelope')->item(0)->appendChild($message);

    }

}