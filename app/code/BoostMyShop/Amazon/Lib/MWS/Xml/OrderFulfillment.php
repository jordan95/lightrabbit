<?php namespace BoostMyShop\Amazon\Lib\MWS\Xml;

/**
 * Class OrderFulfillment
 *
 * @package   BoostMyShop\Amazon\Lib\MWS\Xml
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class OrderFulfillment extends \BoostMyShop\Amazon\Lib\MWS\Xml\Feed {

    /**
     * @return string
     */
    public function getType(){

        return \BoostMyShop\Amazon\Lib\MWS\Feeds::TYPE_ORDER_FULFILLMENT_FEED;

    }

    public function openStream(){

        $this->_domDocument = new \DOMDocument();
        $amazonEnvelope = $this->_domDocument->createElement('AmazonEnvelope', '');
        $xmlns = $this->_domDocument->createAttribute('xmlns:xsi');
        $xmlns->appendChild($this->_domDocument->createTextNode('http://www.w3.org/2001/XMLSchema-instance'));

        $xsi = $this->_domDocument->createAttribute('xsi:noNamespaceSchemaLocation');
        $xsiValue = $this->_domDocument->createTextNode('amzn-envelope.xsd');
        $xsi->appendChild($xsiValue);

        $amazonEnvelope->appendChild($xmlns);
        $amazonEnvelope->appendChild($xsi);
        $this->_domDocument->appendChild($amazonEnvelope);

        $header = $this->_domDocument->createElement('Header', '');
        $documentVersion = $this->_domDocument->createElement('DocumentVersion', '1.01');
        $header->appendChild($documentVersion);

        $merchantIdentifier = $this->_domDocument->createElement('MerchantIdentifier', $this->getMerchantIdentifier());
        $header->appendChild($merchantIdentifier);

        $amazonEnvelope->appendChild($header);

        $messageType = $this->_domDocument->createElement('MessageType', 'OrderFulfillment');
        $amazonEnvelope->appendChild($messageType);

    }

    /**
     * @param string $amazonOrderId
     * @param array $shipments
     */
    public function happenOrderFulfillment($amazonOrderId, $shipments){

        foreach($shipments as $shipment){

            $message = $this->_domDocument->createElement('Message', '');

            $messageId = $this->_domDocument->createElement('MessageID', $this->_domDocument->getElementsByTagName('Message')->length + 1);
            $message->appendChild($messageId);

            $orderFulfillment = $this->_domDocument->createElement('OrderFulfillment', '');

            $amazonOrderIdNode = $this->_domDocument->createElement('AmazonOrderID', $amazonOrderId);
            $orderFulfillment->appendChild($amazonOrderIdNode);

            $fulfillmentDate = $this->_domDocument->createElement('FulfillmentDate', $shipment['FulfillmentDate']);
            $orderFulfillment->appendChild($fulfillmentDate);

            $fulfillmentData = $this->_domDocument->createElement('FulfillmentData', '');

            $carrierCodeValue = $this->retrieveCarrierCode($shipment['CarrierCode']);
            if($carrierCodeValue !== false){

                $carrierCode = $this->_domDocument->createElement('CarrierCode', $carrierCodeValue);
                $fulfillmentData->appendChild($carrierCode);

            }else{

                $carrierName = $this->_domDocument->createElement('CarrierName', $shipment['CarrierName']);
                $fulfillmentData->appendChild($carrierName);

            }

            $shippingMethod = $this->_domDocument->createElement('ShippingMethod', $shipment['ShippingMethod']);
            $fulfillmentData->appendChild($shippingMethod);

            $shipperTrackingNumber = $this->_domDocument->createElement('ShipperTrackingNumber', $shipment['ShipperTrackingNumber']);
            $fulfillmentData->appendChild($shipperTrackingNumber);

            $orderFulfillment->appendChild($fulfillmentData);

            foreach($shipment['products'] as $product){

                $itemNode = $this->_domDocument->createElement('Item', '');

                $amazonOrderItemCode = $this->_domDocument->createElement('AmazonOrderItemCode', $product['AmazonOrderItemCode']);
                $itemNode->appendChild($amazonOrderItemCode);

                /*$merchantOrderItemId = $this->_domDocument->createElement('MerchantOrderItemID', $product['MerchantOrderItemID']);
                $itemNode->appendChild($merchantOrderItemId);*/

                $merchantFulfillmentItemId = $this->_domDocument->createElement('MerchantFulfillmentItemID', $product['MerchantFulfillmentItemID']);
                $itemNode->appendChild($merchantFulfillmentItemId);

                $quantity = $this->_domDocument->createElement('Quantity', (int) $product['Quantity']);
                $itemNode->appendChild($quantity);

                $orderFulfillment->appendChild($itemNode);

            }

            $message->appendChild($orderFulfillment);
            $this->_domDocument->getElementsByTagName('AmazonEnvelope')->item(0)->appendChild($message);

        }

    }

    /**
     * @param string $shipmentCarrierCode
     * @return bool | string
     */
    protected function retrieveCarrierCode($shipmentCarrierCode){

        $carrierNode = null;
        $carrierCode = [];
        $shipmentCarrierCode = strtolower($shipmentCarrierCode);
        $filename = $this->_xsdDirectory.'/amzn-base.xsd';

        if(file_exists($filename)) {
            $content = file_get_contents($filename);
            $xmlBase = new \DOMDocument();
            $xmlBase->loadXML($content);

            $root = $xmlBase->documentElement;
            foreach ($root->getElementsByTagName('element') as $elt) {
                if ($elt->hasAttribute('name') && $elt->getAttribute('name') == 'CarrierCode') {
                    $carrierNode = $elt;
                    break;
                }
            }

            if (!is_null($carrierNode)) {

                foreach ($carrierNode->getElementsByTagName('enumeration') as $enum) {
                    $code = $enum->getAttribute('value');
                    $carrierCode[strtolower($code)] = $code;
                }

            }
        }

        return (array_key_exists($shipmentCarrierCode, $carrierCode)) ? $carrierCode[$shipmentCarrierCode] : false;

    }

}