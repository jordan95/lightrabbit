<?php namespace BoostMyShop\Amazon\Lib\MWS\Xml;

/**
 * Class Inventory
 *
 * @package   BoostMyShop\Amazon\Lib\MWS\Xml
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Inventory extends \BoostMyShop\Amazon\Lib\MWS\Xml\Feed {

    /**
     * @return string
     */
    public function getType(){

        return \BoostMyShop\Amazon\Lib\MWS\Feeds::TYPE_INVENTORY_FEED;

    }

    public function openStream(){

        $this->_domDocument = new \DOMDocument('1.0', 'utf-8');

        $amazonEnvelope = $this->_domDocument->createElement('AmazonEnvelope', '');

        $xmlxsi = $this->_domDocument->createAttribute('xmlns:xsi');
        $xmlxsiValue = $this->_domDocument->createTextNode('http://www.w3.org/2001/XMLSchema-instance');
        $xmlxsi->appendChild($xmlxsiValue);

        $xsi = $this->_domDocument->createAttribute('xsi:noNamespaceSchemaLocation');
        $xsiValue = $this->_domDocument->createTextNode('amzn-envelope.xsd');
        $xsi->appendChild($xsiValue);

        $amazonEnvelope->appendChild($xmlxsi);
        $amazonEnvelope->appendChild($xsi);
        $this->_domDocument->appendChild($amazonEnvelope);

        $header = $this->_domDocument->createElement('Header', '');
        $amazonEnvelope->appendChild($header);

        $documentVersion = $this->_domDocument->createElement('DocumentVersion', '');
        $txtDocumentVersion = $this->_domDocument->createTextNode('1.01');
        $documentVersion->appendchild($txtDocumentVersion);

        $header->appendChild($documentVersion);

        $merchantIdentifier = $this->_domDocument->createElement('MerchantIdentifier');
        $txtMerchantIdentifier = $this->_domDocument->createTextNode($this->getMerchantIdentifier());
        $merchantIdentifier->appendChild($txtMerchantIdentifier);

        $header->appendChild($merchantIdentifier);

        $messageType = $this->_domDocument->createElement('MessageType');
        $txtMessageType = $this->_domDocument->createTextNode('Inventory');
        $messageType->appendChild($txtMessageType);

        $amazonEnvelope->appendChild($messageType);

    }

    /**
     * @param array $inventoryItem
     */
    public function happenInventory($inventoryItem){

        $message = $this->_domDocument->createElement('Message', '');

        $messageId = $this->_domDocument->createElement('MessageID');
        $txtMessageId = $this->_domDocument->createTextNode($this->_domDocument->getElementsByTagName('Message')->length + 1);
        $messageId->appendChild($txtMessageId);
        $message->appendChild($messageId);

        $operationType = $this->_domDocument->createElement('OperationType', '');
        $operationType->appendChild($this->_domDocument->createTextNode('Update'));
        $message->appendchild($operationType);

        $inventory = $this->_domDocument->createElement('Inventory', '');

        $sku = $this->_domDocument->createElement('SKU');
        $skuValue = trim($inventoryItem['sku']);
        $sku->appendChild($this->_domDocument->createTextNode($skuValue));
        $inventory->appendChild($sku);

        $quantity = $this->_domDocument->createElement('Quantity', '');
        $quantity->appendChild($this->_domDocument->createTextNode((int) $inventoryItem['quantity']));
        $inventory->appendChild($quantity);

        $fulfillmentLatency = $this->_domDocument->createElement('FulfillmentLatency', '');
        $fulfillmentLatency->appendChild($this->_domDocument->createTextNode($inventoryItem['fulfillmentLatency']));
        $inventory->appendChild($fulfillmentLatency);

        $message->appendChild($inventory);
        $this->_domDocument->getElementsByTagName('AmazonEnvelope')->item(0)->appendChild($message);

    }

}