<?php namespace BoostMyShop\Amazon\Lib\MWS\Xml;

/**
 * Class DeleteAllCatalog
 *
 * @package   BoostMyShop\Amazon\Lib\MWS\Xml
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class DeleteAllCatalog extends Feed {

    /**
     * @return string
     */
    public function getType(){

        return \BoostMyShop\Amazon\Lib\MWS\Feeds::TYPE_PRODUCT_FEED;

    }

    public function openStream()
    {
        $this->_domDocument = new \DOMDocument();
        $amazonEnvelope = $this->_domDocument->createElement('AmazonEnvelope', '');
        $xmlns = $this->_domDocument->createAttribute('xmlns:xsi');
        $xmlns->appendChild($this->_domDocument->createTextNode('http://www.w3.org/2001/XMLSchema-instance'));

        $xsi = $this->_domDocument->createAttribute('xsi:noNamespaceSchemaLocation');
        $xsiValue = $this->_domDocument->createTextNode('amzn-envelope.xsd');
        $xsi->appendChild($xsiValue);

        $amazonEnvelope->appendChild($xmlns);
        $amazonEnvelope->appendChild($xsi);
        $this->_domDocument->appendChild($amazonEnvelope);

        $header = $this->_domDocument->createElement('Header', '');
        $documentVersion = $this->_domDocument->createElement('DocumentVersion', '1.01');
        $header->appendChild($documentVersion);

        $merchantIdentifier = $this->_domDocument->createElement('MerchantIdentifier', $this->getMerchantIdentifier());
        $header->appendChild($merchantIdentifier);

        $amazonEnvelope->appendChild($header);

        $messageType = $this->_domDocument->createElement('MessageType', 'Product');
        $amazonEnvelope->appendChild($messageType);

        $purgeAndReplace = $this->_domDocument->createElement('PurgeAndReplace', '');
        $purgeAndReplace->appendChild($this->_domDocument->createTextNode('true'));
        $amazonEnvelope->appendChild($purgeAndReplace);

    }

}