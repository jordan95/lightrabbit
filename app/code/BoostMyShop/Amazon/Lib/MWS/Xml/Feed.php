<?php namespace BoostMyShop\Amazon\Lib\MWS\Xml;

/**
 * Class Feed
 *
 * @package   BoostMyShop\Amazon\Lib\MWS\Xml
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
abstract class Feed {

    /**
     * @var \DOMDocument
     */
    protected $_domDocument;

    /**
     * @var string
     */
    protected $_merchantIdentifier;

    /**
     * @var string
     */
    protected $_xsdDirectory;

    abstract function openStream();

    abstract function getType();

    /**
     * @param string $merchantIdentifier
     * @return $this
     */
    public function setMerchantIdentifier($merchantIdentifier){

        $this->_merchantIdentifier = $merchantIdentifier;
        return $this;

    }

    /**
     * @return string
     */
    public function getMerchantIdentifier(){

        return $this->_merchantIdentifier;

    }

    /**
     * @param string $xsdDirectory
     * @return $this
     */
    public function setXsdDirectory($xsdDirectory){
        $this->_xsdDirectory = $xsdDirectory;
        return $this;
    }

    /**
     * @return array|bool
     */
    public function validate(){

        $errors = array();
        libxml_use_internal_errors(true);
        $res = $this->_domDocument->schemaValidate($this->_xsdDirectory.'/amzn-envelope.xsd');

        if($res === false){

            foreach(libxml_get_errors() as $error){

                $errors[] = trim($error->message);

            }
            libxml_clear_errors();

        }
        libxml_use_internal_errors(false);

        return (count($errors) == 0) ? false : $errors;

    }

    /**
     * @return string
     */
    public function closeStream(){

        return $this->_domDocument->saveXML();

    }

}