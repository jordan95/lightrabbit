<?php namespace BoostMyShop\Amazon\Lib\MWS\Xml;

/**
 * Class Matching
 *
 * @package   BoostMyShop\Amazon\Lib\MWS\Xml
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Matching extends Feed {

    /**
     * @return string
     */
    public function getType(){

        return \BoostMyShop\Amazon\Lib\MWS\Feeds::TYPE_PRODUCT_FEED;

    }

    public function openStream()
    {
        $this->_domDocument = new \DOMDocument();
        $amazonEnvelope = $this->_domDocument->createElement('AmazonEnvelope', '');
        $xmlns = $this->_domDocument->createAttribute('xmlns:xsi');
        $xmlns->appendChild($this->_domDocument->createTextNode('http://www.w3.org/2001/XMLSchema-instance'));

        $xsi = $this->_domDocument->createAttribute('xsi:noNamespaceSchemaLocation');
        $xsiValue = $this->_domDocument->createTextNode('amzn-envelope.xsd');
        $xsi->appendChild($xsiValue);

        $amazonEnvelope->appendChild($xmlns);
        $amazonEnvelope->appendChild($xsi);
        $this->_domDocument->appendChild($amazonEnvelope);

        $header = $this->_domDocument->createElement('Header', '');
        $documentVersion = $this->_domDocument->createElement('DocumentVersion', '1.01');
        $header->appendChild($documentVersion);

        $merchantIdentifier = $this->_domDocument->createElement('MerchantIdentifier', $this->getMerchantIdentifier());
        $header->appendChild($merchantIdentifier);

        $amazonEnvelope->appendChild($header);

        $messageType = $this->_domDocument->createElement('MessageType', 'Product');
        $amazonEnvelope->appendChild($messageType);

        $purgeAndReplace = $this->_domDocument->createElement('PurgeAndReplace', '');
        $purgeAndReplace->appendChild($this->_domDocument->createTextNode('false'));
        $amazonEnvelope->appendChild($purgeAndReplace);
    }

    /**
     * @param string $reference
     * @param string $asin
     */
    public function happenProduct($reference, $asin){

        $message = $this->_domDocument->createElement('Message', '');
        $messageId = $this->_domDocument->createElement('MessageID', '');
        $messageId->appendChild($this->_domDocument->createTextNode($this->_domDocument->getElementsByTagName('Message')->length + 1));
        $message->appendChild($messageId);

        $operationType = $this->_domDocument->createElement('OperationType', '');
        $operationType->appendChild($this->_domDocument->createTextNode('Update'));
        $message->appendChild($operationType);

        $product = $this->_domDocument->createElement('Product', '');

        $sku = $this->_domDocument->createElement('SKU', '');
        $sku->appendChild($this->_domDocument->createTextNode($reference));
        $product->appendChild($sku);

        $standardProductId = $this->_domDocument->createElement('StandardProductID', '');

        $type = $this->_domDocument->createElement('Type', '');
        $type->appendChild($this->_domDocument->createTextNode('ASIN'));
        $standardProductId->appendChild($type);

        $value = $this->_domDocument->createElement('Value', '');
        $value->appendChild($this->_domDocument->createTextNode($asin));
        $standardProductId->appendChild($value);

        $product->appendChild($standardProductId);
        $message->appendChild($product);
        $this->_domDocument->getElementsByTagName('AmazonEnvelope')->item(0)->appendChild($message);

    }

}