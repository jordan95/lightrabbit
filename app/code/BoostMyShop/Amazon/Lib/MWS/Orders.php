<?php namespace BoostMyShop\Amazon\Lib\MWS;

/**
 * Class Order
 *
 * @package   BoostMyShop\Amazon\Lib\MWS
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Orders extends \BoostMyShop\Amazon\Lib\MWS\Client {

    const kVersion = '2013-09-01';

    const LIST_ORDERS_REQUEST_QUOTA = 6;
    const LIST_ORDER_ITEMS_REQUEST_QUOTA = 30;

    const ORDER_STATUS_PENDING_AVAILABILITY = 'PendingAvailability';
    const ORDER_STATUS_PENDING = 'Pending';
    const ORDER_STATUS_UN_SHIPPED = 'Unshipped';
    const ORDER_STATUS_PARTIALLY_SHIPPED = 'PartiallyShipped';
    const ORDER_STATUS_SHIPPED = 'Shipped';
    const ORDER_STATUS_INVOICE_UNCONFIRMED = 'InvoiceUnconfirmed';
    const ORDER_STATUS_CANCELED = 'Canceled';
    const ORDER_STATUS_UNFULFILLABLE = 'Unfulfillable';

    const FULFILLMENT_CHANNEL_MFN = 'MFN';
    const FULFILLMENT_CHANNEL_AFN = 'AFN';

    const PAYMENT_METHOD_COD = 'COD'; // Cash on delivery
    const PAYMENT_METHOD_CVS = 'CVS'; // Convenience store payment
    const PAYMENT_METHOD_OTHER = 'Other';

    /**
     * Get version
     *
     * @return string
     */
    public function getVersion()
    {
        return self::kVersion;
    }

    /**
     * Get before query
     *
     * @return string
     */
    protected function getBeforeQuery()
    {
        return '/Orders/'.$this->getVersion();
    }

    /**
     * Get uri
     *
     * @return string
     */
    public function getUri()
    {
        return 'https://'.$this->getBaseUrl().$this->getBeforeQuery();
    }

    /**
     * Add marketplace id data in request parameters
     */
    protected function addMarketPlaceId()
    {
        $this->_params['MarketplaceId.Id.1'] = $this->getMarketplaceId();
    }

    /**
     * List orders
     *
     * @param array $createdDates
     * @param array $updatedDates
     * @param array $statuses
     * @param array $fulfillmentChannels
     * @param null $sellerOrderId
     * @param null $buyerEmail
     * @param array $paymentMethods
     * @param array $tfmShipmentStatuses
     * @param int $maxResultsPerPage
     * @return string
     * @see http://docs.developer.amazonservices.com/en_FR/orders-2013-09-01/Orders_ListOrders.html
     */
    public function listOrders(
        $createdDates = array(),
        $updatedDates = array(),
        $statuses = array(),
        $fulfillmentChannels = array(),
        $sellerOrderId = null,
        $buyerEmail = null,
        $paymentMethods = array(),
        $tfmShipmentStatuses = array(),
        $maxResultsPerPage = 50
    ){
        $params = array();
        $params['Action'] = 'ListOrders';
        $this->addCreatedDatesRange($createdDates, $params);
        $this->addUpdatedDatesRange($updatedDates, $params);
        $this->addStatusFilter($statuses, $params);
        $this->addFulfillmentChannelsFilter($fulfillmentChannels, $params);
        $this->addSellerOrderIdFilter($sellerOrderId, $params);
        $this->addBuyerEmailFilter($buyerEmail, $params);
        $this->addPaymentMethodsFilter($paymentMethods, $params);
        $this->addTfmShipmentStatusFilter($tfmShipmentStatuses, $params);
        $this->addMaxResultsPerPage($maxResultsPerPage, $params);

        return $this->query($params, true);

    }

    /**
     * Add created dates range filter
     *
     * @param array $createdDates
     * @param array $params
     */
    protected function addCreatedDatesRange($createdDates, &$params){

        foreach(array('CreatedAfter', 'CreatedBefore') as $filter){
            if(isset($createdDates[$filter])){
                $params[$filter] = $createdDates[$filter];
            }
        }

    }

    /**
     * Add last updated dates range filter
     *
     * @param array $updatedDates
     * @param array $params
     */
    protected function addUpdatedDatesRange($updatedDates, &$params){

        foreach(array('LastUpdateAfter', 'LastUpdateBefore') as $filter){
            if(isset($updatedDates[$filter])){
                $params[$filter] = $updatedDates[$filter];
            }
        }

    }

    /**
     * Add status filter
     *
     * @param array $statuses
     * @param array $params
     */
    protected function addStatusFilter($statuses, &$params){

        $allowed = array('Pending', 'Unshipped', 'PartiallyShipped', 'Shipped', 'Canceled', 'UnFulfillable', 'PendingAvailability');
        $cpt = 1;
        foreach($statuses as $status){

            if(in_array($status, $allowed)){
                $params['OrderStatus.Status.'.$cpt] = $status;
                $cpt++;
            }

        }

    }

    /**
     * Add fulfillment channels filter
     *
     * @param array $fulfillmentChannels
     * @param array $params
     */
    protected function addFulfillmentChannelsFilter($fulfillmentChannels, &$params){

        $allowed = array('MFN', 'AFN');
        $cpt = 1;

        foreach($fulfillmentChannels as $channel){

            if(in_array($channel, $allowed)){
                $params['FulfillmentChannel.Channel.'.$cpt] = $channel;
                $cpt++;
            }

        }

    }

    /**
     * Add seller order ID filter
     *
     * @param string $sellerOrderId
     * @param array $params
     */
    protected function addSellerOrderIdFilter($sellerOrderId, &$params){

        if($sellerOrderId !== null){
            $params['SellerOrderId'] = $sellerOrderId;
        }

    }

    /**
     * Add buyer email filter
     *
     * @param string $buyerEmail
     * @param array $params
     */
    protected function addBuyerEmailFilter($buyerEmail, &$params){

        if($buyerEmail !== null){
            $params['BuyerEmail'] = $buyerEmail;
        }

    }

    /**
     * Add payment methods filter
     *
     * @param array $paymentMethods
     * @param array $params
     */
    protected function addPaymentMethodsFilter($paymentMethods, &$params){

        $allowed = array('COD', 'CVS', 'Other');
        $cpt = 1;

        foreach($paymentMethods as $method){

            if(in_array($method, $allowed)){

                $params['PaymentMethod.Method.'.$cpt] = $method;
                $cpt++;

            }

        }

    }

    /**
     * Add TFM Shipment status filter
     *
     * @param array $tfmShipmentStatuses
     * @param array $params
     */
    protected function addTfmShipmentStatusFilter($tfmShipmentStatuses, &$params){

        $allowed = array('PendingPickUp', 'LabelCanceled', 'PickedUp', 'AtDestinationFC', 'Delivered', 'RejectedByBuyer', 'Undeliverable', 'ReturnedToSeller', 'Lost');
        $cpt = 1;

        foreach($tfmShipmentStatuses as $status){

            if(in_array($status, $allowed)) {

                $params['TFMShipmentStatus.Status.' . $cpt] = $status;
                $cpt++;

            }

        }

    }

    /**
     * Add max results per page (default 100)
     *
     * @param int $maxResultsPerPage
     * @param array $params
     */
    protected function addMaxResultsPerPage($maxResultsPerPage, &$params){


        if(!empty($maxResultsPerPage) && $maxResultsPerPage >= 1 && $maxResultsPerPage <= 100) {
            $params['MaxResultsPerPage'] = $maxResultsPerPage;
        }

    }

    /**
     * List Orders by next token
     *
     * @param string $nextToken
     * @return string
     * @see http://docs.developer.amazonservices.com/en_FR/orders-2013-09-01/Orders_ListOrdersByNextToken.html
     */
    public function listOrdersByNextToken($nextToken){

        $params = array(
            'Action' => 'ListOrdersByNextToken',
            'NextToken' => $nextToken
        );
        return $this->query($params, true);

    }

    /**
     * Get order
     *
     * @param array $amazonOrderIds
     * @return string
     * @see http://docs.developer.amazonservices.com/en_FR/orders-2013-09-01/Orders_GetOrder.html
     */
    public function getOrder($amazonOrderIds){

        $params = array(
            'Action' => 'GetOrder'
        );
        $cpt = 1;

        foreach($amazonOrderIds as $amazonOrderId){

            $params['AmazonOrderId.Id.'.$cpt] = $amazonOrderId;

        }
        return $this->query($params, true);

    }

    /**
     * List order items
     *
     * @param string $amazonOrderId
     * @return string
     * @see http://docs.developer.amazonservices.com/en_FR/orders-2013-09-01/Orders_ListOrderItems.html
     */
    public function listOrderItems($amazonOrderId){

        $params = array(
            'Action' => 'ListOrderItems',
            'AmazonOrderId' => $amazonOrderId
        );
        return $this->query($params, true);

    }

    /**
     * List order items by next token
     *
     * @param string $nextToken
     * @return string
     * @see http://docs.developer.amazonservices.com/en_FR/orders-2013-09-01/Orders_ListOrderItemsByNextToken.html
     */
    public function listOrderItemsByNextToken($nextToken){

        $params = array(
            'Action' => 'ListOrderItemsNyNextToken',
            'NextToken' => $nextToken
        );
        return $this->query($params, true);

    }

}
