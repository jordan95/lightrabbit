<?php namespace BoostMyShop\Amazon\Lib\MWS;

/**
 * Class Feed
 *
 * @package   BoostMyShop\Amazon\Lib\MWS
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @see http://docs.developer.amazonservices.com/en_FR/feeds/Feeds_Overview.html
 */
class Feeds extends \BoostMyShop\Amazon\Lib\MWS\Client {

    const FEED_PROCESSING_STATUS_SUBMITTED = '_SUBMITTED_';
    const FEED_PROCESSING_STATUS_IN_PROGRESS = '_IN_PROGRESS_';
    const FEED_PROCESSING_STATUS_CANCELED = '_CANCELLED_';
    const FEED_PROCESSING_STATUS_DONE = '_DONE_';
    const FEED_PROCESSING_STATUS_AWAITING_ASYNCHRONOUS_REPLY = '_AWAITING_ASYNCHRONOUS_REPLY_';
    const FEED_PROCESSING_STATUS_IN_SAFETY_NET = '_IN_SAFETY_NET_';
    const FEED_PROCESSING_UNCONFIRMED = '_UNCONFIRMED_';

    const TYPE_PRODUCT_FEED = '_POST_PRODUCT_DATA_';
    const TYPE_INVENTORY_FEED = '_POST_INVENTORY_AVAILABILITY_DATA_';
    const TYPE_OVERRIDES_FEED = '_POST_PRODUCT_OVERRIDES_DATA_';
    const TYPE_PRICING_FEED = '_POST_PRODUCT_PRICING_DATA_';
    const TYPE_PRODUCT_IMAGES_FEED = '_POST_PRODUCT_IMAGE_DATA_';
    const TYPE_RELATIONSHIPS_FEED = '_POST_PRODUCT_RELATIONSHIP_DATA_';

    const TYPE_ORDER_ACKNOWLEDGEMENT_FEED = '_POST_ORDER_ACKNOWLEDGEMENT_DATA_';
    const TYPE_ORDER_ADJUSTMENTS_FEED = '_POST_PAYMENT_ADJUSTMENT_DATA_';
    const TYPE_ORDER_FULFILLMENT_FEED = '_POST_ORDER_FULFILLMENT_DATA_';

    const kVersion = '2009-01-01';

    protected $_feedSubmissionId;

    public function getFeedSubmissionId(){

        return $this->_feedSubmissionId;

    }

    /**
     * Get uri
     *
     * @return string
     */
    public function getUri()
    {
        return 'https://'.$this->getBaseUrl().$this->getBeforeQuery();
    }

    /**
     * Get before query
     *
     * @return string
     */
    protected function getBeforeQuery()
    {
        return '/';
    }

    /**
     * Get version
     *
     * @return string
     */
    public function getVersion()
    {
        return self::kVersion;
    }

    /**
     * Add marketplace id
     */
    protected function _addMarketPlaceId()
    {
        $this->_params['MarketplaceIdList.Id.1'] = $this->getMarketplaceId();
    }

    /**
     * @param string $content
     * @return $this
     */
    public function setContent($content){

        $this->_fp = fopen('php://temp', 'rw+');
        fputs($this->_fp, $content);
        rewind($this->_fp);

        return $this;

    }

    /**
     * @param array $feedSubmissionIdList
     * @param array $feedTypeList
     * @param string $submittedFromDate
     * @param string $submittedToDate
     * @return \Zend_Http_Response
     * @see http://docs.developer.amazonservices.com/en_FR/feeds/Feeds_CancelFeedSubmissions.html
     */
    public function cancelFeedSubmissions($feedSubmissionIdList = [], $feedTypeList = [], $submittedFromDate = '', $submittedToDate = ''){

        $params = [
            'Action' => 'CancelFeedSubmissions'
        ];

        $this->addFeedSubmissionIdListFilter($feedSubmissionIdList, $params);
        $this->addFeedTypeListFilter($feedTypeList, $params);
        $this->addSubmittedFromDateFilter($submittedFromDate, $params);
        $this->addSubmittedToDateFilter($submittedToDate, $params);

        return $this->query($params);

    }

    /**
     * @param string $submittedFromDate
     * @param string $submittedToDate
     * @param array $feedTypeList
     * @param array $feedProcessingStatusList
     * @param null $maxCount
     * @param array $feedSubmissionIdList
     * @return \Zend_Http_Response
     * @see http://docs.developer.amazonservices.com/en_FR/feeds/Feeds_GetFeedSubmissionList.html
     */
    public function getFeedSubmissionList($submittedFromDate = '', $submittedToDate = '', $feedTypeList = [], $feedProcessingStatusList = [], $maxCount = null, $feedSubmissionIdList = []){

        $params = [
            'Action' => 'GetFeedSubmissionList'
        ];

        $this->addSubmittedFromDateFilter($submittedFromDate, $params);
        $this->addSubmittedToDateFilter($submittedToDate, $params);
        $this->addFeedTypeListFilter($feedTypeList, $params);
        $this->addFeedProcessingStatusListFilter($feedProcessingStatusList, $params);
        $this->addMaxCountFilter($maxCount, $params);
        $this->addFeedSubmissionIdListFilter($feedSubmissionIdList, $params);

        return $this->query($params);

    }

    /**
     * @param string $nextToken
     * @return \Zend_Http_Response
     * @see http://docs.developer.amazonservices.com/en_FR/feeds/Feeds_GetFeedSubmissionListByNextToken.html
     */
    public function getFeedSubmissionListByNextToken($nextToken){

        $params = [
            'Action' => 'GetFeedSubmissionListByNextToken',
            'NextToken' => $nextToken
        ];

        return $this->query($params);

    }

    /**
     * @param string $submittedFromDate
     * @param string $submittedToDate
     * @param array $feedTypeList
     * @param array $feedProcessingStatusList
     * @return \Zend_Http_Response
     * @see http://docs.developer.amazonservices.com/en_FR/feeds/Feeds_GetFeedSubmissionCount.html
     */
    public function getFeedSubmissionCount($submittedFromDate = '', $submittedToDate = '', $feedTypeList = [], $feedProcessingStatusList = []){

        $params = [
            'Action' => 'GetFeedSubmissionCount'
        ];

        $this->addSubmittedFromDateFilter($submittedFromDate, $params);
        $this->addSubmittedToDateFilter($submittedToDate, $params);
        $this->addFeedTypeListFilter($feedTypeList, $params);
        $this->addFeedProcessingStatusListFilter($feedProcessingStatusList, $params);

        return $this->query($params);

    }

    /**
     * Get feed submission result
     *
     * @param int $feedSubmissionId
     * @return string
     * @see http://docs.developer.amazonservices.com/en_FR/feeds/Feeds_GetFeedSubmissionResult.html
     */
    public function getFeedSubmissionResult($feedSubmissionId)
    {
        $params = array(
            'Action' => 'GetFeedSubmissionResult',
            'FeedSubmissionId' => $feedSubmissionId
        );

        return $this->query($params);
    }

    /**
     * Submit feed
     *
     * @param string $type
     * @return \Zend_Http_Response
     * @see http://docs.developer.amazonservices.com/en_FR/feeds/Feeds_SubmitFeed.html
     */
    public function submitFeed($type)
    {
        $response = $this->query(array('Action' => 'SubmitFeed', 'FeedType' => $type));
        if($response->getStatusCode() == 200){
            $xml = new \DomDocument();
            $xml->loadXML($response->getBody());
            $this->_feedSubmissionId = $xml->getElementsByTagName('FeedSubmissionId')->item(0)->nodeValue;
        }
        return $response;
    }

    /**
     * @param string $submittedFromDate
     * @param array $params
     */
    protected function addSubmittedFromDateFilter($submittedFromDate, &$params){

        if(!empty($submittedFromDate)){
            $params['SubmittedFromDate'] = $submittedFromDate;
        }

    }

    /**
     * @param string $submittedToDate
     * @param array $params
     */
    protected function addSubmittedToDateFilter($submittedToDate, &$params){

        if(!empty($submittedToDate)){
            $params['SubmittedToDate'] = $submittedToDate;
        }

    }

    /**
     * @param array $feedTypeList
     * @param array $params
     */
    protected function addFeedTypeListFilter($feedTypeList, &$params){

        for($i=0;$i<count($feedTypeList);$i++){
            $cpt=$i+1;
            $params['FeedTypeList.Type.'.$cpt] = $feedTypeList[$i];
        }

    }

    /**
     * @param array $feedSubmissionIdList
     * @param array $params
     */
    protected function addFeedSubmissionIdListFilter($feedSubmissionIdList, &$params){

        for($i=0;$i<count($feedSubmissionIdList);$i++){
            $cpt=$i+1;
            $params['FeedSubmissionIdList.Id.'.$cpt] = $feedSubmissionIdList[$i];
        }

    }

    /**
     * @param array $feedProcessingStatusList
     * @param array $params
     */
    protected function addFeedProcessingStatusListFilter($feedProcessingStatusList, &$params){

        for($i=0;$i<count($feedProcessingStatusList);$i++){
            $cpt=$i+1;
            $params['FeedProcessingStatusList.Status.'.$cpt] = $feedProcessingStatusList[$i];
        }

    }

    /**
     * @param int $maxCount
     * @param array $params
     */
    protected function addMaxCountFilter($maxCount, &$params){

        if(!empty($maxCount)){
            $params['MaxCount'] = $maxCount;
        }

    }

}