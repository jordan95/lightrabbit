<?php namespace BoostMyShop\Amazon\Lib\MWS;

/**
 * Class Seller
 *
 * @package   BoostMyShop\Lib\MWS
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Sellers extends \BoostMyShop\Amazon\Lib\MWS\Client {

    const kVersion = '2011-07-01';

    /**
     * Get before query
     *
     * @return string
     */
    protected function getBeforeQuery()
    {
        return '/Sellers/'.$this->getVersion();
    }

    /**
     * Get uri
     *
     * @return string
     */
    public function getUri()
    {
        return ('https://'.$this->getBaseUrl().$this->getBeforeQuery());
    }

    /**
     * Get version
     *
     * @return string
     */
    public function getVersion()
    {
        return self::kVersion;
    }

    /**
     * List marketplace participations
     *
     * @return \Zend_Http_Response
     * @see http://docs.developer.amazonservices.com/en_FR/sellers/Sellers_ListMarketplaceParticipations.html
     */
    public function listMarketplaceParticipations()
    {
        return $this->query(array('Action' => 'ListMarketplaceParticipations'), false);
    }

    /**
     * @param $nextToken
     * @return \Zend_Http_Response
     * @see http://docs.developer.amazonservices.com/en_FR/sellers/Sellers_ListMarketplaceParticipationsByNextToken.html
     */
    public function listMarketplaceParticipationsByNextToken($nextToken){

        $params = [
            'Action' => 'ListMarketplaceParticipationByNextToken',
            'NextToken' => $nextToken
        ];

        return $this->query($params, false);

    }

}