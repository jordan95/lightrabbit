<?php namespace BoostMyShop\Amazon\Lib\MWS;

/**
 * Class Product
 *
 * @package   BoostMyShop\Amazon\Lib\MWS
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @see http://docs.developer.amazonservices.com/en_FR/products/Products_Overview.html
 */
class Products extends \BoostMyShop\Amazon\Lib\MWS\Client {

    const kVersion = '2011-10-01';

    /**
     * Get before query
     *
     * @return string
     */
    protected function getBeforeQuery()
    {
        return '/Products/'.$this->getVersion();
    }

    /**
     * Get uri
     *
     * @return string
     */
    public function getUri()
    {
        return 'https://'.$this->getBaseUrl().$this->getBeforeQuery();
    }

    /**
     * Get version
     *
     * @return string
     */
    public function getVersion()
    {
        return self::kVersion;
    }

    /**
     * @param string $query
     * @param string $queryContextId
     * @return \Zend_Http_Response
     * @see http://docs.developer.amazonservices.com/en_FR/products/Products_ListMatchingProducts.html
     */
    public function listMatchingProducts($query, $queryContextId = ''){

        $params = [
            'Action' => 'ListMatchingProducts'
        ];

        $this->addQueryParam($query, $params);
        $this->addQueryContextIdParam($queryContextId, $params);

        return $this->query($params);

    }

    /**
     * @param array $asinList
     * @return \Zend_Http_Response
     * @see http://docs.developer.amazonservices.com/en_FR/products/Products_GetMatchingProduct.html
     */
    public function getMatchingProduct($asinList){

        $params = [
            'Action' => 'GetMatchingProduct'
        ];

        $this->addAsinListFilter($asinList, $params);

        return $this->query($params);

    }

    /**
     * @param string $idType
     * @param array $idList
     * @return \Zend_Http_Response
     * @see http://docs.developer.amazonservices.com/en_FR/products/Products_GetMatchingProductForId.html
     */
    public function getMatchingProductForId($idType, $idList){

        $params = [
            'Action' => 'GetMatchingProductForId'
        ];

        $this->addIdTypeParam($idType, $params);
        $this->addIdListParam($idList, $params);

        return $this->query($params);

    }

    /**
     * @param array $sellerSkuList
     * @return \Zend_Http_Response
     * @see http://docs.developer.amazonservices.com/en_FR/products/Products_GetCompetitivePricingForSKU.html
     */
    public function getCompetitivePricingForSku($sellerSkuList){

        $params = [
            'Action' => 'GetCompetitivePricingForSKU'
        ];

        $this->addSellerSkuListParam($sellerSkuList, $params);

        return $this->query($params);

    }

    /**
     * @param array $asinList
     * @return \Zend_Http_Response
     * @see http://docs.developer.amazonservices.com/en_FR/products/Products_GetCompetitivePricingForASIN.html
     */
    public function getCompetitivePricingForAsin($asinList){

        $params = [
            'Action' => 'GetCompetitivePricingForASIN'
        ];

        $this->addAsinListFilter($asinList, $params);

        return $this->query($params);

    }

    /**
     * @param string $sellerSku
     * @param string $itemCondition
     * @return \Zend_Http_Response
     * @see http://docs.developer.amazonservices.com/en_FR/products/Products_GetLowestPricedOffersForSKU.html
     */
    public function getLowestPricedOffersForSku($sellerSku, $itemCondition){

        $params = [
            'Action' => 'GetLowestPricedOffersForSKU'
        ];

        $this->addSellerSkuParam($sellerSku, $params);
        $this->addItemConditionParam($itemCondition, $params);

        return $this->query($params);

    }

    /**
     * @param string $asin
     * @param string $itemCondition
     * @return \Zend_Http_Response
     * @see http://docs.developer.amazonservices.com/en_FR/products/Products_GetLowestPricedOffersForASIN.html
     */
    public function getLowestPricedOffersForAsin($asin, $itemCondition){

        $params = [
            'Action' => 'GetLowestPricedOffersForASIN'
        ];

        $this->addAsinParam($asin, $params);
        $this->addItemConditionParam($itemCondition, $params);

        return $this->query($params);

    }

    /**
     * @param array $sellerSkuList
     * @return \Zend_Http_Response
     * @see http://docs.developer.amazonservices.com/en_FR/products/Products_GetLowestOfferListingsForSKU.html
     */
    public function getLowestOfferListingsForSku($sellerSkuList){

        $params = [
            'Action' => 'GetLowestOfferListingsForSKU'
        ];

        $this->addSellerSkuListParam($sellerSkuList, $params);

        return $this->query($params);

    }

    /**
     * @param array $asinList
     * @return \Zend_Http_Response
     * @see http://docs.developer.amazonservices.com/en_FR/products/Products_GetLowestOfferListingsForASIN.html
     */
    public function getLowestOfferListingsForAsin($asinList){

        $params = [
            'Action' => 'GetLowestOfferListingsForASIN'
        ];

        $this->addAsinListFilter($asinList, $params);

        return $this->query($params);

    }

    /**
     * @todo (lot of parameters...)
     * @see http://docs.developer.amazonservices.com/en_FR/products/Products_GetMyFeesEstimate.html
     */
    public function getMyFeesEstimate(){

    }

    /**
     * @param array $sellerSkuList
     * @return \Zend_Http_Response
     * @see http://docs.developer.amazonservices.com/en_FR/products/Products_GetMyPriceForSKU.html
     */
    public function getMyPriceForSku($sellerSkuList){

        $params = [
            'Action' => 'GetMyPriceForSKU'
        ];

        $this->addSellerSkuListParam($sellerSkuList, $params);

        return $this->query($params);

    }

    /**
     * @param array $asinList
     * @return \Zend_Http_Response
     * @see http://docs.developer.amazonservices.com/en_FR/products/Products_GetMyPriceForASIN.html
     */
    public function getMyPriceForAsin($asinList){

        $params = [
            'Action' => 'GetMyPriceForASIN'
        ];

        $this->addAsinListFilter($asinList, $params);

        return $this->query($params);

    }

    /**
     * @param string $sellerSku
     * @return \Zend_Http_Response
     * @see http://docs.developer.amazonservices.com/en_FR/products/Products_GetProductCategoriesForSKU.html
     */
    public function getProductCategoriesForSku($sellerSku){

        $params = [
            'Action' => 'GetProductCategoriesForSKU'
        ];

        $this->addSellerSkuParam($sellerSku, $params);

        return $this->query($params);

    }

    /**
     * @param string $asin
     * @return \Zend_Http_Response
     * @see http://docs.developer.amazonservices.com/en_FR/products/Products_GetProductCategoriesForASIN.html
     */
    public function getProductCategoriesForAsin($asin){

        $params = [
            'Action' => 'GetProductCategoriesForASIN'
        ];

        $this->addAsinParam($asin, $params);

        return $this->query($params);

    }

    /**
     * @param string $asin
     * @param array $params
     */
    protected function addAsinParam($asin, &$params){

        $params['ASIN'] = $asin;

    }

    /**
     * @param string $sellerSku
     * @param array $params
     */
    protected function addSellerSkuParam($sellerSku, &$params){

        $params['SellerSKU'] = $sellerSku;

    }

    /**
     * @param string $itemCondition
     * @param array $params
     */
    protected function addItemConditionParam($itemCondition, &$params){

        $params['ItemCondition'] = $itemCondition;

    }

    /**
     * @param array $sellerSkuList
     * @param array $params
     */
    protected function addSellerSkuListParam($sellerSkuList, &$params){

        for($i=0;$i<count($sellerSkuList);$i++){
            $cpt=$i+1;
            $params['SellerSKUList.SellerSKU.'.$cpt] = $sellerSkuList[$i];
        }

    }

    /**
     * @param string $idType
     * @param array $params
     */
    protected function addIdTypeParam($idType, &$params){

        $params['IdType'] = $idType;

    }

    /**
     * @param array $idList
     * @param array $params
     */
    protected function addIdListParam($idList, &$params){

        for($i=0;$i<count($idList);$i++){
            $cpt=$i+1;
            $params['IdList.Id.'.$cpt] = $idList[$i];
        }

    }

    /**
     * @param array $asinList
     * @param array $params
     */
    protected function addAsinListFilter($asinList, &$params){

        for($i=0;$i<count($asinList);$i++){
            $cpt=$i+1;
            $params['ASINList.ASIN.'.$cpt] = $asinList[$i];
        }

    }

    /**
     * @param string $query
     * @param array $params
     */
    protected function addQueryParam($query, &$params){

        $params['Query'] = $query;

    }

    /**
     * @param string $queryContextId
     * @param array $params
     */
    protected function addQueryContextIdParam($queryContextId, &$params){

        if(!empty($queryContextId)){
            $params['QueryContextId'] = $queryContextId;
        }

    }

}