<?php namespace BoostMyShop\Amazon\Controller\Adminhtml\Dashboard;

/**
 * Class Index
 *
 * @package   BoostMyShop\Amazon\Controller\Adminhtml\Dashboard
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Index extends \Magento\Backend\App\AbstractAction {

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_registry;

    /**
     * @var \BoostMyShop\Amazon\Model\Account\Country
     */
    protected $_country;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $_resultPageFactory;

    /**
     * Index constructor.
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \BoostMyShop\Amazon\Model\Account\Country $country
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Backend\App\Action\Context $context
     */
    public function __construct(
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \BoostMyShop\Amazon\Model\Account\Country $country,
        \Magento\Framework\Registry $registry,
        \Magento\Backend\App\Action\Context $context
    ){
        parent::__construct($context);
        $this->_registry = $registry;
        $this->_country = $country;
        $this->_resultPageFactory = $resultPageFactory;
    }

    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute(){

        $accountCountryId = $this->getRequest()->getParam('account_country_id', false);

        $this->_registry->register('account_country_id', $accountCountryId);

        $resultPage = $this->_resultPageFactory->create();
        $resultPage->getLayout()->getBlock('dashboard.country.switcher')->setRedirectUrl('amazon/dashboard/index');
        $resultPage->getConfig()->getTitle()->prepend(__('Dashboard'));

        return $resultPage;

    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('BoostMyShop_Amazon::dashboard');
    }

}