<?php namespace BoostMyShop\Amazon\Controller\Adminhtml;

/**
 * Class Account
 *
 * @package   BoostMyShop\Amazon\Controller\Adminhtml
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
abstract class Account extends \Magento\Backend\App\AbstractAction {

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * @var \BoostMyShop\Amazon\Model\AccountFactory
     */
    protected $_accountFactory;

    /**
     * Account constructor.
     * @param \BoostMyShop\Amazon\Model\AccountFactory $accountFactory
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Backend\App\Action\Context $context
     */
    public function __construct(
        \BoostMyShop\Amazon\Model\AccountFactory $accountFactory,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Backend\App\Action\Context $context
    ){
        parent::__construct($context);
        $this->_coreRegistry = $coreRegistry;
        $this->_accountFactory = $accountFactory;
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('BoostMyShop_Amazon::account');
    }

}