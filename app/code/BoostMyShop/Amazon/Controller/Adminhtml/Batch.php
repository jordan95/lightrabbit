<?php namespace BoostMyShop\Amazon\Controller\Adminhtml;

/**
 * Class Batch
 *
 * @package   BoostMyShop\Amazon\Controller\Adminhtml
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
abstract class Batch extends \Magento\Backend\App\AbstractAction {

    /**
     * @var \BoostMyShop\Amazon\Model\BatchFactory
     */
    protected $_batchFactory;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * Batch constructor.
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \BoostMyShop\Amazon\Model\BatchFactory $batchFactory
     * @param \Magento\Backend\App\Action\Context $context
     */
    public function __construct(
        \Magento\Framework\Registry $coreRegistry,
        \BoostMyShop\Amazon\Model\BatchFactory $batchFactory,
        \Magento\Backend\App\Action\Context $context
    ){
        parent::__construct($context);
        $this->_batchFactory = $batchFactory;
        $this->_coreRegistry = $coreRegistry;
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('BoostMyShop_Amazon::batch');
    }

}