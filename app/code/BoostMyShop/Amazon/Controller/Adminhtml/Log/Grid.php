<?php namespace BoostMyShop\Amazon\Controller\Adminhtml\Log;

/**
 * Class Grid
 *
 * @package   BoostMyShop\Amazon\Controller\Adminhtml\Log
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Grid extends \BoostMyShop\Amazon\Controller\Adminhtml\Log {

    public function execute()
    {

        if($batchId = filter_var($this->getRequest()->getParam('batchId'))){

            $this->_coreRegistry->register('bms_amazon_current_batch', $this->_batchFactory->create()->load($batchId));

        }

        $this->_view->loadLayout(false);
        $this->_view->renderLayout();
    }

}