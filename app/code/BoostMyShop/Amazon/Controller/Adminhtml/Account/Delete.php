<?php namespace BoostMyShop\Amazon\Controller\Adminhtml\Account;

/**
 * Class Delete
 *
 * @package   BoostMyShop\Amazon\Controller\Adminhtml\Account
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Delete extends \BoostMyShop\Amazon\Controller\Adminhtml\Account {

    public function execute()
    {
        try{

            $id = filter_var($this->getRequest()->getParam('id'), FILTER_VALIDATE_INT);

            if(!empty($id)){

                $this->_accountFactory->create()->load($id)->delete();

                $this->messageManager->addSuccess(__('Account successfully deleted'));

            }else{

                $this->messageManager->addSuccess(__('Account can not be found'));

            }

        }catch(\Exception $e){

            $this->messageManager->addError($e->getMessage());

        }

        return $this->resultRedirectFactory->create()->setPath('amazon/account/index');

    }

}