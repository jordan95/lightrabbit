<?php namespace BoostMyShop\Amazon\Controller\Adminhtml\Account;

/**
 * Class Edit
 *
 * @package   BoostMyShop\Amazon\Controller\Adminhtml\Account
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Edit extends \BoostMyShop\Amazon\Controller\Adminhtml\Account {

    public function execute()
    {

        $this->_registerCurrentAccount();
        $this->_view->loadLayout();
        if($this->_coreRegistry->registry('bms_amazon_current_account')->getId()){

            $this->_view->getPage()->getConfig()->getTitle()->prepend(__('Edit account "'.$this->_coreRegistry->registry('bms_amazon_current_account')->getName().'"'));

        }else{

            $this->_view->getPage()->getConfig()->getTitle()->prepend(__('Create new Amazon account'));

        }
        $this->_view->renderLayout();

    }

    protected function _registerCurrentAccount(){

        if($id = filter_var($this->getRequest()->getParam('id'))){

            $this->_coreRegistry->register('bms_amazon_current_account', $this->_accountFactory->create()->load($id));

        }else{

            $this->_coreRegistry->register('bms_amazon_current_account', $this->_accountFactory->create());

        }

    }

}