<?php namespace BoostMyShop\Amazon\Controller\Adminhtml\Account;

/**
 * Class Save
 *
 * @package   BoostMyShop\Amazon\Controller\Adminhtml\Account
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Save extends \BoostMyShop\Amazon\Controller\Adminhtml\Account {

    /**
     * @return $this
     */
    public function execute()
    {
        try{

            $clean = $this->_extractData();

            if(!empty($clean['id'])){

                $account = $this->_updateRecord($clean);

            }else{

                $account = $this->_insertNewRecord($clean);

            }

            if(isset($clean['country'])){

                $account->updateCountriesOptions($clean['country']);

            }

            $this->messageManager->addSuccess(__('Account successfully saved'));

        }catch(\Exception $e){

            $this->messageManager->addError($e->getMessage());

        }

        $params = (isset($account) && is_object($account)) ? ['id' => $account->getId()] : [];
        return $this->resultRedirectFactory->create()->setPath('amazon/account/edit', $params);

    }

    /**
     * @return array $clean
     */
    protected function _extractData(){

        $data = $this->getRequest()->getParam('account');

        $clean = filter_var_array(
            $data,
            [
                'id' => FILTER_VALIDATE_INT,
                'name' => FILTER_SANITIZE_STRING,
                'active' => FILTER_VALIDATE_INT,
                'merchant_id' => FILTER_SANITIZE_STRING,
                'access_key_id' => FILTER_SANITIZE_STRING,
                'secret_key' => FILTER_SANITIZE_STRING
            ]
        );

        if(isset($data['country'])){

            $clean['country'] = [];

            foreach($data['country'] as $countryId => $country){

                $clean['country'][$countryId] = filter_var_array(
                    $country,
                    [
                        'reference' => FILTER_SANITIZE_STRING,
                        'store' => FILTER_VALIDATE_INT,
                        'order_importation_enabled' => FILTER_VALIDATE_INT,
                        'order_importation_range' => FILTER_VALIDATE_INT,
                        'order_importation_limit' => FILTER_VALIDATE_INT,
                        'customer_mode' => FILTER_SANITIZE_STRING,
                        'customer_group' => FILTER_SANITIZE_STRING,
                        'order_state' => FILTER_SANITIZE_STRING,
                        'generate_invoice' => FILTER_VALIDATE_INT,
                        'shipment_method' => FILTER_SANITIZE_STRING,
                        'payment_method' => FILTER_SANITIZE_STRING,
                        'customer_tax_class_id' => FILTER_VALIDATE_INT,
                        'shipment_confirmation_enabled' => FILTER_VALIDATE_INT,
                        'barcode' => FILTER_SANITIZE_STRING,
                        'stock_attribute' => FILTER_SANITIZE_STRING,
                        'price_attribute' => FILTER_SANITIZE_STRING,
                        'delay_attribute' => FILTER_SANITIZE_STRING,
                        'default_delay' => FILTER_VALIDATE_INT,
                        'use_special_price' => FILTER_VALIDATE_INT,
                        'add_tax' => FILTER_VALIDATE_INT,
                        'price_coef' => FILTER_VALIDATE_FLOAT,
                        'offer_update_enabled' => FILTER_VALIDATE_INT
                    ]
                );

            }

        }

        return $clean;

    }

    /**
     * @param array $clean
     * @throws \Exception
     */
    protected function _updateRecord($clean){

        $account = $this->_accountFactory->create()
            ->load($clean['id']);

        if($account->getId()){

            $account->setName($clean['name'])
                ->setActive($clean['active'])
                ->setMerchantId($clean['merchant_id'])
                ->setAccessKeyId($clean['access_key_id'])
                ->setSecretKey($clean['secret_key'])
                ->save();

        }else{

            throw new \Exception('Not able to load account with ID #'.$clean['id']);

        }

        return $account;

    }

    /**
     * @param array $clean
     */
    protected function _insertNewRecord($clean){

        return $this->_accountFactory->create()
            ->setName($clean['name'])
            ->setActive($clean['active'])
            ->setMerchantId($clean['merchant_id'])
            ->setAccessKeyId($clean['access_key_id'])
            ->setSecretKey($clean['secret_key'])
            ->save();

    }

}