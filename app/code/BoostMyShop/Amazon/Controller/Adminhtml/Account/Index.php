<?php namespace BoostMyShop\Amazon\Controller\Adminhtml\Account;

/**
 * Class Index
 *
 * @package   BoostMyShop\Amazon\Controller\Adminhtml\Account
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Index extends \BoostMyShop\Amazon\Controller\Adminhtml\Account {

    public function execute()
    {

        $this->_view->loadLayout();
        $this->_view->getPage()->getConfig()->getTitle()->prepend(__('Amazon accounts'));
        $this->_view->renderLayout();

    }

}