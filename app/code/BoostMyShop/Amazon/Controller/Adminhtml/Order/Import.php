<?php namespace BoostMyShop\Amazon\Controller\Adminhtml\Order;

/**
 * Class Import
 *
 * @package   BoostMyShop\Amazon\Controller\Adminhtml\Order
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Import extends \BoostMyShop\Amazon\Controller\Adminhtml\Order
{

    /**
     * @var \BoostMyShop\Amazon\Model\ResourceModel\Account\Country\CollectionFactory
     */
    protected $_accountCountryCollectionFactory;

    /**
     * Import constructor.
     * @param \BoostMyShop\Amazon\Model\ResourceModel\Account\Country\CollectionFactory $accountCountryCollectionFactory
     * @param \Magento\Backend\App\Action\Context $context
     */
    public function __construct(
        \BoostMyShop\Amazon\Model\ResourceModel\Account\Country\CollectionFactory $accountCountryCollectionFactory,
        \Magento\Backend\App\Action\Context $context
    ){
        parent::__construct($context);
        $this->_accountCountryCollectionFactory = $accountCountryCollectionFactory;
    }

    /**
     * @return $this
     */
    public function execute()
    {

        foreach ($this->_accountCountryCollectionFactory->create() as $country) {

            try {

                $batchId = $country->importOrders();
                $this->messageManager->addSuccess($country->getLabel() . ' (<a target="_blanck" href="'.$this->getUrl('amazon/batch/view', ['id' => $batchId]).'">see details</a>)');

            } catch (\Exception $e) {

                $this->messageManager->addError($country->getLabel() . ' : ' . $e->getMessage());

            }

        }

        return $this->resultRedirectFactory->create()->setPath('amazon/order/index');

    }

}