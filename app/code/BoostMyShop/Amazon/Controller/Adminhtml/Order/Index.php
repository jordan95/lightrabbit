<?php namespace BoostMyShop\Amazon\Controller\Adminhtml\Order;

/**
 * Class Index
 *
 * @package   BoostMyShop\Amazon\Controller\Adminhtml\Order
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Index extends \BoostMyShop\Amazon\Controller\Adminhtml\Order {

    public function execute()
    {
        $this->_view->loadLayout();
        $this->_view->getPage()->getConfig()->getTitle()->prepend(__('Orders'));
        $this->_view->renderLayout();
    }

}