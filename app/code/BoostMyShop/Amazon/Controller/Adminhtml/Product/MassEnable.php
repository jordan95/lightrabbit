<?php namespace BoostMyShop\Amazon\Controller\Adminhtml\Product;

/**
 * Class MassEnable
 *
 * @package   BoostMyShop\Amazon\Controller\Adminhtml\Product
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class MassEnable extends \BoostMyShop\Amazon\Controller\Adminhtml\Product {

    /**
     * @var \BoostMyShop\Amazon\Model\ProductFactory
     */
    protected $_productFactory;

    /**
     * MassEnable constructor.
     * @param \BoostMyShop\Amazon\Model\ProductFactory $productFactory
     * @param \Magento\Backend\App\Action\Context $context
     */
    public function __construct(
        \BoostMyShop\Amazon\Model\ProductFactory $productFactory,
        \Magento\Backend\App\Action\Context $context
    ){
        parent::__construct($context);
        $this->_productFactory = $productFactory;
    }

    /**
     * @return $this
     */
    public function execute(){

        $accountCountryId = $this->getRequest()->getParam('account_country_id');
        $productIds = $this->getRequest()->getParam('amazon_products_mass_actions');

        foreach($productIds as $productId){

            $product = $this->_productFactory->create()->loadByAccountCountryId($productId, $accountCountryId);
            if($product->getId()){

                $status = ($product->getAsin()) ? \BoostMyShop\Amazon\Model\Product::STATUS_ASSOCIATED : \BoostMyShop\Amazon\Model\Product::STATUS_NOT_ASSOCIATED;
                $product->setStatus($status)
                    ->save();

            }else{

                $product->setProductId($productId)
                    ->setAccountCountryId($accountCountryId)
                    ->setStatus(\BoostMyShop\Amazon\Model\Product::STATUS_NOT_ASSOCIATED)
                    ->setErrorMessage('')
                    ->setAsin('')
                    ->save();

            }

        }

        $this->messageManager->addSuccess('Product(s) successfully enabled');
        return $this->resultRedirectFactory->create()->setPath('amazon/product/index', ['account_country_id' => $accountCountryId]);

    }

}