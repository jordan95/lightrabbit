<?php namespace BoostMyShop\Amazon\Controller\Adminhtml\Product;

/**
 * Class MassList
 *
 * @package   BoostMyShop\Amazon\Controller\Adminhtml\Product
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class MassList extends \BoostMyShop\Amazon\Controller\Adminhtml\Product {

    /**
     * @var \BoostMyShop\Amazon\Model\Account\Country
     */
    protected $_country;

    /**
     * MassList constructor.
     * @param \BoostMyShop\Amazon\Model\Account\Country $country
     * @param \Magento\Backend\App\Action\Context $context
     */
    public function __construct(
        \BoostMyShop\Amazon\Model\Account\Country $country,
        \Magento\Backend\App\Action\Context $context
    ){
        parent::__construct($context);
        $this->_country = $country;
    }

    /**
     * @return $this
     */
    public function execute(){

        $accountCountryId = $this->getRequest()->getParam('account_country_id');
        $productIds = $this->getRequest()->getParam('amazon_products_mass_actions');

        $result = $this->_country->load($accountCountryId)->match($productIds);

        $this->messageManager->addSuccess($result);
        return $this->resultRedirectFactory->create()->setPath('amazon/product/index', ['account_country_id' => $accountCountryId]);

    }

}