<?php namespace BoostMyShop\Amazon\Controller\Adminhtml\Product;

/**
 * Class Grid
 *
 * @package   BoostMyShop\Amazon\Controller\Adminhtml\Product
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Grid extends \BoostMyShop\Amazon\Controller\Adminhtml\Product {

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_registry;

    /**
     * @var \BoostMyShop\Amazon\Model\Account\Country
     */
    protected $_country;

    /**
     * Grid constructor.
     * @param \BoostMyShop\Amazon\Model\Account\Country $country
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Backend\App\Action\Context $context
     */
    public function __construct(
        \BoostMyShop\Amazon\Model\Account\Country $country,
        \Magento\Framework\Registry $registry,
        \Magento\Backend\App\Action\Context $context
    ){
        parent::__construct($context);
        $this->_registry = $registry;
        $this->_country = $country;
    }

    public function execute(){

        if(false === $accountCountryId = $this->getRequest()->getParam('account_country_id', false)){
            $accountCountryId = $this->_country->getCollection()->getFirstItem()->getId();
        }

        $this->_registry->register('account_country_id', $accountCountryId);

        $this->_view->loadLayout(false);
        $this->_view->renderLayout();

    }

}