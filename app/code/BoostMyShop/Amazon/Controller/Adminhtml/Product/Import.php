<?php namespace BoostMyShop\Amazon\Controller\Adminhtml\Product;

/**
 * Class Import
 *
 * @package   BoostMyShop\Amazon\Controller\Adminhtml\Product
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Import extends \BoostMyShop\Amazon\Controller\Adminhtml\Product {

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_registry;

    /**
     * @var \BoostMyShop\Amazon\Model\Account\CountryFactory
     */
    protected $_countryFactory;

    /**
     * Import constructor.
     * @param \BoostMyShop\Amazon\Model\Account\CountryFactory $countryFactory
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Backend\App\Action\Context $context
     */
    public function __construct(
        \BoostMyShop\Amazon\Model\Account\CountryFactory $countryFactory,
        \Magento\Framework\Registry $registry,
        \Magento\Backend\App\Action\Context $context)
    {
        parent::__construct($context);
        $this->_registry = $registry;
        $this->_countryFactory = $countryFactory;
    }

    /**
     * @return $this
     */
    public function execute(){

        $countryId = $this->getRequest()->getParam('account_country_id');
        if(!empty($countryId)){

            $country = $this->_countryFactory->create()->load($countryId);
            if($country->getId()){

                $batchId = $country->importProducts();

                $this->messageManager->addSuccess('Product(s) successfully synchronized (<a href="'.$this->getUrl('amazon/batch/view', ['id' => $batchId]).'">see details</a>)');

            }else{
                $this->messageManager->addError('Can not load country with id '.$countryId);
            }

        }else{

            $this->messageManager->addError('Missing country id');

        }

        return $this->resultRedirectFactory->create()->setPath('amazon/product/index', ['account_country_id' => $countryId]);

    }

}