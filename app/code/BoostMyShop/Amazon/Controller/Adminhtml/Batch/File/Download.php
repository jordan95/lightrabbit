<?php namespace BoostMyShop\Amazon\Controller\Adminhtml\Batch\File;

use Magento\Framework\App\Filesystem\DirectoryList;

/**
 * Class Download
 *
 * @package   BoostMyShop\Amazon\Controller\Adminhtml\Batch\File
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Download extends \BoostMyShop\Amazon\Controller\Adminhtml\Batch {

    /**
     * @var \BoostMyShop\Amazon\Helper\Logger
     */
    protected $_logger;

    /**
     * @var \Magento\Framework\App\Response\Http\FileFactory
     */
    protected $_fileFactory;

    /**
     * Download constructor.
     * @param \Magento\Framework\App\Response\Http\FileFactory $fileFactory
     * @param \BoostMyShop\Amazon\Helper\Logger $logger
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \BoostMyShop\Amazon\Model\BatchFactory $batchFactory
     * @param \Magento\Backend\App\Action\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \BoostMyShop\Amazon\Helper\Logger $logger,
        \Magento\Framework\Registry $coreRegistry,
        \BoostMyShop\Amazon\Model\BatchFactory $batchFactory,
        \Magento\Backend\App\Action\Context $context
    ){
        parent::__construct($coreRegistry, $batchFactory, $context);
        $this->_logger = $logger;
        $this->_fileFactory = $fileFactory;
    }

    /**
     * @return $this|\Magento\Framework\App\ResponseInterface
     * @throws \Exception
     */
    public function execute(){

        if($filename = $this->getRequest()->getParam('filename')){

            $filename = $this->_logger->getDirname().base64_decode($filename);

            if(file_exists($filename)) {

                $content = file_get_contents($filename);
                return $this->_fileFactory->create(
                    basename($filename),
                    $content,
                    DirectoryList::VAR_DIR,
                    'application/xml'
                );

            }else{

                $this->messageManager->addError(__('file no more exists'));

            }

        }

        return $this->resultRedirectFactory->create()->setPath('amazon/batch/index');

    }

}