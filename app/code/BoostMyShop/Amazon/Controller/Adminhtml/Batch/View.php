<?php namespace BoostMyShop\Amazon\Controller\Adminhtml\Batch;

/**
 * Class View
 *
 * @package   BoostMyShop\Amazon\Controller\Adminhtml\Batch
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class View extends \BoostMyShop\Amazon\Controller\Adminhtml\Batch {

    public function execute()
    {
        if($id = filter_var($this->getRequest()->getParam('id'))){

            $this->_coreRegistry->register('bms_amazon_current_batch', $this->_batchFactory->create()->load($id));

        }else{

            $this->_coreRegistry->register('bms_amazon_current_batch', $this->_batchFactory->create());

        }

        $this->_view->loadLayout();
        $this->_view->getPage()->getConfig()->getTitle()->prepend(__('Batch'));
        $this->_view->renderLayout();
    }

}