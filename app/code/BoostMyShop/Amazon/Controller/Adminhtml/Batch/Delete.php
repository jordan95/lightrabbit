<?php namespace BoostMyShop\Amazon\Controller\Adminhtml\Batch;

/**
 * Class Delete
 *
 * @package   BoostMyShop\Amazon\Controller\Adminhtml\Batch
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Delete extends \BoostMyShop\Amazon\Controller\Adminhtml\Batch {

    public function execute()
    {
        try{

            if($id = filter_var($this->getRequest()->getParam('id'), FILTER_VALIDATE_INT)){

                    $batch = $this->_batchFactory->create()->load($id);
                    if ($batch->getId()) {

                        $batch->delete();
                        $this->messageManager->addSuccess(__('Batch successfully deleted'));

                    }

            }else{

                $this->messageManager->addSuccess(__('Batch can not be found'));

            }

        }catch(\Exception $e){

            $this->messageManager->addError($e->getMessage());

        }

        return $this->resultRedirectFactory->create()->setPath('amazon/batch/index');

    }

}