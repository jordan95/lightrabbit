<?php namespace BoostMyShop\Amazon\Controller\Adminhtml\Batch;

/**
 * Class Update
 *
 * @package   BoostMyShop\Amazon\Controller\Adminhtml\Batch
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Update extends \BoostMyShop\Amazon\Controller\Adminhtml\Batch {

    /**
     * @var \BoostMyShop\Amazon\Model\Account\CountryFactory
     */
    protected $_countryFactory;

    /**
     * Update constructor.
     * @param \BoostMyShop\Amazon\Model\Account\CountryFactory $countryFactory
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \BoostMyShop\Amazon\Model\BatchFactory $batchFactory
     * @param \Magento\Backend\App\Action\Context $context
     */
    public function __construct(
        \BoostMyShop\Amazon\Model\Account\CountryFactory $countryFactory,
        \Magento\Framework\Registry $coreRegistry,
        \BoostMyShop\Amazon\Model\BatchFactory $batchFactory,
        \Magento\Backend\App\Action\Context $context
    ){
        parent::__construct($coreRegistry, $batchFactory, $context);
        $this->_countryFactory = $countryFactory;
    }

    public function execute(){

        try{

            if($id = filter_var($this->getRequest()->getParam('id'), FILTER_VALIDATE_INT)){

                $batch = $this->_batchFactory->create()->load($id);
                if ($batch->getId()) {

                    $country = $this->_countryFactory->create()->load($batch->getAccountCountryId());
                    $country->checkResponseForBatch($batch);
                    $this->messageManager->addSuccess(__('Batch successfully updated'));

                }

            }else{

                $this->messageManager->addSuccess(__('Batch can not be found'));
                return $this->resultRedirectFactory->create()->setPath('amazon/batch/index');

            }

        }catch(\Exception $e){

            $this->messageManager->addError($e->getMessage());

        }

        return $this->resultRedirectFactory->create()->setPath('amazon/batch/view', ['id' => $id]);

    }

}
