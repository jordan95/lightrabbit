<?php namespace BoostMyShop\Amazon\Controller\Adminhtml;

/**
 * Class Order
 *
 * @package   BoostMyShop\Amazon\Controller\Adminhtml
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
abstract class Order extends \Magento\Backend\App\AbstractAction {

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('BoostMyShop_Amazon::order');
    }

}