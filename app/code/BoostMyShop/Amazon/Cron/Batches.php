<?php namespace BoostMyShop\Amazon\Cron;

/**
 * Class Batches
 *
 * @package   BoostMyShop\Amazon\Cron
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
abstract class Batches extends \BoostMyShop\Amazon\Cron\Cron {

    /**
     * @var \BoostMyShop\Amazon\Model\ResourceModel\Batch\CollectionFactory
     */
    protected $_batchCollectionFactory;

    /**
     * Clean constructor.
     * @param \BoostMyShop\Amazon\Model\ResourceModel\Batch\CollectionFactory $batchCollectionFactory
     */
    public function __construct(
        \BoostMyShop\Amazon\Model\ResourceModel\Batch\CollectionFactory $batchCollectionFactory
    ){
        $this->_batchCollectionFactory = $batchCollectionFactory;
    }

}