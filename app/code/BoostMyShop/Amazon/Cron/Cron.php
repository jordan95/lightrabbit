<?php namespace BoostMyShop\Amazon\Cron;

/**
 * Class Cron
 *
 * @package   BoostMyShop\Amazon\Cron
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
abstract class Cron {

    abstract function execute();

}