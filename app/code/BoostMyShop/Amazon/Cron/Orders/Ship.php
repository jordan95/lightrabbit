<?php namespace BoostMyShop\Amazon\Cron\Orders;

/**
 * Class Ship
 *
 * @package   BoostMyShop\Amazon\Cron\Orders
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Ship extends \BoostMyShop\Amazon\Cron\Country {

    public function execute()
    {
        foreach($this->getCountryCollection() as $country){

            if($country->isAllowedToShipOrders()){

                $country->shipOrders();

            }

        }
    }

}