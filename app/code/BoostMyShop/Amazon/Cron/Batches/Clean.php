<?php namespace BoostMyShop\Amazon\Cron\Batches;

/**
 * Class Clean
 *
 * @package   BoostMyShop\Amazon\Cron\Batches
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Clean extends \BoostMyShop\Amazon\Cron\Batches {

    public function execute()
    {
        $this->_batchCollectionFactory->create()->cleanHistory();
    }

}