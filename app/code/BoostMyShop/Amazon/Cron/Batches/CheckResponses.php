<?php namespace BoostMyShop\Amazon\Cron\Batches;

/**
 * Class CheckResponses
 *
 * @package   BoostMyShop\Amazon\Cron\Batches
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class CheckResponses extends \BoostMyShop\Amazon\Cron\Country {

    public function execute()
    {
        foreach($this->getCountryCollection() as $country){

            $country->checkResponseForProcessingBatches();

        }
    }

}