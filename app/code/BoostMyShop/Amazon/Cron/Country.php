<?php namespace BoostMyShop\Amazon\Cron;

/**
 * Class Country
 *
 * @package   BoostMyShop\Amazon\Cron
 * @author    Nicolas Mugnier <contact@boostmyshop.com>
 * @copyright 2015-2016 BoostMyShop (http://www.boostmyshop.com)
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
abstract class Country extends \BoostMyShop\Amazon\Cron\Cron {

    /**
     * @var \BoostMyShop\Amazon\Model\ResourceModel\Account\Country\CollectionFactory
     */
    protected $_countryCollectionFactory;

    /**
     * Country constructor.
     * @param \BoostMyShop\Amazon\Model\ResourceModel\Account\Country\CollectionFactory $countryCollectionFactory
     */
    public function __construct(
        \BoostMyShop\Amazon\Model\ResourceModel\Account\Country\CollectionFactory $countryCollectionFactory
    )
    {
        $this->_countryCollectionFactory = $countryCollectionFactory;
    }

    /**
     * @return \BoostMyShop\Amazon\Model\ResourceModel\Account\Country\Collection
     */
    public function getCountryCollection(){

        return $this->_countryCollectionFactory->create();

    }

}