<?php

class MDN_ProfitReport_Model_System_Config_Source_Table extends Mage_Eav_Model_Entity_Attribute_Source_Abstract {

    protected $_options;

    public function toOptionArray() {

        if (!$this->_options) {
            $this->getAllOptions();
        }
        return $this->_options;
    }

    public function getAllOptions() {
        if (!$this->_options) {

            $this->_options = array();

            $this->_options[] = array('value' => 'catalog_product_entity_int', 'label' => 'Integer');
            $this->_options[] = array('value' => 'catalog_product_entity_varchar', 'label' => 'Varchar');
        }
        return $this->_options;
    }

}