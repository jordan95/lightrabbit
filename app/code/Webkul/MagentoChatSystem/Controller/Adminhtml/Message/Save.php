<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MagentoChatSystem
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MagentoChatSystem\Controller\Adminhtml\Message;

class Save extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $date;


    /**
     * @param \Magento\Backend\App\Action\Context              $context
     * @param \Magento\Framework\Registry                      $coreRegistry
     * @param \Magento\Framework\View\Result\PageFactory       $resultPageFactory
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Magento\Framework\Stdlib\DateTime\DateTime      $date
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\Stdlib\DateTime\DateTime $date
    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->date = $date;
    }
   
    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $response = new \Magento\Framework\DataObject();
        $response->setError(false);
        $data = $this->getRequest()->getParam('formData');
        if (isset($data['senderId']) && isset($data['receiverId'])) {
            $chatCustomer = $this->_objectManager->create('Webkul\MagentoChatSystem\Model\CustomerData')
                ->getCollection()
                ->addFieldToFilter('customer_id', ['eq' => $data['receiverId']]);
            $customerUniqueId = $chatCustomer->getFirstItem()->getUniqueId();

            if (isset($data['isSuperAdmin']) && $data['isSuperAdmin'] == true) {
                $assignedChatCollection = $this->_objectManager->create(
                    'Webkul\MagentoChatSystem\Model\AssignedChat'
                )->getCollection()
                ->addFieldToFilter('agent_id', ['eq' => $data['senderId']]);
                if (!$assignedChatCollection->getSize()) {
                    $assignedChatCollection = $this->_objectManager->create(
                        'Webkul\MagentoChatSystem\Model\AssignedChat'
                    );
                    $assignedChatCollection->setAgentId($data['senderId']);
                    $assignedChatCollection->setAgentUniqueId($data['senderUniqueId']);
                    $assignedChatCollection->setCustomerId($data['receiverId']);
                    $assignedChatCollection->setUniqueId($customerUniqueId);
                    $assignedChatCollection->setIsAdminChatting(1);
                    $assignedChatCollection->setChatStatus(1);
                    $assignedChatCollection->save();
                }
            }
            
            $messageModel = $this->_objectManager->create('Webkul\MagentoChatSystem\Model\Message');
            $messageModel->setSenderId($data['senderId']);
            $messageModel->setSenderUniqueId($data['senderUniqueId']);
            $messageModel->setReceiverId($data['receiverId']);
            $messageModel->setReceiverUniqueId($customerUniqueId);
            $messageModel->setMessage($data['message']);
            $messageModel->setDate($this->date->gmtDate('Y-m-d H:i:s', $data['dateTime']));
            $messageModel->save();
            $response->setMessage(
                __('Message Saved')
            );
        } else {
            $response->setError(true);
        }
        return $this->resultJsonFactory->create()->setJsonData($response->toJson());
    }
}
