<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MagentoChatSystem
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MagentoChatSystem\Controller\Adminhtml\Message;

class LoadHistory extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var \Magento\Framework\View\LayoutFactory
     */
    protected $layoutFactory;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;

    /**
     * View file system
     *
     * @var \Magento\Framework\View\Asset\Repository
     */
    protected $_viewFileSystem;

    /**
     * @param \Magento\Backend\App\Action\Context              $context
     * @param \Magento\Framework\Registry                      $coreRegistry
     * @param \Magento\Framework\View\Result\PageFactory       $resultPageFactory
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Magento\Store\Model\StoreManagerInterface       $storeManager
     * @param \Magento\Framework\Stdlib\DateTime\DateTime      $date
     * @param \Magento\Framework\View\Asset\Repository         $viewFileSystem
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Framework\View\Asset\Repository $viewFileSystem
    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->storeManager = $storeManager;
        $this->_viewFileSystem = $viewFileSystem;
        $this->_date = $date;
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $defaultImageUrl = $this->_viewFileSystem->getUrlWithParams('Webkul_MagentoChatSystem::images/default.png', []);
        $response = new \Magento\Framework\DataObject();
        $response->setError(false);
        $data = $this->getRequest()->getParam('formData');
        if (isset($data['customerId'])) {
            $customerData = [];
            $customerData['messages'] = [];
            $customer = $this->_objectManager->create('Magento\Customer\Model\Customer')->load($data['customerId']);
            $chatCustomerModel = $this->_objectManager->create('Webkul\MagentoChatSystem\Model\CustomerData')
                ->getCollection()
                ->addFieldToFilter('customer_id', ['eq' => $data['customerId']])
                ->addFieldToFilter('chat_status', ['neq' => 3]);

            $customerData['chatStatus'] = $chatCustomerModel->getFirstItem()->getChatStatus();
                $customerData['profileImageUrl'] = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).
                'chatsystem/profile/'
                .$data['customerId'].'/'.$chatCustomerModel->getFirstItem()->getImage();

            if ($chatCustomerModel->getFirstItem()->getImage() == '') {
                $customerData['profileImageUrl'] = $defaultImageUrl;
            }
            $customerData['customer_name'] = $customer->getName();

            $customerUniqueId = $chatCustomerModel->getFirstItem()->getUniqueId();
            $messageModel = $this->_objectManager->create('Webkul\MagentoChatSystem\Model\Message')
                ->getCollection()
                ->addFieldToFilter(['sender_unique_id', 'receiver_unique_id'], [['eq' => $customerUniqueId], ['eq' => $customerUniqueId]])
                ->addFieldToFilter(['sender_unique_id', 'receiver_unique_id'], [['eq' => $data['agentUniqueId']], ['eq' => $data['agentUniqueId']]])
                ->setOrder('date', 'DESC')
                ->setPageSize(1);
            $lastMsgDate = $messageModel->getFirstItem()->getDate();
            $loadDate = date("Y-m-d H:i:s");
            if ($data['loadtime'] == 1) {
                $loadDate = date('Y-m-d H:i:s', strtotime($loadDate . ' -1 day'));
            } elseif ($data['loadtime'] == 2) {
                $loadDate = date('Y-m-d H:i:s', strtotime($loadDate . ' -7 day'));
            } elseif ($data['loadtime'] == 3) {
                $loadDate = date('Y-m-d H:i:s', strtotime($loadDate . ' -(5*365) day'));
            } else {
                $loadDate = date('Y-m-d H:i:s', strtotime($loadDate . ' -12 hour'));
            }
            $messageModel = $this->_objectManager->create('Webkul\MagentoChatSystem\Model\Message')
                ->getCollection()
                ->addFieldToFilter(['sender_unique_id', 'receiver_unique_id'], [['eq' => $customerUniqueId], ['eq' => $customerUniqueId]])
                ->addFieldToFilter(['sender_unique_id', 'receiver_unique_id'], [['eq' => $data['agentUniqueId']], ['eq' => $data['agentUniqueId']]])
                ->addFieldToFilter('date', ['gteq'=> $loadDate])
                ->setOrder('date', 'ASC');

            $customerData['messages'] = [];
            foreach ($messageModel as $key => $value) {
                $data = $value->getData();
                $data['time'] = date('h:i A', strtotime($data['date']));
                $data['date'] = date('Y-m-d', strtotime($data['date']));
                if (strpos($data['message'], 'data:image') !== false) {
                    $data['type'] = 'image';
                } elseif (strpos($data['message'], 'data:') !== false && strpos($data['message'], 'base64') !== false) {
                    $data['type'] = 'file';
                } else {
                    $data['type'] = 'text';
                }
                $customerData['messages'][$key] = $data;
            }
            $response->setMessageData($customerData);
            return $this->resultJsonFactory->create()->setJsonData($response->toJson());
        }
    }
}
