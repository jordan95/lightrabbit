/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MagentoChatSystem
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
/*jshint browser:true jquery:true*/
/*global alert*/
define([
    'jquery',
    'uiComponent',
    'mage/validation',
    'ko',
    'Webkul_MagentoChatSystem/js/modal/reply',
    'Webkul_MagentoChatSystem/js/action/load-history',
    'Webkul_MagentoChatSystem/js/socket.io'
    ], function (
        $,
        Component,
        validation,
        ko,
        replyModel,
        loadHistory,
        io
    ) {
        'use strict';
        var totalCustomer= ko.observableArray([]);
        var isLoading = ko.observable(false);
        return Component.extend({
            defaults: {
                template: 'Webkul_MagentoChatSystem/chatbox'
            },
            userTyping: ko.observable(),
            showLoader: ko.observable(false),
            initialize: function () {
                this._super();
                if (window.chatboxConfig.isServerRunning == 1) {
                    this._connectServer();
                }

                var self = this;
                $('#chatbox-component').on('click', '.clearfix', function (event) {
                    if($(this).hasClass('wkopen')){
                        self._minimizeChatWindow($(this).find('#minim-chat'));
                    }else{
                        self._openChatWindow($(this).find('#maxi-chat'));
                    }
                });
                $('#chatbox-component').on('click', '#maxi-chat', function (event) {
                    event.preventDefault();
                    self._openChatWindow($(this));
                    $(this).parent('.clearfix').click();
                });
                $('#chatbox-component').on('click', '#minim-chat', function (event) {
                    event.preventDefault();
                    self._minimizeChatWindow($(this));
                    $(this).parent('.clearfix').click();
                });
                $('#chatbox-component').on('click', '#close-chat', function (event) {
                    event.preventDefault();
                    self._closeChatWindow($(this));
                });
                $('#chatbox-component').on('click', '.load_history', function (event) {
                    self.showLoadMessagePanel($(this));
                });
                $('#chatbox-component').on('click', '.action-reply', function (event) {
                    event.preventDefault();
                    self.reply('#form-'+$(this).attr('id'));
                });
                $('#chatbox-component').on('click', '.chathistory', function (event) {
                    event.preventDefault();
                    self._loadChatHistory($(this));
                });
                $('#chatbox-component').on('change', '[id^="send-attachment-"]', function (event) {
                    event.preventDefault();
                    self._sendAttachment(event);
                });
                $('body').on('click', '.msg_notification', function (event) {
                    event.preventDefault();
                    self._openChatWindowByNotify($(this));
                });
                $('body').on('keypress', '.type_message', function (event) {
                    if (event.which == 13 && !event.shiftKey) {
                      $(this).parent('form').find('.action').click();
                    }
                });
            },
            reply: function (replyForm) {

                var replyData = {},
                    formDataArray = $(replyForm).serializeArray();

                formDataArray.forEach(function (entry) {
                    // replyData[entry.name] = entry.value.replace(/\n/g, "<br />");
                    var currentData = entry.value.replace(/\n/g, "<br />");
                    replyData[entry.name] = currentData.replace(/(https?:\/\/[^\s]+)/g, function(url) {
                        return '<a href="' + url + '">' + url + '</a>';
                    });
                });
                replyData.type = 'text';
                
                replyModel.adminReply(replyData);
                console.log(replyForm);
                $(replyForm).trigger("reset");
            },
            _connectServer: function () {
                var name = window.chatboxConfig.adminData.name;
                var clientData = {
                    sender: 'admin',
                    customerName: name,
                    adminId: window.chatboxConfig.adminData.id,
                }
                var socket = io(window.chatboxConfig.host+':'+window.chatboxConfig.port);
                replyModel.setSocketObject(socket);
                socket.on('connect', function () {
                    socket.emit('newUserConneted', clientData);
                });
                socket.on('customerMessage', function (data) {
                    console.log(data);
                    if (parseInt(replyModel.adminStatusChange())) {
                        replyModel.setResponse(data);
                    }
                });
                socket.on('customerStatusChange', function (data) {
                    var statusData = [data];
                    replyModel.setCustomerStatus(statusData);
                });
                socket.on('refresh admin chat list', function (data) {
                    var userExists = false;
                    replyModel.usersList().forEach(function (savedData) {
                        if (savedData.customer_unique_id == data.customer_unique_id) {
                            userExists = true;
                        }
                    });
                    if (userExists == false) {
                        replyModel.usersList.push(data);
                    }
                });
            },
            _sendAttachment: function (e) {
                var customerId = e.target.id.split('-')[2];
                console.log(customerId);
                var fileType = e.originalEvent.target.files[0].type;
                if (fileType.indexOf("image") >= 0) {
                    var type = 'image';
                } else {
                    var type = 'file';
                }
                var replyData = {};
                var socket = replyModel.getSocketObject();
                var file = e.originalEvent.target.files[0];
                replyData.type = type;
                replyData.customerId = customerId;
                var reader = new FileReader();
                reader.onload = function(evt) {
                    replyData.reply = evt.target.result;
                    console.log(replyData);
                    replyModel.adminReply(replyData);
                }
                reader.readAsDataURL(file);
            },
            _openChatWindow: function (element) {
                if (element.length) {
                    $(element).parents('[id^="live-chat-"]').css('margin',0);
                    $(element).hide();
                    $(element).parents('[id^="live-chat-"]').find('header').addClass('wkopen');
                    $(element).parents('[id^="live-chat-"]').find('header').removeClass('wkclose');
                    $(element).siblings('#minim-chat').show();
                }
            },
            _openChatWindowByNotify: function (element) {
                if (element.length) {
                    var id = $(element).attr('id');
                    if ($('body').find('#live-chat-'+id).length) {
                        $('body').find('#live-chat-'+id).css('margin',0);
                        $('body').find('#live-chat-'+id).children('#minim-chat').show();
                    }
                }
            },
            _minimizeChatWindow: function (element) {
                if (element.length) {
                    $(element).parents('[id^="live-chat-"]').css('margin','0 0 -400px 0');
                    $(element).parents('[id^="live-chat-"]').find('header').addClass('wkclose');
                    $(element).parents('[id^="live-chat-"]').find('header').removeClass('wkopen');
                    $(element).hide();
                    $(element).siblings('#maxi-chat').show();
                }
            },
            showLoadMessagePanel: function (element) {
                $(element).children('.wk_chat_history_options').slideToggle('fast');
            },
            _loadChatHistory: function (element) {
                var self = this;
                var loadtime = $(element).attr('id');
                var result = loadtime.split('_customer_');
                var loadData = {};
                if (loadtime !== 'undefined') {
                    loadData['loadtime'] = result[0];
                    loadData['customerId'] = result[1];
                    loadData['uniqueId'] = result[2];
                    //self.showLoader(true);
                    loadHistory(loadData).always(function () {
                        //self.showLoader(false);
                    });
                }
            },
            _closeChatWindow: function (element) {
                if (element.length) {
                    $(element).parents('[id^="live-chat-"]').remove();
                    replyModel.openWindowCount(replyModel.openWindowCount()-1);
                    /*if (window.chatboxConfig.isSuperAdmin == true) {
                        var removeData = {
                            isSuperAdmin: window.chatboxConfig.isSuperAdmin,
                            agentId: window.chatboxConfig.adminData.id,
                            agentUniqueId: window.chatboxConfig.adminData.agent_unique_id,
                        };
                        removeSuperAdminChat(removeData).always(function (response) {

                        });
                    }*/
                }
                $('[id^="live-chat-"]').css('right',(20)+'px');
            },
            userTyping: function (data) {
                console.log(data);
            },
            getLoaderImage: function () {
                return window.chatboxConfig.loaderImage;
            },
        });
    });
