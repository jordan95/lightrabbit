/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MagentoChatSystem
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
/*jshint browser:true jquery:true*/
/*global alert*/
define([
    'jquery',
    'mage/template',
    'uiComponent',
    'mage/validation',
    'ko',
    'Webkul_MagentoChatSystem/js/modal/reply',
    'Webkul_MagentoChatSystem/js/socket.io',
    'Webkul_MagentoChatSystem/js/action/save-message'
    ], function ($, mageTemplate, Component, validation, ko, replyModel, io, saveMessageAction) {
        'use strict';
        return Component.extend({
            options: {},

            initialize: function () {
                var self = this;
                this._super();
                this.resTmpl = mageTemplate('#reply_admin_template');
                this.repTmpl = mageTemplate('#reply_client_template');
                this.notifyTmpl = mageTemplate('#notification-template');
                this.chatTmpl = mageTemplate('#chat_window_template');
                replyModel.adminReply.subscribe(function (adminReply) {
                    self._createReplyData(adminReply);
                });
                replyModel.customerResponse.subscribe(function (response) {
                    self._createResponseData(response);
                });
                replyModel.customerStatus.subscribe(function (status) {
                    self._updateClientStatus(status[0]);
                });
                replyModel.adminStatusChange.subscribe(function (status) {
                    self.sendStatusUpdateSignal(status);
                });
            },
            _createReplyData: function (adminReply) {
                var name = window.chatboxConfig.adminData.name;
                var clientData = {
                    sender: 'admin',
                    adminId: window.chatboxConfig.adminData.id,
                    adminUniqueId: window.chatboxConfig.adminData.agent_unique_id,
                    adminName: window.chatboxConfig.adminData.name,
                    adminImage: window.chatboxConfig.adminImage,
                    message: adminReply.reply,
                    isSuperAdmin: window.chatboxConfig.isSuperAdmin,
                    time: this.getFormateTime(),
                    date: this.getDate(),
                    receiver: adminReply.customerId,
                    type: adminReply.type
                }
                if ($.trim(clientData.message.replace(/[<]br[^>]*[>]/gi,"")).length) {
                    this._sendNewMessage(clientData);
                    var saveMsgData = {
                        customer_id:  window.chatboxConfig.adminData.id,
                        agent_unique_id: window.chatboxConfig.adminData.agent_unique_id,
                        receiver_id: adminReply.customerId,
                        isSuperAdmin: window.chatboxConfig.isSuperAdmin,
                        message: adminReply.reply,
                        dateTime:this.getDate()+this.getTime()
                    }
                    var statusError = 0;
                    $.each(replyModel.customerStatus(), function (i, v) {
                        if (adminReply.customerId == v.customerId && v.status == 0) {
                            statusError = 1;
                        }
                    });
                    if (statusError == 0) {
                        saveMessageAction(saveMsgData);
                    }
                }
            },
            _sendNewMessage: function (reply) {
                var socket = replyModel.getSocketObject();
                var data = {},
                    repTmpl;

                if (data !== 'undefined') {
                    var statusError = 0;
                    $.each(replyModel.customerStatus(), function (i, v) {
                        if (reply.receiver == v.customerId && v.status == 0) {
                            statusError = 1;
                        }
                    });
                    reply.statusError = statusError;
                    repTmpl = this.repTmpl({
                        data: reply
                    });
                    $(repTmpl)
                    .appendTo($('#chat-history-'+reply.receiver));
                }
                $("div#chat-history-"+reply.receiver).animate({scrollTop:$("#chatbox-component div#chat-history-"+reply.receiver)[0].scrollHeight});
                if (statusError == 0) {
                    socket.emit('newAdminMessageSumbit', reply);
                }
            },
            _createResponseData: function (response) {
                var data = {},
                    resTmpl,
                    notifyTmpl;
                if (response !== 'undefined') {
                    $('body').find('.chat-message-notification').remove();
                    this.openChatWindow(response);

                    if (response.image == '' || response.image == 'undefined') {
                        response.image = window.chatboxConfig.defaultImageUrl;
                    }
                    resTmpl = this.resTmpl({
                        data: response
                    });
                    $(resTmpl)
                    .appendTo($('#chat-history-'+response.customerId));

                    if (response.receiver == window.chatboxConfig.adminData.id) {
                        notifyTmpl = this.notifyTmpl({
                            data: response
                        });
                        $(notifyTmpl).prependTo('body');
                    }
                    this.blinkTab(response.message);
                    setTimeout(function () {
                        $('body').find('.chat-message-notification').fadeOut('slow', function () {
                            $('body').find('.chat-message-notification').remove();
                        });
                        
                    },5000);
                    $('#chatbox-component').find('#myAudio').get(0).play();
                }
                $("div#chat-history-"+response.customerId).animate({scrollTop:$("#chatbox-component div#chat-history-"+response.customerId)[0].scrollHeight});
            },
            blinkTab: function (message) {
              var oldTitle = document.title,
                  timeoutId,
                  blink = function () {
                    document.title = document.title == message ? ' ' : message; },
                  clear = function () {
                    clearInterval(timeoutId);
                    document.title = oldTitle;
                    window.onmousemove = null;
                    timeoutId = null;
                  };
             
              if (!timeoutId) {
                timeoutId = setInterval(blink, 1000);
                window.onmousemove = clear;
              }
            },
            sendStatusUpdateSignal: function (status) {
                var statusData = {
                    sender: 'admin',
                    receiverData: replyModel.usersList(),
                    status: status
                }
                var socket = replyModel.getSocketObject();
                socket.emit('admin status changed', statusData);
            },
            openChatWindow: function (selectedUser) {
                var chatTmplate = mageTemplate('#chat_window_template');
                if ($('#chatbox-component').find('#live-chat-'+selectedUser.customerId).length === 0) {
                    if (replyModel.openWindowCount() == 2) {
                        var openChats = $('body').find('[id^="live-chat-"]');
                        openChats[0].remove();
                        $('[id^="live-chat-"]').css('right',(20)+'px');
                        replyModel.openWindowCount(replyModel.openWindowCount()-1);
                    }
                    if (replyModel.openWindowCount() < 2) {
                        var cssclass = '';
                        if (selectedUser.chat_status == 1) {
                            cssclass = 'active';
                        } else if (selectedUser.chat_status == 2) {
                            cssclass = 'busy';
                        } else if (selectedUser.chat_status == 0) {
                            cssclass = 'offline';
                        }
                        var data = {
                            customerId: selectedUser.customerId,
                            name: selectedUser.customerName,
                            chat_status: selectedUser.chat_status,
                            class: cssclass,
                            image: selectedUser.image,
                        },
                            chatTmpl;
                        if (data !== 'undefined') {
                            chatTmpl = chatTmplate({
                                data: data
                            });
                            $(chatTmpl)
                            .appendTo($('#chatbox-component'));
                        }
                        if (replyModel.openWindowCount() > 0) {
                            $('#live-chat-'+selectedUser.customerId).css('right',(300*replyModel.openWindowCount()+30)+'px');
                        }
                        replyModel.openWindowCount(replyModel.openWindowCount()+1);
                    } else {
                        alert({
                            title: 'Attension!',
                            content: 'Maximum two chat winodw can be open.',
                            actions: {
                                always: function (){}
                            }
                        });
                    }
                }
            },
            _updateClientStatus: function (status) {
                if (status.status == 1) {
                    $('.active-users-model #client_'+status.customerId+' .user-status').removeClass('busy');
                    $('.active-users-model #client_'+status.customerId+' .user-status').removeClass('offline');
                    $('.active-users-model #client_'+status.customerId+' .user-status').addClass('active');

                    $('#live-chat-'+status.customerId+' .user-status').removeClass('busy');
                    $('#live-chat-'+status.customerId+' .user-status').removeClass('offline');
                    $('#live-chat-'+status.customerId+' .user-status').addClass('active');
                }
                if (status.status == 2) {
                    $('.active-users-model #client_'+status.customerId+' .user-status').removeClass('active');
                    $('.active-users-model #client_'+status.customerId+' .user-status').removeClass('offline');
                    $('.active-users-model #client_'+status.customerId+' .user-status').addClass('busy');

                    $('#live-chat-'+status.customerId+' .user-status').addClass('busy');
                    $('#live-chat-'+status.customerId+' .user-status').removeClass('active');
                    $('#live-chat-'+status.customerId+' .user-status').removeClass('offline');
                }
                if (status.status == 0) {
                    $('.active-users-model #client_'+status.customerId+' .user-status').removeClass('active');
                    $('.active-users-model #client_'+status.customerId+' .user-status').removeClass('busy');
                    $('.active-users-model #client_'+status.customerId+' .user-status').addClass('offline');

                    $('#live-chat-'+status.customerId+' .user-status').addClass('offline');
                    $('#live-chat-'+status.customerId+' .user-status').removeClass('active');
                    $('#live-chat-'+status.customerId+' .user-status').removeClass('busy');
                }
            },
            getDate: function () {
              var now = new Date();
              var year = "" + now.getFullYear();
              var month = "" + (now.getMonth() + 1); if (month.length == 1) {
month = "0" + month; }
              var day = "" + now.getDate(); if (day.length == 1) {
day = "0" + day; }
              return year + "-" + month + "-" + day + " ";
            },
            getTime: function () {
                var now = new Date();
                var hour = "" + now.getHours();
                if (hour.length == 1) {
                    hour = "0" + hour;
                }
                var minute = "" + now.getMinutes();
                if (minute.length == 1) {
                    minute = "0" + minute;
                }
                var second = "" + now.getSeconds();
                if (second.length == 1) {
                    second = "0" + second;
                }
              return hour + ":" + minute;
            },
            getFormateTime: function () {
                var now = new Date();
                var hours = now.getHours() > 12 ? now.getHours() - 12 : now.getHours();
                var am_pm = now.getHours() >= 12 ? "PM" : "AM";
                hours = hours < 10 ? "0" + hours : hours;

                var minute = "" + now.getMinutes();
                if (minute.length == 1) {
                    minute = "0" + minute;
                }
                var second = "" + now.getSeconds();
                if (second.length == 1) {
                    second = "0" + second;
                }
              return hours + ":" + minute + " " + am_pm;;
            }
        });
    });