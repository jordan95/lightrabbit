/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MagentoChatSystem
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
/*jshint browser:true jquery:true*/
/*global alert*/
define(
    [
        'ko'
    ],
    function (ko) {
        'use strict';
        return {
            adminReply: ko.observable(),
            customerResponse: ko.observable(),
            customerStatus: ko.observable(window.enableChatUsersConfig.enableUserData),
            adminStatusChange: ko.observable(window.chatboxConfig.adminData.status),
            socketObject: null,
            openWindowCount: ko.observable(0),
            usersList: ko.observableArray(),
            /**
             * @return {Function}
             */
            getReply: function () {
                return this.adminReply();
            },
            /**
             * @return {Function}
             */
            setReply: function (reply) {
                return this.adminReply(reply);
            },

            /**
             * @return {Function}
             */
            getResponse: function () {
                return this.customerResponse();
            },
            /**
             * @return {Function}
             */
            setResponse: function (response) {
                return this.customerResponse(response);
            },

            getSocketObject: function () {
                return this.socketObject;
            },

            setSocketObject: function (socket) {
                this.socketObject = socket;
            },
            setCustomerStatus: function (response) {
                this.customerStatus(response);
            },
        };
    }
);
