/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MagentoChatSystem
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
define([
    'jquery',
    'uiComponent',
    'mage/template',
    'ko',
    'Webkul_MagentoChatSystem/js/modal/reply',
    'Webkul_MagentoChatSystem/js/socket.io',
    'Webkul_MagentoChatSystem/js/action/load-history',
    'Webkul_MagentoChatSystem/js/action/clear-history',
    'Webkul_MagentoChatSystem/js/action/update-status',
    'Magento_Ui/js/modal/alert',
    'Magento_Ui/js/modal/confirm',
    'mage/translate'
    ], function (
        $,
        Component,
        mageTemplate,
        ko,
        replyModel,
        io,
        loadHistoryAction,
        clearHistoryAction,
        updateStatus,
        alert,
        confirm
    ) {
        'use strict';
        var totalCustomer= ko.observableArray([]);
        var isLoading = ko.observable(false);
        var enableUsers = window.enableChatUsersConfig.enableUserData;
        return Component.extend({
            defaults: {
                template: 'Webkul_MagentoChatSystem/active-model'
            },
            conditionModel: '',
            userTyping: ko.observable(),
            supportName: window.chatboxConfig.adminChatName,
            usersList: ko.observableArray(),
            options:{openWindowCount: 0},
            initialize: function () {
                var self = this;
                this._super();
                this.chatTmpl = mageTemplate('#chat_window_template');
                $('#chatbox-active-users').on('click','.pannel-control' , function () {
                    self._openModel($(this));
                });
                $('#chatbox-active-users').on('click', '.chatStatus', function (event) {
                    self._updateChatStatus($(this));
                });
            },
            _openModel: function (pannel) {
                if ($(pannel).hasClass('close')) {
                    $('#chatbox-active-users').css('right','0');
                    $(pannel).removeClass('close');
                    $(pannel).addClass('open');
                } else {
                    $('#chatbox-active-users').css('right','-250px');
                    $(pannel).removeClass('open');
                    $(pannel).addClass('close');
                }
                
            },
            setUserList: function () {
                var self = this;
                replyModel.usersList.push();
                $.each(enableUsers, function (i, v) {
                    replyModel.usersList.push(v);
                });
            },
            getUserList: function () {
                return replyModel.usersList();
            },
            refreshChatUserList: function () {
                //refreshChatAction();
            },
            goOffline: function () {
                console.log('not implemented yet!');
            },
            showStatus: function () {
                $('.wk_chat_status_options').slideToggle('fast');
            },
            supportStatus: function () {
                return true;
            },
            _updateChatStatus: function (element) {
                var self = this;
                var status = $(element).attr('id');
                var statusData = {};
                if (status !== 'undefined') {
                    statusData['status'] = status;
                    updateStatus(statusData).always(function () {
                        replyModel.adminStatusChange(status);
                        if (status == 0) {
                            location.reload();
                        }
                    });
                }
            },
            supportStatus: function () {
                if (replyModel.adminStatusChange() == 1) {
                    return '#1a8a34';
                } else if (replyModel.adminStatusChange() == 2) {
                    return '#D10000';
                } else {
                    return '#77777A';
                }
            },
            openChatWindow: function (selectedUser) {
                var chatTmplate = mageTemplate('#chat_window_template');
                if ($('#chatbox-component').find('#live-chat-'+selectedUser.customerId).length === 0) {
                    if (replyModel.openWindowCount() < 2) {
                        var data = selectedUser,
                            chatTmpl;
                        if (data !== 'undefined') {
                            chatTmpl = chatTmplate({
                                data: data
                            });
                            $(chatTmpl)
                            .appendTo($('#chatbox-component'));
                        }
                        if (replyModel.openWindowCount() > 0) {
                            $('#live-chat-'+selectedUser.customerId).css('right',(300*replyModel.openWindowCount()+30)+'px');
                        }
                        var loadData = {};
                        loadData['loadtime'] = 0;
                        loadData['customerId'] = selectedUser.customerId;
                        loadData['uniqueId'] = selectedUser.customer_unique_id;
                        loadHistoryAction(loadData).always(function () {
                        });

                        replyModel.openWindowCount(replyModel.openWindowCount()+1);
                    } else {
                        alert({
                            title: ('Attension!'),
                            content: $.mage.__('Maximum two chat winodw can be open.'),
                            actions: {
                                always: function (){}
                            }
                        });
                    }
                }
            },
            clearChatHistory: function (selectedUser) {
                confirm({
                    title: $.mage.__('Attension!'),
                    content: $.mage.__('Are you sure want to clear chat history.'),
                    actions: {
                        confirm: function (data) {
                            clearHistoryAction(selectedUser).always(function () {
                            });
                        }
                    }
                });
            }

        });
    });