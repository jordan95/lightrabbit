/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MagentoChatSystem
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
/*jshint browser:true jquery:true*/
/*global alert*/
define(
    [
        'jquery',
        'mage/storage',
        'Magento_Ui/js/model/messageList',
        'Webkul_MagentoChatSystem/js/action/start-chat',
        'Magento_Ui/js/modal/alert'
    ],
    function ($, storage, globalMessageList, startChatAction, alert) {
        'use strict';
        var callbacks = [],
            action = function (registerData, redirectUrl, isGlobal, messageContainer, canChat) {
                messageContainer = messageContainer || globalMessageList;
                console.log(registerData);
                return storage.post(
                    'chatsystem/customer/createpost',
                    JSON.stringify(registerData),
                    isGlobal
                ).done(function (response) {
                    if (response.errors) {
                       alert({
                            title: 'Error!',
                            content: response.message,
                            actions: {
                                always: function (){}
                            }
                        });
                    } else {
                        startChatAction(messageContainer, registerData, canChat).always(function () {
                            location.reload();
                        });
                    }
                }).fail(function () {
                    messageContainer.addErrorMessage({'message': 'Could not register. Please try again later'});
                    callbacks.forEach(function (callback) {
                        callback(registerData);
                    });
                });
            };

        action.registerLoginCallback = function (callback) {
            callbacks.push(callback);
        };

        return action;
    }
);