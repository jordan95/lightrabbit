/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MagentoChatSystem
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
/*jshint browser:true jquery:true*/
/*global alert*/
define(
    [
        'ko'
    ],
    function (ko) {
        'use strict';
        return {
            clientReply: ko.observable(),
            clientStatusChange: ko.observable(),
            adminStatus: ko.observable(window.chatboxConfig.isAdminLoggedIn),
            adminResponse: ko.observable(),
            chatHistory: ko.observableArray(),
            profileImageUrl: ko.observable(window.chatboxConfig.customerData.profileImageUrl),
            notificationSound: ko.observable(),
            socketObject: null,
            receiverUniqueId: ko.observable(window.chatboxConfig.receiverUniqueId),
            receiverId: ko.observable(window.chatboxConfig.receiverId),
            receiverName: ko.observable(
                window.chatboxConfig.agentData.firstname+
                ' '+
                window.chatboxConfig.agentData.lastname
            ),
            receiverEmail: ko.observable(window.chatboxConfig.agentData.email),
            customerId: ko.observable(window.chatboxConfig.customerData.id),
            customerUniqueId: ko.observable(window.chatboxConfig.customerData.uniqueId),
            customerName: ko.observable(
                window.chatboxConfig.customerData.firstname+
                ' '+
                window.chatboxConfig.customerData.lastname
            ),
            customerEmail: ko.observable(window.chatboxConfig.customerData.email),
            loadingState: ko.observable(''),
            showLoader: ko.observable(false),
            agentGoesOff: ko.observable(false),
            agentGoesOffError: ko.observable(''),
            /**
             * @return {Function}
             */
            getReply: function () {
                return this.clientReply();
            },
            /**
             * @return {Function}
             */
            setReply: function (reply) {
                return this.clientReply(reply);
            },

            /**
             * @return {Function}
             */
            getResponse: function () {
                return this.adminResponse();
            },
            /**
             * @return {Function}
             */
            setResponse: function (response) {
                return this.adminResponse(response);
            },

            getSocketObject: function () {
                return this.socketObject;
            },

            setSocketObject: function (socket) {
                this.socketObject = socket;
            },
            getChatHistory: function () {
                return this.chatHistory();
            },
            setChatHistory: function (object) {
                this.chatHistory.push(object);
            },
            getCustomerName: function () {
                return window.chatboxConfig.customerData.firstname+' '+window.chatboxConfig.customerData.lastname;
            },
            getDate: function () {
              var now = new Date();
              var year = "" + now.getFullYear();
              var month = "" + (now.getMonth() + 1);
              if (month.length == 1) {
    month = "0" + month; }
              var day = "" + now.getDate(); if (day.length == 1) {
    day = "0" + day; }
              return year + "-" + month + "-" + day + " ";
            },
            getTime: function () {
                var now = new Date();
                var hour = "" + now.getHours();
                if (hour.length == 1) {
                    hour = "0" + hour;
                }
                var minute = "" + now.getMinutes();
                if (minute.length == 1) {
                    minute = "0" + minute;
                }
                var second = "" + now.getSeconds();
                if (second.length == 1) {
                    second = "0" + second;
                }
              return hour + ":" + minute;
            },
        };
    }
);
