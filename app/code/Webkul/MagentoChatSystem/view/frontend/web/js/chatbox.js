/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MagentoChatSystem
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
define([
    'jquery',
    'mage/template',
    'uiComponent',
    'mage/validation',
    'ko',
    'Webkul_MagentoChatSystem/js/model/reply',
    'Webkul_MagentoChatSystem/js/socket.io',
    'Webkul_MagentoChatSystem/js/action/login',
    'Webkul_MagentoChatSystem/js/action/register',
    'Webkul_MagentoChatSystem/js/action/start-chat',
    'Webkul_MagentoChatSystem/js/action/assign-chat',
    'Webkul_MagentoChatSystem/js/action/update-status',
    'Webkul_MagentoChatSystem/js/action/update-profile',
    'Webkul_MagentoChatSystem/js/action/load-history',
    'mage/translate'
    ], function (
        $,
        mageTemplate,
        Component,
        validation,
        ko,
        replyModel,
        io,
        loginAction,
        registerAction,
        startChatAction,
        assignChatAction,
        updateStatus,
        updateProfile,
        loadHistory,
        messageContainer
    ) {
        'use strict';
        var totalCustomer= ko.observableArray([]);
        var isLoading = ko.observable(false);
        var isLoogedIn = ko.observable(window.chatboxConfig.isCustomerLoggedIn);
        var chatStatus = 0;
        if ((replyModel.receiverId() !== '' || replyModel.receiverId() !== 'undefined') &&
            window.chatboxConfig.customerData.chatStatus !== 0
        ) {
            var chatStatus = window.chatboxConfig.customerData.chatStatus;
        }
        var canChat = ko.observable(chatStatus);
        var soundFileUrl = ko.observable(window.chatboxConfig.soundUrl);
        //var profileImageUrl = ko.observable(window.chatboxConfig.customerData.profileImageUrl);
        return Component.extend({
            defaults: {
                template: 'Webkul_MagentoChatSystem/chatbox'
            },
            customerName: window.chatboxConfig.customerData.firstname+' '+window.chatboxConfig.customerData.lastname,
            showLoader: ko.observable(false),
            supportName: replyModel.receiverName(),
            initialize: function () {
                var self = this;
                $('.register-tab-data').hide();
                this.authenticateTmp = mageTemplate('#authentication-template');
                this._super();
                if (this.isChatEnable() !== 0 && this.isActive()) {
                    this._startChatServer();
                }
                if (window.chatboxConfig.isServerRunning == true) {
                    this._connectServer();
                }
                $('#chatbox-component').on('click', '#minim-chat', function (event) {
                    event.preventDefault();
                    self._minimizeChatWindow($(this));
                });
                $('#chatbox-component').on('click', '.register-tab', function () {
                    self._registerTabWindow($(this));
                });
                $('#chatbox-component').on('click', '.login-tab', function () {
                    self._loginTabWindow($(this));
                });
                $('#chatbox-component').on('click', '.chatStatus', function (event) {
                    self._updateChatStatus($(this));
                });
                $('#chatbox-component').on('click', '.chathistory', function () {
                    self._loadChatHistory($(this));
                });
                $('#chatbox-component').on('click', '#wk_profile_setting', function () {
                    self._updateProfile($(this));
                });
                $('#chatbox-component').on('click', '#user_profile', function () {
                    self._updateProfile($(this));
                });
                $('#chatbox-component').on('click', '.wk_chat_sound', function (event) {
                    event.preventDefault();
                    self._enableDisableSound($(this));
                });
                $('#chatbox-component').on('change', '#send-attachment', function (event) {
                    event.preventDefault();
                    self._sendAttachment(event);
                });
                $('body').on('click', '.chat-message-notification', function (event) {
                    event.preventDefault();
                    self._openChatWindowByNotify($(this));
                });
                replyModel.agentGoesOff.subscribe(function (profileData) {
                    canChat(0);
                });
            },
            /** Is login form enabled for current customer */
            isActive: function () {
                return isLoogedIn();
            },
            isChatEnable: function () {
                return canChat() !== 0 && replyModel.receiverId() !== 0;
            },
            reply: function (replyForm) {
                this._refreshPopup();
                if (this.isActive && window.chatboxConfig.isServerRunning == true) {
                    var replyData = {},
                    formDataArray = $(replyForm).serializeArray();
                    formDataArray.forEach(function (entry) {
                        // replyData[entry.name] = entry.value.replace(/\n/g, "<br />");
                        var currentData = entry.value.replace(/\n/g, "<br />");
                        replyData[entry.name] = currentData.replace(/(https?:\/\/[^\s]+)/g, function(url) {
                            return '<a href="' + url + '">' + url + '</a>';
                        });
                    });
                    replyData.status = canChat();
                    replyData.type = 'text';
                    replyModel.clientReply(replyData);
                    $(replyForm).trigger("reset");
                }
                else {
                    // location.reload();
                    $('.type_message').val('');
                    $('<div />').html('Please refresh the page. ')
                    .modal({
                        title: 'Attention!',
                        autoOpen: true,
                        buttons: [{
                         text: 'Ok',
                            attr: {
                                'data-action': 'cancel'
                            },
                            'class': 'action',
                            click: function () {
                                $('.type_message').val('');
                                this.closeModal();
                            }
                        }]
                    });
                }
            },
            replyByEnter: function (data, event) {
                this._refreshPopup();
                if (this.isActive && window.chatboxConfig.isServerRunning == true) {
                    if (event.which == 13 && !event.shiftKey) {
                        var replyData = {};
                        var currentData = $(event.target).val().replace(/\n/g, "<br />");
                        currentData = currentData.replace(/(https?:\/\/[^\s]+)/g, function(url) {
                            return '<a href="' + url + '">' + url + '</a>';
                        });
                        replyData.message = $(event.target).val(currentData).val();
                        replyData.status = canChat();
                        replyData.type = 'text';
                        replyModel.clientReply(replyData);
                        $(event.target).val('');
                        return false;
                    } else if(event.shiftKey && event.keyCode==13) {
                        return true;
                    } else {
                        return true;
                    }
                    /**/
                } else {
                    return true;
                    // location.reload();
                }
            },
            _connectServer: function () {
                var socket = io(window.chatboxConfig.host+':'+window.chatboxConfig.port);
                replyModel.setSocketObject(socket);
                socket.on('adminStatusUpdate', function (status) {
                    replyModel.adminStatus(status);
                    if (replyModel.adminStatus() == 0) {
                        canChat(0)
                        replyModel.receiverId(0);
                        replyModel.receiverUniqueId(0);
                        replyModel.agentGoesOff(true);
                        replyModel.agentGoesOffError($.mage.__('Agent logged out, please start chat again.'));
                    }
                });
            },
            _startChatServer: function () {
                var name = window.chatboxConfig.customerData.firstname+' '+window.chatboxConfig.customerData.lastname;
                var statusClass = '';
                if (canChat() == 1) {
                    statusClass = 'active';
                } else if (canChat() == 2) {
                    statusClass = 'busy';
                } else if (canChat() == 0) {
                    statusClass = 'offline';
                }
                var clientData = {
                    sender: 'customer',
                    name: replyModel.customerName(),
                    email: window.chatboxConfig.customerData.email,
                    customerId: replyModel.customerId(),
                    customer_unique_id: replyModel.customerUniqueId(),
                    receiver: replyModel.receiverId(),
                    chat_status: canChat(),
                    status: canChat(),
                    class: statusClass,
                    image: this.getProfileImage()
                }
                if (window.chatboxConfig.isServerRunning == true) {
                    console.log('server started');
                    var socket = io(window.chatboxConfig.host+':'+window.chatboxConfig.port);
                    replyModel.setSocketObject(socket);
                    socket.on('connect', function () {
                        socket.emit('newUserConneted', clientData);
                        /*socket.emit('newCustomerMessageSumbit', clientData);*/
                    });
                    socket.on('adminMessage', function (data) {
                        if (canChat()) {
                            replyModel.setResponse(data);
                        }
                    });
                    socket.on('adminStatusUpdate', function (status) {
                        replyModel.adminStatus(status);
                        if (replyModel.adminStatus() == 0) {
                            canChat(0)
                            replyModel.receiverId(0);
                            replyModel.receiverUniqueId(0);
                            replyModel.agentGoesOff(true);
                            replyModel.agentGoesOffError($.mage.__('Agent logged out, please start chat again.'));
                        }
                    });
                }
            },
            _sendAttachment: function (e) {
                var fileType = e.originalEvent.target.files[0].type;
                if (fileType.indexOf("image") >= 0) {
                    var type = 'image';
                } else {
                    var type = 'file';
                }
                var replyData = {};
                var socket = replyModel.getSocketObject();
                var file = e.originalEvent.target.files[0];
                replyData.type = type;
                replyData.status = canChat();
                var reader = new FileReader();
                reader.onload = function(evt) {
                    replyData.message = evt.target.result;
                    replyModel.clientReply(replyData);
                }
                reader.readAsDataURL(file);
            },
            _openChatWindow: function (element, event) {
              if ($(event.currentTarget).hasClass('windowOpen')) {
                    $(event.currentTarget).removeClass('windowOpen');
                    $(event.currentTarget).parents('#chatbox-component').children('.chat').hide();
                    $(event.currentTarget).parents("#chatbox-component").children('.clearfix.customer-controls').hide();

                    // $(event.currentTarget).parents('#chatbox-component').css('margin','0 0 0px 0');
                    $(event.currentTarget).children('#maxi-chat').show();
                    $(event.currentTarget).children('#minim-chat').hide();

              } else {
                    $(event.currentTarget).addClass('windowOpen');
                    $(event.currentTarget).parents('#chatbox-component').children('.chat').show();
                    $(event.currentTarget).parents("#chatbox-component").children('.clearfix.customer-controls').show();

                    // $(event.currentTarget).parents('#chatbox-component').css('margin','0 0 0px 0');
                    $(event.currentTarget).children('#maxi-chat').hide();
                    $(event.currentTarget).children('#minim-chat').show();
                    this._refreshPopup();
              }
            },
            _openChatWindowByNotify: function (element) {
                if (element.length) {
                    $('body').find('#chatbox-component').children('.chat').show();
                    $('body').find('#chatbox-component').children('.clearfix.customer-controls').show();

                    $(element).hide();
                    $('#minim-chat').show();
                    this._refreshPopup();
                }
            },
            _minimizeChatWindow: function (element) {
                if (element.length) {
                    $(element).parents('#chatbox-component').children('.chat').hide();
                    $(element).parents("#chatbox-component").children('.clearfix.customer-controls').hide();

                    // $(element).parents('#chatbox-component').css('margin','0 0 -380px 0');
                    $(element).hide();
                    $('#maxi-chat').show();
                    this._refreshPopup();
                }
            },
            getSendImage: function () {
                return window.chatboxConfig.sendImage;
            },
            /** Provide login action */
            login: function (loginForm) {
                var self = this;
                var loginData = {},
                    formDataArray = $(loginForm).serializeArray();

                formDataArray.forEach(function (entry) {
                    loginData[entry.name] = entry.value;
                });
                loginData.dateTime = replyModel.getDate()+replyModel.getTime();
                loginData.customer_id = window.chatboxConfig.customerData.id;
                loginData.startChat = true;
                loginData.agent_id = 0;
                loginData.agent_unique_id = 0;
                if ($(loginForm).validation()
                    && $(loginForm).validation('isValid')
                ) {
                    self.showLoader(true);
                    loginAction(loginData, false, undefined, canChat, self.showLoader).then(function () {
                        self._startChatServer();

                        //location.reload();
                    });
                }
            },
            register: function (registerForm) {
                var self = this;
                var registerData = {},
                    formDataArray = $(registerForm).serializeArray();

                formDataArray.forEach(function (entry) {
                    registerData[entry.name] = entry.value;
                });

                registerData.dateTime = replyModel.getDate()+replyModel.getTime();
                registerData.startChat = true;

                if ($(registerForm).validation()
                    && $(registerForm).validation('isValid')
                ) {
                    self.showLoader(true);
                    registerAction(registerData, false, undefined, messageContainer, canChat).always(function () {
                        self.showLoader(false);
                    });
                }
            },
            startChat: function (startChatForm) {
                var self = this;
                var chatData = {},
                    formDataArray = $(startChatForm).serializeArray();
                formDataArray.forEach(function (entry) {
                    chatData[entry.name] = entry.value;
                });
                chatData.dateTime = replyModel.getDate()+replyModel.getTime();
                chatData.startChat = false;
                chatData.type = 'text';
                chatData.customer_id = window.chatboxConfig.customerData.id;
                chatData.unique_id = window.chatboxConfig.customerData.uniqueId;

                if ($(startChatForm).validation()
                    && $(startChatForm).validation('isValid')
                ) {
                    this._connectServer();
                    if (replyModel.customerUniqueId() == null) {
                        chatData.agent_id = 0;
                        chatData.agent_unique_id = 0;
                        self.showLoader(true);
                        startChatAction(chatData, canChat).always(function (data) {
                            var data = $.parseJSON(data);
                            chatData.unique_id = data.unique_id;
                            replyModel.customerUniqueId(data.unique_id);
                            replyModel.customerId(data.customer_id);
                            assignChatAction(chatData, canChat).then(function () {
                                self.showLoader(false);
                                if (canChat()) {
                                    self._startChatServer();
                                    replyModel.clientReply(chatData);
                                    replyModel.clientStatusChange(1);
                                    $('body').find('#chatbox-component').children('.clearfix.customer-controls').show();
                                }
                            });
                        });
                    } else {
                        self.showLoader(true);
                        assignChatAction(chatData, canChat).then(function () {
                            self.showLoader(false);
                            if (canChat()) {
                                self._startChatServer();
                                replyModel.clientReply(chatData);
                                replyModel.clientStatusChange(1);
                                $('body').find('#chatbox-component').children('.clearfix.customer-controls').show();
                            }
                        });
                    }
                }
            },
            _loadChatHistory: function (element) {
                var self = this;
                var loadtime = $(element).attr('id');
                var loadData = {};
                if (loadtime !== 'undefined') {
                    loadData['loadtime'] = loadtime;
                    loadData['customerId'] =  window.chatboxConfig.customerData.id;
                    loadData['uniqueId'] =  window.chatboxConfig.customerData.uniqueId;
                    self.showLoader(true);
                    loadHistory(loadData).always(function () {
                        self.showLoader(false);
                    });
                }
            },
            _updateChatStatus: function (element) {
                var self = this;
                var status = $(element).attr('id');
                var chatData = {};
                if (status !== 'undefined') {
                    chatData['status'] = status;
                    self.showLoader(true);
                    updateStatus(chatData, canChat).always(function () {
                        replyModel.clientStatusChange(status);
                        self.showLoader(false);
                    });
                }
            },
            _updateProfile: function (element) {
                this._refreshPopup();
                $('.profile-setting-box').show();
            },
            uploadProfileImage: function (image) {
                var data = new FormData();
                data.append('file', $('#profile_image')[0].files[0]);
                updateProfile(data, $('.profile-setting-box'), this.showLoader);
            },
            showSelectedImage: function () {
                var oFReader = new FileReader();
                oFReader.readAsDataURL(document.getElementById("profile_image").files[0]);

                oFReader.onload = function (oFREvent) {
                    document.getElementById("user-profile-image").src = oFREvent.target.result;
                };
            },
            _registerTabWindow: function (element) {
                if (element.length) {
                    $('.login-tab-data').hide();
                    $('.register-tab-data').show();
                }
            },
            _loginTabWindow: function (element) {
                if (element.length) {
                    $('.login-tab-data').show();
                    $('.register-tab-data').hide();
                }
            },
            _refreshPopup: function () {
                $('.wk_chat_history_options').hide();
                $('.wk_chat_status_options').hide();
                $('.wk_chat_setting_options').hide();
                $('.profile-setting-box').hide();
            },
            _enableDisableSound: function (element) {

                if ($(element).hasClass('disable')) {
                    $(element).css("background-position", "-21px 0px");
                    $(element).addClass('enable');
                    $(element).removeClass('disable');
                    soundFileUrl(window.chatboxConfig.soundUrl);
                } else {
                    $(element).css("background-position", "0px 0px");
                    $(element).addClass('disable');
                    $(element).removeClass('enable');
                    soundFileUrl('');
                }
            },
            isHistoryAvialable: function () {
                return replyModel.chatHistory().length !== 0;
            },
            getChatHistory: function () {
                return replyModel.chatHistory();
            },
            showStatus: function () {
                $('.wk_chat_status_options').slideToggle('fast');
                $('.profile-setting-box').hide();
                $('.wk_chat_history_options').hide();
                $('.wk_chat_setting_options').hide();
            },
            showLoadMessagePanel: function () {
                $('.wk_chat_history_options').slideToggle('fast');
                $('.profile-setting-box').hide();
                $('.wk_chat_status_options').hide();
                $('.wk_chat_setting_options').hide();
            },
            showSettingPanel: function () {
                $('.wk_chat_history_options').hide();
                $('.wk_chat_status_options').hide();
                $('.profile-setting-box').hide();
                $('.wk_chat_setting_options').slideToggle('fast');
            },
            imagePath: function () {
                return window.chatboxConfig.opeChatImage;
            },
            supportStatus: function () {
                return replyModel.adminStatus();
            },
            getCustomerStatus: function () {
                if (canChat() == 1) {
                    return '#1a8a34';
                } else if (canChat() == 2) {
                    return '#D10000';
                } else {
                    return '#77777A';
                }
            },
            getAdminStatus: function () {
                if (replyModel.adminStatus() == 1) {
                    return '#1a8a34';
                } else if (replyModel.adminStatus() == 2) {
                    return '#D10000';
                } else {
                    return '#77777A';
                }
            },
            getProfileImage: function () {
                return replyModel.profileImageUrl();
            },
            getDownloadImage: function () {
                return window.chatboxConfig.downloadImage;
            },
            getLoadingState: function () {
                return replyModel.loadingState();
            },
            getLoaderImage: function () {
                return window.chatboxConfig.loaderImage;
            },
            getAttachmentImage: function () {
                return window.chatboxConfig.attachmentImage;
            },
            getAdminImage: function () {
                return window.chatboxConfig.adminImage;
            },
            getSoundUrl: function () {
                return soundFileUrl();
            },
            getSuppertName: function () {
                if (replyModel.receiverName() == 'undefined undefined') {
                    return window.chatboxConfig.adminChatName;
                } else {
                    return replyModel.receiverName();
                }
            },
            isAgentOff: function () {
                return replyModel.agentGoesOff();
            },
            getAgentError: function () {
                return replyModel.agentGoesOffError();
            }
        });
    });
