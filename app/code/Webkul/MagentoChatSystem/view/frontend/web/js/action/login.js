/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MagentoChatSystem
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
/*jshint browser:true jquery:true*/
/*global alert*/
define(
    [
        'jquery',
        'mage/storage',
        'Magento_Ui/js/model/messageList',
        'Webkul_MagentoChatSystem/js/action/start-chat',
        'Webkul_MagentoChatSystem/js/action/assign-chat',
        'Webkul_MagentoChatSystem/js/model/reply',
        'Magento_Ui/js/modal/alert'
    ],
    function ($, storage, globalMessageList, startChatAction, assignChatAction, replyModel, alert) {
        'use strict';
        var callbacks = [],
            action = function (loginData, redirectUrl, isGlobal, canChat, showLoader) {
                return storage.post(
                    'customer/ajax/login',
                    JSON.stringify(loginData),
                    isGlobal
                ).done(function (response) {
                    showLoader(false);
                    if (response.errors) {
                        alert({
                            title: 'Error!',
                            content: response.message,
                            actions: {
                                always: function (){
                                }
                            }
                        });
                    } else {
                        startChatAction(loginData, canChat).always(function (data) {
                            var data = $.parseJSON(data);
                            loginData.unique_id = data.unique_id;
                            loginData.customer_id = data.customer_id;
                            loginData.status = data.chat_status;
                            replyModel.customerId(data.customer_id);
                            replyModel.customerName(data.customer_name);
                            replyModel.customerUniqueId(data.unique_id);
                            assignChatAction(loginData, canChat).then(function () {
                                showLoader(false);
                                replyModel.clientReply(loginData);
                                replyModel.clientStatusChange(1);
                                location.reload();
                            });
                        });
                    }
                }).fail(function () {
                    alert({
                        title: 'Error!',
                        content: 'Something went wrong!',
                        actions: {
                            always: function (){}
                        }
                    });
                });
            };

        action.registerLoginCallback = function (callback) {
            callbacks.push(callback);
        };

        return action;
    }
);