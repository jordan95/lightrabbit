/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MagentoChatSystem
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
/*jshint browser:true jquery:true*/
/*global alert*/
define(
    [
        'jquery',
        'Webkul_MagentoChatSystem/js/model/url-builder',
        'mage/storage',
        'Magento_Ui/js/modal/alert'
    ],
    function ($, urlBuilder, storage, alert) {
        'use strict';

        return function (messageData) {
            var serviceUrl,
                payload;

            /**
             * Checkout for guest and registered customer.
             */
            serviceUrl = 'rest/V1/message/save-message';
            payload = {
                senderId: messageData.customer_id,
                receiverId: messageData.receiver_id,
                receiverUniqueId: messageData.receiver_unique_id,
                message: messageData.message,
                dateTime: messageData.dateTime
            };
        
            return storage.post(
                serviceUrl,
                JSON.stringify(payload)
            ).fail(
                function (response) {
                    console.log(response);
                }
            ).done(
                function (response) {
                    /*fullScreenLoader.stopLoader();*/
                }
            );
        };
    }
);