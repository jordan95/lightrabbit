<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MagentoChatSystem
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MagentoChatSystem\Api;
 
interface SaveAssignedChatInterface
{
    /**
     * Returns assigned response
     *
     * @api
     * @param int $customerId
     * @param string $uniqueId
     * @return string  agentassigned chat data.
     */
    public function assignChat($customerId, $uniqueId);
}
