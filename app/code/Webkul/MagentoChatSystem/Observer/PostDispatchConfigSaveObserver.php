<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MagentoChatSystem
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MagentoChatSystem\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\Io\File as IoFile;

/**
 * Webkul Pwa PostDispatchConfigSaveObserver Observer.
 */
class PostDispatchConfigSaveObserver implements ObserverInterface
{
    /**
     * @var ManagerInterface
     */
    private $messageManager;

    /**
     * @var IoFile
     */
    protected $filesystemFile;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @param ManagerInterface $messageManager
     * @param Filesystem       $filesystem
     * @param HelperData       $helper
     */
    public function __construct(
        ManagerInterface $messageManager,
        Filesystem $filesystem,
        IoFile $filesystemFile,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->messageManager = $messageManager;
        $this->storeManager = $storeManager;
        $this->_baseDirectory = $filesystem->getDirectoryWrite(DirectoryList::ROOT);
        $this->filesystemFile = $filesystemFile;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try {
            /** @var \Magento\Framework\ObjectManagerInterface $objManager */
            $objManager = \Magento\Framework\App\ObjectManager::getInstance();
            /** @var \Magento\Framework\Module\Dir\Reader $reader */
            $reader = $objManager->get('Magento\Framework\Module\Dir\Reader');

            /** @var \Magento\Framework\Filesystem $filesystem */
            $filesystem = $objManager->get('Magento\Framework\Filesystem');

            $observerRequestData = $observer['request'];
            $params = $observerRequestData->getParams();
            if ($params['section'] == 'chatsystem') {
                $paramsData = $params['groups']['config']['fields'];
                if (isset($paramsData['port_number']['value']) && $paramsData['port_number']['value']) {
                    $baseDirPath = $this->_baseDirectory->getAbsolutePath();
                    $manifestFile = fopen($baseDirPath."/app.js", "w");
                    if ($this->isCurrentlySecure()) {
                      $manifestFileData =

'/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MagentoChatSystem
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

var roomUsers  = {};
var customerRoom = "customerRoom";
var adminRoom = adminRoom;
var https     = require("https"),
    pem       = require("pem");
pem.createCertificate({days:1, selfSigned:true}, function(err, keys){
    var app = https.createServer( { key: keys.serviceKey, cert :keys.certificate });
    var io = require("socket.io")(app);

    app.listen('.$paramsData["port_number"]["value"].', function () {
        console.log("listening");
    });

    io.on("connection", function (socket) {
        socket.on("newUserConneted", function (details) {
          if (details.sender === "admin") {
            var index = details.sender+"_"+details.adminId;
            roomUsers[index] = socket.id;
          } else if (details.sender === "customer") {
            var index = details.sender+"_"+details.customerId;
            roomUsers[index] = socket.id;
            Object.keys(roomUsers).forEach(function (key, value) {
                if (key === "admin_"+details.receiver) {
                  receiverSocketId = roomUsers[key];
                  socket.broadcast.to(receiverSocketId).emit("refresh admin chat list", details);
                }
            });
          }
        });

        socket.on("newCustomerMessageSumbit", function (data) {
          var isSupportActive = true;
          if (typeof(data) !== "undefined") {
            Object.keys(roomUsers).forEach(function (key, value) {
                if (key === "admin_"+data.receiver) {
                  isSupportActive = true;
                  receiverSocketId = roomUsers[key];
                  socket.broadcast.to(receiverSocketId).emit("customerMessage", data);
                }
            });
            if (!isSupportActive) {
              receiverSocketId = roomUsers["customer_"+data.sender];
              socket.broadcast.to(receiverSocketId).emit("supportNotActive", data);
            }
          }
      });
      socket.on("newAdminMessageSumbit", function (data) {
          if (typeof(data) !== "undefined") {
           Object.keys(roomUsers).forEach(function (key, value) {
                if (key === "customer_"+data.receiver) {
                  receiverSocketId = roomUsers[key];
                  socket.broadcast.to(receiverSocketId).emit("adminMessage", data);
                }
            });
          }
      });
      socket.on("updateStatus", function (data) {
          var isSupportActive = true;
          if (typeof(data) !== "undefined") {
            Object.keys(roomUsers).forEach(function (key, value) {
                if (key === "admin_"+data.receiver) {
                  receiverSocketId = roomUsers[key];
                  socket.broadcast.to(receiverSocketId).emit("customerStatusChange", data);
                }
            });
          }
      });

      socket.on("admin status changed", function (data) {
          if (typeof(data) !== "undefined") {
           Object.keys(roomUsers).forEach(function (key, value) {
            Object(data.receiverData).forEach(function (k) {
                if (key === "customer_"+k.customerId) {
                    receiverSocketId = roomUsers[key];
                    socket.broadcast.to(receiverSocketId).emit("adminStatusUpdate", data.status);
                }
                });
            });
          }
      });
  });
});';
                    } else {
                      $manifestFileData =
  '
  /**
   * Webkul Software.
   *
   * @category  Webkul
   * @package   Webkul_MagentoChatSystem
   * @author    Webkul
   * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
   * @license   https://store.webkul.com/license.html
   */
  var app = require("http").createServer()
  var io = require("socket.io")(app);
  var roomUsers  = {};
  var customerRoom = "customerRoom";
  var adminRoom = "adminRoom";

  app.listen('.$paramsData["port_number"]["value"].', function () {
      console.log("listening");
  });

  io.on("connection", function (socket) {
      socket.on("newUserConneted", function (details) {
        if (details.sender === "admin") {
          var index = details.sender+"_"+details.adminId;
          roomUsers[index] = socket.id;
        } else if (details.sender === "customer") {
          var index = details.sender+"_"+details.customerId;
          roomUsers[index] = socket.id;
          Object.keys(roomUsers).forEach(function (key, value) {
              if (key === "admin_"+details.receiver) {
                receiverSocketId = roomUsers[key];
                socket.broadcast.to(receiverSocketId).emit("refresh admin chat list", details);
              }
          });
        }
      });

      socket.on("newCustomerMessageSumbit", function (data) {
        var isSupportActive = true;
        if (typeof(data) !== "undefined") {
          Object.keys(roomUsers).forEach(function (key, value) {
              if (key === "admin_"+data.receiver) {
                isSupportActive = true;
                receiverSocketId = roomUsers[key];
                socket.broadcast.to(receiverSocketId).emit("customerMessage", data);
              }
          });
          if (!isSupportActive) {
            receiverSocketId = roomUsers["customer_"+data.sender];
            socket.broadcast.to(receiverSocketId).emit("supportNotActive", data);
          }
        }
    });
    socket.on("newAdminMessageSumbit", function (data) {
        if (typeof(data) !== "undefined") {
         Object.keys(roomUsers).forEach(function (key, value) {
              if (key === "customer_"+data.receiver) {
                receiverSocketId = roomUsers[key];
                socket.broadcast.to(receiverSocketId).emit("adminMessage", data);
              }
          });
        }
    });
    socket.on("updateStatus", function (data) {
        var isSupportActive = true;
        if (typeof(data) !== "undefined") {
          Object.keys(roomUsers).forEach(function (key, value) {
              if (key === "admin_"+data.receiver) {
                receiverSocketId = roomUsers[key];
                socket.broadcast.to(receiverSocketId).emit("customerStatusChange", data);
              }
          });
        }
    });

    socket.on("admin status changed", function (data) {
        if (typeof(data) !== "undefined") {
         Object.keys(roomUsers).forEach(function (key, value) {
          Object(data.receiverData).forEach(function (k) {
              if (key === "customer_"+k.customerId) {
                  receiverSocketId = roomUsers[key];
                  socket.broadcast.to(receiverSocketId).emit("adminStatusUpdate", data.status);
              }
              });
          });
        }
    });
  });';
                    }
                    fwrite($manifestFile, $manifestFileData);
                    fclose($manifestFile);
                }
            }
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }
    }

    /**
     * Check if current requested URL is secure
     *
     * @return boolean
     */
    public function isCurrentlySecure()
    {
        return $this->storeManager->getStore()->isCurrentlySecure();
    }
}
