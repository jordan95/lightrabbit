<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MagentoChatSystem
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MagentoChatSystem\Model;

use Webkul\MagentoChatSystem\Api\SaveMessageInterface;
use Webkul\MagentoChatSystem\Api\Data\MessageInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;
use Webkul\MagentoChatSystem\Model\ResourceModel\CustomerData\CollectionFactory;
 
class SaveMessage implements SaveMessageInterface
{
        /**
         * @var Items
         */
    protected $messageRepository;

    /**
     * @var CollectionFactory
     */
    protected $_dataCollection;


    /** @var DataObjectHelper  */
    protected $dataObjectHelper;

    /** @var PreorderItemsInterfaceFactory  */

    protected $messageFactory;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;

    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var CollectionFactory
     */
    private $_chatCustomerCollection;

    /**
     * @param \Webkul\MarketplacePreorder\Helper\Data $preorderHelper
     * @param ItemsRepository $itemsRepository
     * @param MessageInterfaceFactory $preorderItemsFactory
     * @param CollectionFactory $completeCollection
     * @param DataObjectHelper $dataObjectHelper
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     */
    public function __construct(
        MessageRepository $messageRepository,
        MessageInterfaceFactory $messageFactory,
        DataObjectHelper $dataObjectHelper,
        CollectionFactory $dataCollection,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Customer\Model\Session $customerSession
    ) {
        $this->messageRepository = $messageRepository;
        $this->messageFactory = $messageFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->customerSession = $customerSession;
        $this->_date = $date;
        $this->_chatCustomerCollection = $dataCollection;
        $this->_objectManager = $objectManager;
    }

   /**
    * Returns greeting message to user
    *
    * @api
    * @param int $senderId
    * @param int $receiverId
    * @param string $receiverUniqueId
    * @param string $message
    * @param string $dateTime
    * @return string Greeting message with users name.
    */
    public function saveMeassage($senderId, $receiverId, $receiverUniqueId, $message, $dateTime)
    {
        $agentData = [];
        $customerId = $this->customerSession->getCustomer()->getId();
        $customer = $this->_objectManager->create(
            'Magento\Customer\Model\Customer'
        )->load($customerId);
        if ($customer) {
            $chatCustomerCollection = $this->_chatCustomerCollection->create()
                ->addFieldToFilter('customer_id', ['eq' => $customer->getId()]);
            $chatCustomerUniqueId = $chatCustomerCollection->getFirstItem()->getUniqueId();

            $agentModel = $this->_objectManager->create(
                'Webkul\MagentoChatSystem\Model\AgentData'
            )->getCollection()
            ->addFieldToFilter('agent_id', ['eq' => $receiverId])
            ->addFieldToFilter('chat_status', ['neq' => 0]);
            if (!$agentModel->getSize()) {
                $message = [
                    'errors' => true,
                    'msg' => __('Agent logged out.')
                ];
                /*$assignNewAgent = $this->_objectManager->create('Webkul\MagentoChatSystem\Model\SaveAssignedChat');
                $data = $assignNewAgent->assignChat($customerId, $chatCustomerUniqueId);
                $agentData = (array)json_decode($data);
                $receiverId = $agentData['agent_id'];
                $receiverUniqueId = $agentData['agent_unique_id'];*/
                return json_encode($message);
            } else {
                $message = [
                    'sender_id' => $customerId,
                    'sender_unique_id' => $chatCustomerUniqueId,
                    'receiver_id' => $receiverId,
                    'receiver_unique_id' => $receiverUniqueId,
                    'message'   => $message,
                    'date'  => $this->_date->gmtDate('Y-m-d H:i:s', $dateTime)
                ];
                $dataObject = $this->messageFactory->create();

                $this->dataObjectHelper->populateWithArray(
                    $dataObject,
                    $message,
                    '\Webkul\MagentoChatSystem\Api\Data\MessageInterface'
                );
                try {
                    $this->messageRepository->save($dataObject);
                    $message['errors'] = false;
                } catch (\Exception $e) {
                    $message['errors'] = true;
                    throw new \Magento\Framework\Exception\LocalizedException(__($e->getMessage()));
                }
                return json_encode($message);
            }
        }
    }
}
