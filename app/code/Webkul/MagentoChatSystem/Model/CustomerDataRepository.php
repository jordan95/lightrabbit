<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MagentoChatSystem
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MagentoChatSystem\Model;

use Webkul\MagentoChatSystem\Api\Data;
use Webkul\MagentoChatSystem\Api\CustomerDataRepositoryInterface;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Webkul\MagentoChatSystem\Model\ResourceModel\CustomerData as ResourceCustomerData;
use Webkul\MagentoChatSystem\Model\ResourceModel\CustomerData\CollectionFactory as CustomerDataCollectionFactory;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class CustomerDataRepository
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class CustomerDataRepository implements CustomerDataRepositoryInterface
{
    /**
     * @var ResourceBlock
     */
    protected $resource;

    /**
     * @var BlockFactory
     */
    protected $timeSlotConfigFactory;

    /**
     * @var BlockCollectionFactory
     */
    protected $customerDataCollectionFactory;

    /**
     * @var Data\BlockSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * @var DataObjectProcessor
     */
    protected $dataObjectProcessor;

    /**
     * @var \Webkul\MagentoChatSystem\Api\Data\CustomerDataInterfaceFactory
     */
    protected $dataCustomerFactory;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @param ResourceTimeSlotConfig $resource
     * @param TimeSlotConfigFactory $customerDataFactory
     * @param Data\CustomerDataInterfaceFactory $dataCustomerConfigFactory
     * @param CustomerDataCollectionFactory $timeSlotCollectionFactory
     * @param Data\PreorderCompleteSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ResourceCustomerData $resource,
        CustomerDataFactory $customerDataFactory,
        \Webkul\MagentoChatSystem\Api\Data\CustomerDataInterfaceFactory $dataCustomerFactory,
        CustomerDataCollectionFactory $customerDataCollectionFactory,
        Data\CustomerDataSearchResultInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager
    ) {
        $this->resource = $resource;
        $this->customerDataFactory = $customerDataFactory;
        $this->customerDataCollectionFactory = $customerDataCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataCustomerFactory = $dataCustomerFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
    }

    /**
     * Save Customer data
     *
     * @param \Webkul\MagentoChatSystem\Api\Data\CustomerDataInterface $customerData
     * @return PreorderComplete
     * @throws CouldNotSaveException
     */
    public function save(Data\CustomerDataInterface $customerData)
    {
        $storeId = $this->storeManager->getStore()->getId();
        $customerData->setStoreId($storeId);
        try {
            $this->resource->save($customerData);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
        return $customerData;
    }

    /**
     * Load Preorder Complete data by given Block Identity
     *
     * @param string $id
     * @return PreorderComplete
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getById($id)
    {
        $customerData = $this->customerDataFactory->create();
        $this->resource->load($customerData, $id);
        if (!$customerData->getEntityId()) {
            throw new NoSuchEntityException(__('Customer with id "%1" does not exist.', $id));
        }
        return $customerData;
    }

    /**
     * Load PreorderComplete data collection by given search criteria
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @param \Magento\Framework\Api\SearchCriteriaInterface $criteria
     * @return \Webkul\MarketplacePreorder\Model\ResourceModel\PreorderComplete\Collection
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $criteria)
    {
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);

        $collection = $this->customerDataCollectionFactory->create();
        foreach ($criteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ?: 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }
        $searchResults->setTotalCount($collection->getSize());
        $sortOrders = $criteria->getSortOrders();
        if ($sortOrders) {
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($criteria->getCurrentPage());
        $collection->setPageSize($criteria->getPageSize());
        $customerData = [];
        /** @var PreorderComplete $timeSlotData */
        foreach ($collection as $customerDataModel) {
            $timeSlot = $this->dataCustomerFactory->create();
            $this->dataObjectHelper->populateWithArray(
                $timeSlot,
                $customerDataModel->getData(),
                'Webkul\MagentoChatSystem\Api\Data\CustomerDataInterface'
            );
            $customerData[] = $this->dataObjectProcessor->buildOutputDataArray(
                $timeSlot,
                'Webkul\MagentoChatSystem\Api\Data\CustomerDataInterface'
            );
        }
        $searchResults->setItems($customerData);
        return $searchResults;
    }

    /**
     * Delete PreorderComplete
     *
     * @param \Webkul\MarketplacePreorder\Api\Data\PreorderCompleteInterface $timeSlot
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(Data\CustomerDataInterface $customerData)
    {
        try {
            $this->resource->delete($customerData);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    /**
     * Delete PreorderComplete by given Block Identity
     *
     * @param string $id
     * @return bool
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById($id)
    {
        return $this->delete($this->getById($id));
    }
}
