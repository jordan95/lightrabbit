<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MagentoChatSystem
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MagentoChatSystem\Model;

use Webkul\MagentoChatSystem\Api\SaveAssignedChatInterface;
use Webkul\MagentoChatSystem\Api\Data\AssignedChatInterfaceFactory;
use Webkul\MagentoChatSystem\Model\Agent\AssignedChatRepository;
use Magento\Framework\Api\DataObjectHelper;
use Webkul\MagentoChatSystem\Model\ResourceModel\AssignedChat\CollectionFactory;
use Webkul\MagentoChatSystem\Model\ResourceModel\AgentData\CollectionFactory as AgentCollectionFactory;

class SaveAssignedChat implements SaveAssignedChatInterface
{
        /**
         * @var Items
         */
    protected $assignedChatRepository;

    /**
     * @var CollectionFactory
     */
    protected $_dataCollection;


    /** @var DataObjectHelper  */
    protected $dataObjectHelper;

    /** @var PreorderItemsInterfaceFactory  */

    protected $assignedChatFactory;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $date;

    /**
     * @var ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var CollectionFactory
     */
    private $dataCollection;

    /**
     * @param \Webkul\MarketplacePreorder\Helper\Data $preorderHelper
     * @param ItemsRepository $itemsRepository
     * @param MessageInterfaceFactory $preorderItemsFactory
     * @param CollectionFactory $completeCollection
     * @param DataObjectHelper $dataObjectHelper
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     */
    public function __construct(
        AssignedChatRepository $assignedChatRepository,
        AssignedChatInterfaceFactory $assignedChatFactory,
        DataObjectHelper $dataObjectHelper,
        CollectionFactory $dataCollection,
        AgentCollectionFactory $agentCollection,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Customer\Model\Session $customerSession
    ) {
        $this->assignedChatRepository = $assignedChatRepository;
        $this->assignedChatFactory = $assignedChatFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->customerSession = $customerSession;
        $this->date = $date;
        $this->dataCollection = $dataCollection;
        $this->agentCollection = $agentCollection;
        $this->objectManager = $objectManager;
    }

    /**
     * Returns assigned response
     *
     * @api
     * @param int $customerId
     * @param string $uniqueId
     * @return string  agentassigned chat data.
     */
    public function assignChat($customerId, $uniqueId)
    {
        $returnData = [];
        $assignedData['error'] = false;
        if ($customerId) {
            $agentCollection = $this->agentCollection->create()
                ->addFieldToFilter('chat_status', ['eq' => 1]);

            $freeAgentId = $this->findLessFreeAgent($agentCollection);
            if ($freeAgentId) {
                $agentCollection = $this->agentCollection->create()
                    ->addFieldToFilter('agent_id', ['eq' => $freeAgentId]);
                $assignChatCollection = $this->dataCollection->create()
                    ->addFieldToFilter('agent_unique_id', $agentCollection->getFirstItem()->getAgentUniqueId())
                    ->addFieldToFilter('customer_id', $customerId);
                if ($assignChatCollection->getSize()) {
                    $entityId = $assignChatCollection->getFirstItem()->getEntityId();
                    $dataObject = $this->assignedChatFactory->create()->load($entityId);
                    $assignedData = $dataObject->getData();
                    $assignedData['chat_status'] = 1;
                } else {
                    $assignedData = [
                        'agent_id' => $freeAgentId,
                        'agent_unique_id' => $agentCollection->getFirstItem()->getAgentUniqueId(),
                        'customer_id' => $customerId,
                        'unique_id' => $uniqueId,
                        'chat_status' => 1
                    ];
                    $dataObject = $this->assignedChatFactory->create();
                }

                $this->dataObjectHelper->populateWithArray(
                    $dataObject,
                    $assignedData,
                    '\Webkul\MagentoChatSystem\Api\Data\AssignedChatInterface'
                );
                $agentModel = $this->objectManager->create(
                    'Magento\User\Model\User'
                )->load($freeAgentId);

                $assignedData['agent_name'] = $agentModel->getFirstName().' '.$agentModel->getLastName();
                $assignedData['agent_email'] = $agentModel->getEmail();
                $assignedData['agent_status'] = $agentCollection->getFirstItem()->getChatStatus();

                try {
                    $this->assignedChatRepository->save($dataObject);

                    $totalAssignedCollection = $this->objectManager->create(
                        'Webkul\MagentoChatSystem\Model\TotalAssignedChat'
                    )->getCollection()
                    ->addFieldToFilter('agent_id', ['eq' => $freeAgentId]);

                    if ($totalAssignedCollection->getSize()) {
                        $entityId = $totalAssignedCollection->getFirstItem()->getEntityId();
                        $totalChat = $totalAssignedCollection->getFirstItem()->getTotalActiveChat()+1;
                        $totalAssignedModel = $this->objectManager->create(
                            'Webkul\MagentoChatSystem\Model\TotalAssignedChat'
                        )->load($entityId);

                        $totalAssignedModel->setTotalActiveChat($totalChat);
                        $totalAssignedModel->setId($entityId)->save();
                    } else {
                        $totalAssignedModel = $this->objectManager->create(
                            'Webkul\MagentoChatSystem\Model\TotalAssignedChat'
                        );
                        $totalAssignedModel->setAgentId($freeAgentId);
                        $totalAssignedModel->setAgentUniqueId($agentCollection->getFirstItem()->getAgentUniqueId());
                        $totalAssignedModel->setTotalActiveChat(1);
                        $totalAssignedModel->save();
                    }
                    $assignedData['error'] = false;
                } catch (\Exception $e) {
                    $assignedData['error'] = true;
                    throw new \Magento\Framework\Exception\LocalizedException(__($e->getMessage()));
                }
            } else {
                $assignedData['error'] = true;
            }

            return json_encode($assignedData);
        }
    }

    /**
     * Algo to find free agent
     * @param   \Webkul\MagentoChatSystem\Model\ResourceModel\AgentData\Collection $agentCollection
     * @return int
     */
    private function findLessFreeAgent($agentCollection)
    {
        $agentId = 0;
        $agentChatArray = [];
        if ($agentCollection->getSize()) {
            foreach ($agentCollection as $agent) {
                $totalChatModel = $this->objectManager->create(
                    'Webkul\MagentoChatSystem\Model\TotalAssignedChat'
                )->getCollection()
                ->addFieldToFilter('agent_unique_id', ['eq' => $agent->getAgentUniqueId()]);
                if ($totalChatModel->getSize()) {
                    $totalChat = $totalChatModel->getFirstItem()->getTotalActiveChat();
                    $agentChatArray[] = [
                        'agent_id' => $agent->getAgentId(),
                        'total_chat' => $totalChat
                    ];
                } else {
                    $agentId = $agent->getAgentId();
                }
            }
            if (count($agentChatArray) && !$agentId) {
                $sortArray = [];
                foreach ($agentChatArray as $value) {
                    if (empty($sortArray)) {
                        $sortArray = $value;
                    } else {
                        if ($sortArray['total_chat'] > $value['total_chat']) {
                            $sortArray = $value;
                        }
                    }
                }
                $agentId = $sortArray['agent_id'];
            }
        }
        return $agentId;
    }
}
