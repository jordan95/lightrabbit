<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MagentoChatSystem
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MagentoChatSystem\Model;

use Magento\Customer\Api\CustomerRepositoryInterface as CustomerRepository;
use Magento\Customer\Model\Context as CustomerContext;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Customer\Model\Url as CustomerUrlManager;
use Magento\Framework\App\Http\Context as HttpContext;
use Magento\Framework\Data\Form\FormKey;
use Magento\Framework\Locale\FormatInterface as LocaleFormat;
use Magento\Framework\UrlInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Webkul\MagentoChatSystem\Model\ResourceModel\CustomerData\CollectionFactory;
use Webkul\MagentoChatSystem\Model\ResourceModel\Message\CollectionFactory as MessageCollection;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.TooManyFields)
 */
class ChatDataConfigProvider
{

    /**
     * @var CustomerRepository
     */
    private $customerRepository;

    /**
     * @var CustomerSession
     */
    private $customerSession;

    /**
     * @var CustomerUrlManager
     */
    private $customerUrlManager;

    /**
     * @var CollectionFactory
     */
    protected $dataCollection;

    /**
     * @var MessageCollection
     */
    protected $messageCollection;

    /**
     * @var HttpContext
     */
    private $httpContext;

    /**
     * @var FormKey
     */
    protected $formKey;

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var UrlInterface
     */
    protected $urlBuilder;
    /**
     * @var UrlInterface
     */
    protected $helper;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $_objectManager;

    /**
     * View file system
     *
     * @var \Magento\Framework\View\Asset\Repository
     */
    protected $_viewFileSystem;

    /**
     * @param CustomerRepository                          $customerRepository
     * @param CustomerSession                             $customerSession
     * @param CustomerUrlManager                          $customerUrlManager
     * @param CollectionFactory                           $dataCollection
     * @param MessageCollection                           $messageCollection
     * @param HttpContext                                 $httpContext
     * @param \Magento\Customer\Model\Address\Mapper      $addressMapper
     * @param \Magento\Customer\Model\Address\Config      $addressConfig
     * @param FormKey                                     $formKey
     * @param \Magento\Store\Model\StoreManagerInterface  $storeManager
     * @param UrlInterface                                $urlBuilder
     * @param \Webkul\MagentoChatSystem\Helper\Data       $helper
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param \Magento\Framework\View\Asset\Repository    $viewFileSystem
     * @param \Magento\Framework\ObjectManagerInterface   $objectManager
     */
    public function __construct(
        CustomerRepository $customerRepository,
        \Magento\Customer\Model\Session $customerSession,
        CustomerUrlManager $customerUrlManager,
        CollectionFactory $dataCollection,
        MessageCollection $messageCollection,
        HttpContext $httpContext,
        \Magento\Customer\Model\Address\Mapper $addressMapper,
        \Magento\Customer\Model\Address\Config $addressConfig,
        FormKey $formKey,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        UrlInterface $urlBuilder,
        \Webkul\MagentoChatSystem\Helper\Data $helper,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Framework\View\Asset\Repository $viewFileSystem,
        \Magento\Framework\ObjectManagerInterface $objectManager
    ) {
        $this->customerRepository = $customerRepository;
        $this->customerUrlManager = $customerUrlManager;
        $this->dataCollection = $dataCollection;
        $this->messageCollection = $messageCollection;
        $this->httpContext = $httpContext;
        $this->formKey = $formKey;
        $this->storeManager = $storeManager;
        $this->urlBuilder = $urlBuilder;
        $this->helper = $helper;
        $this->date = $date;
        $this->_viewFileSystem = $viewFileSystem;
        $this->_objectManager = $objectManager;
    }
    /**
     * {@inheritdoc}
     */
    public function getConfig()
    {
		$this->customerSession = $this->_objectManager->create('Magento\Customer\Model\Session');
        $output['formKey'] = $this->formKey->getFormKey();
        $output['storeCode'] = $this->getStoreCode();
        $output['customerData'] = $this->getCustomerData();
        $output['adminChatName'] = $this->helper->getConfigData('config', 'chat_name');
        $output['isCustomerLoggedIn'] = $this->isCustomerLoggedIn();
        $output['isServerRunning'] = $this->isServerRunning();
        $output['isAdminLoggedIn'] = $this->isAdminLoggedIn();
        $output['adminImage'] = $this ->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).
        'chatsystem/admin/'.
        $this->helper->getConfigData('config', 'admin_image');
        $output['receiverId'] = $this->getAgentId();
        $output['receiverUniqueId'] = $this->getAgentUniqueId();
        $output['agentData'] = $this->getAgentData();
        $output['superAdminData'] = $this->getSuperAdminData();
        $output['registerUrl'] = $this->getRegisterUrl();
        $output['host'] = $this->helper->getConfigData('config', 'host_name');
        $output['port'] = $this->helper->getConfigData('config', 'port_number');

        return $output;
    }

    /**
     * Creating customer data with messages history
     * @return array
     */
    private function getCustomerData()
    {
        $defaultImageUrl = $this->_viewFileSystem->getUrlWithParams('Webkul_MagentoChatSystem::images/default.png', []);
        $customerData = [];
        if ($this->isCustomerLoggedIn()) {
            $customer = $this->customerRepository->getById($this->customerSession->getCustomerId());
            $customerData = $customer->__toArray();
            $customerData['chatStatus'] = 0;

            $chatCustomerCollection = $this->dataCollection->create()
                ->addFieldToFilter('customer_id', ['eq' => $customer->getId()])
                ->addFieldToFilter('chat_status', ['neq' => 0]);
            if ($chatCustomerCollection->getSize()) {
                $customerData['chatStatus'] = $chatCustomerCollection->getFirstItem()->getChatStatus();
                $customerData['profileImageUrl'] = $this ->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).
                'chatsystem/profile/'
                .$customer->getId().'/'.$chatCustomerCollection->getFirstItem()->getImage();
                if ($chatCustomerCollection->getFirstItem()->getImage() == '') {
                    $customerData['profileImageUrl'] = $defaultImageUrl;
                }
                $customerData['uniqueId'] = $chatCustomerCollection->getFirstItem()->getUniqueId();
                $customerUniqueId = $chatCustomerCollection->getFirstItem()->getUniqueId();
                $message = $this->messageCollection->create()
                ->addFieldToFilter(['sender_unique_id', 'receiver_unique_id'], [['eq' => $customerUniqueId], ['eq' => $customerUniqueId]])
                ->addFieldToFilter('date', ['gteq'=> date("Y-m-d H:i:s")])
                    ->setOrder('date', 'ASC')
                    ->setCurPage(1);
                $previousDate = '';
                foreach ($message as $key => $value) {
                    $data = $value->getData();
                    $changeDate = 0;
                    $currentDate = strtotime($this->date->gmtDate('Y-m-d', $data['date']));
                    if ($previousDate == '') {
                        $previousDate = strtotime($this->date->gmtDate('Y-m-d', $data['date']));
                        $changeDate = true;
                    } elseif ($currentDate !== $previousDate) {
                        $changeDate = true;
                        $previousDate = strtotime($this->date->gmtDate('Y-m-d', $data['date']));
                    }
                    if (strpos($data['message'], 'data:image') !== false) {
                        $data['type'] = 'image';
                    } elseif (strpos($data['message'], 'data:') !== false && strpos($data['message'], 'base64') !== false) {
                        $data['type'] = 'file';
                    } else {
                        $data['type'] = 'text';
                    }
                    $data['time'] = $this->date->gmtDate('h:i A', $data['date']);
                    $data['date'] = $this->date->gmtDate('Y-m-d', $data['date']);
                    $data['changeDate'] = $changeDate;
                    $customerData['messages'][$key] = $data;
                }
            } else {
                $chatCustomerCollection = $this->dataCollection->create()
                    ->addFieldToFilter('customer_id', ['eq' => $customer->getId()]);
                $customerData['uniqueId'] = $chatCustomerCollection->getFirstItem()->getUniqueId();
                $customerData['profileImageUrl'] = $defaultImageUrl;
            }
        }
        return $customerData;
    }

    protected function getAgentData()
    {
        $agentData = [];
        $agentId = $this->getAgentId();
        $agentModel = $this->_objectManager->create(
            'Magento\User\Model\User'
        )->load($agentId);
        $agentData[] = $agentModel->getData();
        return $agentData[0];
    }

    protected function getSuperAdminData()
    {
        $adminUserCollection = $this->_objectManager->create('Webkul\MagentoChatSystem\Model\AssignedChat')
            ->getCollection()
            ->addFieldToFilter('customer_id', ['eq' => $this->customerSession->getCustomerId()])
            ->addFieldToFilter('is_admin_chatting', ['eq' => 1])
            ->addFieldToSelect(['agent_id', 'agent_unique_id'])
            ->addFieldToFilter('chat_status', ['eq' => 1]);
        $superAdminData  = $adminUserCollection->getData();
        return $superAdminData;
    }

    /**
     * Check if customer is logged in
     *
     * @return bool
     * @codeCoverageIgnore
     */
    private function isCustomerLoggedIn()
    {
        return (bool)$this->httpContext->getValue(CustomerContext::CONTEXT_AUTH);
    }

    /**
     * Check if customer is logged in
     *
     * @return bool
     * @codeCoverageIgnore
     */
    private function isAdminLoggedIn()
    {
        $adminUserCollection = $this->_objectManager->create('Webkul\MagentoChatSystem\Model\AgentData')
            ->getCollection()
            ->addFieldToFilter('agent_id', ['eq' => $this->getAgentId()]);
        return $adminUserCollection->getFirstItem()->getChatStatus();
    }

    private function getAgentId()
    {
        $id = 0;
        $adminUserCollection = $this->_objectManager->create('Webkul\MagentoChatSystem\Model\AssignedChat')
            ->getCollection()
            ->addFieldToFilter('customer_id', ['eq' => $this->customerSession->getCustomerId()])
            ->addFieldToFilter('chat_status', ['eq' => 1]);
        if ($adminUserCollection->getSize()) {
            return $adminUserCollection->getFirstItem()->getAgentId();
        }
        return $id;
    }

    private function getAgentUniqueId()
    {
        $uniqueId = 0;
        $adminUserCollection = $this->_objectManager->create('Webkul\MagentoChatSystem\Model\AssignedChat')
            ->getCollection()
            ->addFieldToFilter('customer_id', ['eq' => $this->customerSession->getCustomerId()])
            ->addFieldToFilter('chat_status', ['eq' => 1]);
        if ($adminUserCollection->getSize()) {
            return $adminUserCollection->getFirstItem()->getAgentUniqueId();
        }
        return $uniqueId;
    }
    /**
     * Retrieve customer registration URL
     *
     * @return string
     * @codeCoverageIgnore
     */
    public function getRegisterUrl()
    {
        return $this->customerUrlManager->getRegisterUrl();
    }

    private function isServerRunning()
    {
        $host = $this->helper->getConfigData('config', 'host_name');
        $port = $this->helper->getConfigData('config', 'port_number');
        $chkServerRunning = exec('timeout 1s telnet '.$host.' '.$port.'');
        $getBrack = explode(' ', $chkServerRunning);

        if ((count($getBrack) > 2) && (strtolower($getBrack[0]) == 'escape')) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Retrieve store code
     *
     * @return string
     * @codeCoverageIgnore
     */
    private function getStoreCode()
    {
        return $this->storeManager->getStore()->getCode();
    }
}
