#Installation

Magento 2 Admin Buyer ChatSystem module installation is very easy, please follow the steps for installation-

1. Unzip the respective extension zip and create Webkul(vendor) and MagentoChatSystem(module) name folder inside your magento/app/code/ directory and then move all module's files into magento root directory Magento2/app/code/Webkul/MagentoChatSystem/ folder.

Run Following Command via terminal
-----------------------------------
php bin/magento setup:upgrade
php bin/magento setup:di:compile
php bin/magento setup:static-content:deploy

## Make sure Node is already installed on your server.##

Run Folowwing command from root directory of your Magento to install socket.io on your server.
-----------------------------------------------------------------------------------------------
npm install
npm install socket.io

2. Flush the cache and reindex all.

#User Guide

For Magento 2 Admin Buyer ChatSystem module's working process follow user guide : 

#Support

Find us our support policy - https://store.webkul.com/support.html/

#Refund

Find us our refund policy - https://store.webkul.com/refund-policy.html/