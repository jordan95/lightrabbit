<?php

namespace Hiddentechies\Googlecustomerreview\Model\Config\Source;

/**
 * Display mode
 *
 */
class Badgeposition implements \Magento\Framework\Option\ArrayInterface {

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray() {
        return [
            ['value' => 'BOTTOM_LEFT', 'label' => __('BOTTOM LEFT')],
            ['value' => 'BOTTOM_RIGHT', 'label' => __('BOTTOM_RIGHT')],
        ];
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray() {
        $array = [];
        foreach ($this->toOptionArray() as $item) {
            $array[$item['value']] = $item['label'];
        }
        return $array;
    }

}
