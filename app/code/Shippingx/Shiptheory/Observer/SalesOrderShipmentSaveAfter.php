<?php

namespace Shippingx\Shiptheory\Observer;

class SalesOrderShipmentSaveAfter implements \Magento\Framework\Event\ObserverInterface
{

    protected $_scopeConfig;
    protected $_submitShipment;
    public function __construct(
        \Shippingx\Shiptheory\Model\SubmitShipmentFactory $submitShipment,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ){
        $this->_submitShipment = $submitShipment;
        $this->_scopeConfig = $scopeConfig;
    }


    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORES;
        $shipment = $observer->getEvent()->getShipment();

        $webhook = $this->_scopeConfig->getValue("shipthory/setting/webhook", $storeScope, $shipment->getStoreId());

        if($webhook==true){
            $this->_submitShipment->create()->submitShipment($shipment);
        }

        return $this;
    }

}