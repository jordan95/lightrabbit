<?php

namespace Shippingx\Shiptheory\Block\Adminhtml;

class History extends \Magento\Backend\Block\Widget\Container
{
    protected $_template = 'history/history.phtml';

    public function __construct(\Magento\Backend\Block\Widget\Context $context,array $data = [])
    {
        parent::__construct($context, $data);
    }
    
    protected function _prepareLayout()
    {
        $this->setChild(
            'grid',
            $this->getLayout()->createBlock('Shippingx\Shiptheory\Block\Adminhtml\History\Grid', 'boostmyshop.history.grid')
        );
        return parent::_prepareLayout();
    }
    
    public function getGridHtml()
    {
        return $this->getChildHtml('grid');
    }

}