<?php
namespace Shippingx\Shiptheory\Model\Resource\ShipmentHistory;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
    	$this->_init('Shippingx\Shiptheory\Model\ShipmentHistory', 'Shippingx\Shiptheory\Model\Resource\ShipmentHistory');
    }
}
