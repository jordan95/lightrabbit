<?php

namespace Shippingx\Shiptheory\Model\Config\Backend;

class EnableWebhook extends \Magento\Framework\App\Config\Value
{
    public function beforeSave()
    {

       $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
       $apiModel = $objectManager->create('Shippingx\Shiptheory\Model\Api');

        $websiteId = $this->getWebsiteId($this->getScopeId(), $this->getScope());

       if($this->getValue()==1){
          $apiModel->createUserAndRole($websiteId);
       }
       else if($this->getValue()==0){
         $apiModel->removeUserAndRole($websiteId);
       }

        $this->setValue($this->getValue());

        parent::beforeSave();
    }

    protected function getWebsiteId($scopeId, $scope)
    {
        switch($scope)
        {
            case 'default':
                return 0;
                break;
            case 'websites':
                return $scopeId;
                break;
        }
    }
 
}