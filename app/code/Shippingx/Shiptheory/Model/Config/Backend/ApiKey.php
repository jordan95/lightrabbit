<?php

namespace Shippingx\Shiptheory\Model\Config\Backend;

class ApiKey extends \Magento\Framework\App\Config\Value
{
    public function beforeSave()
    {
       $label = $this->getData('field_config/label');

       $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
       $configmodel = $objectManager->create('Shippingx\Shiptheory\Model\Configuration');
       $result = $configmodel->checkConfiguration($this->getValue()); 

        if ($this->getValue() == '') {
            throw new \Magento\Framework\Exception\ValidatorException(__($label . ' is required.'));
        } else if (!$result) {
            throw new \Magento\Framework\Exception\ValidatorException(__('An error occurred, please check configuration settings'));
        }

        $this->setValue($this->getValue());

        parent::beforeSave();
    }

}