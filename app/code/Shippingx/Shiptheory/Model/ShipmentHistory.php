<?php

namespace Shippingx\Shiptheory\Model;

class ShipmentHistory extends \Magento\Framework\Model\AbstractModel
{
	public function _construct()
    {
         $this->_init('Shippingx\Shiptheory\Model\Resource\ShipmentHistory');
    }


    public function loadByShipmentId($shipmentId){
    	$data = $this->getResource()->loadByShipmentId($shipmentId);
        if ($data !== false) {
            $this->setData($data);
            $this->setOrigData();
        }
        return $this;
    } 
}