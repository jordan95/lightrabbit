<?php
namespace Shippingx\Shiptheory\Model;

use Magento\Authorization\Model\Acl\Role\Group as RoleGroup;
use Magento\Authorization\Model\UserContextInterface;

class Api extends \Magento\Framework\Model\AbstractModel
{	
    protected $roleFactory;
 
    protected $rulesFactory;

    protected $_userFactory;

    protected $_scopeConfig;


    
    const API_ROLE_NAME  = 'shiptheory';
    const API_USER_NAME  = 'shiptheory';
    const API_FIRST_NAME = 'shiptheory';
    const API_LAST_NAME  = 'shiptheory';
    const API_EMAIL      = 'shiptheory@shiptheory.com';

    /**
     * Init
     *
     * @param \Magento\Authorization\Model\RoleFactory $roleFactory
     * @param \Magento\Authorization\Model\RulesFactory $rulesFactory
     */
    public function __construct(
        \Magento\Authorization\Model\RoleFactory $roleFactory, 
        \Magento\Authorization\Model\RulesFactory $rulesFactory,
        \Magento\User\Model\UserFactory $userFactory,
 		\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Model\Context $context
        )
    {
        $this->roleFactory = $roleFactory;
        $this->rulesFactory = $rulesFactory;
		$this->_userFactory = $userFactory;
		$this->_scopeConfig = $scopeConfig;
    }



    public function createUserAndRole($websiteId){
    	$role = $this->createRole();
    	$user = $this->createUser($role, $websiteId);
    	$this->submitUserToShipTheory($user, $websiteId);
    	
	}

    public function createRole(){
		$role  = $this->roleFactory->create();
		$role1 = $role->getCollection()
		  	 ->addFieldToFilter('role_type', RoleGroup::ROLE_TYPE)
		     ->addFieldToFilter('role_name',self::API_ROLE_NAME);

        //if role already exists
        if($role1->count()>0)
        {	$result = '';
			foreach($role1 as $r){
	 			$result  = $r;
	 		}
     		return $result;
		}

        $role->setName(self::API_ROLE_NAME)
                ->setPid(0)
                ->setRoleType(RoleGroup::ROLE_TYPE) 
                ->setUserType(UserContextInterface::USER_TYPE_ADMIN);

        $role->save();
        $resource=[	'',
                    'Magento_Backend::admin',
                    'Magento_Sales::sales',
                    'Magento_Sales::sales_operation',
                    'Magento_Sales::shipment',
                    'Shippingx_Shiptheory::shipthory',
                    'Shippingx_Shiptheory::shipthory_config',
                    'Shippingx_Shiptheory::shipthory_history'
                  ];

        $this->rulesFactory->create()->setRoleId($role->getId())->setResources($resource)->saveRel();
        return $role;
	}

    public function createUser($role, $websiteId){

        $userName = Self::API_USER_NAME.'_'.$websiteId;
    	$passwordWithoutHashed ="shiptheory".date('Ymdhis’');
    	$userModel = $this->_userFactory->create();
    	$existUserData = $this->checkUserExist($userName);

    	if($existUserData){
    		$userModel->load($existUserData->getUserId())->setPassword($passwordWithoutHashed)->save();
    		$userModel->setPasswordNotHashed($passwordWithoutHashed);
    		return $userModel;
		}

    	$userInfo = [
				        'username'  => $userName,
				        'firstname' => Self::API_FIRST_NAME,
				        'lastname'  => Self::API_LAST_NAME,
				        'email'     => $websiteId.'_'.Self::API_EMAIL,
				        'password'  => $passwordWithoutHashed,
				        'is_active' => 1
				    ];

	    $userInfo['password_not_hashed']=$passwordWithoutHashed;
	    $userModel->setData($userInfo);
	    $userModel->setRoleId($role->getRoleId());
        $userModel->save();

        return $userModel;
    }

    public function checkUserExist($userName){
    	$userModel = $this->_userFactory->create();
		$user = $userModel->loadByUsername($userName);
		if($user->getUserId())
			return $user;
	}

    public function submitUserToShipTheory($user, $websiteId){

        $websiteScope = \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE;

        $apiKey = $this->_scopeConfig->getValue("shipthory/setting/api_key", $websiteScope, $websiteId);
        $baseUrl = $this->_scopeConfig->getValue("web/secure/base_url", $websiteScope, $websiteId);

        $data = [
                    'apikey'=>$apiKey,
                    'username'=>$user['username'],
                    'password'=>$user['password_not_hashed'],
                    'url' => $baseUrl
                ];

        $fields_string = "";
        foreach($data as $key=>$value)
        {
            $fields_string .= $key.'='.$value.'&';
        }
        rtrim($fields_string, '&');
       
        $apiUrl = 'https://shiptheory.com/magento2/setup';
        $ch = curl_init($apiUrl);
        curl_setopt($ch,CURLOPT_POST, count($data));
        curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/x-www-form-urlencoded"));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if ($httpcode != 200)
            throw new \Exception('Shiptheory setup API returned an incorrect http code ('.$httpcode.') ('.$fields_string.')');

    }

    public function removeUserAndRole($websiteId){
        $this->removeUser($websiteId);
    }   


    public function removeRole(){
        $role  = $this->roleFactory->create();
        $roleExist = $role->getCollection()
                          ->addFieldToFilter('role_type', RoleGroup::ROLE_TYPE)
                          ->addFieldToFilter('role_name',self::API_ROLE_NAME);

        if($roleExist->count()>0)
        {
            foreach($roleExist as $r){
                $role->load($r->getRoleId())->delete();
            }
        }

    }

    public function removeUser($websiteId){
        $userName = Self::API_USER_NAME.'_'.$websiteId;
        $user = $this->_userFactory->create();
        $existUserData = $this->checkUserExist($userName);

        if($existUserData){
            $existUserData->delete();
        }

    }


}