<?php
  
namespace Shippingx\Shiptheory\Setup;
  
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;
  
class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        
        /**
         * Create table 'shiptheory_shipment'
          */
        $tableName = $installer->getTable('shiptheory_shipment');        
        if ($installer->getConnection()->isTableExists($tableName) != true) {            
            $table = $installer->getConnection()
                ->newTable($tableName)
                ->addColumn(
                    'id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                    ],
                    'id'
                )

                ->addColumn(
                    'created_at',
                    \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
                    null,
                    ['nullable' => false, 'default' => '0000-00-00'],
                    'Created At'
                )
                

                 ->addColumn(
                    'shipment_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    ['nullable' => false],
                    'Shipment Id'
                )

                ->addColumn('success', 
                    \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, 
                    null, 
                    ['nullable'  => false,'default'   => '1',], 
                    'success'
                )

                 ->addColumn(
                    'message',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    200,
                    ['nullable' => false,
                     'default' => ''],
                     'message'
                ) 
                
                ->setComment('Shiptheory Shipment Table')
                ->setOption('type', 'InnoDB')
                ->setOption('charset', 'utf8')
                ->addIndex(
                    $installer->getIdxName('shiptheory_shipment', ['shipment_id']),
                    ['shipment_id']
                );
                
            $installer->getConnection()->createTable($table);                 
        }


        $installer->endSetup();
    }
}