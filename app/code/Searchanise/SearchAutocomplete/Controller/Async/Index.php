<?php

namespace Searchanise\SearchAutocomplete\Controller\Async;

class Index extends \Magento\Framework\App\Action\Action
{
    const UPDATE_TIMEOUT = 3600;

    /**
     * @var Searchanise\SearchAutocomplete\Helper\ApiSe
     */
    private $apiSeHelper;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    private $resultJsonFactory;

    /**
     * @var Searchanise\SearchAutocomplete\Helper\ApiSe\NOT_USE_HTTP_REQUEST
     */
    private $notUseHttpRequestText = null;

    /**
     * @var Searchanise\SearchAutocomplete\Helper\ApiSe\FL_SHOW_STATUS_ASYNC
     */
    private $flShowStatusAsync = null;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Searchanise\SearchAutocomplete\Helper\ApiSe $apiSeHelper
    ) {
        $this->storeManager = $storeManager;
        $this->apiSeHelper = $apiSeHelper;
        $this->resultJsonFactory = $resultJsonFactory;

        parent::__construct($context);
    }

    /**
     * Get 'not_use_http_request' param for URL
     *
     * @return  string
     */
    private function _getNotUseHttpRequestText()
    {
        if ($this->notUseHttpRequestText === null) {
            $this->notUseHttpRequestText = $this
                ->getRequest()
                ->getParam(\Searchanise\SearchAutocomplete\Helper\ApiSe::NOT_USE_HTTP_REQUEST);
        }

        return $this->notUseHttpRequestText;
    }

    /**
     * Check if 'not_use_http_request' param is true
     *
     * @return  boolean
     */
    private function _checkNotUseHttpRequest()
    {
        return $this->_getNotUseHttpRequestText() == \Searchanise\SearchAutocomplete\Helper\ApiSe::NOT_USE_HTTP_REQUEST_KEY;
    }

    /**
     * Get 'show_status' param for URL
     *
     * @return  string
     */
    private function _getFlShowStatusAsync()
    {
        if ($this->flShowStatusAsync === null) {
            $this->flShowStatusAsync = $this
                ->getRequest()
                ->getParam(\Searchanise\SearchAutocomplete\Helper\ApiSe::FL_SHOW_STATUS_ASYNC);
        }

        return $this->flShowStatusAsync;
    }

    /**
     * Check if 'show_status' param is true
     *
     * @return  boolean
     */
    private function _checkShowSatusAsync()
    {
        return $this->_getFlShowStatusAsync() == \Searchanise\SearchAutocomplete\Helper\ApiSe::FL_SHOW_STATUS_ASYNC_KEY;
    }

    /**
     * Async
     *
     * {@inheritDoc}
     * @see \Magento\Framework\App\ActionInterface::execute()
     */
    public function execute()
    {
        // ToCheck:
        $storeId = '';
        $result = '';
        $flIgnoreProcessing = false;

        if ($this->apiSeHelper->getStatusModule($storeId) == 'Y' && $this->apiSeHelper->checkStartAsync()) {
            $checkKey = $this->apiSeHelper->checkPrivateKey();
            $this->apiSeHelper->setHttpResponse($this->getResponse());

            ignore_user_abort(true);
            set_time_limit(self::UPDATE_TIMEOUT);

            if ($checkKey && $this->getRequest()->getParam('display_errors') === 'Y') {
                error_reporting(E_ALL | E_STRICT);
            } else {
                error_reporting(0);
            }

            $flIgnoreProcessing = $checkKey && $this->getRequest()->getParam('ignore_processing') == 'Y';

            try {
                $result = $this->apiSeHelper->async($flIgnoreProcessing);
            } catch (\Exception $e) {
                return $this->resultJsonFactory->create()->setData([
                    'error' => '[' . $e->getCode() . ']: ' . $e->getMessage(),
                ]);
            }

            // TODO: Refactoring
            /*if ($this->_checkShowSatusAsync()) {
                $result = 'Searchanise status sync: ' . $result;
            }*/
        } else {
            $result = __('Nothing update');
        }

        return $this->resultJsonFactory->create()->setData([
            'success' => $result,
        ]);
    }
}
