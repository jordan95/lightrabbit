<?php

namespace Searchanise\SearchAutocomplete\Block;

class Async extends \Magento\Framework\View\Element\Template
{
    /**
     * @var Searchanise\SearchAutocomplete\Helper\ApiSe
     */
    private $apiSeHelper;

    /**
     * @var \Searchanise\SearchAutocomplete\Helper\Notification
     */
    private $notificationHelper;

    public function __construct (
        \Magento\Framework\View\Element\Template\Context $context,
        \Searchanise\SearchAutocomplete\Helper\ApiSe $apiSeHelper,
        \Searchanise\SearchAutocomplete\Helper\Notification $notificationHelper,
        array $data = []
    ) {
        $this->apiSeHelper = $apiSeHelper;
        $this->notificationHelper = $notificationHelper;

        parent::__construct($context, $data);
    }

    private function _startSignup()
    {
        if ($this->apiSeHelper->signup(null, false) == true) {
            $this->apiSeHelper->queueImport(null, false);
        }

        return true;
    }

    public function toHtml()
    {
        $html = '';

        if ($this->apiSeHelper->checkStatusModule()) {
            $storeId = '';

            if ($this->apiSeHelper->isAdmin()) {
                $storeId = $this->_storeManager->getDefaultStoreView()->getId();
                $textNotification = '';

                if ($this->apiSeHelper->checkModuleIsUpdated()) {
                    $this->apiSeHelper->updateInsalledModuleVersion();
                    $textNotification = __(
                        'Searchanise was successfully updated. Catalog indexation in process. <a href="%1">Searchanise Admin Panel</a>.',
                        $this->apiSeHelper->getModuleUrl()
                    );

                } elseif ($this->apiSeHelper->checkAutoInstall()) {
                    $textNotification = __(
                        'Searchanise was successfully installed. Catalog indexation in process. <a href="%1">Searchanise Admin Panel</a>.',
                        $this->apiSeHelper->getModuleUrl()
                    );
                }

                if ($textNotification != '') {
                    $this->notificationHelper->setNotification(
                        \Searchanise\SearchAutocomplete\Helper\Notification::TYPE_NOTICE,
                        __('Notice'),
                        $textNotification
                    );

                    // ToDo: to check
                    $this->_startSignup();

                } else {
                    // ToDo: to check
                    $this->apiSeHelper->showNotificationAsyncCompleted();
                }
            }

            $asyncUrl = $this->apiSeHelper->getAsyncUrl(false, $storeId);

            // TODO: Check if this option is cached by block
            if ($this->apiSeHelper->checkObjectAsync()) {
                $html .= "\n<object data=\"$asyncUrl\" width=\"0\" height=\"0\" type=\"text/html\"></object>\n";
            }

            if ($this->apiSeHelper->checkAjaxAsync()) {
                $html .= <<<JS
                    <script type="text/javascript">
                    //<![CDATA[
                        require([
                           "jquery"
                        ], function($){
                            $.ajax({
                                url: '{$asyncUrl}',
                                method: 'get',
                                async: true,
                                timeout: 5 * 60 * 1000, // 5 minues
                            });
                        });
                    //]]>
                    </script>
JS;
            }
        }

        return $html;
    }
}
