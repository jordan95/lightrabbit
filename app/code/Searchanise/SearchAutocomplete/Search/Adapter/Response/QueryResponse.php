<?php

namespace Searchanise\SearchAutocomplete\Search\Adapter\Response;

use \Searchanise\SearchAutocomplete\Model\Request as SearchaniseRequest;

/**
 * Searchanise response adapter
 */
class QueryResponse implements \Magento\Framework\Search\ResponseInterface
{
    /**
     * Document Collection
     *
     * @var Document[]
     */
    protected $documents = [];

    /**
     * @var integer
     */
    protected $count = 0;

    /**
     * Aggregation Collection
     *
     * @var AggregationInterface
     */
    protected $aggregations;

    /**
     * @var array
     */
    private $_suggestions = [];

    /**
     * @var \Magento\CatalogSearch\Helper\Data
     */
    private $_catalogSearchHelper;

    /**
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $_storeManager;

    public function __construct(
        DocumentFactory $documentFactory,
        AggregationFactory $aggregationFactory,
        SearchaniseRequest $searchRequest,
        \Magento\CatalogSearch\Helper\Data $catalogSearchHelper,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->_prepareDocuments($searchRequest, $documentFactory);
        $this->_prepareAggregations($searchRequest, $aggregationFactory);

        $this->_catalogSearchHelper = $catalogSearchHelper;
        $this->_storeManager = $storeManager;

        if ($searchRequest) {
            $this->_suggestions = $searchRequest->getSuggestions();
            // TODO: Shouldn't be here
            $this->renderSuggestions();
        }
    }

    /**
     * {@inheritDoc}
     */
    public function count()
    {
        return $this->count;
    }

    /**
     * {@inheritDoc}
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->documents);
    }

    /**
     * {@inheritDoc}
     */
    public function getAggregations()
    {
        return $this->aggregations;
    }

    /**
     * Return suggestions list
     *
     * @return array
     */
    public function getSuggestions()
    {
        return $this->_suggestions;
    }

    /**
     * Render suggestions
     *
     * @return boolean
     */
    public function renderSuggestions()
    {
        if (empty($this->_suggestions) || $this->count() > 0) {
            return false;
        }

        $suggestionsMaxResults = \Searchanise\SearchAutocomplete\Helper\ApiSe::getSuggestionsMaxResults();

        $message = __('Did you mean: ');
        $link = [];
        $textFind = $this->_catalogSearchHelper->getEscapedQueryText();
        $count_sug = 0;

        foreach ($this->getSuggestions() as $k => $sug) {
            if ((!empty($sug)) && ($sug != $textFind)) {
                $link[] = '<a href="' . $this->_getUrlSuggestion($sug). '">' . $sug .'</a>';
                $count_sug++;
            }

            if ($count_sug >= $suggestionsMaxResults) {
                break;
            }
        }

        if (!empty($link)) {
            $this->_catalogSearchHelper->addNoteMessage($message . implode(',', $link) . '?');
        }

        return true;
    }

    /**
     * Build buckets from raw search response.
     *
     * @param SearchaniseRequest        $searchRequest      Engine processed request
     * @param AggregationFactory        $aggregationFactory Aggregation factory.
     *
     * @return void
     */
    private function _prepareAggregations(SearchaniseRequest $searchRequest, AggregationFactory $aggregationFactory)
    {
        if (!$searchRequest) {
            return;
        }

        $this->aggregations = $aggregationFactory->create($searchRequest->getFacets());
    }

    /**
     * Build document list from the engine raw search response.
     *
     * @param SearchaniseRequest    $searchResponse  Engine processed request
     * @param DocumentFactory       $documentFactory Document factory
     *
     * @return void
     */
    private function _prepareDocuments(SearchaniseRequest $searchRequest, DocumentFactory $documentFactory)
    {
        if (!$searchRequest) {
            return;
        }

        $this->documents = [];

        $items = $searchRequest->getProductIds();

        if (!empty($items)) {
            foreach ($items as $item) {
                $this->documents[] = $documentFactory->create($item);
            }
        }

        $this->count = $searchRequest->getTotalProduct();
    }

    /**
     * Returns suggestion link
     *
     * @param string $suggestion
     * @return string
     */
    private function _getUrlSuggestion($suggestion)
    {
        $pager = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Theme\Block\Html\Pager');

        $query = array(
            'q'                         => $suggestion,
            $pager->getPageVarName()    => null // exclude current page from urls
        );

        return $this->_storeManager->getStore()->getUrl('*/*/*', [
            '_current'      => true,
            '_use_rewrite'  => true,
            '_query'        => $query
        ]);
    }

}
