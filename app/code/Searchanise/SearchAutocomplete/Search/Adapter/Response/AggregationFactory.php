<?php

namespace Searchanise\SearchAutocomplete\Search\Adapter\Response;

/**
 * Searhanise aggregations response builder.
 */
class AggregationFactory extends \Magento\Framework\Search\Adapter\Mysql\AggregationFactory
{
    /**
     * {@inheritDoc}
     */
    public function create(array $rawAggregation)
    {
        $aggregations = $this->_preprocessAggregations($rawAggregation);
        return parent::create($aggregations);
    }

    /**
     * Derefences children aggregations (nested and filter) while they have the same name.
     *
     * @param array $rawAggregation Aggregations response.
     *
     * @return array
     */
    private function _preprocessAggregations(array $rawAggregation)
    {
        $processedAggregations = [];

        foreach ($rawAggregation as $bucketName => $aggregation) {
            if (!empty($aggregation['title'])) {
                $bucketName = strtolower($aggregation['title']) . \Magento\CatalogSearch\Model\Search\RequestGenerator::BUCKET_SUFFIX;
            }

            if (!empty($aggregation['buckets'])) {
                foreach ($aggregation['buckets'] as $key => $currentBuket) {

                    if (!is_numeric($currentBuket['value'])) {
                        $currentBuket['value'] = $this->_getAttributeOptionId(strtolower($aggregation['title']), $currentBuket['value']);
                    }

                    $processedAggregations[$bucketName][$key] = [
                        'value' => $currentBuket['value'],
                        'count' => $currentBuket['count'],
                    ];
                }
            }
        }

        return $processedAggregations;
    }

    /**
     * Get attribute option id by label
     *
     * @param string $attribute     Attribute name
     * @param string $label         Option label
     * @return numeric
     */
    private function _getAttributeOptionId($attribute, $label)
    {
        $productResource = $this->objectManager->get('Magento\Catalog\Model\ResourceModel\ProductFactory')->create();
        $attr = $productResource->getAttribute($attribute);

        if ($attr && $attr->usesSource()) {
            return  $attr->getSource()->getOptionId($label);
        }

        return $label;
    }
}