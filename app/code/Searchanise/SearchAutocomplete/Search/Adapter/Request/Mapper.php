<?php

namespace Searchanise\SearchAutocomplete\Search\Adapter\Request;

use Searchanise\SearchAutocomplete\Search\RequestInterface;
use Searchanise\SearchAutocomplete\Search\Adapter\Request\Query\Builder as QueryBuilder;
use Searchanise\SearchAutocomplete\Search\Adapter\Request\SortOrder\Builder as SortOrderBuilder;
use Searchanise\SearchAutocomplete\Search\Adapter\Request\Aggregation\Builder as AggregationBuilder;

/**
 * Map a search request into a Searchanise Search query.
 */
class Mapper
{
    /**
     * @var QueryBuilder
     */
    private $_queryBuilder;

    /**
     * @var SortOrderBuilder
     */
    private $_sortOrderBuilder;

    /**
     * @var AggregationBuilder
     */
    private $_aggregationBuilder;

    public function __construct(
        QueryBuilder $queryBuilder,
        SortOrderBuilder $sortOrderBuilder,
        AggregationBuilder $aggregationBuilder
    ) {
        $this->_queryBuilder       = $queryBuilder;
        $this->_sortOrderBuilder   = $sortOrderBuilder;
        $this->_aggregationBuilder = $aggregationBuilder;
    }

    /**
     * Transform the search request into an Searchanise request.
     *
     * @param RequestInterface $request Search Request.
     *
     * @return array
     */
    public function buildSearchRequest(RequestInterface $request)
    {
        $searchRequest = [];

        $searchRequest['restrictBy']['status'] = '1';
        $searchRequest['union']['price']['min'] = \Searchanise\SearchAutocomplete\Helper\ApiSe::getLabelForPricesUsergroup();

        $query = $this->_getRootQuery($request);
        $filter = $this->_getRootFilter($request);
        $sort = $this->_getSortOrders($request);
        $aggregations = $this->_getAggregations($request);

        if (!empty($query)) {
            $this->_addQuery($query, $searchRequest);
        }

        if (!empty($sort)) {
            foreach ($sort as $k => $sortData) {
                if (empty($sortData)) {
                    continue;
                }

                foreach ($sortData as $field => $sortCondition) {
                    $searchRequest['sortBy'] = $field;
                    $searchRequest['sortOrder'] = $sortCondition['order'];
                }
            }
        }

        if (!empty($searchRequest['q'])) {
            $searchRequest['q'] = strtolower(trim($searchRequest['q']));
        }

        if ($request->getType() == RequestInterface::TEXT_FIND) {
            $searchRequest['facets']           = 'true';
            $searchRequest['suggestions']      = 'true';
            $searchRequest['query_correction'] = 'false';
        } else {
            //$searchRequest['facets']           = 'false';
            $searchRequest['facets']           = 'true';
            $searchRequest['suggestions']      = 'false';
            $searchRequest['query_correction'] = 'false';
        }

        $searchRequest['startIndex'] = $request->getFrom();
        $searchRequest['maxResults'] = $request->getSize();

        return $searchRequest;
    }

    /**
     * Adds query parameters to Searchanise request
     *
     * @param array $query              Query
     * @param array $searchRequest      Searchanise request
     */
    private function _addQuery(array $query, &$searchRequest)
    {
        if (!empty($query)) {
            foreach ($query as $type => $data) {
                if (!empty($data['query']) && is_array($data['query'])) {
                    $this->_addQuery($data['query'], $searchRequest);
                } else {
                    // TODO: Adds additional types
                    if ($type == 'match') {
                        foreach ($data as $field => $q) {
                            if (!empty($q['query'])) {
                                $searchRequest[$field] = $q['query'];
                            }
                        }
                    }
                }

                if (!empty($data['filter']) && is_array($data['filter'])) {
                    $this->_addFilter($data['filter'], $searchRequest);
                }
            }
        }

        return true;
    }

    /**
     * Adds filters to Searchanise request
     *
     * @param array $filter             Filter data
     * @param array $searchRequest      Searchainse request
     */
    private function _addFilter(array $filter, &$searchRequest)
    {
        if (empty($filter)) {
            return;
        }

        if (!isset($searchRequest['restrictBy'])) {
            $searchRequest['restrictBy'] = [];
        }

        foreach ($filter as $type => $condition) {
            if ($type == 'terms') {
                foreach ($condition as $field => $cond) {
                    $searchRequest['restrictBy'][$field] = is_array($cond) ? implode('|', $cond) : $cond;
                }
            } elseif ($type == 'match') {
                $searchRequest['queryBy'] = isset($searchRequest['queryBy']) ? $searchRequest['queryBy'] : [];

                foreach ($condition as $field => $matchCondition) {
                    $searchRequest['queryBy'][$field] = $matchCondition['query'];
                }
            } elseif ($type == 'boolean') {
                if (!empty($condition['must']) && is_array($condition['must'])) {
                    foreach ($condition['must'] as $mustCondition) {
                        $this->_addFilter($mustCondition, $searchRequest);
                    }
                }
            } elseif ($type == 'range') {
                foreach ($condition as $field => $rangeCondition) {
                    $searchRequest['restrictBy'][$field] =
                    (!empty($rangeCondition['gte']) ? $rangeCondition['gte'] : '')
                    . ','
                    . (!empty($rangeCondition['lte']) ? $rangeCondition['lte'] : '');
                }
            }
        }
    }

    /**
     * Extract and build the root query of the search request.
     *
     * @param RequestInterface $request Search request.
     *
     * @return array
     */
    private function _getRootQuery(RequestInterface $request)
    {
        $query = null;

        if ($request->getQuery()) {
            $query = $this->_queryBuilder->buildQuery($request->getQuery());
        }

        return $query;
    }

    /**
     * Extract and build the root filter of the search request.
     *
     * @param RequestInterface $request Search request.
     *
     * @return array
     */
    private function _getRootFilter(RequestInterface $request)
    {
        $filter = null;

        if ($request->getFilter()) {
            $filter = $this->_queryBuilder->buildQuery($request->getFilter());
        }

        return $filter;
    }

    /**
     * Extract and build sort orders of the search request.
     *
     * @param RequestInterface $request Search request.
     *
     * @return array
     */
    private function _getSortOrders(RequestInterface $request)
    {
        $sortOrders = [];

        if ($request->getSortOrders()) {
            $sortOrders = $this->_sortOrderBuilder->buildSortOrders($request->getSortOrders());
        }

        return $sortOrders;
    }

    /**
     * Extract and build aggregations of the search request.
     *
     * @param RequestInterface $request Search request.
     *
     * @return array
     */
    private function _getAggregations(RequestInterface $request)
    {
        $aggregations = [];

        if ($request->getAggregation()) {
            $aggregations = $this->_aggregationBuilder->buildAggregations($request->getAggregation());
        }

        return $aggregations;
    }
}