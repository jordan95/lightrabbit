<?php

namespace Searchanise\SearchAutocomplete\Search\Adapter\Request\Query\Builder;

use Searchanise\SearchAutocomplete\Search\Adapter\Request\Query\BuilderInterface;

/**
 * Build an filtered query.
 */
class Filtered extends AbstractBuilder implements BuilderInterface
{
    /**
     * {@inheritDoc}
     */
    public function buildQuery(\Magento\Framework\Search\Request\QueryInterface $query)
    {
        $searchQuery = [];

        if ($query->getFilter()) {
            $searchQuery['filter'] = $this->parentBuilder->buildQuery($query->getFilter());
        }

        if ($query->getQuery()) {
            $searchQuery['query'] = $this->parentBuilder->buildQuery($query->getQuery());
        }

        $searchQuery['boost'] = $query->getBoost();

        return ['filtered' => $searchQuery];
    }
}