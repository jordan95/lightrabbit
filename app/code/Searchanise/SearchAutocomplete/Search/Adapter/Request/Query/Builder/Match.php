<?php

namespace Searchanise\SearchAutocomplete\Search\Adapter\Request\Query\Builder;

use Searchanise\SearchAutocomplete\Search\Adapter\Request\Query\BuilderInterface;

/**
 * Build an match query.
 */
class Match implements BuilderInterface
{
    /**
     * {@inheritDoc}
     */
    public function buildQuery(\Magento\Framework\Search\Request\QueryInterface $query)
    {
        $searchQueryParams = [
            'query'                => $query->getQueryText(),
            'boost'                => $query->getBoost(),
        ];

        return ['match' => [$query->getField() => $searchQueryParams]];
    }
}