<?php

namespace Searchanise\SearchAutocomplete\Search\Adapter;

use Searchanise\SearchAutocomplete\Search\Adapter\Response\QueryResponseFactory;
use Searchanise\SearchAutocomplete\Search\Adapter\Request\Mapper;

/**
 * Searchanise Search Adapter.
 */
class Adapter implements \Magento\Framework\Search\AdapterInterface
{
    // @var \Simtech\Search\Adapter\Response\QueryResponseFactory
    private $_responseFactory;

    // @var \Searchanise\SearchAutocomplete\Helper\Data
    private $_searchaniseHelper;

    // @var \Simtech\Search\Adapter\Request\Mapper
    private $_requestMapper;

    // @var \Searchanise\SearchAutocomplete\Helper\Logger
    private $_loggerHelper;

    public function __construct(
       QueryResponseFactory $responseFactory,
       Mapper $requestMapper,
       \Searchanise\SearchAutocomplete\Helper\Data $searchaniseHelper,
       \Searchanise\SearchAutocomplete\Helper\Logger $loggerHelper)
    {
        $this->_responseFactory = $responseFactory;
        $this->_searchaniseHelper = $searchaniseHelper;
        $this->_requestMapper = $requestMapper;
        $this->_loggerHelper = $loggerHelper;
    }

    /**
     * @param RequestInterface $request
     * @return QueryResponse
     */
    public function query(\Magento\Framework\Search\RequestInterface $request)
    {
        try {
            $searchRequest = $this->_doSearch($request);
        } catch (\Exception $e) {
            $this->_loggerHelper->log($e->getMessage());
            $searchRequest = null;
        }

        return $this->_responseFactory->create(['searchRequest' => $searchRequest]);
    }

    /**
     * Execute the search request
     *
     * @param \Magento\Framework\Search\RequestInterface $request Search request.
     *
     * @return array
     */
    private function _doSearch(\Magento\Framework\Search\RequestInterface $request)
    {
        $searchRequest = [
            'type'      => $request->getType(),
            'request'   => $this->_requestMapper->buildSearchRequest($request),
        ];

        return $this->_searchaniseHelper->search($searchRequest);
    }
}