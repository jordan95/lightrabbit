<?php

namespace Searchanise\SearchAutocomplete\Model\Search;

/**
 * Config search class. Used to override sorting options
 */
class Config
{
	/**
	 * @var \Magento\Store\Model\StoreManagerInterface
	 */
	private $storeManager;

	/**
	 * @var \Searchanise\SearchAutocomplete\Model\Configuration
	 */
	private $configuration;

    /**
     * @var array
     */
    private $unusedAttributes = [
        'position',
        'category_ids',
    ];

    public function __construct(
    	\Magento\Store\Model\StoreManagerInterface $storeManager,
    	\Searchanise\SearchAutocomplete\Model\Configuration $configuration
    ) {
    	$this->storeManager = $storeManager;
    	$this->configuration = $configuration;
    }

    /**
     * Change sorting attribute
     *
     * @param \Magento\Catalog\Model\Config $catalogConfig
     * @param array $options
     * @return array
     */
    public function afterGetAttributeUsedForSortByArray(\Magento\Catalog\Model\Config $catalogConfig, $options)
    {
    	if (
    		!empty($this->unusedAttributes)
    		&& $this->configuration->isSearchaniseEngineEnable($this->storeManager->getStore()->getId())
    	) {
            foreach ($this->unusedAttributes as $name) {
                if (isset($options[$name])) {
                    unset($options[$name]);
                }
            }
        }

        return $options;
    }
}
