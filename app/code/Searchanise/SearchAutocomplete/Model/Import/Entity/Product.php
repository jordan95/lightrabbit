<?php

namespace Searchanise\SearchAutocomplete\Model\Import\Entity;

class Product extends \Magento\CatalogImportExport\Model\Import\Product
{
    /**
     * Delete products.
     *
     * @return \Magento\CatalogImportExport\Model\Import\Product
     */
    protected function _deleteProducts()
    {
        $idsToDelete = null;

        while ($bunch = $this->_dataSourceModel->getNextBunch()) {
            $idsToDelete = [];

            foreach ($bunch as $rowNum => $rowData) {
                if ($this->validateRow($rowData, $rowNum) && self::SCOPE_DEFAULT == $this->getRowScope($rowData)) {
                    $idsToDelete[] = $this->_oldSku[$rowData[self::COL_SKU]]['entity_id'];
                }
            }
        }

        $this->_eventManager->dispatch('searchanise_import_delete_product_entity_after', [
            'idsToDelete' => $idsToDelete
        ]);

        return parent::_deleteProducts();
    }

    /**
     * Update and insert data in entity table.
     *
     * @param array $entityRowsIn Row for insert
     * @param array $entityRowsUp Row for update
     * @return \Magento\CatalogImportExport\Model\Import\Product
     */
    public function saveProductEntity(array $entityRowsIn, array $entityRowsUp)
    {
        static $entityTable = null;
        $productIds = [];
        $entityId = 'entity_id';

        if (!$entityTable) {
            $entityTable = $this->_resourceFactory->create()->getEntityTable();
        }

        // Update products data
        $ret = parent::saveProductEntity($entityRowsIn, $entityRowsUp);

        $select = $this->_connection
            ->select()
            ->from($entityTable, [$entityId])
            ->where('sku IN (?)', array_merge(array_keys($entityRowsIn), array_keys($entityRowsUp)));

        $updatedProducts = $this->_connection->fetchAll($select);

        if (!empty($updatedProducts)) {
            foreach ($updatedProducts as $data) {
                if (empty($data[$entityId])) {
                    continue;
                }

                $productIds[] = $data[$entityId];
            }
        }

        if (!empty($productIds)) {
            $this->_eventManager->dispatch('searchanise_import_save_product_entity_after', [
                'productIds' => $productIds
            ]);
            unset($productIds);
        }

        return $ret;
    }
}
