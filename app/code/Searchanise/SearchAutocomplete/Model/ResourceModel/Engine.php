<?php

namespace Searchanise\SearchAutocomplete\Model\ResourceModel;

/**
 * Searchanise search engine implementation.
 */
class Engine extends \Magento\CatalogSearch\Model\ResourceModel\Engine
{
    const SEARCHANISE_NAME = 'searchanise';
}
