<?php

namespace Searchanise\SearchAutocomplete\Model\ResourceModel\Product\Fulltext;

use Searchanise\SearchAutocomplete\Search\Request\QueryInterface;
use Searchanise\SearchAutocomplete\Search\Request\BucketInterface;

/**
 * Search engine product collection.
 */
class Collection extends \Magento\Catalog\Model\ResourceModel\Product\Collection
{
    /**
     * @var QueryResponse
     */
    private $queryResponse;

    /**
     * @var \Searchanise\SearchAutocomplete\Search\Request\Builder
     */
    private $requestBuilder;

    /**
     * @var \Magento\Search\Model\SearchEngine
     */
    private $searchEngine;

    /**
     * @var \Searchanise\SearchAutocomplete\Model\Configuration
     */
    private $configuration;

    /**
     * @var string
     */
    private $queryText;

    /**
     * @var string
     */
    private $searchRequestName;

    /**
     * @var array
     */
    private $attrFilters = [];

    /**
     * @var QueryInterface[]
     */
    private $queryFilters = [];

    /**
     * @var array
     */
    private $facets = [];

    /**
     * @var array
     */
    private $countByAttributeSet;

    /**
     * @var array
     */
    private $fieldNameMapping = [
        'name'              => 'title',
        'sku'               => 'product_code',
        'description'       => 'full_description',
        'short_description' => 'description',
        // Searchanise do not support sorting by category parameters
        // Uncomment and adjust in further
        //'position'          => 'category.position',
        //'category_ids'      => 'category.category_id',
    ];

    /**
     * Pager page size backup variable.
     * Page size is always set to false in _renderFiltersBefore() after executing the query to Searchanise,
     * to be sure to pull correctly all matched products from the DB.
     * But it needs to be reset so low-level methods like getLastPageNumber() still work.
     *
     * @var integer|false
     */
    private $originalPageSize = false;

    /**
     * FIXME: Quick fix for Advanced search
     * Magento initialized product collection twice during advance search
     * And we have to store filters data to static variable to share between two class instances
     * Should be fixed in further and this variable should be removed.
     *
     * @var array
     */
    private static $storeFilters = [];

    public function __construct(
        \Magento\Framework\Data\Collection\EntityFactory $entityFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Eav\Model\Config $eavConfig,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Eav\Model\EntityFactory $eavEntityFactory,
        \Magento\Catalog\Model\ResourceModel\Helper $resourceHelper,
        \Magento\Framework\Validator\UniversalFactory $universalFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Module\Manager $moduleManager,
        \Magento\Catalog\Model\Indexer\Product\Flat\State $catalogProductFlatState,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Catalog\Model\Product\OptionFactory $productOptionFactory,
        \Magento\Catalog\Model\ResourceModel\Url $catalogUrl,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\Stdlib\DateTime $dateTime,
        \Magento\Customer\Api\GroupManagementInterface $groupManagement,
        \Searchanise\SearchAutocomplete\Search\Request\Builder $requestBuilder,
        \Magento\Search\Model\SearchEngine $searchEngine,
        \Searchanise\SearchAutocomplete\Model\Configuration $configuration,
        \Magento\Framework\DB\Adapter\AdapterInterface $connection = null,
        $searchRequestName = 'catalog_view_container'
    ) {
        parent::__construct(
            $entityFactory,
            $logger,
            $fetchStrategy,
            $eventManager,
            $eavConfig,
            $resource,
            $eavEntityFactory,
            $resourceHelper,
            $universalFactory,
            $storeManager,
            $moduleManager,
            $catalogProductFlatState,
            $scopeConfig,
            $productOptionFactory,
            $catalogUrl,
            $localeDate,
            $customerSession,
            $dateTime,
            $groupManagement,
            $connection
        );

        $this->requestBuilder    = $requestBuilder;
        $this->searchEngine      = $searchEngine;
        $this->searchRequestName = $searchRequestName;
        $this->configuration	 = $configuration;
    }

    /**
     * {@inheritDoc}
     */
    public function getSize()
    {
    	if (!$this->configuration->isSearchaniseEngineEnable($this->_storeManager->getStore()->getId())) {
            return parent::getSize();
        }

        if ($this->_totalRecords === null) {
            $this->_loadProductCounts();
        }

        return $this->_totalRecords;
    }

    /**
     * {@inheritDoc}
     */
    public function setOrder($attribute, $dir = self::SORT_ORDER_DESC)
    {
    	if (!$this->configuration->isSearchaniseEngineEnable($this->_storeManager->getStore()->getId())) {
            $this->_orders = ['field' => $attribute, 'dir' => $dir];

            if ($attribute != 'relevance') {
                parent::setOrder($attribute, $dir);
            }
        } else {
            $this->_orders[$attribute] = $dir;
        }

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function addFieldToFilter($attribute, $condition = null)
    {
        $field = $this->_mapFieldName($attribute);

        if (!empty($field)) {
            $this->attrFilters[$field] = $condition;
        }

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function addAttributeToSort($attribute, $dir = self::SORT_ORDER_ASC)
    {
    	if (!$this->configuration->isSearchaniseEngineEnable($this->_storeManager->getStore()->getId())) {
            return parent::addAttributeToSort($attribute, $dir);
        }

        return $this->setOrder($attribute, $dir);
    }

    /**
     * Append a prebuilt (QueryInterface) query filter to the collection.
     *
     * @param QueryInterface $queryFilter Query filter.
     *
     * @return $this
     */
    public function addQueryFilter(QueryInterface $queryFilter)
    {
        $this->queryFilters[] = $queryFilter;

        return $this;
    }

    /**
     * Add search query filter
     *
     * @param string $query Search query text.
     *
     * @return \Searchanise\SearchAutocomplete\Model\ResourceModel\Product\Fulltext\Collection
     */
    public function addSearchFilter($query)
    {
    	if (!$this->configuration->isSearchaniseEngineEnable($this->_storeManager->getStore()->getId())) {
            $this->queryText = trim($this->queryText . ' ' . $query);
        } else {
            $this->queryText = $query;
        }

        return $this;
    }

    /**
     * Append a facet to the collection
     *
     * @param string $field       Facet field.
     * @param string $facetType   Facet type.
     * @param array  $facetConfig Facet config params.
     * @param array  $facetFilter Facet filter.
     *
     * @return \Searchanise\SearchAutocomplete\Model\ResourceModel\Product\Fulltext\Collection
     */
    public function addFacet($field, $facetType, $facetConfig, $facetFilter = null)
    {
        $this->facets[$field] = ['type' => $facetType, 'filter' => $facetFilter, 'config' => $facetConfig];

        return $this;
    }

    /**
     * Return field faceted data from faceted search result.
     *
     * @param string $field Facet field.
     *
     * @return array
     */
    public function getFacetedData($field)
    {
        $this->_renderFilters();
        $result = [];

        $aggregations = $this->queryResponse->getAggregations();

        if (null !== $aggregations) {
            $bucket = $aggregations
                ->getBucket($field . \Magento\CatalogSearch\Model\Search\RequestGenerator::BUCKET_SUFFIX);

            if ($bucket) {
                foreach ($bucket->getValues() as $value) {
                    $metrics = $value->getMetrics();
                    $result[$metrics['value']] = $metrics;
                }
            }
        }

        return $result;
    }

    /**
     * {@inheritDoc}
     */
    public function addCategoryFilter(\Magento\Catalog\Model\Category $category)
    {
    	if (!$this->configuration->isSearchaniseEngineEnable($this->_storeManager->getStore()->getId())) {
            $this->addFieldToFilter('category_ids', $category->getId());
            return parent::addCategoryFilter($category);
        }

        $categoryId = $category;

        if (is_object($category)) {
            $categoryId = $category->getId();
        }

        $this->attrFilters['category_ids'][] = $categoryId;
        $this->_productLimitationFilters['category_ids'] = $categoryId;

        $this->_addFilterSubCategories($category);

        return $this;
    }

    /**
     * Add subcategories to filter
     *
     * @param \Magento\Catalog\Model\Category $category
     * @return \Searchanise\SearchAutocomplete\Model\ResourceModel\Product\Fulltext\Collection
     */
    private function _addFilterSubCategories(\Magento\Catalog\Model\Category $category)
    {
        if (!empty($category) && $category->getChildrenCount() > 0) {
            foreach ($category->getChildrenCategories() as $child) {
                if (!in_array($child->getId(), $this->attrFilters['category_ids'])) {
                    $this->attrFilters['category_ids'][] = $child->getId();

                    $this->_addFilterSubCategories($child);
                }
            }
        }

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function setVisibility($visibility)
    {
    	if (!$this->configuration->isSearchaniseEngineEnable($this->_storeManager->getStore()->getId())) {
            return parent::setVisibility($visibility);
        }

        return $this->addFieldToFilter('visibility', $visibility);
    }

    /**
     * Load the product count by attribute set id.
     *
     * @return array
     */
    /*public function getProductCountByAttributeSetId()
    {
        if ($this->countByAttributeSet === null) {
            $this->_loadProductCounts();
        }

        return $this->countByAttributeSet;
    }*/

    /**
     * Filter in stock product.
     *
     * @return \Searchanise\SearchAutocomplete\Model\ResourceModel\Product\Fulltext\Collection
     */
    public function addIsInStockFilter()
    {
        return $this->addFieldToFilter('is_in_stock', 1);
    }

    /**
     * Set param for a sort order.
     *
     * @param string $sortName     Sort order name (eg. position, ...).
     * @param string $sortField    Sort field.
     * @param string $nestedPath   Optional nested path for the sort field.
     * @param array  $nestedFilter Optional nested filter for the sort field.
     * TODO: Used?
     *
     * @return \Searchanise\SearchAutocomplete\Model\ResourceModel\Product\Fulltext\Collection
     */
    public function addSortFilterParameters($sortName, $sortField)
    {
        $sortParams = [];

        if (isset($this->_productLimitationFilters['sortParams'])) {
            $sortParams = $this->_productLimitationFilters['sortParams'];
        }

        $sortParams[$sortName] = [
            'sortField'    => $sortField,
        ];

        $this->_productLimitationFilters['sortParams'] = $sortParams;

        return $this;
    }

    /**
     * Return standard search criteria builder
     *
     * @return Magento\Framework\Api\Search\SearchCriteriaBuilder
     */
    private function _getSearchCriteriaBuilder()
    {
        return \Magento\Framework\App\ObjectManager::getInstance()
            ->get('\Magento\Framework\Api\Search\SearchCriteriaBuilder');
    }

    /**
     * Returns standard filter builder
     *
     * @return Magento\Framework\Api\FilterBuilder
     */
    private function _getFilterBuilder()
    {
        return \Magento\Framework\App\ObjectManager::getInstance()
            ->get('\Magento\Framework\Api\FilterBuilder');
    }

    /**
     * Returns standard search interface
     *
     * @return Magento\Search\Api\SearchInterface
     */
    private function _getSearch($class = '\Magento\Search\Api\SearchInterface')
    {
        return \Magento\Framework\App\ObjectManager::getInstance()->get($class);
    }

    /**
     * Returns standard search result factory
     *
     * @return Magento\Framework\Api\Search\SearchResultFactory
     */
    private function _getSearchResultFactory()
    {
        return \Magento\Framework\App\ObjectManager::getInstance()
            ->get('\Magento\Framework\Api\Search\SearchResultFactory');
    }

    /**
     * Returns standard temporarily storage factory
     *
     * @return Magento\Framework\Search\Adapter\Mysql\TemporaryStorageFactory
     */
    private function _getTemporaryStorageFactory()
    {
        return \Magento\Framework\App\ObjectManager::getInstance()
            ->get('\Magento\Framework\Search\Adapter\Mysql\TemporaryStorageFactory');
    }

    /**
     * Returns request builder. Used for magento 2.0.x
     *
     * @return \Magento\Framework\Search\Request\Builder
     */
    private function _getRequestBuilder()
    {
        return \Magento\Framework\App\ObjectManager::getInstance()
            ->get('\Magento\Framework\Search\Request\Builder');
    }

    /**
     * Emulate standard search for magento 2.1.x
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return object
     */
    private function processSearch21()
    {
        $searchCriteriaBuilder = $this->_getSearchCriteriaBuilder();
        $filterBuilder = $this->_getFilterBuilder();

        if ($this->queryText) {
            $filterBuilder->setField('search_term');
            $filterBuilder->setValue($this->queryText);
            $searchCriteriaBuilder->addFilter($filterBuilder->create());
        }

        $priceRangeCalculation = $this->_scopeConfig->getValue(
            \Magento\Catalog\Model\Layer\Filter\Dynamic\AlgorithmFactory::XML_PATH_RANGE_CALCULATION,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        if ($priceRangeCalculation) {
            $filterBuilder->setField('price_dynamic_algorithm');
            $filterBuilder->setValue($priceRangeCalculation);
            $searchCriteriaBuilder->addFilter($filterBuilder->create());
        }

        if (!empty($this->attrFilters)) {
            foreach ($this->attrFilters as $field_name => $condition) {
                if (in_array($field_name, $this->fieldNameMapping)) {
                    $field_name = array_search($field_name, $this->fieldNameMapping);
                }

                if (!is_array($condition) || !in_array(key($condition), ['from', 'to'])) {
                    $filterBuilder->setField($field_name);
                    $filterBuilder->setValue($condition);
                    $searchCriteriaBuilder->addFilter($filterBuilder->create());
                } else {
                    if (!empty($condition['from'])) {
                        $filterBuilder->setField("{$field_name}.from");
                        $filterBuilder->setValue($condition['from']);
                        $searchCriteriaBuilder->addFilter($filterBuilder->create());
                    }
                    if (!empty($condition['to'])) {
                        $filterBuilder->setField("{$field_name}.to");
                        $filterBuilder->setValue($condition['to']);
                        $searchCriteriaBuilder->addFilter($filterBuilder->create());
                    }
                }
            }
        }

        $searchCriteria = $searchCriteriaBuilder->create();
        $searchCriteria->setRequestName($this->searchRequestName);

        try {
            $this->queryResponse = $this->_getSearch()->search($searchCriteria);
        } catch (\Magento\Framework\Search\Request\EmptyRequestDataException $e) {
            /** @var \Magento\Framework\Api\Search\SearchResultInterface $searchResult */
            $this->queryResponse = $this->_getSearchResultFactory()->create()->setItems([]);
        } catch (\Magento\Framework\Search\Request\NonExistingRequestNameException $e) {
            $this->_logger->error($e->getMessage());
            throw new \Magento\Framework\Exception\LocalizedException(
                __('Sorry, something went wrong. You can find out more in the error log.')
            );
        }

        $temporaryStorage = $this->_getTemporaryStorageFactory()->create();
        $table = $temporaryStorage->storeApiDocuments($this->queryResponse->getItems());

        return $table;
    }

    /**
     * Emulate standard search for magento 2.0.x
     *
     * @return object
     */
    private function processSearch20()
    {
        $requestBuilder = $this->_getRequestBuilder();

        $requestBuilder->bindDimension('scope', $this->getStoreId());

        if (!empty($this->queryText)) {
            $requestBuilder->bind('search_term', $this->queryText);
        }

        $priceRangeCalculation = $this->_scopeConfig->getValue(
            \Magento\Catalog\Model\Layer\Filter\Dynamic\AlgorithmFactory::XML_PATH_RANGE_CALCULATION,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        if ($priceRangeCalculation) {
            $requestBuilder->bind('price_dynamic_algorithm', $priceRangeCalculation);
        }

        if (!empty($this->attrFilters)) {
            foreach ($this->attrFilters as $field_name => $condition) {
                if (in_array($field_name, $this->fieldNameMapping)) {
                    $field_name = array_search($field_name, $this->fieldNameMapping);
                }

                if (!is_array($condition) || !in_array(key($condition), ['from', 'to'])) {
                    $requestBuilder->bind($field_name, $condition);
                } else {
                    if (!empty($condition['from'])) {
                        $requestBuilder->bind("{$field_name}.from", $condition['from']);
                    }
                    if (!empty($condition['to'])) {
                        $requestBuilder->bind("{$field_name}.to", $condition['to']);
                    }
                }
            }
        }

        $requestBuilder->setRequestName($this->searchRequestName);
        $queryRequest = $requestBuilder->create();

        $this->queryResponse = $this->_getSearch('\Magento\Search\Model\SearchEngine')->search($queryRequest);

        $temporaryStorage = $this->_getTemporaryStorageFactory()->create();
        $table = $temporaryStorage->storeDocuments($this->queryResponse->getIterator());

        return $table;
    }

    /**
     * Performs the standard search
     *
     * @throws LocalizedException
     */
    private function _processStandardSearch()
    {
        $version = \Magento\Framework\App\ObjectManager::getInstance()
            ->get('\Searchanise\SearchAutocomplete\Helper\ApiSe')
            ->getMagentoVersion();

        if (version_compare($version, '2.1', '>=')) {
            $table = $this->processSearch21();
            $this->_totalRecords = $this->queryResponse->getTotalCount();
        } else {
            $table = $this->processSearch20();
            $this->_totalRecords = $this->queryResponse->count();
        }

        $this->getSelect()->joinInner([
            'search_result' => $table->getName(),
        ], 'e.entity_id = search_result.'
            . \Magento\Framework\Search\Adapter\Mysql\TemporaryStorage::FIELD_ENTITY_ID, []);

        if ($this->_orders && 'relevance' === $this->_orders['field']) {
            $this->getSelect()->order(
                'search_result.'
                . \Magento\Framework\Search\Adapter\Mysql\TemporaryStorage::FIELD_SCORE
                . ' '
                . $this->_orders['dir']
            );
        }
    }

    /**
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     *
     * {@inheritdoc}
     */
    protected function _renderFiltersBefore()
    {
    	if (!$this->configuration->isSearchaniseEngineEnable($this->_storeManager->getStore()->getId())) {
            $this->_processStandardSearch();
        } else {
            $searchRequest = $this->prepareRequest();

            $this->queryResponse = $this->searchEngine->search($searchRequest);

            // Update the product count.
            $this->_totalRecords = $this->queryResponse->count();

            // Filter search results. The pagination has to be resetted since it is managed by the engine itself.
            $docIds = array_map(
                function (\Magento\Framework\Api\Search\Document $doc) {
                    return (int) $doc->getId();
                },
                $this->queryResponse->getIterator()->getArrayCopy()
            );

            if (empty($docIds)) {
                $docIds[] = 0;
            }

            $this->getSelect()->where('e.entity_id IN (?)', ['in' => $docIds]);
            $this->originalPageSize = $this->_pageSize;
            $this->_pageSize = false;
        }

        return parent::_renderFiltersBefore();
    }

    /**
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     *
     * {@inheritDoc}
     */
    protected function _renderFilters()
    {
        $this->_filters = [];

        return parent::_renderFilters();
    }

    /**
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     *
     * {@inheritDoc}
     */
    protected function _renderOrders()
    {
    	if (!$this->configuration->isSearchaniseEngineEnable($this->_storeManager->getStore()->getId())) {
            return parent::_renderOrders();
        }

        // Sort orders are managed through the search engine and are added through the prepareRequest method.
        return $this;
    }

    /**
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     *
     * {@inheritDoc}
     */
    protected function _afterLoad()
    {
    	if (!$this->configuration->isSearchaniseEngineEnable($this->_storeManager->getStore()->getId())) {
            return parent::_afterLoad();
        }

        // Resort items according the search response.
        $orginalItems = $this->_items;
        $this->_items = [];

        foreach ($this->queryResponse->getIterator() as $document) {
            $documentId = $document->getId();

            if (isset($orginalItems[$documentId])) {
                $orginalItems[$documentId]->setDocumentScore($document->getScore());
                $orginalItems[$documentId]->setDocumentSource($document->getSource());
                $this->_items[$documentId] = $orginalItems[$documentId];
            }
        }

        if (false === $this->_pageSize && false !== $this->originalPageSize) {
            $this->_pageSize = $this->originalPageSize;
        }

        return parent::_afterLoad();
    }

    /**
     * Prepare the search request before it will be executed.
     *
     * @return RequestInterface
     */
    private function prepareRequest()
    {
        if (!empty(self::$storeFilters['attrFilters'])) {
            $this->attrFilters = self::$storeFilters['attrFilters'];
        }

        if (!empty(self::$storeFilters['queryFilters'])) {
            $this->queryFilters = self::$storeFilters['queryFilters'];
        }

        // Store id and request name.
        $storeId           = $this->getStoreId();
        $searchRequestName = $this->searchRequestName;

        // Pagination params.
        $size = $this->_pageSize ? $this->_pageSize : $this->getSize();
        $from = $size * (max(1, $this->getCurPage()) - 1);

        // Query text.
        $queryText = $this->queryText;
        // Setup sort orders.
        $sortOrders = $this->_prepareSortOrders();

        $showOutOfStock = $this->_scopeConfig
            ->getValue(\Magento\CatalogInventory\Model\Configuration::XML_PATH_SHOW_OUT_OF_STOCK);

        if (empty($showOutOfStock)) {
            $this->addIsInStockFilter();
        }

        $searchRequest = $this->requestBuilder->create(
            $storeId,
            $searchRequestName,
            $from,
            $size,
            $queryText,
            $sortOrders,
            $this->attrFilters,
            $this->queryFilters,
            $this->facets
        );

        return $searchRequest;
    }

    /**
     * Prepare sort orders for the request builder.
     *
     * @return array()
     */
    private function _prepareSortOrders()
    {
        $sortOrders = [];

        $useProductuctLimitation = isset($this->_productLimitationFilters['sortParams']);

        foreach ($this->_orders as $attribute => $direction) {
            $sortParams = ['direction' => $direction];

            if ($useProductuctLimitation && isset($this->_productLimitationFilters['sortParams'][$attribute])) {
                $sortField  = $this->_productLimitationFilters['sortParams'][$attribute]['sortField'];
                $sortParams = array_merge($sortParams, $this->_productLimitationFilters['sortParams'][$attribute]);
            }

            $sortField = $this->_mapFieldName($attribute);
            $sortOrders[$sortField] = $sortParams;
        }

        return $sortOrders;
    }

    /**
     * Convert field names
     * (eg. name => title).
     *
     * @param string $fieldName Field name to be mapped.
     *
     * @return string
     */
    private function _mapFieldName($fieldName)
    {
        if (isset($this->fieldNameMapping[$fieldName])) {
            $fieldName = $this->fieldNameMapping[$fieldName];
        }

        return $fieldName;
    }

    /**
     * Load product count :
     *  - collection size
     *  - number of products by attribute set
     *
     * @return void
     */
    private function _loadProductCounts()
    {
        $storeId     = $this->getStoreId();
        $requestName = $this->searchRequestName;

        // Query text.
        $queryText = $this->queryText;

        $setIdFacet = ['attribute_set_id' => [
            'type' => BucketInterface::TYPE_TERM,
            'config' => [
                'size' => 0
            ]]
        ];

        $showOutOfStock = $this->_scopeConfig
            ->getValue(\Magento\CatalogInventory\Model\Configuration::XML_PATH_SHOW_OUT_OF_STOCK);

        if (empty($showOutOfStock)) {
            $this->addIsInStockFilter();
        }

        $searchRequest = $this->requestBuilder->create(
            $storeId,
            $requestName,
            0,
            1,  // FIXME
            $queryText,
            [],
            $this->attrFilters,
            $this->queryFilters,
            $setIdFacet
        );

        $searchResponse = $this->searchEngine->search($searchRequest);

        $this->_totalRecords = $searchResponse->count();
        $this->countByAttributeSet = [];

        $bucket = $searchResponse->getAggregations()->getBucket('attribute_set_id');

        if ($bucket) {
            foreach ($bucket->getValues() as $value) {
                $metrics = $value->getMetrics();
                $this->countByAttributeSet[$metrics['value']] = $metrics['count'];
            }
        }

        self::$storeFilters = [
            'attrFilters' => $this->attrFilters,
            'queryFilters' => $this->queryFilters,
        ];
    }
}
