<?php

namespace Searchanise\SearchAutocomplete\Model\ResourceModel\Product\Advanced;

use Searchanise\SearchAutocomplete\Model\ResourceModel\Product\Fulltext\Collection as FulltextCollection;

/**
 * Advanced Search Product Collection
 */
class Collection extends FulltextCollection
{
    /**
     * Add multiple fields to filter
     *
     * @param array $fields The fields to filter
     *
     * @return $this
     */
    public function addFieldsToFilter($fields)
    {
        if ($fields) {
            foreach ($fields as $fieldByType) {
                foreach ($fieldByType as $attributeId => $condition) {
                    $attributeCode = $this->getEntity()->getAttribute($attributeId)->getAttributeCode();
                    $condition = $this->_cleanCondition($condition);

                    if (null !== $condition) {
                        $this->addFieldToFilter($attributeCode, $condition);
                    }
                }
            }
        }

        return $this;
    }

    /**
     * Ensure proper building of condition
     *
     * @param array|string $condition The condition to apply
     *
     * @return array|string|null
     */
    private function _cleanCondition($condition)
    {
        if (is_array($condition)) {
            $condition = array_filter($condition);

            if (empty($condition)) {
                $condition = null;
            }
        }

        return $condition;
    }
}
