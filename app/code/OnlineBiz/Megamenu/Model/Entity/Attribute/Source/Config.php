<?php

/**
 * OnlineBiz Software Solution
 * 
 * @project lightrabbit2
 * @version 0.0.1
 * @encoding UTF-8
 * @author Joe Vu<joe@onlinebizsoft.com>
 * @see http://onlinebizsoft.com
 * @copyright (c) 2017 , OnlineBiz Software Solution
 * 
 * Create at: Dec 28, 2017 7:13:31 PM
 */
namespace OnlineBiz\Megamenu\Model\Entity\Attribute\Source;

class Config extends  \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource{
     /**
     * @var array
     */
    protected $_optionsData;

    /**
     * @param array $options
     * @codeCoverageIgnore
     */
    public function __construct(array $options = array())
    {
        $this->_optionsData = $options;
    }

    /**
     * Retrieve all options for the source from configuration
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return array
     */
    public function getAllOptions()
    {
        $this->_options = [];
        if ($this->_options === null) {
            if (!is_array($this->_optionsData)) {
                throw new \Magento\Framework\Exception\LocalizedException(__('No options found.'));
            }
            foreach ($this->_optionsData as $option) {
                $this->_options[] = ['value' => $option['value'], 'label' => __($option['label'])];
            }
        }

        return $this->_options;
    }
}
