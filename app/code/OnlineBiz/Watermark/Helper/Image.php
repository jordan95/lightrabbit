<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace OnlineBiz\Watermark\Helper;

/**
 * Catalog image helper
 *
 * @api
 * @SuppressWarnings(PHPMD.TooManyFields)
 * @since 100.0.2
 */
class Image extends \Magento\Catalog\Helper\Image
{
    /**
     * Initialize Helper to work with Image
     *
     * @param \Magento\Catalog\Model\Product $product
     * @param string $imageId
     * @param array $attributes
     * @return $this
     */
    public function init($product, $imageId, $attributes = [])
    {
        $this->_reset();

        $this->attributes = array_merge(
            $this->getConfigView()->getMediaAttributes('Magento_Catalog', self::MEDIA_TYPE_CONFIG_NODE, $imageId),
            $attributes
        );

        $this->setProduct($product);
        $this->setImageProperties();
        if(!$product->getDisableWatermark()){
            
            $check_size_lightware =  $this->scopeConfig->getValue(
                                        "design/watermark/{$this->getType()}_size_lightware",
                                        \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                                    );
            $check_size_fidelity = $this->scopeConfig->getValue(
                                        "design/watermark/{$this->getType()}_size_fidelity",
                                        \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                                    );
            $check_size_saxby = $this->scopeConfig->getValue(
                                    "design/watermark/{$this->getType()}_size_saxby",
                                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                                );
            $check_size_base =  $this->scopeConfig->getValue(
                                    "design/watermark/{$this->getType()}_size",
                                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                                );

            if(is_null($check_size_lightware) && is_null($check_size_fidelity) && is_null($check_size_saxby) &&  is_null($check_size_base)){
                    return $this;
            }else{
                    if($product->getWatermark()=="117"){
                        $this->setWatermark(
                            $this->scopeConfig->getValue(
                                "design/watermark/{$this->getType()}_image_lightware",
                                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                            )
                        );
                        $this->setWatermarkSize(
                            $this->scopeConfig->getValue(
                                "design/watermark/{$this->getType()}_size_lightware",
                                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                            )
                        );

                     }elseif($product->getWatermark()=="116") {
                         $this->setWatermark(
                            $this->scopeConfig->getValue(
                                "design/watermark/{$this->getType()}_image_fidelity",
                                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                            )
                        );
                        $this->setWatermarkSize(
                            $this->scopeConfig->getValue(
                                "design/watermark/{$this->getType()}_size_fidelity",
                                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                            )
                        );
                     }
                     elseif($product->getWatermark()=="119") {
                         $this->setWatermark(
                            $this->scopeConfig->getValue(
                                "design/watermark/{$this->getType()}_image_saxby",
                                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                            )
                        );
                        $this->setWatermarkSize(
                            $this->scopeConfig->getValue(
                                "design/watermark/{$this->getType()}_size_saxby",
                                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                            )
                        );
                     }else{
                        $this->setWatermark(
                            $this->scopeConfig->getValue(
                                "design/watermark/{$this->getType()}_image",
                                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                            )
                        );
                        $this->setWatermarkSize(
                            $this->scopeConfig->getValue(
                                "design/watermark/{$this->getType()}_size",
                                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                            )
                        );
                     }
                    $this->setWatermarkImageOpacity(
                        $this->scopeConfig->getValue(
                            "design/watermark/{$this->getType()}_imageOpacity",
                            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                        )
                    );
                    $this->setWatermarkPosition(
                        $this->scopeConfig->getValue(
                            "design/watermark/{$this->getType()}_position",
                            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                        )
                    );
            }
             
        }

        return $this;
    }

}
