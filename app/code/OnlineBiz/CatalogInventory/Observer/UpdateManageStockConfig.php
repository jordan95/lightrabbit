<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace OnlineBiz\CatalogInventory\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

/**
 * Catalog inventory module observer
 */
class UpdateManageStockConfig implements ObserverInterface
{
    /**
     * Update items stock status and low stock date.
     *
     * @param EventObserver $observer
     * @return void
     */
    public function execute(EventObserver $observer)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $request =  $objectManager->get('\Magento\Framework\App\Request\Http'); 
        $params = $request->getParams();
        if(isset($params['website'])){
            $websiteId = (int)$params['website'];
            if($websiteId && $websiteId == 11){
                $this->_resources = $objectManager->get('Magento\Framework\App\ResourceConnection');
                if(isset($params['groups']['item_options']['fields']['manage_stock']['value'])){
                    $manageStock = (int)$params['groups']['item_options']['fields']['manage_stock']['value'];
                    $connection= $this->_resources->getConnection();
                    $values = ['manage_stock' => $manageStock, 'use_config_manage_stock' => 0];
                    $where = sprintf(
                        'website_id = %1$d',
                         $websiteId
                    );
                    $connection->update($this->_resources->getTableName('cataloginventory_stock_item'), $values, $where);
                }
            }
        }
    }
}
