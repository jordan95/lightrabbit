<?php
namespace OnlineBiz\Bundle\Model\Product;

use Magento\Bundle\Model\Product\Type as BaseType;

class Type extends BaseType
{
    protected function getQty($selection, $qtys, $selectionOptionId)
    {
        if ($selection->getSelectionCanChangeQty() && isset($qtys[$selectionOptionId])) {
            if(is_array($qtys[$selectionOptionId]) && isset($qtys[$selectionOptionId][$selection->getSelectionId()])){
                $qty = (float)$qtys[$selectionOptionId][$selection->getSelectionId()] > 0 ? $qtys[$selectionOptionId][$selection->getSelectionId()] : 1;
            }else{
                $qty = (float)$qtys[$selectionOptionId] > 0 ? $qtys[$selectionOptionId] : 1;
            }
        } else {
            $qty = (float)$selection->getSelectionQty() ? $selection->getSelectionQty() : 1;
        }
        $qty = (float)$qty;

        return $qty;
    }
}
