<?php

/**
 * OnlineBiz Software Solution
 * 
 * @project lightrabbit2
 * @version 0.0.1
 * @encoding UTF-8
 * @author Joe Vu<joe@onlinebizsoft.com>
 * @see http://onlinebizsoft.com
 * @copyright (c) 2017 , OnlineBiz Software Solution
 * 
 * Create at: Jan 22, 2018 10:48:17 AM
 */

namespace OnlineBiz\Bundle\Block\Adminhtml\Catalog\Product\Edit\Tab\Bundle\Option;

use Magento\Bundle\Block\Adminhtml\Catalog\Product\Edit\Tab\Bundle\Option\Selection as BaseSelection;

class Selection extends BaseSelection{
    
}