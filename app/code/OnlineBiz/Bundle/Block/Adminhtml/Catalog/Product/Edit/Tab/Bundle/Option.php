<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace OnlineBiz\Bundle\Block\Adminhtml\Catalog\Product\Edit\Tab\Bundle;

use Magento\Bundle\Block\Adminhtml\Catalog\Product\Edit\Tab\Bundle\Option as BaseOption;

class Option extends BaseOption
{
}
