<?php
namespace OnlineBiz\DescriptionBottom\Setup;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeData implements UpgradeDataInterface
{
	public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
	{
		if (version_compare($context->getVersion(), '1.0.2', '<')) {
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		    $resource   = $objectManager->get('Magento\Framework\App\ResourceConnection');
		    $connection = $resource->getConnection();
		    $tableName_entity  = $resource->getTableName('eav_entity_attribute'); //gives table name with prefix
		     $tableName  = $resource->getTableName('eav_attribute');
		    /** @var CustomerSetup $customerSetup */
		    $sql = "select attribute_id from " . $tableName . " where attribute_code ='description_attribute_bottom'";
		    $result = $connection->fetchAll($sql);
		    $data = array_column($result, "attribute_id");
		    foreach ($data as $key => $value) {
		    	$attribute_id = $value;
		    }
		    if(isset($attribute_id) && is_string($attribute_id)){
		    	$sql_update = "UPDATE " . $tableName_entity . " SET attribute_set_id = 31 WHERE attribute_id = ".$attribute_id." and attribute_group_id = 4";
		     	$connection->query($sql_update);
		    }
		}
	}
}