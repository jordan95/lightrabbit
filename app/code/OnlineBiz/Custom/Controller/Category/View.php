<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace OnlineBiz\Custom\Controller\Category;

use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Model\Layer\Resolver;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Result\PageFactory;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class View extends \Magento\Catalog\Controller\Category\View
{
 
    public function execute()
    {
        $page = parent::execute();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $category = $objectManager->get('Magento\Framework\Registry')->registry('current_category');
        if ($category) {
            $description = "";
            $meta = "";
            $title = "";
            $meta = $category->getMetaDescription();
            $title = $category->getMetaTitle();
            $description = strip_tags($category->getDescription());
            if($meta !="" && !empty($meta)){
                $description  = $meta;
            }
            if($title !="" && !empty($title)){
                $title =strip_tags($title);
                $page->getConfig()->getTitle()->set($title);
            }else{
                $title = $category->getName();
                if($title !=""){
                    $title =strip_tags($title);
                    $page->getConfig()->getTitle()->set($title);
                }
            }
            if($description !=""){
                $page->getConfig()->setDescription($description);
            }
            return $page;
        }elseif (!$this->getResponse()->isRedirect()) {
            return $this->resultForwardFactory->create()->forward('noroute');
        }
        
    }
}
