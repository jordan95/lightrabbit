<?php

/**
 * OnlineBiz Software Solution
 * 
 * @project lightrabbit2
 * @version 0.0.1
 * @encoding UTF-8
 * @author Joe Vu<joe@onlinebizsoft.com>
 * @see http://onlinebizsoft.com
 * @copyright (c) 2017 , OnlineBiz Software Solution
 * 
 * Create at: Jan 19, 2018 9:34:26 AM
 */

namespace OnlineBiz\Custom\Controller\Cart;

use Magento\Checkout\Controller\Cart\Add as BaseCart;

class Add extends BaseCart {

    public function execute() {
        if (!$this->_formKeyValidator->validate($this->getRequest())) {
            return $this->resultRedirectFactory->create()->setPath('*/*/');
        }

        $params = $this->getRequest()->getParams();
        try {
            if (isset($params['qty'])) {
                $filter = new \Zend_Filter_LocalizedToNormalized(
                        ['locale' => $this->_objectManager->get(
                            \Magento\Framework\Locale\ResolverInterface::class
                    )->getLocale()]
                );
                $params['qty'] = $filter->filter($params['qty']);
            }

            $product = $this->_initProduct();
            $related = $this->getRequest()->getParam('related_product');

            /**
             * Check product availability
             */
            if (!$product) {
                return $this->goBack();
            }

            if (isset($params['data_qty']) && $product->getTypeId() == 'configurable') {
                foreach ($params['data_qty'] as $key => $spvl) {
                    if (!isset($params['qty_multi'][$key]) || !$params['qty_multi'][$key])
                        continue;
                    $storeId = $this->_objectManager->get('Magento\Store\Model\StoreManagerInterface')->getStore()->getId();
                    $_productConfig = $this->_objectManager->create('Magento\Catalog\Model\Product')->setStoreId($storeId)->load($params['product']);
                    $this->cart->addProduct($_productConfig, array(
                        'super_attribute' => $spvl,
                        'qty' => $params['qty_multi'][$key]
                    ));
                }
            }
            elseif ($product->getTypeId() == 'bundle') {

                if($product->getTypeId() == 'bundle' && $product->getPriceType()==0){
                    
                    $simple_id_of_bundle = array();
                    $product_simple_id = array();
                    $param_bundle_qty = array();
                    $params_bundle_qty = $this->getRequest()->getParam("bundle_option_qty");
                    $params_bundle_option = $this->getRequest()->getParam("bundle_option");
                    foreach($params_bundle_option as $key=>$value){
                        if (!is_array($value)){
                            foreach($params_bundle_qty as $k=>$val){
                                if (!is_array($val)){
                                    unset($params_bundle_qty[$k]);
                                    $params_bundle_qty[$k][$value] =$val;

                                }
                            }
                        }
                                        
                    }
                    foreach($params_bundle_qty as $key=>$value){
                        if (is_array($value) || is_object($value)){
                            foreach($value as $k =>$val){
                                if($val !="0" && !empty($val)){
                                    $simple_id_of_bundle[$k] = $val;
                                }   
                            } 
                        }
                                        
                    }
                    
                    $selectionCollection = $product->getTypeInstance()->getSelectionsCollection($product->getOptionsIds($product), $product);
                    foreach($selectionCollection as $key =>$option){
                        if(array_key_exists($option->getSelectionId(),$simple_id_of_bundle)){
                                $product_simple_id[$option->getProductId()] = $simple_id_of_bundle[$option->getSelectionId()];
                        }
                    }
                    foreach($product_simple_id as $id =>$qty){
                        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
         
                        $productRepository = $objectManager->get('\Magento\Catalog\Model\ProductRepository');
                         
                        // get product by product id 
                        $product_simple_of_bundle = $productRepository->getById($id);

                        $param_bundle_qty['qty'] = (int) $qty;
                        $param_bundle_qty['product'] = $id;
                        if(!empty($param_bundle_qty) && $param_bundle_qty['qty'] >=0){
                            $this->cart->addProduct($product_simple_of_bundle, $param_bundle_qty);
                        }
                        
                    }
                }else{
                    foreach ($params['bundle_option'] as $opt_id => $values) {
                        if (is_array($values)) {
                            foreach ($values as $key => $selectId) {
                                if (isset($params['bundle_option_qty'][$opt_id][$selectId]) && $params['bundle_option_qty'][$opt_id][$selectId] == 0) {
                                    unset($params['bundle_option'][$opt_id][$key]);
                                }
                            }
                        }
                    }
                    $this->cart->addProduct($product, $params);
                }
                
            } else {
                $this->cart->addProduct($product, $params);
            }
            if (!empty($related)) {
                $this->cart->addProductsByIds(explode(',', $related));
            }
            $this->cart->save();

            /**
             * @todo remove wishlist observer \Magento\Wishlist\Observer\AddToCart
             */
            $this->_eventManager->dispatch(
                    'checkout_cart_add_product_complete', ['product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponse()]
            );

            if (!$this->_checkoutSession->getNoCartRedirect(true)) {
                if (!$this->cart->getQuote()->getHasError()) {
                    $message = __(
                            'You added %1 to your shopping cart.', $product->getName()
                    );
                    $this->messageManager->addSuccessMessage($message);
                }
                return $this->goBack(null, $product);
            }
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            if ($this->_checkoutSession->getUseNotice(true)) {
                $this->messageManager->addNotice(
                        $this->_objectManager->get(\Magento\Framework\Escaper::class)->escapeHtml($e->getMessage())
                );
            } else {
                $messages = array_unique(explode("\n", $e->getMessage()));
                foreach ($messages as $message) {
                    $this->messageManager->addError(
                            $this->_objectManager->get(\Magento\Framework\Escaper::class)->escapeHtml($message)
                    );
                }
            }

            $url = $this->_checkoutSession->getRedirectUrl(true);

            if (!$url) {
                $cartUrl = $this->_objectManager->get(\Magento\Checkout\Helper\Cart::class)->getCartUrl();
                $url = $this->_redirect->getRedirectUrl($cartUrl);
            }

            return $this->goBack($url);
        } catch (\Exception $e) {
            $this->messageManager->addException($e, $e->getMessage());
            $this->_objectManager->get(\Psr\Log\LoggerInterface::class)->critical($e);
            return $this->goBack();
        }
    }

}
