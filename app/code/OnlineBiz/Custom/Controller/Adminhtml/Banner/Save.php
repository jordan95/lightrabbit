<?php

/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Bannerslider
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

namespace OnlineBiz\Custom\Controller\Adminhtml\Banner;

use Magento\Framework\App\Filesystem\DirectoryList;

/**
 * Save Banner action.
 * @category Magestore
 * @package  Magestore_Bannerslider
 * @module   Bannerslider
 * @author   Magestore Developer
 */
class Save extends \Magestore\Bannerslider\Controller\Adminhtml\Banner\Save
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();

        if ($data = $this->getRequest()->getPostValue()) {
            $model = $this->_bannerFactory->create();
            $storeViewId = $this->getRequest()->getParam('store');

            if ($id = $this->getRequest()->getParam(static::PARAM_CRUD_ID)) {
                $model->load($id);
            }

            $imageRequest = $this->getRequest()->getFiles('image');
            if ($imageRequest) {
                if (isset($imageRequest['name'])) {
                    $fileName = $imageRequest['name'];
                } else {
                    $fileName = '';
                }
            } else {
                $fileName = '';
            }

            if ($imageRequest && strlen($fileName)) {
                /*
                 * Save image upload
                 */
                try {
                    $uploader = $this->_uploaderFactory->create(['fileId' => 'image']);

                    $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png','svg']);

                    /** @var \Magento\Framework\Image\Adapter\AdapterInterface $imageAdapter */
                    $imageAdapter = $this->_adapterFactory->create();

                    //$uploader->addValidateCallback('banner_image', $imageAdapter, 'validateUploadFile');
                    $uploader->setAllowRenameFiles(true);
                    $uploader->setFilesDispersion(true);

                    /** @var \Magento\Framework\Filesystem\Directory\Read $mediaDirectory */
                    $mediaDirectory = $this->_objectManager->get('Magento\Framework\Filesystem')
                        ->getDirectoryRead(DirectoryList::MEDIA);
                    $result = $uploader->save(
                        $mediaDirectory->getAbsolutePath(\Magestore\Bannerslider\Model\Banner::BASE_MEDIA_PATH)
                    );
                    $data['image'] = \Magestore\Bannerslider\Model\Banner::BASE_MEDIA_PATH.$result['file'];
                } catch (\Exception $e) {
                    if ($e->getCode() == 0) {
                        $this->messageManager->addError($e->getMessage());
                    }
                }
            } else {
                if (isset($data['image']) && isset($data['image']['value'])) {
                    if (isset($data['image']['delete'])) {
                        $data['image'] = null;
                        $data['delete_image'] = true;
                    } elseif (isset($data['image']['value'])) {
                        $data['image'] = $data['image']['value'];
                    } else {
                        $data['image'] = null;
                    }
                }
            }

            //start code prepare save image background
            $backgroundRequest = $this->getRequest()->getFiles('background');
            if ($backgroundRequest) {
                if (isset($backgroundRequest['name'])) {
                    $fileName = $backgroundRequest['name'];
                } else {
                    $fileName = '';
                }
            } else {
                $fileName = '';
            }

            if ($backgroundRequest && strlen($fileName)) {
                /*
                 * Save image upload
                 */
                try {
                    $uploader = $this->_uploaderFactory->create(['fileId' => 'background']);

                    $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png','svg']);

                    /** @var \Magento\Framework\Image\Adapter\AdapterInterface $imageAdapter */
                    $imageAdapter = $this->_adapterFactory->create();

                    //$uploader->addValidateCallback('banner_image', $imageAdapter, 'validateUploadFile');
                    $uploader->setAllowRenameFiles(true);
                    $uploader->setFilesDispersion(true);

                    /** @var \Magento\Framework\Filesystem\Directory\Read $mediaDirectory */
                    $mediaDirectory = $this->_objectManager->get('Magento\Framework\Filesystem')
                        ->getDirectoryRead(DirectoryList::MEDIA);
                    $result = $uploader->save(
                        $mediaDirectory->getAbsolutePath(\Magestore\Bannerslider\Model\Banner::BASE_MEDIA_PATH)
                    );
                    $data['background'] = \Magestore\Bannerslider\Model\Banner::BASE_MEDIA_PATH.$result['file'];
                } catch (\Exception $e) {
                    if ($e->getCode() == 0) {
                        $this->messageManager->addError($e->getMessage());
                    }
                }
            } else {
                if (isset($data['background']) && isset($data['background']['value'])) {
                    if (isset($data['background']['delete'])) {
                        $data['background'] = null;
                        $data['delete_image'] = true;
                    } elseif (isset($data['background']['value'])) {
                        $data['background'] = $data['background']['value'];
                    } else {
                        $data['background'] = null;
                    }
                }
            }
             //end code prepare save image background desktop
            //start code prepare save image background tablet
            $backgroundRequest = $this->getRequest()->getFiles('banner_tablet');
            if ($backgroundRequest) {
                if (isset($backgroundRequest['name'])) {
                    $fileName = $backgroundRequest['name'];
                } else {
                    $fileName = '';
                }
            } else {
                $fileName = '';
            }

            if ($backgroundRequest && strlen($fileName)) {
                /*
                 * Save image upload
                 */
                try {
                    $uploader = $this->_uploaderFactory->create(['fileId' => 'banner_tablet']);

                    $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png','svg']);

                    /** @var \Magento\Framework\Image\Adapter\AdapterInterface $imageAdapter */
                    $imageAdapter = $this->_adapterFactory->create();

                    //$uploader->addValidateCallback('banner_image', $imageAdapter, 'validateUploadFile');
                    $uploader->setAllowRenameFiles(true);
                    $uploader->setFilesDispersion(true);

                    /** @var \Magento\Framework\Filesystem\Directory\Read $mediaDirectory */
                    $mediaDirectory = $this->_objectManager->get('Magento\Framework\Filesystem')
                        ->getDirectoryRead(DirectoryList::MEDIA);
                    $result = $uploader->save(
                        $mediaDirectory->getAbsolutePath(\Magestore\Bannerslider\Model\Banner::BASE_MEDIA_PATH)
                    );
                    $data['banner_tablet'] = \Magestore\Bannerslider\Model\Banner::BASE_MEDIA_PATH.$result['file'];
                } catch (\Exception $e) {
                    if ($e->getCode() == 0) {
                        $this->messageManager->addError($e->getMessage());
                    }
                }
            } else {
                if (isset($data['banner_tablet']) && isset($data['banner_tablet']['value'])) {
                    if (isset($data['banner_tablet']['delete'])) {
                        $data['banner_tablet'] = null;
                        $data['delete_image'] = true;
                    } elseif (isset($data['banner_tablet']['value'])) {
                        $data['banner_tablet'] = $data['banner_tablet']['value'];
                    } else {
                        $data['banner_tablet'] = null;
                    }
                }
            }
             //end code prepare save image background tablet
            //start code prepare save image background mobie
            $backgroundRequest = $this->getRequest()->getFiles('banner_mobie');
            if ($backgroundRequest) {
                if (isset($backgroundRequest['name'])) {
                    $fileName = $backgroundRequest['name'];
                } else {
                    $fileName = '';
                }
            } else {
                $fileName = '';
            }

            if ($backgroundRequest && strlen($fileName)) {
                /*
                 * Save image upload
                 */
                try {
                    $uploader = $this->_uploaderFactory->create(['fileId' => 'banner_mobie']);

                    $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png','svg']);

                    /** @var \Magento\Framework\Image\Adapter\AdapterInterface $imageAdapter */
                    $imageAdapter = $this->_adapterFactory->create();

                    //$uploader->addValidateCallback('banner_image', $imageAdapter, 'validateUploadFile');
                    $uploader->setAllowRenameFiles(true);
                    $uploader->setFilesDispersion(true);

                    /** @var \Magento\Framework\Filesystem\Directory\Read $mediaDirectory */
                    $mediaDirectory = $this->_objectManager->get('Magento\Framework\Filesystem')
                        ->getDirectoryRead(DirectoryList::MEDIA);
                    $result = $uploader->save(
                        $mediaDirectory->getAbsolutePath(\Magestore\Bannerslider\Model\Banner::BASE_MEDIA_PATH)
                    );
                    $data['banner_mobie'] = \Magestore\Bannerslider\Model\Banner::BASE_MEDIA_PATH.$result['file'];
                } catch (\Exception $e) {
                    if ($e->getCode() == 0) {
                        $this->messageManager->addError($e->getMessage());
                    }
                }
            } else {
                if (isset($data['banner_mobie']) && isset($data['banner_mobie']['value'])) {
                    if (isset($data['banner_mobie']['delete'])) {
                        $data['banner_mobie'] = null;
                        $data['delete_image'] = true;
                    } elseif (isset($data['banner_mobie']['value'])) {
                        $data['banner_mobie'] = $data['banner_mobie']['value'];
                    } else {
                        $data['banner_mobie'] = null;
                    }
                }
            }
            //end code prepare save image background mobie
            //start code prepare save image  banner_indicator_icon
            $backgroundRequest = $this->getRequest()->getFiles('banner_indicator_icon');
            if ($backgroundRequest) {
                if (isset($backgroundRequest['name'])) {
                    $fileName = $backgroundRequest['name'];
                } else {
                    $fileName = '';
                }
            } else {
                $fileName = '';
            }

            if ($backgroundRequest && strlen($fileName)) {
                /*
                 * Save image upload
                 */
                try {
                    $uploader = $this->_uploaderFactory->create(['fileId' => 'banner_indicator_icon']);

                    $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png','svg']);

                    /** @var \Magento\Framework\Image\Adapter\AdapterInterface $imageAdapter */
                    $imageAdapter = $this->_adapterFactory->create();

                    //$uploader->addValidateCallback('banner_image', $imageAdapter, 'validateUploadFile');
                    $uploader->setAllowRenameFiles(true);
                    $uploader->setFilesDispersion(true);

                    /** @var \Magento\Framework\Filesystem\Directory\Read $mediaDirectory */
                    $mediaDirectory = $this->_objectManager->get('Magento\Framework\Filesystem')
                        ->getDirectoryRead(DirectoryList::MEDIA);
                    $result = $uploader->save(
                        $mediaDirectory->getAbsolutePath(\Magestore\Bannerslider\Model\Banner::BASE_MEDIA_PATH)
                    );
                    $data['banner_indicator_icon'] = \Magestore\Bannerslider\Model\Banner::BASE_MEDIA_PATH.$result['file'];
                } catch (\Exception $e) {
                    if ($e->getCode() == 0) {
                        $this->messageManager->addError($e->getMessage());
                    }
                }
            } else {
                if (isset($data['banner_indicator_icon']) && isset($data['banner_indicator_icon']['value'])) {
                    if (isset($data['banner_indicator_icon']['delete'])) {
                        $data['banner_indicator_icon'] = null;
                        $data['delete_image'] = true;
                    } elseif (isset($data['banner_indicator_icon']['value'])) {
                        $data['banner_indicator_icon'] = $data['banner_indicator_icon']['value'];
                    } else {
                        $data['banner_indicator_icon'] = null;
                    }
                }
            }
            //end code prepare save image  banner_indicator_icon
            //start code prepare save image banner_indicator_hightlight
            $backgroundRequest = $this->getRequest()->getFiles('banner_indicator_hightlight');
            if ($backgroundRequest) {
                if (isset($backgroundRequest['name'])) {
                    $fileName = $backgroundRequest['name'];
                } else {
                    $fileName = '';
                }
            } else {
                $fileName = '';
            }

            if ($backgroundRequest && strlen($fileName)) {
                /*
                 * Save image upload
                 */
                try {
                    $uploader = $this->_uploaderFactory->create(['fileId' => 'banner_indicator_hightlight']);

                    $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png','svg']);

                    /** @var \Magento\Framework\Image\Adapter\AdapterInterface $imageAdapter */
                    $imageAdapter = $this->_adapterFactory->create();

                    //$uploader->addValidateCallback('banner_image', $imageAdapter, 'validateUploadFile');
                    $uploader->setAllowRenameFiles(true);
                    $uploader->setFilesDispersion(true);

                    /** @var \Magento\Framework\Filesystem\Directory\Read $mediaDirectory */
                    $mediaDirectory = $this->_objectManager->get('Magento\Framework\Filesystem')
                        ->getDirectoryRead(DirectoryList::MEDIA);
                    $result = $uploader->save(
                        $mediaDirectory->getAbsolutePath(\Magestore\Bannerslider\Model\Banner::BASE_MEDIA_PATH)
                    );
                    $data['banner_indicator_hightlight'] = \Magestore\Bannerslider\Model\Banner::BASE_MEDIA_PATH.$result['file'];
                } catch (\Exception $e) {
                    if ($e->getCode() == 0) {
                        $this->messageManager->addError($e->getMessage());
                    }
                }
            } else {
                if (isset($data['banner_indicator_hightlight']) && isset($data['banner_indicator_hightlight']['value'])) {
                    if (isset($data['banner_indicator_hightlight']['delete'])) {
                        $data['banner_indicator_hightlight'] = null;
                        $data['delete_image'] = true;
                    } elseif (isset($data['banner_indicator_hightlight']['value'])) {
                        $data['banner_indicator_hightlight'] = $data['banner_indicator_hightlight']['value'];
                    } else {
                        $data['banner_indicator_hightlight'] = null;
                    }
                }
            }
            //end code prepare save image banner_indicator_hightlight
             //start code prepare save image banner_deal_icon
            $backgroundRequest = $this->getRequest()->getFiles('banner_deal_icon');
            if ($backgroundRequest) {
                if (isset($backgroundRequest['name'])) {
                    $fileName = $backgroundRequest['name'];
                } else {
                    $fileName = '';
                }
            } else {
                $fileName = '';
            }

            if ($backgroundRequest && strlen($fileName)) {
                /*
                 * Save image upload
                 */
                try {
                    $uploader = $this->_uploaderFactory->create(['fileId' => 'banner_deal_icon']);

                    $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png','svg']);

                    /** @var \Magento\Framework\Image\Adapter\AdapterInterface $imageAdapter */
                    $imageAdapter = $this->_adapterFactory->create();

                    //$uploader->addValidateCallback('banner_image', $imageAdapter, 'validateUploadFile');
                    $uploader->setAllowRenameFiles(true);
                    $uploader->setFilesDispersion(true);

                    /** @var \Magento\Framework\Filesystem\Directory\Read $mediaDirectory */
                    $mediaDirectory = $this->_objectManager->get('Magento\Framework\Filesystem')
                        ->getDirectoryRead(DirectoryList::MEDIA);
                    $result = $uploader->save(
                        $mediaDirectory->getAbsolutePath(\Magestore\Bannerslider\Model\Banner::BASE_MEDIA_PATH)
                    );
                    $data['banner_deal_icon'] = \Magestore\Bannerslider\Model\Banner::BASE_MEDIA_PATH.$result['file'];
                } catch (\Exception $e) {
                    if ($e->getCode() == 0) {
                        $this->messageManager->addError($e->getMessage());
                    }
                }
            } else {
                if (isset($data['banner_deal_icon']) && isset($data['banner_deal_icon']['value'])) {
                    if (isset($data['banner_deal_icon']['delete'])) {
                        $data['banner_deal_icon'] = null;
                        $data['delete_image'] = true;
                    } elseif (isset($data['banner_deal_icon']['value'])) {
                        $data['banner_deal_icon'] = $data['banner_deal_icon']['value'];
                    } else {
                        $data['banner_deal_icon'] = null;
                    }
                }
            }
            //end code prepare save image banner_deal_icon
             //start code prepare save image banner_deal_hightlight
            $backgroundRequest = $this->getRequest()->getFiles('banner_deal_hightlight');
            if ($backgroundRequest) {
                if (isset($backgroundRequest['name'])) {
                    $fileName = $backgroundRequest['name'];
                } else {
                    $fileName = '';
                }
            } else {
                $fileName = '';
            }

            if ($backgroundRequest && strlen($fileName)) {
                /*
                 * Save image upload
                 */
                try {
                    $uploader = $this->_uploaderFactory->create(['fileId' => 'banner_deal_hightlight']);

                    $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png','svg']);

                    /** @var \Magento\Framework\Image\Adapter\AdapterInterface $imageAdapter */
                    $imageAdapter = $this->_adapterFactory->create();

                    //$uploader->addValidateCallback('banner_image', $imageAdapter, 'validateUploadFile');
                    $uploader->setAllowRenameFiles(true);
                    $uploader->setFilesDispersion(true);

                    /** @var \Magento\Framework\Filesystem\Directory\Read $mediaDirectory */
                    $mediaDirectory = $this->_objectManager->get('Magento\Framework\Filesystem')
                        ->getDirectoryRead(DirectoryList::MEDIA);
                    $result = $uploader->save(
                        $mediaDirectory->getAbsolutePath(\Magestore\Bannerslider\Model\Banner::BASE_MEDIA_PATH)
                    );
                    $data['banner_deal_hightlight'] = \Magestore\Bannerslider\Model\Banner::BASE_MEDIA_PATH.$result['file'];
                } catch (\Exception $e) {
                    if ($e->getCode() == 0) {
                        $this->messageManager->addError($e->getMessage());
                    }
                }
            } else {
                if (isset($data['banner_deal_hightlight']) && isset($data['banner_deal_hightlight']['value'])) {
                    if (isset($data['banner_deal_hightlight']['delete'])) {
                        $data['banner_deal_hightlight'] = null;
                        $data['delete_image'] = true;
                    } elseif (isset($data['banner_deal_hightlight']['value'])) {
                        $data['banner_deal_hightlight'] = $data['banner_deal_hightlight']['value'];
                    } else {
                        $data['banner_deal_hightlight'] = null;
                    }
                }
            }
            //end code prepare save image banner_deal_hightlight
             //start code prepare save image banner_onsite_icon
            $backgroundRequest = $this->getRequest()->getFiles('banner_onsite_icon');
            if ($backgroundRequest) {
                if (isset($backgroundRequest['name'])) {
                    $fileName = $backgroundRequest['name'];
                } else {
                    $fileName = '';
                }
            } else {
                $fileName = '';
            }

            if ($backgroundRequest && strlen($fileName)) {
                /*
                 * Save image upload
                 */
                try {
                    $uploader = $this->_uploaderFactory->create(['fileId' => 'banner_onsite_icon']);

                    $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png','svg']);

                    /** @var \Magento\Framework\Image\Adapter\AdapterInterface $imageAdapter */
                    $imageAdapter = $this->_adapterFactory->create();

                    //$uploader->addValidateCallback('banner_image', $imageAdapter, 'validateUploadFile');
                    $uploader->setAllowRenameFiles(true);
                    $uploader->setFilesDispersion(true);

                    /** @var \Magento\Framework\Filesystem\Directory\Read $mediaDirectory */
                    $mediaDirectory = $this->_objectManager->get('Magento\Framework\Filesystem')
                        ->getDirectoryRead(DirectoryList::MEDIA);
                    $result = $uploader->save(
                        $mediaDirectory->getAbsolutePath(\Magestore\Bannerslider\Model\Banner::BASE_MEDIA_PATH)
                    );
                    $data['banner_onsite_icon'] = \Magestore\Bannerslider\Model\Banner::BASE_MEDIA_PATH.$result['file'];
                } catch (\Exception $e) {
                    if ($e->getCode() == 0) {
                        $this->messageManager->addError($e->getMessage());
                    }
                }
            } else {
                if (isset($data['banner_onsite_icon']) && isset($data['banner_onsite_icon']['value'])) {
                    if (isset($data['banner_onsite_icon']['delete'])) {
                        $data['banner_onsite_icon'] = null;
                        $data['delete_image'] = true;
                    } elseif (isset($data['banner_onsite_icon']['value'])) {
                        $data['banner_onsite_icon'] = $data['banner_onsite_icon']['value'];
                    } else {
                        $data['banner_onsite_icon'] = null;
                    }
                }
            }
            //end code prepare save image banner_onsite_icon
             //start code prepare save image banner_onsite_hightlight
            $backgroundRequest = $this->getRequest()->getFiles('banner_onsite_hightlight');
            if ($backgroundRequest) {
                if (isset($backgroundRequest['name'])) {
                    $fileName = $backgroundRequest['name'];
                } else {
                    $fileName = '';
                }
            } else {
                $fileName = '';
            }

            if ($backgroundRequest && strlen($fileName)) {
                /*
                 * Save image upload
                 */
                try {
                    $uploader = $this->_uploaderFactory->create(['fileId' => 'banner_onsite_hightlight']);

                    $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png','svg']);

                    /** @var \Magento\Framework\Image\Adapter\AdapterInterface $imageAdapter */
                    $imageAdapter = $this->_adapterFactory->create();

                    //$uploader->addValidateCallback('banner_image', $imageAdapter, 'validateUploadFile');
                    $uploader->setAllowRenameFiles(true);
                    $uploader->setFilesDispersion(true);

                    /** @var \Magento\Framework\Filesystem\Directory\Read $mediaDirectory */
                    $mediaDirectory = $this->_objectManager->get('Magento\Framework\Filesystem')
                        ->getDirectoryRead(DirectoryList::MEDIA);
                    $result = $uploader->save(
                        $mediaDirectory->getAbsolutePath(\Magestore\Bannerslider\Model\Banner::BASE_MEDIA_PATH)
                    );
                    $data['banner_onsite_hightlight'] = \Magestore\Bannerslider\Model\Banner::BASE_MEDIA_PATH.$result['file'];
                } catch (\Exception $e) {
                    if ($e->getCode() == 0) {
                        $this->messageManager->addError($e->getMessage());
                    }
                }
            } else {
                if (isset($data['banner_onsite_hightlight']) && isset($data['banner_onsite_hightlight']['value'])) {
                    if (isset($data['banner_onsite_hightlight']['delete'])) {
                        $data['banner_onsite_hightlight'] = null;
                        $data['delete_image'] = true;
                    } elseif (isset($data['banner_onsite_hightlight']['value'])) {
                        $data['banner_onsite_hightlight'] = $data['banner_onsite_hightlight']['value'];
                    } else {
                        $data['banner_onsite_hightlight'] = null;
                    }
                }
            }
            //end code prepare save image banner_onsite_hightlight
             //start code prepare save image banner_warranty_icon
            $backgroundRequest = $this->getRequest()->getFiles('banner_warranty_icon');
            if ($backgroundRequest) {
                if (isset($backgroundRequest['name'])) {
                    $fileName = $backgroundRequest['name'];
                } else {
                    $fileName = '';
                }
            } else {
                $fileName = '';
            }

            if ($backgroundRequest && strlen($fileName)) {
                /*
                 * Save image upload
                 */
                try {
                    $uploader = $this->_uploaderFactory->create(['fileId' => 'banner_warranty_icon']);

                    $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png','svg']);

                    /** @var \Magento\Framework\Image\Adapter\AdapterInterface $imageAdapter */
                    $imageAdapter = $this->_adapterFactory->create();

                    //$uploader->addValidateCallback('banner_image', $imageAdapter, 'validateUploadFile');
                    $uploader->setAllowRenameFiles(true);
                    $uploader->setFilesDispersion(true);

                    /** @var \Magento\Framework\Filesystem\Directory\Read $mediaDirectory */
                    $mediaDirectory = $this->_objectManager->get('Magento\Framework\Filesystem')
                        ->getDirectoryRead(DirectoryList::MEDIA);
                    $result = $uploader->save(
                        $mediaDirectory->getAbsolutePath(\Magestore\Bannerslider\Model\Banner::BASE_MEDIA_PATH)
                    );
                    $data['banner_warranty_icon'] = \Magestore\Bannerslider\Model\Banner::BASE_MEDIA_PATH.$result['file'];
                } catch (\Exception $e) {
                    if ($e->getCode() == 0) {
                        $this->messageManager->addError($e->getMessage());
                    }
                }
            } else {
                if (isset($data['banner_warranty_icon']) && isset($data['banner_warranty_icon']['value'])) {
                    if (isset($data['banner_warranty_icon']['delete'])) {
                        $data['banner_warranty_icon'] = null;
                        $data['delete_image'] = true;
                    } elseif (isset($data['banner_warranty_icon']['value'])) {
                        $data['banner_warranty_icon'] = $data['banner_warranty_icon']['value'];
                    } else {
                        $data['banner_warranty_icon'] = null;
                    }
                }
            }
            //end code prepare save image banner_warranty_icon
             //start code prepare save image banner_warranty_hightlight
            $backgroundRequest = $this->getRequest()->getFiles('banner_warranty_hightlight');
            if ($backgroundRequest) {
                if (isset($backgroundRequest['name'])) {
                    $fileName = $backgroundRequest['name'];
                } else {
                    $fileName = '';
                }
            } else {
                $fileName = '';
            }

            if ($backgroundRequest && strlen($fileName)) {
                /*
                 * Save image upload
                 */
                try {
                    $uploader = $this->_uploaderFactory->create(['fileId' => 'banner_warranty_hightlight']);

                    $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png','svg']);

                    /** @var \Magento\Framework\Image\Adapter\AdapterInterface $imageAdapter */
                    $imageAdapter = $this->_adapterFactory->create();

                    //$uploader->addValidateCallback('banner_image', $imageAdapter, 'validateUploadFile');
                    $uploader->setAllowRenameFiles(true);
                    $uploader->setFilesDispersion(true);

                    /** @var \Magento\Framework\Filesystem\Directory\Read $mediaDirectory */
                    $mediaDirectory = $this->_objectManager->get('Magento\Framework\Filesystem')
                        ->getDirectoryRead(DirectoryList::MEDIA);
                    $result = $uploader->save(
                        $mediaDirectory->getAbsolutePath(\Magestore\Bannerslider\Model\Banner::BASE_MEDIA_PATH)
                    );
                    $data['banner_warranty_hightlight'] = \Magestore\Bannerslider\Model\Banner::BASE_MEDIA_PATH.$result['file'];
                } catch (\Exception $e) {
                    if ($e->getCode() == 0) {
                        $this->messageManager->addError($e->getMessage());
                    }
                }
            } else {
                if (isset($data['banner_warranty_hightlight']) && isset($data['banner_warranty_hightlight']['value'])) {
                    if (isset($data['banner_warranty_hightlight']['delete'])) {
                        $data['banner_warranty_hightlight'] = null;
                        $data['delete_image'] = true;
                    } elseif (isset($data['banner_warranty_hightlight']['value'])) {
                        $data['banner_warranty_hightlight'] = $data['banner_warranty_hightlight']['value'];
                    } else {
                        $data['banner_warranty_hightlight'] = null;
                    }
                }
            }
            //end code prepare save image banner_warranty_hightlight












            $localeDate = $this->_objectManager->get('Magento\Framework\Stdlib\DateTime\Timezone');
            $start_time = new \DateTime($data['start_time']);
            $start_time->format('Y-m-d H:i');
            $end_time =  new \DateTime($data['end_time']);
            $end_time->format('Y-m-d H:i');
            $data['start_time']= $start_time;
             $data['end_time'] = $end_time;
            $model->setData($data)
                ->setStoreViewId($storeViewId);

            try {
                $model->save();

                $this->messageManager->addSuccess(__('The banner has been saved.'));
                $this->_getSession()->setFormData(false);

                return $this->_getBackResultRedirect($resultRedirect, $model->getId());
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                $this->messageManager->addException($e, __('Something went wrong while saving the banner.'));
            }

            $this->_getSession()->setFormData($data);

            return $resultRedirect->setPath(
                '*/*/edit',
                [static::PARAM_CRUD_ID => $this->getRequest()->getParam(static::PARAM_CRUD_ID)]
            );
        }

        return $resultRedirect->setPath('*/*/');
    }
}
