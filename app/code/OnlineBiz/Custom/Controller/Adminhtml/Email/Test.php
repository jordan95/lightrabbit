<?php
    /**
     * Hello Rewrite Product View Controller
     *
     * @category    Webkul
     * @package     Webkul_Hello
     * @author      Webkul Software Private Limited
     *
     */
    namespace OnlineBiz\Custom\Controller\Adminhtml\Email;
    use Magento\Framework\Object;
    use Magento\Framework\Controller\ResultFactory;
    class Test extends \Ebizmarts\Mandrill\Controller\Adminhtml\Email\Test
    {
       
        public function execute()
        {
            $store_id = 1;
            $email      = $this->getRequest()->getParam('email');
            if(!empty($this->getRequest()->getParam("store_id"))){
                $store_id   = $this->getRequest()->getParam("store_id");
            }else{
                $store_id = 1;
            }
            
            //var_dump($store_id);die('aaa');
            $template   = "mandrill_test_template";
            $this->_transportBuilder->setTemplateIdentifier($template);
            $this->_transportBuilder->setFrom($this->_helper->getTestSender());
            $this->_transportBuilder->addTo($email);
            $this->_transportBuilder->setTemplateVars([]);
            $this->_transportBuilder->setTemplateOptions(['area' => \Magento\Framework\App\Area::AREA_FRONTEND, 'store' => $store_id]);
            $transport = $this->_transportBuilder->getTransport();
            $transport->sendMessage();

            /** @var \Magento\Framework\Controller\Result\Json $resultJson */
            $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $resultJson->setData(['error'=>0]);
            return $resultJson;
        }
    }