var config = {
    map: {
        "*": {
            'Magento_Catalog/js/validate-product': 'OnlineBiz_Custom/js/validate-product',
            catalogAddToCart: 'OnlineBiz_Custom/js/catalog-add-to-cart',
            'Magento_Catalog/js/catalog-add-to-cart': 'OnlineBiz_Custom/js/catalog-add-to-cart'
        }
    }
};