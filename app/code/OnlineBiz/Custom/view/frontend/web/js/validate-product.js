/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
define([
    'jquery',
    'mage/mage',
    'Magento_Catalog/product/view/validation',
    'catalogAddToCart'
], function($) {
    'use strict';

    $.widget('mage.productValidate', {
        options: {
            bindSubmit: false,
            radioCheckboxClosest: '.nested'
        },

        /**
         * Uses Magento's validation widget for the form object.
         * @private
         */
        _create: function() {
            var bindSubmit = this.options.bindSubmit;

            this.element.validation({
                radioCheckboxClosest: this.options.radioCheckboxClosest,

                /**
                 * Uses catalogAddToCart widget as submit handler.
                 * @param {Object} form
                 * @returns {Boolean}
                 */
                submitHandler: function(form) {
                    if($('.qty-product .quantity-input').length > 0){
                        var pass_qty = false;
                        var isNumber = true;
                        $('.qty-product .quantity-input').each(function(elem) {
                            if ($(this).val() > 0) {
                                pass_qty = true;
                            }
                            var test_num = /^[0-9.]+$/.test($(this).val());
                            if (!test_num) {
                                isNumber = false;
                            }
                        });
                        var canSubmit = true;
                        if (!pass_qty) {
                            $('.product-options .select-pro .validation-advice.required-msg').removeClass('hide');
                            canSubmit = false;
                        } else {
                            $('.product-options .select-pro .validation-advice.required-msg').addClass('hide');
                        }
                        if (!isNumber) {
                            $('.product-options .select-pro .validation-advice.numberic-msg').removeClass('hide');
                            canSubmit = false;
                        } else {
                            $('.product-options .select-pro .validation-advice.numberic-msg').addClass('hide');
                        }
                        if (!canSubmit)
                            return false;
                    }
                    var jqForm = $(form).catalogAddToCart({
                        bindSubmit: bindSubmit
                    });

                    jqForm.catalogAddToCart('submitForm', jqForm);
                    return false;
                }
            });
        }
    });

    return $.mage.productValidate;
});
