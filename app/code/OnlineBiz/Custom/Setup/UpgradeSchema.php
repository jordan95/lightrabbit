<?php
namespace OnlineBiz\Custom\Setup;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;
/**
 * @codeCoverageIgnore
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        if (version_compare($context->getVersion(), '1.0.4', '<')) {
            $setup->getConnection()->addColumn(
                $setup->getTable('eav_attribute_option_swatch'),
                'sku_option',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, // Or any other type
                    'nullable' => true, // Or false
                    'comment' => 'Some comment',
                    'after' => 'swatch_id'
                ]
            );
        }
        if (version_compare($context->getVersion(), '1.0.5', '<')) {
            if ($setup->getConnection()->isTableExists($setup->getTable('lof_gallery_album_image')) == true) {
                $setup->getConnection()->addColumn(
                    $setup->getTable('lof_gallery_album_image'),
                    'tag',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, // Or any other type
                        'nullable' => true, // Or false
                        'comment' => 'Tag of images',
                        'after' => 'album_id'
                    ]
                );
            }
           
        }
        if (version_compare($context->getVersion(), '1.0.6', '<')) {
            if ($setup->getConnection()->isTableExists($setup->getTable('mst_helpdesk_ticket')) == true) {
                $setup->getConnection()->addColumn(
                    $setup->getTable('mst_helpdesk_ticket'),
                    'telephone',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, // Or any other type
                        'nullable' => true, // Or false
                        'comment' => 'Telephone Customer',
                        'after' => 'customer_name'
                    ]
                );
            }
           
        }
        if (version_compare($context->getVersion(), '1.0.7', '<')) {
                $installer = $setup;
                $installer->startSetup();
                if (!$installer->tableExists('lof_gallery_album_image_tag')) {
                    $table = $installer->getConnection()->newTable(
                        $installer->getTable('lof_gallery_album_image_tag')
                    )
                        ->addColumn(
                            'id',
                            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                            null,
                            [
                                'identity' => true,
                                'nullable' => false,
                                'primary'  => true,
                                'unsigned' => true,
                            ],
                            'ID'
                        )
                        ->addColumn(
                            'image_id',
                            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                            null,
                            ['nullable => false'],
                            'Image Id'
                        )
                        ->addColumn(
                            'tag_name',
                            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            255,
                            [],
                            'Tag Name'
                        )
                        ->addColumn(
                            'album_id',
                            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                            null,
                            [],
                            'Album Id'
                        );
                    $installer->getConnection()->createTable($table);
                }
           
        }
        if (version_compare($context->getVersion(), '1.0.8', '<=')) {

            $setup->getConnection()->addColumn(
                $setup->getTable('magestore_bannerslider_banner'),
                'banner_tablet',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 255,
                    'nullable' => true,
                    'comment' => 'Banner Image Tablet'
                ]
            );
            $setup->getConnection()->addColumn(
                $setup->getTable('magestore_bannerslider_banner'),
                'banner_mobie',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 255,
                    'nullable' => true,
                    'comment' => 'Banner Image Mobie'
                ]
            );
            $setup->getConnection()->addColumn(
                $setup->getTable('magestore_bannerslider_banner'),
                'banner_indicator_icon',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 255,
                    'nullable' => true,
                    'comment' => 'Banner Indicator Icon'
                ]
            );
            $setup->getConnection()->addColumn(
                $setup->getTable('magestore_bannerslider_banner'),
                'banner_indicator_hightlight',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 255,
                    'nullable' => true,
                    'comment' => 'Banner Indicator Hightlight'
                ]
            );
            $setup->getConnection()->addColumn(
                $setup->getTable('magestore_bannerslider_banner'),
                'banner_indicator_title',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 255,
                    'nullable' => true,
                    'comment' => 'Banner Indicator Title'
                ]
            );
            $setup->getConnection()->addColumn(
                $setup->getTable('magestore_bannerslider_banner'),
                'banner_deal_icon',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 255,
                    'nullable' => true,
                    'comment' => 'Banner Deal Icon'
                ]
            );
            $setup->getConnection()->addColumn(
                $setup->getTable('magestore_bannerslider_banner'),
                'banner_deal_hightlight',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 255,
                    'nullable' => true,
                    'comment' => 'Banner Deal Hightlight'
                ]
            );
            $setup->getConnection()->addColumn(
                $setup->getTable('magestore_bannerslider_banner'),
                'banner_deal_title',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 255,
                    'nullable' => true,
                    'comment' => 'Banner Deal Title'
                ]
            );
            $setup->getConnection()->addColumn(
                $setup->getTable('magestore_bannerslider_banner'),
                'banner_onsite_icon',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 255,
                    'nullable' => true,
                    'comment' => 'Banner Onsite Icon'
                ]
            );
            $setup->getConnection()->addColumn(
                $setup->getTable('magestore_bannerslider_banner'),
                'banner_onsite_hightlight',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 255,
                    'nullable' => true,
                    'comment' => 'Banner Onsite Hightlight'
                ]
            );
            $setup->getConnection()->addColumn(
                $setup->getTable('magestore_bannerslider_banner'),
                'banner_onsite_title',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 255,
                    'nullable' => true,
                    'comment' => 'Banner Onsite Title'
                ]
            );

            $setup->getConnection()->addColumn(
                $setup->getTable('magestore_bannerslider_banner'),
                'banner_warranty_icon',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 255,
                    'nullable' => true,
                    'comment' => 'Banner Warranty Icon'
                ]
            );
            $setup->getConnection()->addColumn(
                $setup->getTable('magestore_bannerslider_banner'),
                'banner_warranty_hightlight',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 255,
                    'nullable' => true,
                    'comment' => 'Banner Warranty Hightlight'
                ]
            );
            $setup->getConnection()->addColumn(
                $setup->getTable('magestore_bannerslider_banner'),
                'banner_warranty_title',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 255,
                    'nullable' => true,
                    'comment' => 'Banner Warranty Title'
                ]
            );
        }

        $setup->endSetup();
    }
}