<?php
namespace OnlineBiz\Custom\Setup;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeData implements UpgradeDataInterface
{
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
            $resource   = $objectManager->get('Magento\Framework\App\ResourceConnection');
            $connection = $resource->getConnection();
            $tableName_entity  = $resource->getTableName('eav_entity_attribute'); //gives table name with prefix
             $tableName  = $resource->getTableName('eav_attribute');
            /** @var CustomerSetup $customerSetup */
            $sql = "select attribute_id from " . $tableName . " where attribute_code in('ebizmarts_reviews_cntr_total','ebizmarts_reviews_coupon_total')";
            $result = $connection->fetchAll($sql);
            $data = array_column($result, "attribute_id");
            foreach ($data as $key => $value) {
                $attribute_id = $value;
                if(isset($attribute_id) && is_string($attribute_id)){
                    $sql_insert = "INSERT IGNORE INTO " . $tableName_entity . "(entity_type_id, attribute_set_id, attribute_group_id,attribute_id,sort_order) VALUES(1,1,1,".$attribute_id.",0)";
                    $connection->query($sql_insert);
                }
            }
            
        }
        if (version_compare($context->getVersion(), '1.0.2', '<')) {
             $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
            $resource   = $objectManager->get('Magento\Framework\App\ResourceConnection');
            $connection = $resource->getConnection();
            $tableName_entity  = $resource->getTableName('catalog_product_entity_varchar'); //gives table name with prefix
            $sql_delete = "DELETE FROM " . $tableName_entity . " WHERE attribute_id = 443";
                    $connection->query($sql_delete);
        }
        if (version_compare($context->getVersion(), '1.0.3', '<')) {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
            $resource   = $objectManager->get('Magento\Framework\App\ResourceConnection');
            $connection = $resource->getConnection();
            $query_product = "SELECT * FROM eav_attribute_set JOIN catalog_product_entity on eav_attribute_set.attribute_set_id = catalog_product_entity.attribute_set_id WHERE   eav_attribute_set.attribute_set_name in('Migration_Accessories','Migration_Bulbs','Migration_Lamps','Migration_LED Strips','Migration_Panel Category','Migration_Tube Lights') and eav_attribute_set.entity_type_id = 4";
            $query_eav_attribute = "SELECT attribute_id,default_value FROM eav_attribute WHERE  attribute_code = 'dimmableconfig' and entity_type_id = 4";
            $get_product = $connection->fetchAll($query_product);
            $get_eav_attribute = $connection->fetchAll($query_eav_attribute);
            $attribute_id = '';
            $default_value = '';
             $values = "";
            
            foreach ($get_eav_attribute as $key => $value) {
                $attribute_id  = (int)$value['attribute_id'];
                $default_value = $value['default_value'];
            }
            foreach ($get_product as $k => $val) {
                $values .= "(".$attribute_id.",0,".$val['entity_id'].",".$default_value."),";
            }
            if($values!=""){
                $values = substr($values,0,-1);
                $sql_insert = "INSERT  INTO catalog_product_entity_int (attribute_id, store_id, entity_id,value) VALUES" .$values. " ON DUPLICATE KEY UPDATE value = VALUES(value)";
                $connection->query($sql_insert);
            }
            
        }
    }
}