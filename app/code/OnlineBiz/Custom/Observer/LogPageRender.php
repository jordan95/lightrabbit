<?php

namespace OnlineBiz\Custom\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Framework\Event\Observer as EventObserver;

class LogPageRender implements ObserverInterface
{
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $_localeDate;

    /**
     * LogPageRender constructor.
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        TimezoneInterface $localeDate
    )
    {
        $this->_storeManager = $storeManager;
        $this->_localeDate = $localeDate;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute(EventObserver $observer)
    {
        $event = $observer->getEvent();
        $page = $event->getPage();
        $request = $event->getRequest();
        $pageView = $request->getRouteName() . '_' . $request->getControllerName() . '_' . $request->getActionName();
        if ($pageView == 'cms_index_index'){
            $inRange = $this->_localeDate->isScopeDateInInterval(
                null,
                $page->getCustomThemeFrom(),
                $page->getCustomThemeTo()
            );
            $customLayoutUpdate = $page->getCustomLayoutUpdateXml();
            $layoutUpdate = $page->getLayoutUpdateXml();
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/' . $this->_storeManager->getStore()->getCode() . '_' . $pageView .'.log');
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);
            $logger->info('InRange:' . $inRange);
            $logger->info('CustomLayoutUpdate:' . $customLayoutUpdate);
            $logger->info('LayoutUpdate:' . $layoutUpdate);
        }
    }
}
