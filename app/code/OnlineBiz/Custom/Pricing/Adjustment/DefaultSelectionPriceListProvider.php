<?php

namespace OnlineBiz\Custom\Pricing\Adjustment;

use Magento\Bundle\Pricing\Adjustment\DefaultSelectionPriceListProvider as BaseDefaultSelectionPriceListProvider;

class DefaultSelectionPriceListProvider extends BaseDefaultSelectionPriceListProvider {

    /**
     * Add maximum price for multi selection option
     *
     * @param Product $bundleProduct
     * @param \Magento\Bundle\Model\ResourceModel\Selection\Collection $selectionsCollection
     * @param bool $useRegularPrice
     * @return void
     */
    private function addMaximumMultiSelectionPriceList(Product $bundleProduct, $selectionsCollection, $useRegularPrice) {
        $selectionsCollection->addPriceData();
        $selectionsCollection->distinct(true);
        foreach ($selectionsCollection as $selection) {
            $this->priceList[] = $this->selectionFactory->create(
                    $bundleProduct, $selection, $selection->getSelectionQty(), [
                'useRegularPrice' => $useRegularPrice,
                    ]
            );
        }
    }

}
