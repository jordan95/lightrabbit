<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace OnlineBiz\Custom\Pricing\Render;

use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\Pricing\Render\AbstractAdjustment;
use Magento\Framework\View\Element\Template;

/**
 * @method string getIdSuffix()
 * @method string getDisplayLabel()
 */
class TaxAdjustment extends \Magento\Weee\Pricing\Render\TaxAdjustment
{
    public function getDisplayAmountExclTaxCustom($exclude = null, $includeContainer = false)
    {
        //If exclude is not supplied, use the default
        if ($exclude === null) {
            $exclude = $this->getDefaultExclusions();
        }
        $symbol = $this->priceCurrency->getCurrencySymbol();
        $priceTxt = $this->formatCurrency($this->getRawAmount($exclude), $includeContainer);
        $pattern = "/(\\$symbol)(\d+)\.(\d+)/i";
        $replacement ='<span class="gavs_symbol">${1}</span><span class="gav_reprice">${2}</span><span class="after_comma">.${3}</span>';
        $price = preg_replace($pattern, $replacement, $priceTxt);
        return '<span class="price">' .$price. '</span>';
    }
}
