<?php

/**
 * OnlineBiz Software Solution
 * 
 * @project lightrabbit2
 * @version 0.0.1
 * @encoding UTF-8
 * @author Joe Vu<joe@onlinebizsoft.com>
 * @see http://onlinebizsoft.com
 * @copyright (c) 2017 , OnlineBiz Software Solution
 * 
 * Create at: Feb 8, 2018 2:57:56 PM
 */

namespace OnlineBiz\Custom\Pricing\Render;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\Pricing\Render\Amount as BaseAmount;

class Amount extends BaseAmount{
    
    public function formatCurrencyCamel($amount, $includeContainer = false, $precision = PriceCurrencyInterface::DEFAULT_PRECISION) {
        $symbol = $this->priceCurrency->getCurrencySymbol();
        $priceTxt = $this->formatCurrency($amount, $includeContainer, $precision);
        $pattern = "/(\\$symbol)(\d+)\.(\d+)/i";
        $replacement ='<span class="gavs_symbol">${1}</span><span class="gav_reprice">${2}</span><span class="after_comma">.${3}</span>';
        $price = preg_replace($pattern, $replacement, $priceTxt);
        return '<span class="price">' .$price. '</span>';
    }
}