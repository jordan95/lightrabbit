<?php

namespace OnlineBiz\Custom\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Helper\AbstractHelper;

/**
 * Cipher helper
 */
class Cipher extends  \Adfab\Gdpr\Helper\Cipher
{
    public function isActive() {
        if ( ! isset( $this->cipherActive ) ) {
            $this->cipherActive = false;
        }
        return $this->cipherActive;
    }


}
