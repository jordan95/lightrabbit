<?php
/**
 * Ebizmarts_Mandrill Magento JS component
 *
 * @category    Ebizmarts
 * @package     Ebizmarts_Mandrill
 * @author      Ebizmarts Team <info@ebizmarts.com>
 * @copyright   Ebizmarts (http://ebizmarts.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace OnlineBiz\Custom\Helper;

class Data extends \Ebizmarts\Mandrill\Helper\Data
{

    public function getTestSender()
    {
        $scopeId =null;
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();  
        $request = $objectManager->get('Magento\Framework\App\Request\Http');  
        $scopeId = (int) $request->getParam("store_id");
        $result = [];
        $sender = $this->scopeConfig->getValue(
            'checkout/payment_failed/identity',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        
        $result['name'] = $this->scopeConfig->getValue(
            'trans_email/ident_' . $sender . '/name',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $scopeId
        );
        $result['email'] = $this->scopeConfig->getValue(
            'trans_email/ident_' . $sender . '/email',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $scopeId
        );
        return $result;
    }
}
    