<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace OnlineBiz\Custom\Helper;
use Magento\Catalog\Helper\Image;
use Magento\Framework\App\Area;
use Magento\Framework\App\Filesystem\DirectoryList;
class Media extends \Magento\Swatches\Helper\Media
{
   private $imageConfig;
   public function getImageConfig()
    {
        if (!$this->imageConfig) {
            $this->imageConfig = $this->viewConfig->getViewConfig()->getMediaEntities(
                'Magento_Catalog',
                Image::MEDIA_TYPE_CONFIG_NODE
             );
         }
         return $this->imageConfig;
    }
}
