<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-helpdesk
 * @version   1.1.63
 * @copyright Copyright (C) 2018 Mirasvit (https://mirasvit.com/)
 */



namespace OnlineBiz\Custom\Helper;

use Mirasvit\Helpdesk\Model\Config as Config;

/**
 * Class Process.
 *
 * @SuppressWarnings(PHPMD.TooManyFields)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Process extends \Mirasvit\Helpdesk\Helper\Process
{

    public function createFromPost($post, $channel)
    {
        $ticket = $this->ticketFactory->create();
        // если кастомер не был авторизирован, то ищем его
        $customer = $this->helpdeskCustomer->getCustomerByPost($post);

        $ticket->setCustomerId($customer->getId())
            ->setCustomerEmail($customer->getEmail())
            ->setCustomerName($customer->getName())
            ->setQuoteAddressId($customer->getQuoteAddressId())
            ->setCode($this->helpdeskString->generateTicketCode())
            ->setSubject($post['subject']);
            if(isset($post['telephone'])){
                $ticket->setSubject($post['subject'])->setTelephone($post['telephone']);
            }  
            //->setDescription($this->getEnviromentDescription());

        if (isset($post['priority_id'])) {
            $ticket->setPriorityId((int) $post['priority_id']);
        }
        if (isset($post['department_id'])) {
            $ticket->setDepartmentId((int) $post['department_id']);
        } else {
            $ticket->setDepartmentId($this->getConfig()->getContactFormDefaultDepartment());
        }
        if (isset($post['order_id'])) {
            $ticket->setOrderId((int) $post['order_id']);
        }
        $ticket->setStoreId($this->storeManager->getStore()->getId());
        $ticket->setChannel($channel);
        if ($channel == Config::CHANNEL_FEEDBACK_TAB) {
            $url = $this->customerSession->getFeedbackUrl();
            $ticket->setChannelData(['url' => $url]);
        }

        $this->helpdeskField->processPost($post, $ticket);
        $ticket->save();
        $body = $post['message'];
        // $body = $this->helpdeskString->parseBody($post['message'], Config::FORMAT_PLAIN);
        $ticket->addMessage(
            $body,
            $customer,
            false,
            Config::CUSTOMER,
            Config::MESSAGE_PUBLIC,
            false,
            Config::FORMAT_PLAIN
        );
        $this->helpdeskHistory->changeTicket(
            $ticket, $ticket->getState(), $ticket->getState(), Config::CUSTOMER, ['customer' => $customer]
        );

        return $ticket;
    }

 
}
