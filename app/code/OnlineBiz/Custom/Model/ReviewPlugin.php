<?php

namespace OnlineBiz\Custom\Model;

use Faker\Factory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Review\Model\Review;

class ReviewPlugin extends \Adfab\Gdpr\Model\ReviewPlugin{

    protected function isActive() {
        if ( ! isset($this->active) ) {
            $this->active = false;
        }
        return $this->active;
    }
}