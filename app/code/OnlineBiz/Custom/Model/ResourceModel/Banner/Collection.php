<?php

namespace OnlineBiz\Custom\Model\ResourceModel\Banner;

/**
 * Banner Collection
 * @category Magestore
 * @package  OnlineBiz\Custom
 * @module   Onlinebiz
 * @author   Jordan Developer
 */
class Collection extends \Magestore\Bannerslider\Model\ResourceModel\Banner\Collection
{
    public function getBannerCondition($sliderId)
    {
        $dateTimeNow = $this->_stdTimezone->date()->format('Y-m-d H:i:s');
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('magestore_bannerslider_banner');   
        $sql = $connection->select()->from($tableName)->where('slider_id = ?', $sliderId)->where('status = ?', \Magestore\Bannerslider\Model\Status::STATUS_ENABLED)->where('start_time <= ?', $dateTimeNow)->where('end_time>= ?', $dateTimeNow);
        $result = $connection->fetchAll($sql); 
        return $result;
    }
}
