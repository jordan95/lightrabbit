<?php

namespace OnlineBiz\Custom\Model\ResourceModel;

use Magento\Framework\Model\AbstractModel;
use Lof\Gallery\Model\Config;

class Album extends \Lof\Gallery\Model\ResourceModel\Album
{

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var \Lof\Gallery\Model\ResourceModel\Tag\CollectionFactory
     */
    private $tagCollectionFactory;

    /**
     * @var \Lof\Gallery\Model\Tag
     */
    private $galleryTag;


    /**
     * Album constructor.
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param Tag\CollectionFactory $tagCollectionFactory
     * @param \Lof\Gallery\Model\Tag $galleryTag
     * @param null $connectionName
     */
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\Module\Manager $moduleManager,
        \Lof\Gallery\Model\ResourceModel\Tag\CollectionFactory $tagCollectionFactory,
        \Lof\Gallery\Model\Tag $galleryTag,
        $connectionName = null
    ) {
        parent::__construct($context,$objectManager,$moduleManager,$tagCollectionFactory,$galleryTag,$connectionName);
        $this->objectManager        = $objectManager;
        $this->tagCollectionFactory = $tagCollectionFactory;
        $this->galleryTag           = $galleryTag;
        $this->_moduleManager       = $moduleManager;

    }//end __construct()



    /**
     * Perform operations after object save
     *
     * @param  \Magento\Framework\Model\AbstractModel $object
     * @return $this
     */
    protected function _afterSave(AbstractModel $object)
    {
        if(!$object->getData("isfrontend")) {
            $postData = $object->getData();

            if (isset($postData['categories'])) {
                $categories = $postData['categories'];
                $categories = explode(",", str_replace(['[', ']', '"'], ['', '', ''], $categories));

                $table = $this->getTable(Config::TABLE_GALLERY_ALBUM_CATEGORY);
                $where = ['album_id = ?' => (int) $object->getId()];
                $this->getConnection()->delete($table, $where);
                $data = [];
                foreach ($categories as $catId) {
                    if ($catId) {
                        $data[] = [
                                   'album_id'  => (int) $object->getId(),
                                   'entity_id' => $catId,
                                   'position'  => 0,
                                  ];
                    }
                }

                if (!empty($data)) {
                    $this->getConnection()->insertMultiple($table, $data);
                }
            }//end if

            // Related Products
            if (isset($postData['related_products'])) {
                $productsRelated = $postData['related_products'];
                $table           = $this->getTable(Config::TABLE_GALLERY_ALBUM_PRODUCT);
                $where           = ['album_id = ?' => (int) $object->getId()];
                $this->getConnection()->delete($table, $where);
                $data = [];
                if(!empty($productsRelated)) {
                    foreach ($productsRelated as $k => $product) {
                        $data[] = [
                                   'album_id'  => $object->getId(),
                                   'entity_id' => $k,
                                   'position'  => $product['productposition'],
                                  ];
                    }

                    if (!empty($data)) {
                        $this->getConnection()->insertMultiple($table, $data);
                    }
                }
            }

            // Related Posts
            if ($this->_moduleManager->isEnabled('Ves_Blog') && isset($postData['related_posts'])) {
                $postsRelated = $postData['related_posts'];
                $table        = $this->getTable(Config::TABLE_GALLERY_ALBUM_POST);
                $where        = ['album_id = ?' => (int) $object->getId()];
                $this->getConnection()->delete($table, $where);
                $data = [];
                if(!empty($postsRelated)) {
                    foreach ($postsRelated as $k => $post) {
                        $data[] = [
                                   'album_id' => $object->getId(),
                                   'post_id'  => $k,
                                   'position' => $post['postposition'],
                                  ];
                    }

                    if (!empty($data)) {
                        $this->getConnection()->insertMultiple($table, $data);
                    }
                }
            }

            // Related Stores
            if ($this->_moduleManager->isEnabled('Lof_StoreLocator') && isset($postData['related_stores'])) {
                $storesRelated = $postData['related_stores'];
                $table         = $this->getTable(Config::TABLE_GALLERY_ALBUM_STORE);
                $where         = ['album_id = ?' => $object->getId()];
                $this->getConnection()->delete($table, $where);
                $data = [];
                if(!empty($storesRelated)) {
                    foreach ($storesRelated as $k => $store) {
                        $data[] = [
                                   'album_id'        => (int) $object->getId(),
                                   'storelocator_id' => $k,
                                   'position'        => $store['storeposition'],
                                  ];
                    }

                    $this->getConnection()->insertMultiple($table, $data);
                }
            }

            if (isset($postData['tags'])) {
                $tags  = $postData['tags'];
                $tags  = explode(",", $tags);
                $table = $this->getTable(Config::TABLE_GALLERY_ALBUM_TAG);
                $where = ['album_id = ?' => (int) $object->getId()];
                $this->getConnection()->delete($table, $where);
                if(!empty($tags)) {
                    $data = [];
                    foreach ($tags as $k => $_tag) {
                        if ($_tag) {
                            $alias = strtolower(str_replace(["_", " "], "-", trim($_tag)));

                            $tag = $this->tagCollectionFactory->create()->addFieldToFilter('alias', $alias)->getFirstItem();
                            if ($alias) {
                                if (!$tag->getTagId()) {
                                    $tagData = [
                                                'alias'     => $alias,
                                                'name'      => $alias,
                                                'is_active' => 1,
                                               ];
                                    $tag     = $this->objectManager->create('Lof\Gallery\Model\Tag');
                                    $tag->setData($tagData)->save();
                                }

                                $data[] = [
                                           'album_id' => $object->getId(),
                                           'tag_id'   => $tag->getTagId(),
                                          ];
                            }
                        }//end if
                    }//end foreach

                    if (!empty($data)) {
                        $this->getConnection()->insertMultiple($table, $data);
                    }
                }//end if
            }//end if

            if (isset($postData['images'])) {
                $images = $postData['images'];
                $table  = $this->getTable(Config::TABLE_GALLERY_ALBUM_IMAGE);
                $where  = ['album_id = ?' => (int) $object->getId()];
                $this->getConnection()->delete($table, $where);
                $data = [];
                foreach ($images as $k => $image) {                  
                   $image['tag'] =  preg_replace('/[^a-zA-Z0-9_ - \,]/s','',$image['tag']);
                    $image['tag']= preg_replace('/\s+/', '', $image['tag']);
                    $data[] = [
                               'album_id' => $object->getId(),
                               'tag' => $image['tag'],
                               'key'      => $k,
                               'params'   => serialize($image),
                              ];
                }

                if (!empty($data)) {
                    $this->getConnection()->insertMultiple($table, $data);
                }
            }

            if ($stores = $object->getStores()) {
                $table = $this->getTable(Config::TABLE_GALLERY_ALBUM_CORESTORE);
                $where = ['album_id = ?' => (int) $object->getId()];
                $this->getConnection()->delete($table, $where);
                if ($stores) {
                    $data = [];
                    foreach ($stores as $storeId) {
                        $data[] = [
                                   'album_id' => (int) $object->getId(),
                                   'store_id'  => (int) $storeId,
                                  ];
                    }

                    $this->getConnection()->insertMultiple($table, $data);
                }
            }
        } else {
        }//end if
        return $this;

    }//end _afterSave()


}//end class
