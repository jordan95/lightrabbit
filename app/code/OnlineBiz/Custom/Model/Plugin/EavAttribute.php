<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace OnlineBiz\Custom\Model\Plugin;

use Magento\Catalog\Model\ResourceModel\Eav\Attribute;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Swatches\Model\Swatch;

/**
 * Plugin model for Catalog Resource Attribute
 */
class EavAttribute extends \Magento\Swatches\Model\Plugin\EavAttribute
{
    protected function processVisualSwatch(Attribute $attribute)
    {
         $sku_option ='';
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();  
        $request = $objectManager->get('Magento\Framework\App\Request\Http');  
        $param_sku = $request->getParam('sku_option');
        $swatchArray = $attribute->getData('swatch/value');
        if (isset($swatchArray) && is_array($swatchArray)) {
            foreach ($swatchArray as $optionId => $value) {
                if(isset($param_sku[$optionId])){
                    $sku_option = $param_sku[$optionId];
                }
                $optionId = $this->getAttributeOptionId($optionId);
                $isOptionForDelete = $this->isOptionForDelete($attribute, $optionId);
                if ($optionId === null || $isOptionForDelete) {
                    //option was deleted by button with basket
                    continue;
                }
                $swatch = $this->loadSwatchIfExists($optionId, self::DEFAULT_STORE_ID);

                $swatchType = self::determineSwatchTypeCustom($value);

                $this->saveSwatchDataCustom($swatch, $optionId, self::DEFAULT_STORE_ID, $swatchType, $value,$sku_option);
                $this->isSwatchExists = null;
            }
        }
    }
    protected function determineSwatchTypeCustom($value)
    {
        $swatchType = Swatch::SWATCH_TYPE_EMPTY;
        if (!empty($value) && $value[0] == '#') {
            $swatchType = Swatch::SWATCH_TYPE_VISUAL_COLOR;
        } elseif (!empty($value) && $value[0] == '/') {
            $swatchType = Swatch::SWATCH_TYPE_VISUAL_IMAGE;
        }
        return $swatchType;
    }
    protected function saveSwatchDataCustom($swatch, $optionId, $storeId, $type, $value,$sku_option)
    {
        if ($this->isSwatchExists) {
            $swatch->setData('type', $type);
            $swatch->setData('value', $value);
            $swatch->setData('sku_option', $sku_option);
        } else {
            $swatch->setData('option_id', $optionId);
            $swatch->setData('store_id', $storeId);
            $swatch->setData('type', $type);
            $swatch->setData('sku_option', $sku_option);
            $swatch->setData('value', $value);
        }
        $swatch->save();
    }
}
