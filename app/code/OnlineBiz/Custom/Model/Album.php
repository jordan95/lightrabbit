<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category  Landofcoder
 * @package   Lof_Gallery
 * @copyright Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license   http://www.landofcoder.com/LICENSE-1.0.html
 */
namespace OnlineBiz\Custom\Model;

class Album extends \Lof\Gallery\Model\Album
{
    public function getListTags()
    {
        $connection   = $this->resource->getConnection();
                $select       = $connection->select()->from(
                                    ['ce' => 'lof_gallery_album_image_tag'],
                                    ['tag_name', 'count(tag_name) as total']
                                )
                    ->group('tag_name')->where(
                        'album_id = ' . $this->getAlbumId()
                    );

                $images = $connection->fetchAll($select);
                $newImages    = [];
                $code_tag = '';
                $total ='';
                    foreach ($images as $k => $image) {
                       
                        if($image['tag_name'] !=''){
                            $code_tag = str_replace(' ','-', $image['tag_name']);
                            $newImages[$code_tag]['label'] = $image['tag_name'];
                            $newImages[$code_tag]['total']=$image['total'];
                        }
                    }
                 
                return $newImages;

    }//end getImages()
    public function updateImageTag()
    {
            $album_id = $this->getAlbumId();
            $connection   = $this->resource->getConnection();
            $select       = $connection->select()
                ->from($this->resource->getTable('lof_gallery_album_image'))
                ->where(
                    'album_id = ' . $album_id
                );
            $images = $connection->fetchAll($select);
            $insertData =[];
            $getTag  = array();
            $where       = ['album_id = ?' => $album_id];
            $this->resource->getConnection()->delete($this->resource->getTable('lof_gallery_album_image_tag'), $where);//delete old record with condition special album_id
            foreach($images as $key =>$value){
                if($value['tag']!='' && is_string($value['tag'])){
                   $getTag =  explode(",",$value['tag']);
                   if(!empty($getTag)){
                        foreach($getTag as $k=>$val){
                            $insertData=[
                                'image_id'=>(int)  $value['image_id'],
                                'tag_name'=>  $val,
                                'album_id'=>(int) $value['album_id']
                            ];
                             $connection->insertMultiple($this->resource->getTable('lof_gallery_album_image_tag'), $insertData); // insert tag_name with special album_id.
                        }
                   }
                   
                }
                
            }

    }//end updateImageTag()


}//end class
