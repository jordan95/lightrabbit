<?php
/**
 Custom Code by Jordan
 feature: add two options
 **/

namespace OnlineBiz\Custom\Model\Config\Source;

class WidgetSkin extends \Lof\Gallery\Model\Config\Source\WidgetSkin
{


    public function toOptionArray()
    {
        $options   = [];
        $options[] = [
                      'label' => 'Orange',
                      'value' => 'orange',
                     ];
        $options[] = [
                      'label' => 'Aqua',
                      'value' => 'aqua',
                     ];
        $options[] = [
                      'label' => 'Coral',
                      'value' => 'coral',
                     ];
        $options[] = [
                      'label' => 'Green',
                      'value' => 'green',
                     ];
        $options[] = [
                      'label' => 'Purple',
                      'value' => 'purple',
                     ];
        $options[] = [
                      'label' => 'Indigo',
                      'value' => 'indigo',
                     ];
        $options[] = [
                      'label' => 'Salmon',
                      'value' => 'salmon',
                     ];
        $options[] = [
                      'label' => 'Teal',
                      'value' => 'teal',
                     ];
        $options[] = [
                      'label' => 'Turquoise',
                      'value' => 'turquoise',
                     ];
        $options[] = [
                      'label' => 'Violet',
                      'value' => 'violet',
                     ];
        $options[] = [
                      'label' => 'Yellow Green',
                      'value' => 'yellowgreen',
                     ];
        $options[] = [
                      'label' => 'Tan',
                      'value' => 'tan',
                     ];
        $options[] = [
                      'label' => 'Slate Blue',
                      'value' => 'slateblue',
                     ];
        $options[] = [
                      'label' => 'Sky Blue',
                      'value' => 'skyblue',
                     ];
        $options[] = [
                      'label' => 'Sea Green',
                      'value' => 'seagreen',
                     ];
        $options[] = [
                      'label' => 'RebeccaPurple',
                      'value' => 'rebeccapurple',
                     ];
        $options[] = [
                      'label' => 'Cornflower Blue',
                      'value' => 'cornflowerblue ',
                     ];
        $options[] = [
                      'label' => 'LightSalmon',
                      'value' => 'lightsalmon',
                     ];
        $options[] = [
                      'label' => 'Orchid',
                      'value' => 'orchid',
                     ];
        $options[] = [
                      'label' => 'Medium VioletRed',
                      'value' => 'mediumvioletRed ',
                     ];
        $options[] = [
                      'label' => 'Orange Red',
                      'value' => 'orangered',
                     ];
        $options[] = [
                      'label' => 'Cornsilk',
                      'value' => 'cornsilk ',
                     ];
        $options[] = [
                      'label' => 'Blue',
                      'value' => 'blue',
                     ];
        $options[] = [
                      'label' => 'Chocolate',
                      'value' => 'chocolate',
                     ];
        $options[] = [
                      'label' => 'Lime',
                      'value' => 'lime',
                     ];
        $options[] = [
                      'label' => 'LightCoral',
                      'value' => 'lightcoral',
                     ];
        $options[] = [
                      'label' => 'IndianRed',
                      'value' => 'indianred',
                     ];
        $options[] = [
                      'label' => 'Hot Pink',
                      'value' => 'hotpink',
                     ];
        $options[] = [
                      'label' => 'Dark Orange',
                      'value' => 'darkorange',
                     ];
        $options[] = [
                      'label' => 'Aquamarine',
                      'value' => 'aquamarine',
                     ];
        $options[] = [
                      'label' => 'Black',
                      'value' => 'black',
                     ];
        $options[] = [
                      'label' => 'Grey Black',
                      'value' => 'greyblack',
                     ];
        return $options;

    }//end toOptionArray()


}//end class
