<?php

namespace OnlineBiz\Custom\Plugin\Framework\View\Model\Layout;

/**
 * Layout merge model plugin
 */
class Merge
{
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * Merge constructor.
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager
    )
    {
        $this->_storeManager = $storeManager;
    }

    /**
     * /**
     * Around as simple xml
     *
     * @param \Magento\Framework\View\Model\Layout\Merge $subject
     * @param \Closure $proceed
     *
     * @return \SimpleXMLElement
     */
    public function aroundAsSimplexml(
        \Magento\Framework\View\Model\Layout\Merge $subject,
        \Closure $proceed
    )
    {
        $asString = implode('', $subject->asArray());
        $updates = trim($asString);
        $updates = '<?xml version="1.0"?>'
            . '<layout xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">'
            . $updates
            . '</layout>';
        return $this->_loadXmlString($updates);
    }

    /**
     * Return object representation of XML string
     *
     * @param string $xmlString
     * @return \SimpleXMLElement
     */
    protected function _loadXmlString($xmlString)
    {
        return simplexml_load_string($xmlString, \Magento\Framework\View\Layout\Element::class);
    }

}