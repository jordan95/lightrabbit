<?php
/**
 *Customer Also Bought Extension
 *
*/
namespace OnlineBiz\Custom\Block\Catalog\Product\View\Alsobought;
class ListAlso extends \Magento\Catalog\Block\Product\AbstractProduct
{
	protected $_thisProduct;
	protected $_eavAttribute;
	protected $_storeManager;
	protected $_resource;
	protected $_productCollectionFactory;
	protected $_collection;
	protected $_productImageHelper;

	public function __construct(
	 	\Magento\Catalog\Block\Product\Context $context,
	 	array $data = [],
	 	\Magento\Eav\Model\ResourceModel\Entity\Attribute $eavAttribute,
	 	\Magento\Store\Model\StoreManagerInterface $storeManager,
	 	\Magento\Framework\App\ResourceConnection $resourceConnection,
	 	\Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
	 	\Magento\Catalog\Helper\Image $productImageHelper
	){
        $this->_eavAttribute = $eavAttribute;
        $this->_storeManager = $storeManager; 
        $this->_resource = $resourceConnection;
        $this->_productCollectionFactory = $productCollectionFactory;   
        $this->_productImageHelper = $productImageHelper;
        parent::__construct($context, $data);
    }
  	public function getListItems() {

		$this->_thisProduct = $this->getProduct();
	    $productId = $this->_thisProduct->getId();
		$limit =6;
		$resource = $this->_resource;
        $db = $resource->getConnection();
		$visibilityAttId = $this->_eavAttribute->getIdByCode('catalog_product', 'visibility');
		$storeId = $this->getStoreId();
		$orderItemTable = $resource->getTableName('sales_order_item');
		$catalogEntityIntTable = $resource->getTableName('catalog_product_entity_int');
		$catalogInventoryTable = $resource->getTableName('cataloginventory_stock_item');
		$select = $db->select()
            ->from(array('oi' => $orderItemTable), 'order_id')
			->where('oi.product_id=?', $productId)
			->group('order_id');
		$result = $db->fetchAll($select);
		if(count($result)) {
			$orderIds = implode(',', array_map('current', $result));
			$select = $db->select()
				->from(array('oi' => $orderItemTable), array('product_id', 'count(*) as count'))
				->join(array('at_visibility_default' => $catalogEntityIntTable), 'at_visibility_default.entity_id=oi.product_id AND at_visibility_default.attribute_id ='.$visibilityAttId.' AND at_visibility_default.store_id = 0')
				->joinLeft(array('at_visibility' => $catalogEntityIntTable), 'at_visibility.entity_id=oi.product_id AND at_visibility_default.attribute_id ='.$visibilityAttId.' AND at_visibility_default.store_id = '.$storeId.'')
				->join(array('inv' => $catalogInventoryTable), 'inv.product_id = oi.product_id AND inv.is_in_stock =1', '')
				->where('oi.order_id in('.$orderIds.')')
				->where('oi.product_id > 0')
				->where('oi.parent_item_id IS NULL')
				->where('oi.product_id <> ?', $productId)
				->where('oi.sku <> ?', "LR/KEYRING")	
				->where('IF(at_visibility.value_id > 0, at_visibility.value, at_visibility_default.value) = ?', 4)
				->group('oi.product_id')
				->order('count DESC')
				->limit($limit);
			$result = $db->fetchAll($select);
			if(count($result)) {
				$productIds = implode(',', array_map('current', $result));
				if (is_null($this->_collection) && $productIds) {
					$this->_collection = $this->getProductCollection($productIds);
				}
			}
		}
		return $this->_collection;
	}
	public function getStoreId()
    {	
        return $this->_storeManager->getStore()->getId();
    }
    public function getProductCollection($productIds)
    {
        $collection = $this->_productCollectionFactory->create();
        $collection->addAttributeToSelect('*')
						->addAttributeToFilter('entity_id', array('in'=>explode(',', $productIds)))
						->addUrlRewrite()
						->addStoreFilter();
					$collection->load();
		
		return $collection;
    }
    public function resizeImage($product, $image_type, $width)
    {
        $resizedImage = $this->_productImageHelper->init($product, $image_type)->constrainOnly(FALSE)->keepAspectRatio(TRUE)->keepFrame(FALSE)->resize($width);
        return $resizedImage;
    }

}