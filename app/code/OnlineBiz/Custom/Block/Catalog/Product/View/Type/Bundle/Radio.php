<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

// @codingStandardsIgnoreFile

namespace OnlineBiz\Custom\Block\Catalog\Product\View\Type\Bundle;

/**
 * Bundle option renderer
 * @api
 * @since 100.0.2
 */
class Radio extends \Magento\Bundle\Block\Catalog\Product\View\Type\Bundle\Option\Radio
{
    public function getCustomSelectionTitle($selection, $includeContainer = true)
    {
        $priceTitle = '<span class="product-name">' . $this->escapeHtml($selection->getName()) . '</span>';
        return $priceTitle;
    }  
}
