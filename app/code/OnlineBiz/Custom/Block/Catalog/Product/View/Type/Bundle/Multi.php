<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

// @codingStandardsIgnoreFile

namespace OnlineBiz\Custom\Block\Catalog\Product\View\Type\Bundle;

/**
 * Bundle option renderer
 * @api
 * @since 100.0.2
 */
class Multi extends \Magento\Bundle\Block\Catalog\Product\View\Type\Bundle\Option\Multi
{
     public function renderCustomPriceString($selection, $includeContainer = true)
    {
        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();        
         
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $currency = $objectManager->get('\Magento\Directory\Model\Currency');
        $symbol = $currency->getCurrencySymbol();
        $priceTxt = $this->renderPriceString($selection, $includeContainer = true);
        $pattern = "/(\\$symbol)(\d+)\.(\d+)/i";
        $replacement ='<span class="gavs_symbol">${1}</span><span class="gav_reprice">${2}</span><span class="after_comma">.${3}</span>';
        $price = preg_replace($pattern, $replacement, $priceTxt);
        return '<span class="price">' .$price. '</span>';
    }
    public function getCustomSelectionTitle($selection, $includeContainer = true)
    {
        $priceTitle = '<span class="product-name">' . $this->escapeHtml($selection->getName()) . '</span>';
        return $priceTitle;
    }

}
