<?php 

namespace OnlineBiz\Custom\Block\Product\View\Type;
class Configurable extends \Cart2Quote\Quotation\Block\Product\Renderer\Configurable
{

    public function getProductAvailable(){
        $results = array();
        if ( !$this->hasAllowProducts() ) 
        {
            
          $products = $this->getProduct()
          ->getTypeInstance(true)
          ->getUsedProductCollection($this->getProduct())
          ->addAttributeToSelect('*')
          ->addAttributeToFilter('status', 1)
          ->addFilterByRequiredOptions();
        
          foreach ($products as $product) {
            $options = $product->getData();
            $options['colortemp'] = $product->getAttributeText('lightcolour');
            foreach ($this->getAllowAttributes() as $attribute)
            {
              $productAttribute   = $attribute->getProductAttribute();
              $productAttributeId = $productAttribute->getId();
              $attributeValue     = $product->getData($productAttribute->getAttributeCode());
              if ( !isset($options['attribute']) ) { $options['attribute'] = array(); }
              if ( !isset($options['attribute'][$productAttributeId]) ) { $options['attribute'][$productAttributeId] = $attributeValue; }
            }
            $results[] = $options;
          }
        }
        return $results;
      }


}