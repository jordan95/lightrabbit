<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace OnlineBiz\Custom\Block\Adminhtml\Attribute\Edit\Options;

/**
 * Block Class for Visual Swatch
 *
 * @api
 * @since 100.0.2
 */
class Visual extends \Magento\Swatches\Block\Adminhtml\Attribute\Edit\Options\Visual
{
	 /**
     * Retrieve attribute option values for given store id
     *
     * @param int $storeId
     * @return array
     */
    
    public function getJsonConfig()
    {
        $values = [];
        foreach ($this->getOptionValues() as $value) {
            $sku_option = $this->getSwatchById($value['id']);
            $value['sku_option'] =$sku_option;
            $values[] = $value->getData();
        }
        $data = [
            'attributesData' => $values,
            'uploadActionUrl' => $this->getUrl('swatches/iframe/show'),
            'isSortable' => (int)(!$this->getReadOnly() && !$this->canManageOptionDefaultOnly()),
            'isReadOnly' => (int)$this->getReadOnly()
        ];
       
        return json_encode($data);
    }
    protected function getSwatchById($option_id){
        $data ='';
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('eav_attribute_option_swatch');
        $sql = "Select sku_option FROM " . $tableName. " WHERE option_id = ".$option_id;
        $result = $connection->fetchAll($sql);
        foreach($result as $key =>$value){
            $data = $value['sku_option'];
        }
        return $data;
    }
}
