<?php

/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Bannerslider
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

namespace OnlineBiz\Custom\Block\Adminhtml\Banner\Edit\Tab;

use Magestore\Bannerslider\Model\Status;

/**
 * Banner Edit tab.
 * @category Magestore
 * @package  Magestore_Bannerslider
 * @module   Bannerslider
 * @author   Magestore Developer
 */
class Banner extends \Magestore\Bannerslider\Block\Adminhtml\Banner\Edit\Tab\Banner
{
 
    protected function _prepareForm()
    {
        parent::_prepareForm();
        $bannerAttributes = $this->_banner->getStoreAttributes();
        $bannerAttributesInStores = ['store_id' => ''];

        foreach ($bannerAttributes as $bannerAttribute) {
            $bannerAttributesInStores[$bannerAttribute.'_in_store'] = '';
        }

        $dataObj = $this->_objectFactory->create(
            ['data' => $bannerAttributesInStores]
        );
        $model = $this->_coreRegistry->registry('banner');

        if ($sliderId = $this->getRequest()->getParam('current_slider_id')) {
            $model->setSliderId($sliderId);
        }

        $dataObj->addData($model->getData());

        $storeViewId = $this->getRequest()->getParam('store');

        $attributesInStore = $this->_valueCollectionFactory
            ->create()
            ->addFieldToFilter('banner_id', $model->getId())
            ->addFieldToFilter('store_id', $storeViewId)
            ->getColumnValues('attribute_code');

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();

        $form->setHtmlIdPrefix($this->_banner->getFormFieldHtmlIdPrefix());

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Banner Information')]);

        if ($model->getId()) {
            $fieldset->addField('banner_id', 'hidden', ['name' => 'banner_id']);
        }

        $elements = [];
        $elements['name'] = $fieldset->addField(
            'name',
            'text',
            [
                'name' => 'name',
                'label' => __('Name'),
                'title' => __('Name'),
                'required' => true,
            ]
        );

        $elements['status'] = $fieldset->addField(
            'status',
            'select',
            [
                'label' => __('Status'),
                'title' => __('Banner Status'),
                'name' => 'status',
                'options' => Status::getAvailableStatuses(),
            ]
        );

        $slider = $this->_sliderFactory->create()->load($sliderId);

        if ($slider->getId()) {
            $elements['slider_id'] = $fieldset->addField(
                'slider_id',
                'select',
                [
                    'label' => __('Slider'),
                    'name' => 'slider_id',
                    'values' => [
                        [
                            'value' => $slider->getId(),
                            'label' => $slider->getTitle(),
                        ],
                    ],
                ]
            );
        } else {
            $elements['slider_id'] = $fieldset->addField(
                'slider_id',
                'select',
                [
                    'label' => __('Slider'),
                    'name' => 'slider_id',
                    'values' => $model->getAvailableSlides(),
                ]
            );
        }

        $elements['image_alt'] = $fieldset->addField(
            'image_alt',
            'text',
            [
                'title' => __('Alt Text'),
                'label' => __('Alt Text'),
                'name' => 'image_alt',
                'note' => 'Used for SEO',
            ]
        );

        $wysiwygConfig = $this->_wysiwygConfig->getConfig();

        $elements['caption'] = $fieldset->addField(
            'caption',
            'editor',
            [
                'title' => __('Caption'),
                'label' => __('Caption'),
                'name' => 'caption',
                'config' => $wysiwygConfig
            ]
        );

        $elements['click_url'] = $fieldset->addField(
            'click_url',
            'text',
            [
                'title' => __('URL'),
                'label' => __('URL'),
                'name' => 'click_url',
            ]
        );
        $elements['background'] = $fieldset->addField(
            'background',
            'image',
            [
                'title' => __('Background Image'),
                'label' => __('Background Image'),
                'name' => 'background',
                'note' => 'Allow image type: jpg, jpeg, gif, png',
            ]
        ); 
        $elements['image'] = $fieldset->addField(
            'image',
            'image',
            [
                'title' => __('Banner Image Desktop'),
                'label' => __('Banner Image Desktop'),
                'name' => 'image',
                'note' => 'Allow image type: jpg, jpeg, gif, png',
            ]
        );

        $elements['banner_tablet'] = $fieldset->addField(
            'banner_tablet',
            'image',
            
            [
                'title' => __('Banner Image Tablet'),
                'label' => __('Banner Image Tablet'),
                'name' => 'banner_tablet',
                'note' => 'Allow image type: jpg, jpeg, gif, png',
            ]
        );  
        $elements['banner_mobie'] = $fieldset->addField(
            'banner_mobie',
            'image',
            
            [
                'title' => __('Banner Image Mobie'),
                'label' => __('Banner Image Mobie'),
                'name' => 'banner_mobie',
                'note' => 'Allow image type: jpg, jpeg, gif, png',
            ]
        );   
        $elements['banner_deal_icon'] = $fieldset->addField(
            'banner_deal_icon',
            'image',
            
            [
                'title' => __('Banner Deal Icon'),
                'label' => __('Banner Deal Icon'),
                'name' => 'banner_deal_icon',
                'note' => 'Allow image type: jpg, jpeg, gif, png',
            ]
        );   
        $elements['banner_deal_hightlight'] = $fieldset->addField(
            'banner_deal_hightlight',
            'image',
            
            [
                'title' => __('Banner Deal Hightlight'),
                'label' => __('Banner Deal Hightlight'),
                'name' => 'banner_deal_hightlight',
                'note' => 'Allow image type: jpg, jpeg, gif, png',
            ]
        );   
        $elements['banner_deal_title'] = $fieldset->addField(
            'banner_deal_title',
            'text',
            
            [
                'title' => __('Banner Deal Title'),
                'label' => __('Banner Deal Title'),
                'name' => 'banner_deal_title'
            ]
        );  
        $elements['banner_indicator_icon'] = $fieldset->addField(
            'banner_indicator_icon',
            'image',
            
            [
                'title' => __('Banner Delivery Icon'),
                'label' => __('Banner Delivery Icon'),
                'name' => 'banner_indicator_icon',
                'note' => 'Allow image type: jpg, jpeg, gif, png',
            ]
        );   
        $elements['banner_indicator_hightlight'] = $fieldset->addField(
            'banner_indicator_hightlight',
            'image',
            
            [
                'title' => __('Banner Delivery Hightlight'),
                'label' => __('Banner Delivery Hightlight'),
                'name' => 'banner_indicator_hightlight',
                'note' => 'Allow image type: jpg, jpeg, gif, png',
            ]
        );   
        $elements['banner_indicator_title'] = $fieldset->addField(
            'banner_indicator_title',
            'text',
            
            [
                'title' => __('Banner Delivery Title'),
                'label' => __('Banner Delivery Title'),
                'name' => 'banner_indicator_title'
            ]
        );  
        $elements['banner_onsite_icon'] = $fieldset->addField(
            'banner_onsite_icon',
            'image',
            
            [
                'title' => __('Banner Onsite Icon'),
                'label' => __('Banner Onsite Icon'),
                'name' => 'banner_onsite_icon',
                'note' => 'Allow image type: jpg, jpeg, gif, png',
            ]
        );   
        $elements['banner_onsite_hightlight'] = $fieldset->addField(
            'banner_onsite_hightlight',
            'image',
            
            [
                'title' => __('Banner Onsite Hightlight'),
                'label' => __('Banner Onsite Hightlight'),
                'name' => 'banner_onsite_hightlight',
                'note' => 'Allow image type: jpg, jpeg, gif, png',
            ]
        );   
        $elements['banner_onsite_title'] = $fieldset->addField(
            'banner_onsite_title',
            'text',
            
            [
                'title' => __('Banner Onsite Title'),
                'label' => __('Banner Onsite Title'),
                'name' => 'banner_onsite_title'
            ]
        );  




        $elements['banner_warranty_icon'] = $fieldset->addField(
            'banner_warranty_icon',
            'image',
            
            [
                'title' => __('Banner Warranty Icon'),
                'label' => __('Banner Warranty Icon'),
                'name' => 'banner_warranty_icon',
                'note' => 'Allow image type: jpg, jpeg, gif, png',
            ]
        );   
        $elements['banner_warranty_hightlight'] = $fieldset->addField(
            'banner_warranty_hightlight',
            'image',
            
            [
                'title' => __('Banner Warranty Hightlight'),
                'label' => __('Banner Warranty Hightlight'),
                'name' => 'banner_warranty_hightlight',
                'note' => 'Allow image type: jpg, jpeg, gif, png',
            ]
        );   
        $elements['banner_onsite_title'] = $fieldset->addField(
            'banner_warranty_title',
            'text',
            
            [
                'title' => __('Banner Warranty Title'),
                'label' => __('Banner Warranty Title'),
                'name' => 'banner_warranty_title'
            ]
        );  
        $dateFormat = 'M/d/yyyy';
        $timeFormat = 'h:mm a';
        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();        
        $locale = $objectManager->create('Magento\Framework\Locale\ResolverInterface');
        if($dataObj->hasData('start_time')) {
           // $datetime = $this->dateTime->date($dataObj->getData('start_time'), null, $this->_localeDate->getConfigTimezone());
            $datetime = new \DateTime($dataObj->getData('start_time'));

            $dataObj->setData('start_time',$datetime);

        }

        if($dataObj->hasData('end_time')) {
            //$datetime = $this->dateTime->date($dataObj->getData('end_time'), null, $this->_localeDate->getConfigTimezone());
            $datetime = new \DateTime($dataObj->getData('end_time'));
            $dataObj->setData('end_time', $datetime);
        }

        $style = 'color: #000;background-color: #fff; font-weight: bold; font-size: 13px;';
        $elements['start_time'] = $fieldset->addField(
            'start_time',
            'date',
            [
                'name' => 'start_time',
                'label' => __('Starting time'),
                'title' => __('Starting time'),
                'required' => true,
                'readonly' => true,
                'style' => $style,
                'class' => 'required-entry',
                'date_format' => $dateFormat,
                'time_format' => $timeFormat,
                'note' => implode(' ', [$dateFormat, $timeFormat])
            ]
        );

        $elements['end_time'] = $fieldset->addField(
            'end_time',
            'date',
            [
                'name' => 'end_time',
                'label' => __('Ending time'),
                'title' => __('Ending time'),
                'required' => true,
                'readonly' => true,
                'style' => $style,
                'class' => 'required-entry',
                'date_format' => $dateFormat,
                'time_format' => $timeFormat,
                'note' => implode(' ', [$dateFormat, $timeFormat])
            ]
        );

        $elements['target'] = $fieldset->addField(
            'target',
            'select',
            [
                'label' => __('Target'),
                'name' => 'target',
                'values' => [
                    [
                        'value' => \Magestore\Bannerslider\Model\Banner::BANNER_TARGET_SELF,
                        'label' => __('New Window with Browser Navigation'),
                    ],
                    [
                        'value' => \Magestore\Bannerslider\Model\Banner::BANNER_TARGET_PARENT,
                        'label' => __('Parent Window with Browser Navigation'),
                    ],
                    [
                        'value' => \Magestore\Bannerslider\Model\Banner::BANNER_TARGET_BLANK,
                        'label' => __('New Window without Browser Navigation'),
                    ],
                ],
            ]
        );

        foreach ($attributesInStore as $attribute) {
            if (isset($elements[$attribute])) {
                $elements[$attribute]->setStoreViewId($storeViewId);
            }
        }
        $form->addValues($dataObj->getData());
        $this->setForm($form);

        return $this;
    }

}
