<?php



namespace OnlineBiz\Custom\Block;

use Magestore\Bannerslider\Model\Slider as SliderModel;
use Magestore\Bannerslider\Model\Status;

/**
 * Slider item.
 * @category Magestore
 * @package  OnlineBiz_Custom
 * @module   OnlineBiz
 * @author   jordan Developer
 */
class SliderItem extends \Magestore\Bannerslider\Block\SliderItem
{
    
     public function getBannerCondition()
    {
        $sliderId = $this->_slider->getId();
        return $this->_bannerCollectionFactory->getBannerCondition($sliderId);
    }
}
