<?php
namespace OnlineBiz\Custom\Block;
class Mostview extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Reports\Model\ResourceModel\Product\CollectionFactory
     */
    protected $_productsFactory;
     protected $_productImageHelper;
     protected $_productRepository;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Reports\Model\ResourceModel\Product\CollectionFactory $productsFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Reports\Model\ResourceModel\Product\CollectionFactory $productsFactory,
        \Magento\Catalog\Helper\Image $productImageHelper,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        array $data = []
    ) {
        $this->_productRepository = $productRepository;
        $this->_productsFactory = $productsFactory;
        $this->_productImageHelper = $productImageHelper;
        parent::__construct($context, $data);
    }

    /**
     * Getting most viewed products
     */
    public function getCollection()
    {

        $currentWebsiteId = $this->_storeManager->getWebsite()->getWebsiteId();
        // $collection = $this->_productsFactory->create()
        // ->addAttributeToSelect(
        //     '*'
        // )->addViewsCount()->setStoreId(
        //         $currentStoreId
        // )->addStoreFilter(
        //         $currentStoreId
        // )->setPageSize(4);
        // $items = $collection->getItems();
        // return $items;

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
        $resource   = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $sql ="SELECT COUNT(report_table_views.event_id) AS `views`, `e`.*, `stock_status_index`.`stock_status` AS `is_salable` FROM `report_event` AS `report_table_views` INNER JOIN `catalog_product_entity` AS `e` ON e.entity_id = report_table_views.object_id INNER JOIN `catalog_product_website` AS `product_website` ON product_website.product_id = e.entity_id AND product_website.website_id = ".$currentWebsiteId." LEFT JOIN `cataloginventory_stock_status` AS `stock_status_index` ON e.entity_id = stock_status_index.product_id AND stock_status_index.website_id = ".$currentWebsiteId." WHERE (report_table_views.event_type_id = 1) GROUP BY `e`.`entity_id` HAVING (COUNT(report_table_views.event_id) > 0) ORDER BY `views` DESC LIMIT 4";
         $items = $connection->fetchAll($sql);
        return $items;
            
    }
    public function resizeImage($product, $imageId, $width, $height = null)
    {
        $resizedImage = $this->_productImageHelper
                           ->init($product, $imageId)
                           ->constrainOnly(TRUE)
                           ->keepAspectRatio(TRUE)
                           ->keepTransparency(TRUE)
                           ->keepFrame(FALSE)
                           ->resize($width, $height);
        return $resizedImage;
    } 
    public function getProductBySku($sku)
    {
        return $this->_productRepository->get($sku);
    }
}