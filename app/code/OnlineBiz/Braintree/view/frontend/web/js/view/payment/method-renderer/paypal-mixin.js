define([
    'Magento_Braintree/js/view/payment/adapter',
    'Magento_Checkout/js/model/payment/additional-validators',
    'mage/translate'
], function (Braintree, additionalValidators, $t) {
    'use strict';

    return function (Component) {
        return Component.extend({
            payWithPayPal: function () {
                if (additionalValidators.validate()) {
                    try {
                        Braintree.checkout.paypal.initAuthFlow();
                    } catch (e) {
                        this.messageContainer.addErrorMessage({
                            message: $t('Payment ' + this.getTitle() + ' can\'t be initialized.')
                        });
                    }
                }
            }
        });
    }
});