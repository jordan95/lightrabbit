<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-helpdesk
 * @version   1.1.93
 * @copyright Copyright (C) 2019 Mirasvit (https://mirasvit.com/)
 */



namespace OnlineBiz\Helpdesk\Block\Contact;

use Mirasvit\Helpdesk\Model\DepartmentFactory;
use Mirasvit\Helpdesk\Model\PriorityFactory;
use Magento\Framework\View\Element\Template;
use Mirasvit\Helpdesk\Model\Config;
use Magento\Framework\View\Element\Template\Context;
use Mirasvit\Helpdesk\Helper\Field as FieldHelper;
use Magento\Framework\Registry;
use Magento\Framework\Module\Manager as ModuleManager;
use Magento\Customer\Model\CustomerFactory;
use Magento\Customer\Model\Session as CustomerSession;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Form extends \Mirasvit\Helpdesk\Block\Contact\Form
{
    public function getPostAction()
    {
        return $this->context->getUrlBuilder()->getUrl(
            'helpdesk/form/post',
            ['_secure' => $this->isSecure()]
        );
    }
}
