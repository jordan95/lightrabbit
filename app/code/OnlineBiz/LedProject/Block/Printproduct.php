<?php
namespace OnlineBiz\LedProject\Block;
class Printproduct extends  \Magento\Framework\View\Element\Template
{
	protected $_productRepository;
	protected $_imageHelper;
	public function __construct(
		\Magento\Backend\Block\Template\Context $context,		
		\Magento\Catalog\Model\ProductRepository $productRepository,
		\Magento\Catalog\Helper\Image $imageHelper,
		array $data = []
	)
	{
		$this->_productRepository = $productRepository;
		$this->_imageHelper = $imageHelper;
		parent::__construct($context, $data);
	}
	
	public function getProductById($id)
	{
		return $this->_productRepository->getById($id);
	}
	public function getIdProduct(){
		$id = $this->getRequest()->getParam('product_id');
		if(isset($id)){
			return (int)$id;
		}
		return false;
	}
	public function getImageUrl($_product)
	{
		return $this->_imageHelper->init($_product,'category_page_grid')->resize(250,250)->getUrl();
	}
	public function convert_str($string) {
	    $string = htmlspecialchars($string);
	    //$string = preg_replace('/[^A-Za-z0-9\-\\s\\t\\n\\*\(\)\_\+\-\=\?\;\:\,\.\/\<\>\#\$\%\^\&\{\}\|\[\]\\\'\"\~\!\@\[\]\{\}\|]/', '', $string);
	    $regex   = array("\r\n", "\n", "\r");
	    $replace = '<br />';
	    $string = str_replace($regex, $replace, $string);
	    return $string;    
	}
}
