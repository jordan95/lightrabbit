<?php
namespace OnlineBiz\LedProject\Block;
class Printproject extends \OnlineBiz\LedProject\Block\Project
{

	public function getIdProject(){
		$id = $this->getRequest()->getParam('id');
		if(isset($id)){
			return $id;
		}
		return false;
	}
	public function getCustomerName(){
		$currentProject = $this->getCurrentProject();
		$customerID = $currentProject['customer_id'];
		if(isset($customerID)){
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$customer = $objectManager->create('Magento\Customer\Model\Customer')->load($customerID);
			return $customer->getName();
		}else{
			return false;
		}
		
		
	}
	
}
