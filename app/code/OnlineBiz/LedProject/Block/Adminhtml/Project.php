<?php



namespace OnlineBiz\LedProject\Block\Adminhtml;


class Project extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * Constructor.
     */
    protected function _construct()
    {
        $this->_controller = 'adminhtml_project';
        $this->_blockGroup = 'OnlineBiz_LedProject';
        $this->_headerText = __('Project');
     
        parent::_construct();
        $this->removeButton('add');
    }
}
