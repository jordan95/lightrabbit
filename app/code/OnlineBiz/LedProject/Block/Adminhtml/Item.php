<?php



namespace OnlineBiz\LedProject\Block\Adminhtml;


class Item extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * Constructor.
     */
    protected function _construct()
    {
        $this->_controller = 'adminhtml_item';
        $this->_blockGroup = 'OnlineBiz_LedProject';
        $this->_headerText = __('Item');
     
        parent::_construct();
        $this->removeButton('add');
    }
}
