<?php
namespace OnlineBiz\LedProject\Block\Adminhtml\Project\Renderer;
class Quantity extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
 
    public function render(\Magento\Framework\DataObject $row){
        $value =  $row->getData($this->getColumn()->getIndex());
        if($value){
            return $value;
            
        }else{
            return '0';
        }
    }
 
}
?>