<?php
namespace OnlineBiz\LedProject\Block\Adminhtml\Project\Renderer;
class Customer extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
    protected $_customer;
    public function __construct(
        \Magento\Backend\Block\Context $context,
        \Magento\Customer\Model\Customer $customer,
        array $data = []
    ) {
        $this->_customer = $customer;
        parent::__construct($context);
    }
    public function render(\Magento\Framework\DataObject $row){
        $value =  $row->getData($this->getColumn()->getIndex());
        $getName='';
        $collection = $this->_customer->getCollection()
               ->addAttributeToSelect('firstname')
               ->addAttributeToSelect('lastname');
                $collection->getSelect()->where('entity_id ='.$value);
        if($value){
            foreach ($collection as $item)
            {
               $getName = $item->getFirstname(). ' '.$item->getLastname();

            }
            $url = $this->getUrl('*/customer/edit', array('id' => $value));
            $out = "<a href='".$url."' target='_blank' title='View Customer'>".$getName."</a>";
            return $out;
        }
    }
 
}
?>