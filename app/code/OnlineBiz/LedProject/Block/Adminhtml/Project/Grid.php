<?php

/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Bannerslider
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

namespace OnlineBiz\LedProject\Block\Adminhtml\Project;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * slider collection factory.
     *
     * @var \Magestore\Bannerslider\Model\ResourceModel\Slider\CollectionFactory
     */
    protected $_projectCollectionFactory;

    /**
     * helper.
     *
     * @var \Magestore\Bannerslider\Helper\Data
     */
    protected $_projectHelper;
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \OnlineBiz\LedProject\Model\ResourceModel\Project\CollectionFactory $projectCollectionFactory,
        \OnlineBiz\LedProject\Helper\Data $projectHelper,
        array $data = []
    ) {
        $this->_projectCollectionFactory = $projectCollectionFactory;
        $this->_projectHelper = $projectHelper;
        parent::__construct($context, $backendHelper, $data);
    }

    protected function _construct()
    {
        parent::_construct();
        $this->setId('projectGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(false);
        $this->setUseAjax(false);
    }
    /**
     * prepare collection.
     *
     * @return [type] [description]
     */
    protected function _prepareCollection()
    {
        $resource_project = $this->_projectCollectionFactory->create();
        $resource_project->getSelect()->joinLeft(array('project_item'=>'onlinebiz_project_item'),'main_table.id = project_item.project_id',array('SUM(project_item.quantity) as quantity'));
        $resource_project->getSelect()->group('main_table.id');
        $this->setCollection($resource_project);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn(
            'id',
            [
                'header' => __('ID'),
                'type' => 'number',
                'index' => 'id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id',
            ]
        );
        $this->addColumn(
            'customer_name',
            [
                'header' => __('Customer Name'),
                'index' => 'customer_id',
                'type' => 'options',
                'options' => $this->_projectHelper->getAllCustomer(),
                'renderer'  => 'OnlineBiz\LedProject\Block\Adminhtml\Project\Renderer\Customer',
            ]
        );

        $this->addColumn('title',
            array(
                'header' =>__('Project Name '),
                'align' =>'center',
                'width' => 'auto',
                'index' => 'title'
            )
        );
        $this->addColumn('quantity',
            array(
                'header'=>__('Quantity'),
                'align' =>'right',
                'width' => '100px',
                'renderer'  => 'OnlineBiz\LedProject\Block\Adminhtml\Project\Renderer\Quantity',
                'index' => 'quantity',
                'filter_condition_callback' => array($this, '_totalquantyFilter')
            )
        );
        $this->addColumn('description',
            array(
                'header' =>__('Description'),
                'align' =>'center',
                'width' => 'auto',
                'index' => 'description'
            )
        );

        $this->addColumn('Action',
            array(
                'header' => 'Action',
                'width'	=> '100px',
                'type' => 'action',
                'getter' => 'getId',
                'actions'=> array(
                    array(
                        'caption' => __('View'),
                        'url' => array('base'=>'*/item/index'),
                        'field' => 'id',
                        'target'    => '_blank'
                    ),
                ),
                'filter' => false,
                'sortable' => false,
            )
        );
        return parent::_prepareColumns();
    }

    /**
     * @return string
     */
}
