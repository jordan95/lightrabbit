<?php
namespace OnlineBiz\LedProject\Block\Adminhtml\Item;
	class Grid extends \Magento\Backend\Block\Widget\Grid\Extended{
        protected $_itemCollectionFactory;

        /**
         * helper.
         *
         * @var \Magestore\Bannerslider\Helper\Data
         */
        protected $_project;
        protected $_itemHelper;
        protected $_storeManager;
        public function __construct(
            \Magento\Backend\Block\Template\Context $context,
            \Magento\Backend\Helper\Data $backendHelper,
            \OnlineBiz\LedProject\Model\Project $project,
            \OnlineBiz\LedProject\Model\ResourceModel\Item\CollectionFactory $itemCollectionFactory,
            \OnlineBiz\LedProject\Helper\Data $itemHelper,
            \Magento\Store\Model\StoreManagerInterface $storeManager,
            array $data = []
        ) {
            $this->_itemCollectionFactory = $itemCollectionFactory;
            $this->_itemHelper = $itemHelper;
            $this->_storeManager = $storeManager;
            $this->_project = $project;
            parent::__construct($context, $backendHelper, $data);
        }
		public function _construct(){

	        parent::_construct();
	        // Set some defaults for our grid
	        $this->setDefaultSort('id');
	        $this->setId('itemGrid');
	        $this->setDefaultDir('ASC');
	        $this->setUseAjax(false);
	        $this->setSaveParametersInSession(false);
	    }
     	protected function _prepareCollection()
	    {

	        $store_id = $this->_storeManager->getStore()->getId();
	        $id = $this->getRequest()->getParam('id');   
	        if($id){
	            $model_project = $this->_project->getCollection()->addFilter('id',$id);
	            foreach($model_project as $key => $value){
	                $project_id=$value['id'];
	            }
	            $model = $this->_itemCollectionFactory->create();
		        $model->getSelect()->reset(\Zend_Db_Select::COLUMNS)
		        ->columns('id')
		        ->columns('quantity')
		        ->columns('project_id')
		        ->columns('product_id')
		        ->columns('reference')
		        ->columns('Notes')
		        ->columns('varation')
		        ->columns('created_date')
		        ->where('main_table.project_id='.$project_id)
		        ->group(array('main_table.product_id','main_table.varation'));  
		         $model->getSelect()->join(array('project'=>'onlinebiz_project'),'main_table.project_id = project.id',array('project.title','project.customer_id'));
		        $model->getSelect()->join(array('catalog_product'=>'catalog_product_entity'),'main_table.product_id = catalog_product.entity_id',array('catalog_product.entity_id','sku','type_id'));
		        $model->getSelect()->join('catalog_product_entity_varchar','catalog_product.entity_id = catalog_product_entity_varchar.entity_id',array('catalog_product_entity_varchar.entity_id','catalog_product_entity_varchar.value'));
		        //$model->getSelect()->where("catalog_product_entity_varchar.attribute_id=65 and catalog_product_entity_varchar.store_id= ".$store_id);
	            $this->setCollection($model);
	        }
	        else{
	            $model = $this->_itemCollectionFactory->create();
		        $model->getSelect()->reset(\Zend_Db_Select::COLUMNS)
		        ->columns('id')
		        ->columns('quantity')
		        ->columns('project_id')
		        ->columns('product_id')
		        ->columns('reference')
		        ->columns('Notes')
		        ->columns('varation')
		        ->columns('created_date')
		        ->group(array('main_table.product_id','main_table.varation'));
		         $model->getSelect()->join(array('project'=>'onlinebiz_project'),'main_table.project_id = project.id',array('project.title','project.customer_id'));   
		        $model->getSelect()->join(array('catalog_product'=>'catalog_product_entity'),'main_table.product_id = catalog_product.entity_id',array('catalog_product.entity_id','sku','type_id'));
		        $model->getSelect()->join('catalog_product_entity_varchar','catalog_product.entity_id = catalog_product_entity_varchar.entity_id',array('catalog_product_entity_varchar.entity_id','catalog_product_entity_varchar.value'));
		        //$model->getSelect()->where("catalog_product_entity_varchar.attribute_id=65 and catalog_product_entity_varchar.store_id= ".$store_id);

	            $this->setCollection($model);
	        }   
	        return parent::_prepareCollection();
	    }
	    protected function _prepareColumns(){

			$this->addColumn('id',
				array(
					'header'=>__('ID'),
					'align' =>'right',
	                'width' => '100px',
	                'index' => 'id'
				)
			);

			$this->addColumn('title',
				array(
					'header' => __('Project Name'),
					'align' =>'center',
					'width' => 'auto',
					'index' => 'title'
				)
			);
			$this->addColumn('sku',
				array(
					'header' => __('SKU Product'),
					'align' =>'center',
					'width' => 'auto',
					'index' => 'sku'
				)
			);
			$this->addColumn('value',
				array(
					'header' => __('Product Name'),
					'align' =>'center',
					'width' => 'auto',
					'index' => 'value'
				)
			);
			$this->addColumn('varation',
				array(
					'header' => __('Variation'),
					'align' =>'center',
					'width' => 'auto',
					'renderer'  => 'OnlineBiz\LedProject\Block\Adminhtml\Item\Renderer\Variation',
					'index' => 'varation'
				)
			);	
			$this->addColumn('quantity',
				array(
					'header' => __('Quantity'),
					'align' =>'center',
					'width' => 'auto',
					'index' => 'quantity',
				)
			);	
			$this->addColumn('created_date',
				array(
					'header' => __('Date'),
					'align' =>'center',
					'width' => 'auto',
					'type' => 'datetime',
					'index' => 'created_date'
				)
			);	
			$this->addColumn('reference',
				array(
					'header' => __('Reference'),
					'align' =>'center',
					'width' => 'auto',
					'index' => 'reference'
				)
			);
			$this->addColumn('Notes',
				array(
					'header' => __('Notes'),
					'align' =>'center',
					'width' => 'auto',
					'index' => 'Notes'
				)
			);	
		}
	}
?>