<?php
namespace OnlineBiz\LedProject\Block\Adminhtml\Item\Renderer;
class Variation extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{

    protected $_customer;
    protected $_storeManager;
    protected $_option;
    public function __construct(
        \Magento\Backend\Block\Context $context,
        \Magento\Customer\Model\Customer $customer,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Eav\Model\Entity\Attribute\Option $option,
        array $data = []
    ) {
        $this->_customer = $customer;
        $this->_storeManager = $storeManager;
        $this->_option = $option;
        parent::__construct($context);
    }

    public function render(\Magento\Framework\DataObject $row){
        
        $value =  $row->getData($this->getColumn()->getIndex());
        if($value){
            $store_id = $this->_storeManager->getStore()->getId();
        	$varation= array();
        	$attribute_code='';
        	$attribute_value='';
        	$label='';
        	$varation = json_decode($value, true);
        	foreach($varation as $key=>$val){
        		$attribute_code .= $key.',';
        		$attribute_value .= $val.',';
        	}
        	$attribute_code = substr($attribute_code, 0, -1);
        	$attribute_value = substr($attribute_value, 0, -1);
        	$model = $this->_option->getCollection();
        	$model ->getSelect()->where('main_table.attribute_id in('.$attribute_code.') and main_table.option_id in('.$attribute_value.')');
        	$model ->getSelect()->join('eav_attribute','main_table.attribute_id = eav_attribute.attribute_id',array('eav_attribute.frontend_label'));
        	$model ->getSelect()->join('eav_attribute_option_value','main_table.option_id = eav_attribute_option_value.option_id',array('eav_attribute_option_value.value'));
        	$model ->getSelect()->where('eav_attribute_option_value.store_id = '.$store_id);
        	foreach($model->getData() as $k =>$v){
        		$label .= $v['frontend_label'].' : '.$v['value'].'<br/>';

        	}
            return $label;
        }   
    }
 
}
?>