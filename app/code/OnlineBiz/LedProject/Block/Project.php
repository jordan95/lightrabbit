<?php
namespace OnlineBiz\LedProject\Block;
class Project extends \Magento\Framework\View\Element\Template
{
    protected $session;
    protected $_projectFactory;
    protected $_itemFactory;
    public $_urlHelper;
    public $_projectHelper;
    public $_productHelper;
    protected $_product;
    protected $_configurable;
    public $_imageHelper;
    public function __construct(
        \Magento\Customer\Model\Session $customerSession,
        \OnlineBiz\LedProject\Model\Project $projectFactory,
        \OnlineBiz\LedProject\Model\Item $itemFactory,
        \Magento\Framework\Url $urlHelper,
        \OnlineBiz\LedProject\Helper\Data $projectHelper,
        \Magento\Catalog\Helper\Product $productHelper,
        \Magento\Catalog\Model\ProductFactory $product,
        \Magento\Catalog\Helper\Image $imageHelper,
        \Magento\ConfigurableProduct\Model\Product\Type\Configurable $configurable,
        \Magento\Framework\View\Element\Template\Context $context
    )
    {
        $this->session = $customerSession;
        $this->_projectFactory = $projectFactory;
        $this->_itemFactory = $itemFactory;
        $this->_urlHelper = $urlHelper;
        $this->_projectHelper = $projectHelper;
        $this->_productHelper = $productHelper;
        $this->_product = $product;
        $this->_imageHelper = $imageHelper;
        $this->_configurable = $configurable;
        parent::__construct($context);
        $this->_isScopePrivate = true;
    }
    public function _getCollectionClass(){
        return 'ledproject/project_collection'; 
    }
    public function getProject(){
       $model = $this->_projectFactory->getCollection();
       return $model;
    }
    public function getProjectOfCustomer(){
       $model = $this->_projectFactory->getCollection();
       $model->getSelect()->where("main_table.customer_id=".$this->getCustomer());
       return $model;
    }
    public function getProjectForCustomer(){
      $id_pro = $this->getRequest()->getParam('id');
      $customer_id = $this->getRequest()->getParam('customer_id');
      if(isset($id_pro)|| $id_pro !=''){
        $id_pro = $this->getRequest()->getParam('id');
         $model = $this->_projectFactory->load($id_pro);
      }else if(isset($customer_id)|| $customer_id !=''){
        $model = $this->_projectFactory->getCollection();
        $model->getSelect()->where("main_table.customer_id=".$customer_id);
      }else{
        $model = $this->_projectFactory->getCollection();
        $model->getSelect()->where("main_table.customer_id=".$this->getCustomer());
      }
      return $model;
    }
    public function getCustomer(){
       $customer = $this->session;
       return $customer->getCustomerId();
    }
    public function getCurrentProject(){
       $id = $this->getRequest()->getParam('id');
       $model = $this->_projectFactory->load($id);
       return $model;
    }
    public function getProduct(){
      $id = $this->getRequest()->getParam('product_id');
      if(!empty($id)){
          $product= $this->_product->create()->load($id);
          return $product;
      }
    }
    public function _updateqtyProduct(){
      $get = $this->getRequest()->getParams();
         if(isset($get['varation'])){
            $model = $this->_itemFactory->getCollection()->addFilter('project_id',$get['id'])->addFilter('product_id',$get['product_id'])->addFilter('varation',$get['varation']);
        }else if(isset($get['bundle_option']) && isset($get['bundle_option_qty'])){
                $model = $this->_itemFactory->getCollection()->addFilter('project_id',$get['id'])->addFilter('product_id',$get['product_id'])->addFilter('bundle_option',$get['bundle_option'])->addFilter('bundle_option_qty',$get['bundle_option_qty']);
        }else{
             $model = $this->_itemFactory->getCollection()->addFilter('project_id',$get['id'])->addFilter('product_id',$get['product_id']);
        }
        return $model;
    }
    public function getParentProduct($id){
      if(!empty($id)){
          $product= $this->_product->create()->load($id);
          //print_r($product);die('a');
          return $product;
      }
      // $childProducts = $this->_configurable->getUsedProducts(null,$product);
    }
    public function getProductforProject($id,$varation){
      if(!empty($id)){
          $configurableProduct = $this->_product->create()->load($id);
          $product=$this->_configurable->getProductByAttributes($varation, $configurableProduct); 
          return $product;
      }
    }
    public function getSummaryProject(){
        $resource_project = $this->_projectFactory->getCollection();
        $resource_project->getSelect()->where("main_table.customer_id=".$this->getCustomer());
        $resource_project->getSelect()->joinLeft(array('project_item'=>'onlinebiz_project_item'),'main_table.id = project_item.project_id',array('main_table.title','SUM(project_item.quantity) as qty'));
        $resource_project->getSelect()->group('main_table.id');
        return $resource_project;
    }
    public function getProducttoProject($id){
        $model = $this->_itemFactory->getCollection();
        $model->getSelect()->reset(\Zend_Db_Select::COLUMNS)
        ->columns('SUM(quantity) as qty')
        ->columns('product_id')
        ->columns('varation')
        ->columns('bundle_option')
        ->columns('bundle_option_qty')
        ->where('main_table.project_id='.$id)
        ->group(array('main_table.product_id','main_table.varation','main_table.bundle_option','main_table.bundle_option_qty'));
        return $model;

    
    }
}
