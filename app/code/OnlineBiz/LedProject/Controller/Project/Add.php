<?php
namespace OnlineBiz\LedProject\Controller\Project;

class Add extends \Magento\Framework\App\Action\Action
{
    /**
     * @var Session
     */
    protected $session;

    protected $_resultPageFactory;

    protected $_project;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \OnlineBiz\LedProject\Model\Project $project,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Customer\Model\Session $customerSession
        )
    {
        $this->_project = $project;
        $this->session = $customerSession;
        $this->_resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $this->_isScopePrivate = true;
        $model = $this->_project->getCollection();
        if(!$this->session->isLoggedIn()){
            $this->_redirect('customer/account/login');
        }else{
            if(count($model)<=0){
                $this->_redirect('ledproject/project/create');
            }
            $resultPage = $this->_resultPageFactory->create();
            //$resultPage->_initLayoutMessages('customer/session');
            //$resultPage->_initLayoutMessages('catalog/session');
            return $resultPage;
        }
    }
}
