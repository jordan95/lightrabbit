<?php
namespace OnlineBiz\LedProject\Controller\Project;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Registry;
use Mpdf\Mpdf;
use Magento\Framework\App\Response\Http\FileFactory;
class Printproduct extends \OnlineBiz\LedProject\Controller\Project\Printproject
{
    public function execute()
    {
        $product_id = $this->getRequest()->getParam('product_id');
        $name_download = "Product-".$product_id.".pdf";
        if(isset($product_id)){
            $resultPage = $this->_resultPageFactory->create();
            $resultPage->getConfig()->getTitle()->prepend(__('Print Product'));
            $html = $resultPage->getLayout()
                    ->createBlock('OnlineBiz\LedProject\Block\Printproduct')
                    ->setTemplate('OnlineBiz_LedProject::project/print-product.phtml')
                    ->toHtml();
             //$this->getResponse()->setBody($html);
            $mpdf = new \Mpdf\Mpdf(['mode' => 'c',"format"=>"Letter","default_font_size"=>"","default_font"=>"","margin_left"=>"15","margin_right"=>"15","margin_top"=>"15","margin_bottom"=>"15","margin_header"=>0,"margin_footer"=>0,"tempDir"=>"/home/cloudpanel/htdocs/test.lightrabbit.co.uk/var/tmp"]);
             $mpdf->WriteHTML($html);
            $mpdf->Output($name_download,\Mpdf\Output\Destination::DOWNLOAD);
        }else{
            $this->_redirect('*/*/summary');
        }
        
    }
}
