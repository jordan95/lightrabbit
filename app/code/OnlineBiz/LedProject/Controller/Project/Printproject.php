<?php
namespace OnlineBiz\LedProject\Controller\Project;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Registry;
use Mpdf\Mpdf;
use Magento\Framework\App\Response\Http\FileFactory;
class Printproject extends \Magento\Framework\App\Action\Action
{
    /**
     * @var Session
     */
    protected $_customerSession;
    protected $_coreRegistry;
    protected $_resultPageFactory;
    protected $_project;
    protected $_fileFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \OnlineBiz\LedProject\Model\Project $project,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Customer\Model\Session $customerSession,
        Registry $coreRegistry,
        FileFactory $fileFactory
        )
    {
        $this->_fileFactory = $fileFactory;
        $this->_project = $project;
        $this->_coreRegistry = $coreRegistry;
        $this->_customerSession = $customerSession;
        $this->_resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        if(isset($id)){
            $resultPage = $this->_resultPageFactory->create();
            $resultPage->getConfig()->getTitle()->prepend(__('Print Project'));
            $html = $resultPage->getLayout()
                    ->createBlock('OnlineBiz\LedProject\Block\Printproject')
                    ->setTemplate('OnlineBiz_LedProject::project/print-project.phtml')
                    ->toHtml();
            //$this->getResponse()->setBody($html);
            $mpdf = new \Mpdf\Mpdf(['mode' => 'c',"format"=>"Letter","default_font_size"=>"","default_font"=>"","margin_left"=>"15","margin_right"=>"15","margin_top"=>"15","margin_bottom"=>"15","margin_header"=>0,"margin_footer"=>0,"tempDir"=>"/home/cloudpanel/htdocs/test.lightrabbit.co.uk/var/tmp"]);
             $mpdf->WriteHTML($html);
            $mpdf->Output('Product.pdf',\Mpdf\Output\Destination::DOWNLOAD);
        }else{
            $this->_redirect('*/*/summary');
        }
        
    }
}
