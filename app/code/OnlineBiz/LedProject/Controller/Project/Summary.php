<?php
namespace OnlineBiz\LedProject\Controller\Project;

class Summary extends \Magento\Framework\App\Action\Action
{
    /**
     * @var Session
     */
    protected $session;

    protected $_resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Customer\Model\Session $customerSession
        )
    {
        $this->session = $customerSession;
        $this->_resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $this->_isScopePrivate = true;
        if(!$this->session->isLoggedIn()){
            $this->_redirect('customer/account/login');
        }else{
            $resultPage = $this->_resultPageFactory->create();
            //$resultPage->_initLayoutMessages('customer/session');
            //$resultPage->_initLayoutMessages('catalog/session');
            return $resultPage;
        }
    }
}
