<?php
namespace OnlineBiz\LedProject\Controller\Project;

class Saveproduct extends \Magento\Framework\App\Action\Action
{
    /**
     * @var Session
     */
    protected $session;

    protected $_resultPageFactory;

    protected $_project;

    protected $_customer;

    protected $_template;

    protected $scopeConfig;
    
    protected $_item;

    protected $formKeyValidator;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \OnlineBiz\LedProject\Model\Project $project,
        \OnlineBiz\LedProject\Model\Item $item,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Customer\Model\Customer $customerFacotry,
        \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator,
        \Magento\Email\Model\Template $emailTemplate
    )
    {
        $this->session = $customerSession;
        $this->_project = $project;
        $this->_item = $item;
        $this->_customer = $customerFacotry;
        $this->_resultPageFactory = $resultPageFactory;
        $this->scopeConfig = $scopeConfig;
        $this->formKeyValidator = $formKeyValidator;
        $this->_template = $emailTemplate;
        parent::__construct($context);
    }

    public function execute()
    {
        $this->_isScopePrivate = true;
        if (!$this->session->isLoggedIn()) {
            $this->_redirect('customer/account/login');
        } else {
            $post = $this->getRequest()->getParams();
            $id = $this->getRequest()->getParam('id');
            //initialized the update data
            if(empty($post['varation'])){
                $updateData = array('quantity'=>$post['quantity']);
            } else {
                $updateData = array('quantity'=>$post['quantity'],'varation'=>$post['varation']);
            }
            //create the model object and load the  all data of a particular row and add the data in model class.
            $model = $this->_item->load($id)->addData($updateData);
            try {
                //save the data
                $model->setId($id)->save();
                $this->messageManager->addSuccess(__('Updated product successful.'));
                $this->_redirect('*/*/index/');

            } catch (Exception $e) {
                $this->messageManager->addError(__('Unable to update product. Please, try again later'));
                $this->_redirect('*/*/index/');
            }
        }
    }
}
