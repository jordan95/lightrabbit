<?php
namespace OnlineBiz\LedProject\Controller\Project;

class Addcart extends \Magento\Framework\App\Action\Action
{
    /**
     * @var Session
     */
    protected $session;

    protected $_resultPageFactory;

    protected $_project;

    protected $_customer;

    protected $_template;

    protected $scopeConfig;

    protected $_cart;

    protected $_product;

    protected $eventManager;

    protected $_checkoutSession;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \OnlineBiz\LedProject\Model\Project $project,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Email\Model\Template $emailTemplate,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Customer\Model\Customer $customerFacotry,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Catalog\Model\Product $product,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\Event\Manager $eventManager
        )
    {
        $this->session = $customerSession;
        $this->_project = $project;
        $this->_customer = $customerFacotry;
        $this->_template = $emailTemplate;
        $this->_resultPageFactory = $resultPageFactory;
        $this->scopeConfig = $scopeConfig;
        $this->_cart = $cart;
        $this->_product = $product;
        $this->eventManager = $eventManager;
        $this->_checkoutSession = $checkoutSession;
        parent::__construct($context);
    }

    public function execute()
    {
        $this->_isScopePrivate = true;
        if(!$this->session->isLoggedIn()){
            $this->_redirect('customer/account/login');
        }else{
            $cart   = $this->_cart;
            $data = $this->getRequest()->getParams();
            if(isset($data)){
                if(!empty($data['qty'])){
                    $project_id = $data['id'];
                    $allproduct = $this->_view->getLayout()->createBlock('OnlineBiz\LedProject\Block\Project')->getProducttoProject($project_id);
                    foreach($allproduct->getData() as $k => $val){
                        if ($val['product_id'] == '') continue;
                        if(!is_numeric($val['qty']) || $val['qty'] <= 0) continue;
                        if($val['varation'] !=''){
                            $super_attr = json_decode($val['varation'],true);
                        }
                        //$_product = $this->_product->load($val['product_id']); // not use because add multi bundle product will error.
                        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
                        $_product =  $objectManager->create('\Magento\Catalog\Model\Product')->load($val['product_id']);
                        $productRepository = $objectManager->get('\Magento\Catalog\Model\ProductRepository');
                        if($_product->getTypeId() == \Magento\ConfigurableProduct\Model\Product\Type\Configurable::TYPE_CODE){
                            $params['super_attribute'] = $super_attr;
                            $params['qty'] = $val['qty'];
                            $cart->addProduct($_product, $params);
                        }
                        elseif($_product->getTypeId() == \Magento\Catalog\Model\Product\Type::TYPE_BUNDLE){
                            
                            if($_product->getPriceType()==0){
                                $simple_id_of_bundle = array();
                                $product_simple_id = array();
                                $param_bundle_qty = array();
                                $params_bundle_qty = json_decode($val['bundle_option_qty'],true);
                                $params_bundle_option = json_decode($val['bundle_option'],true);
                                foreach($params_bundle_option as $key=>$value){
                                    if (!is_array($value)){
                                        foreach($params_bundle_qty as $k=>$val){
                                            if (!is_array($val)){
                                                unset($params_bundle_qty[$k]);
                                                $params_bundle_qty[$k][$value] =$val;

                                            }
                                        }
                                    }
                                                    
                                }
                                foreach($params_bundle_qty as $key=>$value){
                                    if (is_array($value) || is_object($value)){
                                        foreach($value as $k =>$val){
                                            if($val !="0" && !empty($val)){
                                                $simple_id_of_bundle[$k] = $val;
                                            }   
                                        } 
                                    }
                                                    
                                }
                                
                                $selectionCollection = $_product->getTypeInstance()->getSelectionsCollection($_product->getOptionsIds($_product), $_product);
                                foreach($selectionCollection as $key =>$option){
                                    if(array_key_exists($option->getSelectionId(),$simple_id_of_bundle)){
                                            $product_simple_id[$option->getProductId()] = $simple_id_of_bundle[$option->getSelectionId()];
                                    }
                                }
                                foreach($product_simple_id as $id =>$qty){
                                    // get product by product id 
                                    $product_simple_of_bundle = $productRepository->getById($id);

                                    $param_bundle_qty['qty'] = (int) $qty;
                                    $param_bundle_qty['product'] = $id;
                                    if(!empty($param_bundle_qty) && $param_bundle_qty['qty'] >=0){
                                        $cart->addProduct($product_simple_of_bundle, $param_bundle_qty);
                                    }
                                    
                                }
                            }else{
                                $params['bundle_option'] = json_decode($val['bundle_option'],true);
                                $params['bundle_option_qty'] = json_decode($val['bundle_option_qty'],true);
                                $params['qty']=$val['qty'];
                                $params['product']=$val['product_id'];
                                foreach($params['bundle_option'] as $opt_id => $values){
                                    if(is_array($values)){
                                        foreach($values as $key=>$selectId){
                                            if(isset($params['bundle_option_qty'][$opt_id][$selectId]) && $params['bundle_option_qty'][$opt_id][$selectId] == 0){
                                                unset($params['bundle_option'][$opt_id][$key]);
                                            }
                                        }
                                    }
                                }
                                $cart->addProduct($_product, $params);
                            } 
                        }
                        else{
                            try {
                                $params = array(
                                    'product' => $_product,
                                    'qty' => $val['qty']
                                );
                                $cart->addProduct($_product, $params);
                            }catch (Exception $e) {
                                $this->messageManager->addException(__('Can not add item to shopping cart'));
                            }
                        }
                    }


                    $cart->save();

                    $this->_checkoutSession->setCartWasUpdated(true);

                    /**
                     * @todo remove wishlist observer processAddToCart
                     */
                    $this->eventManager->dispatch('checkout_cart_add_product_complete',
                        array('product' => $_product, 'request' => $this->getRequest(), 'response' => $this->getResponse())
                    );

                    if (!$this->_checkoutSession->getNoCartRedirect(true)) {
                        if (!$cart->getQuote()->getHasError()){
                            $message = __('these products was added to your shopping cart.');
                            $this->messageManager->addSuccess($message);
                        }
                        //$this->goBack();
                    }
                }else{
                    $this->messageManager->addError(__('Can not add item to shopping cart.'));
                }
            }
            $this->_redirect('checkout/cart');
        }
    }
}
