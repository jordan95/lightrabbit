<?php
namespace OnlineBiz\LedProject\Controller\Project;

class Deleteproduct extends \Magento\Framework\App\Action\Action
{
    /**
     * @var Session
     */
    protected $session;

    protected $_resultPageFactory;

    protected $_project;

    protected $_item;

    protected $_projectHelper;

    protected $_customer;

    protected $_template;

    protected $scopeConfig;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \OnlineBiz\LedProject\Model\Project $project,
        \OnlineBiz\LedProject\Model\Item $item,
        \OnlineBiz\LedProject\Helper\Data $projectHelper,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Email\Model\Template $emailTemplate,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Customer\Model\Customer $customerFacotry
        )
    {
        $this->session = $customerSession;
        $this->_project = $project;
        $this->_item = $item;
        $this->_projectHelper = $projectHelper;
        $this->_customer = $customerFacotry;
        $this->_template = $emailTemplate;
        $this->_resultPageFactory = $resultPageFactory;
        $this->scopeConfig = $scopeConfig;
        parent::__construct($context);
    }

    public function execute()
    {
        $this->_isScopePrivate = true;
        if(!$this->session->isLoggedIn()){
            $this->_redirect('customer/account/login');
        }else{
            $get=$this->getRequest()->getParams();
            if(isset($get['varation'])){
                $model = $this->_item->getCollection()->addFilter('project_id',$get['id'])->addFilter('product_id',$get['product_id'])->addFilter('varation',$get['varation']);
            }else{
                $model = $this->_item->getCollection()->addFilter('project_id',$get['id'])->addFilter('product_id',$get['product_id']);
            }
            try {
                foreach ($model as $item) {
                    $item->setId($item['id'])->delete();
                }

                $this->messageManager->addSuccess(__('Delete product successful.'));
                $this->_redirect('*/*/summary');

            } catch (Exception $e){
                $this->messageManager->addError(__('Unable to delete product. Please, try again'));
                $this->_redirect('*/*/summary');
            }
        }
    }
}
