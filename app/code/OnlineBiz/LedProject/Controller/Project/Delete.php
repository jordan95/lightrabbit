<?php
namespace OnlineBiz\LedProject\Controller\Project;

class Delete extends \Magento\Framework\App\Action\Action
{
    /**
     * @var Session
     */
    protected $session;

    protected $_resultPageFactory;

    protected $_project;

    protected $_item;

    protected $_projectHelper;

    protected $_customer;

    protected $_template;

    protected $scopeConfig;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \OnlineBiz\LedProject\Model\Project $project,
        \OnlineBiz\LedProject\Model\Item $item,
        \OnlineBiz\LedProject\Helper\Data $projectHelper,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Email\Model\Template $emailTemplate,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Customer\Model\Customer $customerFacotry
        )
    {
        $this->session = $customerSession;
        $this->_project = $project;
        $this->_item = $item;
        $this->_projectHelper = $projectHelper;
        $this->_customer = $customerFacotry;
        $this->_template = $emailTemplate;
        $this->_resultPageFactory = $resultPageFactory;
        $this->scopeConfig = $scopeConfig;
        parent::__construct($context);
    }

    public function execute()
    {
        $this->_isScopePrivate = true;
        if(!$this->session->isLoggedIn()){
            $this->_redirect('customer/account/login');
        }else{
            $project_id = $this->getRequest()->getParams('id');
            $id = $project_id['id'];
            $model = $this->_project;
            $model_item = $this->_item->getCollection()->addFilter('project_id',$id);
            try {
                $model->setId($id)->delete();
                foreach ($model_item as $item) {
                    $item->setId($item['id'])->delete();
                }
                $this->messageManager->addSuccess(__('Delete project successful.'));
                $this->_redirect('*/*/summary');

            } catch (Exception $e){
                $this->messageManager->addError(__('Unable to delete project. Please, try again'));
                $this->_redirect('*/*/summary');
            }
        }
    }
}
