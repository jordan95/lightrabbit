<?php
namespace OnlineBiz\LedProject\Controller\Project;

class Edit extends \Magento\Framework\App\Action\Action
{
    /**
     * @var Session
     */
    protected $session;

    protected $_resultPageFactory;

    protected $_project;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \OnlineBiz\LedProject\Model\Project $project,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Customer\Model\Session $customerSession
        )
    {
        $this->_project = $project;
        $this->session = $customerSession;
        $this->_resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $this->_isScopePrivate = true;
        $model = $this->_project->getCollection();
        if(!$this->session->isLoggedIn()){
            $this->_redirect('customer/account/login');
        }else{
            $id = $this->getRequest()->getParam('id');
            $model = $this->_project->load($id);
            if($model->getId()){
                $resultPage = $this->_resultPageFactory->create();
                return $resultPage;
            }
        }
    }
}
