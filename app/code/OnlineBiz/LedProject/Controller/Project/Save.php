<?php
namespace OnlineBiz\LedProject\Controller\Project;

class Save extends \Magento\Framework\App\Action\Action
{
    /**
     * @var Session
     */
    protected $session;

    protected $_resultPageFactory;

    protected $_project;

    protected $_customer;

    protected $_template;

    protected $scopeConfig;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \OnlineBiz\LedProject\Model\Project $project,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\Mail\Template\TransportBuilder $emailTemplate,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Customer\Model\Customer $customerFacotry
        )
    {
        $this->session = $customerSession;
        $this->_project = $project;
        $this->_customer = $customerFacotry;
        $this->_template = $emailTemplate;
        $this->_resultPageFactory = $resultPageFactory;
        $this->scopeConfig = $scopeConfig;
        parent::__construct($context);
    }

    public function execute()
    {
        $this->_isScopePrivate = true;
        if(!$this->session->isLoggedIn()){
            $this->_redirect('customer/account/login');
        }else{
            $post = $this->getRequest()->getParams();
            $id = $this->getRequest()->getParam('id');
            $data = array('title'=>$post['title-project'],'description'=>$post['description-project'],'customer_id'=>$post['customerId']);
            try {
                if(isset($post['id'])){
                    $model = $this->_project->load($id)->addData($data);
                    $model->setId($id)->save();
                    $this->messageManager->addSuccess(__('Edit project successful.'));
                }else{
                    $model = $this->_project->setData($data);
                    $insertId = $model->save()->getId();
                    $this->messageManager->addSuccess(__('Create project successful.'));
                    $customerData = $this->_customer->load($data['customer_id']);
                    $customer_email = $customerData->getEmail();
                    $data['email_customer'] = $customer_email;
                    $template_id = 'send_email_create_project_success';
                    $email_template  = $this->_template->setTemplateIdentifier($template_id);
                    $send_to_email = $this->scopeConfig->getValue('trans_email/ident_custom1/email', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                    if(!empty($sender_email)){
                        $transport = $email_template->setFrom('general')
                        ->addTo($send_to_email,'admin')
                        ->getTransport();
                        $transport->sendMessage();
                    }
                }

                $this->_redirect('*/*/summary');
            } catch (Exception $e){
                $this->messageManager->addError(__('Unable to create project. Please, try again later'));
                $this->_redirect('*/*/summary');
            }
        }
    }
}
