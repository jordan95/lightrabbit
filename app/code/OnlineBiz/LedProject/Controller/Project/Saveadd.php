<?php
namespace OnlineBiz\LedProject\Controller\Project;

class Saveadd extends \Magento\Framework\App\Action\Action
{
    /**
     * @var Session
     */
    protected $session;

    protected $_resultPageFactory;

    protected $_project;

    protected $_customer;

    protected $_template;

    protected $scopeConfig;
    
    protected $_item;

    protected $formKeyValidator;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \OnlineBiz\LedProject\Model\Project $project,
        \OnlineBiz\LedProject\Model\Item $item,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Customer\Model\Customer $customerFacotry,
        \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator,
        \Magento\Framework\Mail\Template\TransportBuilder $emailTemplate
    )
    {
        $this->session = $customerSession;
        $this->_project = $project;
        $this->_item = $item;
        $this->_customer = $customerFacotry;
        $this->_resultPageFactory = $resultPageFactory;
        $this->scopeConfig = $scopeConfig;
        $this->formKeyValidator = $formKeyValidator;
        $this->_template = $emailTemplate;
        parent::__construct($context);
    }

    public function execute()
    {
        $this->_isScopePrivate = true;
        if ($this->formKeyValidator->validate($this->getRequest())) {
            return $this->_redirect('*/*/');
        }
        if(!$this->session->isLoggedIn()){
            $this->_redirect('customer/account/login');
        }else{
            $post = $this->getRequest()->getParams();
            $data_create = array('title'=>$post['title-project'],'description'=>$post['description-project'],'customer_id'=>$post['customerId']);
            if(!empty($data_create['title'])){
                $model = $this->_project->setData($data_create);
                $insertId = $model->save()->getId();
                if(isset($post['varation'])){
                    $data = array('project_id'=>$model->getId(),'varation'=>$post['varation'],'quantity'=>$post['quantity'],'reference'=>$post['reference'],'notes'=>$post['notes'],'product_id'=>$post['product_id'],'created_date'=>$post['created_date']);
                }else if(isset($post['bundle_option']) && isset($post['bundle_option_qty'])){
                    foreach($post['bundle_option'] as $opt_id => $values){
                        if(is_array($values)){
                            foreach($values as $key=>$selectId){
                                if(isset($post['bundle_option_qty'][$opt_id][$selectId]) && $post['bundle_option_qty'][$opt_id][$selectId] == 0){
                                    unset($post['bundle_option'][$opt_id][$key]);
                                    if(empty($post['bundle_option'][$opt_id])){
                                        unset($post['bundle_option'][$opt_id]);
                                        unset($post['bundle_option_qty'][$opt_id]);
                                    }
                                }
                            }
                        }
                    }
                    $bundle_option = json_encode($post['bundle_option']);
                    $bundle_option_qty = json_encode($post['bundle_option_qty']);
                    $data = array('project_id'=>$model->getId(),'quantity'=>$post['quantity'],'reference'=>$post['reference'],'notes'=>$post['notes'],'product_id'=>$post['product_id'],'created_date'=>$post['created_date'],'bundle_option'=>$bundle_option,'bundle_option_qty'=>$bundle_option_qty);
                }else{
                    $data = array('project_id'=>$model->getId(),'quantity'=>$post['quantity'],'reference'=>$post['reference'],'notes'=>$post['notes'],'product_id'=>$post['product_id'],'created_date'=>$post['created_date']);
                }
            }else{
                if(isset($post['varation'])){
                    $data = array('project_id'=>$post['id'],'varation'=>$post['varation'],'quantity'=>$post['quantity'],'reference'=>$post['reference'],'notes'=>$post['notes'],'product_id'=>$post['product_id'],'created_date'=>$post['created_date']);
                }else if(isset($post['bundle_option']) && isset($post['bundle_option_qty'])){
                    foreach($post['bundle_option'] as $opt_id => $values){
                        if(is_array($values)){
                            foreach($values as $key=>$selectId){
                                if(isset($post['bundle_option_qty'][$opt_id][$selectId]) && $post['bundle_option_qty'][$opt_id][$selectId] == 0){
                                    unset($post['bundle_option'][$opt_id][$key]);
                                    if(empty($post['bundle_option'][$opt_id])){
                                        unset($post['bundle_option'][$opt_id]);
                                        unset($post['bundle_option_qty'][$opt_id]);
                                    }
                                }
                            }
                        }
                    }
                    $bundle_option = json_encode($post['bundle_option']);
                    $bundle_option_qty = json_encode($post['bundle_option_qty']);
                    $data = array('project_id'=>$post['id'],'quantity'=>$post['quantity'],'reference'=>$post['reference'],'notes'=>$post['notes'],'product_id'=>$post['product_id'],'created_date'=>$post['created_date'],'bundle_option'=>$bundle_option,'bundle_option_qty'=>$bundle_option_qty);
                }else{
                    $data = array('project_id'=>$post['id'],'quantity'=>$post['quantity'],'reference'=>$post['reference'],'notes'=>$post['notes'],'product_id'=>$post['product_id'],'created_date'=>$post['created_date']);
                }
            }
            $model = $this->_item->setData($data);
            try {
                $insertId = $model->save()->getId();
                $this->messageManager->addSuccess(__('Add to project successful.'));
                if(!empty($data_create['title'])){
                    $customerData = $this->_customer->load($data_create['customer_id']);
                    $customer_email = $customerData->getEmail();
                    $data_create['email_customer'] = $customer_email;
                    $template_id = 'send_email_create_project_success';
                    $email_template  = $this->_template->setTemplateIdentifier($template_id);
                    $send_to_email = $this->scopeConfig->getValue('trans_email/ident_custom1/email', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                    if(!empty($sender_email)){
                        $email_template->setFrom('general')
                            ->addTo($send_to_email,'admin')
                            ->getTransport();
                        $email_template->sendMessage();
                    }
                }
                $this->_redirect('*/*/summary');
            } catch (Exception $e){
                $this->messageManager->addError(__('Unable to add project. Please, try again later'));
                $this->_redirect('*/*/summary');
            }
        }
    }
}
