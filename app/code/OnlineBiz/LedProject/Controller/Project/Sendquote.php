<?php
namespace OnlineBiz\LedProject\Controller\Project;

class Sendquote extends \Magento\Framework\App\Action\Action
{
    /**
     * @var Session
     */
    protected $session;

    protected $_resultPageFactory;
    protected $_template;
    protected $_storeManager;
    protected $scopeConfig;
    protected $_message;
    protected $transportInterfaceFactory;
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Mail\Template\TransportBuilder $emailTemplate,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Mail\Message $message,
        \Magento\Framework\Mail\TransportInterfaceFactory $transportInterfaceFactory
    )
    {
        $this->_template = $emailTemplate;
        $this->session = $customerSession;
        $this->_storeManager = $storeManager;
        $this->_resultPageFactory = $resultPageFactory;
        $this->scopeConfig = $scopeConfig;
        $this->_message = $message;
        $this->transportInterfaceFactory = $transportInterfaceFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $this->_isScopePrivate = true;
        if(!$this->session->isLoggedIn()){
            $this->_redirect('customer/account/login');
        }else{
            $post = $this->getRequest()->getParams();
            $from_email = $post['email'];
            $from_name = $post['name'];
            $subject = $post['subject'];
            $link_project = $post['link_project'];
            $phone = $post['phone'];
            $request_content = $post['request_quote'];
            $html = '<h1 style="text-align:center;">'.$subject.'</h1>'.'<p><span style="font-weight:bold">customer : '.$from_name.'</span></p>'.'<p><span style="font-weight:bold"> Phone : '.$phone.'</span></p>'.'<p><span style="font-weight:bold"> Link of project : </span><a href="'.$post['link_project'].'">'.$post['link_project'].'</a></p>'.'<p>Request of Customer:</p>'.'<p style="margin-top:40px">'.$request_content.'</p>';
            $mail = $this->_message;
            $mail->setMessageType('text/html');
            $mail->setBody($html);
            $mail->setSubject($subject);
            $mail->setFrom($from_email, $from_name);
            $mail->addTo(
                $this->scopeConfig->getValue(
                'trans_email/ident_sales/email',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
                $this->scopeConfig->getValue(
                'trans_email/ident_sales/name',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE)
            );
            $mailTransport = $this->transportInterfaceFactory->create(['message' => clone $mail]);
            try {
                $mailTransport->sendMessage();
                $this->messageManager->addSuccess('Your request has been sent');
                $this->_redirect('*/*/index');
            }
            catch (Exception $e) {
                $this->messageManager->addError('Unable to send.');
                $this->_redirect('*/*/index');
            }
        }
    }
}
