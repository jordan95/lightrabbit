<?php

namespace OnlineBiz\LedProject\Controller\Adminhtml;

abstract class Project extends \OnlineBiz\LedProject\Controller\Adminhtml\AbstractAction
{
    const PARAM_CRUD_ID = 'id';

    
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('OnlineBiz_LedProject::ledproject_project');
    }
}