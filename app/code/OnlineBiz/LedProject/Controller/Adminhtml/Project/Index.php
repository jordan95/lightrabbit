<?php

namespace OnlineBiz\LedProject\Controller\Adminhtml\Project;


class Index extends \OnlineBiz\LedProject\Controller\Adminhtml\Project
{
    
    public function execute()
    {

        $resultPage = $this->_resultPageFactory->create();

        return $resultPage;
    }
}
