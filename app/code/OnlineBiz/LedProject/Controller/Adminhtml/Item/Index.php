<?php

namespace OnlineBiz\LedProject\Controller\Adminhtml\Item;


class Index extends \OnlineBiz\LedProject\Controller\Adminhtml\Item
{
    
    public function execute()
    {

        $resultPage = $this->_resultPageFactory->create();

        return $resultPage;
    }
}
