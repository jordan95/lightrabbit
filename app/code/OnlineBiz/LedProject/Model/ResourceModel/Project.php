<?php
namespace OnlineBiz\LedProject\Model\ResourceModel;
class Project extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb {

    public function _construct() {
        $this->_init('onlinebiz_project', 'id');
    }

}