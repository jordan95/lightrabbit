<?php
namespace OnlineBiz\LedProject\Model\ResourceModel\Item;
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection {

    public function _construct() {
    	parent::_construct();
        $this->_init('OnlineBiz\LedProject\Model\Item','OnlineBiz\LedProject\Model\ResourceModel\Item');
    }

}