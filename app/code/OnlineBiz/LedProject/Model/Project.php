<?php
namespace OnlineBiz\LedProject\Model;
class Project extends \Magento\Framework\Model\AbstractModel {

    public function _construct() {
        parent::_construct();
        $this->_init('OnlineBiz\LedProject\Model\ResourceModel\Project');
    }

}