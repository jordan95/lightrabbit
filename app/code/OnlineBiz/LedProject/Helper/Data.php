<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Wishlist
 * @copyright  Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Wishlist Data Helper
 *
 * @category   Mage
 * @package    Mage_Wishlist
 * @author     Magento Core Team <core@magentocommerce.com>
 */
namespace OnlineBiz\LedProject\Helper;
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $session;
    protected $_customer;
    protected $_project;
    public function __construct(
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Model\Customer $customerFacotry,
        \OnlineBiz\LedProject\Model\Project $project,
        \Magento\Framework\App\Helper\Context $context
    ){
        $this->session = $customerSession;
        $this->_project = $project;
        $this->_customer = $customerFacotry;
        parent::__construct($context);
    }
	public function getCustomerName(){
       $customer = $this->session->getCustomer();
       $fullName = $customer->getName();
       return $fullName;
    }
    public function getAllCustomer(){
    	$getName = array(); 
    	$project_id ='';
    	$model_project = $this->_project->getCollection();
    	$model_project->getSelect()->group('customer_id');
	            foreach($model_project as $key => $value){
	            	if($value['customer_id'] != NULL){
	            		$project_id .= $value['customer_id'].',';
	            	}

	            }
	    $project_id= substr($project_id,0,-1);
	  	if(!empty($project_id)){
	      $collection = $this->_customer->getCollection()
			   ->addAttributeToSelect('firstname')
			   ->addAttributeToSelect('lastname');
			   	$collection->getSelect()->where('entity_id in('.$project_id.')');
			foreach ($collection as $item)
			{
			   $getName[$item->getEntityId()] = $item->getFirstname(). ' '.$item->getLastname();

			}
			return $getName;
		}
		
    }
    
}
