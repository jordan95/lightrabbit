<?php
/**
 * DISCLAIMER
 * Do not edit or add to this file if you wish to upgrade Smile ElasticSuite to newer
 * versions in the future.
 *
 * @category  Smile
 * @package   Smile\ElasticsuiteCatalog
 * @author    Romain Ruaud <romain.ruaud@smile.fr>
 * @copyright 2018 Smile
 * @license   Open Software License ("OSL") v. 3.0
 */
namespace OnlineBiz\ElasticsuiteCatalog\Block\Navigation\Renderer;

use Magento\Store\Model\ScopeInterface;
use Smile\ElasticsuiteCatalog\Model\Layer\Filter\Price;
use Magento\Catalog\Model\Layer\Filter\DataProvider\Price as PriceDataProvider;

/**
 * This block handle price slider rendering.
 *
 * @category Smile
 * @package  Smile\ElasticsuiteCatalog
 * @author   Romain Ruaud <romain.ruaud@smile.fr>
 */
class PriceSlider extends \Smile\ElasticsuiteCatalog\Block\Navigation\Renderer\PriceSlider
{
    /**
     * Retrieve filter URL template with placeholders for range.
     *
     * @return string
     */
    public function getPriceRangeUrlTemplate()
    {
        $filter = $this->getFilter();
        $item   = current($this->getFilter()->getItems());

        $regexp      = "/({$filter->getRequestVar()})-(-?[0-9]+)/";
        $replacement = '${1}-{{from}}_{{to}}';

        return preg_replace($regexp, $replacement, $item->getUrl());
    }

    public function getMin()
    {
        $minValue = $this->getFilter()->getMinValue();

        if ($this->isManualCalculation() && ($this->getStepValue() > 0)) {
            $stepValue = $this->getStepValue();
            $minValue  = floor($minValue / $stepValue) * $stepValue;
        }

        return $minValue;
    }

    public function getMax()
    {
        $maxValue = $this->getFilter()->getMaxValue() + 1;

        if ($this->isManualCalculation() && ($this->getStepValue() > 0)) {
            $stepValue = $this->getStepValue();
            $maxValue  = ceil($maxValue / $stepValue) * $stepValue;
        }

        return $maxValue;
    }

    /**
     * Check if price interval is manually set in the configuration
     *
     * @return bool
     */
    private function isManualCalculation()
    {
        $result      = false;
        $calculation = $this->_scopeConfig->getValue(PriceDataProvider::XML_PATH_RANGE_CALCULATION, ScopeInterface::SCOPE_STORE);

        if ($calculation === PriceDataProvider::RANGE_CALCULATION_MANUAL) {
            $result = true;
        }

        return $result;
    }

    /**
     * Retrieve the value for "Default Price Navigation Step".
     *
     * @return int
     */
    private function getStepValue()
    {
        $value = $this->_scopeConfig->getValue(PriceDataProvider::XML_PATH_RANGE_STEP, ScopeInterface::SCOPE_STORE);

        return (int) $value;
    }

    public function getFrom()
    {
        $filter = $this->getFilter();
        $currentValues = $this->getFilter()->getCurrentValue();
        if (isset($currentValues[0])) {
            return $currentValues[0];
        }
        return $this->getMinValue();
    }

    public function getTo()
    {
        $currentValues = $this->getFilter()->getCurrentValue();
        if (isset($currentValues[1])) {
            return $currentValues[1];
        }
        return $this->getMaxValue();
    }
}
