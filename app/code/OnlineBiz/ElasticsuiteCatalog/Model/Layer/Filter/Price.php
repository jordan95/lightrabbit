<?php
/**
 * Created by PhpStorm.
 * User: nguyenbach
 * Date: 23/12/2018
 * Time: 11:29
 */
namespace OnlineBiz\ElasticsuiteCatalog\Model\Layer\Filter;

class Price extends \Smile\ElasticsuiteCatalog\Model\Layer\Filter\Price
{
    /**
     * @var $urlBuilder  \Niks\LayeredNavigation\Model\Url\Builder
     */
    protected $urlBuilder;

    /**
     * @var \Magento\Catalog\Model\Layer\Filter\DataProvider\Price
     */
    protected $dataProvider;

    public function __construct(
        \Niks\LayeredNavigation\Model\Url\Builder $urlBuilder,
        \Magento\Catalog\Model\Layer\Filter\ItemFactory $filterItemFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\Layer $layer,
        \Magento\Catalog\Model\Layer\Filter\Item\DataBuilder $itemDataBuilder,
        \Magento\Catalog\Model\ResourceModel\Layer\Filter\Price $resource,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\Search\Dynamic\Algorithm $priceAlgorithm,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        \Magento\Catalog\Model\Layer\Filter\Dynamic\AlgorithmFactory $algorithmFactory,
        \Magento\Catalog\Model\Layer\Filter\DataProvider\PriceFactory $dataProviderFactory,
        \Smile\ElasticsuiteCore\Search\Request\Query\QueryFactory $queryFactory,
        array $data = [])
    {
        parent::__construct($filterItemFactory, $storeManager, $layer, $itemDataBuilder, $resource, $customerSession, $priceAlgorithm, $priceCurrency, $algorithmFactory, $dataProviderFactory, $queryFactory, $data);
        $this->urlBuilder = $urlBuilder;
        $this->dataProvider = $dataProviderFactory->create(['layer' => $this->getLayer()]);
    }

    /**
     * Get applied values
     *
     * @return array|bool
     */
    public function getCurrentValue()
    {
        $values = $this->urlBuilder->getValuesFromUrl($this->_requestVar);
        $filter = false;
        if ($values) {
            $filter = $values[0];
        }
        $filterParams = explode(',', $filter);
        return $this->dataProvider->validateFilter($filterParams[0]);
    }

    public function apply(\Magento\Framework\App\RequestInterface $request)
    {
        $this->applyToCollection($this->getLayer()->getProductCollection(), true);
        return $this;
    }

    public function applyToCollection($collection, $addFilter = false)
    {
        $values = $this->urlBuilder->getValuesFromUrl($this->_requestVar);
        $filter = false;
        if ($values) {
            $filter = $values[0];
        }

        $filterParams = explode(',', $filter);
        $filter = $this->getCurrentValue();
        if (!$filter) {
            return $this;
        }

        if ($addFilter) {
            $this->dataProvider->setInterval($filter);
            $priorFilters = $this->dataProvider->getPriorFilters($filterParams);
            if ($priorFilters) {
                $this->dataProvider->setPriorIntervals($priorFilters);
            }
        }

        list($from, $to) = $filter;

        $collection->addFieldToFilter(
            'price',
            ['from' => $from, 'to' =>  empty($to) || $from == $to ? $to : $to - self::PRICE_DELTA]
        );

        if ($addFilter) {
            $this->getLayer()->getState()->addFilter(
                $this->_createItem($this->_renderRangeLabel(empty($from) ? 0 : $from, $to), $filter)
            );
        }
        return $this;
    }
}