<?php
/**
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Smile ElasticSuite to newer
 * versions in the future.
 *
 * @category  Smile
 * @package   Smile\ElasticsuiteCatalog
 * @author    Aurelien FOUCRET <aurelien.foucret@smile.fr>
 * @copyright 2018 Smile
 * @license   Open Software License ("OSL") v. 3.0
 */

namespace OnlineBiz\ElasticsuiteCatalog\Model\Autocomplete\Product;

use Magento\Framework\ObjectManagerInterface;
use Magento\Catalog\Helper\Image as ImageHelper;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\Pricing\Render;
use Smile\ElasticsuiteCatalog\Model\Autocomplete\Product\AttributeConfig;

/**
 * Create an autocomplete item from a product.
 *
 * @category Smile
 * @package  Smile\ElasticsuiteCatalog
 * @author   Aurelien FOUCRET <aurelien.foucret@smile.fr>
 */
class ItemFactory extends \Smile\ElasticsuiteCatalog\Model\Autocomplete\Product\ItemFactory
{
    /**
     * @var ImageHelper
     */
    private $imageHelper;

    /**
     * @var \Magento\Framework\Pricing\Render
     */
    private $priceRenderer = null;

    /**
     * @var ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var array
     */
    private $attributes;

    public function __construct(ObjectManagerInterface $objectManager, ImageHelper $imageHelper, AttributeConfig $attributeConfig)
    {
        $this->attributes    = $attributeConfig->getAdditionalSelectedAttributes();
        $this->imageHelper   = $imageHelper;
        $this->objectManager = $objectManager;
        parent::__construct($objectManager, $imageHelper, $attributeConfig);
    }

    /**
     * {@inheritDoc}
     */
    public function create(array $data)
    {
        $data = $this->addProductData($data);
        unset($data['product']);

        return \Magento\Search\Model\Autocomplete\ItemFactory::create($data);
    }

    /**
     * Load product data and append them to the original data.
     *
     * @param array $data Autocomplete item data.
     *
     * @return array
     */
    private function addProductData($data)
    {
        $product = $data['product'];

        $productData = [
            'title' => $product->getName(),
            'image' => $this->getImageUrl($product),
            'url'   => $product->getProductUrl(),
            'price' => $this->renderProductPrice($product, \Magento\Catalog\Pricing\Price\FinalPrice::PRICE_CODE),
        ];

        foreach ($this->attributes as $attributeCode) {
            if ($product->hasData($attributeCode)) {
                $productData[$attributeCode] = $product->getData($attributeCode);
                if ($product->getResource()->getAttribute($attributeCode)->usesSource()) {
                    $productData[$attributeCode] = $product->getAttributeText($attributeCode);
                }
            }
        }

        $data = array_merge($data, $productData);

        return $data;
    }

    private function getImageUrl($product)
    {
        $this->imageHelper->init($product, self::AUTOCOMPLETE_IMAGE_ID);

        return $this->imageHelper->getUrl();
    }

    private function renderProductPrice(\Magento\Catalog\Model\Product $product, $priceCode)
    {
        $price = $product->getPriceInfo()->getPrice('regular_price')->getAmount()->getBaseAmount();
        return $price;
    }
}
