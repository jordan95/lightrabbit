<?php

namespace OnlineBiz\AddCompany\Block\Adminhtml\Quote;

/**
 * Adminhtml quotations quotes grid
 */
class Grid extends \Cart2Quote\Quotation\Block\Adminhtml\Quote\Grid
{
    /**
     * Apply sorting and filtering to collection
     *
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareCollection()
    {
        if ($this->getCollection()) {
            foreach ($this->getDefaultFilter() as $field => $value) {
                $this->getCollection()->addFieldToFilter($field, $value);
            }
        }
        $this->getCollection()->getSelect()->joinLeft(
            ['quote_address' => 'quote_address'],
            'main_table.quote_id = quote_address.quote_id',
            ['company'=>'company']
        );
        $this->getCollection()->addFieldToFilter('address_type',"billing");
        return parent::_prepareCollection();
    }
}
