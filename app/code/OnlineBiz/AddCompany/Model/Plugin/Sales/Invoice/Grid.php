<?php
namespace OnlineBiz\AddCompany\Model\Plugin\Sales\Invoice;
 
 
class Grid
{
 
    public static $table = 'sales_invoice_grid';
    public function afterSearch($intercepter, $collection)
    {
        if ($collection->getMainTable() === $collection->getConnection()->getTableName(self::$table)) {
            $where = $collection->getSelect()->getPart(\Magento\Framework\DB\Select::WHERE);
            $collection->getSelect()->setPart(\Magento\Framework\DB\Select::WHERE, $where);
            $collection->getSelect()->distinct(true);
        }
        return $collection;
 
 
    }
 
 
}