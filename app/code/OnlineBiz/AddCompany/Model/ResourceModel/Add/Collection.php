<?php
namespace OnlineBiz\AddCompany\Model\ResourceModel\Add;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

	/**
	 * Define resource model
	 *
	 * @return void
	 */
	protected function _construct()
	{
		$this->_init('OnlineBiz\AddCompany\Model\Add', 'OnlineBiz\AddCompany\Model\ResourceModel\Add');
	}

}