<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace OnlineBiz\AddCompany\Model\ResourceModel\Order\Grid;

use Magento\Framework\Data\Collection\Db\FetchStrategyInterface as FetchStrategy;
use Magento\Framework\Data\Collection\EntityFactoryInterface as EntityFactory;
use Magento\Framework\Event\ManagerInterface as EventManager;
use Psr\Log\LoggerInterface as Logger;

/**
 * Order grid collection
 */
class Collection extends \Magento\Sales\Model\ResourceModel\Order\Grid\Collection
{
    /**
     * Initialize dependencies.
     *
     * @param EntityFactory $entityFactory
     * @param Logger $logger
     * @param FetchStrategy $fetchStrategy
     * @param EventManager $eventManager
     * @param string $mainTable
     * @param string $resourceModel
     */

    protected function _initSelect()
    {
        parent::_initSelect();
        $this->getSelect()
                ->joinLeft(
                    ['co'=>'sales_order_address'],
                    "co.parent_id = main_table.entity_id",
                    [
                        'company' => 'co.company',
                        'address_type' => 'GROUP_CONCAT(DISTINCT co.address_type)'
                    ]
                )->where('co.address_type = (?)', 'billing')->group("main_table.entity_id");
        $this->addFilterToMap('entity_id', 'main_table.entity_id');
        $this->addFilterToMap('customer_id', 'main_table.customer_id');
           
    }
}
