<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace OnlineBiz\AddCompany\Model\ResourceModel\Order\Invoice\Grid;

use Magento\Framework\Data\Collection\Db\FetchStrategyInterface as FetchStrategy;
use Magento\Framework\Data\Collection\EntityFactoryInterface as EntityFactory;
use Magento\Framework\Event\ManagerInterface as EventManager;
use Magento\Sales\Ui\Component\DataProvider\Document;
use Psr\Log\LoggerInterface as Logger;

class Collection extends \Magento\Sales\Model\ResourceModel\Order\Invoice\Grid\Collection
{
    /**
     * @inheritdoc
     */
    protected $document = Document::class;
    
    /**
     * Initialize dependencies.
     *
     * @param EntityFactory $entityFactory
     * @param Logger $logger
     * @param FetchStrategy $fetchStrategy
     * @param EventManager $eventManager
     * @param string $mainTable
     * @param string $resourceModel
     */
    protected function _initSelect()
    {
        parent::_initSelect();
        $this->getSelect()
                ->joinLeft(
                    ['co'=>'sales_order_address'],
                    "co.parent_id = main_table.order_id",
                    [
                        'company' => 'co.company'
                    ]
                )->where('co.address_type = (?)', 'billing');
            $this->addFilterToMap('company', 'co.company');
        $this->addFilterToMap('entity_id', 'main_table.entity_id');
    }
}
