<?php
namespace OnlineBiz\AddCompany\Model\ResourceModel;


class Add extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
	
	public function __construct(
		\Magento\Framework\Model\ResourceModel\Db\Context $context
	)
	{
		parent::__construct($context);
	}
	
	protected function _construct()
	{
		$this->_init('sales_order_address', 'entity_id');
	}
	
}