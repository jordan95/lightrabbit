<?php
namespace OnlineBiz\AddCompany\Model;
class Add extends \Magento\Framework\Model\AbstractModel
{

	protected function _construct()
	{
		$this->_init('OnlineBiz\AddCompany\Model\ResourceModel\Add');
	}
}