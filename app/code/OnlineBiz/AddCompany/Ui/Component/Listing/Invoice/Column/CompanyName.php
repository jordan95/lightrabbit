<?php
namespace OnlineBiz\AddCompany\Ui\Component\Listing\Invoice\Column;
 
use \Magento\Sales\Api\InvoiceRepositoryInterface;
use \Magento\Framework\View\Element\UiComponent\ContextInterface;
use \Magento\Framework\View\Element\UiComponentFactory;
use \Magento\Ui\Component\Listing\Columns\Column;
use \Magento\Framework\Api\SearchCriteriaBuilder;
use \OnlineBiz\AddCompany\Model\AddFactory;
 
class CompanyName extends Column
{
 
    protected $_invoiceRepository;
    protected $_searchCriteria;
    protected $_addfactory;
 
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        InvoiceRepositoryInterface $invoiceRepository,
        SearchCriteriaBuilder $criteria,
        AddFactory $addFactory,
        array $components = [], array $data = [])
    {
        $this->_invoiceRepository = $invoiceRepository;
        $this->_searchCriteria  = $criteria;
        $this->_addfactory = $addFactory;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }
 
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $invoice  = $this->_invoiceRepository->get($item["entity_id"]);
 
                $order_id = $invoice->getOrderId();
 
                $collection = $this->_addfactory->create()->getCollection();
                $collection->addFieldToFilter('parent_id',$order_id);
 
                $data = $collection->getFirstItem();
 
 
 
                $item[$this->getData('name')] = $data->getCompany();
            }
        }
        return $dataSource;
    }
}