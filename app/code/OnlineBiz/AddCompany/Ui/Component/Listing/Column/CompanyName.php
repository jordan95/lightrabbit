<?php
namespace OnlineBiz\AddCompany\Ui\Component\Listing\Column;
 
use \Magento\Sales\Api\OrderRepositoryInterface;
use \Magento\Framework\View\Element\UiComponent\ContextInterface;
use \Magento\Framework\View\Element\UiComponentFactory;
use \Magento\Ui\Component\Listing\Columns\Column;
use \Magento\Framework\Api\SearchCriteriaBuilder;
use \OnlineBiz\AddCompany\Model\AddFactory;
 
class CompanyName extends Column
{
 
    protected $_orderRepository;
    protected $_searchCriteria;
    protected $_addfactory;
 
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        OrderRepositoryInterface $orderRepository,
        SearchCriteriaBuilder $criteria,
        AddFactory $addFactory,
        array $components = [], array $data = [])
    {
        $this->_orderRepository = $orderRepository;
        $this->_searchCriteria  = $criteria;
        $this->_addfactory = $addFactory;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }
 
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $order  = $this->_orderRepository->get($item["entity_id"]);
 
                $order_id = $order->getEntityId();
 
                $collection = $this->_addfactory->create()->getCollection();
                $collection->addFieldToFilter('parent_id',$order_id);
 
                $data = $collection->getFirstItem();
 
 
 
                $item[$this->getData('name')] = $data->getCompany();
            }
        }
        return $dataSource;
    }
}