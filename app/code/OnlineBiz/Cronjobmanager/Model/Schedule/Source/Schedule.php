<?php

namespace OnlineBiz\Cronjobmanager\Model\Schedule\Source;

class Schedule extends \EthanYehuda\CronjobManager\Model\Schedule\Source\Schedule
{
	public function toOptionArray() 
	{
		$cronJobs = $this->mergeCronGroups($this->cronConfig->getJobs());
		$options = [];
		foreach ($cronJobs as $key => $cron) {
            if(empty($cron['name'])){
                $cron['name'] = $key;
            }
			$option = [
				'value' => $cron['name'],
				'label' => $cron['name']
			];
			array_push($options, $option);
		}
		
		return $options;
	}

    private function mergeCronGroups($groups)
    {
        $merged = [];
        foreach ($groups as $group) {
            $merged = array_merge($merged, $group);
        }

        return $merged;
    }
}