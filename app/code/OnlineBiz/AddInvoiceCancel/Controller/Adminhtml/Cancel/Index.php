<?php
namespace OnlineBiz\AddInvoiceCancel\Controller\Adminhtml\Cancel;
 
use Magento\Sales\Model\Order\Invoice; 
class Index extends \Magento\Backend\App\Action
{
    protected $resultPageFactory = false;
    protected $_request;
    protected $_orderRepository;
    protected $_invoiceFactory;
    protected $_formKey;
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Sales\Model\OrderRepository $orderRepository,
        \Magento\Sales\Model\Order\InvoiceFactory $invoiceFactory,
        \Magento\Framework\Data\Form\FormKey $formKey  
    )
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->_orderRepository = $orderRepository;
        $this->_invoiceFactory = $invoiceFactory;
        $this->_formKey = $formKey;
    }

    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $order_id = $this->getRequest()->getParam('order_id');
        if(isset($order_id)){

            $order = $this->_orderRepository->get($order_id);
            $is_paid = false;
            foreach ($order->getInvoiceCollection() as $invoice)
            {
                $invoice_id = $invoice->getIncrementId();
                if($invoice->getState()==\OnlineBiz\ReceivableInvoices\Model\Order\Invoice::STATE_PAID){
                        $is_paid = true;
                        break;
                }
            }
            if(!$is_paid){
                foreach ($order->getInvoiceCollection() as $invoice)
                {
                    $invoice->cancel();
                    $invoice->save();
                    $order->save();
                }
                 $this->messageManager->addSuccess(__('You canceled the invoice.'));
                return $resultRedirect->setPath('sales/order/cancel/',['order_id' => $order_id]);
            }
            $this->messageManager->addNotice(__('Invoice was paid so can not cancel invoice.'));
            return $resultRedirect->setPath('*/*/'); 
        }else{
            return $resultRedirect->setPath('*/*/');
        }
        
    }


}