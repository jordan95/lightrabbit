<?php
namespace OnlineBiz\AddInvoiceCancel\Plugin\Backend\Block\Widget\Button\Toolbar;

use Magento\Backend\Block\Widget\Button\ButtonList;
use Magento\Backend\Block\Widget\Button\Toolbar as ToolbarContext;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\AbstractBlock;

class AddCancelButton
{
    public function __construct(
        Registry $coreRegistry
    ) {
        $this->coreRegistry = $coreRegistry;
    }

    /**
     * @param ToolbarContext $toolbar
     * @param AbstractBlock $context
     * @param ButtonList $buttonList
     * @return array
     */
    public function beforePushButtons(
        ToolbarContext $toolbar,
        \Magento\Framework\View\Element\AbstractBlock $context,
        \Magento\Backend\Block\Widget\Button\ButtonList $buttonList
    ) {
        if (!$context instanceof \Magento\Sales\Block\Adminhtml\Order\View) {
            return [$context, $buttonList];
        }

        $orderId = $this->getOrder()->getId();
        $enable_button = false;
        $invoice_details = $this->getOrder()->getInvoiceCollection();
        if($invoice_details){
            foreach ($invoice_details as $_invoice) {
                if($_invoice->getState()==\OnlineBiz\ReceivableInvoices\Model\Order\Invoice::STATE_PAID){
                    $enable_button = false;
                    break;
                }elseif($_invoice->getState()==\OnlineBiz\ReceivableInvoices\Model\Order\Invoice::STATE_UNPAID){
                    $enable_button = true;
                }
            }
            if($enable_button===true){
                $buttonList->add('cancel_invoice_order',
                    [
                        'label' => __('Cancel Invoice and Order'),
                        'onclick' => 'setLocation(\'' . $context->getUrl('cancelinvoiceorder/cancel/index/', ['order_id' => $orderId]) . '\')',
                        'class' => 'add-cancel-invoice-order',
                        'sort_order' => (count($buttonList->getItems()) + 1) * 11,
                    ]
                );  
            }
        }
        
        return [$context, $buttonList];
    }

    /**
     * @return \Magento\Sales\Model\Order
     */
    private function getOrder()
    {
        return $this->coreRegistry->registry('sales_order');
    }
}