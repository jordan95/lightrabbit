<?php
namespace OnlineBiz\CustomerTrade\Plugin;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Customer\Helper\View as CustomerViewHelper;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Exception\LocalizedException;
use Magento\Customer\Model\Session;
use Magento\Customer\Model\CustomerRegistry;
use OnlineBiz\CustomerTrade\Setup\UpgradeData as CustomerTradeUpgradeData;

/**
* Disable customer email depending on settings value.
*
* @SuppressWarnings(PHPMD.UnusedFormalParameter)
*/
class CustomerEmailNotificationPlugin
{
  /**
  * @var Session
  */
  protected $session;
  /**
  * @var ScopeConfigInterface
  */
  public $scopeConfig;

  /**
  * @var StoreManagerInterface
  */
  private $storeManager;

  /**
  * @var CustomerRegistry
  */
  private $customerRegistry;

  /**
  * @var CustomerViewHelper
  */
  protected $customerViewHelper;

  /**
  * @var DataObjectProcessor
  */
  protected $dataProcessor;

  /**
   * @var TransportBuilder
   */
  private $transportBuilder;


  /**
  * CustomerEmailNotificationPlugin constructor.
  * @param ScopeConfigInterface $scopeConfig
  * @param StoreManagerInterface $storeManager
  * @param Session $customerSession
  * @param CustomerRegistry $customerRegistry
  * @param CustomerViewHelper $customerViewHelper
  * @param DataObjectProcessor $dataProcessor
  * @param DataObjectProcessor $dataProcessor
  * @param DataObjectProcessor $dataProcessor
  * @param TransportBuilder $transportBuilder
  */
  public function __construct(
    ScopeConfigInterface $scopeConfig,
    StoreManagerInterface $storeManager,
    Session $customerSession,
    CustomerRegistry $customerRegistry,
    CustomerViewHelper $customerViewHelper,
    DataObjectProcessor $dataProcessor,
    TransportBuilder $transportBuilder
  ) {
    $this->scopeConfig  = $scopeConfig;
    $this->storeManager = $storeManager;
    $this->session = $customerSession;
    $this->customerRegistry = $customerRegistry;
    $this->customerViewHelper = $customerViewHelper;
    $this->dataProcessor = $dataProcessor;
    $this->transportBuilder = $transportBuilder;
  }

  /**
  * @param \Magento\Customer\Model\EmailNotification $emailNotification
  * @param callable $proceed
  * @param \Magento\Customer\Api\Data\CustomerInterface $customer
  * @param string $type
  * @param string $backUrl
  * @param string|int $storeId
  * @param string|null $sendemailStoreId
  *
  * @return mixed
  */
  public function aroundNewAccount(
    \Magento\Customer\Model\EmailNotification $emailNotification,
    callable $proceed,
    CustomerInterface $customer,
    $type = \Magento\Customer\Model\EmailNotificationInterface::NEW_ACCOUNT_EMAIL_REGISTERED,
    $backUrl = '',
    $storeId = 0,
    $sendemailStoreId = null
  ) {

    if( $customer->getGroupId() == CustomerTradeUpgradeData::TRADE_GROUP_ID){
      return $this->newTradeAccount($customer, $type, $backUrl, $storeId, $sendemailStoreId);
    } else {
      return $proceed($customer, $type, $backUrl, $storeId, $sendemailStoreId);
    }
  }

  /**
  * Send email with new account related information
  *
  * @param CustomerInterface $customer
  * @param string $type
  * @param string $backUrl
  * @param string $storeId
  * @param string $sendemailStoreId
  * @return void
  * @throws LocalizedException
  */
  private function newTradeAccount(
    CustomerInterface $customer,
    $type = \Magento\Customer\Model\EmailNotification::NEW_ACCOUNT_EMAIL_REGISTERED,
    $backUrl = '',
    $storeId = 0,
    $sendemailStoreId = null
  ) {
    $types = \Magento\Customer\Model\EmailNotification::TEMPLATE_TYPES;

    if (!isset($types[$type])) {
      throw new LocalizedException(__('Please correct the transactional account email type.'));
    }
    if (!$storeId) {
      $storeId = $this->getWebsiteStoreId($customer, $sendemailStoreId);
    }

    $store = $this->storeManager->getStore($customer->getStoreId());

    $customerEmailData = $this->getFullCustomerObject($customer);
    $this->sendEmailTemplate(
      $customer,
      $types[$type],
      \Magento\Customer\Model\EmailNotification::XML_PATH_REGISTER_EMAIL_IDENTITY,
      ['customer' => $customerEmailData, 'back_url' => $backUrl, 'store' => $store],
      $storeId
    );
  }

  /**
  * Create an object with data merged from Customer and CustomerSecure
  *
  * @param CustomerInterface $customer
  * @return \Magento\Customer\Model\Data\CustomerSecure
  */
  private function getFullCustomerObject($customer)
  {
    // No need to flatten the custom attributes or nested objects since the only usage is for email templates and
    // object passed for events
    $mergedCustomerData = $this->customerRegistry->retrieveSecureData($customer->getId());
    $customerData = $this->dataProcessor->buildOutputDataArray($customer, CustomerInterface::class);
    $mergedCustomerData->addData($customerData);
    $mergedCustomerData->setData('name', $this->customerViewHelper->getCustomerName($customer));
    $mergedCustomerData->setData('is_trade', true);
    $mergedCustomerData->setData('password', $this->session->getData(CustomerTradeUpgradeData::TRADE_PASSWORD_KEY));
    $this->session->unsetData(CustomerTradeUpgradeData::TRADE_PASSWORD_KEY);
    return $mergedCustomerData;
  }


  /**
  * Get either first store ID from a set website or the provided as default
  *
  * @param CustomerInterface $customer
  * @param int|string|null $defaultStoreId
  * @return int
  */
  private function getWebsiteStoreId($customer, $defaultStoreId = null)
  {
    if ($customer->getWebsiteId() != 0 && empty($defaultStoreId)) {
      $storeIds = $this->storeManager->getWebsite($customer->getWebsiteId())->getStoreIds();
      $defaultStoreId = reset($storeIds);
    }
    return $defaultStoreId;
  }

  /**
  * Send corresponding email template
  *
  * @param CustomerInterface $customer
  * @param string $template configuration path of email template
  * @param string $sender configuration path of email identity
  * @param array $templateParams
  * @param int|null $storeId
  * @param string $email
  * @return void
  */
  private function sendEmailTemplate(
    $customer,
    $template,
    $sender,
    $templateParams = [],
    $storeId = null,
    $email = null
  ) {
    $templateId = $this->scopeConfig->getValue($template, 'store', $storeId);
    if ($email === null) {
        $email = $customer->getEmail();
    }
    $transport = $this->transportBuilder->setTemplateIdentifier($templateId)
      ->setTemplateOptions(['area' => 'frontend', 'store' => $storeId])
      ->setTemplateVars($templateParams)
      ->setFrom($this->scopeConfig->getValue($sender, 'store', $storeId))
      ->addTo($email, $this->customerViewHelper->getCustomerName($customer))
      ->getTransport();
    $transport->sendMessage();
  }


}
