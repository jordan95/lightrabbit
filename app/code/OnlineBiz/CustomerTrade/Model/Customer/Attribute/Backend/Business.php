<?php
namespace OnlineBiz\CustomerTrade\Model\Customer\Attribute\Backend;

class Business extends \Magento\Eav\Model\Entity\Attribute\Source\Table
{

  /**
  * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\CollectionFactory $attrOptionCollectionFactory
  * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory $attrOptionFactory
  */
  public function __construct(
    \Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\CollectionFactory $attrOptionCollectionFactory,
    \Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory $attrOptionFactory
  ) {
    parent::__construct($attrOptionCollectionFactory, $attrOptionFactory);
  }

  /**
  * Get all options
  *
  * @return array
  */
  public function getAllOptions()
  {
    $this->_options = [
      ['label' => 'Choose Option..', 'value' => ''],
      ['label' => 'Electrical Installer', 'value' => 1],
      ['label' => 'Facilities Management', 'value' => 2],
      ['label' => 'Wholesaler', 'value' => 3],
      ['label' => 'Specifier/Designer', 'value' => 4],
      ['label' => 'Builder', 'value' => 5],
      ['label' => 'Other Business', 'value' => 6]
    ];
    return $this->_options;

  }
}
