<?php
namespace OnlineBiz\CustomerTrade\Block;

use OnlineBiz\CustomerTrade\Setup\UpgradeData;
use Magento\Customer\Model\Customer;
use Magento\Customer\Model\Url as CustomerUrl;

class Register extends \Magento\Customer\Block\Form\Register
{


  public function getListBusiness()
  {
    $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    $eavConfig = $objectManager->get('\Magento\Eav\Model\Config');
    $attribute = $eavConfig->getAttribute(Customer::ENTITY, UpgradeData::BUSINESS_CODE);
    $options   = $attribute->getSource()->getAllOptions();
    unset($options[0]);
    $results = array();
    foreach($options as $option) {
      $results[$option['value']] = $option['label'];
    }
    return $results;
  }

  /**
  * Retrieve customer register form post url
  *
  * @return string
  */
  public function getFormActionUrl()
  {
    return $this->getUrl('*/account/createpost');
  }


}
