<?php
namespace OnlineBiz\CustomerTrade\Controller\Index;

use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Action;

class Index extends Action
{
  /**
  * @var Session
  */
  protected $session;

  protected $_resultPageFactory;

  public function __construct( Context $context, CustomerSession $customerSession, PageFactory $resultPageFactory )
  {
    $this->session = $customerSession;
    $this->_resultPageFactory = $resultPageFactory;
    parent::__construct($context);
  }

  public function execute()
  {
    if ($this->session->isLoggedIn()) {
      /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
      $resultRedirect = $this->resultRedirectFactory->create();
      $resultRedirect->setPath('customer/account/');
      return $resultRedirect;
    }
    $resultPage = $this->_resultPageFactory->create();
    return $resultPage;
  }
}
