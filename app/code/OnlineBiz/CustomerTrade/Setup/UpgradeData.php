<?php
/* file: app/code/OnlineBiz/CustomerTrade/Setup/InstallData.php*/

namespace OnlineBiz\CustomerTrade\Setup;

use Magento\Eav\Model\Config;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Customer\Model\Customer;
use Magento\Customer\Model\CustomerFactory;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Enrico69\Magento2CustomerActivation\Setup\InstallData as Magento2CustomerActivationInstallData;
use Magento\Framework\Exception\CouldNotSaveException;
use Psr\Log\LoggerInterface;

class UpgradeData implements UpgradeDataInterface
{

  protected $_customer;
  protected $_customerFactory;

  /**
  * @var LoggerInterface
  */
  protected $logger;

  /**
  * @var AttributeSetFactory
  */
  protected $attributeSetFactory;

  /**
  * Customer setup factory
  *
  * @var CustomerSetupFactory
  */
  protected $customerSetupFactory;

  /**
  * @var \Magento\Customer\Api\CustomerRepositoryInterface
  */
  protected $customerRepository;

  const TRADE_GROUP_ID = 6;
  const BUSINESS_CODE  = 'business';
  const TRADE_PASSWORD_KEY  = 'trade_password';
  const TRADE_PASSWORD_PREFIX  = 'Trade';

  /**
  * InstallData constructor.
  * @param \Magento\Customer\Setup\CustomerSetupFactory $customerSetupFactory
  * @param \Magento\Eav\Model\Entity\Attribute\SetFactory $attributeSetFactory
  */
  public function __construct( LoggerInterface $logger, Config $eavConfig, CustomerFactory $customerFactory, Customer $customers, AttributeSetFactory $attributeSetFactory, CustomerSetupFactory $customerSetupFactory, CustomerRepositoryInterface $customerRepository )
  {
    $this->logger    = $logger;
    $this->eavConfig = $eavConfig;
    $this->_customer = $customers;
    $this->_customerFactory = $customerFactory;
    $this->attributeSetFactory  = $attributeSetFactory;
    $this->customerSetupFactory = $customerSetupFactory;
    $this->customerRepository   = $customerRepository;
  }

  /**
   * @param \Magento\Framework\Setup\ModuleDataSetupInterface $setup
   * @param \Magento\Framework\Setup\ModuleContextInterface $context
   */
  public function upgrade( ModuleDataSetupInterface $setup, ModuleContextInterface $context )
  {
    $installer = $setup;
    $installer->startSetup();
      try {
        if (version_compare($context->getVersion(), '0.0.2') < 0) {
          $this->upgradeToZeroZeroTwo($setup);
        }
        if (version_compare($context->getVersion(), '0.0.3') < 0) {
          $this->upgradeToZeroZeroThree($setup);
        }
        if (version_compare($context->getVersion(), '0.0.4') < 0) {
          $this->upgradeToZeroZeroFour($setup);
        }
        if (version_compare($context->getVersion(), '0.0.5') < 0) {
          $this->upgradeToZeroZeroFive($setup);
        }
      } catch (Exception $e) {
        throw $e;
      }

    $installer->endSetup();
  }

  /**
   * @param \Magento\Framework\Setup\ModuleDataSetupInterface $setup
   */
  protected function upgradeToZeroZeroTwo($setup)
  {
    /** @var CustomerSetup $customerSetup */
    $businessAttribute = $this->eavConfig->getAttribute(Customer::ENTITY, self::BUSINESS_CODE);
    if ($businessAttribute->getId()) {
      $businessAttribute->setSourceModel('OnlineBiz\CustomerTrade\Model\Customer\Attribute\Backend\Business')->save();
    } else {

      /** @var CustomerSetup $customerSetup */
      $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);

      $attributesInfo = [
        self::BUSINESS_CODE =>
          [
            'type'         => 'int',
            'input'        => 'select',
            'label'        => 'Business',
            'source_model' => 'OnlineBiz\CustomerTrade\Model\Customer\Attribute\Backend\Business',
            'global'       => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,
            'required'     => false,
            'visible'      => true,
            'user_defined' => true,
            'default'      => false,
            'system'       => 0,
            'visible_on_front' => true
          ]
      ];

      $customerEntity = $customerSetup->getEavConfig()->getEntityType(Customer::ENTITY);
      $attributeSetId = $customerEntity->getDefaultAttributeSetId();

      /** @var $attributeSet AttributeSet */
      $attributeSet = $this->attributeSetFactory->create();
      $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);

      foreach ($attributesInfo as $attributeCode => $attributeParams) {
        $customerSetup->addAttribute(Customer::ENTITY, $attributeCode, $attributeParams);
      }

      $businessAttribute = $customerSetup->getEavConfig()->getAttribute(Customer::ENTITY, self::BUSINESS_CODE);
      $businessAttribute->addData([
          'attribute_set_id'    => $attributeSetId,
          'attribute_group_id'  => $attributeGroupId,
          'used_in_forms'       => ['adminhtml_customer', 'checkout_register', 'customer_account_create', 'customer_account_edit']
      ]);
      $businessAttribute->save();
    }
    $this->setActiveForCustomer($setup);
  }

  protected function setActiveForCustomer($setup)
  {
    $customers = $this->_customerFactory->create()->getCollection();
    $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
    $resource   = $objectManager->get('Magento\Framework\App\ResourceConnection');
    $connection = $resource->getConnection();
    $tableName  = $resource->getTableName('customer_entity_int'); //gives table name with prefix
    /** @var CustomerSetup $customerSetup */
    $customerSetup   = $this->customerSetupFactory->create(['setup' => $setup]);
    $activeAttribute = $customerSetup->getEavConfig()->getAttribute(Customer::ENTITY, Magento2CustomerActivationInstallData::CUSTOMER_ACCOUNT_ACTIVE);
    $attributeID     = $activeAttribute->getAttributeId() ? $activeAttribute->getAttributeId() : 444;

    foreach ($customers as $customer) {
      try {
        $updatedCustomer = $this->customerRepository->getById($customer->getId());
        $isActive   = $updatedCustomer->getCustomAttribute(Magento2CustomerActivationInstallData::CUSTOMER_ACCOUNT_ACTIVE);
        if (!$isActive) {
          //Insert Data into table
          $sql = "INSERT IGNORE INTO `" . $tableName . "` (`value_id`, `attribute_id`, `entity_id`, `value`) VALUES (NULL, '" . $attributeID . "', '" . $customer->getId() . "', '1')";
          $connection->query($sql);
        }
      } catch (\Exception $ex) {
        $e = new CouldNotSaveException(__($ex->getMessage()), $ex);
        $this->logger->error(__FILE__ . ' : ' . $ex->getMessage());
        $this->logger->error(__FILE__ . ' : ' . $ex->getTraceAsString());
      }
    }
  }

  /**
  * @param \Magento\Framework\Setup\ModuleDataSetupInterface $setup
  */
  protected function upgradeToZeroZeroThree($setup)
  {
    $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
    $resource   = $objectManager->get('Magento\Framework\App\ResourceConnection');
    $connection = $resource->getConnection();
    $tableName  = $resource->getTableName('eav_entity_attribute'); //gives table name with prefix
    /** @var CustomerSetup $customerSetup */
    $customerSetup     = $this->customerSetupFactory->create(['setup' => $setup]);
    $businessAttribute = $customerSetup->getEavConfig()->getAttribute(Customer::ENTITY, self::BUSINESS_CODE);
    $attributeID       = $businessAttribute->getAttributeId() ? $businessAttribute->getAttributeId() : 309;
    try {
      $sql = "INSERT IGNORE INTO `" . $tableName . "` (`entity_attribute_id`, `entity_type_id`, `attribute_set_id`, `attribute_group_id`, `attribute_id`, `sort_order`) VALUES (NULL, '1', '1', '1', '" . $attributeID . "', '26')";
      $connection->query($sql);
    } catch (Exception $e) {
      $e = new CouldNotSaveException(__($ex->getMessage()), $ex);
      $this->logger->error(__FILE__ . ' : ' . $ex->getMessage());
      $this->logger->error(__FILE__ . ' : ' . $ex->getTraceAsString());
    }
  }

  /**
  * @param \Magento\Framework\Setup\ModuleDataSetupInterface $setup
  */
  protected function upgradeToZeroZeroFour($setup)
  {
    $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
    $resource   = $objectManager->get('Magento\Framework\App\ResourceConnection');
    $connection = $resource->getConnection();
    $tableName  = $resource->getTableName('url_rewrite'); //gives table name with prefix

    try {
      $sql = "DELETE FROM `{$tableName}` WHERE `{$tableName}`.`request_path` = 'trade';";
      $connection->query($sql);
    } catch (Exception $e) {
      $e = new CouldNotSaveException(__($ex->getMessage()), $ex);
      $this->logger->error(__FILE__ . ' : ' . $ex->getMessage());
      $this->logger->error(__FILE__ . ' : ' . $ex->getTraceAsString());
    }
  }

  /**
  * @param \Magento\Framework\Setup\ModuleDataSetupInterface $setup
  */
  protected function upgradeToZeroZeroFive($setup)
  {
    try {
      /** @var CustomerSetup $customerSetup */
      $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);
      $newAttribute = $customerSetup->getEavConfig()->getAttribute(Customer::ENTITY, self::BUSINESS_CODE);
      $newAttribute->setData('is_used_in_grid', 1);
      $newAttribute->setData('is_visible_in_grid', 1);
      $newAttribute->setData('is_filterable_in_grid', 1);
      $newAttribute->setData('is_system', 0);
      $newAttribute->save();
    } catch (Exception $e) {
      $e = new CouldNotSaveException(__($ex->getMessage()), $ex);
      $this->logger->error(__FILE__ . ' : ' . $ex->getMessage());
      $this->logger->error(__FILE__ . ' : ' . $ex->getTraceAsString());
    }
  }
}
