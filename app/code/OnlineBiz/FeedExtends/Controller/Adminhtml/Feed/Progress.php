<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-feed
 * @version   1.0.68
 * @copyright Copyright (C) 2018 Mirasvit (https://mirasvit.com/)
 */


namespace OnlineBiz\FeedExtends\Controller\Adminhtml\Feed;

class Progress extends \Mirasvit\Feed\Controller\Adminhtml\Feed\Progress
{

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
      //increase the max execution time
      @ini_set('max_execution_time', -1);
      //memory_limit
      @ini_set('memory_limit', '-1');
      parent::execute();
    }
}
