<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-feed
 * @version   1.0.68
 * @copyright Copyright (C) 2018 Mirasvit (https://mirasvit.com/)
 */


namespace OnlineBiz\FeedExtends\Model\Feed;
use Mirasvit\Feed\Model\Feed;
/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Exporter extends \Mirasvit\Feed\Model\Feed\Exporter
{


  /**
   * Export feed via browser (few iterations)
   *
   * @param Feed $feed
   * @return string
   */
  public function export(Feed $feed)
  {
    //increase the max execution time
    @ini_set('max_execution_time', -1);
    //memory_limit
    @ini_set('memory_limit', '-1');
    register_shutdown_function([$this, 'onShutdown'], $feed);

    $this->startEmulation($feed);

    $handler = $this->getHandler($feed);
    $handler->setFilename($feed->getFilename());

    $handler->execute();

    $this->updateFeed($feed, $handler);

    $this->stopEmulation();

    return $handler->getStatus();
  }


  private function startEmulation(Feed $feed)
  {
      if (!$this->appEmulation) {
          $this->appEmulation = $this->appEmulationFactory->create();
      }

      $this->appEmulation->startEnvironmentEmulation($feed->getStore()->getId());
      $_SERVER['FEED_STORE_ID'] = $feed->getStore()->getId();
  }

  private function stopEmulation()
  {
    $this->appEmulation->stopEnvironmentEmulation();
  }
}
