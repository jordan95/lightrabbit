<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-feed
 * @version   1.0.68
 * @copyright Copyright (C) 2018 Mirasvit (https://mirasvit.com/)
 */


namespace OnlineBiz\FeedExtends\Model\Rule\Condition;

/**
 * @SuppressWarnings(PHPMD)
 * @codingStandardsIgnoreFile
 * @method string getAttribute()
 */
class Product extends \Mirasvit\Feed\Model\Rule\Condition\Product
{


  /**
   * Validate product attrbute value for condition.
   *
   * @param \Magento\Framework\Model\AbstractModel $object
   *
   * @return bool
   */
  public function validate(\Magento\Framework\Model\AbstractModel $object)
  {
    /** @var \Magento\Catalog\Model\Product $object */
    $attrCode = $this->getAttribute();

    switch ($attrCode) {
      case 'is_salable':
        $value = $object->isSalable();
        $object->setIsSalable($value);

        return $this->validateAttribute($value);
        break;

      case 'status_parent':
        $value = $object->getStatus();

        return $this->validateAttribute($value);
        break;

      case 'image':
      case 'small_image':
      case 'thumbnail':
        $value = $object->getData($attrCode);
        if ('' === $value || 'no_selection' === $value) {
          $value = null;
        }

        return $this->validateAttribute($value);
        break;

      case 'category_ids':
        $catIds = array_merge($object->getAvailableInCategories(), $object->getCategoryIds());
        return $this->validateAttribute($catIds);
        break;

      case 'qty':
        if ($object->getTypeId() == 'configurable') {
          $totalQty = 0;
          $children = $this->getChildrenInStock($object);
          foreach ($children as $child) {
            # if product enabled
            if ($child->getStatus() == 1) {
              $totalQty += $child->getQty();
            }
          }

          return $this->validateAttribute($totalQty);
        } else {
          $stockItem = $this->stockItemFactory->create()->load($object->getId(), 'product_id');

          return $this->validateAttribute($stockItem->getQty());
        }

        break;

      case 'children_count':
        $parent = $this->productResolver->getParent($object);
        if ($parent->getTypeId() == 'configurable') {
          $stockQty = 0;
          $childrenCollection = $this->getChildrenInStock($parent);
          if (count($childrenCollection) > 0) {
            foreach ($childrenCollection as $child) {
              if ($child->isSalable() === true) {
                $stockQty++;
              }
            }
          } else {
            return false;
          }

          return $this->validateAttribute($stockQty);
        } else {
          return true;
        }

        break;

      case 'is_in_stock':
        $stockItem = $this->stockItemFactory->create()->load($object->getId(), 'product_id');
        $stockStatus = 0;
        if ($stockItem->getIsInStock()) {
          $stockStatus = 1;
        }

        return $this->validateAttribute($stockStatus);
        break;

      case 'manage_stock':
        $stockItem = $this->stockItemFactory->create()->load($object->getId(), 'product_id');
        $m = 0;
        if ($stockItem->getManageStock()) {
          $m = 1;
        }

        return $this->validateAttribute($m);
        break;

      case 'image_size':
      case 'small_image_size':
      case 'thumbnail_size':
        $imageCode = str_replace('_size', '', $attrCode);

        $imagePath = $object->getData($imageCode);
        $path = $this->filesystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA)->getAbsolutePath() . '/catalog/product' . $imagePath;

        $size = 0;
        if (file_exists($path) && is_file($path)) {
          $size = filesize($path);
        }

        return $this->validateAttribute($size);
        break;

      case 'php':
        $object = $object->load($object->getId());
        extract($object->getData());
        $expr = 'return ' . $this->getValue() . ';';
        $value = eval($expr);

        if ($this->getOperator() == '==') {
          return $value;
        } else {
          return !$value;
        }

        break;
      case 'price':
        if ($object->getTypeId() == \Magento\Catalog\Model\Product\Type::TYPE_BUNDLE) {
          return true;
        } else {
          return \Magento\Rule\Model\Condition\AbstractCondition::validate($object);
        }
        break;

      default:
        if (!isset($this->_entityAttributeValues[$object->getId()])) {
          $attr = $object->getResource()->getAttribute($attrCode);

          if ($attr && $attr->getBackendType() == 'datetime' && !is_int($this->getValue())) {
            $this->setValue(strtotime($this->getValue()));
            $value = strtotime($object->getData($attrCode));

            return $this->validateAttribute($value);
          }

          if ($attr && $attr->getFrontendInput() == 'multiselect') {
            $value = $object->getData($attrCode);
            $value = strlen($value) ? explode(',', $value) : [];

            return $this->validateAttribute($value);
          }

          return \Magento\Rule\Model\Condition\AbstractCondition::validate($object);
        } else {
          $result = false; // any valid value will set it to TRUE
          $oldAttrValue = $object->hasData($attrCode) ? $object->getData($attrCode) : null;
          foreach ($this->_entityAttributeValues[$object->getId()] as $storeId => $value) {
            $attr = $object->getResource()->getAttribute($attrCode);
            if ($attr && $attr->getBackendType() == 'datetime') {
              $value = strtotime($value);
            } elseif ($attr && $attr->getFrontendInput() == 'multiselect') {
              $value = strlen($value) ? explode(',', $value) : [];
            }

            $object->setData($attrCode, $value);
            $result |= \Magento\Rule\Model\Condition\AbstractCondition::validate($object);

            if ($result) {
              break;
            }
          }

          if (is_null($oldAttrValue)) {
            $object->unsetData($attrCode);
          } else {
            $object->setData($attrCode, $oldAttrValue);
          }

          return (bool)$result;
        }
        break;
    }
  }


}
