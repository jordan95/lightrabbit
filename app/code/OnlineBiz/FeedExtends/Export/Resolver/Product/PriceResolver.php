<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-feed
 * @version   1.0.68
 * @copyright Copyright (C) 2018 Mirasvit (https://mirasvit.com/)
 */


namespace OnlineBiz\FeedExtends\Export\Resolver\Product;

class PriceResolver extends \Mirasvit\Feed\Export\Resolver\Product\PriceResolver
{

  /**
   * Price
   *
   * @param Product $product
   * @return float
   */
  public function getPrice($product)
  {
    if ($product->getTypeId() == \Magento\Catalog\Model\Product\Type::TYPE_BUNDLE) {
      return $product->getPriceInfo()->getPrice('regular_price')->getAmount()->getValue();
    }
    return $product->getPrice();
  }


}