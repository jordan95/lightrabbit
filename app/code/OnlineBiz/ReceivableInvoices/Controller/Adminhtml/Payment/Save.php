<?php


namespace OnlineBiz\ReceivableInvoices\Controller\Adminhtml\Payment;

use Magento\Framework\Exception\LocalizedException;

class Save extends \Magento\Backend\App\Action
{

    protected $dataPersistor;
    protected $resultPageFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor,
          \Magento\Framework\App\ResourceConnection $resource,
          \Magento\Sales\Model\ResourceModel\Order\Invoice\Grid\CollectionFactory $invoiceCollection,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->resultPageFactory = $resultPageFactory;
        $this->invoiceCollection = $invoiceCollection;
        $this->_resource      = $resource;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();

            $model = $this->_objectManager->create('OnlineBiz\ReceivableInvoices\Model\PaymentGrid');
           
            $model->setData($data);
            
            $connection = $this->_resource->getConnection();
            $total = (float)$data['total'];

            $invoice_id = $data['invoice_id'];
            $invoice = $this->invoiceCollection->create()
                        ->addfieldtofilter('increment_id', array('eq' => $invoice_id));
            $invoice_data = $invoice->getData();
            $invoice_received = $invoice_data[0]['cod_total_received'];
            $grand_total =  $invoice_data[0]['grand_total'];
            

            $received = $invoice_received + $total;
            $pending = $grand_total - $received;

        
           
           
            if($pending > 0 ){
                $query =  'UPDATE sales_invoice_grid SET cod_total_received = '.$received.',cod_total_pending ='.$pending.' WHERE increment_id ='.$invoice_id;
                $query1 =  'UPDATE sales_invoice SET cod_total_received = '.$received.',cod_total_pending ='.$pending.' WHERE increment_id ='.$invoice_id;
              }
            else {
                $query =  'UPDATE sales_invoice_grid SET state = 2 , cod_total_received = '.$received.',cod_total_pending ='.$pending.' WHERE increment_id ='.$invoice_id;
                $query1 =  'UPDATE sales_invoice SET state = 2 , cod_total_received = '.$received.',cod_total_pending ='.$pending.' WHERE increment_id ='.$invoice_id;
              }
            $connection->query($query);
              $connection->query($query1);
              $model->save();
               $event_data = array('invoice_id' => $invoice_id , 'received' => $received);
            $this->_eventManager->dispatch('onlinebiz_receivable_payment', ['data' => $event_data]);
          

    }
}