<?php
namespace OnlineBiz\ReceivableInvoices\Observer;

class Paymentsave implements \Magento\Framework\Event\ObserverInterface
{
  /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Directory\Model\CurrencyFactory
     */
    protected $currencyFactory;

    /**
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Directory\Model\CurrencyFactory $currencyFactory
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Directory\Model\CurrencyFactory $currencyFactory
    ){
        $this->storeManager = $storeManager;
        $this->currencyFactory = $currencyFactory;
    }

    /**
     * Converts the amount value from one currency to another.
     * If the $currencyCodeFrom is not specified the current currency will be used.
     * If the $currencyCodeTo is not specified the base currency will be used.
     * 
     * @param float $amountValue like 13.54
     * @param string|null $currencyCodeFrom like 'USD'
     * @param string|null $currencyCodeTo like 'BYN'
     * @return float
     */
    public function convert($amountValue, $currencyCodeFrom = null, $currencyCodeTo = null)
    {
        /**
         * If is not specified the currency code from which we want to convert - use current currency
         */
        if (!$currencyCodeFrom) {
            $currencyCodeFrom = $this->storeManager->getStore()->getCurrentCurrency()->getCode();
        }

        /**
         * If is not specified the currency code to which we want to convert - use base currency
         */
        if (!$currencyCodeTo) {
            $currencyCodeTo = $this->storeManager->getStore()->getBaseCurrency()->getCode();
        }

        /**
         * Do not convert if currency is same
         */
        if ($currencyCodeFrom == $currencyCodeTo) {
            return $amountValue;
        }

        /** @var float $rate */
        // Get rate
        $rate = $this->currencyFactory->create()->load($currencyCodeFrom)->getAnyRate($currencyCodeTo);
        // Get amount in new currency
        $amountValue = $amountValue * $rate;

        return $amountValue;
    }
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
       $data = $observer->getData('data');
       $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
         $order = $objectManager->create('Magento\Sales\Model\Order\Invoice')->loadByIncrementId($data['invoice_id'])->getOrder();
         $base_currency_code =  $order->getBaseCurrencyCode();
         $current_currency_code  = $order->getOrderCurrencyCode();
        $base_total_paid = $this->convert($data['received'],$current_currency_code,$base_currency_code);

            $order->setTotalPaid($data['received']);
            $order->setBaseTotalPaid($base_total_paid);
            $order->save();
       return $this;
    }
}