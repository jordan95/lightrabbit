<?php
namespace OnlineBiz\ReceivableInvoices\Block\Adminhtml\Payment;

use Magento\Sales\Model\ResourceModel\Order\Invoice\Collection as InvoiceCollection;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $moduleManager;


    protected $_testFactory;

    protected $_status;


    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Framework\Module\Manager $moduleManager,
        \OnlineBiz\ReceivableInvoices\Model\ResourceModel\PaymentGrid\CollectionFactory $paymentCollection,
        \Magento\Framework\App\Config\ScopeConfigInterface $appConfigScopeConfigInterface,
        \Magento\Payment\Model\Config $paymentModelConfig,
        \Magento\Store\Model\ResourceModel\Store\CollectionFactory $storeCollectionFactory,
        array $data = []
    ) {
        $this->moduleManager = $moduleManager;
        $this->paymentCollection = $paymentCollection;
        $this->_appConfigScopeConfigInterface = $appConfigScopeConfigInterface;
        $this->_paymentModelConfig = $paymentModelConfig;
        $this->storeCollectionFactory = $storeCollectionFactory;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('listsGrid');
        $this->setDefaultSort('payment_grid_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        
        $this->setVarNameFilter('lists_filter');
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = $this->paymentCollection->create();
        $collection->getSelect()->joinLeft(
            ['sales_invoice' => 'sales_invoice'],
            'main_table.invoice_id = sales_invoice.increment_id',
            ['order_currency_code' => "order_currency_code"]
        );
       
        $this->setCollection($collection);

        parent::_prepareCollection();
        return $this;
    }

    /**
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'id',
            [
                'header' => __('ID'),
                'type' => 'text',
                'index' => 'payment_grid_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id',
                'name'=>'id'
            ]
        );
        $this->addColumn(
            'invoice_id',
            [
            	'header' => __('Invoice Id'),
                 'type' => 'text',
                'index' => 'invoice_id',
                'name'=>'invoice_id'
            ]
        );

        
           $this->addColumn(
            'customer_email',
            [
            	'header' => __('Email'),
                 'type' => 'text',
                'index' => 'email',
                'name'=>'customer_email'
            ]
        );
        $this->addColumn('total', array(
		      'header' => __('Amount'),
		      'index' => 'total',
		      'type' => 'text',
		      'align' => 'right',
		      'renderer'  => 'OnlineBiz\ReceivableInvoices\Block\Adminhtml\Payment\Renderer\Total',
		      'filter_index'=>'main_table.total'
		    ));

        

         $this->addColumn(
            'payment_code',
            [
                'header' => __('Payment Method'),
                'type' => 'options',
                'index' => 'payment_code',
                'name'=>'payment_code',
                'options' => $this->PaymentOptionArray(),
     			'filter_index' => 'main_table.payment_code',
            ]
        );

       $this->addColumn(
            'received_at',
            [
                'header' => __('Received Date'),
                'index' => 'received_at',
                'name'=>'received_at',
                 'filter_index' => 'main_table.received_at',
                'type'         => 'date'
            ]
        );
       $this->addColumn(
            'created_at',
            [
                'header' => __('Created Date'),
                'index' => 'created_at',
                'name'=>'created_at',
                 'filter_index' => 'main_table.created_at',
                'type'         => 'datetime'
            ]
        );

         $this->addColumn(
            'updated_at',
            [
                'header' => __('Updated Date'),
                'index' => 'updated_at',
                'name'=>'created_at',
                'filter_index' => 'main_table.updated_at',
                'type'         => 'datetime',
            ]
        );
        return parent::_prepareColumns();
    }


   

     public function PaymentOptionArray()
    {
        $payments = $this->_paymentModelConfig->getActiveMethods();
        $methods = array();
        foreach ($payments as $paymentCode => $paymentModel) {
            $paymentTitle = $this->_appConfigScopeConfigInterface
                ->getValue('payment/'.$paymentCode.'/title');
            $methods[$paymentCode] = $paymentTitle;
        }
        return $methods;
    }

      /**
     * @return array
     */
    public function getCoreStoreOptionArray()
    {
        $result = [];
        $arr = $this->storeCollectionFactory->create()->toArray();
        foreach ($arr['items'] as $value) {
            $result[$value['store_id']] = $value['name'];
        }

        return $result;
    }

}