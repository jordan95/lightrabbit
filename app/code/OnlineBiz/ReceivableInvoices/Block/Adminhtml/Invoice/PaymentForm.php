<?php
namespace OnlineBiz\ReceivableInvoices\Block\Adminhtml\Invoice;

class PaymentForm extends \Magento\Framework\View\Element\Template
{
	 public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Backend\Model\Auth\Session $backendSession,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\Config\ScopeConfigInterface $appConfigScopeConfigInterface,
        \Magento\Sales\Model\ResourceModel\Order\Invoice\Grid\CollectionFactory $invoiceCollection,
         \Magento\Payment\Model\Config $paymentModelConfig,
        \OnlineBiz\ReceivableInvoices\Model\PaymentGridFactory  $paymentgridModel,
        array $data = []
    ) {
        $this->_backendSession = $backendSession;
        $this->invoiceCollection = $invoiceCollection;
        $this->_coreRegistry = $registry;
        $this->paymentgridModel = $paymentgridModel;
         $this->_paymentModelConfig = $paymentModelConfig;
        $this->_appConfigScopeConfigInterface = $appConfigScopeConfigInterface;
        parent::__construct($context, $data);
    }
	 /**
     * Retrieve invoice model instance
     *
     * @return \Magento\Sales\Model\Order\Invoice
     */
    public function getInvoice()
    {
        return $this->_coreRegistry->registry('current_invoice');
    }

    public function isCOD() {
    	
        $invoice_id = $this->getInvoice()->getId();
        $invoice_row = $this->invoiceCollection->create()->addfieldtofilter('entity_id', array('eq' => $invoice_id));
        $data = $invoice_row->getData();
        $payment_method = $data[0]['payment_method'];
        $payments = $this->_appConfigScopeConfigInterface->getValue('sales/invoive_receivable/payment_method_recievable', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                    $payment_arr = explode(",",$payments);

        if (in_array($payment_method, $payment_arr)) {
            return true;
        } else {
            return false;
        }
    }

    public function _getPaymentGrid() {
        $invoice_id = $this->getInvoice()->getIncrementId();
        $paygrid_collection = $this->paymentgridModel->create()->getCollection()
                        ->addfieldtofilter('invoice_id', array('eq' => $invoice_id));

        return $paygrid_collection;
    }
    public function getPaymentTitle($paymentCode){
    	 return $this->_appConfigScopeConfigInterface
                ->getValue('payment/'.$paymentCode.'/title');
    }

    public function PaymentOptionArray()
    {
        $payments = $this->_paymentModelConfig->getActiveMethods();
        $methods = array();
        foreach ($payments as $paymentCode => $paymentModel) {
            $paymentTitle = $this->_appConfigScopeConfigInterface
                ->getValue('payment/'.$paymentCode.'/title');
            $methods[$paymentCode] = array(
                'label' => $paymentTitle,
                'value' => $paymentCode
            );
        }
        return $methods;
    }

}
