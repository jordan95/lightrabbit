<?php 
namespace OnlineBiz\ReceivableInvoices\Block\Adminhtml;

use Magento\Backend\Block\Widget\Grid\Container;

class Receivable extends Container
{
    /**
     * {@inheritdoc}
     */
    protected function _construct()
    {
       
        $this->_controller = 'adminhtml_receivable';
        $this->_blockGroup = 'OnlineBiz_ReceivableInvoices';
         $this->_headerText = __('ReceivableInvoices');
        parent::_construct();
        $this->buttonList->remove('add');
         
    }
}
