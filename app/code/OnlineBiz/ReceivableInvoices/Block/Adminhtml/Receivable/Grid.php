<?php
namespace OnlineBiz\ReceivableInvoices\Block\Adminhtml\Receivable;

use Magento\Sales\Model\ResourceModel\Order\Invoice\Collection as InvoiceCollection;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $moduleManager;


    protected $_testFactory;

    protected $_status;


    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Framework\Module\Manager $moduleManager,
        \Magento\Sales\Model\ResourceModel\Order\Invoice\Grid\CollectionFactory $invoiceCollection,
        \Magento\Sales\Model\Order\Invoice $invoiceModel,
        \Magento\Framework\App\Config\ScopeConfigInterface $appConfigScopeConfigInterface,
        \Magento\Payment\Model\Config $paymentModelConfig,
        \Magento\Store\Model\ResourceModel\Store\CollectionFactory $storeCollectionFactory,
        array $data = []
    ) {
        $this->moduleManager = $moduleManager;
        $this->invoiceCollection = $invoiceCollection;
        $this->invoiceModel = $invoiceModel;
        $this->_appConfigScopeConfigInterface = $appConfigScopeConfigInterface;
        $this->_paymentModelConfig = $paymentModelConfig;
        $this->storeCollectionFactory = $storeCollectionFactory;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('listsGrid');
        $this->setDefaultSort('created_at');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $payment_arr = $this->getPayment_method_recievable();
        $collection = $this->invoiceCollection->create()->addfieldtofilter('payment_method', array('in' => $payment_arr));
        
       
        $this->setCollection($collection);
        parent::_prepareCollection();
        return $this;
    }

    /**
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'id',
            [
                'header' => __('ID'),
                'type' => 'text',
                'index' => 'increment_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id',
                'name'=>'id',
            ]
        );
        $this->addColumn(
            'order_id',
            [
                'header' => __('ORDER ID'),
                 'type' => 'text',
                'index' => 'order_increment_id',
                'name'=>'order_increment_id'
            ]
        );

         $this->addColumn(
            'store_id',
            [
                'header' => __('Store'),
                'type' => 'options',
                'index' => 'store_id',
                'name'=>'store_id',
                'options' => $this->getCoreStoreOptionArray(),
                'filter_index' => 'main_table.store_id',
            ]
        );
           $this->addColumn(
            'billing_name',
            [
                'header' => __('Bill-to-Name'),
                 'type' => 'text',
                'index' => 'billing_name',
                'name'=>'billing_name'
            ]
        );
        $this->addColumn('grand_total', array(
              'header' => __('Amount'),
              'index' => 'grand_total',
              'type' => 'text',
              'align' => 'right',
              'renderer'  => 'OnlineBiz\ReceivableInvoices\Block\Adminhtml\Receivable\Renderer\GrandTotal',
              'filter_index'=>'main_table.grand_total'
            ));

        $this->addColumn(
            'state',
            [
                'header' => __('State'),
                'type' => 'options',
                'index' => 'state',
                'name'=>'state',
                'options' => $this->invoiceModel->getStates(),
                'filter_index' => 'main_table.state',
            ]
        );

         $this->addColumn(
            'payment_method',
            [
                'header' => __('Payment Method'),
                'type' => 'options',
                'index' => 'payment_method',
                'name'=>'payment_method',
                'options' => $this->PaymentOptionArray(),
                'filter_index' => 'main_table.payment_method',
            ]
        );

        $this->addColumn(
            'created_at',
            [
                'header' => __('Created Date'),
                'index' => 'created_at',
                'name'=>'created_at',
                 'filter_index' => 'main_table.created_at',
                'type'         => 'datetime'
            ]
        );

         $this->addColumn(
            'updated_at',
            [
                'header' => __('Updated Date'),
                'index' => 'updated_at',
                'name'=>'created_at',
                'filter_index' => 'main_table.updated_at',
                'type'         => 'datetime',
            ]
        );

        $this->addColumn('cod_total_received', array(
          'header' => __('Total Received'),
          'index' => 'cod_total_received',
          'type' => 'text',
          'align' => 'right',
          'renderer'  => 'OnlineBiz\ReceivableInvoices\Block\Adminhtml\Receivable\Renderer\TotalReceived'
        ));

        $this->addColumn('cod_total_pending', array(
          'header' => __('Total Pending'),
          'index' => 'cod_total_pending',
          'type' => 'currency',
          'align' => 'right',
          'currency' => 'order_currency_code',
        ));

        return parent::_prepareColumns();
    }


    /**
     * {@inheritdoc}
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('sales/invoice/view/', ['invoice_id' => $row->getId()]);
    }

     public function PaymentOptionArray()
    {
        $payments = $this->_paymentModelConfig->getActiveMethods();
        $methods = array();
        $payment_arr = $this->getPayment_method_recievable();
        foreach ($payments as $paymentCode => $paymentModel) {
            if(in_array($paymentCode,  $payment_arr)){
                $paymentTitle = $this->_appConfigScopeConfigInterface
                    ->getValue('payment/'.$paymentCode.'/title');
                $methods[$paymentCode] = $paymentTitle;
             }
        }
        return $methods;
    }

      /**
     * @return array
     */
    public function getCoreStoreOptionArray()
    {
        $result = [];
        $arr = $this->storeCollectionFactory->create()->toArray();
        foreach ($arr['items'] as $value) {
            $result[$value['store_id']] = $value['name'];
        }

        return $result;
    }

    public function getPayment_method_recievable(){
        $payments =  $this->_appConfigScopeConfigInterface->getValue('sales/invoive_receivable/payment_method_recievable', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $payment_arr = explode(",",$payments);
        return $payment_arr;

    }

}