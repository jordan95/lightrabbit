<?php
 
namespace OnlineBiz\ReceivableInvoices\Block\Adminhtml\Receivable\Renderer;
 
use Magento\Framework\DataObject;
 
class GrandTotal extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
    protected $_currencyFactory;
    public function __construct(
         \Magento\Directory\Model\CurrencyFactory $currencyFactory
    ) {
        $this->_currencyFactory = $currencyFactory;
    }
 
    /**
     * get category name
     * @param  DataObject $row
     * @return string
     */
    public function render(DataObject $row)
    {
        $currency = $row->getOrderCurrencyCode();
        $total = number_format($row->getCodTotalReceived(),2,",",".");
        $currencySymbol = $this->_currencyFactory->create()->load($currency)->getCurrencySymbol();
        return $currencySymbol.$total;
    }
}