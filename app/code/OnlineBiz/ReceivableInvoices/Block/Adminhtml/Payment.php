<?php 
namespace OnlineBiz\ReceivableInvoices\Block\Adminhtml;

use Magento\Backend\Block\Widget\Grid\Container;

class Payment extends Container
{
    /**
     * {@inheritdoc}
     */
    protected function _construct()
    {
       
        $this->_controller = 'adminhtml_payment';
        $this->_blockGroup = 'OnlineBiz_ReceivableInvoices';
        $this->_headerText = __('Payments');
         parent::_construct();
         $this->buttonList->remove('add');
    }
}
