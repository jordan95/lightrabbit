<?php


namespace OnlineBiz\ReceivableInvoices\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;
class InstallSchema implements InstallSchemaInterface
{

    /**
     * {@inheritdoc}
     */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $installer = $setup;
        $installer->startSetup();

        $table = $installer->getConnection()->newTable(
            $installer->getTable('onlinebiz_payment_grid')
        )->addColumn(
            'payment_grid_id',
            Table::TYPE_INTEGER,
            null,
            ['unsigned' => false, 'nullable' => false, 'identity' => true, 'primary' => true],
            'Payment Grid Id'
        )->addColumn(
            'invoice_id',
            Table::TYPE_TEXT,
            255,
            ['unsigned' => false, 'nullable' => false],
            'invoice increment id'
        )->addColumn(
            'payment_code',
            Table::TYPE_TEXT,
            255,
            ['unsigned' => false, 'nullable' => false],
            'Payment Code'
        )->addColumn(
            'total',
            Table::TYPE_FLOAT,
            null,
            ['unsigned' => false, 'nullable' => false],
            'TOTAL'
        )->addColumn(
            'note',
            Table::TYPE_TEXT,
            255,
            ['unsigned' => false, 'nullable' => false],
            'Note'
        )->addColumn(
            'billing_name',
            Table::TYPE_TEXT,
            255,
            ['unsigned' => false, 'nullable' => false],
            'Billing Name'
        )->addColumn(
            'email',
            Table::TYPE_TEXT,
            255,
            ['unsigned' => false, 'nullable' => false],
            'email'
        )->addColumn(
            'received_at',
            Table::TYPE_DATE,
            null,
            ['unsigned' => false, 'nullable' => true],
            'Received At'
        )->addColumn(
            'created_at',
            Table::TYPE_TIMESTAMP,
            null,
            ['unsigned' => false, 'nullable' => true,'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
            'Created At'
        )->addColumn(
            'updated_at',
            Table::TYPE_TIMESTAMP,
            null,
            ['unsigned' => false, 'nullable' => true,'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
            'Updated At'
        );
        $installer->getConnection()->createTable($table);

        $eavTable = $installer->getTable('sales_invoice_grid');

        if ($setup->getConnection()->isTableExists($eavTable) == true) {

       $installer->getConnection()->addColumn(
                $eavTable,
                'cod_total_received',
                [
                    'type'     =>  Table::TYPE_DECIMAL,
                    'length'   => '12,4',
                    'nullable' => false,
                    'comment'  => 'Total Received'
                ]
            );
        $installer->getConnection()->addColumn(
                $eavTable,
                'cod_total_pending',
                [
                    'type'     =>  Table::TYPE_DECIMAL,
                    'length'   => '12,4',
                    'nullable' => false,
                    'comment'  => 'Total Pending'
                ]
            );
       }

       $eavTable = $installer->getTable('sales_invoice');

        if ($setup->getConnection()->isTableExists($eavTable) == true) {

       $installer->getConnection()->addColumn(
                $eavTable,
                'cod_total_received',
                [
                    'type'     =>  Table::TYPE_DECIMAL,
                    'length'   => '12,4',
                    'nullable' => false,
                    'comment'  => 'Total Received'
                ]
            );
        $installer->getConnection()->addColumn(
                $eavTable,
                'cod_total_pending',
                [
                    'type'     =>  Table::TYPE_DECIMAL,
                    'length'   => '12,4',
                    'nullable' => false,
                    'comment'  => 'Total Pending'
                ]
            );
       }
        $setup->endSetup();
    }
}
