<?php


namespace OnlineBiz\ReceivableInvoices\Model;

use OnlineBiz\ReceivableInvoices\Api\Data\PaymentGridInterface;

class PaymentGrid extends \Magento\Framework\Model\AbstractModel 
{
     const PAYMENT_GRID_ID = 'payment_grid_id';
    protected $_eventPrefix = 'onlinebiz_payment_grid';

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('OnlineBiz\ReceivableInvoices\Model\ResourceModel\PaymentGrid');
    }

    /**
     * Get paymentgrid_id
     * @return string
     */
    public function getPaymentgridId()
    {
        return $this->getData('payment_grid_id');
    }

    /**
     * Set paymentgrid_id
     * @param string $paymentgridId
     * @return \OnlineBiz\ReceivableInvoices\Api\Data\PaymentGridInterface
     */
    public function setPaymentgridId($paymentgridId)
    {
        return $this->setData('payment_grid_id', $paymentgridId);
    }

}
