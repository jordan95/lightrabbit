<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace OnlineBiz\ReceivableInvoices\Model\Order;

use Magento\Framework\Api\AttributeValueFactory;
use Magento\Sales\Api\Data\InvoiceInterface;
use Magento\Sales\Model\AbstractModel;
use Magento\Sales\Model\EntityInterface;

/**
 * @api
 * @method \Magento\Sales\Model\Order\Invoice setSendEmail(bool $value)
 * @method \Magento\Sales\Model\Order\Invoice setCustomerNote(string $value)
 * @method string getCustomerNote()
 * @method \Magento\Sales\Model\Order\Invoice setCustomerNoteNotify(bool $value)
 * @method bool getCustomerNoteNotify()
 * @SuppressWarnings(PHPMD.ExcessivePublicCount)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @since 100.0.2
 */
class Invoice extends \Magento\Sales\Model\Order\Invoice
{
        /**#@+
     * Invoice states
     */
    const STATE_OPEN = 1;

    const STATE_PAID = 2;

    const STATE_CANCELED = 3;

    const STATE_UNPAID = 4;

    /**
     * Pay invoice
     *
     * @return $this
     */
    public function pay()
    {
        if ($this->_wasPayCalled) {
            return $this;
        }
        $this->_wasPayCalled = true;

        $this->setState(self::STATE_PAID);

        $order = $this->getOrder();
        $order->getPayment()->pay($this);
        $totalPaid = $this->getGrandTotal();
        $baseTotalPaid = $this->getBaseGrandTotal();
        $invoiceList = $order->getInvoiceCollection();
         $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

         $scopeConfig = $objectManager->create('Magento\Framework\App\Config\ScopeConfigInterface');
        $payments = $scopeConfig->getValue('sales/invoive_receivable/payment_method_recievable', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $payment_arr = explode(",",$payments);
         $payment = $this->getOrder()->getPayment()->getMethodInstance()->getCode();
        // calculate all totals
        if (count($invoiceList->getItems()) > 1 ) {
            $totalPaid += $order->getTotalPaid();
            $baseTotalPaid += $order->getBaseTotalPaid();
        }
        if(!in_array($payment, $payment_arr)){
            $order->setTotalPaid($totalPaid);
            $order->setBaseTotalPaid($baseTotalPaid);
        }
        $this->_eventManager->dispatch('sales_order_invoice_pay', [$this->_eventObject => $this]);
        return $this;
    }
    //@codeCoverageIgnoreEnd

      /**
     * Retrieve invoice states array
     *
     * @return array
     */
    public static function getStates()
    {
        if (null === static::$_states) {
            static::$_states = [
                self::STATE_OPEN => __('Pending'),
                self::STATE_PAID => __('Paid'),
                self::STATE_CANCELED => __('Canceled'),
                self::STATE_UNPAID => __('UnPaid')
            ];
        }
        return static::$_states;
    }
}
