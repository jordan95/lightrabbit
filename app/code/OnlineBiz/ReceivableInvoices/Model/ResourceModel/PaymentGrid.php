<?php


namespace OnlineBiz\ReceivableInvoices\Model\ResourceModel;

class PaymentGrid extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('onlinebiz_payment_grid', 'payment_grid_id');
    }
}
