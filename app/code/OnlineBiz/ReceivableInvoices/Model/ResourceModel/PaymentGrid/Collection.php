<?php


namespace OnlineBiz\ReceivableInvoices\Model\ResourceModel\PaymentGrid;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            'OnlineBiz\ReceivableInvoices\Model\PaymentGrid',
            'OnlineBiz\ReceivableInvoices\Model\ResourceModel\PaymentGrid'
        );
    }
}
