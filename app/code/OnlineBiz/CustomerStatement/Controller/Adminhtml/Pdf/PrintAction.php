<?php


namespace OnlineBiz\CustomerStatement\Controller\Adminhtml\Pdf;

class PrintAction extends \Magento\Backend\App\Action
{    
    const STORE_ID = 1;
    const IS_DEFAULT =1;
    
    protected $resultPageFactory = false;
    protected $_request;
    protected $_resourceConnection;
    protected $_collectionFactory;
    protected $_customFactory;
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\App\RequestInterface $requestInterface,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Sales\Model\ResourceModel\Order\Invoice\CollectionFactory $collectionFactory,
        \Magento\Customer\Model\CustomerFactory $customerFactory
    )
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->_request = $requestInterface;
        $this->_collectionFactory = $collectionFactory;
        $this->_resourceConnection = $resourceConnection;
        $this->_customFactory = $customerFactory;
    }

    public function execute()
    {
        $customer_id ='';
        $data_template = '';
        $customer_id = $this->_request->getParam('customer_id');
        if($customer_id){
            $customer = $this->_customFactory->create()->load($customer_id);
            $connection = $this->_resourceConnection->getConnection();
            $tableName = $this->_resourceConnection->getTableName('eadesign_pdf_store');
            $table_template = $this->_resourceConnection->getTableName('eadesign_pdf_templates');
            $select = $connection->select()->from(
                                ['o' =>  $tableName]
                            )->where('o.store_id=?',$customer->getStoreId());
            $result = $connection->fetchAll($select);
             if($result){
                foreach($result as $k =>$val){
                    $data_template .= $val['template_id'].',';
                } 
                $data_template = explode(',',substr($data_template,0,-1));  //remove dau phay cuoi cung
                $result_template = $connection->select()->from(
                                ['em' =>  $table_template]
                            )->where('em.template_id IN (?)', $data_template)->where('em.is_active=?',self::IS_DEFAULT)->where('em.template_default=?', self::IS_DEFAULT)->where('em.template_type=?', \OnlineBiz\CustomerStatement\Model\Source\TemplateType::TYPE_STATEMENT);
                $result_data = $connection->fetchAll($result_template);  //get data contain template_id is active
                if($result_data){
                    foreach($result_data as $ki =>$vi){
                        $template_id = $vi['template_id'];
                    }      
                }
            }
            if(!empty($customer_id) && !empty($template_id)){

                $this->_redirect('customerstate/pdf/printpdf/template_id/'.$template_id.'/customer_id/'.$customer_id.'/');
            
            }else{
                $this->messageManager->addError(__('There are no template to be map in customer statement.'));
                $this->_redirect('cusomter/index/index/');
            }
        }else{
            $this->_redirect('cusomter/index/index/');
        }
    }


}
