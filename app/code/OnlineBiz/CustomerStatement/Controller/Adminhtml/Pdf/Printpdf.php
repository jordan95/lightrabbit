<?php
/**
 * EaDesgin
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@eadesign.ro so we can send you a copy immediately.
 *
 * @category    eadesigndev_pdfgenerator
 * @copyright   Copyright (c) 2008-2016 EaDesign by Eco Active S.R.L.
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

namespace OnlineBiz\CustomerStatement\Controller\Adminhtml\Pdf;

use Eadesigndev\Pdfgenerator\Controller\Adminhtml\Order\Abstractpdf;
use OnlineBiz\CustomerStatement\Helper\Pdf;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Email\Model\Template\Config;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Eadesigndev\Pdfgenerator\Model\PdfgeneratorRepository;
use Magento\Sales\Model\Order\InvoiceRepository;
use Magento\Sales\Model\ResourceModel\Order\Invoice\CollectionFactory;
use Magento\Customer\Model\CustomerFactory;
use Magento\Framework\App\ResourceConnection;
/**
 * Class Printpdf
 * @package Eadesigndev\Pdfgenerator\Controller\Adminhtml\Order\Invoice
 * @SuppressWarnings("CouplingBetweenObjects")
 * @SuppressWarnings("ExcessiveParameterList")
 */
class Printpdf extends Abstractpdf
{

    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    /**
     * @var DateTime
     */
    private $dateTime;

    /**
     * @var FileFactory
     */

    private $fileFactory;
    /**
     * @var ForwardFactory
     */

    private $resultForwardFactory;

    /**
     * @var Pdf
     */
    private $helper;

    /**
     * @var PdfgeneratorRepository
     */
    private $pdfGeneratorRepository;

    /**
     * @var InvoiceRepository
     */
    private $invoiceRepository;
    protected $_customFactory;
    protected $_collectionFactory;
    protected $_resourceConnection;
    /**
     * Printpdf constructor.
     * @param Context $context
     * @param Registry $coreRegistry
     * @param Config $emailConfig
     * @param JsonFactory $resultJsonFactory
     * @param Pdf $helper
     * @param DateTime $dateTime
     * @param FileFactory $fileFactory
     * @param ForwardFactory $resultForwardFactory
     * @param PdfgeneratorRepository $pdfGeneratorRepository
     * @param InvoiceRepository $invoiceRepository
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        Config $emailConfig,
        JsonFactory $resultJsonFactory,
        Pdf $helper,
        DateTime $dateTime,
        FileFactory $fileFactory,
        ForwardFactory $resultForwardFactory,
        PdfgeneratorRepository $pdfGeneratorRepository,
        InvoiceRepository $invoiceRepository,
        CustomerFactory $customerFactory,
        CollectionFactory $collectionFactory,
        ResourceConnection $resourceConnection
    ) {
        $this->fileFactory = $fileFactory;
        $this->helper = $helper;
        parent::__construct($context, $coreRegistry, $emailConfig, $resultJsonFactory);
        $this->resultForwardFactory = $resultForwardFactory;
        $this->dateTime = $dateTime;
        $this->pdfGeneratorRepository = $pdfGeneratorRepository;
        $this->invoiceRepository = $invoiceRepository;
        $this->_customFactory = $customerFactory;
        $this->_resourceConnection = $resourceConnection;
        $this->_collectionFactory = $collectionFactory;
    }

    /**
     * @return object
     */
    public function execute()
    {
         $email_customer='';
         $getcompany='';
        $templateId = $this->getRequest()->getParam('template_id');

        if (!$templateId) {
            return $this->returnNoRoute();
        }

        $templateModel = $this->pdfGeneratorRepository
            ->getById($templateId);

        if (!$templateModel) {
            return $this->returnNoRoute();
        }

        $customerId = $this->getRequest()->getParam('customer_id');
        if (!$customerId) {
            return $this->returnNoRoute();
        }

        $customer = $this->_customFactory
            ->create()->load($customerId);
        $email_customer =$customer->getEmail();
         $company =  $this->_customFactory->create()->getCollection();
         $company->getSelect()->joinLeft(
            ['customer_grid_flat' => 'customer_grid_flat'],
            'e.entity_id = customer_grid_flat.entity_id',
            ['billing_company'=>'billing_company']
        );
        $company->addFieldToFilter('entity_id', array('eq' => $customerId)); 
        foreach($company->getData() as $item){
            $getcompany =  $item['billing_company'];
        }
        $collection = $this->invoiceRepository->create()->getCollection();
        
        $collection->getSelect()->joinLeft(
            ['sale_grid' => 'sales_invoice_grid'],
            'main_table.entity_id = sale_grid.entity_id',
            ['customer_email'=>'customer_email','customer_name'=>'customer_name','payment_method'=>'payment_method']
        )->joinLeft(
            ['sales_payment' => 'sales_order_payment'],
            'main_table.order_id = sales_payment.parent_id',
            ['po_number'=>'po_number']
        )->joinLeft(
            ['onlinebiz_payment' => 'onlinebiz_payment_grid'],
            'main_table.increment_id = onlinebiz_payment.invoice_id',
            ['invoice_id'=>'invoice_id','note'=>'note','total'=>'total','received_at'=>'received_at']
        );

        $collection->addFieldToFilter('customer_email', array('eq' => $email_customer));
        $collection->setOrder('created_at','ASC');
        $collection->addFilterToMap('customer_email', 'sale_grid.customer_email');
        $collection->addFilterToMap('entity_id', 'main_table.entity_id');
        if (!$customer) {
            return $this->returnNoRoute();
        }
        $helper = $this->helper;
        $balanceCollection = clone $collection;
        $balance = $this->calculateBalance($balanceCollection,$email_customer);
        $helper->setStatement($collection);
        $helper->setTemplate($templateModel);
        $helper->getBalance($balance);
        $helper->getCustomer($customer);
        $helper->getCompany($getcompany);
        $pdfFileData = $helper->template2PdfStatement();

        $date = $this->dateTime->date('Y-m-d_H-i-s');

        $fileName = $pdfFileData['filename'] . $date . '.pdf';

        return $this->fileFactory->create(
            $fileName,
            $pdfFileData['filestream'],
            DirectoryList::VAR_DIR,
            'application/pdf'
        );
    }

    /**
     * @return $this
     */
    private function returnNoRoute()
    {
        return $this->resultForwardFactory->create()->forward('noroute');
    }
    protected function calculateBalance($balanceCollection,$email_customer)
    {
        $total_receive = 0;
        $total_invoice = 0;
        $total_invoice = 0;
        $unique = array();
        $dataTotal = $balanceCollection->getData();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $scopeConfig = $objectManager->create('Magento\Framework\App\Config\ScopeConfigInterface');
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $currency = $objectManager->get('\Magento\Directory\Model\Currency');
        $payments = $scopeConfig->getValue('sales/invoive_receivable/payment_method_recievable', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $payment_arr = explode(",",$payments);

        foreach ($balanceCollection->getData() as $_item): 
             if($_item['state'] ==\OnlineBiz\ReceivableInvoices\Model\Order\Invoice::STATE_PAID && !in_array($_item['payment_method'], $payment_arr)){
                if(!is_null($_item['grand_total'])){
                       $total_receive += (float)$_item['grand_total'];
                    }
                    
             }else{
                    if(!is_null($_item['total'])){
                       $total_receive += (float)$_item['total'];
                      
                    }
                }
        endforeach;
        foreach ($dataTotal as $value)
        {
            $unique[$value['entity_id']] = $value;
        }
        if(is_array($unique)){
            $data['entity_id'] = array_values($unique);
            foreach ($data['entity_id'] as $_item): 
                if(!is_null($_item['grand_total'])){
                   $total_invoice += (float)$_item['grand_total'];
                }  
            endforeach;
        }
        $total_invoice = $total_invoice - $total_receive;
        if($total_invoice !=''){
         $total_invoice =  number_format((float)$total_invoice, 2);
        }else{
            $total_invoice =  number_format(0, 2);
        }
        return $currency->getCurrencySymbol().$total_invoice;
        
    }
}
