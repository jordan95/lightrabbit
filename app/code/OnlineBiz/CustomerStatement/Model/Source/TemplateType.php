<?php
/**
 * OnlineBiz\CustomerStatement
 * Code By Jordan
 */
namespace OnlineBiz\CustomerStatement\Model\Source;

use Magento\Framework\View\Model\PageLayout\Config\BuilderInterface;

/**
 * Class PageLayout
 */
class TemplateType extends \Eadesigndev\Pdfgenerator\Model\Source\AbstractSource
{
    
    /**
     * @var \Magento\Framework\View\Model\PageLayout\Config\BuilderInterface
     */
    private $pageLayoutBuilder;

    /**
     * Constructor
     *
     * @param BuilderInterface $pageLayoutBuilder
     */
    public function __construct(BuilderInterface $pageLayoutBuilder)
    {
        $this->pageLayoutBuilder = $pageLayoutBuilder;
    }

    /**
     * Types
     */
    const TYPE_INVOICE = 1;
    const TYPE_ORDER = 2;
    const TYPE_QUOTATION = 3;
    const TYPE_CREDIT_MEMO = 4;
    const TYPE_PACKING_SLIP= 5;
    const TYPE_STATEMENT = 6;
    /**
     * Prepare post's statuses.
     *
     * @return array
     */
    public function getAvailable()
    {
        return [self::TYPE_INVOICE => __('Invoice'),self::TYPE_ORDER => __('Order'),self::TYPE_QUOTATION => __('Quotation'),self::TYPE_CREDIT_MEMO => __('Credit Memo'),self::TYPE_PACKING_SLIP => __('Packing Slip'),self::TYPE_STATEMENT => __('Customer Statement')];
    }
}
