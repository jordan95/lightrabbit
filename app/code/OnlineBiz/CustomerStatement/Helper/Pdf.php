<?php
/**
 * EaDesgin
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@eadesign.ro so we can send you a copy immediately.
 *
 * @category    eadesigndev_pdfgenerator
 * @copyright   Copyright (c) 2008-2016 EaDesign by Eco Active S.R.L.
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

namespace OnlineBiz\CustomerStatement\Helper;

use Eadesigndev\Pdfgenerator\Model\Pdfgenerator;
use Eadesigndev\Pdfgenerator\Model\Source\TemplatePaperOrientation;
use Eadesigndev\Pdfgenerator\Model\Source\TemplatePaperForm;
use Eadesigndev\Pdfgenerator\Model\Template\Processor;
use Magento\Payment\Helper\Data as PaymentHelper;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Email\Container\InvoiceIdentity;
use Magento\Sales\Model\Order\Address\Renderer;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Sales\Model\Order\Invoice;
use Magento\Framework\App\Filesystem\DirectoryList;
use Mpdf\Mpdf;

/**
 * Class Pdf
 * @package Eadesigndev\Pdfgenerator\Helper
 * @SuppressWarnings("CouplingBetweenObjects")
 */
class Pdf extends \Eadesigndev\Pdfgenerator\Helper\Pdf
{
    protected $_statement;
    protected $_balance;
    protected $_customer;
    protected $_company;
	public function template2PdfStatement()
    {
        /**transport use to get the variables $order object, $invoice object and the template model object*/
        $parts = $this->_transportStatement();
        /** instantiate the mPDF class and add the processed html to get the pdf*/
        $applySettings = $this-> _eaPDFSettings($parts);

        $fileParts = [
            'filestream' => $applySettings,
            'filename' => filter_var($parts['filename'], FILTER_SANITIZE_URL)
        ];

        return $fileParts;
    }
    public function _transportStatement()
    {

        $statement = $this->_statement;
        $balance = $this->_balance;
        $customer = $this->_customer;
        $transport = [
            'statement' => $statement,
            'balance' => $balance,
            'customer' => $customer,
            'company'=>$this->_company,
            'comment' => 'customer infor'
        ];

        $processor = $this->processor;
        $processor->setVariables($transport);
        $processor->setTemplate($this->template);
        $parts = $processor->processTemplate();

        return $parts;
    }
    public function setStatement($statement)
    {
        $this->_statement = $statement;
        return $this;
    }
    public function getBalance($balance){
    	$this->_balance = $balance;
    	return $this;
    }
    public function getCustomer($customer){
        $this->_customer = $customer;
        return $this;
    }
    public function getCompany($company){
        $this->_company = $company;
        return $this;
    }
    

}
