<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace OnlineBiz\UrlRewrite\Model\Storage;
use Magento\UrlRewrite\Service\V1\Data\UrlRewrite;

class DbStorage extends \Magento\UrlRewrite\Model\Storage\DbStorage
{


  /**
   * {@inheritdoc}
   */
  protected function doReplace(array $urls)
  {
    foreach ($this->createFilterDataBasedOnUrls($urls) as $type => $urlData) {
      $urlData[UrlRewrite::ENTITY_TYPE] = $type;
      $this->deleteByData($urlData);
    }
    $data = [];
    $storeId_requestPaths = [];
    foreach ($urls as $url) {
      $storeId = $url->getStoreId();
      $requestPath = $url->getRequestPath();
      $url->getRequestPath($requestPath);

      // Skip if is exist in the database
      $sql = "SELECT * FROM url_rewrite where store_id = $storeId and request_path = '$requestPath'";
      $exists = $this->connection->fetchOne($sql);

      if ($exists) continue;

      $storeId_requestPaths[] = $storeId . '-' . $requestPath;
      $data[] = $url->toArray();
    }
    

    // remove root links
    foreach ($data as $key => $info) {
      if (isset($info['target_path']) && (stristr($info['target_path'], '/category/1') || stristr($info['target_path'], '/category/2')) && $info['entity_type'] == 'product' || $info['request_path'] == "") {
        unset($data[$key]);
      }
    }

    try {
      // create links
      if (count($data) > 0) {
        $this->insertMultiple($data);
      }
    } catch (\Magento\Framework\Exception\AlreadyExistsException $e) {
      /** @var \Magento\UrlRewrite\Service\V1\Data\UrlRewrite[] $urlConflicted */
      $urlConflicted = [];
      foreach ($urls as $url) {
        $urlFound = $this->doFindOneByData(
          [
            UrlRewriteData::REQUEST_PATH => $url->getRequestPath(),
            UrlRewriteData::STORE_ID => $url->getStoreId()
          ]
        );
        if (isset($urlFound[UrlRewriteData::URL_REWRITE_ID])) {
          $urlConflicted[$urlFound[UrlRewriteData::URL_REWRITE_ID]] = $url->toArray();
        }
      }
      if ($urlConflicted) {
        throw new \Magento\UrlRewrite\Model\Exception\UrlAlreadyExistsException(
          __('URL key for specified store already exists.'),
          $e,
          $e->getCode(),
          $urlConflicted
        );
      } else {
        throw $e->getPrevious() ?: $e;
      }
    }

    return $urls;
  }
}
