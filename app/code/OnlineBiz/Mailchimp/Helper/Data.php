<?php
/**
 * Created by PhpStorm.
 * User: nguyenbach
 * Date: 03/01/2019
 * Time: 17:49
 */

namespace OnlineBiz\MailChimp\Helper;

class Data extends \Ebizmarts\MailChimp\Helper\Data
{
    public function getSubscriberInterest($subscriberId, $storeId, $interest = null)
    {
        if (!$interest) {
            $interest = $this->getInterest($storeId);
        }
        /**
         * @var $interestGroup \Ebizmarts\MailChimp\Model\MailChimpInterestGroup
         */

        $interestGroup = $this->_interestGroupFactory->create();
        $interestGroup->getBySubscriberIdStoreId($subscriberId, $storeId);
        if (!empty($interestGroup->getGroupdata())) {
            $groups = $this->_serializer->unserialize($interestGroup->getGroupdata());
        }

        if (isset($groups['group'])) {
            foreach ($groups['group'] as $key => $value) {
                if (isset($interest[$key])) {
                    if (is_array($value)) {
                        foreach ($value as $groupId) {
                            foreach ($interest[$key]['category'] as $gkey => $gvalue) {
                                if ($gvalue['id'] == $groupId) {
                                    $interest[$key]['category'][$gkey]['checked'] = true;
                                } elseif (!isset($interest[$key]['category'][$gkey]['checked'])) {
                                    $interest[$key]['category'][$gkey]['checked'] = false;
                                }
                            }
                        }
                    } else {
                        foreach ($interest[$key]['category'] as $gkey => $gvalue) {
                            if ($gvalue['id'] == $value) {
                                $interest[$key]['category'][$gkey]['checked'] = true;
                            } else {
                                $interest[$key]['category'][$gkey]['checked'] = false;
                            }
                        }
                    }
                }
            }
        }
        return $interest;
    }
}