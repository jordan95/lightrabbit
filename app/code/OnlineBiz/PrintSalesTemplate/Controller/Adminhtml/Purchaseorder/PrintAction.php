<?php


namespace OnlineBiz\PrintSalesTemplate\Controller\Adminhtml\Purchaseorder;

use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\Filesystem\DirectoryList;

class PrintAction extends \BoostMyShop\Supplier\Controller\Adminhtml\Order\PrintAction
{

    /**
     * @return ResponseInterface|void
     */
    public function execute()
    {
        
        $poId = $this->getRequest()->getParam('po_id');   
        if ($poId) { 
            $data_template ='';    
            $collection = $this->_orderFactory->create()->getCollection();
            $collection->addFieldToFilter('po_id',$poId);
            foreach($collection as $item){
                $store_id = $item->getPoStoreId();
            }

            $resource = $this->_objectManager->get('Magento\Framework\App\ResourceConnection');
            $connection = $resource->getConnection();
            $tableName = $resource->getTableName('eadesign_pdf_store');
            $table_template = $resource->getTableName('eadesign_pdf_templates');
            $select = $connection->select()->from(
                                ['o' =>  $tableName]
                            )->where('o.store_id=?',$store_id);
            $result = $connection->fetchAll($select);
            if($result){
                    foreach($result as $k =>$val){
                        $data_template .= $val['template_id'].',';
                    }   
                    $data_template = explode(',',substr($data_template,0,-1));  //remove dau phay cuoi cung
                    $result_template = $connection->select()->from(
                                    ['em' =>  $table_template]
                                )->where('em.template_id IN (?)', $data_template)->where('em.is_active=?', \OnlineBiz\PrintSalesTemplate\Model\Source\AbstractSource::IS_DEFAULT)->where('em.template_default=?', \OnlineBiz\PrintSalesTemplate\Model\Source\AbstractSource::IS_DEFAULT)->where('em.template_type=?', \OnlineBiz\PrintSalesTemplate\Model\Source\TemplateType::TYPE_ORDER);
                    $result_data = $connection->fetchAll($result_template);  //get data contain template_id is active
                    if($result_data){
                        foreach($result_data as $ki =>$vi){
                            $template_id = $vi['template_id'];
                        }      
                    }
            }
            if(!empty($poId) && !empty($template_id)){
                $this->_redirect('onlinebiz_pdf/purchaseorder_pdforders/printpdf/template_id/'.$template_id.'/po_id/'.$poId);
            }else{
                $model = $this->_orderFactory->create();
                $po = $model->load($poId);
                if ($po) {
                    $pdf = $this->_objectManager->create('BoostMyShop\Supplier\Model\Pdf\Order')->getPdf([$po]);
                    $date = $this->_objectManager->get('Magento\Framework\Stdlib\DateTime\DateTime')->date('Y-m-d_H-i-s');
                    return $this->_objectManager->get('\Magento\Framework\App\Response\Http\FileFactory')->create(
                        'purchase_order' . $date . '.pdf',
                        $pdf->render(),
                        DirectoryList::VAR_DIR,
                        'application/pdf'
                    );
                }
            }
        } else {
            return $this->resultForwardFactory->create()->forward('noroute');
        }
    }
}
