<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace OnlineBiz\PrintSalesTemplate\Controller\Adminhtml\Shipment;

use Magento\Backend\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Sales\Model\Order\Pdf\Shipment;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Sales\Model\ResourceModel\Order\Shipment\CollectionFactory;

class Pdfshipments extends \Magento\Sales\Controller\Adminhtml\Shipment\Pdfshipments
{
 	public function massAction(AbstractCollection $collection)
    {
    	$shipment_id ='';
        $order_id ='';
        $store_id = '';
        $template_id= '';
        $data_template = '';
    	$shipment_ids = $collection->getAllIds();
    	if(is_array($shipment_ids)){
    		$shipment_id = $this->getShipmentId($shipment_ids);
    	}
    	
    	$shipmentCollection = $collection->addFieldToFilter('entity_id',$shipment_id);
    	 if (!$shipmentCollection->getSize()) {
            $this->messageManager->addError(__('There are no printable documents related to selected orders.'));
            return $this->resultRedirectFactory->create()->setPath($this->getComponentRefererUrl());
        }else{
            foreach($shipmentCollection->getData() as $key=>$value){
                $shipment_id = $value['entity_id'];
                $order_id = $value['order_id'];
                $store_id = $value['store_id'];
            }
            
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
            $connection = $resource->getConnection();
            $tableName = $resource->getTableName('eadesign_pdf_store');
            $table_template = $resource->getTableName('eadesign_pdf_templates');
            $select = $connection->select()->from(
                                ['o' =>  $tableName]
                            )->where('o.store_id=?',$store_id);
            $result = $connection->fetchAll($select);
            if($result){
                foreach($result as $k =>$val){
                    $data_template .= $val['template_id'].',';
                }   
                $data_template = explode(',',substr($data_template,0,-1));  //remove dau phay cuoi cung
                $result_template = $connection->select()->from(
                                ['em' =>  $table_template]
                            )->where('em.template_id IN (?)', $data_template)->where('em.is_active=?', \OnlineBiz\PrintSalesTemplate\Model\Source\AbstractSource::IS_DEFAULT)->where('em.template_default=?', \OnlineBiz\PrintSalesTemplate\Model\Source\AbstractSource::IS_DEFAULT)->where('em.template_type=?', \OnlineBiz\PrintSalesTemplate\Model\Source\TemplateType::TYPE_PACKING_SLIP);
                $result_data = $connection->fetchAll($result_template);  //get data contain template_id is active
                if($result_data){
                    foreach($result_data as $ki =>$vi){
                        $template_id = $vi['template_id'];
                    }      
                }
            }
            if(!empty($shipment_id) && !empty($order_id) && !empty($template_id)){
                $this->_redirect('onlinebiz_pdf/order_pdfshipments/printpdf/template_id/'.$template_id.'/order_id/'.$order_id.'/shipment_id/'.$shipment_id);
            }else{
                return $this->fileFactory->create(
		            sprintf('packingslip%s.pdf', $this->dateTime->date('Y-m-d_H-i-s')),
		            $this->pdfShipment->getPdf($collection)->render(),
		            DirectoryList::VAR_DIR,
		            'application/pdf'
		        );
            }
        }
        
    }
    protected function getShipmentId($allId){
    	$shipment_id =null;
    	foreach($allId as $key=>$value){
    		$shipment_id  = $value;
    	}
    	return $shipment_id;
    }
    
}
