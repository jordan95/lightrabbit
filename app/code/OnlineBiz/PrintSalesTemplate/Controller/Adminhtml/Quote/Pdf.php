<?php
/**
 *  CART2QUOTE CONFIDENTIAL
 *  __________________
 *  [2009] - [2018] Cart2Quote B.V.
 *  All Rights Reserved.
 *  NOTICE OF LICENSE
 *  All information contained herein is, and remains
 *  the property of Cart2Quote B.V. and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to Cart2Quote B.V.
 *  and its suppliers and may be covered by European and Foreign Patents,
 *  patents in process, and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from Cart2Quote B.V.
 * @category    Cart2Quote
 * @package     Quotation
 * @copyright   Copyright (c) 2018. Cart2Quote B.V. (https://www.cart2quote.com)
 * @license     https://www.cart2quote.com/ordering-licenses(https://www.cart2quote.com)
 */

namespace OnlineBiz\PrintSalesTemplate\Controller\Adminhtml\Quote;
class Pdf extends \Cart2Quote\Quotation\Controller\Adminhtml\Quote\Pdf
{
    public function execute()
    {
       $quoteId = $this->getRequest()->getParam('quote_id');  
      // var_dump($quoteId);die('ssss'); 
        if ($quoteId) { 
            $data_template ='';    
            $collection = $this->quoteFactory->create()->getCollection();
            $collection->addFieldToFilter('quote_id',$quoteId);
            foreach($collection as $item){
                $store_id = $item['store_id'];
            }

            $resource = $this->_objectManager->get('Magento\Framework\App\ResourceConnection');
            $connection = $resource->getConnection();
            $tableName = $resource->getTableName('eadesign_pdf_store');
            $table_template = $resource->getTableName('eadesign_pdf_templates');
            $select = $connection->select()->from(
                                ['o' =>  $tableName]
                            )->where('o.store_id=?',$store_id);
            $result = $connection->fetchAll($select);
            if($result){
                    foreach($result as $k =>$val){
                        $data_template .= $val['template_id'].',';
                    }   
                    $data_template = explode(',',substr($data_template,0,-1));  //remove dau phay cuoi cung
                    $result_template = $connection->select()->from(
                                    ['em' =>  $table_template]
                                )->where('em.template_id IN (?)', $data_template)->where('em.is_active=?', \OnlineBiz\PrintSalesTemplate\Model\Source\AbstractSource::IS_DEFAULT)->where('em.template_default=?', \OnlineBiz\PrintSalesTemplate\Model\Source\AbstractSource::IS_DEFAULT)->where('em.template_type=?', \OnlineBiz\PrintSalesTemplate\Model\Source\TemplateType::TYPE_QUOTATION);
                    $result_data = $connection->fetchAll($result_template);  //get data contain template_id is active
                    if($result_data){
                        foreach($result_data as $ki =>$vi){
                            $template_id = $vi['template_id'];
                        }      
                    }
            }
            if(!empty($quoteId) && !empty($template_id)){
                $this->_redirect('onlinebiz_pdf/quote/pdfquote/template_id/'.$template_id.'/quote_id/'.$quoteId);
            }else{
                return $this->traitExecute();
            }
        } else{
            $this->_redirect('quotation/quote');
        }
        
        
    }

}
