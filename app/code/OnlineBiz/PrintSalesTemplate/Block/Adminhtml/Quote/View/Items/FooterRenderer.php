<?php

namespace OnlineBiz\PrintSalesTemplate\Block\Adminhtml\Quote\View\Items;
use Magento\Framework\Pricing\PriceCurrencyInterface;
/**
 * Class FooterRenderer
 * @package Cart2Quote\Quotation\Block\Adminhtml\Quote\View\Items
 */
class FooterRenderer extends \Cart2Quote\Quotation\Block\Adminhtml\Quote\View\Items\FooterRenderer
{
    protected $priceCurrency;
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\CatalogInventory\Api\StockConfigurationInterface $stockConfiguration,
        \Magento\Framework\Registry $registry,
        \Magento\Catalog\Model\Product\OptionFactory $optionFactory,
        \Cart2Quote\Quotation\Model\Quote $quote,
        \Magento\Quote\Model\Quote\Item $emptyQuoteItem,
        \Magento\Tax\Helper\Data $taxHelper,
        \Magento\Tax\Block\Item\Price\Renderer $itemPriceRenderer,
        PriceCurrencyInterface $priceCurrency,
        array $data = []
    ) {
        $this->priceCurrency = $priceCurrency;
        parent::__construct($context, $stockRegistry, $stockConfiguration, $registry, $optionFactory,$quote,$emptyQuoteItem,$taxHelper,$itemPriceRenderer, $data);
    }
    public function FormatPrice($price, $decision = 2)
    {
        return number_format($price, 2, '.', '');
    }

    public function convertTax($price, $decision = 2)
    {
        return  number_format($price, 2, '.', '');
    }
}
