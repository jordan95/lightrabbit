<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace OnlineBiz\PrintSalesTemplate\Block\Adminhtml\Shipping;

use Magento\Framework\Pricing\PriceCurrencyInterface;

/**
 * Adminhtml sales order create shipping method form block
 *
 * @api
 * @author      Magento Core Team <core@magentocommerce.com>
 * @since 100.0.2
 */
class Method extends \Magento\Sales\Block\Adminhtml\Order\Create\Shipping\Method\Form
{
	public function getAddress()
    {
        return $this->getShip()->getShippingAddress();
    }
    public function getShippingPrice($price, $flag)
    {
        return $this->priceCurrency->convertAndFormat(
            $this->_taxData->getShippingPrice(
                $price,
                $flag,
                $this->getAddress(),
                null,
                $this->getAddress()->getQuote()->getStore()
            ),
            true,
            PriceCurrencyInterface::DEFAULT_PRECISION,
            $this->getShip()->getStore()
        );
    }
    public function getActiveMethodRate()
    {
        $rates = $this->getShippingRates();
        if (is_array($rates)) {
            foreach ($rates as $group) {
                foreach ($group as $rate) {
                    if ($rate->getCode() == $this->getShippingMethod()) {
                        return $rate;
                    }
                }
            }
        }
        return false;
    }
    public function getShippingRates()
    {
        if (empty($this->_rates)) {
            $this->_rates = $this->getAddress()->getGroupedAllShippingRates();
        }
        return $this->_rates;
    }
    public function getMethodTitle()
    {
        if ($this->_rates->getMethodTitle()) {
            $methodTitle = $this->_rates->getMethodTitle();
        } else {
            $methodTitle = $this->_rates->getMethodDescription();
        }

        return $this->escapeHtml($methodTitle);
    }

}
