<?php
namespace OnlineBiz\CustomData\Setup;

class InstallSchema implements \Magento\Framework\Setup\InstallSchemaInterface
{

	public function install(\Magento\Framework\Setup\SchemaSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context)
	{
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
            $resource   = $objectManager->get('Magento\Framework\App\ResourceConnection');
            $connection = $resource->getConnection();
            $tableName  = $resource->getTableName('quotation_quote_status');
            $update_cancel	 = "UPDATE " . $tableName . " SET label = 'Rejected - job lost' WHERE status ='canceled'";
            $connection->query($update_cancel);
            $update_proposal_sent	 = "UPDATE " . $tableName . " SET label = 'Live' WHERE status ='proposal_sent'";
            $connection->query($update_proposal_sent);
            $update_out_stock	 = "UPDATE " . $tableName . " SET label = 'Rejected - lead time' WHERE status ='out_of_stock'";
            $connection->query($update_out_stock);
            $update_quote_available	 = "UPDATE " . $tableName . " SET label = 'Rejected - cost' WHERE status ='quote_available'";
            $connection->query($update_quote_available);
            $update_closed	 = "UPDATE " . $tableName . " SET label = 'Rejected - reason not specified / expired' WHERE status ='closed'";
            $connection->query($update_closed);
		
	}
}