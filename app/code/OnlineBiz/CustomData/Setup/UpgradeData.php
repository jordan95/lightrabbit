<?php
namespace OnlineBiz\CustomData\Setup;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeData implements UpgradeDataInterface
{
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
            $resource   = $objectManager->get('Magento\Framework\App\ResourceConnection');
            $connection = $resource->getConnection();
            $tableName  = $resource->getTableName('sales_order');
            $update_shipping_method  = "UPDATE " . $tableName . " SET shipping_method = 'freeshipping_freeshipping' WHERE shipping_method in ('rom#48T##N','rom#48T##P','rom#FST#REC#P','rom#INT##E','rom#INT#REE#E','rom#INT#REE#P','rom#INT#REW#P','rom#SCD##P','rom#SCD#REC#P','rom#SDG##N','dpd1^11','dpd1^12','dpd1^19','dpd1^59','dpd1^81') ";
            $connection->query($update_shipping_method);           
        }
    }
}