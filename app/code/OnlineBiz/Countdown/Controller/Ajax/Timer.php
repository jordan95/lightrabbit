<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace OnlineBiz\Countdown\Controller\Ajax;

use Magento\Framework\App\Action\Context;
use OnlineBiz\Countdown\Helper\Data as CountdownHelper;

/**
 * @SuppressWarnings(PHPMD.TooManyFields)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Timer extends \Magento\Framework\App\Action\Action
{

  /** @var CountdownHelper */
  protected $_countdownHelper;


  /**
   * @param Context $context
   * @param CountdownHelper $dataHelper
   *
   * @SuppressWarnings(PHPMD.ExcessiveParameterList)
   */
  public function __construct( Context $context, CountdownHelper $dataHelper )
  {
    $this->_countdownHelper = $dataHelper;
    parent::__construct($context);
  }


  /**
   * Create customer account action
   *
   * @return void
   * @SuppressWarnings(PHPMD.CyclomaticComplexity)
   * @SuppressWarnings(PHPMD.NPathComplexity)
   */
  public function execute()
  {

    if(!$this->_getHelper()->isEnabled() || !$this->getRequest()->isAjax())
      return;

    $xhtml = $this->_view->getLayout()->createBlock('\OnlineBiz\Countdown\Block\Product\View\Countdown')->toHtml();
    $this->getResponse()->setBody($xhtml);

  }


  /**
   * get helper data
   *
   * @return CountdownHelper
   */
  protected function _getHelper()
  {
    return $this->_countdownHelper;
  }
}
