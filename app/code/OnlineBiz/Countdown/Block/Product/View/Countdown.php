<?php

namespace OnlineBiz\Countdown\Block\Product\View;

class Countdown extends \Magento\Framework\View\Element\Template
{
  /** @var CountdownHelper */
  protected $_countdownHelper;

  protected $date;

  protected $_escaper;

  /**
   * Path to template file in theme.
   *
   * @var string
   */
  protected $_template = "OnlineBiz_Countdown::product/view/countdown.phtml";

  /**
   * Constructor
   *
   * @param Template\Context $context
   * @param array $data
   */
  public function __construct(
    \Magento\Framework\View\Element\Template\Context $context,
    \OnlineBiz\Countdown\Helper\Data $dataHelper,
    \Magento\Framework\Escaper $escaper,
    array $data = []
  )
  {
    $this->_countdownHelper = $dataHelper;
    $this->_escaper = $escaper;
    parent::__construct($context, $data);
  }

  public function getHtmlHolidayBlock($isEscaper = false)
  {
    $xhtml = '<div class="default_no_countdown">' . $this->getLayout()->createBlock('Magento\Cms\Block\Block')->setBlockId($this->_getHelper()->getHolidayBlock())->toHtml() . '</div>';
    if ($isEscaper) {
      $xhtml = $this->_escaper->escapeHtml($xhtml);
    }
    return $xhtml;
  }

  public function _getHelper()
  {
    return $this->_countdownHelper;
  }
}
