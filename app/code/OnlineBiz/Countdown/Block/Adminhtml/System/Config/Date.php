<?php
namespace OnlineBiz\Countdown\Block\Adminhtml\System\Config;
use Magento\Config\Block\System\Config\Form\Field;

class Date extends Field
{
  const DATE_FORMAT = 'MM-dd';

  public function render(\Magento\Framework\Data\Form\Element\AbstractElement $element)
  {
    $element->setDateFormat(self::DATE_FORMAT);
    $element->setTimeFormat(null);
    return parent::render($element);
  }
}
