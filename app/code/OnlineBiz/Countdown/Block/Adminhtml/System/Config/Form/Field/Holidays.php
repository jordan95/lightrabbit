<?php
namespace OnlineBiz\Countdown\Block\Adminhtml\System\Config\Form\Field;

class Holidays  extends \Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray
{
  protected function _prepareToRender()
  {
    $this->addColumn('name', ['label' => __('Holiday')]);
    $this->addColumn('date', ['label' => __('Date')]);
    $this->_addAfter = false;
    $this->_addButtonLabel = __('Add Holiday');
  }
}
