(function () {
  require(['jquery'], function ($) {
  });
})();
var OnlineBizCountdown = {
  init: function (time, nextTime, container, hollidayContent) {
    this.dateTo = time;
    this.nextDate = nextTime;
    this.container = container;
    this.hollidayContent = hollidayContent;
    this.format = 'DHMS';
    this.getCounter();
  },
  addZero: function (str) {
    if (str < 10) {
      return '0' + str;
    }
    return str;
  },
  getCounter: function () {
    amount = this.dateTo;
    newTime = this.nextDate;
    /*
    if (amount < 0 && newTime > 0) {
      amount = newTime;
    } else if((amount && newTime) < 0) {
      jQuery(".ob_countdown_timer_container_default").html( this.hollidayContent );
    }*/
    if (amount < 0) {
      jQuery("#ob-product-detail_delivery").html(this.hollidayContent);
      return;
    }
    days = Math.floor(amount / 86400);
    amount = amount % 86400;
    hours = Math.floor(amount / 3600);
    amount = amount % 3600;
    mins = Math.floor(amount / 60);
    amount = amount % 60;
    secs = Math.floor(amount);
    timerContainer = jQuery('.ob_countdown_timer');
    hoursContainer = jQuery('.ob_countdown_hours_container');
    hoursContainerNumber = jQuery('.ob_countdown_hours_number');
    afterhoursSeparator = jQuery('.ob_countdown_separator_afterhours');
    minsContainer = jQuery('.ob_countdown_minutes_container');
    minsContainerNumber = jQuery('.ob_countdown_minutes_number');
    afterminsSeparator = jQuery('.ob_countdown_separator_afterminutes');
    secsContainer = jQuery('.ob_countdown_seconds_container');
    secsContainerNumber = jQuery('.ob_countdown_seconds_number');
    if (this.format.indexOf('H') != -1) {
      hoursContainerNumber.html(this.addZero(hours));
    } else {
      hoursContainer.hide();
    }
    if (this.format.indexOf('M') != -1) {
      minsContainerNumber.html(this.addZero(mins));
    } else {
      minsContainer.hide();
      afterhoursSeparator.hide();
    }
    if (this.format.indexOf('S') != -1) {
      secsContainerNumber.html(this.addZero(secs));
    } else {
      secsContainer.hide();
      afterminsSeparator.hide();
    }
    this.dateTo = this.dateTo - 1;
    setTimeout(function () {
      this.getCounter();
    }.bind(this), 1000);
  },
  getEventTime: function (url, element) {
    if(!url)
      return;

    jQuery.ajax({url: url, success: function(result){
        if(result){
          jQuery(element).html(result);
        }
    }});
  }
}