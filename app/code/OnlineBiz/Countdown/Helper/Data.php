<?php
namespace OnlineBiz\Countdown\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Stdlib\DateTime\DateTime;

class Data extends AbstractHelper
{
	const XML_GENERAL_CONFIG_PATH = 'countdown/general/';

	/**
	* @var StoreManagerInterface
	*/
	private $_storeManager;

	/**
	* @var ScopeConfigInterface
	*/
	private $_scopeConfig;

	/**
	* @var SerializerInterface
	*/
	private $serializer;

	/**
	* @var DateTime
	*/
  protected $date;

	/**
	* Data constructor.
	* @param \Magento\Framework\App\Helper\Context $context
	* @param \Magento\Store\Model\StoreManagerInterface $storeManager
	*/
	public function __construct(
	  Context $context,
	  SerializerInterface $serializer,
	  StoreManagerInterface $storeManager,
    DateTime $date
	) {
	  $this->_storeManager  = $storeManager;
	  $this->serializer  	  = $serializer;
	  $this->date  	  = $date;
	  $this->_scopeConfig   = $context->getScopeConfig();
	  parent::__construct($context);
	}

	/**
	* @param null $storeId
	*
	* @return mixed
	*/
	public function isEnabled($storeId = null)
	{
		return $this->getGeneralConfig('enable', $storeId) && $this->isModuleOutputEnabled();
	}

	/**
	* @param null $storeId
	*
	* @return mixed
	*/
	public function getEventTimeFinish($storeId = null)
	{
		$configTime = explode(',', $this->getGeneralConfig('cutdown_time', $storeId));
		return implode(':', $configTime);
	}

	/**
	* @param null $storeId
	*
	* @return mixed
	*/
	public function getWeekDaysOff($storeId = null)
	{
		return explode(',', $this->getGeneralConfig('week_days_off', $storeId));
	}

	/**
	* @param null $storeId
	*
	* @return mixed
	*/
	public function getYearDaysOff($storeId = null)
	{
		$holidays = $this->getGeneralConfig('year_days_off', $storeId);
		if ($holidays) {
			return $this->serializer->unserialize($holidays);
		}
		return null;
	}

	/**
	* @param null $storeId
	*
	* @return mixed
	*/
	public function getEventNotice($storeId = null)
	{
		return $this->getGeneralConfig('notice', $storeId);
	}

	/**
	* @param null $storeId
	*
	* @return mixed
	*/
	public function getTodayEndTime($storeId = null)
	{
		$endTime = $this->date->gmtDate('Y-m-d') . ' ' .$this->getEventTimeFinish();
		$timeDiff = $this->date->gmtTimestamp();
		return $this->date->timestamp($endTime) - $timeDiff;
	}

	/**
	* @param null $storeId
	*
	* @return mixed
	*/
	public function getNextDayEndTime($storeId = null)
	{
		$todayEndTime = $this->getTodayEndTime($storeId);
		if ($this->checkDayIsHoliday($storeId, true)){
			return (int)false;
		}

		return $todayEndTime + 86400;
	}

	/**
	* @param null $storeId
	*
	* @return mixed
	*/
	public function getHolidayBlock($storeId = null)
	{
		$confValue = $this->getGeneralConfig('holiday_block', $storeId);
		if (!$confValue)
			$confValue = 'instead_of_countdown';

		return $confValue;
	}

	/**
	* @param null $storeId
	*
	* @return mixed
	*/
	public function checkDayIsHoliday($storeId = null, $isNextDay = false)
	{
		$yearOff = $this->getYearDaysOff($storeId);
		$dayAndMonth = $this->date->date('m-d');
		if($isNextDay){
			$dayAndMonth = $this->date->date('m-d', strtotime('+1 days', $this->date->gmtTimestamp()));
		}
		$dayOfTheWeek = $this->date->date('w');
		if($isNextDay){
			$dayOfTheWeek = $dayOfTheWeek == 0 ? 1 : $dayOfTheWeek + 1;
		}
		if (count($yearOff)) {
			foreach ($yearOff as $date) {
				if($date['date'] == $dayAndMonth)
					return true;
			}
		}

		if (count($this->getWeekDaysOff($storeId)) && in_array($dayOfTheWeek, $this->getWeekDaysOff($storeId))) {
			return true;
		}

		return false;
	}


	/**
	* @param null $storeId
	*
	* @return mixed
	*/
	public function getHolidayDate($storeId = null)
	{
		$isHoiday = $this->checkDayIsHoliday($storeId);
		if(!$this->getNextDayEndTime()){
			$cutdownTime = explode(':', $this->getEventTimeFinish());
			$cutdownTime = isset($cutdownTime[0]) ? $cutdownTime[0] : 15;
			$hoursNow = $this->date->date('H');
			if($hoursNow >= $cutdownTime)
				$isHoiday = true;
		}
		return $isHoiday;
	}

	/**
	* @param string $code
	* @param null $storeId
	* @return mixed
	*/
	public function getGeneralConfig($field = '', $storeId = null)
	{
		return $this->getConfigValue(self::XML_GENERAL_CONFIG_PATH . $field, $storeId);
	}

	/**
	* @param $path
	* @param null $storeId
	* @return mixed
	* @throws \Magento\Framework\Exception\LocalizedException
	*/
	public function getConfigValue($path, $storeId = null, $scope = null)
	{
	  if($scope) {
      $value = $this->_scopeConfig->getValue($path, $scope, $storeId);
	  } else {
      $value = $this->_scopeConfig->getValue($path, ScopeInterface::SCOPE_STORES, $storeId);
	  }
	  return $value;
	}

}
