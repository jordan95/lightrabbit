<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-rma
 * @version   2.0.27
 * @copyright Copyright (C) 2018 Mirasvit (https://mirasvit.com/)
 */



namespace OnlineBiz\Rma\Model\ResourceModel\Rma\Grid;


use Magento\Framework\Api\Search\AggregationInterface;
use Mirasvit\Rma\Api\Data\RmaInterface;

class Collection extends \Mirasvit\Rma\Model\ResourceModel\Rma\Grid\Collection
{
    protected function initFields()
    {
        /* @noinspection PhpUnusedLocalVariableInspection */
        $select = $this->getSelect();
        $select->joinInner(
            ['order' => $this->getTable('sales_order')],
            'main_table.order_id = order.entity_id',
            ['order_increment_id' => 'order.increment_id']
        );
        $select->joinLeft(
            ['status' => $this->getTable('mst_rma_status')],
            'main_table.status_id = status.status_id',
            ['status_name' => 'status.name']
        );
        $select->columns(['name' => new \Zend_Db_Expr("CONCAT(main_table.firstname, ' ', main_table.lastname)")]);
    }
}