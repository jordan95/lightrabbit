<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-rma
 * @version   2.0.35
 * @copyright Copyright (C) 2019 Mirasvit (https://mirasvit.com/)
 */



namespace OnlineBiz\Rma\Model\ResourceModel\Rma;

/**
 * @method \Mirasvit\Rma\Model\Rma getFirstItem()
 * @method \Mirasvit\Rma\Model\Rma getLastItem()
 * @method \Mirasvit\Rma\Model\ResourceModel\Rma\Collection|\Mirasvit\Rma\Model\Rma[] addFieldToFilter
 * @method \Mirasvit\Rma\Model\ResourceModel\Rma\Collection|\Mirasvit\Rma\Model\Rma[] setOrder
 */
class Collection extends \Mirasvit\Rma\Model\ResourceModel\Rma\Collection
{
    protected $_idFieldName = 'rma_id';
    // to fix issue https://pm.opentechiz.com/onlinebizsoft/lightrabbit.co.uk/issues/249 please add protected $_idFieldName = 'rma_id'; 
    //to \Mirasvit\Rma\Model\ResourceModel\Rma\Collection

}
