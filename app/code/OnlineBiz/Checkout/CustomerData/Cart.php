<?php
/**
 * Created by PhpStorm.
 * User: nguyenbach
 * Date: 22/11/2018
 * Time: 09:07
 */

namespace OnlineBiz\Checkout\CustomerData;

class Cart extends \Magento\Checkout\CustomerData\Cart
{

    /**
     * Get array of last added items
     *
     * @return \Magento\Quote\Model\Quote\Item[]
     */

    protected function getRecentItems()
    {
        if ($this->getQuote()->getStore()->getId() != 10 && $this->getQuote()->getStore()->getId() != 1) {
            return parent::getRecentItems();
        }

        $items = [];
        if (!$this->getSummaryCount()) {
            return $items;
        }

        foreach (array_reverse($this->getAllQuoteItems()) as $item) {
            /* @var $item \Magento\Quote\Model\Quote\Item */
            if (!$item->getProduct()->isVisibleInSiteVisibility()) {
                $product =  $item->getOptionByCode('product_type') !== null
                    ? $item->getOptionByCode('product_type')->getProduct()
                    : $item->getProduct();

                $products[$product->getId()] = ['store_id' => $this->getQuote()->getStore()->getId(), 'visibility' => 1,'url_rewrite' => '#'];
                $urlDataObject = new \Magento\Framework\DataObject($products[$product->getId()]);
                $item->getProduct()->setUrlDataObject($urlDataObject);
            }
            $items[] = $this->itemPoolInterface->getItemData($item);
        }
        return $items;
    }
}