<?php
namespace OnlineBiz\FixCustomerGrid\Plugin\Admin\Customer;

use Cminds\Salesrep\Helper\Data;
use Magento\Backend\Model\Auth\Session;
use Magento\Eav\Model\ResourceModel\Entity\Attribute;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Data\Collection\EntityFactoryInterface;
use Magento\User\Model\ResourceModel\User\Collection;

class Grid extends \Cminds\Salesrep\Plugin\Admin\Customer\Grid
{

    public function beforeLoad($printQuery = false, $logQuery = false)
    {
        $isModuleEnabled = $this->salesrepHelper->isModuleEnabled();

        if ($isModuleEnabled) {
            if ($printQuery instanceof \Magento\Customer\Model\ResourceModel\Grid\Collection) {
                $collection = $printQuery;
                $view_all_customer = true;
                $joined_tables = array_keys(
                    $collection->getSelect()->getPart('from')
                );
                if (!in_array('salesrep', $joined_tables)) {
                    $isAdmin = $this->authSession->isAllowed(
                        'Magento_Backend::all'
                    );
                    $view_rep_name = $this->authSession->isAllowed(
                        'Cminds_Salesrep::access_order_detail_page'
                    );
                    $view_rep_name_all = $this->authSession->isAllowed(
                        'Cminds_Salesrep::customer_grid_view_assigned_sales_representative_all_orders'
                    );
                    $view_rep_name_sub = $this->authSession->isAllowed(
                        'Cminds_Salesrep::customer_grid_view_assigned_sales_representative_subordinate'
                    );
                    $view_own_customers = $this->authSession->isAllowed(
                        'Cminds_Salesrep::customer_grid_view_assigned_sales_representative_only_own'
                    );


                    if (!$isAdmin && !$view_rep_name_all && $view_all_customer===false) {
                        $custemerEntityInt = $this->coreResource
                            ->getTableName("customer_entity_int");

                        $salesrepAttributeId = $this->eavAttribute
                            ->getIdByCode(
                                'customer',
                                'salesrep_rep_id'
                            );

                        $collection->getSelect()
                            ->joinLeft(
                                ['customer_entity_int' => $custemerEntityInt],
                                'customer_entity_int.entity_id = main_table.entity_id AND customer_entity_int.attribute_id = \'' . $salesrepAttributeId . '\'',
                                [
                                    'value',
                                    'attribute_id'
                                ]
                            );

                        if ($view_rep_name_sub) {
                            $subordinateIds = $this->salesrepHelper
                                ->getSubordinateIds(
                                    $this->authSession->getUser()->getId()
                                );

                            $collection->addFieldToFilter(
                                'value',
                                ['in' => $subordinateIds]
                            );
                        }

                        if ($view_own_customers) {
                            $collection->addFieldToFilter(
                                'value',
                                ['eq' => $this->authSession->getUser()->getId()]
                            );
                        }
                    }
                }
            }
        }
    }
}
