<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace OnlineBiz\CategoryAttribute\Model\Category;

/**
 * Class DataProvider
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class DataProvider extends \Smartwave\Megamenu\Model\Category\DataProvider
{
    /**
     * @return array
     */
    protected function getFieldsMap()
    {
        return [
            'general' =>
                [
                    'parent',
                    'path',
                    'is_active',
                    'include_in_menu',
                    'name',
                ],
            'content' =>
                [
                    'image',
                    'banner_cate_desk',
                    'banner_cate_tab',
                    'banner_cate_mobie',
                    'description',
                    'description_attribute_bottom',
                    'landing_page',
                ],
            'display_settings' =>
                [
                    'display_mode',
                    'is_anchor',
                    'available_sort_by',
                    'use_config.available_sort_by',
                    'default_sort_by',
                    'use_config.default_sort_by',
                    'filter_price_range',
                    'use_config.filter_price_range',
                ],
            'search_engine_optimization' =>
                [
                    'url_key',
                    'url_key_create_redirect',
                    'use_default.url_key',
                    'url_key_group',
                    'meta_title',
                    'meta_keywords',
                    'meta_description',
                ],
            'assign_products' =>
                [
                ],
            'design' =>
                [
                    'custom_use_parent_settings',
                    'custom_apply_to_products',
                    'custom_design',
                    'page_layout',
                    'custom_layout_update',
                ],
            'schedule_design_update' =>
                [
                    'custom_design_from',
                    'custom_design_to',
                ],
                'sw-menu' =>
                [
                    'sw_menu_hide_item',
                    'sw_menu_type',
                    'sw_menu_static_width',
                    'sw_menu_cat_columns',
                    'sw_menu_float_type',
                    'sw_menu_cat_label',
                    'sw_menu_icon_img',
                    'sw_menu_font_icon',
                    'sw_menu_block_top_content',
                    'sw_menu_block_left_width',
                    'sw_menu_block_left_content',
                    'sw_menu_block_right_width',
                    'sw_menu_block_right_content',
                    'sw_menu_block_bottom_content',
                ],
                'case-study' =>
                [
                    'case_study_sub_title',
                    'case_study_energy',
                    'case_study_payback',
                    'case_study_lifetime',
                    'case_study_thumbs_img',
                    'case_study_opportunity',
                    'case_study_solution',
                    'case_study_benefits',
                ],
                 'setting-sub-category' =>
                [
                    'page_sub_title_category',
                    'product_title_sub_category',
                    'content_sub_category',
                    'sub_category_img',
                    'title_btn_sub_cate_1',
                    'link_btn_sub_cate1',
                    'enable_button_subcate',
                    'sub_cate_thumbimg'
                ],
                'setting-cate-gallery' =>
                [
                    'gallery_cate_thumbimg'
                    
                ],
                'lightRabbit-sub-category-b' =>
                [
                    'lr_sub_title_cate',
                    'lr_enable_button',
                    'lr_title_link_cate',
                    'lr_link_sub_cate',
                    'lr_cate_thumbimg',
                    'lr_cate_img',
                    'lr_content_cate'
                    
                    
                    
                ],
            'category_view_optimization' =>
                [
                ],
            'category_permissions' =>
                [
                ],
        ];
    }
}
