<?php
/**
* Copyright © 2016 SW-THEMES. All rights reserved.
*/

namespace OnlineBiz\CategoryAttribute\Setup;

use Magento\Framework\Module\Setup\Migration;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Catalog\Setup\CategorySetupFactory;

/**
 * @codeCoverageIgnore
 */
class UpgradeData implements UpgradeDataInterface
{
    /**
     * Category setup factory
     *
     * @var CategorySetupFactory
     */
    private $categorySetupFactory;
 
    /**
     * Init
     *
     * @param CategorySetupFactory $categorySetupFactory
     */
    public function __construct(CategorySetupFactory $categorySetupFactory)
    {
        $this->categorySetupFactory = $categorySetupFactory;
    }
    
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        if (version_compare($context->getVersion(), '2.2.0', '<')) {
            $categorySetup = $this->categorySetupFactory->create(['setup' => $setup]);
            $entityTypeId = $categorySetup->getEntityTypeId(\Magento\Catalog\Model\Category::ENTITY);
            $attributeSetId = $categorySetup->getDefaultAttributeSetId($entityTypeId);

            $menu_attributes = [
                'case_study_sub_title' => [
                    'type' => 'varchar',
                    'label' => 'Sub Title',
                    'input' => 'text',
                    'required' => false,
                    'sort_order' => 20,
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                    'group' => 'Case Study'
                ],
                'case_study_energy' => [
                    'type' => 'varchar',
                    'label' => 'Annual Energy Savings',
                    'input' => 'text',
                    'required' => false,
                    'sort_order' => 30,
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                    'group' => 'Case Study'
                ],
                'case_study_payback' => [
                    'type' => 'varchar',
                    'label' => 'Payback',
                    'input' => 'text',
                    'required' => false,
                    'sort_order' => 40,
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                    'group' => 'Case Study'
                ],
                'case_study_lifetime' => [
                    'type' => 'varchar',
                    'label' => 'Lifetime Savings',
                    'input' => 'text',
                    'required' => false,
                    'sort_order' => 50,
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                    'group' => 'Case Study'
                ],
                 'case_study_thumbs_img' => [
                    'type' => 'varchar',
                    'label' => 'Thumbnail Image',
                    'input' => 'image',
                    'backend' => 'Magento\Catalog\Model\Category\Attribute\Backend\Image',
                    'required' => false,
                    'sort_order' => 70,
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                    'group' => 'Case Study'
                ],
                'case_study_opportunity' => [
                    'type' => 'text',
                    'label' => 'Opportunity',
                    'input' => 'textarea',
                    'required' => false,
                    'sort_order' => 90,
                    'wysiwyg_enabled' => true,
                    'is_html_allowed_on_front' => true,
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                    'group' => 'Case Study'
                ],
                'case_study_solution' => [
                    'type' => 'text',
                    'label' => 'Solution',
                    'input' => 'textarea',
                    'required' => false,
                    'sort_order' => 110,
                    'wysiwyg_enabled' => true,
                    'is_html_allowed_on_front' => true,
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                    'group' => 'Case Study'
                ],
                'case_study_benefits' => [
                    'type' => 'text',
                    'label' => 'Benefits',
                    'input' => 'textarea',
                    'required' => false,
                    'sort_order' => 130,
                    'wysiwyg_enabled' => true,
                    'is_html_allowed_on_front' => true,
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                    'group' => 'Case Study'
                ]
            ];

            foreach($menu_attributes as $item => $data) {
                $categorySetup->addAttribute(\Magento\Catalog\Model\Category::ENTITY, $item, $data);
            }

            $idg =  $categorySetup->getAttributeGroupId($entityTypeId, $attributeSetId, 'Case Study');

            foreach($menu_attributes as $item => $data) {
                $categorySetup->addAttributeToGroup(
                    $entityTypeId,
                    $attributeSetId,
                    $idg,
                    $item,
                    $data['sort_order']
                );
            }

        }
        if (version_compare($context->getVersion(), '2.2.1', '<')) {
            $categorySetup = $this->categorySetupFactory->create(['setup' => $setup]);
            $entityTypeId = $categorySetup->getEntityTypeId(\Magento\Catalog\Model\Category::ENTITY);
            $attributeSetId = $categorySetup->getDefaultAttributeSetId($entityTypeId);

            $menu_attributes = [
                'page_sub_title_category' => [
                    'type' => 'varchar',
                    'label' => 'Page Sub Title',
                    'input' => 'text',
                    'required' => false,
                    'sort_order' => 10,
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                    'group' => 'Setting Sub Category'
                ],
                'title_sub_category_1' => [
                    'type' => 'varchar',
                    'label' => 'Title 1',
                    'input' => 'text',
                    'required' => false,
                    'sort_order' => 20,
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                    'group' => 'Setting Sub Category'
                ],
                'title_sub_category_2' => [
                    'type' => 'varchar',
                    'label' => 'Title 2',
                    'input' => 'text',
                    'required' => false,
                    'sort_order' => 30,
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                    'group' => 'Setting Sub Category'
                ],
                'product_title_sub_category' => [
                    'type' => 'varchar',
                    'label' => 'Product Title',
                    'input' => 'text',
                    'required' => false,
                    'sort_order' => 40,
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                    'group' => 'Setting Sub Category'
                ],
                'content_sub_category' => [
                    'type' => 'text',
                    'label' => 'Description',
                    'input' => 'textarea',
                    'required' => false,
                    'sort_order' => 50,
                    'wysiwyg_enabled' => true,
                    'is_html_allowed_on_front' => true,
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                    'group' => 'Setting Sub Category'
                ],
                 'sub_category_img' => [
                    'type' => 'varchar',
                    'label' => 'Images',
                    'input' => 'image',
                    'backend' => 'Magento\Catalog\Model\Category\Attribute\Backend\Image',
                    'required' => false,
                    'sort_order' => 60,
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                    'group' => 'Setting Sub Category'
                ],
                'title_btn_sub_cate_1' => [
                    'type' => 'varchar',
                    'label' => 'Title Button 1',
                    'input' => 'text',
                    'required' => false,
                    'sort_order' => 70,
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                    'group' => 'Setting Sub Category'
                ],
                'background_btn_sub_cate_1' => [
                    'type' => 'varchar',
                    'label' => 'Background Button 1',
                    'input' => 'text',
                    'required' => false,
                    'sort_order' => 80,
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                    'group' => 'Setting Sub Category'
                ],
                'color_btn_sub_cate_1' => [
                    'type' => 'varchar',
                    'label' => 'Color Button 1',
                    'input' => 'text',
                    'required' => false,
                    'sort_order' => 90,
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                    'group' => 'Setting Sub Category'
                ],
                'title_btn_sub_cate_2' => [
                    'type' => 'varchar',
                    'label' => 'Title Button 2',
                    'input' => 'text',
                    'required' => false,
                    'sort_order' => 91,
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                    'group' => 'Setting Sub Category'
                ],
                'background_btn_sub_cate_2' => [
                    'type' => 'varchar',
                    'label' => 'Background Button 2',
                    'input' => 'text',
                    'required' => false,
                    'sort_order' => 92,
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                    'group' => 'Setting Sub Category'
                ],
                'color_btn_sub_cate_2' => [
                    'type' => 'varchar',
                    'label' => 'Color Button 2',
                    'input' => 'text',
                    'required' => false,
                    'sort_order' => 93,
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                    'group' => 'Setting Sub Category'
                ],

            ];

            foreach($menu_attributes as $item => $data) {
                $categorySetup->addAttribute(\Magento\Catalog\Model\Category::ENTITY, $item, $data);
            }

            $idg =  $categorySetup->getAttributeGroupId($entityTypeId, $attributeSetId, 'Setting Sub Category');

            foreach($menu_attributes as $item => $data) {
                $categorySetup->addAttributeToGroup(
                    $entityTypeId,
                    $attributeSetId,
                    $idg,
                    $item,
                    $data['sort_order']
                );
            }

        }
        if (version_compare($context->getVersion(), '2.2.2', '<')) {
            $categorySetup = $this->categorySetupFactory->create(['setup' => $setup]);
            $entityTypeId = $categorySetup->getEntityTypeId(\Magento\Catalog\Model\Category::ENTITY);
            $attributeSetId = $categorySetup->getDefaultAttributeSetId($entityTypeId);

            $menu_attributes = [
                    'link_btn_sub_cate1' => [
                        'type' => 'varchar',
                        'label' => 'Link Button 1',
                        'input' => 'text',
                        'required' => false,
                        'sort_order' => 71,
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                        'group' => 'Setting Sub Category'
                    ],
                    'link_btn_sub_cate2' => [
                        'type' => 'varchar',
                        'label' => 'Link Button 2',
                        'input' => 'text',
                        'required' => false,
                        'sort_order' => 92,
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                        'group' => 'Setting Sub Category'
                    ],
            ];

            foreach($menu_attributes as $item => $data) {
                $categorySetup->addAttribute(\Magento\Catalog\Model\Category::ENTITY, $item, $data);
            }

            $idg =  $categorySetup->getAttributeGroupId($entityTypeId, $attributeSetId, 'Setting Sub Category');

            foreach($menu_attributes as $item => $data) {
                $categorySetup->addAttributeToGroup(
                    $entityTypeId,
                    $attributeSetId,
                    $idg,
                    $item,
                    $data['sort_order']
                );
            }

        }
        if (version_compare($context->getVersion(), '2.2.3', '<')) {
            $categorySetup = $this->categorySetupFactory->create(['setup' => $setup]);
            $entityTypeId = $categorySetup->getEntityTypeId(\Magento\Catalog\Model\Category::ENTITY);
            $attributeSetId = $categorySetup->getDefaultAttributeSetId($entityTypeId);

            $menu_attributes = [
                    'enable_button_subcate' => [
                        'type' => 'int',
                        'label' => 'Enable Button',
                        'input' => 'select',
                        'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                        'required' => false,
                        'sort_order' => 55,
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                        'group' => 'Setting Sub Category'
                    ],
                    'sub_cate_thumbimg' => [
                        'type' => 'varchar',
                        'label' => 'Thumbnail Images',
                        'input' => 'image',
                        'backend' => 'Magento\Catalog\Model\Category\Attribute\Backend\Image',
                        'required' => false,
                        'sort_order' => 56,
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                        'group' => 'Setting Sub Category'
                    ],
            ];

            foreach($menu_attributes as $item => $data) {
                $categorySetup->addAttribute(\Magento\Catalog\Model\Category::ENTITY, $item, $data);
            }

            $idg =  $categorySetup->getAttributeGroupId($entityTypeId, $attributeSetId, 'Setting Sub Category');

            foreach($menu_attributes as $item => $data) {
                $categorySetup->addAttributeToGroup(
                    $entityTypeId,
                    $attributeSetId,
                    $idg,
                    $item,
                    $data['sort_order']
                );
            }

        }
        if (version_compare($context->getVersion(), '2.2.4', '<')) {
            $categorySetup = $this->categorySetupFactory->create(['setup' => $setup]);
            $entityTypeId = $categorySetup->getEntityTypeId(\Magento\Catalog\Model\Category::ENTITY);
            $attributeSetId = $categorySetup->getDefaultAttributeSetId($entityTypeId);

            $menu_attributes = [
                    'gallery_cate_thumbimg' => [
                        'type' => 'varchar',
                        'label' => 'Thumbnail Images',
                        'input' => 'image',
                        'backend' => 'Magento\Catalog\Model\Category\Attribute\Backend\Image',
                        'required' => false,
                        'sort_order' => 60,
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                        'group' => 'Setting Gallery Category'
                    ],
            ];

            foreach($menu_attributes as $item => $data) {
                $categorySetup->addAttribute(\Magento\Catalog\Model\Category::ENTITY, $item, $data);
            }

            $idg =  $categorySetup->getAttributeGroupId($entityTypeId, $attributeSetId, 'Setting Gallery Category');

            foreach($menu_attributes as $item => $data) {
                $categorySetup->addAttributeToGroup(
                    $entityTypeId,
                    $attributeSetId,
                    $idg,
                    $item,
                    $data['sort_order']
                );
            }

        }
        if (version_compare($context->getVersion(), '2.2.5', '<')) {
            $categorySetup = $this->categorySetupFactory->create(['setup' => $setup]);
            $entityTypeId = $categorySetup->getEntityTypeId(\Magento\Catalog\Model\Category::ENTITY);
            $attributeSetId = $categorySetup->getDefaultAttributeSetId($entityTypeId);

            $menu_attributes = [
                    'banner_cate_desk' => [
                        'type' => 'varchar',
                        'label' => 'Category Banner Desktop',
                        'input' => 'image',
                        'backend' => 'Magento\Catalog\Model\Category\Attribute\Backend\Image',
                        'required' => false,
                        'sort_order' => 60,
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                        'group' => 'General Information'
                    ],
                    'banner_cate_tab' => [
                        'type' => 'varchar',
                        'label' => 'Category Banner Tablet',
                        'input' => 'image',
                        'backend' => 'Magento\Catalog\Model\Category\Attribute\Backend\Image',
                        'required' => false,
                        'sort_order' => 60,
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                        'group' => 'General Information'
                    ],
                    'banner_cate_mobie' => [
                        'type' => 'varchar',
                        'label' => 'Category Banner Mobie',
                        'input' => 'image',
                        'backend' => 'Magento\Catalog\Model\Category\Attribute\Backend\Image',
                        'required' => false,
                        'sort_order' => 60,
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                        'group' => 'General Information'
                    ],
            ];

            foreach($menu_attributes as $item => $data) {
                $categorySetup->addAttribute(\Magento\Catalog\Model\Category::ENTITY, $item, $data);
            }

            $idg =  $categorySetup->getAttributeGroupId($entityTypeId, $attributeSetId, 'General Information');

            foreach($menu_attributes as $item => $data) {
                $categorySetup->addAttributeToGroup(
                    $entityTypeId,
                    $attributeSetId,
                    $idg,
                    $item,
                    $data['sort_order']
                );
            }

        }
        if (version_compare($context->getVersion(), '2.2.6', '<')) {
            $categorySetup = $this->categorySetupFactory->create(['setup' => $setup]);
            $entityTypeId = $categorySetup->getEntityTypeId(\Magento\Catalog\Model\Category::ENTITY);
            $attributeSetId = $categorySetup->getDefaultAttributeSetId($entityTypeId);

            $menu_attributes = [
                    'lr_sub_title_cate' => [
                        'type' => 'varchar',
                        'label' => 'Sub Title',
                        'input' => 'text',
                        'required' => false,
                        'sort_order' => 1,
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                        'group' => 'LightRabbit Sub Category B'
                    ],
                    'lr_title_cate' => [
                        'type' => 'varchar',
                        'label' => 'Title',
                        'input' => 'text',
                        'required' => false,
                        'sort_order' => 2,
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                        'group' => 'LightRabbit Sub Category B'
                    ],
                    'lr_enable_button' => [
                        'type' => 'int',
                        'label' => 'Enable Button',
                        'input' => 'select',
                        'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                        'required' => false,
                        'sort_order' => 3,
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                        'group' => 'LightRabbit Sub Category B'
                    ],
                    'lr_title_link_cate' => [
                        'type' => 'varchar',
                        'label' => 'Button Title',
                        'input' => 'text',
                        'required' => false,
                        'sort_order' => 4,
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                        'group' => 'LightRabbit Sub Category B'
                    ],
                    'lr_link_sub_cate' => [
                        'type' => 'varchar',
                        'label' => 'Button Link',
                        'input' => 'text',
                        'required' => false,
                        'sort_order' => 5,
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                        'group' => 'LightRabbit Sub Category B'
                    ],
                    'lr_cate_thumbimg' => [
                        'type' => 'varchar',
                        'label' => 'Thumbnail Images',
                        'input' => 'image',
                        'backend' => 'Magento\Catalog\Model\Category\Attribute\Backend\Image',
                        'required' => false,
                        'sort_order' => 6,
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                        'group' => 'LightRabbit Sub Category B'
                    ],
                    'lr_cate_img' => [
                        'type' => 'varchar',
                        'label' => 'Images',
                        'input' => 'image',
                        'backend' => 'Magento\Catalog\Model\Category\Attribute\Backend\Image',
                        'required' => false,
                        'sort_order' => 7,
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                        'group' => 'LightRabbit Sub Category B'
                    ],
                    'lr_content_cate' => [
                        'type' => 'text',
                        'label' => 'Description',
                        'input' => 'textarea',
                        'required' => false,
                        'sort_order' => 8,
                        'wysiwyg_enabled' => true,
                        'is_html_allowed_on_front' => true,
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                        'group' => 'LightRabbit Sub Category B'
                    ],
            ];

            foreach($menu_attributes as $item => $data) {
                $categorySetup->addAttribute(\Magento\Catalog\Model\Category::ENTITY, $item, $data);
            }

            $idg =  $categorySetup->getAttributeGroupId($entityTypeId, $attributeSetId, 'LightRabbit Sub Category B');

            foreach($menu_attributes as $item => $data) {
                $categorySetup->addAttributeToGroup(
                    $entityTypeId,
                    $attributeSetId,
                    $idg,
                    $item,
                    $data['sort_order']
                );
            }

        }
       
        $installer->endSetup();
    }
}
