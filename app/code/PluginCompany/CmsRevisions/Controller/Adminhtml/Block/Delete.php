<?php
/**
 * Created by:  Milan Simek
 * Company:     Plugin Company
 *
 * LICENSE: http://plugin.company/docs/magento-extensions/magento-extension-license-agreement
 *
 * YOU WILL ALSO FIND A PDF COPY OF THE LICENSE IN THE DOWNLOADED ZIP FILE
 *
 * FOR QUESTIONS AND SUPPORT
 * PLEASE DON'T HESITATE TO CONTACT US AT:
 *
 * SUPPORT@PLUGIN.COMPANY
 */
namespace PluginCompany\CmsRevisions\Controller\Adminhtml\Block;

/**
 * Class Delete
 */
class Delete extends MassDelete
{
    protected function getBlockRevisionIds()
    {
        $id = $this->_request->getParam('block_revision_id');
        return [$id];
    }
}
