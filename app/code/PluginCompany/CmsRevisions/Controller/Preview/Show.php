<?php
/**
 * Created by:  Milan Simek
 * Company:     Plugin Company
 *
 * LICENSE: http://plugin.company/docs/magento-extensions/magento-extension-license-agreement
 *
 * YOU WILL ALSO FIND A PDF COPY OF THE LICENSE IN THE DOWNLOADED ZIP FILE
 *
 * FOR QUESTIONS AND SUPPORT
 * PLEASE DON'T HESITATE TO CONTACT US AT:
 *
 * SUPPORT@PLUGIN.COMPANY
 */
namespace PluginCompany\CmsRevisions\Controller\Preview;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\ForwardFactory;
use PluginCompany\CmsRevisions\Model\PageRepository;

class Show extends Action
{
    private $resultForwardFactory;
    private $pageRevisionRepository;

    /**
     * @param PageRepository $pageRevisionRepository
     * @param Context $context
     * @param ForwardFactory $resultForwardFactory
     */
    public function __construct(
        PageRepository $pageRevisionRepository,
        Context $context,
        ForwardFactory $resultForwardFactory
    ) {
        $this->pageRevisionRepository = $pageRevisionRepository;
        $this->resultForwardFactory = $resultForwardFactory;
        parent::__construct($context);
    }

    /**
    * View CMS page action
    *
    * @return \Magento\Framework\Controller\ResultInterface
    */
    public function execute()
    {
        $resultPage = $this
            ->_objectManager
            ->get('PluginCompany\CmsRevisions\Helper\PageRevision')
            ->setPage($this->getPageWithRevisionData())
            ->prepareResultPage($this);
        $this->addPageToLayoutBlock();
        if (!$resultPage) {
            $resultForward = $this->resultForwardFactory->create();
            return $resultForward->forward('noroute');
        }
        return $resultPage;
    }

    /**
     * @return \PluginCompany\CmsRevisions\Model\Page
     */
    private function getPageRevision()
    {
        return $this->pageRevisionRepository->getById($this->getRevisionId());
    }
    
    private function getRevisionId()
    {
        return $this->_request->getParam('page_revision_id');
    }
    
    private function addPageToLayoutBlock()
    {
        $layout = $this->_view->getLayout();

        $layout->getBlock('cms_page')->setPage($this->getPageWithRevisionData());
    }
    
    private function getPageWithRevisionData()
    {
        return $this
            ->getPageRevision()
            ->getCmsPageWithRevisionData();
    }
}
