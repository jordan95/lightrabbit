<?php
/**
 * Created by:  Milan Simek
 * Company:     Plugin Company
 *
 * LICENSE: http://plugin.company/docs/magento-extensions/magento-extension-license-agreement
 *
 * YOU WILL ALSO FIND A PDF COPY OF THE LICENSE IN THE DOWNLOADED ZIP FILE
 *
 * FOR QUESTIONS AND SUPPORT
 * PLEASE DON'T HESITATE TO CONTACT US AT:
 *
 * SUPPORT@PLUGIN.COMPANY
 */
namespace PluginCompany\CmsRevisions\Api;

use PluginCompany\CmsRevisions\Model\BlockInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * Interface BlockRepositoryInterface
 * @package PluginCompany\CmsRevisions\Api
 */
interface BlockRepositoryInterface
{
    /**
     * @param BlockInterface $page
     * @return mixed
     */
    public function save(BlockInterface $page);

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id);

    /**
     * @param SearchCriteriaInterface $criteria
     * @return mixed
     */
    public function getList(SearchCriteriaInterface $criteria);

    /**
     * @param BlockInterface $page
     * @return mixed
     */
    public function delete(BlockInterface $page);

    /**
     * @param $id
     * @return mixed
     */
    public function deleteById($id);
}
