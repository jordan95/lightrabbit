<?php
/**
 * Created by:  Milan Simek
 * Company:     Plugin Company
 *
 * LICENSE: http://plugin.company/docs/magento-extensions/magento-extension-license-agreement
 *
 * YOU WILL ALSO FIND A PDF COPY OF THE LICENSE IN THE DOWNLOADED ZIP FILE
 *
 * FOR QUESTIONS AND SUPPORT
 * PLEASE DON'T HESITATE TO CONTACT US AT:
 *
 * SUPPORT@PLUGIN.COMPANY
 */
namespace PluginCompany\CmsRevisions\Model;

use Magento\Cms\Model\PageRepository as CmsPageRepository;
use Magento\Framework\ObjectManagerInterface;
use Magento\Cms\Model\Page as CmsPage;

class PageFactory extends RevisionFactory
{
    /**
     * Factory constructor
     *
     * @param ObjectManagerInterface $objectManager
     * @param CmsPageRepository $pageRepository
     * @param string $instanceName
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        CmsPageRepository $pageRepository,
        $instanceName = '\\PluginCompany\\CmsRevisions\\Model\\Page')
    {
        parent::__construct(
            $objectManager,
            $pageRepository,
            $instanceName
        );
    }

    public function createFromCmsPageId($pageId)
    {
        return $this->createFromEntityId($pageId);
    }

    public function createFromCmsPage(CmsPage $page)
    {
        return $this->createFromEntity($page);
    }

}