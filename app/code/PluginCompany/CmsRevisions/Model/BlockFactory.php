<?php
/**
 * Created by:  Milan Simek
 * Company:     Plugin Company
 *
 * LICENSE: http://plugin.company/docs/magento-extensions/magento-extension-license-agreement
 *
 * YOU WILL ALSO FIND A PDF COPY OF THE LICENSE IN THE DOWNLOADED ZIP FILE
 *
 * FOR QUESTIONS AND SUPPORT
 * PLEASE DON'T HESITATE TO CONTACT US AT:
 *
 * SUPPORT@PLUGIN.COMPANY
 */
namespace PluginCompany\CmsRevisions\Model;

use Magento\Framework\ObjectManagerInterface;
use Magento\Cms\Model\Block as CmsBlock;
use Magento\Cms\Model\BlockRepository as CmsBlockRepository;

class BlockFactory extends RevisionFactory
{
    /**
     * Factory constructor
     *
     * @param ObjectManagerInterface $objectManager
     * @param CmsBlockRepository $blockRepository
     * @param string $instanceName
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        CmsBlockRepository $blockRepository,
        $instanceName = '\\PluginCompany\\CmsRevisions\\Model\\Block')
    {
        parent::__construct(
            $objectManager,
            $blockRepository,
            $instanceName
        );
    }

    public function createFromCmsBlockId($blockId)
    {
        return $this->createFromEntityId($blockId);
    }

    public function createFromCmsBlock(CmsBlock $block)
    {
        return $this->createFromEntity($block);
    }

}