<?php
/**
 * Created by:  Milan Simek
 * Company:     Plugin Company
 *
 * LICENSE: http://plugin.company/docs/magento-extensions/magento-extension-license-agreement
 *
 * YOU WILL ALSO FIND A PDF COPY OF THE LICENSE IN THE DOWNLOADED ZIP FILE
 *
 * FOR QUESTIONS AND SUPPORT
 * PLEASE DON'T HESITATE TO CONTACT US AT:
 *
 * SUPPORT@PLUGIN.COMPANY
 */
namespace PluginCompany\CmsRevisions\Model\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Yes/No source model class for grid listing
 */
class Yesno implements OptionSourceInterface
{
    
    const VALUE_YES = 1;
    const VALUE_NO = 0;

    /**
     * Retrieve all options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = [
            ['label' => __('Yes'), 'value' => self::VALUE_YES],
            ['label' => __('No'), 'value' => self::VALUE_NO],
        ];
        return $options;
    }
}
