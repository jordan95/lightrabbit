<?php
/**
 * Created by:  Milan Simek
 * Company:     Plugin Company
 *
 * LICENSE: http://plugin.company/docs/magento-extensions/magento-extension-license-agreement
 *
 * YOU WILL ALSO FIND A PDF COPY OF THE LICENSE IN THE DOWNLOADED ZIP FILE
 *
 * FOR QUESTIONS AND SUPPORT
 * PLEASE DON'T HESITATE TO CONTACT US AT:
 *
 * SUPPORT@PLUGIN.COMPANY
 */
namespace PluginCompany\CmsRevisions\Component\Adminhtml;

use Magento\Ui\Component\Listing;

use Magento\Framework\DataObject;
use Magento\Framework\View\Element\UiComponentInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;

class PageListing extends Listing
{

    /**
     * Constructor
     *
     * @param ContextInterface $context
     * @param UiComponentInterface[] $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        array $components = [],
        array $data = []
    ) {
        
        $this->context = $context;
        $this->components = $components;
        
        $data['config']['render_url'] = $this->getUrlWithPageId();
        $data['config']['update_url'] = $this->getUrlWithPageId();
        $data['config']['filter_url_params'] = ['page_id'];
        
        $this->initObservers($data);
        $this->context->getProcessor()->register($this);
        $this->_data = array_replace_recursive($this->_data, $data);
        
        parent::__construct($context, $components, $data);
    }
    
    private function getUrlWithPageId()
    {
        $url = $this->context->getUrl('mui/index/render', ['_current' => true]);
        return $url;
    }
}
