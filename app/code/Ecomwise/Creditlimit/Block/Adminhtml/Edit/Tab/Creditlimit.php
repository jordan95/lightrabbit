<?php

namespace Ecomwise\Creditlimit\Block\Adminhtml\Edit\Tab;

use Magento\Customer\Controller\RegistryConstants;
use Magento\Ui\Component\Layout\Tabs\TabInterface;
 
class Creditlimit extends \Magento\Framework\View\Element\Template implements TabInterface
{
    protected $coreRegistry;
    
    protected $_template = 'tab/creditlimit.phtml';
       
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->coreRegistry = $registry;
        parent::__construct($context, $data);
    }
 
    public function getCustomerId()
    {
        return $this->coreRegistry->registry(RegistryConstants::CURRENT_CUSTOMER_ID);
    }
    
    public function getTabLabel()
    {
        return __('Credit Limit');
    }
    
    public function getTabTitle()
    {
        return __('Credit Limit');
    }
    
    public function canShowTab()
    {
        if ($this->getCustomerId()) {
            return true;
        }
        return false;
    }
 
    public function isHidden()
    {
        if ($this->getCustomerId()) {
            return false;
        }
        return true;
    }
    
    public function getTabClass()
    {
        return '';
    }
    
    public function getTabUrl()
    {
        return "";
    }
    public function isAjaxLoaded()
    {
        return false;
    }
    
    protected function _prepareLayout()
    {
        $this->setChild(
            'grid',
            $this->getLayout()->createBlock(
                'Ecomwise\Creditlimit\Block\Adminhtml\Edit\Tab\Creditlimit\Grid',
                'creditlimit.grid'
            )
        );
        parent::_prepareLayout();
        return $this;
    }
}
