<?php
namespace Ecomwise\Creditlimit\Block\Adminhtml\Edit\Tab\Creditlimit;

use Magento\Customer\Controller\RegistryConstants;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    protected $coreRegistry = null;

    protected $collectionFactory;
    
    protected $limitHelper;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Ecomwise\Creditlimit\Model\ResourceModel\Limits\CollectionFactory $collectionFactory,
        \Magento\Framework\Registry $coreRegistry,
        \Ecomwise\Creditlimit\Helper\Limit $limitHelper,
        array $data = []
    ) {
        $this->coreRegistry = $coreRegistry;
        $this->collectionFactory = $collectionFactory;
        $this->limitHelper = $limitHelper;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('creditlimitGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('asc');

        $this->setUseAjax(false);

        $this->setEmptyText(__('No Credit Limit Found'));
    }

    public function getGridUrl()
    {
        return false;
    }

    protected function _prepareCollection()
    {
        $collection = $this->collectionFactory->create();
       
        $collection->getSelect()->joinLeft(
            ['rules' => $collection->getTable('ecomwise_credit_limit_customers')],
            'rules.rule_id = main_table.id',
            ['rule_id', 'customer_id']
        )->where('rules.customer_id=?', $this->coreRegistry->registry(RegistryConstants::CURRENT_CUSTOMER_ID));
        
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }
    
    public function getMainButtonsHtml()
    {
        $html = '';
       
        $customer_id = $this->coreRegistry->registry(RegistryConstants::CURRENT_CUSTOMER_ID);
        $rule = $this->limitHelper->getRuleByCustomer($customer_id);
        
        if ($rule) {
            $button_html =  $this->getLayout()->createBlock('Magento\Backend\Block\Widget\Button')->setData(
                [
                    'label' => __('Delete Limit'),
                    'onclick' => "setLocation('".$this->getUrl('creditlimit/rules/delete', ['id' => $rule->getId()])."')",
                    'class' => 'task',
                ]
            )->toHtml();
        } else {
            $button_html =  $this->getLayout()->createBlock('Magento\Backend\Block\Widget\Button')->setData(
                [
                    'label' => __('Add Limit'),
                    'onclick' => "setLocation('".$this->getUrl('creditlimit/rules/new', ['cid' => $customer_id])."')",
                    'class' => 'task',
                ]
            )->toHtml();
        }
       
        $html .= $button_html;
        return $html;
    }

    protected function _prepareColumns()
    {
        $this->addColumn(
            'id',
            [
                        'header'=> __('ID'),
                        'align' =>'right',
                        'width' => '50px',
                        'index' => 'id',
                        'type'  => 'number',
                        'filter' => false,
                        'sortable'  => false,
                ]
        );
                
        $this->addColumn(
            'amount',
            [
                        'header'=> __('Amount'),
                        'index' => 'amount',
                        'filter' => false,
                        'sortable'  => false,
                        'type'  => 'number'
                ]
        );
        
        $this->addColumn(
        		'open_amount',
        		[
        				'header'=> __('Used Amount'),
        				'index' => 'amount',
        				'filter' => false,
        				'sortable'  => false,
        				'type'  => 'string',
        				'renderer'=>'Ecomwise\Creditlimit\Block\Adminhtml\Renderers\Openamount'
        		]
        		);
        
        $this->addColumn(
            'action',
            [
                'header'    => ' ',
                'filter'    => false,
                'sortable'  => false,
                'width'     => '100px',
                'renderer'  => 'Ecomwise\Creditlimit\Block\Adminhtml\Renderers\Action'
              ]
        );
        
        return parent::_prepareColumns();
    }
}
