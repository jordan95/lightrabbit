<?php

namespace Ecomwise\Creditlimit\Block\Adminhtml\Creditlimit;

class Support extends \Magento\Backend\Block\Template implements \Magento\Framework\Data\Form\Element\Renderer\RendererInterface
{
    
    protected $_template = 'Ecomwise_Creditlimit::ecomwisecredit/support.phtml';
    public $module = 'Ecomwise_Creditlimit';
    public $supportUrl = 'http://support.ecomwise.com/support/home';
    public $email = 'feedback@ecomwise.com';
    public $faq = 'http://support.ecomwise.com/support/solutions/folders/111251';
    public $name = 'Credit Limit';
    public $compatibility = 'Magento CE 2.0.0 - 2.1.x';
    public $manualUrl = 'http://support.ecomwise.com/solution/articles/3000060746-credit-limit-2-0';
    protected $moduleList;
       
    public function __construct(
        \Magento\Framework\Module\ModuleListInterface $moduleList,
        \Magento\Backend\Block\Template\Context $context
    ) {
        $this->moduleList = $moduleList;
        parent::__construct($context);
    }
        
    public function render(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        return $this->toHtml();
    }
    
    public function getVersion()
    {
        $version = $this->moduleList->getOne($this->module)['setup_version'];
        return $version;
    }
}
