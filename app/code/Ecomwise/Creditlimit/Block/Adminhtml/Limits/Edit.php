<?php

namespace Ecomwise\Creditlimit\Block\Adminhtml\Limits;

class Edit extends \Magento\Backend\Block\Widget\Form\Container
{
    protected $coreRegistry = null;

    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    protected function _construct()
    {
        $this->_objectId = 'id';
        $this->_blockGroup = 'Ecomwise_Creditlimit';
        $this->_controller = 'adminhtml_limits';

        parent::_construct();

        $this->buttonList->update('save', 'label', __('Save Credit Limit'));
        $this->buttonList->update('delete', 'label', __('Delete Credit Limit'));
        $this->removeButton('reset');
    }
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}
