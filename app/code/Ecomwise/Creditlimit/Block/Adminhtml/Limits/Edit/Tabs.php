<?php

namespace Ecomwise\Creditlimit\Block\Adminhtml\Limits\Edit;

/**
 * Admin page left menu
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('limit_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Credit Limit Information'));
    }
}
