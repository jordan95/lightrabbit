<?php
namespace Ecomwise\Creditlimit\Block\Adminhtml\Limits\Edit\Tab;

class Main extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    
    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('current_credit_limit');

        $form = $this->_formFactory->create();

        $form->setHtmlIdPrefix('limits_');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Credit Limit Information')]);

        if ($model->getId()) {
            $fieldset->addField('id', 'hidden', ['name' => 'id']);
        }

        $fieldset->addField(
            'amount',
            'text',
            [
              'name' => 'amount',
              'label' => __('Amount'),
              'title' => __('Amount'),
              'required' => true,
            ]
        );

        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    public function getTabLabel()
    {
        return __('Credit');
    }

    public function getTabTitle()
    {
        return __('Credit');
    }

    public function canShowTab()
    {
        return true;
    }

    public function isHidden()
    {
        return false;
    }

    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}
