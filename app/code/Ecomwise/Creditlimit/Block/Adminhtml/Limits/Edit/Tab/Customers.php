<?php

namespace Ecomwise\Creditlimit\Block\Adminhtml\Limits\Edit\Tab;

class Customers extends \Magento\Backend\Block\Widget\Grid\Extended implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    protected $customerCollectionFactory;

    protected $limitsFactory;

    protected $registry;

    protected $objectManager = null;
    
    protected $rulesFactory;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Ecomwise\Creditlimit\Model\LimitsFactory $limitsFactory,
        \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory $customerCollectionFactory,
        \Ecomwise\Creditlimit\Model\ResourceModel\Rules\CollectionFactory $rulesFactory,
        array $data = []
    ) {
        $this->limitsFactory = $limitsFactory;
        $this->customerCollectionFactory = $customerCollectionFactory;
        $this->objectManager = $objectManager;
        $this->registry = $registry;
        $this->rulesFactory = $rulesFactory;
        parent::__construct($context, $backendHelper, $data);
    }

    protected function _construct()
    {
        parent::_construct();
        $this->setId('customerGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
        if ($this->getRequest()->getParam('id') || $this->getRequest()->getParam('cid')) {
            $this->setDefaultFilter(['in_customer' => 1]);
        }
    }

    protected function _addColumnFilterToCollection($column)
    {
        if ($column->getId() == 'in_customer') {
            $customerId = $this->_getSelectedCustomer();

            if (empty($customerId)) {
                $customerId = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('entity_id', ['in' => [$customerId]]);
            } else {
                if ($customerId) {
                    $this->getCollection()->addFieldToFilter('entity_id', ['nin' => [$customerId]]);
                }
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }

        return $this;
    }

    protected function _prepareCollection()
    {
        $collection = $this->customerCollectionFactory->create();
        $collection->addAttributeToSelect('email');
        $collection->addAttributeToSelect('firstname');
        $collection->addAttributeToSelect('lastname');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn(
            'in_customer',
            [
                'header_css_class' => 'a-center',
                'type' => 'radio',
                'name' => 'cid',
                'html_name' => 'cid',
                'align' => 'center',
                'index' => 'entity_id',
                'value' => $this->_getSelectedCustomer(),
            ]
        );

        $this->addColumn(
            'entity_id',
            [
                'header' => __('Customer ID'),
                'type' => 'number',
                'index' => 'entity_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id',
            ]
        );
        $this->addColumn(
            'firstname',
            [
                'header' => __('First Name'),
                'index' => 'firstname',
                'class' => 'xxx',
                'width' => '50px',
            ]
        );
        $this->addColumn(
            'lastname',
            [
                'header' => __('Last Name'),
                'index' => 'lastname',
                'class' => 'xxx',
                'width' => '50px',
            ]
        );
        $this->addColumn(
            'email',
            [
                'header' => __('Email'),
                'index' => 'email',
                'width' => '250px',
            ]
        );

        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/customersgrid', ['_current' => true]);
    }

    public function getRowUrl($row)
    {
        return '';
    }

    protected function _getSelectedCustomer()
    {
        $limit = $this->getLimit();
        if ($limit->getId()) {
            return $limit->getCustomerId();
        }
        if (!empty($this->getRequest()->getParam('cid'))) {
            return $this->getRequest()->getParam('cid');
        }
        
        return null;
    }

    public function getSelectedCustomer()
    {
        return  $this->_getSelectedCustomer();
    }

    protected function getLimit()
    {
        $limit = $this->registry->registry('current_credit_limit');
        
        if (!$limit || !$limit->getId()) {
            $limitId = $this->getRequest()->getParam('id');
            $limit   = $this->limitsFactory->create();
            if ($limitId) {
                $limit->load($limitId);
            }
        }
        
        if ($limit->getId()) {
                $rules_collection = $this->rulesFactory->create()
                ->addFieldToFilter("rule_id", ['eq'=> $limit->getId()])
                ->addFieldToSelect("customer_id");
            
            if (!empty($rules_collection)) {
                $rule = $rules_collection->getFirstItem();
                $limit->setData('customer_id', $rule->getCustomerId());
            }
        }
        
        return $limit;
    }

    public function canShowTab()
    {
        return true;
    }

    public function isHidden()
    {
        return false;
    }
    
    public function getTabLabel()
    {
        return __('Customers');
    }
    public function getTabTitle()
    {
        return __('Customers');
    }
}
