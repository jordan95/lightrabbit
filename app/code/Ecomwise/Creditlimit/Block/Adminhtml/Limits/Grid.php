<?php
namespace Ecomwise\Creditlimit\Block\Adminhtml\Limits;

class Grid extends \Magento\Backend\Block\Widget\Grid
{
    protected function _prepareCollection()
    {
        parent::_prepareCollection();
        $collection = $this->getCollection();
        
        $collection->getSelect()
            ->joinLeft(
                ['rules' => $collection->getTable('ecomwise_credit_limit_customers')],
                'rules.rule_id = main_table.id',
                ['rule_id', 'customer_id']
            )
            ->joinLeft(
                ['customers' => $collection->getTable('customer_grid_flat')],
                'customers.entity_id = rules.customer_id',
                ['name', 'email']
            );
            
        return $this;
    }
}
