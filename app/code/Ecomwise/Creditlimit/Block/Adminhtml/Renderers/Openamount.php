<?php

namespace Ecomwise\Creditlimit\Block\Adminhtml\Renderers;

class Openamount extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
    //protected $actions = [];

   protected $customerFactory;
   protected $helper;
	public function __construct(
			\Ecomwise\Creditlimit\Helper\Data $helper,
			\Magento\Customer\Model\CustomerFactory $customerFactory,
			\Magento\Backend\Block\Context $context, 
			array $data = []
		)
	{
		parent::__construct($context, $data);
		$this->customerFactory = $customerFactory;
		$this->helper = $helper;
	}
	
	public function render(\Magento\Framework\DataObject $row)
    {
        $customer_id = $row->getCustomerId();
        $customer = $this->customerFactory->create()->load($customer_id);
        $open_amount = $this->helper->getTotalOfOrders($customer);
      // \Zend_Debug::dump($open_amount); die();
        return  (string) $open_amount;
    	
    }

    
}
