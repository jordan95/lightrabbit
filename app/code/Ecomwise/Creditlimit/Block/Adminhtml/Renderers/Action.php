<?php

namespace Ecomwise\Creditlimit\Block\Adminhtml\Renderers;

class Action extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
    protected $actions = [];

    public function render(\Magento\Framework\DataObject $row)
    {
        $this->actions = [];
        $reorderAction = [
          '@' => [
              'href' => $this->getUrl('creditlimit/rules/edit', ['id' => $row->getId()]),
          ],
          '#' => __('Edit'),
        ];
        $this->addToActions($reorderAction);
        return $this->_actionsToHtml();
    }

    protected function _getEscapedValue($value)
    {
        return addcslashes(htmlspecialchars($value), '\\\'');
    }

    protected function _actionsToHtml(array $actions = [])
    {
        $html = [];
        $attributesObject = new \Magento\Framework\DataObject();

        if (empty($actions)) {
            $actions = $this->actions;
        }

        foreach ($actions as $action) {
            $attributesObject->setData($action['@']);
            $html[] = '<a ' . $attributesObject->serialize() . '>' . $action['#'] . '</a>';
        }
        return implode('', $html);
    }

    public function addToActions($actionArray)
    {
        $this->actions[] = $actionArray;
    }
}
