<?php
namespace Ecomwise\Creditlimit\Block\Form;

class Payonaccount extends \Magento\Payment\Block\Form
{
     const XML_PATH_PO_ENABLED = "payment/pay_on_account/po_enabled";
     
    protected $_template = 'Ecomwise_Creditlimit::form/pay.phtml';
     
    protected $scopeConfig;
     
    protected $helper;
    
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\App\Config\MutableScopeConfigInterface $scopeConfig,
        \Ecomwise\Creditlimit\Helper\Data $helper
    ) {
    
    
        parent::__construct($context);
        
        $this->scopeConfig = $scopeConfig;
        $this->helper = $helper;
    }
    
    public function getPoEnabledConfig()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_PO_ENABLED,
            \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE
        );
    }
    
    public function disableMethod()
    {
        $should_disable = $this->helper->getDisableStatus('admin');
        \Ecomwise\Creditlimit\Helper\Log::logMsg($should_disable, "creditlimit-log.log");
        return $should_disable;
    }
}
