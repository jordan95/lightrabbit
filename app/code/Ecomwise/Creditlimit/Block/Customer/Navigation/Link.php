<?php
namespace Ecomwise\Creditlimit\Block\Customer\Navigation;

class Link extends \Magento\Framework\View\Element\Html\Link\Current
{
    protected $helper;
        
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\App\DefaultPathInterface $defaultPath,
        \Ecomwise\Creditlimit\Helper\Data $helper
    ) {
        parent::__construct($context, $defaultPath);
        $this->helper = $helper;
    }
        
    protected function _toHtml()
    {
        $shoul_hide = $this->helper->getShouldHideTab();
        if ($shoul_hide) {
            return "";
        }
        return parent::_toHtml();
    }
}
