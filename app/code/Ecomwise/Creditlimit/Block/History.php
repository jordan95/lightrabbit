<?php
namespace Ecomwise\Creditlimit\Block;

class History extends \Magento\Framework\View\Element\Template
{
    
    protected $helper;
    protected $customer;
        
    
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Ecomwise\Creditlimit\Helper\Data $helper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->helper = $helper;
        $this->customer = $this->helper->getCustomer('front');
    }
    
    public function getTotalOfOrders()
    {
        return $this->helper->getTotalOfOrders($this->customer);
    }
    
    public function getCreditLimit()
    {
            
        return $this->helper->getCustomerCreditLimit($this->customer);
    }
}
