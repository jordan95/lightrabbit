<?php
namespace Ecomwise\Creditlimit\Block\Info;

class Payonaccount extends \Magento\Payment\Block\Info
{
    protected $scopeConfig;
    
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\App\Config\MutableScopeConfigInterface $scopeConfig
    ) {
    
    
        $this->scopeConfig = $scopeConfig;
        parent::__construct($context);
    }
    
    public function getSpecificInformation()
    {
        parent::_prepareSpecificInformation();
        
        if ($this->scopeConfig->getValue(
            \Ecomwise\Creditlimit\Block\Form\Payonaccount::XML_PATH_PO_ENABLED,
            \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE
        )
            ) {
            $info = $this->getInfo();
            $this->_paymentSpecificInformation->addData([
                (string) __('Purchase Order Number') => $info->getCreditlimitPoNumber()
            ]);
        }
         
        return $this->_paymentSpecificInformation->getData();
    }
}
