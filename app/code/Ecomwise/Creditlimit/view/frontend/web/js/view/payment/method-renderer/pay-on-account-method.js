define(
    [
        'Magento_Checkout/js/view/payment/default',
        'jquery',
        'mage/validation'
    ],
    function (Component, $) {
        'use strict';
       
        return Component.extend({
            defaults: {
                template: 'Ecomwise_Creditlimit/payment/payonaccount-form',
                creditlimitPoNumber: ''
            },
            initObservable: function () {
            	this._super()
                    .observe('creditlimitPoNumber');
                return this;
            },
            getData: function () {
            	
                return {
                    "method": this.item.method,
                    //'creditlimit_po_number': this.creditlimitPoNumber(),
                    "additional_data": {'creditlimit_po_number':this.creditlimitPoNumber()}
                };

            },
			
			validate: function () {
                var form = 'form[data-role=payonaccount-form]';
                return $(form).validation() && $(form).validation('isValid');
           },
			            
            getPoRequired: function(){
            	return window.checkoutConfig.payment.pay_on_account.show_input_required;
            },
            
            getShowPoField: function(){
            	return window.checkoutConfig.payment.pay_on_account.show_input;
            },
            
            afterShow: function(){
            	var disable_method = window.checkoutConfig.payment.credit_limit.disable_method;
                var disable_msg = window.checkoutConfig.payment.credit_limit.disable_msg;
                var payment_method = window.checkoutConfig.payment.credit_limit.payment_method; 
                
                if(disable_method == "1"){
            		$("#"+payment_method).prop('disabled', 'disabled');
            		$("#"+payment_method).prop('checked', false);
            		$("#"+payment_method).parent().next('.payment-method-content').hide();
            		var title = $("label[for="+payment_method+"] span").text();
            		if(title.indexOf(disable_msg) <= 0 ){
            			$("label[for="+payment_method+"] span").text(title +" - "+ disable_msg);
            		}
            	}
            }
       });
    }
);
