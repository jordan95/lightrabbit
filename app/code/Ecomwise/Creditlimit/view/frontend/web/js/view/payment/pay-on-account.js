define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
           {
                type: 'pay_on_account',
                component: 'Ecomwise_Creditlimit/js/view/payment/method-renderer/pay-on-account-method'
            }
        );
        return Component.extend({});
    }
);