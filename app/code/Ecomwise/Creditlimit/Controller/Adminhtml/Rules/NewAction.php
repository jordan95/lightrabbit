<?php
namespace Ecomwise\Creditlimit\Controller\Adminhtml\Rules;

class NewAction extends \Magento\Backend\App\Action
{
    public function execute()
    {
        $this->_forward('edit');
    }
}
