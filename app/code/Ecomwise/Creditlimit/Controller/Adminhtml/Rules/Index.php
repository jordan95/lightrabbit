<?php
namespace Ecomwise\Creditlimit\Controller\Adminhtml\Rules;

class Index extends \Magento\Backend\App\Action
{
    
    public function execute()
    {
        if ($this->getRequest()->getQuery('ajax')) {
            $this->_forward('grid');
            return;
        }

        $this->_view->loadLayout();
        $this->_setActiveMenu('Ecomwise_Creditlimit::limits');
        $this->_view->getPage()->getConfig()->getTitle()->prepend(__('Credit Limit'));
        $this->_view->renderLayout();
    }
}
