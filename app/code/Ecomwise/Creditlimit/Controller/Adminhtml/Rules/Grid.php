<?php

namespace Ecomwise\Creditlimit\Controller\Adminhtml\Rules;

class Grid extends Index
{
    /**
     * Grid action
     *
     * @return void
     */
    public function execute()
    {
        $this->_view->loadLayout(false);
        $this->_view->renderLayout();
    }
}
