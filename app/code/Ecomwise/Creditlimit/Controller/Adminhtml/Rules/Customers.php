<?php

namespace Ecomwise\Creditlimit\Controller\Adminhtml\Rules;

use Magento\Backend\App\Action;
//use Magento\TestFramework\ErrorLog\Logger;

class Customers extends \Magento\Backend\App\Action
{

    protected $resultLayoutFactory;

    public function __construct(
        Action\Context $context,
        \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory
    ) {
        parent::__construct($context);
        $this->resultLayoutFactory = $resultLayoutFactory;
    }

    protected function _isAllowed()
    {
        return true;
    }

    public function execute()
    {
        $resultLayout = $this->resultLayoutFactory->create();
        $resultLayout->getLayout()->getBlock('creditlimit.edit.tab.customers')
                     ->setInCustomer($this->getRequest()->getPost('creditlimit_customer', null));

        return $resultLayout;
    }
}
