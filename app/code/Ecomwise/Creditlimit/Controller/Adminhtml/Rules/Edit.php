<?php

namespace Ecomwise\Creditlimit\Controller\Adminhtml\Rules;

use Magento\Framework\Registry;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Edit extends \Magento\Backend\App\Action
{
   
    protected $coreRegistry;
    protected $resultPageFactory;
    
    public function __construct(Context $context, Registry $coreRegistry, PageFactory $pageFactory)
    {
        parent::__construct($context);
        $this->coreRegistry = $coreRegistry;
        $this->resultPageFactory = $pageFactory;
    }
   
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        
        $model = $this->_objectManager->create('Ecomwise\Creditlimit\Model\Limits');
        if ($id) {
            try {
                $model->load($id);
                if (!$model->getId()) {
                    throw new \Exception('');
                }
            } catch (\Exception  $e) {
                $this->messageManager->addError(__('This Credit Limit no Longer Exists.'));
                $this->_redirect('*/*');
                return;
            }
        }

        $data = $this->_objectManager->get('Magento\Backend\Model\Session')->getPageData(true);
        if (!empty($data)) {
            $model->addData($data);
        }
        
        $this->coreRegistry->register('current_credit_limit', $model);
        
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->prepend(
            $model->getId() ? __('Edit Credit Limit') : __('New Credit Limit')
        );

        return $resultPage;
    }
}
