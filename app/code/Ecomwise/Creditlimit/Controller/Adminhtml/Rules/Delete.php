<?php

namespace Ecomwise\Creditlimit\Controller\Adminhtml\Rules;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Ecomwise\Creditlimit\Model\LimitsFactory;
use Ecomwise\Creditlimit\Model\RulesFactory;
use Magento\Customer\Model\CustomerFactory;
use Ecomwise\Creditlimit\Helper\Data;

class Delete extends Action
{

    protected $limitsFactory;
    protected $rulesFactory;
    protected $customerFactory;
    protected $helper;
    
    public function __construct(
        Context $context,
        LimitsFactory $limitsFactory,
        RulesFactory $rulesfactory,
        CustomerFactory $customerFactory,
        Data $helper
    ) {
    
        parent::__construct($context);
        $this->limitsFactory = $limitsFactory;
        $this->rulesFactory = $rulesfactory;
        $this->customerFactory = $customerFactory;
        $this->helper = $helper;
    }
    
    protected function _isAllowed()
    {
        return true;
    }
    
    
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            try {
                $rule_model = $this->rulesFactory->create();
                $customer_id = $rule_model->loadByRuleId($id);
                                
                $customer = $this->customerFactory->create()->load($customer_id);
                
                $total = (int) $this->helper->getTotalOfOrders($customer);
                
                if ($total > 0) {
                    throw new \Exception(
                        __("Cannot delete this credit limit. There is a used credit limit for this customer.")
                    );
                }
                                                
                $this->limitsFactory->create()->load($id)->delete();
                $rule_model->deleteByRuleId($id);
                $this->messageManager->addSuccess(__('Record Successfully Deleted'));
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
            }
        }
         return $resultRedirect->setPath('*/*/');
    }
}
