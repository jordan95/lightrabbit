<?php

namespace Ecomwise\Creditlimit\Controller\Adminhtml\Rules;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Ecomwise\Creditlimit\Model\LimitsFactory;
use Ecomwise\Creditlimit\Model\RulesFactory;
use Magento\Customer\Model\CustomerFactory;
use Ecomwise\Creditlimit\Helper\Data;

class Save extends Action
{
    protected $limitsFactory;
    protected $rulesFactory;
    protected $customerFactory;
    protected $helper;
  
    public function __construct(
        Context $context,
        LimitsFactory $limitsFactory,
        RulesFactory $rulesfactory,
        CustomerFactory $customerFactory,
        Data $helper
    ) {
    
        parent::__construct($context);
        $this->limitsFactory = $limitsFactory;
        $this->rulesFactory = $rulesfactory;
        $this->customerFactory = $customerFactory;
        $this->helper = $helper;
    }
  
    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return true;
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $resultRedirect = $this->resultRedirectFactory->create();
        
        if ($data) {
            try {
                $limit = $this->limitsFactory->create();
                $id = isset($data['id']) && !empty($data['id'])? $data['id'] : null;
                if ($id) {
                    $limit->load($id);
                }
    
                $limit->setData($data);
                
                $customer_id = (isset($data['cid']) && !empty($data['cid'])) ? $data['cid']: null;
                
                $rule_model = $this->rulesFactory->create();
                
                if (empty($customer_id) && $id) {
                    $customer_id = $rule_model->loadByRuleId($id);
                }
                
                if (empty($customer_id) && empty($id)) {
                    throw new \Exception(__("Customer is Not Selected"));
                }
                
                if (!empty($customer_id)) {
                    $rule_model = $rule_model->loadByCustomerId($customer_id);
                    if ($rule_model->getId()) {
                        if (empty($id) || $rule_model->getRuleId() != $id) {
                            throw new \Exception(__("Credit limit for customer already exist"));
                        }
                    }
                    
                    $customer_model = $this->customerFactory->create()->load($customer_id);
                    if (!$customer_model->getId()) {
                        throw new \Exception(__("Customer Does Not Exists"));
                    }
                    // check if the customer on which the rule applies have open amount larger than the limit amount
                    $total = (int) $this->helper->getTotalOfOrders($customer_model);
                    //\Zend_Debug::dump($total); die();
                    if ($total > 0 && $total > (int) $data['amount']) {
                        throw new \Exception(
                            __(
                                "Cannot edit the credit limit. The used amount of credit 
								 limit for the customer (%1) is larger than the entered credit limit.",
                                $total
                            )
                        );
                    }
                }
                
                $limit->save();
                
                
                if (!empty($customer_id)) {
                    $rule_model->deleteByRuleId($limit->getId());
                    unset($rule_model);
                    $rule_model_new  = $this->rulesFactory->create();
                    
                    $data_rule = [
                            'customer_id' => $customer_id,
                            'rule_id' => $limit->getId()
                    ];
                    $rule_model_new->addData($data_rule);
                    $rule_model_new->save();
                }
                
                $this->messageManager->addSuccess(__('Credit Limit is Saved.'));
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
                
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                if ($id) {
                    $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData($data);
                    return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
                } else {
                    return $resultRedirect->setPath('*/*/new', ['id' => $id]);
                }
            }
        }
        return $resultRedirect->setPath('*/*/');
    }
}
