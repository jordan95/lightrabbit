<?php
namespace Ecomwise\Creditlimit\Controller\Customer;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\Result\ForwardFactory;
//use Magento\Framework\Exception\NotFoundException;
//use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\ResultFactory;
use Ecomwise\Creditlimit\Helper\Data as Helper;

class Info extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;
    protected $resultForwardFactory;
    protected $resultFactory;
    protected $helper;
    
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        ForwardFactory $resultForwardFactory,
        Helper $helper
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->resultForwardFactory = $resultForwardFactory;
        $this->resultFactory = $context->getResultFactory();
        $this->helper = $helper;
        parent::__construct($context);
    }
    
    public function execute()
    {
         $should_hide = $this->helper->getShouldHideTab();
        if ($should_hide) {
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath("customer/account/index");
            return $resultRedirect;
        }
            
        $customerSession = $this->_objectManager->get('Magento\Customer\Model\Session');
        if (!$customerSession->isLoggedIn()) {
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setPath("customer/account/index");
            return $resultRedirect;
        }
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->set(__('Credit Limit'));
        if (!$resultPage) {
            $resultForward = $this->resultForwardFactory->create();
            return $resultForward->forward('noroute');
        }
        return $resultPage;
    }
}
