#README

###What is this repository for?
Ecomwise Credit Limit. This product allows Magento 2.x CE to set a customer specific credit limit and also global limit which will be used by all of the customers. Credit Limit 2.x adds a payment method "Pay on account"which is used to accept credit orders. If the credit limit is exceeded a message is shown on the Checkout page and customer can not place the order.

###Version
**2.0.2**

###How do I get set up?
- Unpack the extensions files in the magento root folder
- Enable the extension
	php bin/magento module:enable Ecomwise_Creditlimit
- Upgrade Magento
	php bin/magento setup:upgrade
- Recompile
	php bin/magento setup:di:compile
- Re-deploy static content (Optional)
	php bin/magento setup:static-content:deploy
- Reindexing (Optional)
	php bin/magento indexer:reindex
- Clear the Magento cache
	php bin/magento cache:clean
	
###Constraints


###Contribution guidelines
**Ecomwise Team, Ecomwise B.V.**

###Have questions?
**support@ecomwise.com**

###Full user manual
**http://support.ecomwise.com/solution/articles/3000060746-credit-limit-2-0**
