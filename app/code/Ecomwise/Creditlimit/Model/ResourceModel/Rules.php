<?php
namespace Ecomwise\Creditlimit\Model\ResourceModel;

use Ecomwise\Creditlimit\Helper\Log;

class Rules extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
        
    protected $logHelper;
    protected $ruleFactory;
    
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        Log $logHelper,
        \Ecomwise\Creditlimit\Model\RulesFactory $ruleFactory
    ) {
        $this->ruleFactory = $ruleFactory;
        $this->logHelper = $logHelper;
        parent::__construct($context, null);
    }
        
    protected function _construct()
    {
        $this->_init('ecomwise_credit_limit_customers', 'id');
    }
    
    public function deleteByRuleId($rule_id)
    {
        try {
            $table = $this->getMainTable();
            $where = [];
            $where[] =  $this->getConnection('core_write')->quoteInto('rule_id = ?', $rule_id);
            $result = $this->getConnection('core_write')->delete($table, $where);
        } catch (\Exception $e) {
            $this->logHelper->logMsg($e->getMessage(), Log::CREDITLIMIT_GENERAL_LOG_FILE);
            return false;
        }
        return $result;
    }
    
    public function loadByRuleId($rule_id)
    {
        try {
            $table = $this->getMainTable();
            $where = $this->getConnection('core_read')->quoteInto('rule_id = ?', $rule_id);
            $select = $this->getConnection('core_read')->select()->from($table, ['customer_id'])->where($where);
            $result = $this->getConnection('core_read')->fetchOne($select);
        } catch (\Exception $e) {
            $this->logHelper->logMsg($e->getMessage(), Log::CREDITLIMIT_GENERAL_LOG_FILE);
            return false;
        }
            
        return $result;
    }
    
    public function deleteByCustomerId($customer_id)
    {
        try {
            $table = $this->getMainTable();
            $where = [];
            $where[] =  $this->getConnection('core_write')->quoteInto('customer_id = ?', $customer_id);
            $result = $this->getConnection('core_write')->delete($table, $where);
        } catch (\Exception $e) {
            $this->logHelper->logMsg($e->getMessage(), Log::CREDITLIMIT_GENERAL_LOG_FILE);
            return false;
        }
            
        return $result;
    }
    
    public function loadByCustomerId($customer_id)
    {
        
        try {
            $model = $this->ruleFactory->create();
            $read = $this->getConnection('core_read');
            $table = $this->getMainTable();
        
            $query = $read->select()
                ->from($table)
                ->where('customer_id=?', $customer_id);
                
            $result = $read->fetchRow($query);
        } catch (\Exception $ex) {
            $this->logHelper->logMsg($e->getMessage(), Log::CREDITLIMIT_GENERAL_LOG_FILE);
            return false;
        }
        
        if ($result) {
            $model->load($result['id']);
        }
            //
        
        return $model;
    }
}
