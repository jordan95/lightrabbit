<?php
namespace Ecomwise\Creditlimit\Model\ResourceModel\Limits;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Ecomwise\Creditlimit\Model\Limits', 'Ecomwise\Creditlimit\Model\ResourceModel\Limits');
    }
}
