<?php
namespace Ecomwise\Creditlimit\Model\ResourceModel;

class Limits extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
        
    protected function _construct()
    {
        $this->_init('ecomwise_credit_limit', 'id');
    }
}
