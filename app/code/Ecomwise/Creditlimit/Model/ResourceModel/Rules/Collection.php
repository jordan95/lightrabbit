<?php
namespace Ecomwise\Creditlimit\Model\ResourceModel\Rules;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Ecomwise\Creditlimit\Model\Rules', 'Ecomwise\Creditlimit\Model\ResourceModel\Rules');
    }
}
