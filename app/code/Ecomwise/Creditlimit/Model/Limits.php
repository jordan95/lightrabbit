<?php
namespace Ecomwise\Creditlimit\Model;

class Limits extends \Magento\Framework\Model\AbstractModel
{
    
    protected $_cacheTag = 'customer_creditlimit';
    
    protected function _construct()
    {
        $this->_init('Ecomwise\Creditlimit\Model\ResourceModel\Limits');
    }
    
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId(), self::CACHE_TAG . '_' . $this->getId()];
    }
     
    public function getId()
    {
        return $this->getData('id');
    }
}
