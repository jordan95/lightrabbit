<?php
namespace Ecomwise\Creditlimit\Model\Source\Config;

class Payment implements \Magento\Framework\Option\ArrayInterface
{
    
    protected $methodConfig;

    public function __construct(\Magento\Payment\Model\Config $methodConfig)
    {
        $this->methodConfig = $methodConfig;
    }
    public function toOptionArray()
    {
        $methods = [];
        foreach ($this->methodConfig->getActiveMethods() as $method) {
            $methods [] = ['value'=>$method->getCode(), 'label'=>$method->getTitle()];
        }
    
        return $methods;
    }
}
