<?php
namespace Ecomwise\Creditlimit\Model\Source\Config;

class Orderstatus implements \Magento\Framework\Option\ArrayInterface
{

    protected $statusCollectionFactory;
    
    public function __construct(
        \Magento\Sales\Model\ResourceModel\Order\Status\CollectionFactory $statusCollectionFactory
    ) {
    
        $this->statusCollectionFactory = $statusCollectionFactory;
    }
          
    public function toOptionArray()
    {
        $statuses = [];
        $collection = $this->statusCollectionFactory->create();
        foreach ($collection as $status) {
            $statuses [] = ['value'=> $status->getStatus(), 'label'=> $status->getLabel()];
        }
        return $statuses;
    }
}
