<?php
namespace Ecomwise\Creditlimit\Model\Source\Config;

class Pon implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray()
    {
        return [
            ['value'=>'2', 'label'=>__('Enabled and mandatory')],
            ['value'=>'1', 'label'=>__('Enabled')],
            ['value'=>'0', 'label'=>__('Disabled')],
        ];
    }
}
