<?php
namespace Ecomwise\Creditlimit\Model\Source\Config;

class Customergroups implements \Magento\Framework\Option\ArrayInterface
{

    protected $groupCollectionFactory;
    
    public function __construct(\Magento\Customer\Model\ResourceModel\Group\CollectionFactory $groupCollectionFactory)
    {
        $this->groupCollectionFactory = $groupCollectionFactory;
    }
     
    public function toOptionArray()
    {
        $groups = [];
        $collection = $this->groupCollectionFactory->create();
        foreach ($collection as $group) {
            $statuses [] = ['value'=> $group->getCustomerGroupId(), 'label'=> $group->getCustomerGroupCode()];
        }
        return $statuses;
    }
}
