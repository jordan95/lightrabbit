<?php

namespace Ecomwise\Creditlimit\Model\Plugins\Payment\Convert;

class Plugin
{
    protected $checkoutSession;
    
    public function aroundConvert($subject, $proceed, $object, $data = [])
    {
        $result = $proceed($object, $data);
        $result->setCreditlimitPoNumber($object->getCreditlimitPoNumber());
        return $result;
    }
}
