<?php
namespace Ecomwise\Creditlimit\Model\Plugins\Config;

class Plugin
{
    protected $_resource;
    protected $_appConfig;
    protected $_helper;
	protected $datetime;
    
    public function __construct(
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Framework\App\Config\ReinitableConfigInterface $config,
        \Ecomwise\Creditlimit\Helper\Data $helper,
       \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone
    ) {
        $this->_appConfig = $config;
        $this->_resource = $resource;
        $this->_helper = $helper;
		$this->datetime = $timezone;
		
    }
    public function aroundSave(
        \Magento\Config\Model\Config $subject,
        \Closure $proceed
    ) {
        $section = $subject->getSection();
        
        if ($section !== "creditlimit") {
            return $proceed();
        }
        
        $groups = $subject->getGroups();
                
        if (empty($groups)) {
            return $proceed();
        }
    
        $payment_method = $subject->getConfigDataValue(
            \Ecomwise\Creditlimit\Helper\Data::EXT_CREDITLIMIT_PAYMENT_METHOD
        );
        $order_status = $subject->getConfigDataValue(
            \Ecomwise\Creditlimit\Helper\Data::EXT_CREDITLIMIT_OPEN_ORDER_STATUS_PATH
        );
        $enable_disable = $subject->getConfigDataValue(
            \Ecomwise\Creditlimit\Helper\Data::EXT_CREDITLIMIT_ENABLED_CONFIG_PATH
        );
        
        $old_store_config = $this->getStoresConfig();
        
        $proceed();
        
        $new_store_config = $this->getStoresConfig();
                
        foreach ($old_store_config as $store => $data) {
            if ($store == 0) {
                continue;
            }
			
			$timezone = $this->datetime->getConfigTimezone('store', $store);
			
			/*
			$date = new \DateTime();
			$date->setTimezone(new \DateTimeZone($timezone));
			
			$date_formated = $date->format("Y-m-d H:i:s");
			*/
			$date_formated = date("Y-m-d H:i:s");
            $new_data = $new_store_config[$store];
            if (($data['enabled'] != $new_data['enabled'])
                    || ($data['payment_method'] != $new_data['payment_method'])
                    || ($data['order_status'] != $new_data['order_status'])) {
                $this->saveConfig($store, $date_formated);
            }
        }
        
        return $subject;
    }
    
    protected function saveConfig($store, $date)
    {
        if (empty($store)) {
            return false;
        }
        
        $connection = $this->_resource->getConnection('core/write');
        $config_table = $this->_resource->getTableName('core_config_data');
        $path = \Ecomwise\Creditlimit\Helper\Data::EXT_CREDITLIMIT_LAST_CHANGE_DATE;
        
        try {
            $config_query = $connection->select()
                                       ->from($config_table, ["*"])
                                       ->where('path=?', $path)
                                       ->where('scope=?', 'stores')
                                       ->where('scope_id=?', $store);
        
            $row = $connection->fetchAll($config_query);
            if ($row) {
                $connection->update(
                    $config_table,
                    ['value' => $date],
                    [
                        'path = ?' => $path,
                        'scope = ?' => 'stores',
                        'scope_id = ?' => $store
                    ]
                );
            } else {
                $connection->insert(
                    $config_table,
                    ['scope_id' => $store, 'scope' => 'stores', 'path' => $path, 'value' => $date]
                );
            }
        } catch (\Exception $e) {
            \Ecomwise\Creditlimit\Helper\Log::logMsg($e->getMessage());
        }
    }
    
    public function getStoresConfig()
    {
        $connection = $this->_resource->getConnection('core/read');
        $stores_table = $this->_resource->getTableName('store');
        
        $data_prepared = [];
        $query = $connection->select()->from($stores_table, ['store_id']);
        $stores = $connection->fetchCol($query);
        
        foreach ($stores as $store) {
            $store_data = [];
            $store_data['payment_method'] = $this->_helper->getCreditlimitPaymentMethod($store);
            $store_data['enabled'] = $this->_helper->getCreditlimitEnabled($store);
            $store_data['order_status'] = $this->_helper->getOrderSatatus($store);
            
            $data_prepared[$store] = $store_data;
        }
        
        return $data_prepared;
    }
}
