<?php
namespace Ecomwise\Creditlimit\Model;

class Rules extends \Magento\Framework\Model\AbstractModel
{
    
    protected $_cacheTag = 'customer_creditlimit_rules';
    
    protected function _construct()
    {
        $this->_init('Ecomwise\Creditlimit\Model\ResourceModel\Rules');
    }
    
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId(), self::CACHE_TAG . '_' . $this->getId()];
    }
     
    public function deleteByRuleId($rule_id)
    {
        return $this->getResource()->deleteByRuleId($rule_id);
    }
    
    public function loadByRuleId($rule_id)
    {
        return $this->getResource()->loadByRuleId($rule_id);
    }
    
    public function deleteByCustomerId($customer_id)
    {
        return $this->getResource()->deleteByCustomerId($customer_id);
    }
     
    public function loadByCustomerId($customer_id)
    {
        return $this->getResource()->loadByCustomerId($customer_id);
    }
}
