<?php
namespace Ecomwise\Creditlimit\Model\Payment\Method;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Payment\Helper\Data as PaymentHelper;
use Magento\Framework\App\Config\MutableScopeConfigInterface;
use Ecomwise\Creditlimit\Helper\Data as Helper;

class PayConfig implements ConfigProviderInterface
{
    
     /**
      * @var string[]
      */
    protected $_methodCode = Pay::PAYMENT_METHOD_PAYONACCOUNT_CODE;

    /**
     * @var Pay on account
     */
    protected $_method;

    protected $_scopeConfig;
    
    protected $_poNumberEnabled;
    
    protected $_helper;
    
    const XML_PATH_PO_DISABLE_MSG = "creditlimit/parameters_creditlimit/message";
    const XML_PATH_CREDITLIMIT_PAYMENT_METHOD = "creditlimit/parameters_creditlimit/payment_method";
    
    /**
     * @param PaymentHelper $paymentHelper
     */
    public function __construct(
        PaymentHelper $paymentHelper,
        MutableScopeConfigInterface $_scopeConfig,
        Helper $helper
    ) {
    
        $this->_method = $paymentHelper->getMethodInstance($this->_methodCode);
        $this->_scopeConfig = $_scopeConfig;
        $this->_helper = $helper;
        $this->_poNumberEnabled = $this->_scopeConfig->getValue(
            \Ecomwise\Creditlimit\Block\Form\Payonaccount::XML_PATH_PO_ENABLED,
            \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE
        );
    }

    public function getConfig()
    {
        $store = $this->_helper->getRealQuoteStoreId();
        $payment_method =   $this->_helper->getCreditlimitPaymentMethod($store);
        $disable_msg = $this->getDisableMsg();
        $disable_method = $this->getDisableMethod();
        $config = [
                    'payment'=>[
                        'credit_limit'=>[
                            'payment_method'=> $payment_method,
                            'disable_msg' => $disable_msg,
                            'disable_method'=> $disable_method
                        ]
                    ]
                ];
            
        if ($this->_method->isAvailable()) {
            $config ['payment'] ['pay_on_account'] = [
                    'show_input' => $this->getShowPoInput(),
                    'show_input_required' => $this->getShowPoInputRequired()
            ];
        }
        
        return $config;
    }
    
    protected function getShowPoInput()
    {
        return ($this->_poNumberEnabled != "0")? true : false;
    }
    
    protected function getShowPoInputRequired()
    {
        return ($this->_poNumberEnabled == "2")? true : false;
    }
    
    public function getDisableMsg()
    {
        $area = $this->_helper->getArea();
        if ($area === "front") {
            $customer = $this->_helper->getCustomer($area);
            if (!$customer || !$customer->getId()) {
                return __("Credit Limit is not available for guest customers.");
            }
        }
        return __((string)$this->_scopeConfig->getValue(
            self::XML_PATH_PO_DISABLE_MSG,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        ));
    }
    
    function getDisableMethod()
    {
        return (int) $this->_helper->getDisableStatus();
    }
}
