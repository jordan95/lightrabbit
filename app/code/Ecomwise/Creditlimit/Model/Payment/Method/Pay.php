<?php
namespace Ecomwise\Creditlimit\Model\Payment\Method;

class Pay extends \Magento\Payment\Model\Method\AbstractMethod
{
    
    const PAYMENT_METHOD_PAYONACCOUNT_CODE = 'pay_on_account';
    
    protected $_code = self::PAYMENT_METHOD_PAYONACCOUNT_CODE;
    
    protected $_isOffline = true;
    protected $_isInitializeNeeded = false;
    
    protected $_formBlockType = 'Ecomwise\Creditlimit\Block\Form\Payonaccount';
    protected $_infoBlockType = 'Ecomwise\Creditlimit\Block\Info\Payonaccount';
            
    public function assignData(\Magento\Framework\DataObject $data)
    {
        $additional_data = $data->getAdditionalData();
         $this->getInfoInstance()->addData(
             ['creditlimit_po_number' => $additional_data['creditlimit_po_number']]
         );
        
        return $this;
    }
}
