<?php
namespace Ecomwise\Creditlimit\Model\Limits;

use Ecomwise\Creditlimit\Model\ResourceModel\Limits\CollectionFactory;
use Magento\Framework\Registry;
use Ecomwise\Creditlimit\Model\ResourceModel\Rules\CollectionFactory as RulesFactory;

class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    protected $collection;
    
    protected $coreRegistry;

    protected $loadedData;
    
    protected $rulesFactory;
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $limitsCollectionFactory,
        Registry $coreRegistry,
        RulesFactory $rulesFactory,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $limitsCollectionFactory->create();
        $this->coreRegistry = $coreRegistry;
        $this->rulesFactory = $rulesFactory;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->meta = $this->prepareMeta($this->meta);
    }

    public function prepareMeta(array $meta)
    {
        return $meta;
    }

    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        foreach ($items as $limit) {
            $this->loadedData[$limit->getId()] = $limit->getData();
        }
        
        $model = $this->coreRegistry->registry('current_credit_limit');
        
        if ($model) {
            $limit = $this->collection->getNewEmptyItem();
            $limit->setData($model->getData());
            
            $rules_collection = $this->rulesFactory->create()
                ->addFieldToFilter("rule_id", ['eq'=> $model->getId()])
                ->addFieldToSelect("customer_id");
            
            if (!empty($rules_collection)) {
                $rule = $rules_collection->getFirstItem();
                $limit->setData('customer_id', $rule->getCustomerId());
            }
            
            $this->loadedData[$limit->getId()] = $limit->getData();
        }

        return $this->loadedData;
    }
}
