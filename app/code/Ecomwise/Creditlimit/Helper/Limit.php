<?php
namespace Ecomwise\Creditlimit\Helper;

class Limit extends \Magento\Framework\App\Helper\AbstractHelper
{
        
    protected $rulesCollectionFactory;
        
    public function __construct(
        \Ecomwise\Creditlimit\Model\ResourceModel\Rules\CollectionFactory $collectionFactory,
        \Magento\Framework\App\Helper\Context $context
    ) {
    
        parent::__construct($context);
        $this->rulesCollectionFactory = $collectionFactory;
    }
    
    public function getRuleByCustomer($customer_id)
    {
            
        $rules_collection = $this->rulesCollectionFactory->create()
            ->addFieldToFilter("customer_id", ['eq'=> $customer_id ])
            ->addFieldToSelect('rule_id');
            
        if (!empty($rules_collection)) {
            return $rules_collection->getFirstItem();
        }
            
        return false;
    }
}
