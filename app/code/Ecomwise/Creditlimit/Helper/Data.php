<?php
namespace Ecomwise\Creditlimit\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $resource;
    protected $customerSession;
    protected $logger;
    protected $customerFactory;
    protected $checkoutSession;
    protected $objectManager;
    protected $backendUrl;
    protected $appState;
    
    const EXT_CREDITLIMIT_ENABLED_CONFIG_PATH = "creditlimit/general/enabled";
    const EXT_CREDITLIMIT_OPEN_ORDER_STATUS_PATH = "creditlimit/parameters_creditlimit/order_status_open";
    const EXT_CREDITLIMIT_PAYMENT_METHOD = "creditlimit/parameters_creditlimit/payment_method";
    const EXT_CREDITLIMIT_GLOBAL_CREDIT = "creditlimit/parameters_creditlimit/global_limit";
    const EXT_CREDITLIMIT_HIDE_FOR_GROUPS = "creditlimit/parameters_creditlimit/hide_groups";
    const EXT_CREDITLIMIT_HIDE_FOR_EMPTY = "creditlimit/parameters_creditlimit/hide_no_credit";
    const EXT_CREDITLIMIT_LAST_CHANGE_DATE = "creditlimit/parameters_creditlimit/last_change_date";
    
    
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Backend\Model\UrlInterface $backendUrl,
        \Magento\Framework\App\State $appState,
        Log $logger
        ) {
            
            parent::__construct($context);
            $this->resource = $resource;
            $this->customerSession = $customerSession;
            $this->logger = $logger;
            $this->customerFactory = $customerFactory;
            $this->checkoutSession = $checkoutSession;
            $this->objectManager = $objectManager;
            $this->backendUrl = $backendUrl;
            $this->appState = $appState;
    }
    
    public function getCustomerGroup(){
        return $this->customerSession->getCustomer()->getGroupId();
    }
    
    public function getTotalOfOrders($customer)
    {
        if (!$customer || !$customer->getId()) {
            return false;
        }
        
        $total_sum = 0;
        try {
            $read = $this->resource->getConnection('core/read');
            //$config_data_prepared = $this->getConfigArrayPrepared($read);
            
            $store_ids_query = $read->select()->from($this->resource->getTableName('store'), ['store_id']);
            $stores = $read->fetchCol($store_ids_query);
            
            $class = get_class($read);
            
            foreach ($stores as $sid) {
                if ($sid == 0) {
                    continue;
                }
                $order_status = explode(",", (string) $this->scopeConfig->getValue(
                    self::EXT_CREDITLIMIT_OPEN_ORDER_STATUS_PATH,
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                    $sid
                    ));
                $creditlimit_method = $this->scopeConfig->getValue(
                    self::EXT_CREDITLIMIT_PAYMENT_METHOD,
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                    $sid
                    );
                
                $store_credit_enabled = $this->scopeConfig->getValue(
                    self::EXT_CREDITLIMIT_ENABLED_CONFIG_PATH,
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                    $sid
                    );
                
                
                if ($store_credit_enabled === "1") {
                    $select_total = $read->select()
                    ->from(['s' => $this->resource->getTableName('sales_order')], ['total'=> new \Zend_Db_Expr('SUM(s.grand_total)')])
                    ->joinLeft(['sp'=>$this->resource->getTableName('sales_order_payment')], 's.entity_id=sp.parent_id', ['method'])
                    ->where('s.customer_id=?', $customer->getId())
                    ->where('s.status IN(?)', $order_status)
                    ->where('s.store_id=?', $sid);
                    
                    $last_config_change_date = $this->scopeConfig->getValue(
                        self::EXT_CREDITLIMIT_LAST_CHANGE_DATE,
                        \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                        $sid
                        );
                    
                    if (!empty($last_config_change_date)) {
                        $select_total->where('s.created_at >= ?', $last_config_change_date);
                    }
                    $select_total->where('sp.method=?', $creditlimit_method);
                    $total_row =  $read->fetchRow($select_total);
                    $total_sum += (float) $total_row['total'];
                    
                    unset($select_total);
                }
            }
        } catch (\Exception $e) {
            $this->logger->logMsg($e->getMessage(), "creditlimit-log.log");
            return false;
        }
        
        return $total_sum;
    }
    
    public function getCustomer($section)
    {
        if ($section == 'front') {
            return $this->customerSession->getCustomer();
        } elseif ($section == 'admin') {
            $customer_data =  $this->objectManager->get('Magento\Backend\Model\Session\Quote')
            ->getQuote()
            ->getCustomer();
            return $this->customerFactory->create()->load($customer_data->getId());
        } else {
            return $this->customerFactory->create()->load($section);
        }
        
        return false;
    }
    
    public function getDisableStatus($section = 'front')
    {
        //general extension enabled
        $sid = $this->getRealQuoteStoreId();
        $extension_enabled =  (bool) $this->scopeConfig->getValue(
            self::EXT_CREDITLIMIT_ENABLED_CONFIG_PATH,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $sid
            );
        if (!$extension_enabled) {
            return false;
        }
        
        //customer check
        $customer = $this->getCustomer($section);
        if (!$customer || !$customer->getId()) {
            return true;
        }
        
        //total orders check
        $total_orders = $this->getTotalOfOrders($customer);
        if ($total_orders === false) {
            return  true;
        }
        
        if ($section == 'admin') {
            $admin_session = $this->objectManager->get('Magento\Backend\Model\Session\Quote');
            $total_cart = (float) $admin_session->getQuote()->getGrandTotal();
        } else {
            $total_cart = (float) $this->checkoutSession->getQuote()->getGrandTotal();
        }
        
        $credit_customer = (float) $this->getCustomerCreditLimit($customer);
        
        return ($credit_customer < ($total_orders + $total_cart))? true : false;
    }
    
    public function getCustomersGridUrl()
    {
        return $this->backendUrl->getUrl('creditlimit/rules/customers', ['_current' => true]);
    }
    
    public function getCustomerCreditLimit($customer)
    {
        
        $general_limit = $this->scopeConfig->getValue(
            self::EXT_CREDITLIMIT_GLOBAL_CREDIT,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            );
        
        $credit_rule = $this->objectManager->get('Ecomwise\Creditlimit\Model\Rules')
        ->loadByCustomerId($customer->getId());
        
        if ($credit_rule && $credit_rule->getId()) {
            $limit = $this->objectManager->create('Ecomwise\Creditlimit\Model\Limits')
            ->load($credit_rule->getRuleId());
            if ($limit->getId()) {
                return $limit->getAmount();
            }
        }
        
        
        if (!empty($general_limit) && is_numeric($general_limit)) {
            return $general_limit;
        }
        
        if (!$customer || !$customer->getId()) {
            return false;
        }
        

        

        
        return 0;
    }
    
    public function getShouldHideTab()
    {
        $extension_enabled =  (bool) $this->scopeConfig->getValue(
            self::EXT_CREDITLIMIT_ENABLED_CONFIG_PATH,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            );
        
        if (!$extension_enabled) {
            return true;
        }
        
        $customer = $this->getCustomer('front');
        
        $hide_customer_groups = explode(",", $this->scopeConfig->getValue(
            self::EXT_CREDITLIMIT_HIDE_FOR_GROUPS,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            ));
        if (is_array($hide_customer_groups) &&
            !empty($hide_customer_groups) &&
            in_array($customer->getGroupId(), $hide_customer_groups)
            ) {
                return true;
            }
            
            
            $credit_rule = $this->objectManager->get('Ecomwise\Creditlimit\Model\Rules')
            ->loadByCustomerId($customer->getId());
            
            $hide_no_credit = $this->scopeConfig->getValue(
                self::EXT_CREDITLIMIT_HIDE_FOR_EMPTY,
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                );
            
            if ((!$credit_rule || !$credit_rule->getId()) && $hide_no_credit) {
                $global_credit = $this->scopeConfig->getValue(
                    self::EXT_CREDITLIMIT_GLOBAL_CREDIT,
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                    );
                if ($global_credit === null || !is_numeric($global_credit)) {
                    return true;
                }
            }
            
            return false;
    }
    
    public function getCreditlimitPaymentMethod($store = null)
    {
        return   $this->scopeConfig->getValue(
            self::EXT_CREDITLIMIT_PAYMENT_METHOD,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
            );
    }
    public function getCreditlimitEnabled($store = null)
    {
        return  (bool) $this->scopeConfig->getValue(
            self::EXT_CREDITLIMIT_ENABLED_CONFIG_PATH,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
            );
    }
    
    public function getOrderSatatus($store = null)
    {
        return  $this->scopeConfig->getValue(
            self::EXT_CREDITLIMIT_OPEN_ORDER_STATUS_PATH,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
            );
    }
    
    
    public function getArea()
    {
        try {
            $area_code  = $this->appState->getAreaCode();
            if ($area_code == \Magento\Backend\App\Area\FrontNameResolver::AREA_CODE) {
                return "admin";
            } else {
                return  "front";
            }
        } catch (\Exception $e) {
            $this->logger->logMsg($e->getMessage(), "creditlimit-log.log");
        }
        
        return false;
    }
    
    public function getQuote()
    {
        $area = $this->getArea();
        if ($area == "admin") {
            $admin_session = $this->objectManager->get('Magento\Backend\Model\Session\Quote');
            return $admin_session->getQuote();
        }
        
        return $this->checkoutSession->getQuote();
    }
    
    public function getRealQuoteStoreId()
    {
        $quote = $this->getQuote();
        if (!$quote) {
            return null;
        }
        
        if (!$quote->getId()) {
            return $quote->getStore()->getid();
        }
        try {
            $qquery = $this->resource->getConnection('core/read')
            ->select()
            ->from($this->resource->getTableName('quote'), ['store_id'])->where('entity_id=?', $quote->getId());
            $qid = $this->resource->getConnection('core/read')->fetchOne($qquery);
            return $qid;
        } catch (\Exception $e) {
            $this->logger->logMsg($e->getMessage(), "creditlimit-log.log");
        }
        
        return null;
    }
}
