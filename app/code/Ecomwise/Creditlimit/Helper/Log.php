<?php
namespace Ecomwise\Creditlimit\Helper;

class Log extends \Magento\Framework\App\Helper\AbstractHelper
{
    
    const CREDITLIMIT_GENERAL_LOG_FILE = "creditlimit-log.log";
    
    public function __construct(
        \Magento\Framework\App\Helper\Context $context
    ) {
        $this->context = $context;
        parent::__construct($context);
    }
    
    public function log($msg, $file)
    {
        self::logMsg($msg, $file);
    }
    
    public static function logMsg($msg, $file = self::CREDITLIMIT_GENERAL_LOG_FILE)
    {
        try {
            $fn = @fopen(BP . '/var/log/'.$file, 'a');
            if (!$fn) {
                throw new \Zend\Log\Exception\InvalidArgumentException(
                    sprintf("Can not open file %s", BP . '/var/log/'.$file)
                );
            }
            $writer = new \Zend\Log\Writer\Stream($fn);
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);
            $logger->info($msg);
        } catch (\Zend\Log\Exception\InvalidArgumentException $ex) {
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/logger.log');
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);
            $logger->info($ex->getMessage());
        }
    }
}
