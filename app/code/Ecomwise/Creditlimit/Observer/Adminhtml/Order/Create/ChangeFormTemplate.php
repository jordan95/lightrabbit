<?php
namespace Ecomwise\Creditlimit\Observer\Adminhtml\Order\Create;

use Magento\Framework\Event\ObserverInterface;

class ChangeFormTemplate implements ObserverInterface
{
    
    protected $helper;
            
    public function __construct(\Ecomwise\Creditlimit\Helper\Data $helper)
    {
        $this->helper = $helper;
    }
            
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $block = $observer->getBlock();
        //
        if ($block instanceof \Magento\Sales\Block\Adminhtml\Order\Create\Billing\Method) {
            $disable_status = $this->helper->getDisableStatus('admin');
            $quote = $block->getQuote();
            $store = $quote?  $quote->getStore()->getId() : null;
            $payment_method = $this->helper->getCreditlimitPaymentMethod($store);
                
            $block->setTemplate('Ecomwise_Creditlimit::order/create/billing/method/form.phtml');
            $block->setCreditlimitDisableMethod($disable_status);
            $block->setCreditlimitPaymentMethod($payment_method);
            $block->setDisableMassageAdmin(__('Customer has exceeded credit limit'));
        }
         
        return $this;
    }
}
