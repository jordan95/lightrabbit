<?php
namespace Ecomwise\Creditlimit\Observer\Customer;

use Magento\Framework\Event\ObserverInterface;

class DeleteAfter implements ObserverInterface
{
    protected $resource;
    protected $storeManager;
    
    public function __construct(
        \Magento\Framework\App\ResourceConnection $resource
    ) {
    
        $this->resource = $resource;
    }
        
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $customer = $observer->getCustomer();
        if (!$customer || !$customer->getId()) {
            return $this;
        }
        
        $cid = $customer->getId();
        
        try {
            $write = $this->resource->getConnection('core/write');
            $creditlimt = $this->resource->getTableName('ecomwise_credit_limit');
            $creditlimt_customer = $this->resource->getTableName('ecomwise_credit_limit_customers');
            $row = $write->select()
                         ->from($creditlimt_customer, ['rule_id'])
                         ->where('customer_id=?', $cid);
            $rule_id = $write->fetchOne($row);
            
            if ($rule_id) {
                $write->delete(
                    $creditlimt_customer,
                    ['customer_id = ?' => $cid]
                );
                
                $write->delete(
                    $creditlimt,
                    ['id = ?' => $rule_id]
                );
            }
        } catch (\Exception $e) {
            \Ecomwise\Creditlimit\Helper\Log::logMsg($e->getMessage());
        }
        
        return $this;
    }
}
