<?php
namespace Ecomwise\Creditlimit\Observer\PaymentMethod;

use Magento\Framework\Event\ObserverInterface;

class Available implements ObserverInterface
{
    protected $helper;
    protected $resource;
    protected $storeManager;
    
    public function __construct(
        \Magento\Framework\App\ResourceConnection $resource,
        \Ecomwise\Creditlimit\Helper\Data $helper,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
        ) {
            
            $this->helper = $helper;
            $this->resource = $resource;
            $this->storeManager = $storeManager;
            $this->scopeConfig = $scopeConfig;
    }
    
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $result = $observer->getResult();
        $quote  = $observer->getQuote();
        $method = $observer->getMethodInstance();
        
        $website = $quote ?  $quote->getStore()->getWebsite()->getId() : null;
        $store = $quote ? $quote->getStore()->getId() : null;
        
        if (empty($store) || empty($website)) {
            return $this;
        }
        $creditlimit_data = $this->getCreditlimitConfigData($website, $store);
        
        $enabled_for_store = $this->helper->getCreditlimitEnabled($store);
        $payment_method_store = $this->helper->getCreditlimitPaymentMethod($store);
        
        $method_code =  $method->getCode();
        foreach ($creditlimit_data as $sid => $data) {
            if (($data['payment_method'] == $method_code) && ($data['enabled'] == "1")
                && ($method_code != $payment_method_store)) {
                    $result->setData('is_available', false);
                }
        }
        
        $moduleStatus = $this->helper->getCreditlimitEnabled();
        $hidenGroups = str_split($this->scopeConfig->getValue('creditlimit/parameters_creditlimit/hide_groups', \Magento\Store\Model\ScopeInterface::SCOPE_STORE));
        $customerGroup = $this->helper->getCustomerGroup();
        
        if (!$moduleStatus && ($method_code == 'pay_on_account'))
        {
            $checkResult = $result;
            $checkResult->setData('is_available', false);
        }
        
        if(in_array($customerGroup, $hidenGroups) && $method_code == 'pay_on_account' ){
            $checkResult = $result;
            $checkResult->setData('is_available', false);
        }
        
        return $this;
    }
    
    protected function getCreditlimitConfigData($website, $store)
    {
        $data_prepared = [];
        if (empty($website)) {
            return $data_prepared;
        }
        $read = $this->resource->getConnection('core/read');
        $select = $read->select()->from($this->resource->getTableName('store'), ['store_id'])
        ->where('website_id=?', $website);
        $stores = $read->fetchCol($select);
        foreach ($stores as $sid) {
            if ($sid == $store) {
                continue;
            }
            $enabled_for_store = $this->helper->getCreditlimitEnabled($sid);
            $payment_method = $this->helper->getCreditlimitPaymentMethod($sid);
            $data_prepared[$sid] = ['enabled' => $enabled_for_store, 'payment_method' => $payment_method];
        }
        return $data_prepared;
    }
}