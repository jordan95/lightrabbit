<?php
namespace Ecomwise\Creditlimit\Observer\PaymentMethod;

use Magento\Framework\Event\ObserverInterface;

class Place implements ObserverInterface
{
	protected $resource;
	
	public function __construct(
			\Magento\Framework\App\ResourceConnection $resource
			) {
				
				$this->resource = $resource;
	}
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
    	$payment = $observer->getPayment();
    	$order = $payment->getOrder();
    	$methodInstance = $payment->getMethodInstance();
    	if($methodInstance && $methodInstance->getCode() == "pay_on_account"){
    		$orderStatus = $methodInstance->getConfigData('order_status');
    		$orderState = $this->getStateForStatus($orderStatus);
    		if(!empty($orderStatus) && !empty($orderState)){
    			$order->setState($orderState);
    			$order->setStatus($orderStatus);
    		}
    	}    	
    }
    
    protected function getStateForStatus($orderStatus){
    	try{
    		
    		$read = $this->resource->getConnection('core/read');
    		$select = $read->select()
    					   ->from($this->resource->getTableName('sales_order_status_state'), ['state'])
    					   ->where('status=?', $orderStatus);
    		$state = $read->fetchOne($select);
    		return $state;
    	}catch(\Exception $e){
    		
    	}
    	
        return false;
    }


}
