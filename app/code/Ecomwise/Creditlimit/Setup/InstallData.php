<?php
namespace Ecomwise\Creditlimit\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
    
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
       
        $data [] = ['status' => 'payonaccount_pending', 'label' => 'Placed by Credit Limit'];
        $setup->getConnection()->insertArray($setup->getTable('sales_order_status'), ['status', 'label'], $data);
        
        $data_state [] = ['status' => 'payonaccount_pending', 'state' => 'new',
                'is_default' => 0 , 'visible_on_front'=> 1];
        
        $setup->getConnection()->insertArray(
            $setup->getTable('sales_order_status_state'),
            ['status', 'state', 'is_default', 'visible_on_front'],
            $data_state
        );
        
        $setup->endSetup();
    }
}
