<?php
namespace Ecomwise\Creditlimit\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Adapter\AdapterInterface;

class InstallSchema implements InstallSchemaInterface
{
        
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();
        
        $table_credit = $installer->getConnection()->newTable(
            $installer->getTable('ecomwise_credit_limit')
        )->addColumn(
            'id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Id'
        )->addColumn(
            'amount',
            \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
            '12,4',
            ['nullable' => false, 'default' => '0.0000'],
            'Amount'
        )->setComment(
            'Credit Limit Table'
        );
        $installer->getConnection()->createTable($table_credit);
                
        $table_credit_customer = $installer->getConnection()->newTable(
            $installer->getTable('ecomwise_credit_limit_customers')
        )->addColumn(
            'id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Id'
        )->addColumn(
            'rule_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['nullable' => false],
            'Rule Id'
        )->addColumn(
            'customer_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['nullable' => false],
            'Customer Id'
        )->setComment(
            'Credit Limit Table'
        );
        $installer->getConnection()->createTable($table_credit_customer);
      
        $installer->getConnection()->addColumn(
            $installer->getTable('quote_payment'),
            'creditlimit_po_number',
            [
                      'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                      'nullable' => true,
                      'length' => '255',
                      'comment' => 'Creditlimit Po NUmber'
                        
                  ]
        );
           
        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order_payment'),
            'creditlimit_po_number',
            [
                      'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                      'nullable' => true,
                      'length' => '255',
                      'comment' => 'Creditlimit Po NUmber'
                        
                   ]
        );
         
        $installer->endSetup();
    }
}
