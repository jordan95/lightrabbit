<?php
/**
 *
 * Copyright © 2017 CommerceExtensions. All rights reserved.
 * See COPYING.txt for license details.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 */
namespace CommerceExtensions\Mergecustomers\Block\Adminhtml\Edit\Tab;

use Magento\Customer\Controller\RegistryConstants;

/**
 * Adminhtml customer orders grid block
 */
class Customers extends \Magento\Backend\Block\Widget\Grid\Extended
{
    
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var  \Magento\Framework\View\Element\UiComponent\DataProvider\CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Magento\Framework\View\Element\UiComponent\DataProvider\CollectionFactory $collectionFactory
     * @param \Magento\Sales\Helper\Reorder $salesReorder
     * @param \Magento\Framework\Registry $coreRegistry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Framework\View\Element\UiComponent\DataProvider\CollectionFactory $collectionFactory,
        \Magento\Framework\Registry $coreRegistry,
        array $data = []
    ) {
        $this->_coreRegistry = $coreRegistry;
        $this->_collectionFactory = $collectionFactory;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * {@inheritdoc}
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('merge_customer_grid');
        $this->setDefaultSort('created_at', 'desc');
        $this->setUseAjax(true);
    }

    /**
     * Apply various selection filters to prepare the sales order grid collection.
     *
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = $this->_collectionFactory->getReport('customer_listing_data_source')->addFieldToSelect(
            'entity_id'
        )->addFieldToSelect(
            'name'
        )->addFieldToSelect(
            'email'
        )->addFieldToSelect(
            'created_at'
        )->addFieldToSelect(
            'billing_telephone'
        )->addFieldToSelect(
            'billing_postcode'
        )->addFieldToSelect(
            'billing_country_id'
        )->addFieldToSelect(
            'billing_region'
        )->addFieldToSelect(
            'billing_company'
        )->addFieldToFilter('entity_id', ['neq' => $this->getRequest()->getParam('id')]);

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * {@inheritdoc}
     */
    protected function _prepareColumns()
    {
        $this->addColumn('entity_id', ['header' => __('ID'), 'width' => '100','index'=>'entity_id']);
        $this->addColumn('name', ['header' => __('Name'), 'width' => '100','index'=>'name']);
        $this->addColumn('email', ['header' => __('Email'), 'width' => '100','index'=>'email']);
        $this->addColumn(
            'created_at',
            ['header' => __('Customer Since'), 'index' => 'created_at', 'type' => 'datetime']
        );
        $this->addColumn('billing_telephone', ['header' => __('Phone'),'index'=>'billing_telephone']);
        $this->addColumn('billing_postcode', ['header' => __('ZIP'), 'index' => 'billing_postcode']);
        $this->addColumn('billing_country_id', ['header' => __('Country'), 'index' => 'billing_country_id']);
        $this->addColumn('billing_region', ['header' => __('State/Province'), 'index' => 'billing_region']);
        $this->addColumn('billing_company', ['header' => __('Company'), 'index' => 'billing_company']);
        
        return parent::_prepareColumns();
    }

    /**
     * Retrieve the Url for a specified sales order row.
     *
     * @param \Magento\Sales\Model\Order|\Magento\Framework\DataObject $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('mergecustomer/merge/form', ['master'=>$this->getRequest()->getParam('id'), 'merge' => $row->getId()]);
    }
}
