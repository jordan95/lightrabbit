<?php
/**
 *
 * Copyright © 2017 CommerceExtensions. All rights reserved.
 * See COPYING.txt for license details.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 */
 
namespace CommerceExtensions\Mergecustomers\Block\Adminhtml\Merge;

class Edit extends \Magento\Backend\Block\Widget\Form\Container
{
 /**
  * Constructor
  *
  * @return void
  */
    protected function _construct()
    {
        parent::_construct();
        $customerId = $this->getRequest()->getParam('master');// get id of master customer
        $this->_objectId = 'entity_id';
        $this->_controller = 'adminhtml_merge';
        $this->_blockGroup = 'CommerceExtensions_Mergecustomers';
        $this->buttonList->update('save', 'label', __('Complete Merge'), 'onClick', 'confirmSubmit()');// Change the label of save button
        //$this->buttonList->add('back')
        $this->buttonList->remove('back');
        $this->buttonList->remove('reset');
        $this->addButton(
            'my_back_button',
            [
            'label' => __('Back'),
            'onclick' => 'setLocation(\'' . $this->getUrl('customer/index/edit/id/'.$customerId.'/') . '\')',
            'class' => 'back'
            ],
            -1
        );//Back Button
        $this->addButton(
            'cancel_merge_button',
            [
            'label' => __('Cancel Merge'),
            'onclick' => 'setLocation(\'' . $this->getUrl('customer/index/edit/id/'.$customerId.'/') . '\')',
            'class' => 'primary'
            ],
            -1
        );//Cancel merge button
    }
}
