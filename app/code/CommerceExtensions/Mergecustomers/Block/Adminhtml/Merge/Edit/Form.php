<?php
/**
 *
 * Copyright © 2017 CommerceExtensions. All rights reserved.
 * See COPYING.txt for license details.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 */
namespace CommerceExtensions\Mergecustomers\Block\Adminhtml\Merge\Edit;

class Form extends \Magento\Backend\Block\Widget\Form\Generic
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    private $registry;

    /**
     * Form Factory
     *
     * @var \Magento\Framework\Data\FormFactory $formFactory
     */
    private $formFactory;
    
    /**
     * Customer Model
     *
     * @var \Magento\Customer\Model\Customer $customerInfo
     */
    private $customerInfo;
    private $customerRegistry;
    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Customer\Model\Customer $customerInfo
     * @param array $data
     */

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Customer\Model\Customer $customerInfo,
        \Magento\Customer\Model\CustomerRegistry $customerRegistry,
        array $data = []
    ) {
        $this->customerInfo = $customerInfo;
        $this->_registry= $registry;
        $this->customerRegistry = $customerRegistry;
        parent::__construct($context, $registry, $formFactory, $data);
    }
    
    public function _prepareForm()
    {
        $masterId = $this->_registry->registry('master_customer');
         $mergeId = $this->_registry->registry('merge_customer');
         $master = $this->customerInfo->load($masterId)->getData();
         $merge = $this->customerRegistry->retrieve($mergeId)->getData();
         $form = $this->_formFactory->create(
             [
                'data' => [
                    'id' => 'edit_form',
                    'action' => $this->getUrl('mergecustomer/customer/completemerge'),
                    'method' => 'post',
                    'enctype' => 'multipart/form-data',
                ]
             ]
         );
        
        
        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Merge Options')]);// Merge Options Fieldset
        
        $fieldset->addField(
            'merge_direction',
            'select',
            [
                'name' => 'merge_direction',
                'label' => __('Merge Direction'),
                'id' => 'merge_direction',
                'title' => __('Merge Direction'),
                'options'  => ['forward' => 'Merge Customer B into Customer A', 'reverse' => 'Merge Customer A into Customer B'],
                'class' => 'merge-direction',
                'required' => true,
            ]
        );
        
        $note = <<<EOD
<p style="max-width:400px;">The customer being merged into will become the master customer and the other customer will be deleted. For example, if you choose to merge Customer B into Customer A, then Customer A will become the master customer and Customer B will be deleted. The same is true of the reverse.</p>
<p style="max-width:400px;">During the merge all possible customer data will be merged into the master customer. This includes, but is not limited to addresses, orders, product reviews, etc...</p>
<p style="max-width:400px;">The data contained in the Master Customer's "Account Information" tab will be unchanged by the merge.</p>
EOD;

        $fieldset->addField('merge_note', 'note', [
        'name'     => 'merge_note',
        'label'    => __('Note'),
        'title'    => __('Note'),
        'text'     => __($note),
        'required' => true,
        ]);
    
        $keys = ['a' => $master, 'b' => $merge];
        foreach ($keys as $key => $model) {
              $fieldset = $form->addFieldset("customer_{$key}_fieldset", ['legend' => __('Customer '.strtoupper($key))]);
              $fieldset->addField("customer_{$key}", 'note', [
            'name'  => "customer_{$key}",
            'label' => __('Customer ID'),
            'title' => __('Customer ID'),
            'text'  => __($model['entity_id'])
              ]);
              $fieldset->addField("customer_{$key}_name", 'note', [
                'name'  => "customer_{$key}_id",
                'label' => __('Customer Name'),
                'title' => __('Customer Name'),
                'text'  => $this->escapeHtml(__($model['firstname'].' '.$model['lastname']))
              ]);// Customer A fieldset
              $fieldset->addField("customer_{$key}_id", 'hidden', [
                'name'  => "customer_{$key}_id",
                'value' => $model['entity_id']
              ]);// Customer B fieldset
        }
    
        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
    }
}
