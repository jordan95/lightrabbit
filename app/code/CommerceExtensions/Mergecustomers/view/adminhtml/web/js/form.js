
require(["jquery"], function ($) {

    $(document).ready(function () {
        
        $('#save').on('click',function (event) {
        
            if (!confirm('Are you sure you want to merge these customers? This cannot be undone.')) {
            event.stopImmediatePropagation();
            }
        });
    });
});
