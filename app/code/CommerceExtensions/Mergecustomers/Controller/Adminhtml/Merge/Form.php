<?php
/**
 *
 * Copyright © 2017 CommerceExtensions. All rights reserved.
 * See COPYING.txt for license details.
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 */
namespace CommerceExtensions\Mergecustomers\Controller\Adminhtml\Merge;

use Magento\Backend\App\Action;
use Magento\Framework\View\Result\PageFactory;

class Form extends \Magento\Backend\App\Action
{
    /**
     * PageFactory
     *
     * @var Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    protected $resultPageFactory;
    
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $registry;
    
    /**
     * Customer Model
     *
     * @var \Magento\Framework\View\Result\Layout
     */
    protected $customerInfo;
    
    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Customer\Model\Customer $customerInfo
     */
    
    public function __construct(
        Action\Context $context,
        PageFactory $resultPageFactory,
        \Magento\Framework\Registry $registry,
        \Magento\Customer\Model\Customer $customerInfo
    ) {
        parent::__construct($context);
        $this->registry  = $registry;
        $this->customerInfo = $customerInfo;
        $this->resultPageFactory = $resultPageFactory;
    }

    public function execute()
    {
        try {
            $masterId = $this->getRequest()->getParam('master');
            if (!$masterId) {
                $this->messageManager->addError(__('Master customer Id not present'));
                $this->_redirect('customer/index/index');
            }

            $mergeId = $this->getRequest()->getParam('merge');
            if (!$mergeId) {
                $this->messageManager->addError(__('Merge customer Id not present'));
                $this->_redirect('customer/index/index');
            }

            if ($mergeId == $masterId) {
                $this->messageManager->addError(__('The Master Customer and the Merge Customer must be different. You have selected to merge the Master Customer into itself.'));
                $this->_redirect('customer/index/index');
            }
        
            $this->registry->register('master_customer', $masterId); // Saving master customer id in registry variable master_customer
            $this->registry->register('merge_customer', $mergeId); // Saving merge customer id in registry variable master_customer
            $resultPage = $this->resultPageFactory->create();
            $resultPage->getConfig()->getTitle()->prepend(__('Merge Customer'));
            return $resultPage;
        } catch (\Exception $e) {
            $this->messageManager->addError(__($e->getMessage()));
            return $resultPage;
        }
    }
}
