<?php
/**
 *
 * Copyright © 2017 CommerceExtensions. All rights reserved.
 * See COPYING.txt for license details.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 */
namespace CommerceExtensions\Mergecustomers\Controller\Adminhtml\Customer;

class CompleteMerge extends \Magento\Backend\App\Action
{
    /**
     * Customer Model
     *
     * @var \Magento\Framework\View\Result\Layout
     */
    protected $customerInfo;
    
    /**
     * Sales Order
     *
     * @var \Magento\Sales\Model\OrderFactory $salesOrder
     */
    protected $salesOrder;
    
    /**
     * Rating Vote Option
     *
     * @var \Magento\Review\Model\Rating\Option\VoteFactory $ratingOptionVote
     */
    protected $ratingOptionVote;
    
    /**
     * Resource
     *
     * @var \Magento\Framework\App\ResourceConnection $resource
     */
    protected $resource;
    
    /**
     * Wishlist
     *
     * @var \Magento\Wishlist\Model\Wishlist $wishlist
     */
    protected $wishlist;
    
    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Customer\Model\Customer $customerInfo
     * @param \Magento\Sales\Model\OrderFactory $salesOrder
     * @param \Magento\Review\Model\Rating\Option\VoteFactory $ratingOptionVote
     * @param \Magento\Framework\App\ResourceConnection $resource
     * @param \Magento\Wishlist\Model\Wishlist $wishlist
     */
    
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Customer\Model\Customer $customerInfo,
        \Magento\Sales\Model\OrderFactory $salesOrder,
        \Magento\Review\Model\Rating\Option\VoteFactory $ratingOptionVote,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Wishlist\Model\Wishlist $wishlist
    ) {
    
        parent::__construct($context);
        $this->ratingOptionVote = $ratingOptionVote;
        $this->_resource = $resource;
        $this->salesOrder = $salesOrder;
        $this->wishlist = $wishlist;
        $this->customerInfo = $customerInfo;
    }
    public function execute()
    {
        try {
            $direction = $this->getRequest()->getParam('merge_direction');
      
            if (!$direction) {
                $this->messageManager->addError(__('Please provide merge direction.'));
                $this->_redirect($this->_redirect->getRefererUrl());
            }

            $customerAId = $this->getRequest()->getParam('customer_a_id');
            if (!$customerAId) {
                $this->messageManager->addError(__('Please provide customer id of customer A.'));
                $this->_redirect($this->_redirect->getRefererUrl());
            }

            $customerBId = $this->getRequest()->getParam('customer_b_id');
     
            if (!$customerBId) {
                $this->messageManager->addError(__('Please provide customer id of customer B.'));
                $this->_redirect($this->_redirect->getRefererUrl());
            }
        
            $master = $this->customerInfo;
            $merge = clone $master; // Object Cloning because of the singleton type of $this->customerInfo object
     
         /* Deciding Master and Merge Customer on the basis of merge direction */
      
            if ($direction == 'reverse') {
                $master->load($customerBId);
                $merge->load($customerAId);
            } else {
                $merge->load($customerBId);
                $master->load($customerAId);
            }
        
          /** Reassign Merge Customer Addresses to Master Customer */
            $i = 0;
            foreach ($merge->getAddresses() as $address) {
                $address->setParentId($master->getId());
                $address->setCustomerId($master->getId());
                if ($address->save()) {
                    $i++;
                }
            }
      
            if ($i) {
                $this->messageManager->addSuccess(__($i.' addresses were merged into the master customer.'));
            }
            $models = [
            'order(s)'           => $this->salesOrder,
            'product ratings(s)' => $this->ratingOptionVote
            ];

            foreach ($models as $label => $model) {
                  $i = 0;
                  $collection = $model->create()->getCollection();
                  $collection->addFieldToFilter('customer_id', $merge->getId());
                foreach ($collection as $object) {
                    $object->setCustomerId($master->getId());
                    if ($object->save()) {
                        $i++;
                    }
                }
                if ($i) {
                      $this->messageManager->addSuccess(__($i.' '.$label.' were merged into the master customer.'));
                }
            }
          /* Directly changing into tables as done in magento 1 extention */
      
            $connection = $this->_resource->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
            $showTables =$connection->fetchCol('show tables');
      
            $tables   = [
            'product review(s)'    => 'review_detail',
            'shipment(s)'          => 'sales_shipment',
            'downloadable link(s)' => 'downloadable_link_purchased',
            'price alert(s)'       => 'product_alert_price',
            'stock alert(s)'       => 'product_alert_stock',
            'sales_order_address',
            'sales_order_grid',
            'quote',
            'quote_address',
            'Downloadable Links'=>'downloadable_link_purchased'
            ];
        
            $column = 'customer_id';
      
            foreach ($tables as $label => $table) {
                if ($connection->tableColumnExists($this->_resource->getTableName($table), $column)) {
                    $count = $connection->rawQuery('UPDATE '.$this->_resource->getTableName($table).' set '.$column.' = '.$master->getId().'  WHERE '.$column.' = '.$merge->getId());
                     $numRows = $count->rowCount();
                    if ($numRows && is_string($label)) {
                        $this->messageManager->addSuccess(__($numRows.' '.$label.' were merged into the master customer.'));
                    }
                }
            }
          /* Merging Wishlist */
      
            $wishlistMaster = $this->wishlist;
            $wishListMerge = clone $wishlistMaster; // // Object Cloning because of the singleton type of $this->wishlist object
            $masterWishlist = $wishlistMaster->loadByCustomerId($master->getId());
            $mergeWishlist  = $wishListMerge->loadByCustomerId($merge->getId());
            $successMsg = 'Wishlist merged into the master customer.';
            $wishlistTableName = 'wishlist';
            $wishlistItemTableName = 'wishlist_item';
 
            if (!$masterWishlist->getId() && $mergeWishlist->getId()) {
                  $count = $connection->rawQuery('UPDATE '.$this->_resource->getTableName($wishlistTableName).' set '.$column.' = '.$master->getId().'  WHERE '.$column.' = '.$merge->getId());
                if ($count) {
                    $this->messageManager->addSuccess(__($successMsg));
                }
            } elseif ($masterWishlist->getId() && $mergeWishlist->getId()) {
                  $count = $connection->rawQuery('UPDATE '.$this->_resource->getTableName($wishlistItemTableName).' set wishlist_id = '.$masterWishlist->getId().'  WHERE wishlist_id = '.$mergeWishlist->getId());
                if ($count) {
                    $this->messageManager->addSuccess(__($successMsg));
                }
                  $mergeWishlist->delete(); // Deleting wishlist of the merge customer
            }
      
            $merge->delete(); // Deleting merge customer
            $finalMessage = 'MERGE COMPLETE. Customer ID# '.$master->getId().', '.$master->getName().', is now the master customer. Customer ID# '.$merge->getId().', '.$merge->getName().', has been deleted.';
            $this->messageManager->addSuccess(__($finalMessage));
            $this->_redirect('customer/index/index');
        } catch (\Exception $e) {
            $this->messageManager->addError(__($e->getMessage()));
        }
    }
}
